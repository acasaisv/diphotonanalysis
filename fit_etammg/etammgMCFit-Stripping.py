import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import pandas
import sys

def dimuon_mass(df):
    df['dimuon_M2'] = np.zeros(len(df))
    for c in 'X','Y','Z':
        df['dimuon_M2'] -= df.eval(f'(mu1_P{c}+mu2_P{c})**2')
    df['dimuon_M2'] += df.eval(f'(mu1_PE+mu2_PE)**2')
    df['dimuon_M'] = np.sqrt(df['dimuon_M2'])
    return df


root = '/scratch47/adrian.casais/ntuples/turcal'
#Bs mass window
m1eta,m2eta = 440,680
hdf = 0
efficiency=0
if not hdf:
    
    #f_s = uproot.open(root + '/etammgHardPhotonMC-all.root')
    #f_s = uproot.open(root + '/etammgHardPhotonMC.root')
    f_s= uproot.open(root + '/etammgMC2018-sim10.root')
    f_s= uproot.open(root + '/etammgMC2018-sim10-newmatching.root')
    t_s = f_s['DecayTree/DecayTree']

    variables = ['eta_M',
                 'eta_P',
                 'eta_PT',
                 'eta_ETA',
                 'eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS',
                 'gamma_L0PhotonDecision_TOS',
                 'gamma_L0ElectronDecision_TOS',
                 'gamma_L0Global_TIS',
                 'gamma_Hlt1Phys_TIS',
                 'gamma_Hlt2Phys_TIS',
                 'mu1_L0DiMuonDecision_Dec',
                 'mu2_L0MuonDecision_Dec',
                 'mu1_IPCHI2_OWNPV',
                 'mu2_IPCHI2_OWNPV',
                 'mu1_ProbNNmu',
                 'mu2_ProbNNmu',
                 'mu1_PT',
                 'mu2_PT',
                 'mu1_PX',
                 'mu1_PY',
                 'mu1_PZ',
                 'mu1_PE',
                 'mu2_PX',
                 'mu2_PY',
                 'mu2_PZ',
                 'mu2_PE',
                 'gamma_PT',
                 'gamma_ETA',
                 'gamma_CL',
                 'gamma_P',
                 'gamma_PZ',
                 'gamma_Matching',
                 'gamma_L0Calo_ECAL_realET',
                 'gamma_L0Calo_ECAL_TriggerET',
                 'gamma_ShowerShape',
                 'gamma_PP_IsNotH',
                 'gamma_PP_IsNotE',
                 'gamma_PP_IsPhoton',
                 'gamma_PP_CaloNeutralID',
                 'gamma_PP_CaloNeutralHcal2Ecal',
                 'nSPDHits',
                 'gamma_CaloHypo_X',
                 'gamma_CaloHypo_Y',
                 'gamma_CaloHypo_Saturation',

                 'gamma_TRUEID',
                 'gamma_MC_MOTHER_ID',
                 'mu1_TRUEID',
                 'mu1_MC_MOTHER_ID',
                 'mu2_TRUEID',
                 'mu2_MC_MOTHER_ID',
                 'eta_TRUEID',
                 'eta_BKGCAT',
                 'eta_MC_MOTHER_ID',
                 'eta_ENDVERTEX_CHI2',
                 ]
    df = t_s.pandas.df(branches=variables)
    #df['eta_ETA'] = 0.5*np.log( (df['eta_P']+df['eta_PZ'])/(df['eta_P']-df['eta_PZ']) )

    df.dropna()
    df.to_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df',mode='w')
else:
    df =pandas.read_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df')

df = dimuon_mass(df)

df.query('eta_M > {0} & eta_M < {1}'.format(m1eta,m2eta),inplace=True)

df.query('gamma_P> 6000 & eta_PT > 1000 and nSPDHits<450 and gamma_PT > 3000',inplace=True)
# mc ='abs(gamma_MC_MOTHER_ID)==221  and  gamma_TRUEID==22 and abs(mu1_TRUEID)==13 and abs(mu2_TRUEID)==13 and eta_TRUEID==221'
df.query('mu1_PT > 500 & mu2_PT > 500 &  mu1_IPCHI2_OWNPV < 6 & mu2_IPCHI2_OWNPV <6 & mu1_ProbNNmu > 0.8 & mu2_ProbNNmu > 0.8',inplace=True)
df.query('eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS',inplace=True)
df.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS ",inplace=True)
df = df.query('dimuon_M<430 and nSPDHits <450')

df_full = df.copy()
mc ='(eta_BKGCAT==0)'
df.query(mc,inplace=True)

print(len(df_full), len(df))


#eta0
Eta1MassRange = zfit.Space('eta_M',(m1eta,m2eta))
Eta1sidebands = zfit.Space('eta_M',(m1eta,480)) + zfit.Space('eta_M',(640,m2eta))


#Signal: Double Crystall Ball
from fit_helpers import create_double_cb, create_gauss
name_prefix='Bs_'
BsCB,BsCBParameters = create_double_cb(name_prefix=name_prefix,
                                             mass_range=Eta1MassRange,
                                             mass =547.862,
                                             sigma=(12,0,20),
                                             nevs=len(df),
                                             extended=False,
                                             # al = 2.25,
                                             # ar = 1.14,
                                             # nl = 0.98,
                                             # nr = 1.49
                                             )

BsGauss,BsGaussParameters = create_gauss(name_prefix=name_prefix+"gauss_",
                                             mass_range=Eta1MassRange,
                                             mass =547.862,
                                             sigma=(5,2,30),
                                             nevs=len(df),
                                             extended=False,
                                             )

BsGauss2,BsGauss2Parameters = create_gauss(name_prefix=name_prefix+"gauss2_",
                                             mass_range=Eta1MassRange,
                                             mass =547.862,
                                             sigma=(25,5,30),
                                             nevs=len(df),
                                             extended=False
                                             )


BsParameters = BsCBParameters

BsParameters['CBGaussFrac'] = zfit.Parameter(name_prefix+'CBGaussFrac',0.82,0,1)
BsParameters['CBGauss2Frac'] = zfit.Parameter(name_prefix+'CBGauss2Frac',0.1,0,1)

signal = zfit.pdf.SumPDF([BsCB,BsGauss,BsGauss2],[BsParameters['CBGaussFrac'], BsParameters['CBGauss2Frac']])
#Background: exponential
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',len(df),0,len(df_full))
nBkg = zfit.Parameter(name_prefix+'nBkgComb',len(df_full)/3,0,len(df_full))
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,-2,2)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,-10,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
BsBkgSBComb = zfit.pdf.Exponential(lambda_Bs,Eta1sidebands)
# BsBkgComb = zfit.pdf.Legendre(Eta1MassRange,[firstOrder,
#                                              #secondOrder,
#                                              #thirdOrder,
#                                              #fourthOrder
#                                              ])
BsCBExtended = signal.create_extended(BsParameters['nSig'])
BsBkgCombExtended = BsBkgComb.create_extended(nBkg)

for key in BsGaussParameters:
    BsParameters[name_prefix+"gauss_"+key]=BsGaussParameters[key]

for key in BsGauss2Parameters:
    BsParameters[name_prefix+"gauss2_"+key]=BsGauss2Parameters[key]


#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
#modelBs = BsCBExtended

data_Bs = zfit.Data.from_numpy(array=df['eta_M'].values,obs=Eta1MassRange)

data_Bs_full = zfit.Data.from_numpy(array=df_full['eta_M'].values,obs=Eta1MassRange)

#CREATE LOSS FUNCTION
#modelBs = BsCBExtended
#Extended
nll = zfit.loss.ExtendedUnbinnedNLL(model=BsCBExtended,
                                    data=data_Bs)

nll_full = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,
                                    data=data_Bs_full)
#nll = zfit.loss.UnbinnedNLL(model=BsCB,
#                                    data=data_Bs)

minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=int(1e8))
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result)

BsParameters["a_l"].floating = False
BsParameters["a_r"].floating = False
BsParameters["n_l"].floating = False
BsParameters["n_r"].floating = False

result2 = minimizer.minimize(nll_full)
result2.hesse()
#result.errors()
print(result2)


from hepstats.splot import compute_sweights
sweights = compute_sweights(modelBs, data_Bs_full)
signal_sweights = sweights[list(sweights.keys())[0]]
df_full['sweights'] = signal_sweights
#print(sweights)
#df['sweights'] = np.ones(len(df))
df_full.to_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df',mode='w')

#PLOT

import plot_helpers
import importlib
importlib.reload(plot_helpers)
nSig=zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['eta_M'],
                      full_model=BsCBExtended,
                      components=[BsCBExtended],
                      yields=[nSig],
                      labels=[r'$\eta\to\mu\mu\gamma$ signal'],
                      colors=['red'],
                      nbins=30,
                      myrange=(m1eta,m2eta),
                      xlabel=r'$m(\mu^+\mu^-\gamma)$ [MeV]',
                      savefile='./etammgMCFit-Stripping.pdf')

plot_helpers.plot_fit(mass=df_full['eta_M'],
                      full_model=modelBs,
                      components=[BsCBExtended,BsBkgCombExtended],
                      yields=[nSig,nBkg],
                      labels=[r'$\eta\to\mu\mu\gamma$ signal', 'background'],
                      colors=['red','green'],
                      nbins=30,
                      myrange=(m1eta,m2eta),
                      xlabel=r'$m(\mu^+\mu^-\gamma)$ [MeV]',
                      savefile='./etammgMCFit-Stripping_full.pdf')


import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
composed_pars = [name_prefix+"m", name_prefix+"m_PDG", name_prefix+"gauss_"+"m", name_prefix+"gauss_"+"m_PDG", name_prefix+"gauss2_"+"m", name_prefix+"gauss2_"+"m_PDG" ,name_prefix+"sigmaR",name_prefix+"sigmaL"]
params = [BsParameters[key] for key in BsParameters.keys() if not BsParameters[key].name in composed_pars]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':[BsParameters[key].name for key in BsParameters.keys() if not BsParameters[key].name in composed_pars],
       # ,'Units':['','MeV','MeV','','','',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))

