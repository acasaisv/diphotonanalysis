from ROOT import *


#c = TCanvas()
gStyle.SetOptStat("")
import pickle
h = []
colour = 1
leg = TLegend()
for i in range(7,27):
    R = i/10.
    with open("isolation_profile/isolation-{0}-R={1}.pickle".format("signal",R,"rb")) as handle:
        isolation=pickle.load(handle)
        print R
        print isolation.count(1.0)
        h.append(TH1F("h_{}".format(R),"",10,0,1.01))
        for i in isolation: h[-1].Fill(i)
        h[-1].SetLineColor(colour)
        leg.AddEntry(h[-1],"{}".format(R),"L")
        colour+=1
        if R==0.7:
            h[-1].GetYaxis().SetRangeUser(0,h[-1].Integral()/1.1)
            h[-1].DrawNormalized()
        else:
            h[-1].DrawNormalized("same")
        
            
leg.Draw()
