from ROOT import *
diffIsoDir="/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation"


L0 = "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
L1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)"
L2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"

f = TFile(diffIsoDir+"/hardPhoton-noprotos.root","UPDATE")
t_ = f.Get("B2GammaGammaTuple/DecayTree")
t = t_.CopyTree(' {0} && {1} && {2}'.format(L0,L1,L2))

iso0 = 'gamma0_iso_0.7'
iso1 = 'gamma_iso_0.7'

print "Isolated gamma0 who's mother is pi0 fraction: {}".format(t.GetEntries("gamma0_iso_0.5 > 0.9 && gamma0_MC_MOTHER_ID==111")/1./t.GetEntries("gamma0_iso_0.5 > 0.9"))
print "Isolated gamma0 that has no mother: {}".format(t.GetEntries("gamma0_iso_0.5 > 0.9 && gamma_MC_MOTHER_ID==0")/1./t.GetEntries("gamma0_iso_0.5 > 0.9"))
print "Isolated gamma0 who has no grandma: {}".format(t.GetEntries("gamma0_iso_0.5 > 0.9 && gamma0_MC_GD_MOTHER_ID==0")//1./t.GetEntries("gamma0_iso_0.5 > 0.9"))

grandmothers = []
ggrandmothers = []
tpi0s = t.CopyTree('gamma0_MC_MOTHER_ID == 111 && gamma0_iso_0.5 >0.1')
for ev in tpi0s:
        grandmothers.append(ev.gamma0_MC_GD_MOTHER_ID)
        ggrandmothers.append(ev.gamma0_MC_GD_GD_MOTHER_ID)


from particle import PDGID
def getProperty(pid,property):
    output = getattr(PDGID(pid),property)

    return output

GMothersFromHF_counter = 0
GGMothersFromHF_counter = 0
for i in grandmothers:
    if (getProperty(i,"has_bottom") or getProperty(i,"has_charm")):
        GMothersFromHF_counter+=1
for i in ggrandmothers:
    if (getProperty(i,"has_bottom") or getProperty(i,"has_charm")):
        GGMothersFromHF_counter+=1

print "Pi0 GMothers from HF fraction: {}".format(GMothersFromHF_counter/1./len(grandmothers))
print "Pi0 GGMothers from HF fraction: {}".format(GGMothersFromHF_counter/1./len(ggrandmothers))
