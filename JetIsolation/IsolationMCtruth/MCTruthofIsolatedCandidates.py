from ROOT import *
backgroundDir = "/scratch27/adrian.casais/ntuples/DiPhotonsBender/background/13144001"
BenderDir = "/scratch27/adrian.casais/ntuples/DiPhotonsBender"
diffIsoDir="/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation"

f = TFile(backgroundDir+"/13144001.root")
f = TFile(BenderDir+"/MB.root")
f = TFile(BenderDir+"/hardPhoton_51.root")
f = TFile(diffIsoDir+"/hardPhoton_51.root")


t = f.Get("B2GammaGammaTuple/DecayTree")

print "Isolated gamma0 who's mother is pi0 fraction: {}".format(t.GetEntries("gamma0_iso ==1 && gamma0_MC_MOTHER_ID==111")/1./t.GetEntries("gamma0_iso == 1"))
# print "Isolated gamma who's mother is pi0 fraction: {}".format(t.GetEntries("gamma_iso ==1 && gamma_MC_MOTHER_ID==111")/1./t.GetEntries("gamma_iso == 1"))
print "Isolated gamma0 who has no grandma: {}".format(t.GetEntries("gamma0_iso ==1 && gamma0_MC_GD_MOTHER_ID==0")//1./t.GetEntries("gamma0_iso == 1"))

grandmothers = []
ggrandmothers = []
for ev in t:
    if ev.gamma0_MC_MOTHER_ID == 111:
        grandmothers.append(ev.gamma0_MC_GD_MOTHER_ID)
        ggrandmothers.append(ev.gamma0_MC_GD_GD_MOTHER_ID)


from particle import PDGID
def getProperty(pid,property):
    output = getattr(PDGID(pid),property)

    return output

GMothersFromHF_counter = 0
GGMothersFromHF_counter = 0
for i in grandmothers:
    if (getProperty(i,"has_bottom") or getProperty(i,"has_charm")):
        GMothersFromHF_counter+=1
for i in ggrandmothers:
    if (getProperty(i,"has_bottom") or getProperty(i,"has_charm")):
        GGMothersFromHF_counter+=1

print "Pi0 GMothers from HF fraction: {}".format(GMothersFromHF_counter/1./len(grandmothers))
print "Pi0 GGMothers from HF fraction: {}".format(GGMothersFromHF_counter/1./len(ggrandmothers))
