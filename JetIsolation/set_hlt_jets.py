from ROOT import TCanvas
##############################################################################
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
from GaudiPython import PyAlgorithm
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *



#from ROOT import *

## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
                          
streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
#DaVinci().EvtMax = 10000
dv1 = DaVinci("dv1")
dv1.PrintFreq = 1000
dv1.appendToMainSequence( [ sc.sequence() ] )
dv1.ProductionType = "Stripping"
#######################
ISGRID  = False
TRIGGER = False

#signal = bool(float(sys.argv[2]))
signal = True


if signal:
    dv1.Simulation = True
    dv1.DataType = "2017"
    dv1.DDDBtag  = "dddb-20170721-3"
    dv1.CondDBtag = "sim-20180411-vc-md100"
    dv1.InputType = "DST" # if running on stripped data... changz

    #DaVinci.Input=["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
    #dv1.Input = ['/scratch08/adrian.casais/dsts/ALPs/Brunel.ldst']
    dv1.Input = ['/scratch48/adrian.casais/dsts/49100040_1100000_2.dst']
else:


    #2018 data don't unblind!!! 
    DaVinci().Simulation = False
    DaVinci().DataType = "2018"
    DaVinci().DDDBtag  = "dddb-20171030-3"
    DaVinci().CondDBtag = "cond-20180202"
    DaVinci().InputType = "DST" # if running on stripped data... changz
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075559/0001/"
    DaVinci.Input = map(lambda x: root + "00075559_000199{}_1.bhadroncompleteevent.dst".format(x),range(16,19))

    
    #NOW set for hardPhoton50, earlier for JpsiKstar thing
    dv1.Simulation = True
    dv1.DataType = "2018"
    dv1.DDDBtag  = "dddb-20170721-3"
    dv1.CondDBtag = "sim-20190430-vc-md100"
    dv1.InputType = "DST" # if running on stripped data... changz
    # root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00102314/0000/"
    # dv1.Input = map(lambda x: root + "00102314_0000000{}_7.AllStreams.dst".format(x),[1]+range(5,13))
    
    # #52
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00102310/0000/"
    DaVinci.Input = map(lambda x: root + "00102310_0000000{}_7.AllStreams.dst".format(x),[1]+range(4,8))


    # #JpsiKstar
    # dv1.Simulation = True
    # dv1.DataType = "2017"
    # dv1.DDDBtag  = "dddb-20170721-3"
    # dv1.CondDBtag = "sim-20180411-vc-mu100"
    # dv1.InputType = "DST" # if running on stripped data... changz
    # root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083521/0000/"
    # DaVinci.Input = map(lambda x: root + "00083521_0000108{}_5.ldst".format(x),range(0,5)+[8])

    # #Minbias
    # dv1.CondDBtag= "sim-20190128-vc-mu100"
    # root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00090846/0000/"
    # DaVinci.Input = map(lambda x: root + "00090846_0000000{}_7.AllStreams.dst".format(x),range(7,11))


    #MB xabi
    
    from GaudiConf import IOHelper
    dv1.Simulation = True
    dv1.DataType = "2016"
    dv1.DDDBtag  = "dddb-20170721-3"
    dv1.CondDBtag = "sim-20170721-2-vc-md100"
    dv1.InputType = "DST"
   # mainpath = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction/MC2016_Priv_md_30000000_0.ldst"
    mainpath = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction/00073338_00000001_7.AllStreams.dst"

    IOHelper('ROOT').inputFiles([mainpath])



# from CommonParticles import StdLoosePhotons
# from CommonParticles import StdLooseAllPhotons,StdLooseMergedPi0,StdLooseResolvedPi0

# from JetAccessories.ParticleFlow_Config import ParticleFlowConf
# from JetAccessories.JetMaker_Config import JetMakerConf
# from JetAccessories.HltJetConf import HltParticleFlowConf, HltJetBuilderConf
# from GaudiKernel import SystemOfUnits as Units

# from PhysConf.Filters import LoKi_Filters
# from CommonParticles import StdLooseAllPhotons

# from Configurables import FilterDesktop, FilterInTrees, ParticleVeto
# from PhysSelPython.Wrappers import Selection, SelectionSequence,DataOnDemand

# from Configurables import TisTosParticleTagger

# ### HLT Configuration #########################################

# _cleanBs0 = TisTosParticleTagger("FilterBs0_TriggerTos")
# _cleanBs0.TisTosSpecs = {"Hlt2RadiativeB2GammaGammaDecision%TOS":0,
#                         "Hlt1B2GammaGammaHighMassDecision%TOS":0,
#                         "Hlt1B2GammaGammaDecision%TOS":0,
#                         "L0PhotonDecision%TOS":0,
#                         "L0ElectronDecision%TOS":0}
# cleanBs0 = Selection("Bs0TriggerTOS",RequiredSelections=[DataOnDemand("Phys/Bs2GammaGamma_NoConvLine/Particles")],Algorithm = _cleanBs0)
# cleanBs0_seq = SelectionSequence("CleanBs0Seq", TopSelection=cleanBs0)

# _gammas = FilterInTrees( "StrippedGammasDesktop",Code = "ABSID=='gamma'" )
# gammas = Selection("StrippedGammas",Algorithm = _gammas,RequiredSelections=[DataOnDemand("Phys/Bs0TriggerTOS/Particles")])
# gammas_seq = SelectionSequence("StrippedGammasSeq", TopSelection=gammas)

# _clear = ClearDaughters('ClBsDaughtersDesktop')
# clear = Selection("ClBsDaughters",Algorithm = _clear,RequiredSelections=[DataOnDemand("Phys/Bs0TriggerTOS/Particles")])
# clear_seq = SelectionSequence("clearSeq", TopSelection=clear)

# _stdphotons = FilterBan('FilterBanPhotons', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
# stdphotons = Selection('StdBannedPhotons', Algorithm = _stdphotons,RequiredSelections=[DataOnDemand('Phys/StdLooseAllPhotons/Particles')])
# stdphotons_seq = SelectionSequence('StdPhotonsSeq',TopSelection=stdphotons)

# _stdpi0s = FilterBan('FilterBanMergedPi0s', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
# stdpi0s = Selection('StdBannedMergedPi0s', Algorithm = _stdpi0s,RequiredSelections=[DataOnDemand('Phys/StdLooseMergedPi0/Particles')])
# stdpi0s_seq = SelectionSequence('StdPi0sSeq',TopSelection=stdpi0s)

# _stdresolvedpi0s = FilterBan('FilterBanResolvedPi0s', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
# stdresolvedpi0s = Selection('StdBannedResolvedPi0s', Algorithm = _stdresolvedpi0s,RequiredSelections=[DataOnDemand('Phys/StdLooseResolvedPi0/Particles')])
# stdresolvedpi0s_seq = SelectionSequence('StdResolvedpi0sSeq',TopSelection=stdresolvedpi0s)

# from GaudiConfUtils.ConfigurableGenerators import Pi0Veto__Tagger

# _tag = Pi0Veto__Tagger2g ('SelBs0Tagger',
#     MassWindow     = 25 * Units.MeV  ,
#     MassChi2       = -1        ,
#     ExtraInfoIndex = 25016     ## unique !
#     )

# seltag  = Selection (
#     "SelBs0Tag",
#     Algorithm          =   _tag   ,
#     RequiredSelections = [ DataOnDemand("Phys/Bs2GammaGamma_NoConvLine/Particles")]
#     )

# seltagseq = SelectionSequence('SelBs0TagSequence',TopSelection=seltag)

# dv1.appendToMainSequence([
#     cleanBs0_seq,
#     clear_seq,
#     gammas_seq,
#     stdphotons_seq,
#     stdpi0s_seq,
#     stdresolvedpi0s_seq,
#     seltagseq
#     ])
# pf_hltconf = HltParticleFlowConf('pf_hlt',
#                                  [
#                                   #['Particle','particle',"Phys/Bs2GammaGamma_NoConvLine/Particles"],
#                                   #['Particle','particle','Phys/StrippedGammas/Particles'],
#                                   #['Particle','particle','Phys/StdLooseAllPhotons/Particles'],
#                                   ['Particle','particle','Phys/StdBannedPhotons/Particles'],
#                                   #'Photons',
#                                   #'ResolvedPi0s',
#                                   #['Particle','particle','Phys/StdBannedMergedPi0s/Particles'],
#                                   #'MergedPi0s',
#                                   ['Particle','particle','Phys/StdBannedResolvedPi0s/Particles'],
#                                   'Ks',
#                                   'Lambda',
#                                   'ChargedProtos',
#                                   #'NeutralProtos',
#                                   #'EcalClusters',
#                                   'HcalClusters',
#                                   'EcalMatch',
#                                   'HcalMatch', 
#                                   'PrimaryVtxs',
#                                   ],
#                                  SprRecover=False,
#                                  EcalBest = True )

# pf_hltconf_withB = HltParticleFlowConf('pf_hlt_withB',
#                                  [
#                                   #['Particle','particle',"Phys/Bs2GammaGamma_NoConvLine/Particles"],
#                                   #['Particle','particle','Phys/StrippedGammas/Particles'],
#                                   #['Particle','particle','Phys/StdLooseAllPhotons/Particles'],
#                                   #['Particle','particle','Phys/StdBannedPhotons/Particles'],
#                                   'Photons',
#                                   'ResolvedPi0s',
#                                   #['Particle','particle','Phys/StdBannedMergedPi0s/Particles'],
#                                   'MergedPi0s',
#                                   #['Particle','particle','Phys/StdBannedResolvedPi0s/Particles'],
#                                   'Ks',
#                                   'Lambda',
#                                   'ChargedProtos',
#                                   'NeutralProtos',
#                                   'EcalClusters',
#                                   'HcalClusters',
#                                   'EcalMatch',
#                                   'HcalMatch', 
#                                   'PrimaryVtxs',
#                                   ],
#                                  SprRecover=False,
#                                  EcalBest = True )

# jb_hltconf = HltJetBuilderConf('jb_hlt',                               
#                                [
#                                 pf_hltconf.getOutputLocation(),
#                                 #'Phys/ClBsDaughters/Particles',
#                                 #"Phys/Bs2GammaGamma_NoConvLine/Particles"
#                                 "Phys/StrippedGammas/Particles"
#                                 ],
#                                #InputBans = ["Phys/StrippedGammas/Particles"],
#                                JetEcPath = '',
#                                JetPtMin = 500*Units.MeV,
#                                JetVrt = False,
#                                JetR = 0.5)

# jb_hltconf_withB = HltJetBuilderConf('jb_hlt_withB',                               
#                                [
#                                 pf_hltconf_withB.getOutputLocation(),
#                                 'Phys/ClBsDaughters/Particles',
#                                 ],
#                                InputBans = ["Phys/StrippedGammas/Particles"],
#                                JetEcPath = '',
#                                JetPtMin = 500*Units.MeV,
#                                JetVrt = False,
#                                JetR = 0.5)

# dv1.appendToMainSequence([pf_hltconf.seq,pf_hltconf_withB.seq,jb_hltconf.seq,jb_hltconf_withB.seq])





 
###########################################################


# # #Append custom Bs0 without the daughters
# from putInTES import *

# putBs0InTES = putInTES('Bs0sNoDaughters')
# putBs0InTES.A1Location = '/Event/Phys/Bs2GammaGamma_NoConvLine/Particles'
# putBs0InTES.A1MuonsLocation = '/Event/Phys/Bs0sNoDaughters/Particles'
# #seq = GaudiSequencer('putBs0InTES')
# dv2.appendToMainSequence(['PyAlgorithm/Bs0sNoDaughters'])
# # gaudi = GaudiPython.AppMgr()
# # TES = gaudi.evtsvc()
# # gaudi.addAlgorithm(putBs0InTES)

