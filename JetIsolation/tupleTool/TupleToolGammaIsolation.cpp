/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TupleToolGammaIsolation.h"

// global
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>
#include "Event/Particle.h"
#include "LoKi/ParticleCuts.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolGammaIsolation
//
// 2014-06-30 : Adrian Casais Vidal (based on Xabier Cid Vidal's work)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TupleToolGammaIsolation )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolGammaIsolation::TupleToolGammaIsolation( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent):
TupleToolBase ( type, name , parent ),
  m_dva(0),
  m_dist(0)
{

  declareInterface<IParticleTupleTool>(this);


  declareProperty("LocationJets",
                  m_loc_jets="/Event/Phys/StdJets/Particles",
                  "Location of the jets");


  declareProperty("IndividualParticlePID",m_ind_part=22,
                  "This is initially for Bmunu or similar, the PID that should be searched for");

  
}

//====================================================================
// Initialize DVAlg, etc
//====================================================================
StatusCode TupleToolGammaIsolation::initialize() {

  // initialize the base class  (the first action)
  StatusCode sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  //initialize the dva algo
  m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc() , this) ;
  if (0==m_dva) return Error("Couldn't get parent DVAlgorithm",
                             StatusCode::FAILURE);




  debug()<< "Initialization correct" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Function to sort according to pT and same PV as B
//=============================================================================
void TupleToolGammaIsolation::pt_sorted_samePV(const LHCb::Particles & jets_list, std::vector<LHCb::Particle*> & out_list)
{
  std::vector<PtParticlePair> myPtParticleVector;
  debug()<< jets_list.size()  <<" jets to be sorted" << endmsg;
  for( LHCb::Particles::const_iterator ijet = jets_list.begin() ;
       ijet != jets_list.end() ; ++ijet){

    PtParticlePair mypair((*ijet)->pt(),(*ijet));
    myPtParticleVector.push_back(mypair);
  }
  //sort by pT (smaller to larger)
  std::sort(myPtParticleVector.begin(), myPtParticleVector.end());
  //and now reverse it to have from larger to smaller
  std::reverse(myPtParticleVector.begin(), myPtParticleVector.end());
  debug()<< myPtParticleVector.size()  <<" jets sorted" << endmsg;

  // now store only the jets again
  for( std::vector<PtParticlePair>::const_iterator ipair = myPtParticleVector.begin() ;
       ipair != myPtParticleVector.end() ; ++ipair){
    out_list.push_back(ipair->second->clone());
  }
  debug()<< "Jet list ready" << endmsg;
}

//=============================================================================
// Find jet with particle
//=============================================================================
StatusCode TupleToolGammaIsolation::find_jet_mu(const LHCb::Particles* list_of_jets,
                                          const LHCb::Particle mu, LHCb::Particle& myjet)
{

  if (!(mu.proto() && mu.proto()->calo().size() && mu.proto()->calo()[0]->hasKey())) return StatusCode::FAILURE;

  debug() << "Looking for jets with muon "<< mu.key() << " inside" <<endmsg;
  debug() << "Particle pT " << mu.pt() << endmsg;

  LHCb::Particles jets_list;
  for( LHCb::Particles::const_iterator ijet = list_of_jets->begin();ijet != list_of_jets->end() ; ++ijet){

    // for each jet, loop in all the daughters
    for( SmartRefVector< LHCb::Particle >::const_iterator idau = (*ijet)->daughters().begin() ; idau != (*ijet)->daughters().end() ; ++idau){
      const LHCb::Particle dau = (*(*idau));
      // check if the daughter has the same key as the particle
      if (!(dau.proto() && dau.proto()->calo().size() && dau.proto()->calo()[0]->hasKey())) continue;
      debug() << "Daughter key: " << dau.proto()->calo().at(0)->key() << endmsg;
      debug() << "Gamma key: " << mu.proto()->calo().at(0)->key() << endmsg;
      //if (dau.proto()->key() == mu.proto()->key())
      if (dau.proto()->calo().at(0)->key()==mu.proto()->calo().at(0)->key())
      { debug() << "Both have the same key" << endmsg;
        jets_list.insert(*ijet,(*ijet)->key());
        debug()<< "Jet " << (*ijet)->key() << " has the muon inside!" <<endmsg;
        break;
      }

    }
  }
  debug() << "Looping over jets ended" <<endmsg;
  // if more than 1, store the one with larger pT
  if (!jets_list.size()){
    debug() << "No gammas matched" << endmsg;
    return StatusCode::FAILURE;
  }
  debug()<< jets_list.size() << " jets found with the muon inside" <<endmsg;
  std::vector<LHCb::Particle*> jets_list_sorted;
  pt_sorted_samePV(jets_list, jets_list_sorted); // the -1 is to avoid any PV check
  debug()<< " These jets have been sorted " <<endmsg;
  if (!jets_list_sorted.size()) return StatusCode::FAILURE;
  myjet = *(jets_list_sorted.at(0));
  return StatusCode::SUCCESS;
}





//=============================================================================
// Empty properties list
//=============================================================================
std::map <std::string,double> TupleToolGammaIsolation::emptyProperties(void)
{
  std::map <std::string,double> out;
  out["Px"]= -10.;
  out["Py"]= -10.;
  out["Pz"]= -10.;
  out["PT"]= -10.;
  out["JetWidth"]= -10.;
  out["MNF"]= -10;
  out["ISOLATION"] = 1.;
  debug() << "empty dict created" << endmsg;
  return out;
}


//=============================================================================
// Empty properties list
//=============================================================================
std::map <std::string,double> TupleToolGammaIsolation::fillProperties(LHCb::Particle* jet, const LHCb::Particle* part )
{
  std::map <std::string,double> out;
  out["Px"]= jet->momentum().x();
  out["Py"]= jet->momentum().y();
  out["Pz"]= jet->momentum().z();
  out["PT"]= jet->pt();
  out["JetWidth"]= LoKi::Cuts::INFO(9007,-10.)(jet);
  out["MNF"]= LoKi::Cuts::INFO(9012,-10.)(jet);
  out["ISOLATION"]= part->pt()/jet->pt();
  debug() << "Dictionary created for jet " << jet->key() << endmsg;
  debug() << "with value " << out << endmsg;
  return out;
}


//=============================================================================
// Fill Output m_map
//=============================================================================
void  TupleToolGammaIsolation::fillInfo(const std::string& prefix,
                                  Tuples::Tuple& tuple){
  bool test = true;

  
  test &= tuple->column(prefix+"_JETPX",m_JetMu1["Px"]);
  test &= tuple->column(prefix+"_JETPY",m_JetMu1["Py"]);
  test &= tuple->column(prefix+"_JETPZ",m_JetMu1["Pz"]);
  test &= tuple->column(prefix+"_JETPT",m_JetMu1["PT"]);
  test &= tuple->column(prefix+"_JETWIDTH",m_JetMu1["JetWidth"]);
  test &= tuple->column(prefix+"_JETMNF",m_JetMu1["MNF"]);
  test &= tuple->column(prefix+"_ISOLATION",m_JetMu1["ISOLATION"]);
  
  if (!test) Warning("Error in filling values "+prefix, StatusCode::SUCCESS, 1).ignore();
  }




//=============================================================================
// Fill Jets RelatedInfo structure
//=============================================================================
StatusCode TupleToolGammaIsolation::fill( const LHCb::Particle* top
                                    , const LHCb::Particle* part
                                    , const std::string& head
                                    , Tuples::Tuple& tuple)
{

  const std::string prefix = fullName(head);

  debug() << "The top particle is " << top->key() << endmsg;
  debug() << "And the particle is " << part->key() << endmsg;
  debug() << "Top particle ID: " << top->particleID().pid() << endmsg;
  debug() << "Particle ID: " << part->particleID().pid() << endmsg;
  debug() << "Particle pT 0 " << part->pt() << endmsg;

  m_JetMu1 = emptyProperties();
  fillInfo(prefix,tuple);

  debug()<< "Filled empty dicts" << endmsg;

  // only fill infos for top particle!
  //if (part!=top) return StatusCode::SUCCESS;

  LHCb::Particle mu1;
  mu1 = *part;

  
  

  debug() << "Jets with mu" <<endmsg;
  if ( exist<LHCb::Particles>(m_loc_jets) )
   {
     const LHCb::Particles*  stdjets_inc = get<LHCb::Particles>(m_loc_jets);
     LHCb::Particle myjet1;
     StatusCode sc1 = find_jet_mu(stdjets_inc, *part, myjet1);
     if (sc1.isSuccess()){
      debug() << "find_jet_mu isSuccess" << endmsg;
      m_JetMu1 = fillProperties(&myjet1,part);
      debug()<< "Jets with mu1, top pT " << myjet1.pt()<< endmsg;

      fillInfo(prefix,tuple);
      }
    

    }
  


      

  
  return StatusCode::SUCCESS;
}

