/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLGAMMAISOLATION_H
#define TUPLETOOLGAMMAISOLATION_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface

#include "Kernel/IDistanceCalculator.h"
#include "Event/HltObjectSummary.h"

#include "Kernel/ITriggerTisTos.h"

struct IDVAlgorithm;

typedef std::pair<double,LHCb::Particle*> PtParticlePair;

/** @class TupleToolGammaIsolation TupleToolGammaIsolation.h
 *
 *
 *  @author Adrian Casais Vidal
 *  @date   2014-06-30
 */
class TupleToolGammaIsolation : public TupleToolBase,
                          virtual public IParticleTupleTool{

public:
  /// Standard constructor
  TupleToolGammaIsolation( const std::string& type,
                        const std::string& name,
                        const IInterface* parent);

  StatusCode initialize() override;


  StatusCode fill( const LHCb::Particle*
                   , const LHCb::Particle*
                   , const std::string&
                   , Tuples::Tuple& ) override;

protected:

private:

  std::string m_loc_jets; //location of jets with no muons
  bool m_onemu; // one muon only (for Bmunu)
  int m_ind_part; // pid of the individual particle (13 for Bmunu)

  IDVAlgorithm *m_dva; // parent DVA Algorithm
  const IDistanceCalculator* m_dist;  // for obtaining the best PV
  //IJetTagTool*  m_nnjettag; //Jet Tag NN
    ITriggerTisTos* m_TriggerTisTosTool;
  // maps for all the extra info for all the jets
  std::map <std::string,double> m_JetMu1;
  std::map <std::string,double> m_JetMu2;


  

  void pt_sorted_samePV(const LHCb::Particles & jets_list , std::vector<LHCb::Particle*> & out_list);

  StatusCode find_jet_mu(const LHCb::Particles* list_of_jets,
                         const LHCb::Particle mu, LHCb::Particle& myjet);

  void fillInfo(const std::string& prefix, Tuples::Tuple& tuple);
  std::map <std::string,double> fillProperties(LHCb::Particle* jet, const LHCb::Particle* part);
  std::map <std::string,double> emptyProperties(void);

};

#endif // TUPLETOOLGammaIsolation_H
