from ROOT import TCanvas
##############################################################################
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *
from ROOT import *

## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
                          
streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
#DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().ProductionType = "Stripping"
#######################
ISGRID  = False
TRIGGER = False

#signal = bool(sys.argv[1])
signal = 0
if signal:
    DaVinci.Input=["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
else:
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083517/0000/"
    DaVinci.Input = map(lambda x: root + "00083517_0000183{}_5.ldst".format(x),range(0,6))

DaVinci().Simulation = True
DaVinci().DataType = "2017"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20180411-vc-md100"
DaVinci().InputType = "DST" # if running on stripped data... change

pflow = ParticleFlowConf("PF" , _InputParticles=['Photons',
                                                 'PFNeutrals',
                                                 #"Pi0s",
                                                 ],
                         _params= {"MinPhotonEt":0.,
                                   "MinIsoPhotonEt":0.,
                                   "MinEt":0.,
                                   "MinPhotonID4Photon": -5.,
                                   "UseHCAL":True,
                                   "MinHCALEt":0.,
                                   "NeutralRecovery":False,
                                   "MC_recovery":False,
                                    } )    # for DATA on should put _MCCor = False , this sets up the E/p version

DaVinci().UserAlgorithms +=pflow.algorithms

jetmaker = JetMakerConf("StdJets",
                        PtMin=500.,
                        JetIDCut=False,
                        R=0.7,
                        JetEnergyCorrection=False,
                        AssociateWithPV=False,
                        pvassociationMinIPChi2=False)
DaVinci().UserAlgorithms+=jetmaker.algorithms

# from JetTagging.PseudoTopoCandidates_Config import PseudoTopoCandidatesConf
# pseudoTopo = PseudoTopoCandidatesConf("PseudoTopoCands")
# DaVinci().UserAlgorithms+=pseudoTopo.algorithms
gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()


def pt_matching(line_gamma,jet_gamma,tolerance=0.01):
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    return (jet_gamma.particleID().pid() == 22 and abs(line_gamma.pt()-jet_gamma.pt())/line_gamma.pt()<=tolerance)

def kinetic_matching(line_gamma,jet_gamma,tolerance = [0.1,0.1]):
    if not jet_gamma.particleID().pid()==22: return False
    #if not jet_gamma.proto() or not line_gamma.proto(): return False
    
    x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
    y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
    pos_match = x_match and y_match
    pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]

    return (pos_match and pt_match)
    
def id_matching(line_gamma,jet_gamma):
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    return (jet_gamma.particleID().pid()==22 and line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())

#gaudi.run(1)
isolation = []
mass_isolated=[]
masses = []
#iso_requirement = float(sys.argv[2])
#iso_requirement = 0.9
total_in_line = 0
iso_counters = {}
iso_efficiencies = {}
for i in range(1,11):
    iso = i/10.
    iso_counters[iso]=0
    iso_efficiencies[iso]=0

#for i in range(int(sys.argv[3])):
for i in range(5000):
    gaudi.run(1)
    line=TES["/Event/Phys/Bs2GammaGamma_NoConvLine/Particles"]
    jets=TES["/Event/Phys/StdJets/Particles"]
    PF = TES["/Event/Phys/PFParticles/Particles"]
    PF_gammas = filter(lambda x: x.particleID().pid() == 22, PF)
    
    if line and line.size() and jets and jets.size() :
        total_in_line += line.size()
        # if line.size() and jets.size():
        #      break
        
        for par in line:
            
            g0 = par.daughters()[0]
            g1 = par.daughters()[1]
            k0 = g0.proto().calo()
            k1 = g1.proto().calo()
            # if k0.size()>1:
            #     print k0.size()
            #     break
            iso0=1.
            iso1=1.
            if map(lambda x: (id_matching(g0,x) or id_matching(g1,x)),PF_gammas).count(True)<2:
        
                isolation += [0.,0.]
                
                continue
                
            #if breaker: break
            
            for jet in jets:
                for daug in jet:
                    #if daug.key() == k0:
                    if id_matching(g0,daug):
                        iso0=daug.pt().value()/jet.pt().value()
                        #print "Isolation 0: {}".format(iso0)
                        
                    #if daug.key() == k1:
                    if id_matching(g1,daug):
                        iso1=daug.pt().value()/jet.pt().value()
                        #print "Isolation 1: {}".format(iso1)
            masses.append(par.momentum().M())
            for i in range(1,11):
                iso = i/10.
                if iso0 >=iso and iso1 >= iso1:
                    iso_counters[iso] += 1.
            # if (iso0>=iso_requirement and iso1>=iso_requirement):
            #     mass_isolated.append(par.momentum().M())
                #break
            isolation.append(iso0)
            isolation.append(iso1)
        #if breaker: break


for i in range(1,11):
    iso = i/10.
    iso_efficiencies[iso] = iso_counters[iso]/total_in_line
    #print "Isolation = {0} -> {1} % efficiency".format(iso,iso_efficiencies[iso])
    print "{},".format(iso_efficiencies[iso])

# h_masses_iso = TH1F("h_mass_iso","",40,4000,10000)
# for m in mass_isolated: h_masses_iso.Fill(m)
# h_masses = TH1F("h_mass","",40,4000,10000)
# for m in masses: h_masses.Fill(m)
# c = TCanvas()
# h_masses.SetLineColor(kRed)
# h_masses_iso.SetLineColor(kBlack)
# leg = TLegend()
# leg.AddEntry(h_masses,"No isolation","L")
# leg.AddEntry(h_masses_iso,"Isolation","L")

# h_masses.Draw()
# h_masses_iso.Draw("same")
# leg.Draw()


hiso = TH1F("iso","iso",10,0.,1.01)
for i in isolation: hiso.Fill(i)
hiso.Draw()            


# type="background"
# import pickle
# # with open("isolation-{}.pickle".format(type),"wb") as handle:
# #     pickle.dump(isolation,handle)

# with open("isolation-signal.pickle".format(type),"rb") as handle:
#     signal = pickle.load(handle)

# with open("isolation-background.pickle".format(type),"rb") as handle:
#     background = pickle.load(handle)

# h_background = TH1F("background","",10,0.,1.01)
# h_signal = TH1F("signal","",10,0.,1.01)
# for i in signal: h_signal.Fill(i)
# for i in background: h_background.Fill(i)
