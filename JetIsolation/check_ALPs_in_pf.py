from ROOT import TCanvas
##############################################################################
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *
from ROOT import *

## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf
from GaudiKernel.ProcessJobOptions import importOptions
sys.argv = [ "" , "1.0","0"]
importOptions("set_jets.py")


gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()

def pt_matching(line_gamma,jet_gamma,tolerance=100):
    return (jet_gamma.particleID().pid() == 22 and abs(line_gamma.pt()-jet_gamma.pt())<=tolerance)

def id_matching(line_gamma,jet_gamma):
    if not jet_gamma.proto(): return False
    return (jet_gamma.particleID().pid() == 22 and line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())

def kinetic_matching(line_gamma,jet_gamma,tolerance = [0.1,0.1]):
    if not jet_gamma.particleID().pid()==22: return False
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    
    x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
    y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
    pos_match = x_match and y_match
    pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]

    return (pos_match and pt_match)

###ARE GAMMAS IN PF ?
count  = 0
tot_in_line = 0

for i in range(10000):
    gaudi.run(1)
    line=TES["/Event/Phys/Bs2GammaGamma_NoConvLine/Particles"]
    
    jets=TES["/Event/Phys/StdJets/Particles"]
    PF = TES["/Event/Phys/PFParticles/Particles"]
    PF_gammas = filter(lambda x: x.particleID().pid()==22, PF)
    if line and line.size():# and jets and jets.size() :
        
        
        tot_in_line += line.size()
        bs_pf = filter(lambda x: x.particleID().pid() == 531, PF)
        count+=len(bs_pf)

ef = (count)/1./tot_in_line
print "{} % of gammas in PF (according to ID matching)".format(ef)
###
