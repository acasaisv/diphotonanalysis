#! /usr/bin/env python
import GaudiPython
from GaudiAlgs import TupleAlgo
# from Bender.MainMC import *

class createEmptyTES(TupleAlgo):
    def __init__(self,name):
        TupleAlgo.__init__(self,name)
    def execute(self):
        return GaudiPython.SUCCESS

class putInTES(GaudiPython.PyAlgorithm):    
    def execute(self): 
        TES = GaudiPython.AppMgr().evtsvc()
        A1s = TES[self.A1Location]
        if not (A1s and A1s.size()): return GaudiPython.SUCCESS
        container = GaudiPython.gbl.LHCb.Particles()
        for A1 in A1s:
            A1_clone = A1.clone()
            A1_clone.clearDaughters()
            container.insert(A1_clone)
        TES.registerObject(self.A1MuonsLocation,container)
        return GaudiPython.SUCCESS


