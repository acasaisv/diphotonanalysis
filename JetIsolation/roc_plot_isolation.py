import matplotlib.pyplot as plt
import numpy as np
import pickle
ax = []
for i in range(7,15):
    
    R = i/10.
    with open("isolation_efficiency/isolation_efficiency-{0}-R={1}.pickle".format("signal",R,"rb")) as handle:
        
        signal = pickle.load(handle)
        signal_= range(0,10)
        for i in signal: signal_[int(10*i-1)] = signal[i]
        signal = np.array(signal_)

    with open("isolation_efficiency/isolation_efficiency-{0}-R={1}.pickle".format("background",R,"rb")) as handle:
        background = pickle.load(handle)
        background_= range(0,10)
        for i in background: background_[int(10*i-1)] = background[i]
        background = np.array(background_)
        
    background = 1-background
    ax.append(plt.subplot(111))

    ax[-1].plot(background,signal,"o",label="R={}".format(R))
    ax[-1].set(xlabel='Rejection',
       xlim=[0.0, 1.05],
       ylim =[0.0,1.05],
       ylabel='Efficiency',
       #title=titles[i],
       
       )
    ax[-1].legend()

plt.plot([0,1],[1,0])
plt.show()


    
