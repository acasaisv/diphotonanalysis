from ROOT import TCanvas
##############################################################################
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *
#from ROOT import *

## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
                          
streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
#DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().ProductionType = "Stripping"
#######################
ISGRID  = False
TRIGGER = False

signal = bool(float(sys.argv[2]))
signal = False
if signal:
    DaVinci.Input=["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
else:
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083521/0000/"
    DaVinci.Input = map(lambda x: root + "00083521_0000108{}_5.ldst".format(x),range(0,5)+[8])

DaVinci().Simulation = True
DaVinci().DataType = "2017"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20180411-vc-md100"
DaVinci().InputType = "DST" # if running on stripped data... changz
pflow = ParticleFlowConf("PF" ,
                         _MCCor = True,
                         #_InputParticles=['Photons',
                                #                 'PFNeutrals',
                                #                 #"Pi0s",
                                #                 ],
                         _params= {
                                   # "MinPhotonEt":0.,
                                    #"MinIsoPhotonEt":0.,
                                    #"MinEt":0.,

    
                                    "MinPhotonID4Photon": -1000.,
                                    "MinPhotonID4PhotonTtrack": -1000.,
                                    "MinPhotonIDMax4ResolvedPi0": 1000.,
                                    "MinPhotonIDMin4ResolvedPi0": -2000.,
                                    #"UseHCAL":True,
                                    #"MinHCALEt":0.,
                                    #"NeutralRecovery":False,
                                    #"MC_recovery":False,
                                     "MaxMatchECALTr":   -100000000000000.,
                                     "MaxMatchECALTr_T": -100000000000.,
                                    #"CandidatesToKeepLocation": ["/Event/Phys/Bs2GammaGamma_NoConvLine/Particles"]
                         },
                         )    # for DATA on should put _MCCor = False , this sets up the E/p version
DaVinci().UserAlgorithms +=pflow.algorithms

jetmaker = JetMakerConf("StdJets",PtMin=500.,JetIDCut=False,R=float(sys.argv[1]))
print 80*"-"
print sys.argv[1]
print 80*"-"
DaVinci().UserAlgorithms+=jetmaker.algorithms
