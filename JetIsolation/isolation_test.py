from ROOT import TCanvas
#c = TCanvas()
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *
from ROOT import *

## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf
from GaudiKernel.ProcessJobOptions import importOptions


from LoKiPhysMC.decorators import *
from LoKiPhysMC.functions import mcMatch

sys.argv = ["","0.7","1"]
importOptions("set_hlt_jets.py")
gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()

from GaudiPython.Bindings import gbl
from LinkerInstances.eventassoc import linkedTo, linkedFrom
location = 'Rec/Track/Best'
location_photons = 'Rec/Calo/Photons'
Track = gbl.LHCb.Track
CaloHypo = gbl.LHCb.CaloHypo
MCParticle = gbl.LHCb.MCParticle
LT = linkedTo(MCParticle,Track,location)
LT_photons = linkedTo(MCParticle,CaloHypo,location_photons)

# LT_gammas.range(TES[location_gammas][0])


def pt_matching(line_gamma,jet_gamma,tolerance=0.01):
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    return (jet_gamma.particleID().pid() == 22 and abs(line_gamma.pt()-jet_gamma.pt())/line_gamma.pt()<=tolerance)

def kinetic_matching(line_gamma,jet_gamma,tolerance = [0.1,0.1]):
    if not jet_gamma.particleID().pid()==22: return False
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    
    x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
    y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
    pos_match = x_match and y_match
    pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]

    return (pos_match and pt_match)
    
def id_matching(line_gamma,jet_gamma):
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    return (jet_gamma.particleID().pid()==22 and line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())

def calo_matching(line_gamma,jet_gamma):
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
    if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
    if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
    if not jet_gamma.proto().calo()[0] or not line_gamma.proto().calo()[0]: return False

    return (jet_gamma.proto().calo()[0].data() == line_gamma.proto().calo()[0].data())

#gaudi.run(1)
isolation = []
isolationB = []
mass_isolated=[]
masses = []
#iso_requirement = float(sys.argv[2])
#iso_requirement = 0.9
total_in_line = 0
iso_counters = {}
iso_efficiencies = {}
for i in range(1,11):
    iso = i/10.
    iso_counters[iso]=0
    iso_efficiencies[iso]=0


pids = []
matching = calo_matching
breaker = 0
high_iso_no_daughters = []
breaker_general = False
for i in range(7000):
    if breaker_general: break
    gaudi.run(1)
    line=TES["/Event/Phys/Bs2GammaGamma_NoConvLine/Particles"]
    PF = TES["Rec/pf_hlt/Particles"]
    jets = TES["Phys/jb_hlt/Particles"]
    jets_withB= TES["Phys/jb_hlt_withB/Particles"]
    stdPhotons = TES["Phys/StdBannedPhotons/Particles"]
    stdLoosePhotons = TES["Phys/StdLooseAllPhotons/Particles"]
    strippedGammas = TES['Phys/StrippedGammas/Particles']
    pi0s = TES['Phys/StdLooseMergedPi0/Particles']
    cleanBs0s = TES['Phys/Bs0TriggerTOS/Particles']
    #if line and line.size() and jets and jets.size() :
    if cleanBs0s and cleanBs0s.size():
        #break
        #total_in_line += line.size()
        # if line.size() and jets.size():
        #      break
        
        for par in line:
            if par.m() > 5600: continue
            total_in_line += 1
            g0 = par.daughters()[0]
            g1 = par.daughters()[1]
            k0 = g0.proto().calo()
            k1 = g1.proto().calo()
            
            iso0= -50.
            iso1= -50.
            iso_withB = -50.
            if breaker: break
            breaker = False
            for jet in jets:
                if breaker: break
                for daug in jet:
                    if matching(g0,daug):
                            iso0=daug.pt().value()/jet.pt().value()
                            breaker = True
                            j = jet
                            break
                    if matching(g1,daug):
                            iso1=daug.pt().value()/jet.pt().value()
                            breaker = True
                            j = jet
                            break
                        
            breaker = False
            for jet in jets_withB:
                if breaker: break
                for daug in jet:
                    if daug.particleID().pid() == 531:
                        iso_withB = daug.pt().value()/jet.pt().value()
                        #j = jet
                        #breaker = True
                        break
                        
            # if iso_withB >=0.1 and iso_withB <=0.4:
            #     high_iso_no_daughters.append(j.daughters().size())
            #     print 20*"=="
            #     print iso_withB
            #     print high_iso_no_daughters[-1]
            #     print 20*"=="
            #     breaker_general=True
            #     break

            if max(iso0,iso1) >=0.42 and max(iso0,iso1) <= 0.52:
                print 20*"=="
                print max(iso0,iso1)
                print high_iso_no_daughters[-1]
                print 20*"=="
                breaker_general=True
                break
            
            
            isolation.append(iso0)
            isolation.append(iso1)
            isolationB.append(iso_withB)
            masses.append(par.momentum().M())
            
            
            #BUNCH TO LOOK AT HIGH ISOs and see its MCTruth
            
            # for i in range(5,11):
            #     iso = i/10.
            #     if iso0 >=iso and iso1 >= iso:
            #         iso_counters[iso] += 1.
            #         LT_photons = linkedTo(MCParticle,CaloHypo,location_photons)
            #         if LT_photons.range(g0.proto().calo()[0].target()).size() and iso  :
            #             print "high iso mcmath 1:"
            #             print LT_photons.range(g0.proto().calo()[0].target())[0]
            #             pids.append(LT_photons.range(g0.proto().calo()[0].target())[0].particleID().pid())
            #             # if abs(pids[-1])==5:
            #             #     breaker= 1
            #             #     break
            #         if LT_photons.range(g1.proto().calo()[0].target()).size() and iso == 1:
            #             print "high iso mcmath 2:"
            #             print LT_photons.range(g1.proto().calo()[0].target())[0]
            #             pids.append(LT_photons.range(g1.proto().calo()[0].target())[0].particleID().pid())
                        
                       #  if abs(pids[-1])==5:
            #                 breaker= 1
            #                 break
            # if breaker: break
        
            # if (iso0>=iso_requirement and iso1>=iso_requirement):
            #     mass_isolated.append(par.momentum().M())
                #break
        # if breaker: break
        



#SHIT TO TAKE ACCOUNT OF EFFICIENCIES AND PROFILE AND STORE IT

# for i in range(1,11):
#     iso = i/10.
#     iso_efficiencies[iso] = iso_counters[iso]/total_in_line
#     #print "Isolation = {0} -> {1} % efficiency".format(iso,iso_efficiencies[iso])
#     print "{},".format(iso_efficiencies[iso])


# # type_="background"
# # if bool(float(sys.argv[2])):
# #     type_ = "signal"
# # import pickle
# # with open("isolation_profile/isolation-{0}-R={1}.pickle".format(type_,sys.argv[1]),"wb") as handle:
# #      pickle.dump(isolation,handle)
# # with open("isolation_efficiency/isolation_efficiency-{0}-R={1}.pickle".format(type_,sys.argv[1]),"wb") as handle:
# #      pickle.dump(iso_efficiencies,handle)

# # gaudi.stop()
# # gaudi.finalize()
