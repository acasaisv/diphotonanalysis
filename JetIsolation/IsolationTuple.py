
from   Gaudi.Configuration import *
import GaudiPython
from Configurables import DaVinci
from Configurables import TupleToolGammaIsolation

#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
# from Configurables import LHCbApp

# LHCbApp.OutputLevel = 2
## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
                          
streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
#DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().ProductionType = "Stripping"




    



from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
Butuple = DecayTreeTuple("DTTBs2GammaGamma")
Butuple.Decay = "B_s0 -> ^gamma ^gamma"
Butuple.Inputs = ["Phys/Bs2GammaGamma_NoConvLine/Particles"]

Butuple.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     "TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                   ]

if DaVinci().getProp('Simulation'):
    mc_truth = Butuple.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    Butuple.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        Butuple.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
        #Butupl.TupleToolMCBackgroundInfo.OutputLevel = VERBOSE

                                        
Butuple.addBranches({"B_s0": "B_s0 -> gamma gamma",
                     "gamma": "B_s0 -> ^gamma gamma",
                     "gamma0": "B_s0 -> gamma ^gamma",
                     })

isolation = True
####### ISOLATION TUPLE TOOL ###############
if isolation:
   # pflow = ParticleFlowConf("PF" , _MCCor = False )    # for DATA on should put _MCCor = False , this sets up the E/p version
    pflow = ParticleFlowConf("PF" ,
                         _MCCor = True,
                         _params= {
                                    "MinPhotonID4Photon": -1000.,
                                    "MinPhotonID4PhotonTtrack": -1000.,
                                    "MinPhotonIDMax4ResolvedPi0": 1000.,
                                    "MinPhotonIDMin4ResolvedPi0": -2000.,
                                    "MaxMatchECALTr":   -100000000000000.,
                                    "MaxMatchECALTr_T": -100000000000.,
                         },
                         )    # for DATA on should put _MCCor = False , this sets up the E/p version

    DaVinci().UserAlgorithms +=pflow.algorithms
    R = 0.7
    jetmaker = JetMakerConf("StdJets",
                            PtMin=500.,
                            JetIDCut=False,
                            R=R,
                            JetEnergyCorrection = False,
                            PFTypes=['ChargedHadron','Muon','Electron','Photon','Pi0','HCALNeutrals','V0','Charged0Momentum','BadPhoton','IsolatedPhoton'])
    DaVinci().UserAlgorithms+=jetmaker.algorithms

    IsolationTool = TupleToolGammaIsolation("gamma")
    #IsolationTool.OutputLevel = DEBUG
    IsolationTool.LocationJets = "Phys/StdJets/Particles"
    IsolationTool0 = TupleToolGammaIsolation("gamma0")
    #IsolationTool0.OutputLevel = DEBUG
    IsolationTool0.LocationJets = "Phys/StdJets/Particles"
    Butuple.gamma.ToolList += ["TupleToolGammaIsolation/gamma"]
    Butuple.gamma.addTool(IsolationTool)
    Butuple.gamma0.ToolList += ["TupleToolGammaIsolation/gamma0"]
    Butuple.gamma0.addTool(IsolationTool0)
    
    


############################################
Butuple.addTupleTool('TupleToolTISTOS/TISTOSTool')
Butuple.TISTOSTool.VerboseL0 = True
Butuple.TISTOSTool.VerboseHlt1 = True
Butuple.TISTOSTool.VerboseHlt2 = True
Butuple.TISTOSTool.TriggerList = ["L0ElectronDecision","L0PhotonDecision","Hlt1B2GammaGammaDecision","Hlt1B2GammaGammaHighMassDecision","Hlt2RadiativeB2GammaGammaDecision"]

Butuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
Butuple.LoKi_All.Variables =  {
    'TRUEID' : 'MCID',
    'M'      : 'MCMASS',
    'THETA'  : 'MCTHETA',
    'PT'     : 'MCPT',
    'PX'     : 'MCPX',
    'PY'     : 'MCPY',
    'PZ'     : 'MCPZ',
    'ETA'    : 'MCETA',
    'PHI'    : 'MCPHI'
        }


DaVinci().UserAlgorithms += [Butuple]



DaVinci().TupleFile = "ALPsFromBs2ggStripping-R={}.root".format(R)

#importOptions("./inputMB.py")


#INPUT

#DaVinci.Input=["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
#DaVinci.Input=["./MC2016_Priv_MagDown_ALP5_35.ldst"]
root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083521/0000/"
DaVinci.Input = map(lambda x: root + "00083521_0000108{}_5.ldst".format(x),range(0,5)+[8])

DaVinci().Simulation = True
DaVinci().DataType = "2017"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20180411-vc-md100"
DaVinci().InputType = "DST" # if running on stripped data... change
importOptions("./makeALPs.py")

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()

from GaudiPython import GaudiAlgs
class MyAlg(GaudiAlgs.TupleAlgo):

    def execute(self):
        tup=self.nTuple("Counters")
        tup.column("NoTracks",TES["Rec/Track/Best"].size())
        tup.column("NoPars",TES["MC/Particles"].size())
        tup.column("DummyCounter",1.)

        tup.write()

        return GaudiPython.SUCCESS

gaudi.addAlgorithm(MyAlg("counters"))


gaudi.run(6000)
gaudi.stop()
gaudi.finalize()

