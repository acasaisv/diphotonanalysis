# -*- coding: utf-8 -*-
#!/usr/bin/env python
from Bender.Main import *
import math
import BenderTools.Fill
from   LoKiPhys.decorators import *
import BenderTools.TisTos
from LoKiCore.math import sqrt
from ROOT import *
import os 




def Conditions(Year=2012,Polarity="MagDown"):
    
    if Year == 2012:
        CondDBtag = 'sim-20160321-2-vc-md100'
        DDDBtag = 'dddb-20150928'
    elif Year == 2015:
        CondDBtag = 'sim-20160606-vc-md100'
        DDDBtag = 'dddb-20150724'
    elif Year == 2016:
        CondDBtag = 'sim-20161124-2-vc-md100'
        DDDBtag = 'dddb-20150724'
        
    if Polarity == "MagUp":
        CondDBtag = CondDBtag.replace("md","mu")
    
    return [CondDBtag,DDDBtag]






class Alllg(Algo):
    
    ###############################################################
    def initialize ( self ) :
        
        sc = Algo.initialize ( self )
        if sc.isFailure() : return sc
        
        self.IsMC = True
        
        self.pid = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPid', parent=self)
        if not self.pid : return FAILURE
        
        self.trig = self.tool(cpp.IEventTupleTool,
                              'TupleToolTrigger', parent=self)
        if not self.trig : return FAILURE
        
        self.tuptistos = self.tool(cpp.IParticleTupleTool,
                                   'TupleToolTISTOS', parent=self)
        if not self.tuptistos : return FAILURE
        
        self.kin = self.tool(cpp.IParticleTupleTool,
                             'TupleToolKinematic', parent=self)
        if not self.kin : return FAILURE
        
        self.geo = self.tool(cpp.IParticleTupleTool,
                             'TupleToolGeometry', parent=self)
        if not self.geo : return FAILURE
        
        self.prim = self.tool(cpp.IEventTupleTool,
                              'TupleToolPrimaries', parent=self)
        if not self.prim : return FAILURE
        
        self.track = self.tool(cpp.IParticleTupleTool,
                               'TupleToolTrackInfo', parent=self)
        if not self.track : return FAILURE
        
        self.proptime = self.tool(cpp.IParticleTupleTool,
                                  'TupleToolPropertime', parent=self)
        if not self.proptime : return FAILURE
        
        self.event = self.tool(cpp.IEventTupleTool,
                                'TupleToolEventInfo', parent=self)
        if not self.event : return FAILURE
        
        self.muon = self.tool(cpp.IParticleTupleTool,
                              'TupleToolMuonPid', parent=self)
        if not self.muon : return FAILURE
        
        self.mc = self.tool(cpp.IParticleTupleTool,
                            'TupleToolMCTruth', parent=self)
        if not self.mc : return FAILURE
        
        self.recostats = self.tool(cpp.IEventTupleTool,
                            'TupleToolRecoStats', parent=self)
        if not self.recostats : return FAILURE
        
        self.coneiso = self.tool(cpp.IParticleTupleTool,
                            'TupleToolConeIsolation', parent=self)
        if not self.coneiso : return FAILURE

        self.photoninfo = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPhotonInfo', parent=self)
        if not self.coneiso : return FAILURE
        
        self._dtf = self.decayTreeFitter('LoKi::DecayTreeFit')
        
        if not self._dtf  : return FAILURE
        
        import ROOT 
        ROOT.gInterpreter.ProcessLine("#include \"Kernel/IMatterVeto.h\"") 
        
        self.matterveto = self.tool(cpp.IMatterVeto,
                            'MatterVetoTool', parent=self)
        if not self.matterveto : return FAILURE
        
        triggers = {}
        
        triggers['piplus'] = {}
        triggers['piminus'] = {}

        triggers['eplus'] = {}
        triggers['eminus'] = {}
        
        
        lines = {}
        for key in ["piplus","piminus","eplus","eminus","KS0"]:
            lines [key]  = {}
            lines [key]['L0TOS'] = ''
            lines [key]['L0TIS'] = 'L0(DiMuon|Muon|Photon|Electron)Decision'
            lines [key]['Hlt1TOS'] = ''
            lines [key]['Hlt1TIS'] = 'Hlt1SingleMuon.*Decision'
            lines [key]['Hlt2TOS'] = ''
            lines [key]['Hlt2TIS'] = 'Hlt2SingleMuon.*Decision'
        
        sc = self.tisTos_initialize ( triggers , lines  )
        if sc.isFailure() : return sc


        import GaudiPython
        
        gaudi = GaudiPython.AppMgr()
        TES =gaudi.evtsvc()

        return SUCCESS
    
    ################################################################

    def ISMC( self, sim=True):
    
        self.IsMC = sim

    ## the main 'analysis' method
    def analyse( self ) :   ## IMPORTANT!
        tup = self.nTuple('DecayTree')
        header  = self.get( '/Event/Rec/Header' )
        

        es = ["e+ ","e- "]

        breaker = False
        for e1 in es:
            for e2 in es:
                descriptor = "KS0 -> pi+ pi- " +e1 +e2
                if self.select("ks0s","KS0 -> pi+ pi- " +e1+e2).size():
                    breaker = True
                    
                    break
            if breaker: break
                
        if not breaker: return SUCESS        
        
        
        ks0 = self.select ( 'ks0' , "KS0-> pi+ pi-"+e1+e2)[0]
        print ks0.daughters()
        # piplus = self.select( 'piplus',"KS0->^pi+ pi-"+e1+e2 )[0]
        # piminus = self.select( 'piminus', 'KS0 -> pi+ ^pi-'+e1+e2 )[0]

        
        # eplus = self.select( 'eplus', 'KS0 -> pi+ pi-' +self.select_par(e1)+ e2)[0]
        # eminus = self.select(  'eminus', 'KS0 -> pi+ pi-' +e1 + self.select_par(e2))[0]

        piplus = ks0.daughters()[0]
        piminus = ks0.daughters()[1]
        eminus = ks0.daughters()[2]
        eplus = ks0.daughters()[3]

        #StrippingLine Flag
        
        self.IsStrippingLine(ks0,tup)
        
        
        
        self.prim.fill(tup)
        self.event.fill(tup)
        self.recostats.fill(tup)

        self.tuptistos.fill(0,ks0,"KS0",tup)
        self.tuptistos.fill(0,piplus,"piplus",tup)
        self.tuptistos.fill(0,piminus,"piminus",tup)
        self.tuptistos.fill(0,eplus,"eplus",tup)
        self.tuptistos.fill(0,eminus,"eminus",tup)




        
        
        vv = False
        if eplus.proto().track().type() == "1" and eminus.proto().track().type()==1:
            vv = True
        
        parts = [ks0,eplus,eminus,piplus,piminus]
        names = ["KS0","eplus", "eminus", "piplus","piminus"]
        for part, name in zip(parts,names):
            self.pid.fill(0, part, name, tup)
            self.kin.fill(0, part, name, tup)
            self.geo.fill(0, part, name, tup)
            self.track.fill(0, part, name, tup)
            self.proptime.fill(ks0, part, name, tup)
            tup.column_float ( name+'_ETA', ETA(part))
            tup.column_float ( name+'_Y', Y(part))
            self.CorrMass(ks0, piplus, piminus,eplus,eminus,tup,VV = vv)
            if self.IsMC:
                self.mc.fill(0, part, name, tup)
        
            
            
        
        tup.write()

        return SUCCESS      ## IMPORTANT!!!

    ################################################################
    
    def finalize ( self ) :
        
        del self.pid
        del self.trig
        del self.tuptistos
        del self.kin
        del self.geo
        del self.prim
        del self.track
        del self.proptime
        del self.event
        del self.muon
        del self._dtf
        del self.coneiso
        del self.recostats
        del self.matterveto
        
        return Algo.finalize ( self )

    ################################################################

    
    
    
    
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files
               catalogs = []    ,    ## xml-catalogs (filled by GRID)
               castor   = False ,    ## use the direct access to castor/EOS ?
               params   = {}   ) :
    
    year       = params.get('Year',2016)
    pol        = params.get('Polarity','MagUp')
    info       = params.get('Info','tuple')
    sim        = params.get('Simulation',True)
    stream     = params.get('Stream','EW')
    file_name  = params.get('FileName','ntuple')
    decay_descriptor = params.get('Decay Descriptor','KS0 -> e+ e- pi+ pi-')
    dddbtag = params.get("DDBtag","dddb-20130929-1")
    conddbtag = params.get("CondDBtag","sim-20130522-1-vc-mu100")


    
    ## import DaVinci
    from Configurables import DaVinci, EventSelector
    from KScombinations import myseq, MatchedElectrons_seq, MatchedPions_seq, Ks2pi2e
    bend_sels = {}
    for key in Ks2pi2e:
        bend_sels[key] = BenderSelection(key,Ks2pi2e[key])
    dv = DaVinci (
                  DataType   = str(year) ,
                  InputType  = 'DST'  ,
                  Simulation = sim  )
    



    dv.TupleFile = "tuple.root"
    dv.appendToMainSequence([myseq])
    dv.appendToMainSequence([MatchedElectrons_seq[key] for key in MatchedElectrons_seq])
    dv.appendToMainSequence([MatchedPions_seq])
    dv.appendToMainSequence([bend_sels[key] for key in bend_sels])
    #dv.appendToMainSequence([Ks2pi2e[key] for key in Ks2pi2e])
    #dv.appendToMainSequence([seqsks2pi2e[key] for key in seqsks2pi2e])
                  


    setData(inputdata,catalogs,grid =True,useDBtags = True)

    #Setting Stripping Lines

    from StrippingLines2pi2e import sc

    dv.appendToMainSequence([sc.sequence()])
    dv.ProductionType = "Stripping"

        
    
    #Setting trigger lines for the job
    from DecayTreeTuple.Configuration import TupleToolMCTruth
    from Configurables import TupleToolConeIsolation, TupleToolTrigger, TupleToolTISTOS
    Trigger_List = ['L0HadronDecision'
                        ,'L0ElectronDecision'
        
                        ,'L0PhotonDecision'
                        ,'Hlt1TrackMVADecision'
                        ,'Hlt1TwoTrackMVADecision'
                        ,'Hlt1TrackMVALooseDecision'
                        ,'Hlt1TwoTrackMVALooseDecision'
                        ,'Hlt1TrackMuonDecision'
                        ,'Hlt1SingleElectronNoIPDecision'
                        ,'Hlt1SingleMuonNoIPDecision',
                         'Hlt2DiElectronElSoft']

    Trigger_List.extend(["L0MuonDecision","Hlt1SingleMuonHighPTDecision"])
    conf = {}
    triggertool = {}
    tistostool = {}
    ## Configuring for each algo different tools
    for key in Ks2pi2e:
        
        conf[key] = TupleToolMCTruth(key+'.TupleToolMCTruth')
        conf[key].ToolList = [
            'MCTupleToolKinematic',
            'MCTupleToolHierarchy',
            'MCTupleToolReconstructed',
            ]

        
        triggertool[key] = TupleToolTrigger(key+'.TupleToolTrigger')
        triggertool[key].Verbose = True
        triggertool[key].TriggerList = Trigger_List
        tistostool[key]  = TupleToolTISTOS(key+'.TupleToolTISTOS')
        tistostool[key].Verbose = True
        tistostool[key].TriggerList = Trigger_List

    ## get/create application manager
    gaudi = appMgr()
                  
    ## (1) create the algorithm with the given name
    algs = {}
    for key in Ks2pi2e:
        algs[key] =  Alllg(bend_sels[key])
    
    
        

        
        
    return SUCCESS

#=============================================================================

#=============================================================================
#Job steering
if __name__ == '__main__' :
    HOME = "/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00037694/0000/"
    files_ = filter(lambda x: ".dst" in x,os.listdir(HOME))
    files = map(lambda x: HOME + x,files_)
    configure(inputdata = files,catalogs = [],castor = False,params = {})


    run(1000) 


    import GaudiPython

    gaudi = GaudiPython.AppMgr()
    gaudi.stop()
    gaudi.finalize()
    
#=============================================================================
#The END
#=============================================================================
