from ROOT import TCanvas
##############################################################################
from Gaudi.Configuration import *
from Configurables import *
##############################################################################
import GaudiPython
import sys
#from Bender.MainMC import *
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf
from CommonParticles.StdAllLooseElectrons import *
from CommonParticles.StdAllLooseMuons import *
from CommonParticles.StdLoosePhotons import *
from ROOT import *
from math import sqrt
## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf
from GaudiKernel.ProcessJobOptions import importOptions
sys.argv = [ "" , "2.0","0"]
importOptions("set_jets.py")


gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()

def pt_matching(line_gamma,jet_gamma,tolerance=100):
    return (jet_gamma.particleID().pid() == 22 and abs(line_gamma.pt()-jet_gamma.pt())<=tolerance)

def id_matching(line_gamma,jet_gamma):
    if not jet_gamma.proto(): return False
    if not jet_gamma.proto().calo().size(): return False
    return (line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())

def kinetic_matching(line_gamma,jet_gamma,tolerance = [0.001,0.001]):
    #if not jet_gamma.particleID().pid()==22: return False
    tolerance=[0.01,1]
    if not jet_gamma.proto() or not line_gamma.proto(): return False
    if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
    if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
    if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
    if not jet_gamma.proto().calo()[0].position() or not line_gamma.proto().calo()[0].position(): return False
    x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
    y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
    pos_match = x_match and y_match
    pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]

    return (pos_match)# and pt_match)


def closer_cluster(g,PF):
    default_value = -50
    if not g.proto(): return default_value
    if not 'calo' in dir(g.proto()): return default_value
    if not g.proto().calo().size(): return default_value
    if not g.proto().calo()[0].data(): return default_value
    PF_calo = filter(lambda x: x.proto(),PF)
    PF_calo = filter(lambda x: x.proto().calo(),PF_calo)
    PF_calo = filter(lambda x: x.proto().calo().size(),PF_calo)
    PF_calo = filter(lambda x: x.proto().calo()[0].data(),PF_calo)
    PF_calo = filter(lambda x: x.proto().calo()[0].data().position(),PF_calo)
    position = g.proto().calo()[0].data().position()
    distances = map(lambda x: sqrt((position.x() - x.proto().calo()[0].data().position().x())**2 + (position.y() - x.proto().calo()[0].data().position().y())**2),PF_calo)
    return min(distances)

###ARE GAMMAS IN PF ?
count_g0 = 0
count_g1 = 0
tot_in_line = 0

bs0_counter=0
no_events = 1000
pt_notjets = []
pt_jets = []

def bs0InJets(line,jets):
    for jet in jets:
        for daug in jet:
            if daug.particleID().pid() == 531:
                return True
    return False
                

    
for i in range(no_events):
    gaudi.run(1)
    line=TES["/Event/Phys/Bs2GammaGamma_NoConvLine/Particles"]
    jets=TES["/Event/Phys/StdJets/Particles"]
    PF = TES["/Event/Phys/PFParticles/Particles"]
    #PF_gammas = filter(lambda x: x.particleID().pid()==22, PF)
    
    if line and line.size() and jets and jets.size() :
        for par in line:
            g0 = par.daughters()[0]
            g1 = par.daughters()[1]
            g0_matching=map(lambda x: kinetic_matching(g0,x),PF)
            g1_matching=map(lambda x: kinetic_matching(g1,x),PF)
            count_g0 += g0_matching.count(True)
            count_g1 += g1_matching.count(True)
            tot_in_line += 2
            bs0_found = bs0InJets(line,jets)
            
            #print closer_cluster(g0,PF)
                    
            if not bs0_found:
                pt_notjets.append(par.momentum().pt())
            else:
                pt_jets.append(par.momentum().pt())
            

                
                
        for jet in jets:
            for par in jet:
                if par.particleID().pid() == 531:
                    bs0_counter+=1

                    
ef = 100.*(count_g0+count_g1)/1./tot_in_line
print "{} % of gammas in PF (according to ID matching)".format(ef)
print "{0} B_s0 found out of {1}".format(bs0_counter,tot_in_line/2.)
###

# index = 0

# for par in PF:
#     try:
#         kinetic_matching(g0,par)
#     except:
#         print index
#     index+=1

    
