/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include <algorithm>
#include <functional>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/StatEntity.h"
#include "GaudiKernel/IAlgContextSvc.h"
// ============================================================================
// DaVinci
#include "Kernel/DaVinciFun.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/IHybridFactory.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/algorithm/string.hpp"
// ============================================================================
// local
// ============================================================================
#include "FilterBan.h"
// ============================================================================
/** @file
 *  Implementation for for class FilterBan
 *  @see FilterBan
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef,nl
 *  @date 2008-09-22
 */
// ============================================================================
namespace
{
  // ==========================================================================
  /// make the plots ?
  inline bool validPlots ( const std::string& name )
  {
    if ( name.empty() ) { return false ; }
    return "none" != boost::to_lower_copy ( name ) ;
  }
  /// local "none"
  typedef LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant       _PBOOL ;
  /// local "none"
  typedef LoKi::BasicFunctors<LHCb::Particle::ConstVector>::BooleanConstant _CBOOL ;
  // ==========================================================================
}
// ============================================================================
// the specific initialization
// ============================================================================
StatusCode FilterBan::initialize ()             // the specific initialization
{
  // initialize the base class
  StatusCode sc = DaVinciAlgorithm::initialize () ;
  if ( sc.isFailure () ) { return sc ; }                          // RETURN
  // ensure the proper initialization of LoKi service
  svc<IService> ( "LoKiSvc" ) ;

  // (re)lock the context again:

  // register for the algorithm context service
  IAlgContextSvc* ctx = 0 ;
  if ( registerContext() ) { ctx = contextSvc() ; }
  // setup sentry/guard
  Gaudi::Utils::AlgContext sentry ( ctx , this ) ;

  // decode the cut:
  sc = updateMajor () ;
  if ( sc.isFailure () )
  { return Error ("The error from updateMajor" , sc ) ; }

  // take care about the histos
  sc = updateHistos () ;
  if ( sc.isFailure () )
  { return Error ("The error from updateHistos" , sc ) ; }
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
// finalize /reset functors
// ============================================================================
StatusCode FilterBan::finalize   ()
{
  // reset functors
  m_cut         = _PBOOL ( false ) ;
  m_preMonitor  = _CBOOL ( false ) ;
  m_postMonitor = _CBOOL ( false ) ;
  m_to_be_updated1 = true ;
  // finalize the base
  return DaVinciAlgorithm::finalize () ;
}
// ============================================================================
/* standard constructor
 *  @see DaVinciAlgorithm
 *  @see GaudiTupleAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see Algorithm
 *  @see AlgFactory
 *  @see IAlgFactory
 *  @param name the algorithm instance name
 *  @param pSvc pointer to Service Locator
 */
// ============================================================================
FilterBan::FilterBan                  // standard contructor
( const std::string& name ,                   // the algorithm instance name
  ISvcLocator*       pSvc )                   // pointer to Service Locator
  : DaVinciAlgorithm ( name , pSvc )
// LoKi/Bender "hybrid" factory name
  , m_factory ( "LoKi::Hybrid::Tool/HybridFactory:PUBLIC" )
// the preambulo
  , m_preambulo ()
// the selection functor (predicate) itself
  , m_code ( "PNONE"          )
  , m_cut  ( _PBOOL ( false ) )
// general flag to switch on/off the monitoring
  , m_monitor ( false )
// (pre-monitoring) functor
  , m_preMonitorCode  ( )
  , m_preMonitor      ( _CBOOL ( false ) )
// (post-monitoring) functor
  , m_postMonitorCode ( )
  , m_postMonitor     ( _CBOOL ( false ) )
// input plots
  , m_inputPlotsTool   ( "LoKi::Hybrid::PlotTool/InputPlots"  )
  , m_inputPlots       (  0  )
  , m_inputPlotsPath   ( "I" + name  )
// output plots
  , m_outputPlotsTool  ( "LoKi::Hybrid::PlotTool/OutputPlots" )
  , m_outputPlots      (  0  )
  , m_outputPlotsPath  ( "O" + name  )
// update?
  , m_to_be_updated1   ( true )
  , m_to_be_updated2   ( true )
  , m_cloneFilteredParticles (false)
{
  //
  if      ( 0 == name.find("Hlt1") ) 
  { m_factory = "LoKi::Hybrid::Tool/Hlt1HybridFactory:PUBLIC" ; }
  else if ( 0 == name.find("Hlt2") )
  { m_factory = "LoKi::Hybrid::Tool/Hlt2HybridFactory:PUBLIC" ; }
  //
  // the factory
  declareProperty
    ( "Factory" ,
      m_factory ,
      "The Type/Name for C++/Python Hybrid Factory" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;
  // the preambulo
  declareProperty
    ( "Preambulo" ,
      m_preambulo ,
      "The preambulo to be used for Bender/Pthon script" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;
  // the functor (predicate)
  declareProperty
    ( "Code" ,
      m_code ,
      "The valid LoKi/Bender 'hybrid' functor name" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;
  // input plot tool
  declareProperty
    ( "InputPlotsTool"  ,
      m_inputPlotsTool  ,
      "The Type/Name for 'input' Plots tool" )
    -> declareUpdateHandler ( &FilterBan::updateHandler2 , this ) ;
  // output plot tool
  declareProperty
    ( "OutputPlotsTool" ,
      m_outputPlotsTool ,
      "The Type/Name for 'output' Plots tool" )
    -> declareUpdateHandler ( &FilterBan::updateHandler2 , this ) ;
  // the path for the input plots
  declareProperty
    ( "InputPlotsPath"  ,
      m_inputPlotsPath  ,
      "The THS path for the input plots" )
    -> declareUpdateHandler ( &FilterBan::updateHandler2 , this ) ;
  // the path for the output plots
  declareProperty
    ( "OutputPlotsPath" ,
      m_outputPlotsPath ,
      "The THS path for the output plots" )
    -> declareUpdateHandler ( &FilterBan::updateHandler2 , this ) ;
  //
  declareProperty
    ( "Monitor" ,
      m_monitor ,
      "The general flag to switch on/off the monitoring" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;

  // (pre)monitoring code
  declareProperty
    ( "PreMonitor" ,
      m_preMonitorCode  ,
      "The code used for (pre)monitoring of input particles" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;

  // (post)monitoring code
  declareProperty
    ( "PostMonitor" ,
      m_postMonitorCode  ,
      "The code used for (post)monitoring of output particles" )
    -> declareUpdateHandler ( &FilterBan::updateHandler1 , this ) ;

  // cloning of selected particles and secondary vertices
  declareProperty
    ( "CloneFilteredParticles" ,
      m_cloneFilteredParticles ,
      "Clone filtered particles and end-vertices into KeyedContainers" ) ;
  // Histogramming
  declareProperty
    ( "HistoProduce"          ,
      m_produceHistos = false ,
      "Switch on/off the production of histograms" ) 
    -> declareUpdateHandler ( &FilterBan::updateHandler2 , this ) ;
  //InputBans
  declareProperty
     ( "InputBans"            , 
       m_inBans               ,
       "Input locations of Particles to ban." );
  
}

// ============================================================================
// update the structural property
// ============================================================================
void FilterBan::updateHandler1 ( Property& p )
{
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  /// mark as "to-be-updated"
  m_to_be_updated1 = true ;
  Warning ( "The structural property '" + p.name() +
            "' is updated. It will take effect at the next 'execute'" ,
            StatusCode( StatusCode::SUCCESS, true ) ).ignore() ;
}
// ============================================================================
// update the histogram property
// ============================================================================
void FilterBan::updateHandler2 ( Property& p )
{
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  /// mark as "to-be-updated"
  m_to_be_updated2 = true ;
  debug () << "The histogramming property is updated: " << p << endmsg ;
}
// ============================================================================
void FilterBan::writeEmptyTESContainers()
{

  return m_cloneFilteredParticles ?
    writeEmptyKeyedContainers()  :
    writeEmptySharedContainers() ;

}
// ============================================================================
// construct the preambulo string
// ============================================================================
std::string FilterBan::preambulo() const
{
  std::string result ;
  for ( std::vector<std::string>::const_iterator iline =
          m_preambulo.begin() ; m_preambulo.end() != iline ; ++iline )
  {
    if ( m_preambulo.begin() != iline ) { result += "\n" ; }
    result += (*iline) ;
  }
  return result ;
}
// ============================================================================
// update the major properties
// ============================================================================
StatusCode FilterBan::updateMajor ()
{
  // decode the code
  StatusCode sc = decodeCode () ;
  if ( sc.isFailure() ) { return Error ( "Error from decodeCode()'" ) ; }

  // locate the factory
  LoKi::IHybridFactory* factory = tool<LoKi::IHybridFactory> ( m_factory , this ) ;

  // pre-monitoring:
  if ( monitor() && !m_preMonitorCode.empty () )
  { sc = factory-> get ( m_preMonitorCode  , m_preMonitor  , preambulo() ) ; }
  else { m_preMonitor  = _CBOOL ( false ) ; }

  // post-monitoring:
  if ( monitor() && !m_postMonitorCode.empty() )
  { sc = factory-> get ( m_postMonitorCode , m_postMonitor , preambulo() ) ; }
  else { m_postMonitor = _CBOOL ( false ) ; }

  // release the factory (not needed anymore)
  release ( factory ) ;
  //
  m_to_be_updated1 = false ;
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
// update histos
// ============================================================================
StatusCode FilterBan::updateHistos ()
{
  // ========================================================================
  if ( m_inputPlots  )
  { releaseTool ( m_inputPlots  ) ; m_inputPlots  = nullptr ; }
  if ( m_outputPlots )
  { releaseTool ( m_outputPlots ) ; m_outputPlots = nullptr ; }
  // =========================================================================
  if ( produceHistos () )
  {
    // =======================================================================
    if ( validPlots ( m_inputPlotsTool ) )
    {
      m_inputPlots = tool<IPlotTool>( m_inputPlotsTool, this ) ;
      StatusCode sc = m_inputPlots -> setPath ( m_inputPlotsPath ) ;
      if ( sc.isFailure() )
      { return Error ( "Unable to set Input Plots path" , sc ) ; }
    }
    //
    if ( validPlots ( m_outputPlotsTool ) )
    {
      m_outputPlots = tool<IPlotTool>( m_outputPlotsTool, this ) ;
      StatusCode sc = m_outputPlots -> setPath ( m_outputPlotsPath ) ;
      if ( sc.isFailure() )
      { return Error ("Unable to set Output Plots path" , sc ) ; }
    }
  }
  //
  m_to_be_updated2 = false ;
  //
  return StatusCode::SUCCESS ;
}
// ==========================================================================
// the most interesting method
// ============================================================================
StatusCode FilterBan::execute ()       // the most interesting method
{
  m_buffer.clear();
  m_accepted.clear();
  m_trks.clear();
  m_cals.clear();
  
  if  ( m_to_be_updated1 )
  {
    StatusCode sc = updateMajor () ;
    if ( sc.isFailure() )
    { return Error ( "Error from updateMajor"  , sc ) ; }    // RETURN
  }
  if  ( m_to_be_updated2 )
  {
    StatusCode sc = updateHistos () ;
    if ( sc.isFailure() )
    { return Error ( "The error from updateHistos" , sc ) ; }    // RETURN
  }
  
  // get the input particles
  const LHCb::Particle::ConstVector& particles = i_particles () ;
  // monitor input (if required)
  if ( monitor() && !m_preMonitorCode.empty() ) { m_preMonitor ( particles ) ; }

  // make plots
  if ( produceHistos () && m_inputPlots )
  {
    StatusCode sc = m_inputPlots -> fillPlots ( particles ) ;
    if ( sc.isFailure () )
    { return Error ( "Error from Input Plots tool", sc ) ; }
  }
  //
  m_accepted.reserve ( particles.size() );
  m_buffer.reserve ( particles.size() );
  m_trks.reserve   ( particles.size() );
  m_cals.reserve   ( particles.size() );
  //
  LHCb::Particle::Range prts;
  for ( unsigned int inLoc = 0; inLoc < m_inBans.size(); ++inLoc ) 
  {
    prts = getIfExists<LHCb::Particle::Range>( m_inBans[inLoc] ); 
    for ( LHCb::Particle::Range::iterator prt = prts.begin(); prt != prts.end(); ++prt ) {
      ban( *prt );
      //printf("Particle with PID: %i \n",pid);
    }
  }

  // if (m_cals.size()){
  //     printf("Before filter function\n");
  //     printf("size of m_cals: %i\n",m_cals.size());
  //   }
  
  // Filter particles!!  - the most important line :-)
  StatusCode sc = filter ( particles , m_accepted, m_buffer ) ;

  // store particles in DaVinciAlgorithm local container
  // this section has been moved into "filter"-method
  // this -> markParticles ( m_accepted ) ;
  // this -> markTrees     ( m_accepted ) ;

  // make the final plots
  if ( produceHistos () && m_outputPlots )
  {
    const StatusCode scc = m_outputPlots -> fillPlots ( i_markedParticles()  ) ;
    if ( scc.isFailure () )
    { return Error ( "Error from Output Plots tool", scc ) ; }
  }

  // monitor output (if required)
  if ( monitor() && !m_postMonitorCode.empty() ) { m_postMonitor ( i_markedParticles() ) ; }

  // make the filter decision
  setFilterPassed ( !i_markedParticles().empty() ) ;

  // some statistics
  counter ( "#passed" ) += i_markedParticles().size();

  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  the major method to filter input particles
 *  @param input    (INPUT) the input  container of particles
 *  @param filtered (OUPUT) the output container of particles
 *  @return Status code
 */
// ============================================================================
StatusCode FilterBan::filter(
  const LHCb::Particle::ConstVector& input    ,
  LHCb::Particle::ConstVector&       filtered ,
  LHCb::Particle::ConstVector& buffer)
{
    
  // if (m_cals.size()){
  //     printf("During filter function\n");
  //     printf("size of m_cals: %i\n",m_cals.size());
  //   }
  

  // Filter particles!!  - the most important line :-)
  std::copy_if ( input.begin () ,
                 input.end   () ,
                 std::back_inserter ( buffer ) , std::cref(m_cut) ) ;

  for (LHCb::Particle::ConstVector::const_iterator part = buffer.begin(); part!=buffer.end(); ++part)
  {
   
    if ( banned(*part) ) {
    //   //printf("Particle with PID: %i was banned and thus filtered \n",pid);
      
      continue;
      
     }
    //printf("Particle with PID: %i was NOT banned and is in TES :)\n",pid);
    filtered.emplace_back(*part);    
  }
  


  //
  // mark & store filtered particles in DaVinciAlgorithm local container
  markParticles ( filtered ) ;
  //
  // some countings
  StatEntity& cnt = counter ( "efficiency" ) ;
  //
  const size_t size1 = filtered  . size () ;
  const size_t size2 = input     . size () ;
  //
  for ( size_t i1 = 0     ; size1 > i1 ; ++i1 ) { cnt += true  ; }
  for ( size_t i2 = size1 ; size2 > i2 ; ++i2 ) { cnt += false ; }
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
// save (clone if needed) selected particles in TES
// ============================================================================
template <class PARTICLES, class VERTICES, class CLONER>
StatusCode FilterBan::_save ( ) const
{
  //
  PARTICLES* p_tes = new PARTICLES () ;
  VERTICES*  v_tes = new VERTICES  () ;
  //
  put ( p_tes ,    particleOutputLocation () ) ;
  put ( v_tes , decayVertexOutputLocation () ) ;
  //
  CLONER cloner ;
  //
  for ( const auto& p : i_markedParticles() )
  {
    if ( !p ) { continue ; } // CONTINUE

    // clone if needeed
    LHCb::Particle* p_cloned = cloner ( p ) ;
    p_tes -> insert ( p_cloned ) ;
    //
    this  -> cloneP2PVRelation ( p , p_cloned ) ;
    //
    const LHCb::Vertex* v = p->endVertex() ;
    if ( v )
    {
      LHCb::Vertex* v_cloned = cloner ( v ) ;
      p_cloned -> setEndVertex ( v_cloned ) ;
      v_tes    -> insert( v_cloned ) ;
    }
  }
  //
  // check that the decay trees are fully in the TES
  //
  for ( const auto& ip : *p_tes )
  {
    if (! DaVinci::Utils::decayTreeInTES( ip ) )
    {
      return Error ( "FilterBan::_save Element of saved decay tree not in TES. Likely memory leak!");
    }
  }
  //
  return  i_markedParticles().size() != p_tes->size() ?
          StatusCode::FAILURE : StatusCode::SUCCESS   ;
}
// ============================================================================
namespace
{
  // ==========================================================================
  /// the trivial cloner
  struct _Cloner
  {
    template <class T>
    T* operator() ( const T* a ) const { return a->clone() ; }
  };
  /// helper structure (fake cloner)
  struct _Caster
  {
    template <class T>
    T* operator() ( const T* a ) const { return const_cast<T*> ( a ) ; }
  };
  // ==========================================================================
} // end of anonymous namespace
// ============================================================================
StatusCode FilterBan::_saveInTES ()
{
  if (msgLevel(MSG::VERBOSE))
  {
    verbose() << "FilterBan::_saveInTES " << i_markedParticles().size() << " Particles" << endmsg;
  }
  return m_cloneFilteredParticles ?
    this -> _save<LHCb::Particle::Container,LHCb::Vertex::Container,_Cloner> ():
    this -> _save<LHCb::Particle::Selection,LHCb::Vertex::Selection,_Caster> ();
}
// ============================================================================
void FilterBan::cloneP2PVRelation
( const LHCb::Particle*   particle ,
  const LHCb::Particle*   clone       ) const
{
  const LHCb::VertexBase* bestPV = getStoredBestPV(particle);
  if ( bestPV ) { this->relate ( clone , bestPV ) ; }

}
// ============================================================================
void FilterBan::writeEmptyKeyedContainers() const
{
  LHCb::Particle::Container* container = new LHCb::Particle::Container();
  put(container, particleOutputLocation());
  return;
}
// ============================================================================
void FilterBan::writeEmptySharedContainers() const
{
  LHCb::Particle::Selection* container = new LHCb::Particle::Selection();
  put(container, particleOutputLocation());
  return;
}
// ============================================================================
// decode the code
// ============================================================================
StatusCode FilterBan::decodeCode ()
{
  // locate the factory
  LoKi::IHybridFactory* factory_ = tool<LoKi::IHybridFactory> ( factory() , this ) ;
  //
  // use the factory
  StatusCode sc = factory_ -> get ( code() , m_cut , preambulo() ) ;
  if ( sc.isFailure() )
  { return Error ( "Error from LoKi/Bender 'hybrid' factory for Code='"
                   + code() + "'" , sc )  ; }
  //
  release ( factory_ ) ;
  //
  return sc ;
}

// Ban a Particle.
void FilterBan::ban(const LHCb::Particle *prt) {
  LHCb::Particle::ConstVector dtrs;
  LoKi::Extract::getParticles(prt, back_inserter(dtrs), LoKi::Cuts::HASPROTO && LoKi::Cuts::ISBASIC);
  for (LHCb::Particle::ConstVector::iterator dtr = dtrs.begin(); dtr != dtrs.end();
       ++dtr) {
    const LHCb::ProtoParticle *pro = (*dtr)->proto();
    if (pro->track()) m_trks.insert(pro->track());
    else if (pro->calo().size() > 0) {
      const LHCb::CaloHypo* cal = pro->calo()[0].data();
      m_cals.insert(cal);
    }
  }
}

// Check if a Particle is banned.
bool FilterBan::banned(const LHCb::Particle *prt) {
  if (m_trks.size() == 0 && m_cals.size() == 0) return false;
  LHCb::Particle::ConstVector dtrs;
  LoKi::Extract::getParticles(prt, back_inserter(dtrs), LoKi::Cuts::HASPROTO
			      && LoKi::Cuts::ISBASIC);
  for (LHCb::Particle::ConstVector::iterator dtr = dtrs.begin(); dtr != dtrs.end();
       ++dtr) {
    const LHCb::ProtoParticle *pro = (*dtr)->proto();
    if (pro->track()) return m_trks.find(pro->track()) != m_trks.end();
    else if (pro->calo().size() > 0) {
      const LHCb::CaloHypo* cal = pro->calo()[0].data();
      return (m_cals.find(cal) != m_cals.end());
      
        }
  } return false;
}




// ============================================================================
/// the factory
DECLARE_ALGORITHM_FACTORY(FilterBan)
// ============================================================================
// The END
// ============================================================================
