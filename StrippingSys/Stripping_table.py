import json
import numpy as np
from uncertainties import ufloat

jsonfile = open("stripping_PID_efficiencies.json", "r")
pid_dict = json.load(jsonfile)
jsonfile.close()

mass_label = {
	"49100040": "ALP 5 GeV",
	"49100041": "ALP 6 GeV",
	"49100042": "ALP 7 GeV",
	"49100043": "ALP 8 GeV",
	"49100044": "ALP 9 GeV",
	"49100045": "ALP 10 GeV",
	"49100046": "ALP 12 GeV",
	"49100047": "ALP 14 GeV",
	"49100048": "ALP 15 GeV",
	"49100049": "ALP 17 GeV",
	"49100050": "ALP 19 GeV",
	"49100051": "ALP 20 GeV",
	"Bsgg" : "$B^0_s\\to\gamma\gamma$"
	}

outfile = open("pid_systematic.tex", "w")
print("\\begin{tabular}[lccc]",file=outfile)
print("\t\\toprule",file=outfile)
print("\tsignal & true $\\varepsilon(\gamma)$ & $\\varepsilon_{\\text{rew}}(\gamma)$ MC & $\\varepsilon_{\\text{rew}}(\gamma)$ Data & Total\\\\", file=outfile)
print("\t\midrule",file=outfile)
for key in mass_label:
	print("\t",mass_label[key],"&$",ufloat(pid_dict[key]["van_ALP"][0],pid_dict[key]["van_ALP"][1]),"$&$",ufloat(pid_dict[key]["rew_eta2mmg_MC"][0],pid_dict[key]["rew_eta2mmg_MC"][1]),"$&$",ufloat(pid_dict[key]["rew_eta2mmg_Data"][0],pid_dict[key]["rew_eta2mmg_Data"][1]),"$&$",ufloat(pid_dict[key]["total_diphoton"][0],np.sqrt(pid_dict[key]["total_diphoton"][1]**2+pid_dict[key]["total_diphoton"][2]**2)),"$\\\\",file=outfile)

print("\t","\\bottomrule",file=outfile)
print("\end{tabular}",file=outfile)
outfile.close()
