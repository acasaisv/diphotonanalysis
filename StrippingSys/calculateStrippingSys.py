from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTsHlt2,etasHlt2,spdsHlt2,get_error,get_error_w,pack_eff,get_ALPsdf,sim10_masses_map,bins
import pickle
from uncertainties import ufloat
pTs = pTsHlt2
etas = etasHlt2
spds = spdsHlt2

def convolute_eff(nspdseff,nspdserr,m,df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs):
    eff_gamma=0
    erreff_gamma2=0
    effMC_gamma=0
    erreffMC_gamma2=0
    eff_gamma0=0
    erreff_gamma02=0
    effMC_gamma0=0
    erreffMC_gamma02=0
    
    sum_fractions = 0

    eff_bs =0
    erreff_bs2=0
    
    #den = df.query('gamma_PT >= 3000')['weights']
    #den0 = df.query('gamma0_PT >= 3000')['weights']
    df.query('gamma0_PT >= 3000 and gamma_PT >=3000',inplace=True)
    den = den0 = denbs = df.query('gamma0_PT >= 3000 and gamma_PT >=3000')['weights']
    
    total = 0
    for ipt in range(len(pTs)-1):
        pT = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                
                df_ =  df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =  df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                
                num = df_['weights']
                fraction = num.sum()/den.sum()
                total +=fraction
                err_fraction = get_error_w(num,den)

                eff_gamma += fraction*efficiencies[pT][eta][spd]
                erreff_gamma2 += err_fraction**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction**2

                effMC_gamma += fraction*efficienciesMC[pT][eta][spd]
                print(f"Eff:{fraction*efficiencies[pT][eta][spd]}. Erreff:{np.sqrt(err_fraction**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction**2)} pt: {pT},eta:{eta},nsdps:{spd}")
            
                erreffMC_gamma2 += err_fraction**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction**2
                
                num0 = df0_['weights']
                fraction0 = num0.sum()/den0.sum()
                err_fraction0 = get_error_w(num0,den0)

                eff_gamma0 += fraction0*efficiencies[pT][eta][spd]
                erreff_gamma02 += err_fraction0**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction0**2

                effMC_gamma0 += fraction0*efficienciesMC[pT][eta][spd]
                erreffMC_gamma02 += err_fraction0**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction0**2


    print("Fraction total",total)          
    cutl0 = '(gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1)'
    cutl00 = '(gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1)'
    cutbs = f'{cutl0} and {cutl00}'
    cutkin =  '(gamma_PT >= 3000)'
    cutkin0 = '(gamma0_PT >= 3000)'
    cutkin = cutkin0 = cutkinbs = '(gamma0_PT >= 3000 and gamma_PT >=3000)'


    num_w = df.query("{0} & {1} ".format(cutl0,cutkin))['weights']
    den_w = df.query(cutkin)['weights']
    l0tos =  num_w.sum()/den_w.sum()
    errl0tos = get_error_w(num_w,den_w)

    num0_w = df.query("{0} & {1} ".format(cutl00,cutkin0))['weights']
    den0_w = df.query(cutkin0)['weights']
    l0tos0 =  num0_w.sum()/den0_w.sum()
    errl0tos0 = get_error_w(num0_w,den0_w)
    
    numbs_w = df.query("{0} & {1} ".format(cutbs,cutkinbs))['weights']
    denbs_w = df.query(cutkinbs)['weights']
    l0tosbs =  numbs_w.sum()/denbs_w.sum()
    errl0tosbs = get_error_w(numbs_w,denbs_w)


    bsmcefferror = np.sqrt(erreffMC_gamma02 +erreffMC_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)
    bsefferror = np.sqrt(erreff_gamma02 +erreff_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)

    effbsmc =  (effMC_gamma0+effMC_gamma)*l0tosbs/(l0tos+l0tos0)
    effbs = (eff_gamma0+eff_gamma)*l0tosbs/(l0tos+l0tos0)
    
    #effbsmc =  (effMC_gamma0+effMC_gamma) - effMC_gamma0*effMC_gamma
    #effbs = (eff_gamma0+eff_gamma) - effMC_gamma0+effMC_gamma

    espds = ufloat(nspdseff,nspdserr)
    emcg0 = ufloat(effMC_gamma0,np.sqrt(erreffMC_gamma02))
    emcg1 = ufloat(effMC_gamma,np.sqrt(erreffMC_gamma2))
    edatag0 = ufloat(eff_gamma0,np.sqrt(erreff_gamma02))
    edatag1 = ufloat(eff_gamma,np.sqrt(erreff_gamma2))
    el0tos0 = ufloat(l0tos0,errl0tos0)
    el0tos1 = ufloat(l0tos,errl0tos)
    el0tosbs = ufloat(l0tosbs,errl0tosbs)
    print(el0tosbs)
    print(emcg0,emcg1)
    print(emcg0*emcg1)
    print(edatag0*edatag1)

    bsmc = (emcg0*emcg1)
    bsdata = (edatag0*edatag1)

    fulll0mc = bsmc*espds
    fulll0data = bsdata*espds
    
    effarray = np.array((   m,
                            #pack_eff(100*nspdseff,100*nspdserr),
                            pack_eff(100.*l0tos,100.*errl0tos),
                            pack_eff(100.*l0tos0,100.*errl0tos0),
                            pack_eff(100.*eff_gamma,100.*np.sqrt(erreff_gamma2)),
                            pack_eff(100.*eff_gamma0,100.*np.sqrt(erreff_gamma02)),
                            pack_eff(100.*effMC_gamma,100.*np.sqrt(erreffMC_gamma2)),
                            pack_eff(100.*effMC_gamma0,100.*np.sqrt(erreffMC_gamma02)),
                            pack_eff(100.*l0tosbs,100.*errl0tosbs),
                            pack_eff(100.*(el0tos0*el0tos1).n,100.*(el0tos0*el0tos1).s),
                            pack_eff(100.*bsdata.nominal_value,100.*bsdata.std_dev),
                            pack_eff(100.*bsmc.nominal_value,100.*bsmc.std_dev),
                            pack_eff(100.*fulll0data.nominal_value,100.*fulll0data.std_dev),
                            pack_eff(100.*fulll0mc.nominal_value,100.*fulll0mc.std_dev),
                            pack_eff(100.*(espds*el0tosbs).n,100.*(espds*el0tosbs).s)
                            ))
    if dfeffs.shape == (0,0):
        pass
    else:
       dfeffs = dfeffs.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeffs)),ignore_index=True) 
    
    return fulll0data,dfeffs
    
def reweight_alps_df(dfalps,dfeta):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = [#'eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfalps['nSPDHits'], target=1.7*dfalps['nSPDHits'], target_weight=dfalps['weights'])
    dfalps['weights']= reweighter.predict_weights(dfalps['nSPDHits'])
    return dfalps

def plots(alps,etammg,etammgMC):
    plt.clf()
    fig,ax = plt.subplots(1,1)
    cut  = 'gamma_PT > 3000 and gamma_PT < 10000 and gamma_ETA >2 and gamma_ETA <3 and nSPDHits <450'
    cut = 'gamma_PT >0'
    myalps = alps.query(cut)
    myetammgmc = etammgMC.query(cut)
    myetammg = etammg.query(cut)
    ax.hist(myalps['gamma_CL'],bins=50,weights=myalps['weights'],color='red',density=True,histtype='step',label='alps')
    ax.hist(myetammg['gamma_PP_IsNotH'],bins=50,weights=myetammg['sweights'],color='blue',density=True,histtype='step',label='etammgdata')
    ax.hist(myetammgmc['gamma_PP_IsNotH'],bins=50,weights=myetammgmc['sweights'],color='green',density=True,histtype='step',label='etammgmc')
    plt.legend(loc='upper left')
    fig.savefig('isnoth.pdf')
    #print('Fraction sum = {}'.format(sum_fractions))
if __name__ == '__main__':
    import pickle
    with open('L0Efficiencies.p','rb') as file:
        efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    # masses = {40:'5 GeV',
    #           42:'7 GeV',
    #           44:'9 GeV',
    #           46:'11 GeV',
    #           47:'13 GeV',
    #           50:'15 GeV',
    #           48:'4 GeV',
    #           'bs':'Bs0 (5 GeV)'}
    masses = sim10_masses_map
    plot = []
    plot0 = []
    bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'
    mybin = bins['low']
    pts,etas,spds = mybin['pts'],mybin['etas'],mybin['spds']
    dic = { 'Mass':[],
            #'nSPDHits':[] ,
            '$\\epsilon(\\gamma)$':[],
            '$\\epsilon(\\gamma^0)$':[],
            '$E_{\\textrm{Data}} (\\gamma)$':[],
            '$E_{\\textrm{Data}} (\\gamma^0)$':[],
            '$E_{\\textrm{MC}} (\\gamma)$':[],
            '$E_{\\textrm{MC}} (\\gamma^0)$':[],
            '$\epsilon (\\aa)$':[],
            '$\epsilon (\\gamma) \\times \\epsilon(\\gamma^  $':[],
            '$E_{\\textrm{Data}} (\\aa)$':[],
            '$E_{\\textrm{MC}} (\\aa)$}':[],
            '$E_{\\textrm{Data}} (\\textrm{Stripping})$':[],
            '$E_{\\textrm{MC}} (\\textrm{Stripping})$':[],
            '$\epsilon (\\textrm{Stripping})$':[],
            }
    dfeffs = pd.DataFrame(dic)
    #for i in sim10_masses_map:
    #for i in [40]:
    #for i in ['/scratch46/adrian.casais/49100040_1000-1009_Sim10a-priv.root']:
    #for i in [50]:
              #,'bs']:
    for i in [41,50]:
        print(10*"**")
        print(masses[i])
        bs = False
        if i=='bs':
            bs=True
        
        if i=='sim10' or i==43:
            continue
        #df = get_df(i,bs,background=False)
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
        df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root',bs,background=False,sample=int(1000e4),extravars=['gamma_CL',
        'gamma_PP_CaloNeutralHcal2Ecal',
        'gamma0_CL',
        'gamma0_PP_CaloNeutralHcal2Ecal' 
        ],stripping=False)
         
        stripping_cut = 'B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and B_s0_M > 4800 and B_s0_M < 20000'
        nSPDhits_den = df['weights']
        nSPDhits_num = df.query(stripping_cut)['weights']
        eff = nSPDhits_num.sum()/nSPDhits_den.sum()
        err_eff = get_error_w(nSPDhits_num,nSPDhits_den)
        print('kinematic eff= {0:.2f} +- {1:.2f}'.format(100.*eff,100.*err_eff))
        #dfetamc =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
        #dfeta =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
        dfeta =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalDataStripping.h5',key='df')
        dfetamc =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMCStripping.h5',key='df')
        plots(df,dfeta,dfetamc)

        _,dfeffs=convolute_eff(eff,err_eff,masses[i],df.query(stripping_cut),pts,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs)
    print(dfeffs.to_latex(index=False,escape=False))