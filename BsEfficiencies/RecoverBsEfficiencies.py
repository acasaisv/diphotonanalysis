from ROOT import *

import os
from get_binomial import *

def get_eff(a,b,opt=0,prec=3):
    if opt: a1,b1 = a,b
    else: a1,b1 = get_binomial(a,b)
    if prec==2:
        v,ev = map(lambda x: round(x*100,2),[a1,b1])
        return "%.2f +- %.2f" %(v,ev)
    v,ev = map(lambda x: round(x*100,3),[a1,b1])
    return "%.3f +- %.3f" %(v,ev)

root="root://eoslhcb.cern.ch:/"
os.environ["EHOME"]="/eos/lhcb/user/j/jcidvida"
aa="&&"
fBs=TFile.Open(root+os.environ["EHOME"]+"/ntuples/MC2016_Sim09c_Priv_MagUp_Bs2gg_withConv.root")
categories = ["None","LL","DD","Double"]
t = dict()
for cat in categories:
    t[cat]=fBs.Get("DTTBs2gg_"+cat+"/DecayTree")
totalBs = 442234.


#MCTRUTH MATCHING SELECTION
mcsels = {}
mcsels["Double"] = "abs(eplusg1_MC_GD_MOTHER_ID)==531&&abs(eplusg21_MC_GD_MOTHER_ID)==531&&eplusg1_MC_MOTHER_ID==22&&eminusg1_MC_MOTHER_KEY==eplusg1_MC_MOTHER_KEY&&eplusg21_MC_MOTHER_ID==22&&eminusg21_MC_MOTHER_KEY==eplusg21_MC_MOTHER_KEY&&eplusg1_TRUEID==-11&&eminusg1_TRUEID==11&&eplusg21_TRUEID==-11&&eminusg21_TRUEID==11"
mcsels["LL"] = "abs(eplus_MC_GD_MOTHER_ID)==531&&eplus_MC_MOTHER_ID==22&&eminus_MC_MOTHER_KEY==eplus_MC_MOTHER_KEY&&eplus_TRUEID==-11&&eminus_TRUEID==11&&gamma_TRUEID==22&&abs(gamma_MC_MOTHER_ID)==531"
mcsels["DD"]=mcsels["LL"]
mcsels["None"]="gamma1_TRUEID==22&&abs(gamma1_MC_MOTHER_ID)==531&&gamma2_TRUEID==22&&abs(gamma2_MC_MOTHER_ID)==531"
#BASE NOTECUTS
basecuts= {}
gamma_cuts = "(gamma{0}_P > 6000 && gamma{0}_PT > 3000) "
gamma_cl = '(gamma2_CL > 0.3 && gamma1_CL > 0.3)'
conv_gamma_cuts = "(gamma{0}_M < 60  && gamma{0}_PT > 2000 && gamma{0}_IPCHI2_OWNPV > {1}) "
gammapt_cuts ="((gamma{0}_PT + gamma{1}_PT)>{2})"
electron_cuts = "(e{0}_M < 60 && e{0}_PT > 2000 && e{0}_IPCHI2_OWNPV > {1})"
cand_pt = "A1_PT > {0}"
basecuts["None"]=gamma_cuts.format(1)+aa+gamma_cuts.format(2)+aa+gammapt_cuts.format(1,2,6500)+aa+cand_pt.format(3000)+aa+gamma_cl

basecuts["LL"]=gamma_cuts.format("") + aa + conv_gamma_cuts.format("Conv",4) + aa + electron_cuts.format("minus",0) + aa + gammapt_cuts.format("Conv","",5500) + aa + cand_pt.format(3000)
basecuts["DD"]=gamma_cuts.format("") + aa + conv_gamma_cuts.format("Conv",0) + aa + gammapt_cuts.format("Conv","",5500) + aa + cand_pt.format(3000)
basecuts["Double"]=conv_gamma_cuts.format(1,1)+aa+conv_gamma_cuts.format(2,1)+aa+ gammapt_cuts.format("1","2",5000) + aa + "A1_PT > 3000 && A1_OWNPV_CHI2 < 20"
#TRIGGER REQUIREMENTS
l0tos = '(A1_L0ElectronDecision_TOS || A1_L0PhotonDecision_TOS)'
hlt1 = {"None": l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS',
        "DD":   l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS',
        "LL":   l0tos + aa + '(A1_Hlt1B2GammaGammaDecision_TOS || A1_Hlt1TrackMVADecision_TOS)',
        "Double": l0tos + aa + '(A1_Hlt1B2GammaGammaDecision_TOS || A1_Hlt1TrackMVADecision_TOS)'}

trigger = {"None": l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDecision_TOS'+aa+hlt1["None"],
           "DD":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDDDecision_TOS'+aa+hlt1["DD"],
           "LL":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaLLDecision_TOS'+aa+hlt1["LL"],
           "Double":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDoubleDecision_TOS'+aa+hlt1["Double"],
                      }


MCTRUTH=1
MCTRUTH_str="MCTRUTH MATCHING ON DENOMINTAOR: {0}"
if not MCTRUTH:
    print MCTRUTH_str.format("OFF")
    for key in mcsels:
        mcsels[key]="1"
else:
    print MCTRUTH_str.format("ON")
#paper Hlt1 EFF
num=0
den=0
denl0tos=0
denl0tosfiducial=0
for sel in categories:
    print "=============\n"+sel+"\n============="
    print "*************\nSTRIP EFF"
    print get_eff(t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]),totalBs)
    print "*************\nL0TOS EFF"
    print get_eff(t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+l0tos),t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]))
    print "*************\nHLT1TOS EFF"
    print get_eff(t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+hlt1[sel]),t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+l0tos))
    print ""
    #if sel!="Double":
    #num+=t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+hlt1[sel])
    
    denl0tos+=t[sel].GetEntries(mcsels[sel]+aa+l0tos)
    denl0tosfiducial+=t[sel].GetEntries(mcsels[sel]+aa+l0tos+aa+basecuts[sel])
    num+=t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+hlt1[sel])
    den+=t[sel].GetEntries(mcsels[sel]+aa+basecuts[sel]+aa+l0tos)

print "PAPER NUMBER: ONLY L0TOS"
print get_eff(t["None"].GetEntries(hlt1["None"]),t['None'].GetEntries(l0tos))
print "PAPER NUMBER: L0TOS && FIDUCIAL CUTS "
print get_eff(t["None"].GetEntries(mcsels["None"]+aa+basecuts["None"]+aa+hlt1["None"]),denl0tosfiducial)
print "NOTE NUMBER"
print 100.*num/den
