from ROOT import *
import os
os.environ["EHOME"]="/eos/lhcb/user/j/jcidvida"
aa="&&"
from get_binomial import *


gROOT.ProcessLine(".x /afs/cern.ch/lhcb/software/releases/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

mass = {"40100200": "A1_M>9000 && A1_M <12000"
        ,"40100204":"A1_M>3500 && A1_M<4900"
        ,"13100212":"A1_M>4500 && A1_M <5900"}


tots = {"40100204":39200.,
        #"13100212":42000.,
        "13100212":442234,
        "40100200":56350.}

mcsels = {"13100212":{}}
mcsels["13100212"]["Double"] = "abs(eplusg1_MC_GD_MOTHER_ID)==531&&abs(eplusg21_MC_GD_MOTHER_ID)==531&&eplusg1_MC_MOTHER_ID==22&&eminusg1_MC_MOTHER_KEY==eplusg1_MC_MOTHER_KEY&&eplusg21_MC_MOTHER_ID==22&&eminusg21_MC_MOTHER_KEY==eplusg21_MC_MOTHER_KEY&&eplusg1_TRUEID==-11&&eminusg1_TRUEID==11&&eplusg21_TRUEID==-11&&eminusg21_TRUEID==11"
mcsels["13100212"]["LL"] = "abs(eplus_MC_GD_MOTHER_ID)==531&&eplus_MC_MOTHER_ID==22&&eminus_MC_MOTHER_KEY==eplus_MC_MOTHER_KEY&&eplus_TRUEID==-11&&eminus_TRUEID==11&&gamma_TRUEID==22&&abs(gamma_MC_MOTHER_ID)==531"
mcsels["13100212"]["DD"]=mcsels["13100212"]["LL"]
mcsels["13100212"]["None"]="gamma1_TRUEID==22&&abs(gamma1_MC_MOTHER_ID)==531&&gamma2_TRUEID==22&&abs(gamma2_MC_MOTHER_ID)==531"
for key in ["40100204","40100200"]:
    mcsels[key]={}
    for sel in mcsels["13100212"]:
        mcsels[key][sel]=mcsels["13100212"][sel].replace("531","36")
                  

strip = "1"
sels = ["None","DD","LL","Double"]
f1,t1,h1 = {},{},{}
for key in mass:
    #if key=="13100212": f1[key] = TFile(os.environ["EHOME"]+"/ntuples/Bsgg_MC2016_Priv_13100212_withConv.root")
    if key=="13100212": f1[key] = TFile(os.environ["EHOME"]+"/ntuples/MC2016_Sim09c_Priv_MagUp_Bs2gg_withConv.root")
    else: f1[key] = TFile(os.environ["EHOME"]+"/ntuples/A1gg_MC2016_Priv_"+key+"_withConv.root")
    t1[key]={}
    for sel in sels:
        t1[key][sel]=f1[key].Get("DTTBs2gg_"+sel+"/DecayTree")

l0tos = '(A1_L0ElectronDecision_TOS || A1_L0PhotonDecision_TOS)'
#trigger = {"None": l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS&&A1_Hlt2RadiativeB2GammaGammaDecision_TOS',
#           "DD":   l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS&&A1_Hlt2RadiativeB2GammaGammaDDDecision_TOS',
#           "LL":   l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS&&A1_Hlt2RadiativeB2GammaGammaLLDecision_TOS',
#           "Double":   l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS&&A1_Hlt2RadiativeB2GammaGammaDoubleDecision_TOS',
#           }

hlt1 = {"None": l0tos + aa + 'A1_Hlt1B2GammaGammaDecision_TOS',
        "DD":   l0tos + aa + 'A1_Hlt1Phys_TOS',
        "LL":   l0tos + aa + 'A1_Hlt1Phys_TOS',
        "Double":   l0tos + aa + 'A1_Hlt1Phys_TOS'}


trigger = {"None": l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDecision_TOS'+aa+hlt1["None"],
           "DD":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDDDecision_TOS'+aa+hlt1["DD"],
           "LL":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaLLDecision_TOS'+aa+hlt1["LL"],
           "Double":   l0tos + aa + 'A1_Hlt2RadiativeB2GammaGammaDoubleDecision_TOS'+aa+hlt1["Double"],
           }

def get_eff(a,b,opt=0,prec=3):
    if opt: a1,b1 = a,b
    else: a1,b1 = get_binomial(a,b)
    if prec==2:
        v,ev = map(lambda x: round(x*100,2),[a1,b1])
        return "%.2f +- %.2f" %(v,ev)
    v,ev = map(lambda x: round(x*100,3),[a1,b1])
    return "%.3f +- %.3f" %(v,ev)
eff_print = {}
print "\n"*2
for sel in sels:
 #myk = ["40100204","13100212","40100200"]
 myk = ["13100212"]
 print "\n",sel
 eff_print[sel] = {}
 ## strip eff
 print "*************\nSTRIP EFF"
 for key in myk:
     eff_print[sel]["strip"]=get_eff(t1[key][sel].GetEntries(strip+aa+mcsels[key][sel]+aa+mass[key]),tots[key])
     print key,eff_print[sel]["strip"]
 ## l0 eff
 print "*************\nL0 EFF"
 for key in myk:
     eff_print[sel]["l0"]=get_eff(t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(strip+aa+mcsels[key][sel]+aa+mass[key]))
     print key,eff_print[sel]["l0"]
 ## hlt eff
 print "*************\nHLT EFF"
 for key in myk[:2]:
     eff_print[sel]["hlt"]=get_eff(t1[key][sel].GetEntries(trigger[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key]))
     print key,eff_print[sel]["hlt"]

 ## hlt1 eff
 print "*************\nHLT1 EFF"
 for key in myk[:2]:
     eff_print[sel]["hlt1"]=get_eff(t1[key][sel].GetEntries(hlt1[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key]))
     print key,eff_print[sel]["hlt1"]

 ## hlt2 eff
 print "*************\nHLT2 EFF"
 for key in myk[:2]:
     eff_print[sel]["hlt2"]=get_eff(t1[key][sel].GetEntries(trigger[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(hlt1[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]))
     print key,eff_print[sel]["hlt2"]


 key = "40100204"
 hlt,ehlt = get_binomial(t1[key][sel].GetEntries(trigger[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key]))

 ## trigg eff
 print "*************\nTRIG EFF"
 for key in myk:
     if key == "40100200":
         l0e = t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key])/float(t1[key][sel].GetEntries(strip+aa+mcsels[key][sel]+aa+mass[key]))
         eff_print[sel]["trig"] = get_eff(l0e*hlt,0,1)
     else:
         eff_print[sel]["trig"] = get_eff(t1[key][sel].GetEntries(trigger[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),t1[key][sel].GetEntries(strip+aa+mcsels[key][sel]+aa+mass[key]))
     print key,eff_print[sel]["trig"]


 ## tot eff
 print "*************\nTOT EFF"
 for key in myk:
     if key == "40100200":
         l0e = t1[key][sel].GetEntries(l0tos+aa+strip+aa+mcsels[key][sel]+aa+mass[key])/tots[key]
         eff_print[sel]["tot"] = get_eff(l0e*hlt,0,1)
     else:
         eff_print[sel]["tot"] = get_eff(t1[key][sel].GetEntries(trigger[sel]+aa+strip+aa+mcsels[key][sel]+aa+mass[key]),tots[key])
     print key,eff_print[sel]["tot"]

cn = TCanvas("m","m");cn.SetWindowSize(1200,900);cn.Divide(2,2)
i1=1
key = "13100212"
vals,hmass,lat = {},{},{}
for sel in sels:
    cn.cd(i1);i1+=1
    hmass[sel] = TH1F(sel,"",14,5000,5700)
    hmass[sel].GetXaxis().SetTitle("m_{#gamma#gamma} (MeV/c^{2})")
    hmass[sel].GetYaxis().SetTitle("Number of entries / 50 MeV/c^{2}")
    t1[key][sel].Project(sel,"A1_MM",trigger[sel]+aa+mcsels[key][sel]+aa+"A1_MM>5e3&&A1_MM<5.7e3")
    hmass[sel].Draw()
    lat[sel] = TLatex()
    lat[sel].DrawLatexNDC(.7,.7,sel)

    vals[sel] = []
    for i in range(1,15):
        vals[sel].append([hmass[sel].GetBinLowEdge(i),hmass[sel].GetBinLowEdge(i+1),
                          hmass[sel].GetBinContent(i)/float(hmass[sel].GetEntries())])
    
pickleSave(vals,"histo_norm_bins_bs2gg.pickle")
pickleSave(eff_print,"effs_print_bs2gg.pickle")
##None
## 13100212 1.280 +- 0.017
##DD
## 13100212 0.089 +- 0.004
##LL
## 13100212 0.135 +- 0.006
#Double
#13100212 0.012 +- 0.002


# None
# *************
# STRIP EFF
# 40100204 13.747 +- 0.174
# 13100212 18.809 +- 0.059
# 40100200 9.553 +- 0.124
# *************
# L0 EFF
# 40100204 87.846 +- 0.445
# 13100212 87.681 +- 0.114
# 40100200 90.006 +- 0.409
# *************
# HLT1 EFF
# 40100204 13.414 +- 0.495
# 13100212 15.069 +- 0.132
# *************
# HLT2 EFF
# 40100204 48.819 +- 1.984
# 13100212 51.510 +- 0.477
# *************
# HLT EFF
# 40100204 6.548 +- 0.360
# 13100212 7.762 +- 0.099
# *************
# TRIG EFF
# 40100204 5.752 +- 0.317
# 13100212 6.806 +- 0.087
# 40100200 5.894 +- 0.000
# *************
# TOT EFF
# 40100204 0.791 +- 0.045
# 13100212 1.280 +- 0.017
# 40100200 0.563 +- 0.000

# DD
# *************
# STRIP EFF
# 40100204 1.941 +- 0.070
# 13100212 1.533 +- 0.018
# 40100200 2.012 +- 0.059
# *************
# L0 EFF
# 40100204 83.311 +- 1.352
# 13100212 83.292 +- 0.453
# 40100200 80.776 +- 1.170
# *************
# HLT1 EFF
# 40100204 11.041 +- 1.245
# 13100212 10.021 +- 0.400
# *************
# HLT2 EFF
# 40100204 0.000 +- 0.000
# 13100212 69.435 +- 1.936
# *************
# HLT EFF
# 40100204 0.000 +- 0.000
# 13100212 6.958 +- 0.339
# *************
# TRIG EFF
# 40100204 0.000 +- 0.000
# 13100212 5.796 +- 0.284
# 40100200 0.000 +- 0.000
# *************
# TOT EFF
# 40100204 0.000 +- 0.000
# 13100212 0.089 +- 0.004
# 40100200 0.000 +- 0.000

# LL
# *************
# STRIP EFF
# 40100204 0.610 +- 0.039
# 13100212 0.699 +- 0.013
# 40100200 0.376 +- 0.026
# *************
# L0 EFF
# 40100204 93.305 +- 1.617
# 13100212 80.104 +- 0.718
# 40100200 97.170 +- 1.139
# *************
# HLT1 EFF
# 40100204 98.206 +- 0.889
# 13100212 78.675 +- 0.823
# *************
# HLT2 EFF
# 40100204 74.429 +- 2.948
# 13100212 30.749 +- 1.046
# *************
# HLT EFF
# 40100204 73.094 +- 2.970
# 13100212 24.192 +- 0.861
# *************
# TRIG EFF
# 40100204 68.201 +- 3.012
# 13100212 19.379 +- 0.711
# 40100200 71.025 +- 0.000
# *************
# TOT EFF
# 40100204 0.416 +- 0.033
# 13100212 0.135 +- 0.006
# 40100200 0.267 +- 0.000

# Double
# *************
# STRIP EFF
# 40100204 0.296 +- 0.027
# 13100212 0.128 +- 0.005
# 40100200 0.144 +- 0.016
# *************
# L0 EFF
# 40100204 100.000 +- 0.000
# 13100212 72.134 +- 1.883
# 40100200 100.000 +- 0.000
# *************
# HLT1 EFF
# 40100204 44.828 +- 4.617
# 13100212 41.076 +- 2.433
# *************
# HLT2 EFF
# 40100204 0.000 +- 0.000
# 13100212 30.357 +- 3.547
# *************
# HLT EFF
# 40100204 0.000 +- 0.000
# 13100212 12.469 +- 1.634
# *************
# TRIG EFF
# 40100204 0.000 +- 0.000
# 13100212 8.995 +- 1.202
# 40100200 0.000 +- 0.000
# *************
# TOT EFF
# 40100204 0.000 +- 0.000
# 13100212 0.012 +- 0.002
# 40100200 0.000 +- 0.000



DRAW_OLD = False
if DRAW_OLD:
    c01,c11,c2 = {},{},{}
    for sel in sels:
        h1[sel] = {}
        c01[sel] = TCanvas("comp1"+"_"+sel,"comp1"+"_"+sel);c01[sel].SetWindowSize(1200,900)
        for key in ["40100204","13100212"]:
            h1[sel][key] = TH1F("comp_"+key+"_"+sel,"",33,3500,6800)
            t1[key][sel].Project("comp_"+key+"_"+sel,"A1_M",strip+aa+mcsels[key][sel]+aa+trigger[sel])
            if key=="40100204":
                h1[sel][key].GetXaxis().SetTitle("m_{#gamma#gamma} (MeV/c^{2})")
                h1[sel][key].GetYaxis().SetTitle("Number of entries / 100 MeV/c^{2}")
                h1[sel][key].SetLineColor(2);h1[sel][key].DrawNormalized()
            else: h1[sel][key].DrawNormalized("same")

    c11[sel] = TCanvas("comp2"+"_"+sel,"comp2"+"_"+sel);c11[sel].SetWindowSize(1200,900)
    for key in ["40100204","13100212"]:
        h1[sel][key] = TH1F("comp_"+key+"_"+sel,"",33,3500,6800)
        t1[key][sel].Project("comp_"+key+"_"+sel,"A1_M",strip+aa+mcsels[key][sel])
        if key=="40100204":
            h1[sel][key].GetXaxis().SetTitle("m_{#gamma#gamma} (MeV/c^{2})")
            h1[sel][key].GetYaxis().SetTitle("Number of entries / 100 MeV/c^{2}")
            h1[sel][key].SetLineColor(2);h1[sel][key].DrawNormalized()
        else: h1[sel][key].DrawNormalized("same")

    c2[sel] = TCanvas("sig"+"_"+sel,"sig"+"_"+sel);c2[sel].SetWindowSize(1200,900)
    key = "40100200"
    h1[sel][key] = TH1F("comp_"+key+"_"+sel,"",15,9000,12000)
    h1[sel][key].GetXaxis().SetTitle("m_{#gamma#gamma} (MeV/c^{2})")
    h1[sel][key].GetYaxis().SetTitle("Number of entries / 200 MeV/c^{2}")
    t1[key][sel].Project("comp_"+key+"_"+sel,"A1_M",strip+aa+mcsels[key][sel]+aa+mass[key])
    h1[sel][key].Draw()

