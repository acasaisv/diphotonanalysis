#ifndef CALOPOSTCALIBALG_H 
#define CALOPOSTCALIBALG_H 1

// Include files 
// from DaVinci.
#include "Kernel/DaVinciAlgorithm.h"
#include <memory>
#include "GaudiKernel/IRndmGenSvc.h" 
#include "GaudiKernel/RndmGenerators.h"
#include "CaloUtils/CaloParticle.h"


/** @class CaloPostCalibAlg CaloPostCalibAlg.h Extras/CaloPostCalibAlg.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2013-07-19
 */
class CaloPostCalibAlg : public DaVinciAlgorithm {
public: 
  /// Standard constructor
  CaloPostCalibAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  inline bool isPureNeutralCalo(const LHCb::Particle* P)const{
    LHCb::CaloParticle caloP(  (LHCb::Particle*) P );
    return caloP.isPureNeutralCalo();
  }
private:
  double postcalib(const LHCb::Particle* p,double nSpd);
  int m_PhotonID;
  std::map<int,double> m_calib;
  std::map<int,double> m_calibCNV;
  double m_smear;
  mutable IRndmGenSvc*   m_rndmSvc;        ///< random number service 
  IRndmGenSvc* rndmSvc() const  { return m_rndmSvc ; } 
  bool m_toHypo;
  bool m_uSpd;
  bool m_reVertex;
};
#endif // CALOPOSTCALIBALG_H
