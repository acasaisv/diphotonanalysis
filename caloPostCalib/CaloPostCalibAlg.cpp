// Include files 

 // from Gaudi
// local
#include "CaloPostCalibAlg.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Event/Particle.h"
#include "CaloUtils/CaloParticle.h"
#include "Event/RecSummary.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloPostCalibAlg
//
// Apply Ecal post-calibration to photons and direct mother
//
// Warning : update particle momentum only (not yet propagated to covariance)
//
// 2013-07-19 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloPostCalibAlg )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloPostCalibAlg::CaloPostCalibAlg( const std::string& name,
                                    ISvcLocator* pSvcLocator )
  :DaVinciAlgorithm ( name , pSvcLocator ),
   m_PhotonID(22), 
   m_rndmSvc( 0 ){ 
  declareProperty("Calib",m_calib);
  declareProperty("CalibCNV",m_calibCNV);
  declareProperty("Smear",m_smear=0); // for MC to reproduce data 
  declareProperty("ApplyToHypo"    , m_toHypo=true  ); 
  declareProperty("SpdScale"       , m_uSpd=false   ); // when true coeff must be defined as per-1000 deviation per-100 SpdHits  (i.e.  calib = 1 + coeff x SpdHits * 10^-5)
  declareProperty("ReVertexPhoton" , m_reVertex=false )  ;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloPostCalibAlg::initialize() {
  StatusCode sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  m_rndmSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true );

  if( m_reVertex){
    info() << " ==== RE-EVALUATE MOMENTA MOVING PHOTON ORIGIN TO MOTHER ENDVERTEX === " << endmsg;
  }else{    
    info() << " ==== APPLY ECAL POST-CALIBRATION TO PHOTONS and MOTHER === " << endmsg;
    info() << " ====   - Input locations : "<< inputLocations() << endmsg;
    info() << " ====   - Calib coefs (noConv) : " << m_calib << endmsg;
    info() << " ====   - Calib coefs (conv) : " << m_calibCNV << endmsg;
    info() << " ====   - Energy smearing : " << m_smear << endmsg;
    if(m_toHypo)info() << " +++  PROPAGATE TO CALOHYPO (For DTF)" << endmsg;
    if(m_uSpd) info()  << " +++  Apply SpdMult-dependent calibration" << endmsg;
  }
  
  info() << " ====================================" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloPostCalibAlg::execute() {

  // get summary
  const LHCb::RecSummary* rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
  if ( !rS )rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false); 
  double nSpd= ( rS != NULL) ? rS->info(LHCb::RecSummary::nSPDhits,-1) : 0. ;
  if(m_uSpd)counter("SpdScale") += nSpd;


  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.

  bool identity =false;
  if( m_calib.size() == 0 && m_calibCNV.size() == 0 && m_smear ==0.) identity=true;
  if( identity && ! m_reVertex)return StatusCode::SUCCESS;
  if( ! identity && m_reVertex )return StatusCode::FAILURE; // 

  const LHCb::Particle::Range parts = particles();

  int nB = 0;
  for(LHCb::Particle::Range::const_iterator iparts = (parts).begin();iparts != (parts).end(); ++iparts){
    LHCb::Particle* part = (LHCb::Particle*) *iparts;
    
    if( part->particleID().hasBottom() )nB+=1;
    if( part->info(LHCb::Particle::HasBremAdded,0.) == 1. && ! m_reVertex) {
      counter("Already post-calibrated "+Gaudi::Utils::toString(   part->particleID().pid() ) ) += 1;
      continue;
    }
    
    // case 1 : P is a calorimetric photon
    if (part->particleID().pid() == m_PhotonID &&  isPureNeutralCalo(part) && ! m_reVertex) {
      const Gaudi::LorentzVector gmom  = part->momentum() * postcalib(part,nSpd);
      part->setMomentum(gmom);
      part->addInfo(LHCb::Particle::HasBremAdded,1.);  // abuse brem flag !!
      counter("Photon postCalibration") += postcalib(part,nSpd);
      if( m_toHypo ){
        LHCb::CaloHypo* hypo = (LHCb::CaloHypo*) part->proto()->calo().begin()->data();
        if( hypo->lh() != 99. ){
          hypo->setLh(99.); // abuse likelihood parameter
          if( hypo->position() != NULL){
            LHCb::CaloPosition::Parameters& parameters = hypo ->position()->parameters () ;
            parameters ( LHCb::CaloPosition::E ) = part->momentum().E() ;        
          }          
          // some stat :
          LHCb::CaloCellID id = LHCb::CaloCellID((int)part->proto()->info(LHCb::ProtoParticle::CaloNeutralID,0.));
          bool cnv  = part->proto()->info(LHCb::ProtoParticle::CaloDepositID, 0.)<0;
          std::string suffix=(cnv) ? "-Conv. " : "-Unconv.";
          counter("Hypos Calib("+id.areaName()+suffix+")" ) += postcalib(part,nSpd);
        }else
          counter("Already postCalibrated hypos")  += 1;
      }
    }else{
    //case 2 : P has a daughter photon
      bool hasPhoton = false;
      //LHCb::Particle* gamma=NULL;
      std::vector<LHCb::Particle*> gammas;
      Gaudi::LorentzVector pmom  ;
      // loop over daughters - look for direct photon 
      LHCb::Particle::ConstVector tmp = part->daughtersVector();
      for( LHCb::Particle::ConstVector::const_iterator itmp = tmp.begin(); itmp!=tmp.end(); itmp++){
        LHCb::Particle* d = (LHCb::Particle*) *itmp;
        if (d->particleID().pid() == m_PhotonID &&  isPureNeutralCalo(d)) {
          LHCb::Particle* gamma = (LHCb::Particle*) d;
          gammas.push_back(gamma);
          LHCb::CaloParticle cpart(gamma);
          if( NULL != part->endVertex())cpart.setReferencePoint(part->endVertex());
          hasPhoton = true;
          if( gamma->info(LHCb::Particle::HasBremAdded,0.) != 1.){
            // post-calib the photon and add the corrected momentum
            LHCb::CaloParticle photon(  (LHCb::Particle*) d );
            pmom = pmom +  cpart.momentum() * postcalib(d,nSpd);
          }else{
            // already post-calibrated - add photon momentum
            pmom = pmom + cpart.momentum();
          }
        }else{
          // not a photon - add momentum
          pmom = pmom + d->momentum();
        }
        
        //* == *//
        // Stripping20 correction patch : Lambda_b0 -> (Lambda(1520)0->p~- K+)gamma ===>  Lambda_b~0->(Lambda(1520)~0 -> p~- K+) ....
        bool revert = false;
        if( part->particleID().pid() == 5122 &&   d->particleID().pid() == 3124){
          LHCb::Particle::ConstVector ttmp = d->daughtersVector();
          if( ttmp.size() == 2){
            for( LHCb::Particle::ConstVector::const_iterator ittmp = ttmp.begin(); ittmp!=ttmp.end(); ittmp++){
              if( (*ittmp)->particleID().pid() == -2212 ){revert = true;break;}
            }
          }
        }
        if( revert ){
          int antiLb = - part->particleID().pid();
          int antiL  = - d->particleID().pid();
          part->setParticleID( LHCb::ParticleID(antiLb) );
          d->setParticleID(  LHCb::ParticleID(antiL) );
          counter( "Reverted "+Gaudi::Utils::toString( part->particleID().pid() ))+=1;
          counter( "Reverted "+Gaudi::Utils::toString( d->particleID().pid() ))+=1; 
        }
        /* == */
      }    
      if( hasPhoton ){
        part->setMomentum(pmom);
        part->setMeasuredMass(pmom.M());
        if( ! m_reVertex){
          part->addInfo(LHCb::Particle::HasBremAdded,1.);  // abuse brem flag !!
          counter("PostCalibrated "+Gaudi::Utils::toString(part->particleID().pid())) += 1;
        }else{
          counter("ReVertexed "+Gaudi::Utils::toString(part->particleID().pid())) += 1;
        }
        if(  ! m_reVertex ){
          // loop over gammas
          for(std::vector<LHCb::Particle*>::iterator ig=gammas.begin();gammas.end()!=ig;ig++){
            LHCb::Particle* gamma = *ig;
            if( gamma->info(LHCb::Particle::HasBremAdded,0.) != 1.){
              const Gaudi::LorentzVector gmom  = gamma->momentum() * postcalib(gamma,nSpd);
              gamma->setMomentum(gmom);
              gamma->addInfo(LHCb::Particle::HasBremAdded,1.);  // abuse brem flag !!
              counter("Ph. from "+Gaudi::Utils::toString(part->particleID().pid())) += postcalib(gamma,nSpd);
              if( m_toHypo ){
                LHCb::CaloHypo* hypo = (LHCb::CaloHypo*) gamma->proto()->calo().begin()->data();
                if( hypo->lh() != 99. ){
                  hypo->setLh(99.); // abuse likelihood parameter
                  if( hypo->position() != NULL){
                    LHCb::CaloPosition::Parameters& parameters = hypo->position()->parameters () ;
                    parameters ( LHCb::CaloPosition::E ) = gamma->momentum().E() ;        
                  }
                  
                  // Some stat :
                  LHCb::CaloCellID id = LHCb::CaloCellID((int)gamma->proto()->info(LHCb::ProtoParticle::CaloNeutralID,0.));
                  bool cnv  = gamma->proto()->info(LHCb::ProtoParticle::CaloDepositID, 0.)<0;
                  std::string suffix=(cnv) ? "-Conv. " : "-Unconv.";
                  counter("# Hypos Calib ("+id.areaName()+suffix+")" ) += postcalib(gamma,nSpd);
                }else
                  counter("# Already postCalibrated hypos")  += 1;
              }
            }else 
              counter("Ph. from "+Gaudi::Utils::toString(part->particleID().pid()) + " already post-calibrated") += 1;
          } // endloop over gammas
        }  
      }
    }    
  }
  counter( "#B" ) += nB;
  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloPostCalibAlg::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return DaVinciAlgorithm::finalize();  // must be called after all other actions
}
//=============================================================================

double CaloPostCalibAlg::postcalib(const LHCb::Particle* p,double nSpd=-1.){
  if( NULL == p)return 1.;
  const LHCb::ProtoParticle* proto = p->proto();
  if( NULL == proto)return 1.;
  LHCb::CaloCellID id = LHCb::CaloCellID((int)proto->info(LHCb::ProtoParticle::CaloNeutralID,0.));
  bool cnv  = proto->info(LHCb::ProtoParticle::CaloDepositID, 0.)<0;
  if( cnv && m_calibCNV.empty())return 1;
  if( !cnv && m_calib.empty())return 1.;
  double coeff= ( cnv ) ? m_calibCNV[id.area()] : m_calib[id.area()];

  // exctract post-calibration factor
  double calib=1.;
  if( m_uSpd && nSpd >= 0. ){
    calib=1+coeff/1000. * nSpd/100.;   // !!  Coeff is per-mil energy deviation per 100 SpdHits
  }else{
    calib=coeff;
  }

  // apply smearing when requested
  double smear = 1.;
  if( m_smear > 0){
    Rndm::Numbers shot( rndmSvc() , Rndm::Gauss( 0.0 , m_smear ) );
    smear = 1. + shot();
    calib *= smear;
  }  

  debug() << "Photon calibration " << id << " CNV ? " << cnv << " SMEAR = " << smear << " --> " << calib << endmsg;
  return calib;
} 


