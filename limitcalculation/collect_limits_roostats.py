from importlib.machinery import SourceFileLoader

import matplotlib.pyplot as plt
import numpy as np

import params

import pandas as pd

from plot_helpers import *

centermass=[]
lim2down=[]
lim1down=[]
limexp=[]
limobs=[]
lim1up=[]
lim2up = []

rangemax = 5000

# for endpoint in range(6550, 20000, 500):
for center in range(4800, 20200, 200):
	print(center)
	fill = True

	# check file ok
	with open(f"/home3/titus.mombacher/gammagamma/gammacombo/tutorial/UpperLimit_workspace{center}_asymptotic_unblindedonesided.root.txt", "r") as file:
		for line in file:
			content = line.strip().split('\t')
			if content[1] == "inf":
				print("Problem in",center,"!!!")
				print(content)
				fill = False

	if not fill:
		break
		
	# now fill the info
	with open(f"/home3/titus.mombacher/gammagamma/gammacombo/tutorial/UpperLimit_workspace{center}_asymptotic_unblindedonesided.root.txt", "r") as file:
		for line in file:
			content = line.strip().split('\t')
			if content[0]== "max_poi":
				rangemax = float(content[1])
			if content[0]=="median":
				limexp.append(min(float(content[1]),rangemax))
			elif content[0]=="2sigmaminus":
				lim2down.append(min(float(content[1]),rangemax))
			elif content[0]=="1sigmaminus":
				lim1down.append(min(float(content[1]),rangemax))
			elif content[0]=="1sigmaplus":
				lim1up.append(min(float(content[1]),rangemax))
			elif content[0]=="2sigmaplus":
				lim2up.append(min(float(content[1]),rangemax))
			elif content[0]=="measured":
				limobs.append(min(float(content[1]),rangemax))

	centermass.append(center/params.bias_average[0])



# plt.rcParams["figure.figsize"] = [7.00, 3.50]
# plt.rcParams["figure.autolayout"] = True
# n = 256
# X = np.linspace(-np.pi, np.pi, n, endpoint=True)
# Y = np.sin(2 * X)

# plt.rcParams.update({
#     "text.usetex": True,
#     # "font.family": "sans-serif",
#     # "font.sans-serif": "Helvetica",
# })

for i in range(len(limobs)):
	print(centermass[i],"->",limobs[i],": ",lim2down[i],lim1down[i],limexp[i],lim1up[i],lim2up[i])

print(limobs,limexp,lim1up,lim1down,lim2up,lim2down)
fig, ax = plt.subplots()
ax.set_xlabel("$m_{\gamma\gamma}$ [MeV/$\it{c}^2$]")
ax.set_ylabel("UL on $\sigma\\times\mathcal{B}(a\\to\gamma\gamma)$ at 95% CL [pb]")
ax.plot(centermass, lim2up, color='blue', alpha=1.0)
ax.plot(centermass, lim2down, color='blue', alpha=1.0)
ax.fill_between(centermass, lim2down, lim2up, color='blue', alpha=.2)
ax.plot(centermass, lim1up, color='cyan', alpha=1.0)
ax.plot(centermass, lim1down, color='cyan', alpha=1.0)
ax.fill_between(centermass, lim1down, lim1up, color='cyan', alpha=.2)
ax.plot(centermass, limexp, color='red', alpha=1.0)
# ax.plot(centermass, limobs, "o-", color='black', alpha=1.0)

plt.savefig("limitscan_roostats.pdf")

d = {
	"mass": centermass,
	"observed_limit": limobs,
	"expected_minus2sigma": lim2down,
	"expected_minus1sigma": lim1down,
	"expected": limexp,
	"expected_plus1sigma": lim1up,
	"expected_plus2sigma": lim2up
}
df = pd.DataFrame(data=d)
df.to_csv("expected_limit.csv",index=False)

