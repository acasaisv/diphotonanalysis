import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np

masses = [5,6,7,8,9,10,11,13,15,17,19, 20]
pdgratio = [1.01124, 1.0076,1.0082,1.0107,1.0097, 1.0092,1.0091,1.0083,1.0095,1.0117,1.0151, 0.9997]
pdgratio_unc = [2.9e-4, 9e-4,9e-4,9e-4,8e-4, 9e-4,7e-4,8e-4,8e-4,10e-4,6e-4,7e-4]

sigma = [109.0, 147.0, 172.6, 197.5, 220.4, 236.1, 257.0, 305, 357, 420, 406, 241]
sigma_unc = [1.4, 1.7, 2.4, 2.2, 2.2, 2.9, 2.8, 4, 5, 5, 8, 10]

deltasigma = [10, 15, 6, 3, 11, 20, 23, 27, 13, 5, 10, 10]
deltasigma_unc = [100, 4, 6, 5, 5, 7, 6, 8, 10, 13,100,100]


alpha_l = [1,1.55,1.41,1.66,1.69,1.48,1.54,1.41,1.68,1.63,1.07, 0.5]
alpha_l_unc = [10, 0.13,0.14,0.10,0.09,0.12,0.10,0.10,0.17,0.04,0.13,4e-5]

n_l = [2,3.7,4.4,2.0,1.9,3.2,2.9,3.2,1.1,1.0,4.3, 5.2]
n_l_unc = [1000,1.5,2.4,0.7,0.6,1.1,0.9,1.0,0.6,4e-4,2.4, 1000]


alpha_r = [1.18,1.70,1.63,1.68,2.09,1.76,1.78,1.87,1.88,1.91,1,1]
alpha_r_unc = [0.04,0.11,0.10,0.11,0.20,0.11,0.09,0.19,0.13,0.14,10,10]

n_r = [150,130,150,150,2.9,150,160,100,158.70344,158.0,150, 150]
n_r_unc = [60,100,290,260,2.6,220,180,700,3.2e-4,1000,1000,1000]

eff = [0.037,0.069,0.09,0.085,0.083,0.079,0.072,0.069,0.074,0.076,0.073,0.044]
eff_unc = [0.007,0.011,0.013,0.012,0.013,0.013,0.013,0.011,0.012,0.012,0.012,0.008]

def lin(x,a,b):
	return a*x+b

def plateau(x,c):
	return c

par_dict = {
	# "mean_bias"   : [masses,pdgratio,pdgratio_unc],
	# "sigma" 	  : [masses,sigma,sigma_unc],
	# "delta_sigma" : [masses,deltasigma,deltasigma_unc],
	# "alpha_l" 	  : [masses,alpha_l,alpha_l_unc],
	# "alpha_r" 	  : [masses,alpha_r,alpha_r_unc],
	# "n_l" 		  : [masses,n_l,n_l_unc],
	# "n_r" 		  : [masses,n_r,n_r_unc]
	"eff" 		  : [masses,eff,eff_unc]
}

masses.pop(0),masses.pop(-1)#,masses.pop(-1),masses.pop(-1)
for key, val in par_dict.items():
	print(key, "dependency")

	val[1].pop(0),val[1].pop(-1)#,val[1].pop(-1),val[1].pop(-1)
	val[2].pop(0),val[2].pop(-1)#,val[2].pop(-1),val[2].pop(-1)

	popt, pcov = curve_fit(lin, val[0],val[1], sigma=val[2], absolute_sigma=False)
	err = np.sqrt(np.diag(pcov))
	
	print("a=",popt[0],"+/-",err[0], "; ",popt[0]/err[0],"sigma")
	if key=="mean_bias":
		print("b=",popt[1],"+/-",err[1], "; ",(popt[1]-1)/err[0],"sigma")
	else: 
		print("b=",popt[1],"+/-",err[1])
	poptc, pcovc = curve_fit(plateau, val[0],val[1], sigma=val[2], absolute_sigma=False)
	errc = np.sqrt(np.diag(pcovc))
	print("average=",poptc[0],"+/-",errc[0])
	print("mean uncertainy=", np.mean(np.array(val[2])))

	plt.errorbar(val[0],val[1],val[2], fmt="o")
	plt.plot(val[0],lin(np.array(val[0]),*popt))
	plt.ylim([poptc[0]-8*errc[0], poptc[0]+8*errc[0]])
	plt.savefig(key+"_dependency.pdf")
	plt.clf()

# popt, pcov = curve_fit(lin, masses,sigma, sigma=sigma_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("sigma dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,sigma, sigma=sigma_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,sigma,sigma_unc,fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.savefig("sigma_dependence.pdf")
# plt.clf()

# popt, pcov = curve_fit(lin, masses,deltasigma, sigma=deltasigma_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("Delta sigma dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,deltasigma, sigma=deltasigma_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,deltasigma,deltasigma_unc, fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.ylim([0,30])
# plt.savefig("deltasigma_dependence.pdf")
# plt.clf()


# popt, pcov = curve_fit(lin, masses,alpha_l, sigma=alpha_l_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("Alpha l dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,alpha_l, sigma=alpha_l_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,alpha_l,alpha_l_unc, fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.ylim([0,3])
# plt.savefig("alpha_l_dependence.pdf")
# plt.clf()

# popt, pcov = curve_fit(lin, masses,alpha_r, sigma=alpha_r_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("Alpha r dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,alpha_r, sigma=alpha_r_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,alpha_r,alpha_r_unc, fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.ylim([0,3])
# plt.savefig("alpha_r_dependence.pdf")
# plt.clf()


# popt, pcov = curve_fit(lin, masses,n_l, sigma=n_l_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("n l dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,n_l, sigma=n_l_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,n_l,n_l_unc, fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.ylim([0,200])
# plt.savefig("n_l_dependence.pdf")
# plt.clf()


# popt, pcov = curve_fit(lin, masses,n_r, sigma=n_r_unc, absolute_sigma=False)
# err = np.sqrt(np.diag(pcov))

# print("n r dependency")
# print("a=",popt[0],"+/-",err[0], ";", popt[0]/err[0],"sigma")
# print("b=",popt[1],"+/-",err[1])
# poptc, pcovc = curve_fit(plateau, masses,n_r, sigma=n_r_unc, absolute_sigma=False)
# errc = np.sqrt(np.diag(pcovc))
# print("average=",poptc[0],"+/-",errc[0])

# plt.errorbar(masses,n_r,n_r_unc, fmt="o")
# plt.plot(masses,lin(masses,*popt))
# plt.ylim([0,200])
# plt.savefig("n_r_dependence.pdf")
# plt.clf()