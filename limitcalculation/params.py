import numpy as np
from uncertainties import ufloat as uf


def linear(x,a,b):
	return a*x+b

def erroprop_linear(x,err_a, err_b):
	return np.sqrt(err_a**2*x**2+err_b**2)


def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

def interpolate(mass):
	index = find_nearest(masses, mass)
	if masses[index]<mass:
		index2=index+1
	elif masses[index]>mass:
		index2=index-1

	if index==0:
		index2 = 1
	elif index==len(masses)-1:
		index2 = index-1

	eff = efficiencies[index] + (efficiencies[index2] - efficiencies[index])/(masses[index2] - masses[index])*(mass - masses[index])
	unc = uncertainties[index] + (uncertainties[index2] - uncertainties[index])/(masses[index2] - masses[index])*(mass - masses[index])

	return eff, unc, [index,index2]


def get_normalisation(mass):
	lumi = uf(2.07,0.05) #fb-1
	efficiency, uncertainty, _ = interpolate(mass)
	prod = lumi*uf(efficiency,uncertainty)
	return prod, 1./prod


bias_average = [1.0092366214549424,0.00041351558512097775]
sigma_a = [ 22.583476057509966e-3, 0.5500833898951437e-3]
sigma_b = [13.479685590174165, 4.805140817986533]

delta_sigma_val = [13.36847454106725, 2.713233714292147]
sig_alpha_l = [1.559061148975298, 0.04091436596696044]
sig_alpha_r = [ 1.7536505001267468, 0.039752834395833596]
sig_n_l = [2.1505298984781827, 0.3171080324989207]
sig_n_r = [150.70343763955628, 0.007247867961045702]


masses = np.array([6,7,8,9,10,11,13,15,17,19])*1000
efficiencies = 	np.array([0.098,0.125,0.104,0.091,0.085,0.085,0.087,0.085,0.064,0.042])*1e-2
uncertainties = np.array([0.013,0.017,0.014,0.012,0.011,0.012,0.011,0.011,0.008,0.006])*1e-2
