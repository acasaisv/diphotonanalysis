from importlib.machinery import SourceFileLoader

import matplotlib.pyplot as plt
import numpy as np

import params

centermass=[]
lim2down=[]
lim1down=[]
limexp=[]
limobs=[]
lim1up=[]
lim2up = []

# for endpoint in range(6550, 20000, 500):
for center in range(4800, 19800, 200):
	print(center)
	if center in [19200]: print("skipping"); continue
	centermass.append(center/params.bias_average[0])
	# print("/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(endpoint)+"_nsig_DatasetsProb_expected_2StdDown_standardCLs.py")
	twosigmadown = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_expected_2StdDown_standardCLs.py").load_module()
	# print("twosigmadown",twosigmadown.intervals)
	lim2down.append(float(twosigmadown.intervals["0.95"][0]["max"]))
	onesigmadown = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_expected_1StdDown_standardCLs.py").load_module()
	# print("onesigmadown",onesigmadown.intervals)
	lim1down.append(float(onesigmadown.intervals["0.95"][0]["max"]))
	expected = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_expected_standardCLs.py").load_module()
	# print("expected",expected.intervals)
	limexp.append(float(expected.intervals["0.95"][0]["max"]))
	onesigmaup = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_expected_1StdUp_standardCLs.py").load_module()
	# print("onesigmaup",onesigmaup.intervals)
	lim1up.append(float(onesigmaup.intervals["0.95"][0]["max"]))
	twosigmaup = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_expected_2StdUp_standardCLs.py").load_module()
	# print("twosigmaup",twosigmaup.intervals)
	lim2up.append(float(twosigmaup.intervals["0.95"][0]["max"]))
	observed = SourceFileLoader("intervals", "/home3/titus.mombacher/gammagamma/gammacombo/tutorial/plots/cl/clintervals_workspace"+str(center)+"_crosssec_DatasetsProb_CLs2.py").load_module()
	# print("observed",observed.intervals)
	limobs.append(float(observed.intervals["0.95"][0]["max"]))

	# print(float(twosigmaup.intervals["0.95"][0]["max"]))
	# print(float(onesigmaup.intervals["0.95"][0]["max"]))



# plt.rcParams["figure.figsize"] = [7.00, 3.50]
# plt.rcParams["figure.autolayout"] = True
# n = 256
# X = np.linspace(-np.pi, np.pi, n, endpoint=True)
# Y = np.sin(2 * X)

# plt.rcParams.update({
#     "text.usetex": True,
#     # "font.family": "sans-serif",
#     # "font.sans-serif": "Helvetica",
# })

for i in range(len(limobs)):
	print(centermass[i],"->",limobs[i],": ",lim2down[i],lim1down[i],limexp[i],lim1up[i],lim2up[i])
print(limobs,limexp,lim1up,lim1down,lim2up,lim2down)
fig, ax = plt.subplots()
ax.set_xlabel("$m_{\gamma\gamma}$ [MeV/$\it{c}^2$]")
ax.set_ylabel("UL on $\sigma\\times\mathcal{B}(a\\to\gamma\gamma)$ at 95% CL [pb]")
ax.plot(centermass, lim2up, color='blue', alpha=1.0)
ax.plot(centermass, lim2down, color='blue', alpha=1.0)
ax.fill_between(centermass, lim2down, lim2up, color='blue', alpha=.2)
ax.plot(centermass, lim1up, color='cyan', alpha=1.0)
ax.plot(centermass, lim1down, color='cyan', alpha=1.0)
ax.fill_between(centermass, lim1down, lim1up, color='cyan', alpha=.2)
ax.plot(centermass, limexp, color='red', alpha=1.0)
# ax.plot(centermass, limobs, "o-", color='black', alpha=1.0)

plt.savefig("limitscan.pdf")
