import ROOT as R
import numpy as np
import matplotlib.pyplot as plt

import uproot3 as uproot
import pickle
import xgboost as xgb

import lhcbStyle
lhcbStyle.setLHCbStyle()

# sigyield = 100
# bkgyield = 1000
cvals = [1.,0.5,0.05,0.01, 0,0,0]

## setup the model

R.gROOT.SetBatch()

mass = R.RooRealVar("B_M", "#it{m}(#it{#gamma}#it{#gamma}) [MeV/#it{c}^{2}]", 4800., 20000.)
nbkg = R.RooRealVar("nbkg", "nbkg", 10000, 0., 1e7)
nsig = R.RooRealVar("nsig", "nsig", 10, -1e4, 1e4)

c0 = R.RooRealVar("coeff0", "coeff0", cvals[0], -5., 5.)
c1 = R.RooRealVar("coeff1", "coeff1", cvals[1], -2., 2.)
c2 = R.RooRealVar("coeff2", "coeff2", cvals[2], -1., 1.)
c3 = R.RooRealVar("coeff3", "coeff3", cvals[3], -1, 1)
c5 = R.RooRealVar("coeff5", "coeff5", cvals[4], -1, 1)
c7 = R.RooRealVar("coeff7", "coeff7", cvals[5], -1, 1)
c9 = R.RooRealVar("coeff9", "coeff9", cvals[6], -1, 1)

l0 = R.RooLegendre("legendre0", "legendre0", mass, 0)
l1 = R.RooLegendre("legendre1", "legendre1", mass, 1)
l2 = R.RooLegendre("legendre2", "legendre2", mass, 2)
l3 = R.RooLegendre("legendre3", "legendre3", mass, 3)
l5 = R.RooLegendre("legendre5", "legendre5", mass, 5)
l7 = R.RooLegendre("legendre7", "legendre7", mass, 7)
l9 = R.RooLegendre("legendre9", "legendre9", mass, 9)

mu = R.RooRealVar("mu", "mu", 6500., 4800., 8000.)
sigma_l = R.RooRealVar("sigma_l", "sigma_l", 1e3, 0.001, 9000.)
sigma_r = R.RooRealVar("sigma_r", "sigma_r", 2e3, 0.001, 9000.)
alpha_l = R.RooRealVar("alpha_l", "alpha_l", 3., 0.001, 5.)
alpha_r = R.RooRealVar("alpha_r", "alpha_r", 2., 0.001, 5.)
n_l = R.RooRealVar("n_l", "n_l", 1.6, 0.001, 10.)
n_r = R.RooRealVar("n_r", "n_r", 5., 0.001, 10.)
cb = R.RooCrystalBall ("cb", "cb", mass, mu, sigma_l, sigma_r, alpha_l, n_l, alpha_r, n_r)

mean = R.RooRealVar("mean", "mean", 0.)
width = R.RooRealVar("width", "width", 0.1)
gauss = R.RooGaussian("gauss", "gauss", mass, mean, width)

sum_pdf = R.RooRealSumPdf("sum", "sum", R.RooArgList(l0,l1,l2,l3,l5,l7,l9), R.RooArgList(c0,c1,c2,c3,c5,c7,c9))
# sum_pdf = R.RooRealSumPdf("sum", "sum", R.RooArgList(l0,l1,l3,l5,l7,l9), R.RooArgList(c0,c1,c3,c5,c7,c9))
sum_ext = R.RooAddPdf("sum_ext", "sum_ext", R.RooArgList(gauss,sum_pdf), R.RooArgList(nsig,nbkg))

# ## read in the data

# file = R.TFile("/scratch47/adrian.casais/ntuples/background/Stripping34.root")
# tree = file.Get("DecayTree/DecayTree")
# tree.SetBranchStatus("*",False)
# tree.SetBranchStatus("B_M",True)
# tree.SetBranchStatus("B_MERR",True)
# tree.SetBranchStatus("Gamma1_CaloHypo_Saturation",True)
# tree.SetBranchStatus("Gamma2_CaloHypo_Saturation",True)


# hist_total = R.TH1D("hist_total", "hist_total", 100, 4800, 9000)

# tree.Draw("B_M>>hist_total", "!Gamma1_CaloHypo_Saturation&&!Gamma1_CaloHypo_Saturation")

# data = R.RooDataHist("data","data",R.RooArgSet(mass),hist_total)




print("open events")
file = uproot.open("/scratch47/adrian.casais/ntuples/background/Stripping34-2022.root")
events = file['DecayTree/DecayTree']

print("make data frame")
variables_BDT = [
	"B_B_CONEMULT_1_0",
	"B_B_CONEP_1_0",
	"B_B_CONEPASYM_1_0",
	"B_B_CONEPT_1_0",
	"B_B_CONEPTASYM_1_0",
	"B_B_CONEMULT_1_35",
	"B_B_CONEP_1_35",
	"B_B_CONEPASYM_1_35",
	"B_B_CONEPT_1_35",
	"B_B_CONEPTASYM_1_35",
	"B_B_CONEMULT_1_7",
	"B_B_CONEP_1_7",
	"B_B_CONEPASYM_1_7",
	"B_B_CONEPT_1_7",
	"B_B_CONEPTASYM_1_7"
	]
additional_vars = ['B_L0ElectronDecision_TOS','B_L0PhotonDecision_TOS','B_Hlt1B2GammaGammaDecision_TOS','B_Hlt1B2GammaGammaHighMassDecision_TOS','B_Hlt2RadiativeB2GammaGammaDecision_TOS','Gamma1_PT','Gamma2_PT','Gamma1_PP_Saturation','Gamma2_PP_Saturation','Gamma1_PP_IsPhoton','Gamma2_PP_IsPhoton','B_M']
variables = variables_BDT+ additional_vars
df = events.pandas.df(branches=variables)

print("cut saturation")
cuts = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS and Gamma1_PT > 3000 and Gamma2_PT > 3000 and Gamma1_PP_Saturation<1 and Gamma2_PP_Saturation<1 and Gamma1_PP_IsPhoton>0.85 and Gamma2_PP_IsPhoton>0.85'
df = df.query(cuts)

df['MinPT'] = np.minimum(df['Gamma1_PT'],df['Gamma2_PT'])
df['MaxPT'] = np.maximum(df['Gamma1_PT'],df['Gamma2_PT'])

print("size", df.shape[0])
print("read the bdt")
with open('../CalculateEFficiencies/bdt.pickle','rb') as handle:
  bdt = pickle.load(handle)

with open('../CalculateEFficiencies/bdt_Bs.pickle','rb') as handle:
  bdt_Bs = pickle.load(handle)

# with open('/home3/adrian.casais/diphotonanalysis/ConeVarsBDT/bdt.pickle','rb') as handle:
#   bdt = pickle.load(handle)

print("predict bdt")
print(variables_BDT)
variables_BDT_Bs = variables_BDT+['MinPT','MaxPT']

df['bdt'] = np.mean([fold.predict_proba(df[variables_BDT])[:,1] for fold in bdt],axis=0)
df['bdt_Bs'] = np.mean([fold.predict_proba(df[variables_BDT_Bs])[:,1] for fold in bdt_Bs],axis=0)

plt.hist(df["B_M"], 100)
plt.savefig("test_data.png")

df_ALP = df.query('bdt>0.9')
df_Bs = df.query('bdt_Bs>0.85')

for dataset in ["ALP","Bs"]:

	df_fit = df_ALP if dataset=="ALP" else df_Bs 

	print("size", df_fit.shape[0])

	plt.clf()
	plt.hist(df_fit["B_M"], 100)
	plt.savefig("test_data2_"+dataset+"_.png")


	print("get data")
	data = R.RooDataSet.from_pandas(df_fit, R.RooArgSet(mass))

	fr = cb.fitTo(data,R.RooFit.Strategy(2), R.RooFit.Offset(True),R.RooFit.Save())
	fr.Print("v")
	canv = R.TCanvas()
	frame = mass.frame()

	data.plotOn(frame, R.RooFit.Binning(100), R.RooFit.Name("Data"))
	cb.plotOn(frame,R.RooFit.Name("Total"))


	## Draw with pulls
	main_frame_height = 0.75;
	pull_frame_height = 0.25;
	main_pull_ratio = main_frame_height / pull_frame_height;
	num_sigma_pull_range = 5.;
	danger_limit = 3.0;

	# m_massbins = frame.GetNbinsX()
	m_massbins = 100

	## Create RooPlot object for pulls
	frame_pulls = R.RooPlot(frame.GetXaxis().GetXmin(), frame.GetXaxis().GetXmax());
	frame_pulls.GetXaxis().SetTitle(frame.GetXaxis().GetTitle());

	## Main pad
	pad_main = R.TPad("pad_main", "The main pad", 0.0, 1 - main_frame_height, 1.0, 1.0);
	pad_main.SetBottomMargin(0.02); # Insert a spacing between main frame and pull frame, if spacing is set to True
	pad_main.cd();

	LHCbStyle = R.gROOT.GetStyle("lhcbStyle")
	Label = R.TPaveText(0.70 - LHCbStyle.GetPadRightMargin(),
						0.85 - LHCbStyle.GetPadTopMargin(),
						0.95 - LHCbStyle.GetPadRightMargin(),
						0.95 - LHCbStyle.GetPadTopMargin(),
						"BRNDC")
	# Label = R.TPaveText(0.15, 0.2, 0.45, 0.4,"BRNDC")
	# Label.SetBorderSize(1)
	# Label.SetFillColorAlpha(R.kWhite, 0.0)
	# Label.SetLineColorAlpha(R.kWhite, 0.0)
	# Label.AddText("Center at "+str(center)+":")
	# Label.AddText("["+str(round(beginpoint))+" - "+str(round(endpoint))+"]")


	## Manipulate and draw pad_main
	# frame->SetMinimum(0.5)
	frame.GetXaxis().SetLabelSize(0.0);
	frame.GetXaxis().SetTitleSize(0.0);       # Remove the x-axis title of the main frame
	frame.Draw();
	# Label.Draw()

	## Pull pad
	pad_pulls = R.TPad("pad_pulls", "The pull pad", 0.0, 0.0, 1.0, pull_frame_height);
	pad_pulls.SetTopMargin(0.06);
	pad_pulls.SetBottomMargin(0.4);  # The magic happens here!
	pad_pulls.cd();

	## Manipulate and draw pad_pulls
	hpull = frame.pullHist("Data","Total");
	if not hpull: print("ERROR: No RooCurve found!")

	range_min_x = frame.GetXaxis().GetXmin();
	range_max_x = frame.GetXaxis().GetXmax();

	## Create new TH1 objects for pulls

	pulls_all = R.TH1D("pulls_all", "pulls_all", m_massbins, range_min_x, range_max_x);
	pulls_ok = R.TH1D("pulls_ok", "pulls_ok", m_massbins, range_min_x, range_max_x);
	pulls_danger = R.TH1D("pulls_danger", "pulls_danger", m_massbins, range_min_x, range_max_x);

	pulls_all.SetFillStyle(0);
	pulls_ok.SetFillColor(R.kGray);
	pulls_danger.SetFillColor(R.kRed);

	for i in range(1,m_massbins+1):
		cur_x = hpull.GetPointX(i);
		cur_y = hpull.GetPointY(i);
		pulls_ok.SetBinContent(i, 0.);
		pulls_danger.SetBinContent(i, 0.);

		# add to total pulls data set
		# pull_var->setVal(cur_y);
		# pull_total->add(*pull_var_set);
		# Needed for plotting pulls_danger
		if abs(cur_y) > num_sigma_pull_range and cur_y >= 0.: cur_y = num_sigma_pull_range;
		elif abs(cur_y) > num_sigma_pull_range and cur_y < 0.: cur_y = -num_sigma_pull_range;

		pulls_all.SetBinContent(i, cur_y);
		if abs(cur_y) > danger_limit: pulls_danger.SetBinContent(i, cur_y);
		else: pulls_ok.SetBinContent(i, cur_y);

	## Configure frame
	frame_pulls.SetMinimum(-num_sigma_pull_range);
	frame_pulls.SetMaximum(num_sigma_pull_range);
	frame_pulls.SetTitle("");

	## scale frame
	frame_pulls.GetXaxis().SetTitleSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetTickSize(0.05);

	frame_pulls.GetYaxis().SetTitleSize(0.072 * main_pull_ratio);
	frame_pulls.GetYaxis().SetTitleOffset(0.95 / main_pull_ratio);
	frame_pulls.GetYaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetYaxis().SetNdivisions(503);

	frame_pulls.GetYaxis().SetTitle("Pull");
	frame_pulls.Draw();

	## Add sigma lines
	zeroline = R.TLine(range_min_x, 0.0, range_max_x, 0.0);

	threesigmaline = R.TLine(range_min_x, 3.0, range_max_x, 3.0);
	threesigmaline.SetLineStyle(R.kDashed);
	threesigmaline.SetLineColor(R.kGray + 2);

	minusthreesigmaline = R.TLine(range_min_x, -3.0, range_max_x, -3.0);
	minusthreesigmaline.SetLineStyle(R.kDashed);
	minusthreesigmaline.SetLineColor(R.kGray + 2);

	## Draw all the stuff
	zeroline.Draw("SAME");
	threesigmaline.Draw("SAME");
	minusthreesigmaline.Draw("SAME");
	pulls_all.Draw("SAME");
	pulls_danger.Draw("SAME");
	pulls_ok.Draw("SAME");

	## Canvas
	canv.cd()
	pad_main.Draw()
	pad_pulls.Draw("SAME");
	canv.Draw()
	canv.SaveAs("fit_shoulder_"+dataset+".pdf")

	## now generate

	data_toy = cb.generate(R.RooArgSet(mass), data.sumEntries()*25)

	toyfile = R.TFile("toydata_"+dataset+".root", "RECREATE")
	toyfile.cd()
	data_toy.convertToTreeStore()
	toytree = data_toy.tree()
	toytree.Write()
	toyfile.Write()
	toyfile.Close()

