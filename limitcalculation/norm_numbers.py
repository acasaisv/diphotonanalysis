import numpy as np
from uncertainties import ufloat as uf


# effs in %
effs = [uf(0.058,0.012),uf(0.085,0.017),uf(0.089,0.018),uf(0.092,0.018),uf(0.092,0.017),uf(0.084,0.015),uf(0.080,0.014), uf(0.082,0.014),uf(0.079,0.013),uf(0.068,0.012)]

lumi = uf(2.07,0.05) #fb-1
eff_bef = uf(0.76,0.12)*1e-3
eff_new = np.mean(effs)*1e-2

unc = np.mean([e.s/e.n for e in effs])*eff_new.n

eff = uf(eff_new.n,unc)
print(eff_new, eff_bef, eff)


fsfd =uf(0.2539,0.0079)
fu=uf(0.405,0.0405)
sigma= uf(495,np.sqrt(2**2+52**2))*1e9

eff_Bs = uf(0.031,0.006)*1e-2

print("Normalisation ALPs:", lumi*eff, "as SES:", 1./(lumi*eff), "fb")
print("Normalisation Bs2gammagamma:", lumi*eff_Bs*fsfd*fu*sigma*2, "as SES:",1./(lumi*eff_Bs*fsfd*fu*sigma*2))
print("Normalisation Bd2gammagamma:", lumi*eff_Bs*fu*sigma*2, "as SES:",1./(lumi*eff_Bs*fu*sigma*2))


# Normalisation ALPs: 0.00167+/-0.00031 as SES: (6.0+/-1.1)e+02
# Normalisation Bs2gammagamma: (1.7+/-0.4)e+08 as SES: (5.9+/-1.4)e-09
# Normalisation Bd2gammagamma: (6.7+/-1.6)e+08 as SES: (1.49+/-0.35)e-09
