import ROOT as R
import lhcbStyle
lhcbStyle.setLHCbStyle()


def linear(x,a,b):
	return a*x+b

def erroprop_linear(x,err_a, err_b):
	return np.sqrt(err_a**2*x**2+err_b**2)


R.gROOT.SetBatch()
R.RooMsgService.instance().setGlobalKillBelow(R.RooFit.ERROR)
R.RooMsgService.instance().setSilentMode(True)


mass = R.RooRealVar("B_M", "#it{m}(#gamma#gamma)", 4800., 6200.)

mean_broad = R.RooRealVar("mean_broad", "mean_broad", 6000., 4800.,10000.)
width_broad = R.RooRealVar("width_broad", "width_broad", 1200., 500.,6000.)
gaus_broad = R.RooGaussian("gauss_broad", "gauss_broad", mass, mean_broad, width_broad)


c_const = R.RooRealVar("coeff_const", "coeff_const", 0.)
c1_4 = R.RooRealVar("coeff1_4", "coeff1_4", 0.17, 0., 1.)
c2_4 = R.RooRealVar("coeff2_4", "coeff2_4", -0.05, -0.1, 0.)
c3_4 = R.RooRealVar("coeff3_4", "coeff3_4", -0.003, -0.1, 0.05)
c5_4 = R.RooRealVar("coeff5_4", "coeff5_4", -0.003, -0.01, 0.01)
c7_4 = R.RooRealVar("coeff7_4", "coeff7_4", 0.0, -0.01, 0.01)
c9_4 = R.RooRealVar("coeff9_4", "coeff9_4", 0., -0.01, 0.01)
bkg_pdf_2 = R.RooChebychev("sum_cheby_2", "sum_cheby", mass, R.RooArgList(c1_4,c2_4,c3_4))

file = R.TFile("toydata_Bs.root")
tree = file.Get("RooTreeDataStore_cbData_Generated_From_cb")

# tfile_dummy = R.TFile("dump_bumphunt.root", "recreate")
# treecut = tree.CopyTree("B_M>4800&&B_M<6200")

data = R.RooDataSet("data", "data", tree,R.RooArgSet(mass), "B_M>4800&&B_M<6200")

mu = R.RooRealVar("mu", "mu", 5332)
sigma = R.RooRealVar("sigma", "sigma", 0.125)
delta_sigma = R.RooRealVar("delta_sigma", "#Delta#sigma", 13.36847454106725)
sigma_l = R.RooFormulaVar("sigma_l", "@0-@1",R.RooArgList(sigma, delta_sigma))
sigma_r = R.RooFormulaVar("sigma_r", "@0+@1",R.RooArgList(sigma, delta_sigma))

# sigma_l = R.RooRealVar("sigma_l", "sigma_l",sigma.getVal() - delta_sigma.getVal())
# sigma_r = R.RooRealVar("sigma_r", "sigma_r",sigma.getVal() + delta_sigma.getVal())

alpha_l = R.RooRealVar("alpha_l", "alpha_l", 1.56)#, 0.3,0.8)
alpha_r = R.RooRealVar("alpha_r", "alpha_r", 1.75)#, 0.5, 2.)
n_l = R.RooRealVar("n_l", "n_l", 2.15)#, 1., 10.)
n_r = R.RooRealVar("n_r", "n_r", 155)#159)#, 0.5, 3.)
cb = R.RooCrystalBall ("cb", "cb", mass, mu, sigma_l, sigma_r, alpha_l, n_l, alpha_r, n_r)


nbkg = R.RooRealVar("nbkg", "nbkg", 1.3e5, 0., 1e6)
nsig = R.RooRealVar("nsig", "nsig", 0, -1000, 1e5)

norm_Bs = R.RooRealVar("norm_Bs", "norm_Bs", 2.3e-8,1e-11,3e-7)
norm_Bd = R.RooRealVar("norm_Bd", "norm_Bd", 5.8e-9,1e-11,1e-7)

# norm_Bs.setConstant()
# norm_Bd.setConstant()

norm_Bs_obs = R.RooRealVar("norm_Bs_obs", "norm_Bs_obs", 2.3e-8)
norm_Bd_obs = R.RooRealVar("norm_Bd_obs", "norm_Bd_obs", 5.8e-9)

norm_Bs_err = R.RooRealVar("norm_Bs_err", "norm_Bs_err", 0.5e-8)
norm_Bd_err = R.RooRealVar("norm_Bd_err", "norm_Bd_err", 1.3e-9)

norm_constr_Bs = R.RooGaussian("norm_constr_Bs", "norm_constr_Bs", norm_Bs_obs, norm_Bs, norm_Bs_err)
norm_constr_Bd = R.RooGaussian("norm_constr_Bd", "norm_constr_Bd", norm_Bd_obs, norm_Bd, norm_Bd_err)

BF_Bs = R.RooRealVar("BF_Bs", "#font[32]{B}( B^{0}_{#it{s}}#rightarrow#it{#gamma#gamma} )", 0.,-5e-5,1e-4)
BF_Bd = R.RooRealVar("BF_Bd", "#font[32]{B}( B^{0}#rightarrow#it{#gamma#gamma} )", 0,-2e-5,5e-5)


Nsig_Bs = R.RooFormulaVar("Nsig_Bs", "@0/@1",R.RooArgList(BF_Bs,norm_Bs))
Nsig_Bd = R.RooFormulaVar("Nsig_Bd", "@0/@1",R.RooArgList(BF_Bd,norm_Bd))

SumPdf_Bd = R.RooAddPdf("fullpdf_Bd", "SumPdf_Bd", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(Nsig_Bd,nbkg))
SumPdf_Bs = R.RooAddPdf("fullpdf_Bs", "SumPdf_Bs", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(Nsig_Bs,nbkg))

bias_average = [1.0092366214549424,0.00041351558512097775]
sigma_a = [ 22.583476057509966e-3, 0.5500833898951437e-3]
sigma_b = [13.479685590174165, 4.805140817986533]


for decay in ["Bs2gg", "B02gg"]:

	pdgmean = 5279.66 if decay == "B02gg" else 5366.92

	constraints = R.RooArgSet(norm_constr_Bs) if decay=="Bs2gg" else R.RooArgSet(norm_constr_Bd)
	global_obs = R.RooArgSet(norm_Bs_obs) if decay=="Bs2gg" else R.RooArgSet(norm_Bd_obs)


	sigma_val = linear(pdgmean,sigma_a[0],sigma_b[0])/1.1  ## the simulation overestimates the true width
	sigma.setVal(sigma_val)

	mu.setVal(pdgmean*bias_average[0])

	SumPdf= SumPdf_Bd 
	# if decay=="Bs2gg":
	# 	SumPdf= SumPdf_Bs
	SumPdf= R.RooAddPdf("fullpdf", "SumPdf_Bd", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(Nsig_Bd,nbkg)) if decay == "B02gg" else R.RooAddPdf("fullpdf", "SumPdf_Bs", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(Nsig_Bs,nbkg))

	result = SumPdf.fitTo(data, R.RooFit.Save(), R.RooFit.ExternalConstraints(constraints))
	# gaus_broad.fitTo(data)

	result.Print("v")

	canv = R.TCanvas()
	frame = mass.frame()
	data.plotOn(frame, R.RooFit.Name("Data"))
	SumPdf.plotOn(frame, R.RooFit.Name("Total"))
	cb.plotOn(frame, R.RooFit.Normalization(0.1), R.RooFit.LineColor(R.kRed),R.RooFit.LineStyle(R.kDashed))


	## Draw with pulls
	main_frame_height = 0.75;
	pull_frame_height = 0.25;
	main_pull_ratio = main_frame_height / pull_frame_height;
	num_sigma_pull_range = 5.;
	danger_limit = 2.0;

	# m_massbins = frame.GetNbinsX()
	m_massbins = 100

	## Create RooPlot object for pulls
	frame_pulls = R.RooPlot(frame.GetXaxis().GetXmin(), frame.GetXaxis().GetXmax());
	frame_pulls.GetXaxis().SetTitle(frame.GetXaxis().GetTitle());

	## Main pad
	pad_main = R.TPad("pad_main", "The main pad", 0.0, 1 - main_frame_height, 1.0, 1.0);
	pad_main.SetBottomMargin(0.02); # Insert a spacing between main frame and pull frame, if spacing is set to True
	pad_main.cd();

	LHCbStyle = R.gROOT.GetStyle("lhcbStyle")
	# Label = R.TPaveText(0.70 - LHCbStyle.GetPadRightMargin(),
	# 					0.85 - LHCbStyle.GetPadTopMargin(),
	# 					0.95 - LHCbStyle.GetPadRightMargin(),
	# 					0.95 - LHCbStyle.GetPadTopMargin(),
	# 					"BRNDC")
	# Label = R.TPaveText(0.15, 0.2, 0.45, 0.4,"BRNDC")
	# Label.SetBorderSize(1)
	# Label.SetFillColorAlpha(R.kWhite, 0.0)
	# Label.SetLineColorAlpha(R.kWhite, 0.0)
	# Label.AddText("Center at "+str(center)+":")
	# Label.AddText("["+str(round(beginpoint))+" - "+str(round(endpoint))+"]")


	## Manipulate and draw pad_main
	# frame->SetMinimum(0.5)
	frame.GetXaxis().SetLabelSize(0.0);
	frame.GetXaxis().SetTitleSize(0.0);       # Remove the x-axis title of the main frame
	frame.Draw();
	# Label.Draw()

	## Pull pad
	pad_pulls = R.TPad("pad_pulls", "The pull pad", 0.0, 0.0, 1.0, pull_frame_height);
	pad_pulls.SetTopMargin(0.06);
	pad_pulls.SetBottomMargin(0.4);  # The magic happens here!
	pad_pulls.cd();

	## Manipulate and draw pad_pulls
	hpull = frame.pullHist("Data","Total");
	if not hpull: print("ERROR: No RooCurve found!")

	range_min_x = frame.GetXaxis().GetXmin();
	range_max_x = frame.GetXaxis().GetXmax();

	## Create new TH1 objects for pulls

	pulls_all = R.TH1D("pulls_all", "pulls_all", m_massbins, range_min_x, range_max_x);
	pulls_ok = R.TH1D("pulls_ok", "pulls_ok", m_massbins, range_min_x, range_max_x);
	pulls_danger = R.TH1D("pulls_danger", "pulls_danger", m_massbins, range_min_x, range_max_x);

	pulls_all.SetFillStyle(0);
	pulls_ok.SetFillColor(R.kGray);
	pulls_danger.SetFillColor(R.kRed);

	for i in range(1,m_massbins+1):
		cur_x = hpull.GetPointX(i);
		cur_y = hpull.GetPointY(i);
		pulls_ok.SetBinContent(i, 0.);
		pulls_danger.SetBinContent(i, 0.);

		# add to total pulls data set
		# pull_var->setVal(cur_y);
		# pull_total->add(*pull_var_set);
		# Needed for plotting pulls_danger
		if abs(cur_y) > num_sigma_pull_range and cur_y >= 0.: cur_y = num_sigma_pull_range;
		elif abs(cur_y) > num_sigma_pull_range and cur_y < 0.: cur_y = -num_sigma_pull_range;

		pulls_all.SetBinContent(i, cur_y);
		if abs(cur_y) > danger_limit: pulls_danger.SetBinContent(i, cur_y);
		else: pulls_ok.SetBinContent(i, cur_y);

	## Configure frame
	frame_pulls.SetMinimum(-num_sigma_pull_range);
	frame_pulls.SetMaximum(num_sigma_pull_range);
	frame_pulls.SetTitle("");

	## scale frame
	frame_pulls.GetXaxis().SetTitleSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetTickSize(0.05);

	frame_pulls.GetYaxis().SetTitleSize(0.072 * main_pull_ratio);
	frame_pulls.GetYaxis().SetTitleOffset(0.95 / main_pull_ratio);
	frame_pulls.GetYaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetYaxis().SetNdivisions(503);

	frame_pulls.GetYaxis().SetTitle("Pull");
	frame_pulls.Draw();

	## Add sigma lines
	zeroline = R.TLine(range_min_x, 0.0, range_max_x, 0.0);

	threesigmaline = R.TLine(range_min_x, 3.0, range_max_x, 3.0);
	threesigmaline.SetLineStyle(R.kDashed);
	threesigmaline.SetLineColor(R.kGray + 2);

	minusthreesigmaline = R.TLine(range_min_x, -3.0, range_max_x, -3.0);
	minusthreesigmaline.SetLineStyle(R.kDashed);
	minusthreesigmaline.SetLineColor(R.kGray + 2);

	## Draw all the stuff
	zeroline.Draw("SAME");
	threesigmaline.Draw("SAME");
	minusthreesigmaline.Draw("SAME");
	pulls_all.Draw("SAME");
	pulls_danger.Draw("SAME");
	pulls_ok.Draw("SAME");

	## Canvas
	canv.cd()
	pad_main.Draw()
	pad_pulls.Draw("SAME");
	canv.Draw()

	canv.SaveAs(decay+"_sensitivity.pdf")

	parameters = result.floatParsFinal()
	obs = R.RooArgSet(mass)

	poi = BF_Bs
	if decay=="B02gg":
		poi = BF_Bd

	nuisance_pars = R.RooArgSet()
	iterator = parameters.createIterator()
	var = iterator.Next()
	while var :
		if var.GetName()!=poi.GetName():
			nuisance_pars.add(var)
		var = iterator.Next()


	wspc = R.RooWorkspace("workspace"+decay)
	getattr(wspc,"import")(SumPdf)
	getattr(wspc,"import")(data)
	getattr(wspc,"import")(result, "data_fit_result")
	wspc.defineSet('constraint_set', constraints, True)
	wspc.defineSet('global_observables_set', global_obs, True)
	wspc.defineSet('datasetObservables', obs, True)
	wspc.defineSet('parameters', parameters, True)

	##############
	## modifications for use with RooStats
	##############
	wspc.factory("PROD::constrmodel(fullpdf,norm_constr_Bs)") if decay=="Bs2gg" else wspc.factory("PROD::constrmodel(fullpdf,norm_constr_Bd)")

	mc = R.RooStats.ModelConfig("modelconf",wspc)
	mc.SetPdf(wspc.pdf("constrmodel"))
	mc.SetObservables(obs)
	if decay == "Bs2gg":
		mc.SetParametersOfInterest(R.RooArgSet(BF_Bs))
	else:
		mc.SetParametersOfInterest(R.RooArgSet(BF_Bd))
	mc.SetNuisanceParameters(nuisance_pars)
	mc.SetGlobalObservables(global_obs)
	if decay == "Bs2gg":
		mc.SetSnapshot(R.RooArgSet(BF_Bs))
	else:
		mc.SetSnapshot(R.RooArgSet(BF_Bd))

	## copy for background model
	bModel = mc.Clone()
	bModel.SetName(mc.GetName()+"_with_poi_0")
	var = bModel.GetParametersOfInterest().first()
	oldval = var.getVal()
	var.setVal(0)
	var.setConstant(True)
	print("bModel ",bModel)
	bModel.GetPdf().fitTo(data)
	var.setConstant(False)
	bModel.SetSnapshot(R.RooArgSet(var))
	var.setVal(oldval)
	bModel.Print()
	mc.Print()
	data.Print();
	getattr(wspc,"import")(mc)
	getattr(wspc,"import")(bModel)

	wspc.SaveAs("../../workspace"+decay+".root", "RECREATE")


