import ROOT as R
import numpy as np

import matplotlib.pyplot as plt

import lhcbStyle
lhcbStyle.setLHCbStyle()

import params as par

from plot_helpers import *

def fillDataSet(tree, x, endpoint, deltax, dsName = 'data', frac=1.0):
	#cut on x<endpoint&&x>endpoint-2500
	tfile_dummy = R.TFile("dump_bumphunt.root", "recreate")
	# treecut = tree.CopyTree(x.GetName()+"<"+str(endpoint)+"&&"+x.GetName()+">"+str(endpoint - deltax)+"&&!Gamma1_CaloHypo_Saturation&&!Gamma2_CaloHypo_Saturation&&Gamma1_PT>3000&&Gamma2_PT>3000")
	treecut = tree.CopyTree(x.GetName()+"<"+str(endpoint)+"&&"+x.GetName()+">"+str(endpoint - deltax))
	#setVal to -1+2*[(x-endpoint+deltax)/deltax]


	cols = R.RooArgSet(x)
	ds = R.RooDataSet(dsName, dsName, cols)
	#ds.Print()
	# N=treecut.GetEntries()
	print(treecut)
	# print('length data:',N) 
	for entry in treecut:
		x.setVal(-1+2*((entry.B_M-endpoint+deltax)/deltax))
		ds.add(cols)
	ds.Print()

	# ##ToDo: make it a RooDataHist instead: fill TH1D with 200 bins and convert to RooDataHist
	# print("filling between",x.getMin(),"and",x.getMax())
	# hist = R.TH1D("temp", "temp", 1000, x.getMin(), x.getMax())
	# x.setBins(1000)

	# for entry in treecut:
	# 	if np.random.uniform()<frac:
	# 		hist.Fill(-1+2*((entry.B_M-endpoint+deltax)/deltax))

	# ds = R.RooDataHist(dsName,dsName,R.RooArgSet(x),hist)
	return ds

def linear(x,a,b):
	return a*x+b

def erroprop_linear(x,err_a, err_b):
	return np.sqrt(err_a**2*x**2+err_b**2)


R.gROOT.SetBatch()
R.RooMsgService.instance().setGlobalKillBelow(R.RooFit.ERROR)
R.RooMsgService.instance().setSilentMode(True)

R.gROOT.ProcessLineSync(".x RooMultiPdf.cpp+")

file = R.TFile("toydata_ALP.root")
tree = file.Get("RooTreeDataStore_cbData_Generated_From_cb")

yields, yielderrors = [], []
bkgyields, bkgyielderrors = [], []

centermass = []

canv = R.TCanvas()
canv.SaveAs("individual_fits.pdf[")

default_centers = [*range(4800, 20200, 200)]
mass_centers = default_centers+[9398.7*par.bias_average[0]]

mass_centers.sort()

window_length = 10 # in units of sigma

for center in mass_centers:

	sigma_val = linear(center,par.sigma_a[0],par.sigma_b[0])/1.1 ## the simulation overestimates the true width
	deltam = window_length*sigma_val

	centermass.append(center/par.bias_average[0])
	endpoint_theo = center + window_length/2*sigma_val
	endpoint = min(20000.,endpoint_theo)
	beginpoint = max(4800., center - window_length/2*sigma_val)

	range_min = -1.
	range_max = 1.

	if beginpoint == 4800:
		range_min = (4800. - center) / (window_length / 2 * sigma_val)
	if endpoint == 20000:
		range_max = (20000 - center) / (window_length / 2 * sigma_val)

	print(center, deltam, beginpoint, endpoint)
	print("Range:", beginpoint, endpoint, "(",endpoint_theo,") mapped to range",range_min,range_max)

	mass = R.RooRealVar("B_M", "#it{m}(#it{#gamma#gamma})", range_min, range_max)
	# mass = R.RooRealVar("B_M", "#it{m}(#it{#gamma#gamma})x", -1, 1)

	mass.setRange("fitrange_full", range_min, range_max)
	if range_min<-0.3:
		mass.setRange("fitrange_low"+str(center), -1, -0.3)
	else:
		mass.setRange("fitrange_low"+str(center), -0.3, -0.3)
	if range_max>0.3:
		mass.setRange("fitrange_high"+str(center), 0.3, 1)
	else:
		mass.setRange("fitrange_high"+str(center), 0.3, 0.3)

	ds = fillDataSet(tree,mass,endpoint_theo,deltam)


	## Setup model
	cvals = [0,0.,0.,0.,0.,0.,0.]
	if center>8000: cvals[1]=-0.3

	## full wide model coefficients
	c1 = R.RooRealVar("coeff1", "coeff1", cvals[1], -1., 1.)
	c2 = R.RooRealVar("coeff2", "coeff2", 0., -1., 1.)
	c3 = R.RooRealVar("coeff3", "coeff3", 0., -1, 1)
	c4 = R.RooRealVar("coeff4", "coeff4", 0., -1, 1)
	c5 = R.RooRealVar("coeff5", "coeff5", 0., -1, 1)
	c6 = R.RooRealVar("coeff6", "coeff6", 0., -1, 1)
	c7 = R.RooRealVar("coeff7", "coeff7", 0., -1, 1)
	c8 = R.RooRealVar("coeff8", "coeff8", 0., -1, 1)
	c9 = R.RooRealVar("coeff9", "coeff9", 0., -1, 1)
	c_const = R.RooRealVar("coeff_const", "coeff_const", 0.)

	## removing coeff 8
	c1_8 = R.RooRealVar("coeff1_8", "coeff1_8", cvals[1], -1., 1.)
	c2_8 = R.RooRealVar("coeff2_8", "coeff2_8", 0., -1., 1.)
	c3_8 = R.RooRealVar("coeff3_8", "coeff3_8", 0., -1, 1)
	c4_8 = R.RooRealVar("coeff4_8", "coeff4_8", 0., -1, 1)
	c5_8 = R.RooRealVar("coeff5_8", "coeff5_8", 0., -1, 1)
	c6_8 = R.RooRealVar("coeff6_8", "coeff6_8", 0., -1, 1)
	c7_8 = R.RooRealVar("coeff7_8", "coeff7_8", 0., -1, 1)
	c9_8 = R.RooRealVar("coeff9_8", "coeff9_8", 0., -1, 1)

	## removing coeff 8 and 6
	c1_6 = R.RooRealVar("coeff1_6", "coeff1_6", cvals[1], -1., 1.)
	c2_6 = R.RooRealVar("coeff2_6", "coeff2_6", 0., -1., 1.)
	c3_6 = R.RooRealVar("coeff3_6", "coeff3_6", 0., -1, 1)
	c4_6 = R.RooRealVar("coeff4_6", "coeff4_6", 0., -1, 1)
	c5_6 = R.RooRealVar("coeff5_6", "coeff5_6", 0., -1, 1)
	c7_6 = R.RooRealVar("coeff7_6", "coeff7_6", 0., -1, 1)
	c9_6 = R.RooRealVar("coeff9_6", "coeff9_6", 0., -1, 1)

	## removing coeff 8 and 6 and 4
	c1_4 = R.RooRealVar("coeff1_4", "coeff1_4", cvals[1], -1., 1.)
	c2_4 = R.RooRealVar("coeff2_4", "coeff2_4", 0., -1., 1.)
	c3_4 = R.RooRealVar("coeff3_4", "coeff3_4", 0., -1, 1)
	c5_4 = R.RooRealVar("coeff5_4", "coeff5_4", 0., -1, 1)
	c7_4 = R.RooRealVar("coeff7_4", "coeff7_4", 0., -1, 1)
	c9_4 = R.RooRealVar("coeff9_4", "coeff9_4", 0., -1, 1)

	## removing coeff 8 and 6 and 4 and 2
	c1_2 = R.RooRealVar("coeff1_2", "coeff1_2", cvals[1], -1., 1.)
	c3_2 = R.RooRealVar("coeff3_2", "coeff3_2", 0., -1, 1)
	c5_2 = R.RooRealVar("coeff5_2", "coeff5_2", 0., -1, 1)
	c7_2 = R.RooRealVar("coeff7_2", "coeff7_2", 0., -1, 1)
	c9_2 = R.RooRealVar("coeff9_2", "coeff9_2", 0., -1, 1)


	bkg_pdf_8 = R.RooChebychev("sum_cheby_8", "sum_cheby", mass, R.RooArgList(c1,c2,c3,c4,c5,c6,c7,c8,c9))
	bkg_pdf_6 = R.RooChebychev("sum_cheby_6", "sum_cheby", mass, R.RooArgList(c1_8,c2_8,c3_8,c4_8,c5_8,c6_8,c7_8,c_const,c9_8))
	bkg_pdf_4 = R.RooChebychev("sum_cheby_4", "sum_cheby", mass, R.RooArgList(c1_6,c2_6,c3_6,c4_6,c5_6,c_const,c7_6,c_const,c9_6))
	bkg_pdf_2 = R.RooChebychev("sum_cheby_2", "sum_cheby", mass, R.RooArgList(c1_4,c2_4,c3_4,c_const,c5_4,c_const,c7_4,c_const,c9_4))
	bkg_pdf_0 = R.RooChebychev("sum_cheby_0", "sum_cheby", mass, R.RooArgList(c1_2,c_const,c3_2,c_const,c5_2,c_const,c7_2,c_const,c9_2))

	if center>=15000:
		bkg_pdf_2 = R.RooChebychev("sum_cheby_2", "sum_cheby", mass, R.RooArgList(c1,c2,c3))
		bkg_pdf_0 = R.RooChebychev("sum_cheby_0", "sum_cheby", mass, R.RooArgList(c1_2,c_const,c3_2))


	bkgpdfs = R.RooArgList()
	bkgpdfs.add(bkg_pdf_0)
	bkgpdfs.add(bkg_pdf_2)
	if center<15000:
		bkgpdfs.add(bkg_pdf_4)
		bkgpdfs.add(bkg_pdf_6)
		bkgpdfs.add(bkg_pdf_8)

	if range_min > -0.95:
		bkg_pdf_2 = R.RooChebychev("sum_cheby_2", "sum_cheby", mass, R.RooArgList(c1_4,c2_4,c3_4,c_const,c5_4))


	mean_broad = R.RooRealVar("mean_broad", "mean_broad", 1., 0.,10.)
	width_broad = R.RooRealVar("width_broad", "width_broad", 2., 0.5,10.)

	# if range_min>-1.:
	# 	bkg_pdf = R.RooGaussian("gauss_broad", "gauss_broad", mass, mean_broad, width_broad)

	# if range_max<1.0:
	# # 	# c2.setVal(0.)
	# # 	# c2.setConstant()
	# 	bkg_pdf = R.RooChebychev("sum_red_cheby", "Reduced Chebychev", mass, R.RooArgList(c1))

	mu = R.RooRealVar("mu", "mu", 0)
	sigma = R.RooRealVar("sigma", "sigma", sigma_val*2/deltam)
	delta_sigma = R.RooRealVar("delta_sigma", "#Delta#sigma", par.delta_sigma_val[0]*2/deltam)
	sigma_l = R.RooFormulaVar("sigma_l", "@0-@1",R.RooArgList(sigma, delta_sigma))
	sigma_r = R.RooFormulaVar("sigma_r", "@0+@1",R.RooArgList(sigma, delta_sigma))

	alpha_l = R.RooRealVar("alpha_l", "alpha_l", par.sig_alpha_l[0])
	alpha_r = R.RooRealVar("alpha_r", "alpha_r", par.sig_alpha_r[0])
	n_l = R.RooRealVar("n_l", "n_l", par.sig_n_l[0])
	n_r = R.RooRealVar("n_r", "n_r", par.sig_n_r[0])
	cb = R.RooCrystalBall ("cb", "cb", mass, mu, sigma_l, sigma_r, alpha_l, n_l, alpha_r, n_r)

	norm, invnorm = par.get_normalisation(center/par.bias_average[0])

	norm_alp = R.RooRealVar("norm_alp", "norm_alp", norm.n*1000,0.01,4)
	norm_alp_obs = R.RooRealVar("norm_alp_obs", "norm_alp_obs", norm.n*1000)
	norm_alp_err = R.RooRealVar("norm_alp_err", "norm_alp_err", norm.s*1000)
	constr_norm_alp = R.RooGaussian("constr_norm_alp","constr_norm_alp", norm_alp_obs, norm_alp,norm_alp_err)

	constraints = R.RooArgSet(constr_norm_alp)

	nbkg = R.RooRealVar("nbkg", "nbkg", ds.sumEntries(), 0., ds.sumEntries()*5)
	crosssec = R.RooRealVar("crosssec", "#sigma(#it{pp}#rightarrow #it{a}#it{X})#font[32]{B}(#it{a}#rightarrow#gamma#gamma) [pb]", 0, -5*np.sqrt(ds.sumEntries())*1000, 5*np.sqrt(ds.sumEntries())*1000)

	nsig = R.RooFormulaVar("nsig_scaled", "@0*@1", R.RooArgList(crosssec, norm_alp))

	fullpdf_0 = R.RooAddPdf("fullpdf_0", "fullpdf", R.RooArgList(cb,bkg_pdf_0), R.RooArgList(nsig,nbkg))
	fullpdf_2 = R.RooAddPdf("fullpdf_2", "fullpdf", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(nsig,nbkg))
	fullpdf_4 = R.RooAddPdf("fullpdf_4", "fullpdf", R.RooArgList(cb,bkg_pdf_4), R.RooArgList(nsig,nbkg))
	fullpdf_6 = R.RooAddPdf("fullpdf_6", "fullpdf", R.RooArgList(cb,bkg_pdf_6), R.RooArgList(nsig,nbkg))
	fullpdf_8 = R.RooAddPdf("fullpdf_8", "fullpdf", R.RooArgList(cb,bkg_pdf_8), R.RooArgList(nsig,nbkg))


	cat = R.RooCategory("pdf_index","Index of Pdf which is active")
	## Make a RooMultiPdf object. The index corresponds to the order in which pdfs are added, ie for below

	# mypdfs = R.RooArgList()
	# mypdfs.add(fullpdf_0)
	# mypdfs.add(fullpdf_2)

	# if center<15000:
	# 	mypdfs.add(fullpdf_4)
	# 	mypdfs.add(fullpdf_6)
	# 	mypdfs.add(fullpdf_8)


	mypdfs = R.RooArgList()
	mypdfs.add(bkg_pdf_0)
	mypdfs.add(bkg_pdf_2)

	if center<15000:
		mypdfs.add(bkg_pdf_4)
		mypdfs.add(bkg_pdf_6)
		mypdfs.add(bkg_pdf_8)


	multipdf = R.RooMultiPdf("roomultipdf","All Pdfs",cat,mypdfs)
	# By default the multipdf will tell combine to add 0.5 to the nll for each parameter (this is the penalty for the discrete profiling method)
	multipdf.setCorrectionFactor(R.RooMultiPdf.PenatlyScheme.AIC)

	# fullpdf = R.RooAddPdf("fullpdf", "fullpdf", R.RooArgList(cb,multipdf), R.RooArgList(nsig,nbkg))
	fullpdf = R.RooAddPdf("fullpdf", "fullpdf", R.RooArgList(cb,bkg_pdf_2), R.RooArgList(nsig,nbkg))


	# crosssec.setVal(0)
	# crosssec.setConstant()


	# iterator = bkgpdfs.createIterator()
	# pdf = iterator.Next()
	# while pdf :
	# 	pdf.fitTo(ds, R.RooFit.Range("fitrange_full"),R.RooFit.Offset(True))
	# 	pdf = iterator.Next()

	# multipdf = fullpdf_2

	fitresult = fullpdf.fitTo(ds, R.RooFit.Range("fitrange_full"), R.RooFit.Strategy(2), R.RooFit.Offset(True), R.RooFit.Save(), R.RooFit.ExternalConstraints(constraints))

	fitresult.Print("v")

	frame = mass.frame(R.RooFit.Range("fitrange_full"), R.RooFit.Title(str(centermass[-1])+": ["+str(beginpoint)+" - "+str(endpoint)+"]"))
	ds.plotOn(frame, R.RooFit.Name("Data"), R.RooFit.Binning(100))
	fullpdf.plotOn(frame, R.RooFit.Name("Total"))
	# multipdf.plotOn(frame, R.RooFit.Components("sum_cheby"), R.RooFit.LineStyle(R.kDashed))
	cb.plotOn(frame, R.RooFit.Normalization(0.1), R.RooFit.LineColor(R.kRed),R.RooFit.LineStyle(R.kDashed))

	## Draw with pulls
	main_frame_height = 0.75;
	pull_frame_height = 0.25;
	main_pull_ratio = main_frame_height / pull_frame_height;
	num_sigma_pull_range = 5.;
	danger_limit = 2.0;

	m_massbins = round((range_max - range_min)/2*100)
	# m_massbins = 100

	## Create RooPlot object for pulls
	frame_pulls = R.RooPlot(frame.GetXaxis().GetXmin(), frame.GetXaxis().GetXmax());
	frame_pulls.GetXaxis().SetTitle(frame.GetXaxis().GetTitle());

	## Main pad
	pad_main = R.TPad("pad_main", "The main pad", 0.0, 1 - main_frame_height, 1.0, 1.0);
	pad_main.SetBottomMargin(0.02); # Insert a spacing between main frame and pull frame, if spacing is set to True
	pad_main.cd();

	LHCbStyle = R.gROOT.GetStyle("lhcbStyle")
	Label = R.TPaveText(0.65 - LHCbStyle.GetPadRightMargin(),
						0.75 - LHCbStyle.GetPadTopMargin(),
						0.95 - LHCbStyle.GetPadRightMargin(),
						0.95 - LHCbStyle.GetPadTopMargin(),
						"BRNDC")
	if center<10000:
		Label = R.TPaveText(0.15, 0.2, 0.45, 0.4,"BRNDC")
	Label.SetBorderSize(1)
	Label.SetFillColorAlpha(R.kWhite, 0.0)
	Label.SetLineColorAlpha(R.kWhite, 0.0)
	Label.AddText("Center at "+str(round(center))+":")
	Label.AddText("["+str(round(beginpoint))+" - "+str(round(endpoint))+"]")


	## Manipulate and draw pad_main
	# frame->SetMinimum(0.5)
	frame.GetXaxis().SetLabelSize(0.0);
	frame.GetXaxis().SetTitleSize(0.0);       # Remove the x-axis title of the main frame
	frame.Draw();
	Label.Draw()

	## Pull pad
	pad_pulls = R.TPad("pad_pulls", "The pull pad", 0.0, 0.0, 1.0, pull_frame_height);
	pad_pulls.SetTopMargin(0.06);
	pad_pulls.SetBottomMargin(0.4);  # The magic happens here!
	pad_pulls.cd();

	## Manipulate and draw pad_pulls
	hpull = frame.pullHist("Data","Total");
	if not hpull: print("ERROR: No RooCurve found!")

	range_min_x = frame.GetXaxis().GetXmin();
	range_max_x = frame.GetXaxis().GetXmax();

	## Create new TH1 objects for pulls

	pulls_all = R.TH1D("pulls_all", "pulls_all", m_massbins, range_min_x, range_max_x);
	pulls_ok = R.TH1D("pulls_ok", "pulls_ok", m_massbins, range_min_x, range_max_x);
	pulls_danger = R.TH1D("pulls_danger", "pulls_danger", m_massbins, range_min_x, range_max_x);

	pulls_all.SetFillStyle(0);
	pulls_ok.SetFillColor(R.kGray);
	pulls_danger.SetFillColor(R.kRed);
	
	for i in range(0,m_massbins):
		cur_x = hpull.GetPointX(i-1);
		cur_y = hpull.GetPointY(i-1);
		pulls_ok.SetBinContent(i, 0.);
		pulls_danger.SetBinContent(i, 0.);
		# print(i,"(",cur_x,",",cur_y,")")

		# add to total pulls data set
		# pull_var->setVal(cur_y);
		# pull_total->add(*pull_var_set);
		# Needed for plotting pulls_danger
		if abs(cur_y) > num_sigma_pull_range and cur_y >= 0.: cur_y = num_sigma_pull_range;
		elif abs(cur_y) > num_sigma_pull_range and cur_y < 0.: cur_y = -num_sigma_pull_range;

		pulls_all.SetBinContent(i, cur_y);
		if abs(cur_y) > danger_limit: pulls_danger.SetBinContent(i, cur_y);
		else: pulls_ok.SetBinContent(i, cur_y);

	## Configure frame
	frame_pulls.SetMinimum(-num_sigma_pull_range);
	frame_pulls.SetMaximum(num_sigma_pull_range);
	frame_pulls.SetTitle("");

	## scale frame
	frame_pulls.GetXaxis().SetTitleSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetTickSize(0.05);

	frame_pulls.GetYaxis().SetTitleSize(0.072 * main_pull_ratio);
	frame_pulls.GetYaxis().SetTitleOffset(0.95 / main_pull_ratio);
	frame_pulls.GetYaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetYaxis().SetNdivisions(503);

	frame_pulls.GetYaxis().SetTitle("Pull");
	frame_pulls.Draw();

	## Add sigma lines
	zeroline = R.TLine(range_min_x, 0.0, range_max_x, 0.0);

	threesigmaline = R.TLine(range_min_x, 3.0, range_max_x, 3.0);
	threesigmaline.SetLineStyle(R.kDashed);
	threesigmaline.SetLineColor(R.kGray + 2);

	minusthreesigmaline = R.TLine(range_min_x, -3.0, range_max_x, -3.0);
	minusthreesigmaline.SetLineStyle(R.kDashed);
	minusthreesigmaline.SetLineColor(R.kGray + 2);

	## Draw all the stuff
	zeroline.Draw("SAME");
	threesigmaline.Draw("SAME");
	minusthreesigmaline.Draw("SAME");
	pulls_all.Draw("SAME");
	pulls_danger.Draw("SAME");
	pulls_ok.Draw("SAME");

	## Canvas
	canv.cd()
	pad_main.Draw()
	pad_pulls.Draw("SAME");
	canv.Draw()

	canv.SaveAs("individual_fits.pdf")


	obs = R.RooArgSet(mass)
	global_obs = R.RooArgSet(norm_alp_obs)
	constraints=constraints
	parameters = fitresult.floatParsFinal()

	nuisance_pars = R.RooArgSet()
	iterator = parameters.createIterator()
	var = iterator.Next()
	while var :
		if var.GetName()!=crosssec.GetName():
			nuisance_pars.add(var)
		var = iterator.Next()


	wspc = R.RooWorkspace("workspace"+str(round(center)))
	getattr(wspc,"import")(fullpdf)
	getattr(wspc,"import")(ds)
	getattr(wspc,"import")(fitresult, "data_fit_result")
	wspc.defineSet('constraint_set', constraints, True)
	wspc.defineSet('global_observables_set', global_obs, True)
	wspc.defineSet('datasetObservables', obs, True)
	wspc.defineSet('parameters', parameters, True)

	##############
	## modifications for use with RooStats
	##############
	wspc.factory("PROD::constrmodel(fullpdf,constr_norm_alp)")
	mc = R.RooStats.ModelConfig("modelconf",wspc)
	mc.SetPdf(wspc.pdf("constrmodel"))
	mc.SetObservables(obs)
	mc.SetParametersOfInterest(R.RooArgSet(crosssec))
	mc.SetNuisanceParameters(nuisance_pars)
	mc.SetGlobalObservables(global_obs)
	mc.SetSnapshot(R.RooArgSet(crosssec))

	## copy for background model
	bModel = mc.Clone()
	bModel.SetName(mc.GetName()+"_with_poi_0")
	var = bModel.GetParametersOfInterest().first()
	oldval = var.getVal()
	var.setVal(0)
	var.setConstant(True)
	print("bModel ",bModel)
	bModel.GetPdf().fitTo(ds)
	var.setConstant(False)
	bModel.SetSnapshot(R.RooArgSet(var))
	var.setVal(oldval)
	bModel.Print()
	mc.Print()
	ds.Print();
	getattr(wspc,"import")(mc)
	getattr(wspc,"import")(bModel)

	wspc.SaveAs("../../workspace"+str(round(center))+".root", "RECREATE")


	yields.append(crosssec.getVal())
	yielderrors.append(crosssec.getError())
	bkgyields.append(nbkg.getVal())
	bkgyielderrors.append(nbkg.getError())


canv.SaveAs("individual_fits.pdf]")

print("yields:", yields)
print("errors:", yielderrors)
print("bkgyields:", bkgyields)
print("bkgerrors:", bkgyielderrors)


fig = plt.figure()
plt.errorbar(centermass,yields,yerr=yielderrors, label="$\sigma\\times\mathcal{B}(a\\to\gamma\gamma)$", ms=50)
plt.hlines(0,4800,20000, color="red", linestyle="dashed")
plt.ylabel("$\sigma\\times\mathcal{B}(a\\to\gamma\gamma)$")
plt.xlabel("$m_{\gamma\gamma}$ [MeV/$\it{c}^2$]")
plt.ylim([-1000,1000])
plt.savefig("bump_scan_signalyields.pdf")
plt.clf()
plt.errorbar(centermass,bkgyields,yerr=bkgyielderrors, label="Background yield", ms=10)
plt.ylabel("$N_{bkg}$")
plt.xlabel("$m_{\gamma\gamma}$ [MeV/$\it{c}^2$]")
plt.savefig("bump_scan_yields.pdf")
