import numpy as np
import os.path
masses = [4800]
sigma = 0.021 / 1.1
while masses[-1] < 19500:
    masses.append(masses[-1] * (1 + 0.5*sigma))
out_str =''
for m in masses:
    fname = f'output/{m}'
    if not os.path.isfile(fname): continue
    with open(fname,'r') as file:
        out_str +=f'{file.readlines()[0]}\n'
with open('limits.csv','w') as file:
    file.write(out_str)
