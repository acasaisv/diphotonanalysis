from importlib import reload
import plot_helpers
import numpy as np
import matplotlib.pyplot as plt

import uproot3 as uproot
import pickle
import xgboost as xgb
from fit_helpers import create_double_cb, create_gauss
from helpers import sim10_masses_map
import zfit
print("open events")
file = uproot.open(
    "/scratch47/adrian.casais/ntuples/background/Stripping34-2022.root")
events = file['DecayTree/DecayTree']

print("make data frame")
variables_BDT = [
    "B_B_CONEMULT_1_0",
    "B_B_CONEP_1_0",
    "B_B_CONEPASYM_1_0",
    "B_B_CONEPT_1_0",
    "B_B_CONEPTASYM_1_0",
    "B_B_CONEMULT_1_35",
    "B_B_CONEP_1_35",
    "B_B_CONEPASYM_1_35",
    "B_B_CONEPT_1_35",
    "B_B_CONEPTASYM_1_35",
    "B_B_CONEMULT_1_7",
    "B_B_CONEP_1_7",
    "B_B_CONEPASYM_1_7",
    "B_B_CONEPT_1_7",
    "B_B_CONEPTASYM_1_7"
]
additional_vars = ['B_L0ElectronDecision_TOS', 'B_L0PhotonDecision_TOS', 'B_Hlt1B2GammaGammaDecision_TOS', 'B_Hlt1B2GammaGammaHighMassDecision_TOS',
                   'B_Hlt2RadiativeB2GammaGammaDecision_TOS', 'Gamma1_PT', 'Gamma2_PT', 'Gamma1_PP_Saturation', 'Gamma2_PP_Saturation', 'Gamma1_PP_IsPhoton', 'Gamma2_PP_IsPhoton', 'B_M']
variables = variables_BDT + additional_vars
df = events.pandas.df(branches=variables)

print("cut saturation")
cuts = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS and Gamma1_PT > 3000 and Gamma2_PT > 3000 and Gamma1_PP_Saturation<1 and Gamma2_PP_Saturation<1 and Gamma1_PP_IsPhoton>0.85 and Gamma2_PP_IsPhoton>0.85'
df = df.query(cuts)

df['MinPT'] = np.minimum(df['Gamma1_PT'], df['Gamma2_PT'])
df['MaxPT'] = np.maximum(df['Gamma1_PT'], df['Gamma2_PT'])

print("size", df.shape[0])
print("read the bdt")
with open('../../CalculateEFficiencies/bdt.pickle', 'rb') as handle:
    bdt = pickle.load(handle)

with open('../../CalculateEFficiencies/bdt_Bs.pickle', 'rb') as handle:
    bdt_Bs = pickle.load(handle)

# with open('/home3/adrian.casais/diphotonanalysis/ConeVarsBDT/bdt.pickle','rb') as handle:
#   bdt = pickle.load(handle)

print("predict bdt")
print(variables_BDT)
variables_BDT_Bs = variables_BDT+['MinPT', 'MaxPT']

df['bdt'] = np.mean([fold.predict_proba(df[variables_BDT])[:, 1]
                    for fold in bdt], axis=0)
df['bdt_Bs'] = np.mean([fold.predict_proba(df[variables_BDT_Bs])[
                       :, 1] for fold in bdt_Bs], axis=0)

plt.hist(df["B_M"], 100)
plt.savefig("test_data.png")

df = df.query('bdt>0.9')
# df_Bs = df.query('bdt_Bs>0.85')
m1g, m2g = 4800, 20050
BsMassRange = zfit.Space('B_M', (m1g, m2g))
obs = BsMassRange
name_prefix = 'bkg'
sigma = [2500, 1000, 5000]
BsCBExtended, BsParameters = create_double_cb(name_prefix=name_prefix,
                                              mass_range=obs,
                                              mass=6500,
                                              sigma=sigma,
                                              nevs=len(df),
                                              dsigma=[0, -1000, 1000],
                                              scale=(1.2, 0.7, 1.3),
                                              extended=True
                                              # nl=3.4,
                                              # al=1.57,
                                              # ar=1.82,
                                              # nr=12.16
                                              )
nBkgComb = zfit.Parameter('nBkg', len(df)/2, 0, len(df)*1.01)
orders = []
for i in range(10):
    if i % 2:
        orders.append(zfit.Parameter(name_prefix+str(i),
                      0.45, -20, 20, floating=True))
    else:
        orders.append(zfit.Parameter(name_prefix+str(i), 0, floating=False))
BsBkgCombExtended = zfit.pdf.Legendre(obs, orders).create_extended(nBkgComb)
model_Bs = zfit.pdf.SumPDF([BsCBExtended, BsBkgCombExtended])
model_Bs = BsCBExtended
data = zfit.Data.from_numpy(array=df['B_M'].to_numpy(), obs=obs)
nll = zfit.loss.ExtendedUnbinnedNLL(model=model_Bs, data=data)
# nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)
minimizer = zfit.minimize.Minuit(tol=1e-5, mode=1, gradient=True,
                                 maxiter=7000000)
result = minimizer.minimize(nll)
result.hesse()
# result.error()
print(result)
# plot= True
reload(plot_helpers)
nSig = zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=model_Bs,
                      components=[BsCBExtended,
                                  # BsBkgCombExtended
                                  ],
                      yields=[nSig,
                              # nBkgComb
                              ],
                      labels=[r'$B_s$ signal double Crystal Ball',
                              'Background'
                              ],
                      colors=['blue',
                              # 'red'
                              ],
                      nbins=100,
                      myrange=(m1g, m2g),
                      xlabel=r'M($\gamma\gamma$) [MeV]',
                      savefile=f'./bkg_fit.pdf',
                      plot=False,
                      pull_plot=True)
print(BsCBExtended.to_json())
integral1 = float(BsCBExtended.integrate(limits=(4000, 20000)))
integral2=float(BsCBExtended.integrate(limits=(4800,20000)))
sample = BsCBExtended.sample(nSig*integral2/integral1/0.04,limits=(4000,20000)).to_pandas()
dump = np.array(sample['B_M']).dump('dummy_data.np')
