import numpy as np
import matplotlib.pyplot as plt

import matplotlib.pyplot as plt

def generate_pascals_triangle(rows):
    triangle = [[1]]
    for _ in range(1, rows):
        row = [1]
        last_row = triangle[-1]
        for j in range(len(last_row) - 1):
            row.append(last_row[j] + last_row[j + 1])
        row.append(1)
        triangle.append(row)
    return triangle

def get_coordinates(triangle):
    coordinates = []
    for i, row in enumerate(triangle):
        for j, value in enumerate(row):
            x = j - len(row) / 2
            y = i
            coordinates.append((x, y, value))
    return coordinates

def plot_pascals_triangle(triangle):
    coordinates = get_coordinates(triangle)
    x_vals = [coord[0] for coord in coordinates]
    y_vals = [coord[1] for coord in coordinates]
    values = [coord[2] for coord in coordinates]

    plt.figure(figsize=(10, 10))
    plt.scatter(x_vals, y_vals, c=values, cmap='viridis', s=100)
    
    for (x, y, value) in coordinates:
        plt.text(x, y, str(value), fontsize=12, ha='center', va='center')

    plt.title("Pascal's Triangle")
    plt.axis('off')
    plt.savefig('pascal')

if __name__ == "__main__":
    rows = 5  # You can change the number of rows here
    triangle = generate_pascals_triangle(rows)
    coordinates = get_coordinates(triangle)
    x_vals = [coord[0] for coord in coordinates]
    y_vals = [coord[1] for coord in coordinates]
    print(np.array(coordinates))
    plot_pascals_triangle(triangle)
    x = np.linspace(-16,16,1000)
    rng = np.random.default_rng()
    plt.clf()
    plt.xlim(-7.6,7.6)
    plt.ylim(0,12)
    a= 1
    y_envel = np.inf*np.ones(len(x))
    for h,k in zip(x_vals,y_vals):
        print(h,k)
        y = a*(x - h)**2 + k
        plt.plot(x, y,linewidth=1)
        mask = y- y_envel
        mask = mask < 0
        y_envel[mask] = y[mask]
    plt.plot(x,y_envel,linewidth=2,color='black')
    plt.savefig('myparabola.png')

    