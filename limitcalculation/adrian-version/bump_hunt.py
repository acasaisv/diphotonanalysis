from importlib import reload
import plot_helpers
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import zfit
from fit_helpers import create_double_cb
from hepstats.hypotests.parameters import POI, POIarray
from hepstats.hypotests import UpperLimit
from hepstats.hypotests.calculators import AsymptoticCalculator, FrequentistCalculator
from uncertainties import ufloat as ul
from plot_helpers import plotlimit
import sys
eff_grid = {
    5000: ul(0.051, 0.007),
    6000: ul(0.099, 0.013),
    7000: ul(0.118, 0.017),
    8000: ul(0.107, 0.014),
    9000: ul(0.095, 0.012),
    10000: ul(0.081, 0.010),
    11000: ul(0.083, 0.011),
    13000: ul(0.084, 0.011),
    15000: ul(0.089, 0.013),
    17000: ul(0.071, 0.013),
    19000: ul(0.046, 0.006),
    20000: ul(0.021, 0.004)
}


def get_eff(mass, eff_grid):
    masses = [mass for mass in eff_grid.keys()]
    print(masses)
    masses = np.sort(masses)
    c_grid = [eff_grid[i].n for i in masses]
    s_grid = [eff_grid[i].s for i in masses]
    eff = np.interp(mass, masses, c_grid)
    u_eff = np.interp(mass, masses, s_grid)
    return eff / 100, u_eff / 100


def legendre_moment(df, window, order, nbins=50):
    mass = df.query(f'B_M > {window[0]} and B_M < {window[1]}')['B_M']
    h, bins = np.histogram(mass, nbins)
    c = (bins[:-1] + bins[1:]) / 2
    coefs = np.zeros(order+1)
    coefs[order] = 1
    leg = np.polynomial.legendre.Legendre(coefs, window)
    print(leg)
    prod = leg(c)*h
    moment = (2*order+1)/(window[1]-window[0])*np.sum(prod)*(c[1]-c[0])
    print("moment is", moment)
    return moment


center = 18000
n_inject = 2070*1000*get_eff(center, eff_grid)[0]
n_inject = 0
peak = np.random.normal(center, center*0.022/1.1, int(n_inject))
bkg = np.load('dummy_data.np', allow_pickle=True)
# bkg = np.append(bkg,peak)
bkg = {'B_M': bkg}
df = pd.DataFrame(data=bkg)
masses = [4800]
sigma = 0.021 / 1.1
while masses[-1] < 19500:
    masses.append(masses[-1] * (1 + 0.5*sigma))
print(len(masses))
# 6 sigma around (?)

index = 0
if len(sys.argv) > 1:
    index = int(sys.argv[1])
center = masses[index]
# center = 5357/1.005
print("$$$$$$$$$$$$$$$$$$$$$")
print(center)
print("$$$$$$$$$$$$$$$$$$$$$")
m1g, m2g = center * (1 - 5 * sigma), center * (1 + 5 * sigma)
m1t, m2t = m1g, m2g
if m1g < 4800:
    m1t = 4800
if m2g > 20000:
    m2t = 20000
df_below = df.query('B_M < 4800')
df_below = df_below.sample(frac=0.3)
df = df.query('B_M >= 4800 and B_M < 20000')
n_window = df.query(f"B_M >= {m1g} and B_M < {m2g}").shape[0]
n_in_tail = df_below.query(f"B_M > {m1g} and B_M < 4800").shape[0]
#######################
#############
print("$$$$$$$$$$$$$$$$$$")
print(m1g, m2g)
print("$$$$$$$$$$$$$$$$$$")
# obs = zfit.Space('B_M', (m1g, m2g))
obs = zfit.Space('B_M', (m1t, m2t))
m1b, m2b = m1g, center*(1-2*sigma)
m1b, m2b = m1g, 4800
if m1g <= 4800:
    obs_below = zfit.Space('B_M', (m1b, m2b))
else:
    obs_below = zfit.Space('B_M', (m1g, m2g))
name_prefix = 'signal_'
parameters = {}
parameters['scale_m'] = zfit.Parameter(
    name_prefix + 'scale_m', 1, floating=False)
parameters['sigma'] = zfit.Parameter(
    name_prefix + 'sigma', center * sigma, floating=False)
parameters['dSigma'] = zfit.Parameter(
    name_prefix + 'dsigma', 12, floating=False)
parameters['sigmaR'] = zfit.ComposedParameter(
    name_prefix + 'sigmaR', lambda x, y: x + y, params=[parameters['sigma'], parameters['dSigma']])
parameters['sigmaL'] = zfit.ComposedParameter(
    name_prefix + 'sigmaL', lambda x, y: x - y, params=[parameters['sigma'], parameters['dSigma']])
parameters['m_PDG'] = zfit.Parameter(
    name_prefix + 'm_PDG', center, floating=False)
parameters['m'] = zfit.ComposedParameter(
    name_prefix + 'm', lambda x, y: x * y, params=[parameters['scale_m'], parameters['m_PDG']])

parameters['a_l'] = zfit.Parameter(name_prefix + 'a_l', 1.9, floating=False)
parameters['n_l'] = zfit.Parameter(name_prefix + 'n_l', 1.5, floating=False)
parameters['a_r'] = zfit.Parameter(name_prefix + 'a_r', 1.8, floating=False)
parameters['n_r'] = zfit.Parameter(name_prefix + 'n_r', 20, floating=False)

CB = zfit.pdf.GeneralizedCB(mu=parameters['m'],
                            sigmar=parameters['sigmaR'],
                            sigmal=parameters['sigmaL'],
                            alphar=parameters['a_r'],
                            nr=parameters['n_r'],
                            alphal=parameters['a_l'],
                            nl=parameters['n_l'],
                            obs=obs)

nTot = zfit.Parameter(name_prefix + 'Ntot', 1000, 0,
                      1000 * len(df), floating=True)
xsec = zfit.Parameter('sigma', 0, -10e3, 10e3, floating=True)
lumi = zfit.Parameter('lumi', 2.07 * 1000, 1 * 1000, 3 * 1000, floating=True)
lumi_c = zfit.constraint.GaussianConstraint(
    lumi, observation=2.07 * 1000, uncertainty=0.05 * 1000)
eff, u_eff = get_eff(center, eff_grid)
# eff,u_eff = 0.034/100, 0.004/100
par_eff = zfit.Parameter(name_prefix + 'eff', eff,
                         eff * 0.1, eff * 2, floating=True)
constrain_eff = zfit.constraint.GaussianConstraint(
    par_eff, observation=eff, sigma=u_eff)
nSig = zfit.ComposedParameter(
    'nSig', lambda x, y, z: x * y * z, params=[lumi, xsec, par_eff])
# nSig = zfit.Parameter('nSig',len(df)/2,0,len(df)*1.01)
BsCBExtended = CB.create_extended(nSig)
nBkgComb = zfit.Parameter('nBkg', n_window*0.01, 000, n_window * 1.5)
nBkgCB = zfit.Parameter('nBkgCB', n_window, 0, n_window * 1.5)
nBkgLeg = zfit.Parameter('nBkgLeg', n_window, 0, n_window * 1.5)


orders = []
n_unknown = 8
moments = []
mask = [not i % 2 for i in range(n_unknown)]
mask[1] = True
mask[3] = False
mask[5] = False
for o in range(n_unknown+1):
    moments.append((legendre_moment(df, (m1t, m2t), o)))
for i in range(n_unknown):
    if mask[i]:  # or i ==5:# or i ==7:
        orders.append(zfit.Parameter(name_prefix + str(i+1),
                                     moments[i+1],
                                     # 0,
                                     moments[i+1]*(
            1-np.sign(moments[i+1])*100), moments[i+1]*(1+np.sign(moments[i+1])*100), floating=True))
    else:
        orders.append(zfit.Parameter(
            name_prefix + str(i+1), 0, floating=False))
n_unkown = 4
BkgCB = zfit.pdf.GeneralizedCB(mu=zfit.Parameter('mubkg', 0.93 * 6500, floating=False),
                               sigmar=zfit.Parameter(
    'bkgsimgmar', 1515.01 + 294.793, floating=False),
    sigmal=zfit.Parameter(
    'bkgsimgmal', 1515.01 - 294.793, floating=False),
    alphar=zfit.Parameter(
    'bkgalphar', 1.79246, floating=False),
    nr=zfit.Parameter(
    'bkgnr', 8.00093, floating=False),
    alphal=zfit.Parameter(
    'bkgal', 1.07907, floating=False),
    nl=zfit.Parameter(
    'bkgnl', 21.0127, floating=False), obs=obs)
BkgCB.set_yield(nBkgCB)
leg_space = zfit.Space('space', m1g, m2g)
BkgLegendre = zfit.pdf.Legendre(
    obs, orders, coeff0=moments[0], integration_limits=leg_space, apply_scaling=True)
BkgLegendre.set_yield(nBkgLeg)
# fracBkg = zfit.Parameter('BkgFrac', 0.5, 0, 1)
# fullBkg = zfit.pdf.SumPDF([BkgLegendre, BkgCB], fracs=fracBkg)
# fullBkg.set_yield(nBkg1)

model_Bs = zfit.pdf.SumPDF([
    BsCBExtended,
    BkgLegendre,
    # BkgCB
])
# model_Bs = BsBkgCombExtended
data = zfit.Data.from_numpy(array=df['B_M'].to_numpy(), obs=obs)

nll = zfit.loss.ExtendedUnbinnedNLL(
    model=model_Bs, data=data, constraints=[constrain_eff, lumi_c])
nll_tot = nll
asimov_bins = [1000]
if m1g <= 4800:
    CBTail = zfit.pdf.GeneralizedCB(mu=parameters['m'],
                                    sigmar=parameters['sigmaR'],
                                    sigmal=parameters['sigmaL'],
                                    alphar=parameters['a_r'],
                                    nr=parameters['n_r'],
                                    alphal=parameters['a_l'],
                                    nl=parameters['n_l'],
                                    obs=obs_below)
    # nSigTail = zfit.Parameter('n_in_tail', n_in_tail,
    #                           0, 1.5 * n_in_tail, floating=True)
    prescale = zfit.Parameter('prescale', 0.3, floating=False)
    val = float(CB.numeric_integrate(limits=(m1g, 4800), norm=False)) / \
        float(CB.numeric_integrate(limits=(4800, m2g), norm=False))
    print(val)
    integral = CB.numeric_integrate(limits=(4800, m2g), norm=False)
    integral2 = CB.numeric_integrate(limits=(m1g, 4800), norm=False)
    print(f'integral 4800,{m2g}', integral)
    print(f'integral {m1g},4800', integral2)
    frac_window = zfit.Parameter('frac', float(val), floating=False)
    nSigTail = zfit.ComposedParameter(
        'nSigTail', lambda x, y, z: x * y * z, params=[nSig, prescale, frac_window])
    CBTail.set_yield(nSigTail)
    BkgCBTail = zfit.pdf.GeneralizedCB(mu=zfit.Parameter('mubkgTail', 0.93 * 6500, floating=False),
                                       sigmar=zfit.Parameter(
        'bkgsimgmarTail', 1515.01 + 294.793, floating=False),
        sigmal=zfit.Parameter(
        'bkgsimgmalTail', 1515.01 - 294.793, floating=False),
        alphar=zfit.Parameter(
        'bkgalpharTail', 1.79246, floating=False),
        nr=zfit.Parameter(
        'bkgnrTail', 8.00093, floating=False),
        alphal=zfit.Parameter(
        'bkgalTail', 1.07907, floating=False),
        nl=zfit.Parameter(
        'bkgnlTail', 21.0127, floating=False),
        obs=obs_below)
    overall_mult = zfit.Parameter('overall', 1, 0, 10, floating=True)
    nBkgTail = zfit.ComposedParameter(
        'nBkgTail', lambda x, y, z: x * y * z, params=[nBkgCB, prescale, overall_mult])
    BkgCBTail.set_yield(nBkgTail)
    tail_legendre = zfit.pdf.Legendre(
        obs_below, orders, coeff0=moments[0], integration_limits=leg_space, apply_scaling=True)
    nTailLeg = zfit.ComposedParameter(
        'nTailLeg', lambda x, y, z: x * y * z, params=[nBkgLeg, prescale, overall_mult])
    tail_legendre.set_yield(nTailLeg)
    data_below = zfit.Data.from_numpy(
        array=df_below['B_M'].to_numpy(), obs=obs_below)
    model_tail = zfit.pdf.SumPDF([CBTail,
                                  tail_legendre,
                                  # BkgCBTail
                                  ])
    nll_below = zfit.loss.ExtendedUnbinnedNLL(
        model=model_tail, data=data_below, constraints=[constrain_eff, lumi_c])
    nll_tot = nll_below + nll
    asimov_bins = [1000, 1000]

# nll = zfit.loss.PenalizedExtendedNLL(model=model_Bs, n_unknown=n_unknown, data=data, constraints=[constrain_eff, lumi_c])
#
# minimizer = zfit.minimize.Minuit(
#      tol=1e-3, mode=0, gradient=True, maxiter=7000000)
# result = minimizer.minimize(nll)

# print(result.fminopt)
minimizer = zfit.minimize.Minuit(
    tol=1e-3, mode=0, gradient=True, maxiter=7000000)
result = minimizer.minimize(nll_tot)
print(dir(result))
result.hesse()
# result.error()
print(result)
for i in range(n_unknown-1):
    print(i+1, f" {100.*(orders[i].value()-moments[i+1])/moments[i+1]}",
          f" {100*moments[i+1]/moments[1]}")
reload(plot_helpers)
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=model_Bs,
                      components=[
    BsCBExtended,
    BkgLegendre,
    # BkgCB
],
    yields=[
    nSig,
    nBkgLeg,
    # nBkgCB
],
    labels=[
    r'Signal',
    'Background Legendre',
    # 'Background2'
],
    colors=[
    'green',
    'blue',
    # 'red'
],
    nbins=50,
    myrange=(m1t, m2t),
    xlabel=r'M($\gamma\gamma$) [MeV]',
    savefile=f'output/hunt/{center}.pdf',
    plot=False,
    pull_plot=True)
if m1g <= 4800:
    # if False:
    plot_helpers.plot_fit(mass=df_below['B_M'],
                          full_model=model_tail,
                          components=[
        CBTail,
                              tail_legendre,
                              # BkgCBTail,
    ],
        yields=[
        nSigTail,
        nTailLeg,
        #  nBkgTail,
    ],
        labels=[
            'Signal prescaled',
        'Background Legendre',
        # 'Background CB'
    ],
        colors=[
            'green',
        'blue',
        # 'red',
    ],
        nbins=50,
        myrange=(m1b, m2b),
        xlabel=r'M($\gamma\gamma$) [MeV]',
        savefile=f'output/hunt_tail/{center}.pdf',
        plot=False,
        pull_plot=True)
calculator = AsymptoticCalculator(nll_tot, minimizer, asimov_bins=asimov_bins)
# calculator = FrequentistCalculator(nll, minimizer,ntoysnull=10,ntoysalt=10)

if center >= 4800 and center <= 6000:
    myrange = np.linspace(00, 2500, 50)
elif center > 6000 and center <= 10000:
    myrange = np.linspace(0, 1500, 50)
elif center > 10000 and center <= 12000:
    myrange = np.linspace(0, 500, 50)
elif center > 12000 and center <= 15000:
    myrange = np.linspace(0, 350, 50)
else:
    myrange = np.linspace(0, 350, 50)
poinull = POIarray(xsec, myrange)
poialt = POI(xsec, 0)
ul = UpperLimit(calculator, poinull, poialt, qtilde=False)
limits = ul.upperlimit(alpha=0.05, CLs=True)
res_string = f"{center},{limits['observed']},{limits['expected_m2']},{limits['expected_m1']},{limits['expected']},{limits['expected_p1']},{limits['expected_p2']}"
with open(f'output/{center}', 'w') as file:
    file.write(res_string)
print(limits)
plt.clf()
f = plt.figure(figsize=(9, 8))
plotlimit(ul, alpha=0.05, CLs=True)
plt.savefig(f'output/brazil/{center}.png')
# print((dir(ul)))
# df.query('m ')
