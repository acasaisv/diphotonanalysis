from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop#, CombineParticles
from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

from CommonParticles import StdAllLooseGammaConversion
from CommonParticles import StdLooseAllPhotons

from Configurables import ( DiElectronMaker, ProtoParticleCALOFilter,
                            ParticleTransporter, BremAdder )


#
## Cuts dictionary
# 

cuts =              { 'gammaPT'             : 1000    # MeV/c
                     ,'gammaP'              : 6000   # MeV/c
                     ,'gammaCL'             : 0.3     # adimensional
                     ,'gammaNonePT'         : 1100    # MeV/c
                     ,'gammaNoneP'          : 6000   # MeV/c
                     ,'gammaNoneCL'         : 0.3    # adimensional
                     ,'NoConvHCAL2ECAL'     : 0.1   # adimensional
                     ,'BsPT'                : 2000    # MeV/c
                     ,'BsVertexCHI2pDOF'    : 20      # adimensional
                     ,'BsLowMass'           : 4300    # MeV/cc
                     ,'BsNonePT'            : 2000    # MeV/c
                     ,'BsLowMassDouble'     : 4000    # MeV/cc
                     ,'BsLowMassNone'       : 4800    # MeV/cc
                     ,'BsLowMassNoneWide'   : 0       # MeV/cc
                     ,'BsHighMass'          : 20000    # MeV/cc
                     ,'BsHighMassNone'      : 20000    # MeV/cc
                     ,'BsHighMassNoneWide'  : 4800   # MeV/cc
                     ,'scaleWide'           : 1.0
                     ,'HLT1None'            : "HLT_PASS_RE('Hlt1.*GammaGammaDecision')"
                     ,'HLT2None'            : "HLT_PASS_RE('Hlt2.*GammaGammaDecision')"
                    }


#
## Setting up clean photon selection
#


name = "Bs2gg"

fltrCode_nonConv = "(PT>%(gammaPT)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s)" % cuts
_trkFilterNonConv = FilterDesktop( Code = fltrCode_nonConv )

stdPhotons     = DataOnDemand(Location='Phys/StdLooseAllPhotons/Particles')


# Clean up the non-converted photons to reduce the timing
stdPhotons_clean = Selection( 'PhotonFilter' + name, Algorithm = _trkFilterNonConv, RequiredSelections = [stdPhotons])


#
## Setting up selection
#

def TOSFilter( name = None, sel = None, trigger = None ):
    from Configurables import TisTosParticleTagger
    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = { trigger+"%TOS" : 0 }
    from PhysSelPython.Wrappers import Selection
    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = sel, Algorithm = _filter )
    return _sel



#can this flag be configured externally ?
wide = False

BsGG_DC_none = "(PT>%(gammaNonePT)s*MeV) & (P>%(gammaNoneP)s*MeV) & (CL>%(gammaNoneCL)s)" % cuts
if wide == True:
    BsGG_CC_none = "(in_range( ( %(BsLowMassNoneWide)s )*MeV, AM, ( %(BsHighMassNoneWide)s  )*MeV) )" % cuts
    BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range( ( %(BsLowMassNoneWide)s )*MeV, M, ( %(BsHighMassNoneWide)s )*MeV) )" % cuts
else:
    BsGG_CC_none = "(in_range(%(BsLowMassNone)s*MeV, AM, %(BsHighMassNone)s*MeV))" % cuts
    BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range(%(BsLowMassNone)s*MeV, M, %(BsHighMassNone)s*MeV))" % cuts

suffix=''
if wide == True:
    scaleWide = config['scaleWide']
    suffix='wide'
else:
    scaleWide = 1.0

_Bs2gammagamma_none = CombineParticles(name = "CombineParticles_BsGG_none",
                                       DecayDescriptor = "B_s0 -> gamma gamma"
                                       , DaughtersCuts  = {'gamma' : BsGG_DC_none}
                                       , CombinationCut = BsGG_CC_none
                                       , MotherCut      = BsGG_MC_none)
_Bs2gammagamma_none.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})

Bs2gammagamma_none = Selection(
    name+"_none",
    Algorithm = _Bs2gammagamma_none,
    RequiredSelections = [stdPhotons_clean])


topselL0 = TOSFilter(name+"_NoConvTOSLineL0",[Bs2gammagamma_none],"L0(Photon|Electron)Decision")
topselHLT1 = TOSFilter(name+"_NoConvTOSLineHLT1",[topselL0],"Hlt1(B2GammaGamma|B2GammaGammaHighMass)Decision")
topselHLT2 = TOSFilter(name+"_NoConvTOSLineHLT2",[topselHLT1],"Hlt2(RadiativeB2GammaGamma)Decision")

#sel = TOSFilter("dummysel",[Bs2gammagamma_none],"Hlt1B2GammaGammaHighMassDecision")

#
## Setting up streams
#
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingLine import StrippingLine
from StrippingConf.StrippingStream import StrippingStream

sl = StrippingLine(name+"_NoConvLine",
                   prescale = 1,
                   postscale = 1,
                   selection = Bs2gammagamma_none,
                   #selection = sel,
                   MDSTFlag = False,
                   EnableFlavourTagging = False,
                   )


mystream = StrippingStream(name="Strip", Lines = [ sl ] )
sc = StrippingConf( Streams = [ mystream ],
                    MaxCandidates = -1,
                    TESPrefix = 'Strip'
                    )

mystream.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines



#
## Write DST
#
enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
    }



dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = "Filtered",
			  SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41452811)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
## DaVinci Configuration
#
from Configurables import DaVinci

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60



############ SHIT
from GaudiConf import *
import GaudiPython


DaVinci().DataType = "2016"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
DaVinci().InputType = "DST" # if running on stripped data... changz






#################################################################################
DaVinci.Input= [ '/scratch03/adrian.casais/diphotonanalysis/JetIsolation/MC2016_Priv_MagDown_ALP5_35.ldst' ]
#DaVinci.Input = ["Bs2gg.Bs2gg.dst"]
#DaVinci.Input = [ "/scratch03/adrian.casais/DiPhotons/rerunTrigger/tmp/Bs_hlt1.dst" ] 
gaudi = GaudiPython.AppMgr()
gaudi.initialize()

TES = gaudi.evtsvc()
#gaudi.run(1)


counter = 0
counter_ev = 0
for i in range(1000):
#while counter_ev <= 1000:
    gaudi.run(1)
    if TES['/Event/Phys/Bs2gg_NoConvLine/Particles']  and TES['/Event/Phys/Bs2gg_NoConvLine/Particles'].size():
        counter+= TES['/Event/Phys/Bs2gg_NoConvLine/Particles'].size()
        counter_ev +=1
    
    
    
print counter
gaudi.stop()
gaudi.finalize()
