from ROOT import *
gROOT.ProcessLine(".x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C")

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/'
root = '/scratch47/adrian.casais/ntuples/background/'
#f = TFile(root + 'hardPhoton.root')
#f=TFile(root+'dimuon2018.root')
#f=TFile(root+'JpsiMeson2016.root')
#f=TFile(root+'inclJpsi.root')
#f = TFile(root+'BJpsiK.root')
#f = TFile(root+'hardPhoton-withcones.root')
f = TFile(root+'hardPhoton_52-big.root')
#f=TFile(root+'inclJpsiMeson.root')
#f=TFile(root+'JpsiMeson3.root')

t = f.Get('B2GammaGammaTuple/DecayTree')
#t = TChain('B2GammaGammaTuple/DecayTree')
#t.Add(root+'/inclJpsi.root');t.Add(root+'/JpsiMeson3.root')

# f = TFile(root + '/Data2016_S28_Radiative_withConv.root')
# t = f.Get('DTTBs2gg_None/DecayTree')

f0 = TFile(root + '/Stripping34.root')
t0 = f0.Get('DecayTree/DecayTree')


mlow = 4850
mhigh = 20000
nbins = 25
hL0 = TH1F("L0","L0",nbins,mlow,mhigh)
hL1 = TH1F("L1","L1",nbins,mlow,mhigh)
hL2 = TH1F("L2","L2",nbins,mlow,mhigh)
hL2Strip = TH1F("L2Stripping","L2Stripping",nbins,mlow,mhigh)

# t.Project("L0","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
# t.Project("L1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")
t.Project("L2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)"\
          "&& B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS && !gamma_CaloHypo_Saturation && !gamma0_CaloHypo_Saturation")
# t_ = t.CopyTree("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)"\
#           "& B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS && !gamma_CaloHypo_Saturation && !gamma0_CaloHypo_Saturation")

#t.Project('L2','A1_M',)
t0.Project('L2Stripping','B_M'
           ,'!Gamma1_CaloHypo_Saturation && !Gamma2_CaloHypo_Saturation'
           )
#tHLT2.Project("L2","A1_M")
c=TCanvas()
#
hL2.GetXaxis().SetTitle('Mass [MeV/c^{2}]')
hL2.GetYaxis().SetTitle('Normalized entries/258.3 MeV/c^{2}')
#hL0.GetXaxis().SetRangeUser(0,0.3)
#hL0.SetLineColor(kRed);hL0.DrawNormalized("E")
#hL1.SetLineColor(kBlue);hL1.DrawNormalized("E same")

hL2.SetLineColor(kRed);hL2.DrawNormalized('E')
hL2Strip.SetLineColor(kBlack);hL2Strip.DrawNormalized("E same")

leg=TLegend(.3,.3,.3,.3)
#leg.AddEntry(hL0,"L0","L")
#leg.AddEntry(hL1,"HLT1","L")
leg.AddEntry(hL2,"HLT2 hardPhoton52 MC","L")
leg.AddEntry(hL2Strip,"HLT2 4/100 unblinded Stripping Line","L")

leg.Draw()

# nbinsxy=30
# limit = 13000
# h2D = TH2F("2d","2d",nbinsxy,0,limit,nbinsxy,0,limit)
# h2DMC =TH2F("2dmc","2dmc",nbinsxy,0,limit,nbinsxy,0,limit)
# for i in range(10000):
#     t0.GetEntry(i)
#     t_.GetEntry(i)
#     h2D.Fill(t0.Gamma1_PT,t0.Gamma2_PT)
#     h2DMC.Fill(t_.gamma0_PT,t_.gamma_PT)
# c = TCanvas()
# h2D.Draw('colz')
# c0=TCanvas()
# h2DMC.Draw('colz')
