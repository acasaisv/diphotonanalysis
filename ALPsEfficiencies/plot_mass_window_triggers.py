from ROOT import *
#gROOT.ProcessLine(".x ./lhcbStyle.C")
#gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
gStyle.SetOptStat("")
root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/'


fHP51 =TFile(root + 'hardPhoton_51.root')
fHP52 = TFile(root + 'hardPhoton_52.root')
tHP51 = fHP51.Get('B2GammaGammaTuple/DecayTree')
tHP52 = fHP52.Get('B2GammaGammaTuple/DecayTree')

fHP = TFile(root + 'hardPhoton-withcones.root')
tHP = fHP.Get('B2GammaGammaTuple/DecayTree')
f = TFile(root + 'JpsiMeson.root')
t = f.Get('B2GammaGammaTuple/DecayTree')
fHLT2 = TFile(root + 'Data2016_S28_Radiative_withConv.root')
tHLT2 = fHLT2.Get('DTTBs2gg_None/DecayTree')


fMBdata = TFile(root + 'MBdata.root')
tMBdata = fMBdata.Get('B2GammaGammaTuple/DecayTree')
fMBdata2016 = TFile(root + 'MBdata2016.root')
tMBdata2016 = fMBdata2016.Get('B2GammaGammaTuple/DecayTree')

fdimuon2016 = TFile(root + 'dimuon-2016.root')
tdimuon2016 = fdimuon2016.Get('B2GammaGammaTuple/DecayTree')



low = 4850
high = 6500
nbins = 10
hL0 = TH1F("L0","L0",nbins,low,high)
hL1 = TH1F("L1","L1",nbins,low,high)
hL2 = TH1F("L2","L2",nbins,low,high)
hL2HP = TH1F("L2HP","L2HP",nbins,low,high)
hL2HP51 = TH1F("L2HP51","L2HP51",nbins,low,high)
hL2HP52 = TH1F("L2HP52","L2HP52",nbins,low,high)
hL2data = TH1F("L2data","L2data",nbins,low,high)
hL2MBdata = TH1F("L2MBdata","L2MBdata",nbins,low,high)
hL2MBdata2016 = TH1F("L2MBdata2016","L2MBdata2016",nbins,low,high)
hL2dimuon2016 = TH1F("L2dimuon2016","L2dimuon2016",nbins,low,high)

L0 = "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
L1 = "B_s0_Hlt1B2GammaGammaDecision_TOS"
L2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
#L2 = "1"
all = "{0} && {1} && {2}".format(L0,L1,L2)
t.Project("L0","B_s0_M",L0)
t.Project("L1","B_s0_M","{0} && {1}".format(L0,L1))
t.Project("L2","B_s0_M",all)

tHLT2.Project("L2data","A1_M")

tHP.Project("L2HP","B_s0_M",all)

tHP51.Project("L2HP51","B_s0_M",all)

tHP52.Project("L2HP52","B_s0_M",all)

tMBdata.Project("L2MBdata","B_s0_M","B_s0_Hlt1MBNoBiasDecision_Dec && "+all)

tMBdata2016.Project("L2MBdata2016","B_s0_M","B_s0_Hlt1MBNoBiasDecision_Dec &&" + all)

tdimuon2016.Project("L2dimuon2016","B_s0_M",all)

c=TCanvas()
#hL0.SetLineColor(kRed);hL0.DrawNormalized("E")
#hL1.SetLineColor(kBlue);hL1.DrawNormalized("E same")
hL2.GetXaxis().SetTitle('m(#gamma #gamma) MeV/c^{2}')

hL2.GetYaxis().SetTitle('Norm. entries / 225 MeV/c^{2} ')

hL2.SetLineColor(kGreen+3);hL2.DrawNormalized("E same")

hL2data.SetLineColor(kRed);hL2data.DrawNormalized('E same')

hL2HP.SetLineColor(kBlue);hL2HP.DrawNormalized('E same')


#hL2HP51.SetLineColor(kBlack);hL2HP51.DrawNormalized('E same')

#hL2HP52.SetLineColor(kBlue);hL2HP52.DrawNormalized('E same')



#hL2MBdata.SetLineColor(kYellow);hL2MBdata.DrawNormalized('E same')

#hL2MBdata2016.SetLineColor(kPink+3);hL2MBdata2016.DrawNormalized('E same')

hL2dimuon2016.SetLineColor(kBlack);hL2dimuon2016.DrawNormalized('E same')


leg=TLegend(.3,.3,.3,.3)
# leg.AddEntry(hL0,"L0","L")
# leg.AddEntry(hL1,"HLT1","L")

leg.AddEntry(hL2data,'HLT2 (2016 data)','L')

leg.AddEntry(hL2,"HLT2 (JpsiK* 2016MC)","L")

leg.AddEntry(hL2HP,'HLT2 (hardPhoton_{51,52} 2018 MC)','L')

#leg.AddEntry(hL2HP51,'HLT1 (hardPhoton_{51} 2018 MC)','L')

#leg.AddEntry(hL2HP52,'HLT1 (hardPhoton_{52} 2018 MC)','L')

leg.AddEntry(hL2MBdata,'HLT2 (NoBias 2018 Data)','L')

leg.AddEntry(hL2MBdata2016,'HLT2 (NoBias 2016 Data)','L')

leg.AddEntry(hL2dimuon2016,'HLT2 (Dimuon 2016 Data)','L')

leg.Draw()
# raw_input()
# c.Print("diphotonmass.pdf","pdf")
