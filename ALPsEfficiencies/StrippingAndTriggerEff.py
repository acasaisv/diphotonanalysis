from ROOT import *
from math import sqrt

EOSROOT="root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ntuples/ALPs/"
fnocuts=TFile.Open(EOSROOT+"5GeV_NoCuts_md_2016/ALPsFromBs2ggStripping.root");tnocuts=fnocuts.Get("MCDecayTreeTuple/MCDecayTree")


f=TFile.Open(EOSROOT+"5GeV_GenProdCuts_md_2016/ALPsFromBs2ggStripping.root")
t=f.Get("MCDecayTreeTuple/MCDecayTree")

t0=f.Get("DTTBs2GammaGamma/DecayTree")
# fBs = TFile("/eos/lhcb/user/j/jcidvida/ntuples/Bsgg_MC2016_Priv_13100212.root")
# tBs=fBs.Get("DTTExoticaGG/DecayTree")


#t0.SetAlias("Bs0_ETA","0.5*log((B_s0_P+B_s0_PZ)/(B_s0_P-B_s0_PZ))")
# t0.SetAlias("Bs0_ETA","atanh(B_s0_PZ/B_s0_P)")
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

    
#CUTS
gen_cuts= "({0}_PT > 900 && (({0}_PT^2 + {0}_PZ^2) > 5500^2) && {0}_THETA < 400/1000. && {0}_THETA > 5/1000.)"
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22)"
#truth ="1"
L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
HLT1 = "B_s0_Hlt1B2GammaGammaDecision_TOS"
HLT2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
aa = "&&"
#GEN EFF
print "GEN EFF"

gen_eff=100.*tnocuts.GetEntries(gen_cuts.format("gamma") + "&&" + gen_cuts.format("gamma0"))/tnocuts.GetEntries()
denominator = tnocuts.GetEntries()
print gen_eff,"+-",100.*get_error(gen_eff/100.,1.*denominator) # 0.1885(53) en TightCuts
#TRUTH EFF
print "TRUTH EFF"

print 100.*t0.GetEntries(truth)/t.GetEntries(),"+-",100*get_error(1.*t0.GetEntries(truth)/t.GetEntries(),1.*t.GetEntries())


#TOTAL L0
print "L0 EFF"
print 100.*t0.GetEntries(L0+aa+truth)/t0.GetEntries(truth),"+-",gen_eff*get_error(1.*t0.GetEntries(L0+aa+truth)/t0.GetEntries(truth),t0.GetEntries(truth))

#HLT1_TOS/L0
print "HLT1_TOS/L0"
print 100.*t0.GetEntries(truth+aa+L0+aa+HLT1)/t0.GetEntries(truth+aa+L0),"+-",gen_eff*get_error(1.*t0.GetEntries(truth+aa+L0+aa+HLT1)/t0.GetEntries(truth+aa+L0),t0.GetEntries(truth+aa+L0))

#HLT2_TOS/(L0 HLT1 HLT2)
print "HLT2_TOS/(L0 HLT1 HLT2)"
print 100.*t0.GetEntries(truth+aa+L0+aa+HLT1+aa+HLT2)/t0.GetEntries(truth+aa+L0+aa+HLT1),"+-",get_error(1.*t0.GetEntries(truth+aa+L0+aa+HLT1+aa+HLT2)/t0.GetEntries(truth+aa+L0+aa+HLT1),t0.GetEntries(truth+aa+L0+aa+HLT1))

#TOTAL EFF
print "TOTAL/EFF"
print gen_eff*t0.GetEntries(truth+aa+L0+aa+HLT1+aa+HLT2)/t.GetEntries(),"+-",get_error(1.*t0.GetEntries(truth+aa+L0+aa+HLT1+aa+HLT2)/t.GetEntries(),t.GetEntries())

