from ROOT import *
from math import sqrt
from helpers import get_error, ufloat_eff,sim10_masses_map
from uncertainties import ufloat

gen_effs = {40:0.18,
            41:0.183,
            42:0.192,
            43:0.189,
            44:0.184,
            45:0.183,
            46:0.176,
            47:0.165,
            48:0.170,
            49:0.161,
            50:0.162,
            51:0.162,
            'pi0pi0':0.13,
            'gg':0.17}
    
#CUTS
gen_cuts= "({0}_PT > 900 && (({0}_PT^2 + {0}_PZ^2) > 5500^2) && {0}_THETA < 400/1000. && {0}_THETA > 5/1000.)"
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54)"
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22 && B_s0_TRUEID==54)"
# truth = 'gamma_P>0'
L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
#HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS)" 
HLT2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
Saturation = "(gamma_CaloHypo_Saturation<1 && gamma0_CaloHypo_Saturation<1)"
PT = "(gamma_P>6000 && gamma0_P>6000 && gamma_PT >3000 && gamma0_PT > 3000 && gamma_PP_IsPhoton>0.85 && gamma0_PP_IsPhoton>0.85 && gamma_CL >0.3 && gamma0_CL >0.3 && gamma0_PP_CaloNeutralHcal2Ecal<0.1 && gamma_PP_CaloNeutralHcal2Ecal<0.1) && B_s0_M>4800 && B_s0_M<20000"
#PT = "gamma_P>6000 && gamma0_P>6000 && gamma_PT >3000 && gamma0_PT > 3000 && gamma_CL >0.3 && gamma0_CL >0.3 && gamma0_PP_CaloNeutralHcal2Ecal<0.1 && gamma_PP_CaloNeutralHcal2Ecal<0.1"
#PT = "gamma_P>6000 && gamma0_P>6000 && gamma_PT >3000 && gamma0_PT > 3000 && gamma_CL >0.3 && gamma0_CL >0.3 "
#PT="1"
aa = "&&"

def print_effs(i,truth=truth,Saturation=Saturation):
    ntup='/scratch47/adrian.casais/ntuples/signal/sim10/'
    filename = f'491000{i}_1000-1099_Sim10a-priv.root'
    if i=='pi0pi0':
        filename = f'b2pi0pi0.root'
        truth = "((abs(gamma_MC_MOTHER_ID) == 511 && abs(gamma0_MC_MOTHER_ID) == 511 && gamma_TRUEID==111 && gamma0_TRUEID==111 && abs(B_s0_TRUEID)==511)"
        truth += "|| (abs(gamma_MC_MOTHER_ID) == 111 && abs(gamma0_MC_MOTHER_ID) == 511 && gamma_TRUEID==22 && gamma0_TRUEID==111 && abs(B_s0_TRUEID)==511)"
        truth += "|| (abs(gamma_MC_MOTHER_ID) == 511 && abs(gamma0_MC_MOTHER_ID) == 111 && gamma_TRUEID==111 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==511))"
        truth = "B_s0_TRUEID==511"
        Saturation = "(gamma_PP_Saturation<1 && gamma0_PP_Saturation<1)"
    if i=='gg':
        filename = f'b2gg-stripping.root'
        #filename = f'b2gg.root'
        truth = "abs(gamma_MC_MOTHER_ID) == 531 && abs(gamma0_MC_MOTHER_ID) == 531 && gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==531"
        #truth = "gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==531"
        #truth = 'gamma_P>0'
        Saturation = "(gamma_PP_Saturation<1 && gamma0_PP_Saturation<1)"
        #Saturation = 'gamma_P>0'
    f = TFile(ntup + filename)
    
    t0 = f.Get('DTTBs2GammaGamma/DecayTree')
    t = f.Get('MCDecayTreeTuple/MCDecayTree')
    truth_ = truth
    # truth = 'gamma_P>0'
    print("File is {}".format(filename))
    #Gen eff
    print("GEN EFF")
    gen_eff = ufloat(gen_effs[i],0.0050)
    #GEN EFF
    print (100.*gen_eff)
    #STRIPPING EFF
    print ("STRIPPING EFF")
    # truth_eff = ufloat_eff(t0.GetEntries(truth+aa+PT),t.GetEntries())
    truth_eff = ufloat_eff(t0.GetEntries(truth + aa + PT),t.GetEntries())
    print(t0.GetEntries(truth+aa+PT))
    print (100.*truth_eff)
    #Saturation/(L0 HLT1 HLT2)
    num = truth+aa+PT+aa+Saturation
    den = truth+aa+PT
    print ("Saturation/Offline")
    print(t0.GetEntries(num))
    sat_eff = ufloat_eff(t0.GetEntries(num),t0.GetEntries(den))
    print (100.*sat_eff)
    num +=  aa + L0
    den +=  aa + Saturation
    #TOTAL L0
    
    print ("L0 EFF/Saturation and Offline")
    print(t0.GetEntries(num))
    l0_eff = ufloat_eff(t0.GetEntries(num),t0.GetEntries(den))
    print (100.*l0_eff)
    num +=  aa + HLT1
    den +=  aa + L0

    #HLT1_TOS/L0
    print ("HLT1_TOS/L0 and Saturation and Offline")
    l1_eff = ufloat_eff(t0.GetEntries(num),t0.GetEntries(den))
    print (100.*l1_eff)
    num +=  aa + HLT2
    den +=  aa + HLT1

    #HLT2_TOS/(L0 HLT1 HLT2)
    print ("HLT2_TOS/(Saturation Offline L0 HLT1)")
    l2_eff = ufloat_eff(t0.GetEntries(num),t0.GetEntries(den))
    print (100.*l2_eff)

    #BDT EFF
    print ("BDT")
    bdt_eff = ufloat(0.3,0.005)
    if i=='gg':
        bdt_eff = ufloat(0.188,0.005)
    if i=='pi0pi0':
        bdt_eff = ufloat(0.188,0.011)
    print(100.*bdt_eff)

    
    #TOTAL EFF
    print ("TOTAL/EFF")
    from math import sqrt
    tot_eff = gen_eff*truth_eff*sat_eff*l0_eff*l1_eff*l2_eff*bdt_eff
    
    print (100.*tot_eff)
    print(10*"##")
    print(" ")
    # t0.Draw('B_s0_M',den+aa+truth_)
    # t0.Draw('B_s0_M',den,'same')
    
    print(t0.GetEntries(den+aa+truth_)/t0.GetEntries(den))

if __name__=="__main__":
    #for mass in sim10_masses_map:
    for mass in [
        41,
       #  "gg",
      # "pi0pi0"
                 ]:
        print("dec:{0}, mass:{1}".format(mass,sim10_masses_map[mass]))
        print_effs(mass)
