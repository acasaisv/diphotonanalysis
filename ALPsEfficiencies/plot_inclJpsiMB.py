from ROOT import *
gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/'
root = '/scratch47/adrian.casais/ntuples/background/'
#f = TFile(root + 'hardPhoton.root')
fdimuon=TFile(root+'dimuon-2016.root')
tdimuon = fdimuon.Get('B2GammaGammaTuple/DecayTree')
#f=TFile(root+'JpsiMeson2016.root')
#f=TFile(root+'inclJpsi.root')
f=TFile(root+'MB2017.root')
#f=TFile(root+'JpsiMeson3.root')
f0 = TFile(root + 'inclJpsi.root')
t = f.Get('B2GammaGammaTuple/DecayTree')

t0 = f0.Get('B2GammaGammaTuple/DecayTree')

mlow = 4850
mhigh = 6400
hL0 = TH1F("L0","L0",8,mlow,mhigh)
hL2Strip = TH1F("L2Stripping","L2Stripping",8,mlow,mhigh)
hDimuon = TH1F("DiMuon","DiMuon",8,mlow,mhigh)
t.Project("L0","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
t0.Project('L2Stripping',"B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
tdimuon.Project('DiMuon',"B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
#tHLT2.Project("L2","A1_M")
c=TCanvas()
#
hL0.GetXaxis().SetTitle('Mass [MeV/c^{2}]')
hL0.GetYaxis().SetTitle('Normalized entries/193.75 MeV/c^{2}')
hL0.SetLineColor(kGreen+3);hL0.DrawNormalized("E")
hL2Strip.SetLineColor(kBlack);hL2Strip.DrawNormalized("E same")
hDimuon.SetLineColor(kRed);hDimuon.DrawNormalized('E same')
leg=TLegend(.3,.3,.3,.3)
leg.AddEntry(hL0,"L0 MB2017","L")
leg.AddEntry(hL2Strip,"L0 inclJpsi","L")
leg.AddEntry(hDimuon,'L0 DiMuon 2016 data','L')
leg.Draw()
# raw_input()
# c.Print("diphotonmass.pdf","pdf")
