from helpers import get_ALPsdf
from ROOT import *
fnew = TFile('/scratch47/adrian.casais/ntuples/signal/sim10/49100040_1000-1099_Sim10a-priv-new.root')
tnew = fnew.Get('DTTBs2GammaGamma/DecayTree')
t0new = fnew.Get('MCDecayTreeTuple/MCDecayTree')
f = TFile('/scratch47/adrian.casais/ntuples/signal/sim10/49100040_1000-1099_Sim10a-priv.root')
t = f.Get('DTTBs2GammaGamma/DecayTree')
t0 = f.Get('MCDecayTreeTuple/MCDecayTree')
cuts = 'gamma_PT>1100 && gamma0_PT > 1100 && B_s0_PT> 2000 && gamma_P> 6000 && gamma0_P>6000 && gamma_CL > 0.3 && gamma0_CL > 0.3 && gamma_PP_CaloNeutralHcal2Ecal<0.1 && gamma0_PP_CaloNeutralHcal2Ecal < 0.1 && B_s0_MM > 4800 && B_s0_MM <20000'
truth = 'B_s0_TRUEID==54 && gamma_MC_MOTHER_ID ==54 && gamma0_MC_MOTHER_ID==54 && gamma_TRUEID==22 && gamma0_TRUEID==22'
effnew = tnew.GetEntries(cuts + ' && ' + truth)/0.01/t0new.GetEntries()
eff = t.GetEntries(truth)/0.01/t0.GetEntries()
print(f"Effnew = {effnew}")
print(f"Eff = {eff}")

print("Another check: IsNotH")
print(tnew.GetEntries('gamma_CL >0.3 && gamma0_CL>0.3'+ ' && ' + truth)/tnew.GetEntries(truth))
print(tnew.GetEntries('gamma_CL >0.3' +' && ' + truth)*tnew.GetEntries('gamma0_CL >0.3' +' && ' + truth)/tnew.GetEntries(truth)**2)

print("Another check: Hcal2Ecal")
print(tnew.GetEntries('gamma_PP_CaloNeutralHcal2Ecal <0.1 && gamma0_PP_CaloNeutralHcal2Ecal<0.1'+ ' && ' + truth)/tnew.GetEntries(truth))
print(tnew.GetEntries('gamma_PP_CaloNeutralHcal2Ecal <0.1' +' && ' + truth)*tnew.GetEntries('gamma0_PP_CaloNeutralHcal2Ecal <0.1' +' && ' + truth)/tnew.GetEntries(truth)**2)

