
from ROOT import *
gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/'
root = '/scratch47/adrian.casais/ntuples/background/'
#f = TFile(root + 'hardPhoton.root')
f=TFile(root+'dimuon2018.root')
f = TFile(root+'hardPhoton-withcones.root')
f = TFile(root+'MB2018_MC.root')
#f=TFile(root+'JpsiMeson2016.root')
#f=TFile(root+'inclJpsi.root')
#f = TFile(root+'BJpsiK.root')
#f=TFile(root+'inclJpsiMeson.root')
#f=TFile(root+'JpsiMeson3.root')
#f0 = TFile(root + '/Data2016_S28_Radiative_withConv.root')
t = f.Get('B2GammaGammaTuple/DecayTree')
#t = TChain('B2GammaGammaTuple/DecayTree')
#t.Add(root+'/inclJpsi.root');t.Add(root+'/JpsiMeson3.root')

t0 = f0.Get('DTTBs2gg_None/DecayTree')

mlow = 4850
mhigh = 6450
nbins = 6
hL0 = TH1F("L0","L0",nbins,mlow,mhigh)
hL1 = TH1F("L1","L1",nbins,mlow,mhigh)
hL2 = TH1F("L2","L2",nbins,mlow,mhigh)
hL2Strip = TH1F("L2Stripping","L2Stripping",nbins,mlow,mhigh)

t.Project("L0","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
t.Project("L1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")
t.Project("L2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")
t0.Project('L2Stripping','A1_M')
#tHLT2.Project("L2","A1_M")
c=TCanvas()
#
hL2.GetXaxis().SetTitle('Mass [MeV/c^{2}]')
hL2.GetYaxis().SetTitle('Normalized entries/258.3 MeV/c^{2}')
#hL0.GetXaxis().SetRangeUser(0,0.3)
#hL0.SetLineColor(kRed);hL0.DrawNormalized("E")
#hL1.SetLineColor(kBlue);hL1.DrawNormalized("E same")
hL2.SetLineColor(kGreen+3);hL2.DrawNormalized("E")
hL2Strip.SetLineColor(kBlack);hL2Strip.DrawNormalized("E same")
leg=TLegend(.3,.3,.3,.3)
#leg.AddEntry(hL0,"L0","L")
#leg.AddEntry(hL1,"HLT1","L")
leg.AddEntry(hL2,"HLT2","L")
leg.AddEntry(hL2Strip,"HLT2 Stripping Line","L")
    
#leg.Draw()
# raw_input()
# c.Print("diphotonmass.pdf","pdf")
