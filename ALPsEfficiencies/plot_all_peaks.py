
from ROOT import *

#gROOT.ProcessLine('.x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C')
gStyle.SetOptStat('')
ntup='/scratch47/adrian.casais/ntuples/signal/'

name = '491000{}_MagDown.root'

indices = [40,422,4,6,7,8]
masses = {40:'5 GeV',
          42:'7 GeV',
          44:'9 GeV',
          46:'11 GeV',
          47:'13 GeV',
          50:"15 GeV",
          #48:'4 GeV'
          }
colors = {40:kBlack,
          42:kRed,
          44:kBlue,
          46:kOrange-3,
          47:kPink+3,
          50:kGreen+2,
          48:kGreen}
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22) && B_s0_TRUEID==54"

L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
HLT1 = " (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
#HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS)" 
HLT2 = " B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
aa = "&&"
cut = truth
cut = L0+aa+HLT1+aa+HLT2
cut+=" && !gamma_CaloHypo_Saturation && !gamma0_CaloHypo_Saturation"
cut += aa+truth
#def plot_masses():
plot_masses = 1
if plot_masses:
    f = []
    t = []
    h = []
    leg=TLegend(.3,.3,.3,.3)
    first = True
    for ind in masses:
        f.append( TFile(ntup+name.format(ind)) )
        t.append( f[-1].Get('DTTBs2GammaGamma/DecayTree') )
        h.append( TH1D(str(ind),str(ind),200,3000,20000) )
        h[-1].SetLineColor(colors[ind])
        t[-1].Project(str(ind),'B_s0_M',cut)
        leg.AddEntry(h[-1],masses[ind],"L")
        eff = t[-1].GetEntries(cut + aa + "(gamma_CaloHypo_Saturation || gamma0_CaloHypo_Saturation)")/1./t[-1].GetEntries(cut)
        print("Fraction of signal events that are saturated for mass {0}: {1}/%".format(masses[ind],100*eff))
        if first:
            h[-1].GetXaxis().SetTitle('M [GeV/c^{2}]')
            h[-1].GetYaxis().SetTitle('No. Entries/10 MeV/c^{2}')
            h[-1].DrawNormalized()
            first=False
        else:
            h[-1].DrawNormalized('same')

    leg.Draw()
plot_l0masses=1
if plot_l0masses:

    #c = TCanvas("plot_masses")
    f = []
    t = []
    h = []
    leg=TLegend(.3,.3,.3,.3)
    first = True
    for ind in masses:
        f.append( TFile(ntup+name.format(ind)) )
        t.append( f[-1].Get('DTTBs2GammaGamma/DecayTree') )
        h.append( TH1D(str(ind),str(ind),200,0000,12000) )
        h[-1].SetLineColor(colors[ind])
        t[-1].SetAlias('deltaX','(gamma0_L0Calo_ECAL_xTrigger-gamma_L0Calo_ECAL_xTrigger)**2')
        t[-1].SetAlias('deltaY','(gamma0_L0Calo_ECAL_yTrigger-gamma_L0Calo_ECAL_yTrigger)**2')
        t[-1].SetAlias('sumX','(gamma0_L0Calo_ECAL_xTrigger**2+gamma0_L0Calo_ECAL_yTrigger**2)')
        t[-1].SetAlias('sumY','(gamma_L0Calo_ECAL_xTrigger**2+gamma_L0Calo_ECAL_yTrigger**2)')        
        t[-1].SetAlias('B_s0_Hlt1M0','sqrt(gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(deltaX+deltaY)/sqrt(sumX*sumY))')

        t[-1].Project(str(ind),'B_s0_Hlt1M0',cut)
        leg.AddEntry(h[-1],masses[ind],"L")
        eff = t[-1].GetEntries(cut + aa + "(gamma_CaloHypo_Saturation || gamma0_CaloHypo_Saturation)")/1./t[-1].GetEntries(cut)
        print("Fraction of signal events that are saturated for mass {0}: {1}/%".format(masses[ind],100*eff))

        if first:
            h[-1].GetXaxis().SetTitle('M [GeV/c^{2}]')
            h[-1].GetYaxis().SetTitle('No. Entries/10 MeV/c^{2}')
            h[-1].DrawNormalized()
            first=False
        else:
            h[-1].DrawNormalized('same')
    leg.Draw()


################
# if __name__=="__main__":
#     plot_masses()
