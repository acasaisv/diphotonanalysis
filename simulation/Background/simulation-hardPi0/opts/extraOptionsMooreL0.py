import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = "dddb-20170721-3"
LHCbApp().CondDBtag  = "sim-20190430-1-vc-"+pol+"100"

from Configurables import L0App
L0App().DDDBtag  = "dddb-20170721-3"
L0App().CondDBtag  = "sim-20190430-1-vc-"+pol+"100"

L0App().Simulation = True
L0App().outputFile = "L0output.digi"

from GaudiConf import IOHelper
data = ["Boole.digi"]
IOHelper('ROOT').inputFiles(data)

