import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = "dddb-20170721-3"
LHCbApp().CondDBtag  = "sim-20190430-1-vc-"+pol+"100"

from Configurables import Moore
Moore().outputFile = "HLT1output.digi"

from GaudiConf import IOHelper
data = ["L0output.digi"]
IOHelper('ROOT').inputFiles(data)

