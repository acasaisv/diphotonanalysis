# file /scratch03/adrian.casais/DiPhotons/generatePi/GaussDev_v52r2/Gen/DecFiles/options/49000005.py generated: Thu, 06 Jun 2019 16:40:51
#
# Event Type: 49000005
#
# ASCII decay Descriptor: pp => (pi0 + X) ...
#
from Configurables import Generation
Generation().EventType = 49000005
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "/afs/cern.ch/work/a/acasaisv/ALPs-gauss/hardPi0/hardPi0.dec"
Generation().Special.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromPi0InAcceptance"

# Switch off all Pythia 6 and Pythia 8 options.
from Gaudi.Configuration import importOptions
importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )

# Pythia 6 options.
from Configurables import PythiaProduction
Generation().Special.addTool( PythiaProduction )
Generation().Special.PythiaProduction.Commands += [
    "pysubs msel 1",               # Hard process.
    "pysubs ckin 3 80.0"]          # Minimum pT.
    #"pysubs ckin 4 15.0"]          # Maximum pT.

# Pythia 8 options.
from Configurables import Pythia8Production
Generation().Special.addTool( Pythia8Production )
Generation().Special.Pythia8Production.Commands += [
    "HardQCD:all = on",            # Hard process.
    "PhaseSpace:pTHatMin = 80.0"]  # Minimum pT.
    #"PhaseSpace:pTHatMax = 15.0"]  # Maximum pT.

from Configurables import LoKi__FullGenEventCut
Generation().addTool( LoKi__FullGenEventCut, "GammasFromPi0InAcceptance" )
GammasInAcc = Generation().GammasFromPi0InAcceptance
GammasInAcc.Code = " count ( isGoodPi0 ) > 1 "
### - HepMC::IteratorRange::descendants   4
GammasInAcc.Preambulo += [
      "from GaudiKernel.SystemOfUnits import GeV, mrad"
    , "isPi0           = ( 'pi0' == GID )"
    , "isGoodDaughterGamma = ( ( ~GVEV ) & ( GTHETA < 400.0*mrad ) & ( GTHETA > 5.0*mrad )  & ( 'gamma' == GABSID ) & (GPT > 3*GeV) )"
    , "isGoodPi0 = ( isPi0 & ( GNINTREE( isGoodDaughterGamma, 4 ) >1 ) )"
    ]
# Keep 2 -> 2 hard process in MCParticles.
from Configurables import GenerationToSimulation
GenerationToSimulation("GenToSim").KeepCode = (
    "( GBARCODE >= 1 ) & ( GBARCODE <= 6 )")

