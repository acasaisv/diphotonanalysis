#______________________________________________



fileType     = 'DST'
evtsPerJob   = 350
nJobs        = 2500
runNumber = 1100000
polarity = "d"
EventType = 39112231
nEvents      = evtsPerJob*nJobs

from random import random
#runNumber = int(random()+random()*10+random()*1e2+random()*1e3+random()*1e4+1e5*random()+1e6*random()+1e7*random()+1)

print('***', nEvents, 'events will be generated ***')

task         = LHCbTask()
task.name    = str(EventType)+"_"+str(runNumber)+"_DST_"+str(nEvents)+"_Events"
task.comment = "Local Production task"
task.float   = 10


APPCONFIGOPTS = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r395/options"
DECFILESROOT = "/home3/adrian.casais/simulation/dec_options/"
LBPYTHIA8ROOT = "/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v49r19/Gen/LbPythia8"
#LBMADGRAPH = "/home3/adrian.casais/cmtuser/GaussDev_v54r2/Gen/LbMadgraph"
LBMADGRAPH = '/scratch31/adrian.casais/cmtuser/GaussDev_v54r2/Gen/LbMadgraph'
mainDir = "/scratch03/adrian.casais/diphotonanalysis/simulation/ALPs/ALPsMC/opts"
# User workspace and release area
WORKSPACE     = "/home3/adrian.casais/simulation/steps_ganga"
RELEASE_AREA  = "/home3/adrian.casais/cmtuser3/"





cond = ''\
       'from Configurables import LHCbApp\n'\
       'LHCbApp().DDDBtag  = "dddb-20170721-3"\n'\
       'LHCbApp().CondDBtag  = "sim-20190430-vc-m{}100"\n'.format(polarity)

condL0= ''\
        'from Configurables import L0App\n'\
        'L0App().DataType = "2017"\n'\
        'L0App().DDDBtag  = "dddb-20170721-3"\n'\
        'L0App().CondDBtag = "sim-20190430-vc-m{}100"\n'\
        'L0App().Simulation = True\n'\
        'L0App().outputFile = "L0output.digi"'.format(polarity)

condMoore= ''\
           'from Configurables import Moore\n'\
           'Moore().DataType = "2017"\n'\
           'Moore().DDDBtag  = "dddb-20170721-3"\n'\
           'Moore().CondDBtag = "sim-20190430-vc-m{}100"\n'\
           'Moore().outputFile = "HLToutput.digi"'.format(polarity)



# #    G a u s s 

# step1 = LHCbTransform(name="step1_Gauss",backend=Dirac())
# step1.backend.settings['BannedSites'] = 'LCG.RAL.uk'
# #step1.application = GaudiExec(platform = "x86_64-centos7-gcc9-opt",directory="/home3/adrian.casais/cmtuser/GaussDev_v54r2")
# step1.application = GaudiExec(platform = "x86_64-slc6-gcc49-opt",directory="/home3/adrian.casais/cmtuser/GaussDev_v49r20")
# extraOptsGauss = cond +''\
#     'from Configurables import Gauss\n'\
#     'from Gauss.Configuration import *\n'\
#     'nEvts = ' + str(evtsPerJob) + '\n'\
#     'LHCbApp().EvtMax = nEvts\n'\
#     'GaussGen = GenInit("GaussGen")\n'\
#     'GaussGen.FirstEventNumber = 1\n'\
#     'runNumber = '+str(runNumber)+'\n'\
#     'GaussGen.RunNumber        = runNumber'

# with open(mainDir + '/extraOptsGauss.py','w') as file:
#     file.write(extraOptsGauss)
    
    
# #step1.application.extraOpts = extraOptsGauss

# step1.application.options = [	mainDir + '/extraOptsGauss.py',
# 				APPCONFIGOPTS + "/Gauss/Beam6500GeV-md100-2018-nu1.6.py",
# 				APPCONFIGOPTS + "/Gauss/EnableSpillover-25ns.py",
# 				APPCONFIGOPTS + "/Gauss/DataType-2018.py",
# 				APPCONFIGOPTS + "/Gauss/RICHRandomHits.py", 
# 				APPCONFIGOPTS + "/Gauss/G4PL_FTFP_BERT_EmNoCuts.py",
# 				APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
#                                 mainDir + "/{}.py".format(EventType),
#                                 #mainDir + "/ALPsGiGa.py",
#                                 #LBMADGRAPH + "/options/MadgraphPythia8.py",
#                                 LBPYTHIA8ROOT + "/options/Pythia8.py"
                                
#                          ]

    
    

# step1.mc_num_units = 1

# step1.splitter = GaussSplitter(numberOfJobs=nJobs, eventsPerJob=evtsPerJob)
# step1.outputfiles = [DiracFile("*.sim"), LocalFile("*.root"), LocalFile("*.xml")]
# #step1.inputfiles = [LocalFile(mainDir +'/minbias=hardPhoton,pt4GeV-inclusive.dec')]
# task.appendTransform(step1)


#    B o o l e 
step2 = LHCbTransform(name="step2_Boole",backend=Dirac())
step2.application =GaudiExec(platform = "x86_64-centos7-gcc49-opt",directory="/home3/adrian.casais/cmtuser/BooleDev_v30r4")
step2.application.options = [	APPCONFIGOPTS + "/Boole/Default.py",
                         	APPCONFIGOPTS + "/Boole/EnableSpillover.py",
                                APPCONFIGOPTS + "/Boole/DataType-2015.py",
                         	#APPCONFIGOPTS + "/Boole/Boole-SetOdinRndTrigger.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                         	
                         ]

#step2.mc_num_units =1 
step2.application.extraOpts = cond
with open('/home3/adrian.casais/listfile.txt', 'r') as filehandle:
        filecontents = filehandle.readlines()
mylfns = [i[:-1] for i in filecontents]
datalink = LHCbDataset()
for el in mylfns: datalink.append(DiracFile(lfn=el))
step2.splitter = SplitByFiles(filesPerJob=20,ignoremissing=True)
step2.addInputData(datalink)
step2.application.extraOpts = cond
step2.outputfiles = [DiracFile("*.digi"), LocalFile("*.root"), LocalFile("*.xml")]
step2.delete_chain_input = True
#step2.mc_num_units = 1

task.appendTransform(step2)



#    M o o r e    L 0
step3 = LHCbTransform(name="step3_Moore_L0",backend=Dirac())

step3.application = GaudiExec(directory="/home3/adrian.casais/cmtuser/MooreDev_v28r3p1", platform = "x86_64-centos7-gcc62-opt")
step3.application.options = [	
				APPCONFIGOPTS + "/L0App/L0AppSimProduction.py",
                         	APPCONFIGOPTS + "/L0App/L0AppTCK-0x18a4.py",
                         	APPCONFIGOPTS + "/L0App/ForceLUTVersionV8.py",
                         	APPCONFIGOPTS + "/L0App/DataType-2017.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                         	# WORKSPACE + "/CondL0.py",
                         	# WORKSPACE + "/Cond.py"
                         ]
#step3.splitter = SplitByFiles(filesPerJob=10)
datalink = TaskChainInput()
datalink.include_file_mask = ['\.digi$']
datalink.input_trf_id = step2.getID()
step3.addInputData(datalink)
step3.application.extraOpts = cond + condL0 
step3.outputfiles = [DiracFile("*.digi")]
step3.delete_chain_input = True
task.appendTransform(step3)



#    M o o r e   H l t 1

step4 = LHCbTransform(name="step4_Moore_Hlt1",backend=Dirac())
step4.application = GaudiExec(directory="/home3/adrian.casais/cmtuser/MooreDev_v28r3p1", platform = "x86_64-centos7-gcc62-opt")
step4.application.options = [	
				APPCONFIGOPTS + "/Moore/MooreSimProductionForSeparateL0AppStep2015.py",
                         	APPCONFIGOPTS + "/Conditions/TCK-0x517a18a4.py",
                                APPCONFIGOPTS + "/Moore/DataType-2017.py",
                         	APPCONFIGOPTS + "/Moore/MooreSimProductionHlt1.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                                
                         ]
#step4.splitter = SplitByFiles(filesPerJob=10)
datalink = TaskChainInput()
datalink.include_file_mask = ['\.digi$']
datalink.input_trf_id = step3.getID()
step4.application.extraOpts = cond + condMoore 
# datalink = LHCbDataset()
# for el in lfns: datalink.append(DiracFile(lfn=el))
step4.addInputData(datalink)
step4.outputfiles = [DiracFile("*.digi")]
step4.delete_chain_input = True
task.appendTransform(step4)



#    M o o r e   H l t 2

step5 = LHCbTransform(name="step5_Moore_Hlt2",backend=Dirac())
step5.application = GaudiExec(directory="/home3/adrian.casais/cmtuser/MooreDev_v28r3p1", platform = "x86_64-centos7-gcc62-opt")
step5.application.options = [	
                                APPCONFIGOPTS + "/Moore/MooreSimProductionForSeparateL0AppStep2015.py",
                         	APPCONFIGOPTS + "/Conditions/TCK-0x617d18a4.py",
                                APPCONFIGOPTS + "/Moore/DataType-2017.py",
                         	APPCONFIGOPTS + "/Moore/MooreSimProductionHlt2.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                         ]
#step5.splitter = SplitByFiles(filesPerJob=10)
datalink = TaskChainInput()
datalink.include_file_mask = ['\.digi$']
datalink.input_trf_id = step4.getID()
step5.application.extraOpts = cond + condMoore
step5.addInputData(datalink)
step5.outputfiles = [DiracFile("*.digi"), LocalFile("*.root"), LocalFile("*.xml")]
step5.delete_chain_input = True
task.appendTransform(step5)



#    B r u n e l 

step6 = LHCbTransform(name="step6_Brunel",backend=Dirac())
step6.application = GaudiExec(directory="/home3/adrian.casais/cmtuser/BrunelDev_v54r2", platform = "x86_64-centos7-gcc62-opt")
step6.application.options =  [
                                APPCONFIGOPTS + "/Brunel/DataType-2018.py",
				APPCONFIGOPTS + "/Brunel/MC-WithTruth.py",
                                #APPCONFIGOPTS + "/Brunel/noMergingGenFSR.py",
                                APPCONFIGOPTS + "/Brunel/SplitRawEventOutput.4.3.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                                #APPCONFIGOPTS + "/Brunel/ldst.py"
                         ]
datalink = TaskChainInput()
datalink.include_file_mask = ['\.digi$']
datalink.input_trf_id = step5.getID()
step6.application.extraOpts = cond
step6.addInputData(datalink)
step6.outputfiles = [DiracFile("*dst"), LocalFile("*.root"), LocalFile("*.xml")]
step6.delete_chain_input = True
task.appendTransform(step6)


task.run()
