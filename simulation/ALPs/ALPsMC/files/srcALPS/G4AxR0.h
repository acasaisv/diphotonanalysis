#ifndef G4AxR0_H 
#define G4AxR0_H 1

#include "Geant4/G4ParticleDefinition.hh"

/* ###############################################################################
 * ### Hidden Valley particles for Dark Showers (HV processes in Pythia 8.230) ###
 * ### G4Higgses used as a template to write this file (author: Vanya Belyaev) ###
 * ### 2018-02-03 - written by Carlos Vazquez Sierra (carlos.vazquez@cern.ch)  ###
 * ###############################################################################
 * id="4900111" name="pivDiag" spinType="1" chargeType="0" colType="0" m0="10.00000" 
 * id="4900113" name="rhovDiag" spinType="3" chargeType="0" colType="0" m0="10.00000" 
 * id="4900023" name="Zv" spinType="3" chargeType="0" colType="0" m0="1000.00000" mWidth="20.00000" mMin="100.00000" mMax="0.00000" 
 * id="4900101" name="qv" antiName="qvbar" spinType="1" chargeType="0" colType="0" m0="100.00000"
 * id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
 * id="4900213" name="rhovUp" antiName="rhovDn" spinType="3" chargeType="0" colType="0" m0="10.00000"
 * id="4900021" name="gv" spinType="3" chargeType="0" colType="0" m0="0.00000" 
 */

// ================================================================================================
// id="4900111" name="AxR0" spinType="1" chargeType="0" colType="0" m0="10.00000" 
// id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
// ================================================================================================
class G4AxR0: public G4ParticleDefinition 
{
public:
  static G4AxR0* Definition     () ;
  static G4AxR0* AxR0Definition () ;
  static G4AxR0* AxR0           () ;
public:
  static G4AxR0* Create 
  ( const double mass ,                                    // the mass 
    const double ctau ) ;                                  // the lifetime 
protected:
  G4AxR0 ( const double mass , 
              const double ctau ) ;
public:
  virtual ~G4AxR0() ;  // virtual desctructor 
private:
  G4AxR0 () ;                               // default constructor is disabled 
  G4AxR0 ( const G4AxR0& ) ;                    // copy constructor is disabled 
  G4AxR0& operator=( const G4AxR0& ) ;       // assignment operator is disabled 
private:
  static G4AxR0* s_instance ;
} ;


// ============================================================================
// The END 
// ============================================================================
#endif // G4HIDDENVALLEYUPDATED_H
// ============================================================================

