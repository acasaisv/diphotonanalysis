// ============================================================================           
// Include files                                                                          
// STD & STL                                                                              
#include <algorithm>

// GaudiKernel                                                                            
#include "GaudiKernel/PhysicalConstants.h"

// LHCb                                                                                   
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
// GiGa                                                                                   
#include "GiGa/GiGaPhysConstructorBase.h"
// Geant4                                                                                 
#include "Geant4/G4ParticleTable.hh"
// Local

#include "G4AxR0.h"

// ============================================================================
/** @class GiGaALPsParticles 
 *  simple class to construct Hidden Valley particles 
 *
 *  The major properties:
 *  
 *    - "ALPs" : list of hidden valley particles to be created.
 *                       the default value contains [ "pivDiag" , "pivUp", "pivDn", "rhovDiag", "rhovUp", "rhovDn", "Zv", "gv", "qv", "qvbar" ] 
 *
 *  It also prints (@ MSG::INFO) the properties of created particles.
 *  
 *  GiGaHiggsParticles used as a template to write this file (Vanya Belyaev).
 *
 *  @author Carlos VAZQUEZ SIERRA carlos.vazquez@cern.ch
 *  @date 2018-02-03
 */

class GiGaALPsParticles : public GiGaPhysConstructorBase
{
public:
  // ==========================================================================
  /// constructor 
  GiGaALPsParticles    
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) 
    : GiGaPhysConstructorBase ( type , name , parent )
    , m_axion ()
  {
    m_axion.push_back ( "AxR0" ) ;
    declareProperty 
      ("ALPs" , m_axion , 
       "The list of Axions to be instantiated") ;  
  }
  // ==========================================================================           
  /// construct the particles                                                             
  void ConstructParticle ()    override;   // construct the particles
  /// construct the processed  (empty)                                                    
  void ConstructProcess  () override{} ;  // construct the processed
  // ==========================================================================
protected:
  // ==========================================================================  
  /// get the particle property for the given particle name 
  const LHCb::ParticleProperty* pp 
  ( const std::string&    n , 
    LHCb::IParticlePropertySvc* s ) const 
  {
    Assert ( 0 != s , "Invalid ParticleProperty Service") ;
    const LHCb::ParticleProperty* p = s->find ( n ) ;
    Assert ( 0 != p , "No information is available for '" + n + "'") ;
    return p ;
  }
  // ==========================================================================
private:
  // ==========================================================================
  typedef std::vector<std::string> Strings ;
  /// list of Hidden Valley particles to be instantiated
  Strings    m_axion ; // list of Hidden Valley particles to be instantiated
  // ==========================================================================
};

// ============================================================================
// construct the particles 
// ============================================================================
void GiGaALPsParticles::ConstructParticle () // construct the particles 
{
  
  LHCb::IParticlePropertySvc* ppSvc = 
    svc<LHCb::IParticlePropertySvc> ("LHCb::ParticlePropertySvc") ;
  
  Strings tmp = m_axion ;
  {
    // ========================================================================
    Strings::iterator it = std::find  ( tmp.begin() , tmp.end() , "AxR0" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* piv = pp ( "AxR0" , ppSvc ) ;
      G4AxR0* pi_v = G4AxR0::Create 
        ( piv -> mass     () ,       
          piv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != pi_v , "Unable to create AxR0" ) ;
      if ( msgLevel ( MSG::INFO )  ) { pi_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
        // ========================================================================
    Assert ( tmp.empty() , "Unknown ALPs particles in the list!" ) ;
    // ========================================================================
  }
}


// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT ( GiGaALPsParticles )

// The END 
// ============================================================================

