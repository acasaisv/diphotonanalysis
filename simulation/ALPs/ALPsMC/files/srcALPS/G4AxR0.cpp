// $Id: G4Higgses.cpp,v 1.1 2008-06-24 10:29:45 ibelyaev Exp $                            
// ============================================================================           
// Include files                                                                          
//                                                                                        
// GEANT4                                                                                 
#include "Geant4/G4ParticleTable.hh"

// GaudiKernel                                                                            
#include "GaudiKernel/PhysicalConstants.h"

// GiGa                                                                                   
#include "GiGa/GiGaException.h"

// local  
#include "G4AxR0.h"

/* ###############################################################################
 * ### AxR0 Dark Sector Particle (HV processes in Pythia 8.230) ###
 * ### G4Higgses used as a template to write this file (author: Vanya Belyaev) ###
 * ### 2018-10-12 - written by Adrian Casais Vidal (adrian.casais.vidal@cern.ch)  ###
 * ###############################################################################
 * id="54"      name="AxR0"spintType="1" chargeType="0" colType="1" m0="3.0000"
 
 */

G4AxR0*  G4AxR0::s_instance = 0 ;
// Define the particles:
G4AxR0::G4AxR0  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
    ( "AxR0"                               , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-pari
  "DMS"                                   , // p-type 
  0                                       , // lepton 
  0                                       , // baryon
  54                                      , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "DMS"                                   , // subtype    
  54                                        // antiparticle  
  )
{}


// Create the particles:
G4AxR0* G4AxR0::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4AxR0::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("AxR0");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4AxR0::Create: AxR0 already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 54 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4AxR0::Create: PDGID(54) already exists!") ; }
  // create the particle: 
  s_instance = new G4AxR0 ( mass , ctau ) ;
  return s_instance ;
}


// ============================================================================
// Get the definitions
// ============================================================================
G4AxR0* G4AxR0::Definition  () { return AxR0Definition() ; }
G4AxR0* G4AxR0::AxR0Definition () { return s_instance ; }
G4AxR0* G4AxR0::AxR0            () { return AxR0Definition() ; }

// ============================================================================
// Virtual destructors
// ============================================================================
G4AxR0::~G4AxR0(){} 
// ============================================================================
// The END 
// ============================================================================
