from Configurables import *

# Configure Gauss.
OutputStream('GaussTape').Output = (
    "DATAFILE='PFN:gauss.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 1e2
LHCbApp().DDDBtag = "dddb-20170721-2"
LHCbApp().CondDBtag = "sim-20160321-2-vc-md100"
Gauss().Phases = ["Generator", "GenToMCTree"]
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 2

# Configure the event type.
from Configurables import (Generation, Special, MadgraphProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

# Generation options.
Generation().EventType            = 70000000
Generation().PileUpTool           = "FixedNInteractions"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"

# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
Generation().Special.ProductionTool = "MadgraphProduction"

# Madgraph options.
Generation().Special.addTool(MadgraphProduction)
Generation().Special.MadgraphProduction.Commands += [
    "import model R_axion/",
    "generate p p > R0 > a a DMS==2 @1",
    " set wr0 Auto",
    " set mr0 3.",
    " set pta 0.0",
    " set cgg 10",
    " set cww 10",
    " set cbb 50./3"
    
]
