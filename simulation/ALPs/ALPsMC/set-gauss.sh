export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_93c/MCGenerators/madgraph5amc/2.6.1.atlas/x86_64-slc6-gcc62-opt/bin/:$PATH

/cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc62-opt
lb-dev Gauss/v52r2
cd ./GaussDev_v52r2
git lb-use Gauss
git lb-checkout Gauss/LHCBGAUSS-1374.Madgraph Gen/LbPythia8
git lb-checkout Gauss/LHCBGAUSS-1374.Madgraph Gen/LbMadgraph
git lb-checkout Gauss/LHCBGAUSS-1374.Madgraph Gen/LbPythia/LbPythia
git lb-checkout Gauss/LHCBGAUSS-1374.Madgraph cmake
#git lb-checkout Gauss/LHCBGAUSS-1374.Madgraph .
git lb-checkout Gauss/v53r0 Sim/GaussPhysics
cp ../files/srcALPS/* Sim/GaussPhysics/src
cp Gen/LbMadgraph/doc/FindPythia8.cmake cmake/
cp -r Gen/LbPythia8/LbPythia8/ Gen/LbMadgraph/
mv Gen/LbPythia/LbPythia/ Gen/LbMadgraph/
rm -r Gen/LbPythia/

# make configure
# make -j30
# cp Gen/LbMadgraph/doc/madgraph.* ./