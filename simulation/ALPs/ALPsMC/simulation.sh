#!/bin/bash
# Start with qsub -q long -N Majorana10GeV10ps_md -t 1-200 < Majorana10GeV10ps_md.sh


export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_93c/MCGenerators/madgraph5amc/2.6.2.atlas/x86_64-slc6-gcc62-opt/bin/:$PATH

JOBNAME=$(echo $PBS_JOBNAME | rev | cut -c 6- | rev)
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
seed=$1
echo $seed
JOBNAME=default
evts=50
year=2016
 
magnet=md
EventType=5GeV_GenProdCuts
 
mainDir=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/opts
#output=/eos/lhcb/user/a/acasaisv/ALPs/output2
#output=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/output5GeV
output=/eos/lhcb/user/a/acasaisv/ALPs/${EventType}_${magnet}_${year}
tmp=/tmp/acasaisv/ALPs
export RUNNUMBER=$(($seed))
export EVTMAX=$evts
export MAINDIR=$mainDir
export POLARITY=$magnet
 
echo "================================================================================================================================"
echo "Starting on : $(date)"
echo "Running on node : $(hostname)"
echo "Current directory : $(pwd)"
echo "Working directory : $PBS_O_WORKDIR"
echo "Current job ID : $PBS_JOBID"
echo "Current job name : $JOBNAME"
echo "Job index number : $PBS_ARRAYID"
echo "Output directory: $output"
 
mkdir ${output}
mkdir ${tmp}
mkdir ${tmp}/${EventType}_${magnet}_${year}_${seed}
cd ${tmp}/${EventType}_${magnet}_${year}_${seed}
 
APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r369/options
### decfiles xenericos
DECFILESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v30r9/options
### movidas tuas
#DECFILESROOT=/project/bfys/cvazquez/HiddenValleyProductions/productions/hiddenvalley
#####/scratch03/adrian.casais/ALPs-gauss/output##################
LBPYTHIA8ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v49r8/Gen/LbPythia8
#####
LBMADGRAPH=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/gauss-madgraph/GaussDev_v52r2/Gen/LbMadgraph
#### GAUSS
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc62-opt
/afs/cern.ch/work/a/acasaisv/ALPs-gauss/gauss-madgraph/GaussDev_v52r2/run gaudirun.py ${APPCONFIGOPTS}/Gauss/Beam6500GeV-${magnet}100-2016-nu1.6.py ${APPCONFIGOPTS}/Gauss/EnableSpillover-25ns.py ${APPCONFIGOPTS}/Gauss/DataType-${year}.py ${APPCONFIGOPTS}/Gauss/RICHRandomHits.py ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py  ${LBMADGRAPH}/options/MadgraphPythia8.py   ${mainDir}/madgraph.py ${mainDir}/makeALPs.py ${mainDir}/extraOptionsGauss.py
find . ! -name 'Gauss.sim' -type f -exec rm -f {} +

### BOOLE
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc7-opt
lb-run Boole/v40r1 gaudirun.py ${APPCONFIGOPTS}/Boole/Default.py ${APPCONFIGOPTS}/Boole/EnableSpillover.py ${APPCONFIGOPTS}/Boole/DataType-2015.py ${APPCONFIGOPTS}/Boole/Boole-SetOdinRndTrigger.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsBoole.py
find . ! -name 'Boole.digi' -type f -exec rm -f {} +

### MOORE
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc48-opt
lb-run Moore/v25r4 gaudirun.py ${APPCONFIGOPTS}/L0App/L0AppSimProduction.py ${APPCONFIGOPTS}/L0App/L0AppTCK-0x160F.py ${APPCONFIGOPTS}/L0App/ForceLUTVersionV8.py ${APPCONFIGOPTS}/L0App/DataType-2016.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL0.py
find . ! -name 'L0output.digi' -type f -exec rm -f {} +

lb-run Moore/v25r4 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x5138160F.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt1.py ${APPCONFIGOPTS}/L0App/DataType-2016.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL1.py
# cp HLT1output.digi "${output}/Hlt1Moore_${EventType}_${magnet}_${year}_${PBS_ARRAYID}.digi"
find . ! -name 'HLT1output.digi' -type f -exec rm -f {} +

lb-run Moore/v25r4 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x6139160F.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt2.py ${APPCONFIGOPTS}/L0App/DataType-2016.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL2.py
find . ! -name 'HLT2output.digi' -type f -exec rm -f {} +

### BRUNEL
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc49-opt
lb-run Brunel/v50r4 gaudirun.py ${APPCONFIGOPTS}/Brunel/DataType-2016.py ${APPCONFIGOPTS}/Brunel/MC-WithTruth.py $APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py $APPCONFIGOPTS/Brunel/ldst.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsBrunel.py
find . ! -name 'Brunel.ldst' -type f -exec rm -f {} +


### DAVINCI
# source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc49-opt
# lb-run DaVinci/v41r4p3 gaudirun.py ${APPCONFIGOPTS}/DaVinci/DV-Stripping28-Stripping-MC-NoPrescaling-DST.py ${APPCONFIGOPTS}/DaVinci/DataType-${year}.py ${APPCONFIGOPTS}/DaVinci/InputType-DST.py ${mainDir}/extraOptionsDaVinci.py

# mv 000000.AllStreams.dst "${output}/DaVinci_${EventType}_${magnet}_${year}_${PBS_ARRAYID}.dst"
mv Brunel.ldst "${output}/Brunel_${EventType}_${magnet}_${year}_${seed}.dst"
cd $HOME
rm -rf ${tmp}/${EventType}_${magnet}_${year}_${seed}