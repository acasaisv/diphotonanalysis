import os
pol       = str(os.environ['POLARITY'])
nEvts     = int(os.environ['EVTMAX'])
runnumber = int(os.environ['RUNNUMBER'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = os.environ['DDDBtag']
LHCbApp().CondDBtag  = os.environ['CondDBtag']


from Configurables import Gauss
from Gauss.Configuration import *
LHCbApp().EvtMax = nEvts

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = runnumber
GaussGen.FirstEventNumber = 1000000

OutputStream("GaussTape").Output = "DATAFILE='PFN:Gauss.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"
