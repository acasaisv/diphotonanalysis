import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = os.environ['DDDBtag']
LHCbApp().CondDBtag  = os.environ['CondDBtag']

from Configurables import Moore
Moore().outputFile = "HLT2output.digi"

from GaudiConf import IOHelper
data = ["HLT1output.digi"]
IOHelper('ROOT').inputFiles(data)

