import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = os.environ['DDDBtag']
LHCbApp().CondDBtag  = os.environ['CondDBtag']

from GaudiConf import IOHelper
data = ["HLT2output.digi"]
IOHelper('ROOT').inputFiles(data)

