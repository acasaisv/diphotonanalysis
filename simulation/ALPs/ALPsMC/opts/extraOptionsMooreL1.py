import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = os.environ['DDDBtag']
LHCbApp().CondDBtag  = os.environ['CondDBtag']

from Configurables import Moore
Moore().outputFile = "HLT1output.digi"

from GaudiConf import IOHelper
data = ["L0output.digi"]
IOHelper('ROOT').inputFiles(data)

