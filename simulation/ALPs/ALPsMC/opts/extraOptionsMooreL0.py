import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = os.environ['DDDBtag']
LHCbApp().CondDBtag  = os.environ['CondDBtag']

from Configurables import L0App
L0App().DDDBtag  = os.environ['DDDBtag']
L0App().CondDBtag  = os.environ['CondDBtag']
L0App().Simulation = True
L0App().outputFile = "L0output.digi"

from GaudiConf import IOHelper
data = ["Boole.digi"]
IOHelper('ROOT').inputFiles(data)

