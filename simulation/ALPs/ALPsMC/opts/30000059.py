# file /home3/adrian.casais/cmtuser/GaussDev_v49r19/Gen/DecFiles/options/30000059.py generated: Wed, 16 Dec 2020 18:31:03
#
# Event Type: 30000059
#
# ASCII decay Descriptor: pp => ?
#
from Configurables import Generation
Generation().EventType = 30000059
Generation().SampleGenerationTool = "MinimumBias"
from Configurables import MinimumBias
Generation().addTool( MinimumBias )
Generation().MinimumBias.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "./minbias=hardPhoton,pt4GeV-inclusive.dec"
Generation().MinimumBias.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromMBInAcceptance"

from Configurables import LoKi__FullGenEventCut
Generation().addTool( LoKi__FullGenEventCut, "GammasFromMBInAcceptance" )
GammasInAcc = Generation().GammasFromMBInAcceptance
GammasInAcc.Code = " ( count ( isGoodPi0 ) > 1 ) | ( count ( isGoodGamma ) > 1 ) |"\
                   " ( count ( isGoodPi0 ) > 0 ) & ( count ( isGoodGamma ) > 0 ) "
### - HepMC::IteratorRange::descendants   4
GammasInAcc.Preambulo += [
      "from GaudiKernel.SystemOfUnits import GeV, mrad",
      "inAcceptance          = ( GTHETA < 400.0*mrad ) & ( GTHETA > 5.0*mrad )",
      "kinematicCut          = (GPT > 4*GeV)",
      "isGoodPi0             = ( 'pi0' == GID ) & inAcceptance & kinematicCut",
      "isGoodGamma           = ( 'gamma' == GID ) & inAcceptance & kinematicCut",
    ]

