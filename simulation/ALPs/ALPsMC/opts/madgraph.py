from Configurables import LHCbApp,OutputStream

# Configure the event type.
from Configurables import (Generation, Special, MadgraphProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

# Generation options.
Generation().EventType            = 70000000
Generation().PileUpTool           = "FixedLuminosityForRareProcess"
#Generation().PileUpTool           = "FixedNInteractions"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"

# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
Generation().Special.ProductionTool = "MadgraphProduction"


# Madgraph options.
Generation().Special.addTool(MadgraphProduction)

Generation().Special.MadgraphProduction.Commands += [
    "import model modules/R_axion",
    "generate p p > R0 > a a DMS==2 @1",
    " set wr0 Auto",
    " set mr0 5.",
    " set pta 0.0",
    " set ptl 0.0",
    " set ptlmax -1",
    " set etaa -1",
    " set etaamin 0",
    " set etal -1",
    " set etalmin 0",
    " set etaj -1",
    " set etajmin 0",
    " set draa 0",
    " set draamax -1",
    " set draj 0",
    " set drajmax -1",
    " set cgg 10",
    " set cww 10",
    " set cbb 16.66667"
    
]

#Cuts
from Configurables import LoKi__FullGenEventCut, Generation
Generation().addTool( LoKi__FullGenEventCut, "GammasFromAxInAcceptance" )
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromAxInAcceptance"
tracksInAcc = Generation().GammasFromAxInAcceptance
tracksInAcc.Code = " count ( isGoodAx) > 0 "
### - HepMC::IteratorRange::descendants   4
tracksInAcc.Preambulo += [
      "from GaudiKernel.SystemOfUnits import GeV, mrad, MeV"
    , "isAx= (54==GID)"
    ,  "pT = (GPT> 900*MeV)"
    , "P = (GP> 5500*MeV)"
    , "pZ = (GPZ> 0)"
    , "theta= ( (GTHETA < 400.0*mrad) & (GTHETA> 5.0*mrad) )"
    , "isGoodDaughterGamma =  ( ~GVEV ) & theta & ( 'gamma' == GABSID ) & pT & P & pZ"
    , "isGoodAx = isAx & ( GNINTREE( isGoodDaughterGamma, 4) ==2 )"
    # , "isGoodAx = isAx"   
    ]

# tracksInAcc.Preambulo += [
#       "from GaudiKernel.SystemOfUnits import GeV, mrad, MeV"
#     , "isAx= (54==GID)"
#     , "theta= ( (abs(GETA) < 5) & (abs(GETA)>2) )"
#     , "isGoodDaughterGamma =  ( ~GVEV ) & theta & ( 'gamma' == GABSID )"
#     , "isGoodAx = isAx & ( GNINTREE( isGoodDaughterGamma, 4) ==2 )"
#     # , "isGoodAx = isAx"   
#     ]



