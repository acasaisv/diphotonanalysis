# file /home3/adrian.casais/cmtuser/GaussDev_v49r20/Gen/DecFiles/options/39112231.py generated: Fri, 14 May 2021 18:25:13
#
# Event Type: 39112231
#
# ASCII decay Descriptor: [eta -> mu+ mu- gamma]
#
from Configurables import Generation
Generation().EventType = 39112231
Generation().SampleGenerationTool = "SignalPlain"
from Configurables import SignalPlain
Generation().addTool( SignalPlain )
Generation().SignalPlain.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/eta_mumugamma=TightGamma.dec"
Generation().SignalPlain.CutTool = "LoKi::GenCutTool/TightCut"
Generation().SignalPlain.SignalPIDList = [ 221 ]

#
from Configurables import LoKi__GenCutTool
gen = Generation()
gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
tightCut = gen.SignalPlain.TightCut
tightCut.Decay     = '^[ eta -> ^mu+ ^mu- ^gamma]CC'
tightCut.Cuts      =    {
    '[mu+]cc' : ' goodMu ',
    'eta'   : ' goodEta ',
    'gamma' : 'goodGamma'}
tightCut.Preambulo += [
    'inAcc  = in_range ( 0.005, GTHETA, 0.400 ) ' , 
    'goodMu = (GPT > 500 * MeV) & inAcc',
    'goodEta  = (GPT > 1000 * MeV) & inAcc',
    'goodGamma = (GPT > 2500 * MeV) & inAcc']

