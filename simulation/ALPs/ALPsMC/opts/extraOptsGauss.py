from Configurables import LHCbApp
LHCbApp().DDDBtag  = "dddb-20170721-3"
LHCbApp().CondDBtag  = "sim-20190430-vc-md100"
from Configurables import Gauss
from Gauss.Configuration import *
nEvts = 350
LHCbApp().EvtMax = nEvts
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
runNumber = 1100000
GaussGen.RunNumber        = runNumber