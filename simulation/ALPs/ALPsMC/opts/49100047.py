# file /scratch31/adrian.casais/cmtuser2/GaussDev_v54r2/Gen/DecFiles/options/49100047.py generated: Mon, 04 Jan 2021 17:09:15
#
# Event Type: 49100047
#
# ASCII decay Descriptor: pp -> (ALP -> gamma gamma)
#
from Configurables import Generation
Generation().EventType = 49100047
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "PythiaProduction"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/ALP132gg.dec"
Generation().Special.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromAxInAcceptance"
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ "AxR0 54 54 0.0 13 3.e-19 AxR0 54 1.e-2" ]

#Configure the event type.
from Configurables import (Generation, Special, MadgraphProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions
# Generation options.
Generation().PileUpTool           = "FixedLuminosityForRareProcess"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"
# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
Generation().Special.ProductionTool = "MadgraphProduction"
# Madgraph options.
Generation().Special.addTool(MadgraphProduction)
Generation().Special.MadgraphProduction.Commands += [
    "import model R_axion/",
    "generate p p > R0 > a a DMS==2 @1",
    " set wr0 Auto",
    " set mr0 13.",
    " set pta 0.0",
    " set ptl 0.0",
    " set ptlmax -1",
    " set etaa -1",
    " set etaamin 0",
    " set etal -1",
    " set etalmin 0",
    " set etaj -1",
    " set etajmin 0",
    " set draa 0",
    " set draamax -1",
    " set draj 0",
    " set drajmax -1",
    " set lambdaa 1000",
    " set cgg 10",
    " set cww 10",
    " set cbb 16.66667"
]
#Cuts
from Configurables import LoKi__FullGenEventCut, Generation
Generation().addTool( LoKi__FullGenEventCut, "GammasFromAxInAcceptance" )
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromAxInAcceptance"
tracksInAcc = Generation().GammasFromAxInAcceptance
tracksInAcc.Code = " count ( isGoodAx) > 0 "
### - HepMC::IteratorRange::descendants   4
tracksInAcc.Preambulo += [
      "from GaudiKernel.SystemOfUnits import GeV, mrad, MeV"
    , "isAx= (54==GID)"
    ,  "pT = (GPT> 900*MeV)"
    , "P = (GP> 5500*MeV)"
    , "pZ = (GPZ> 0)"
    , "theta= ( (GTHETA < 400.0*mrad) & (GTHETA> 5.0*mrad) )"
    , "isGoodDaughterGamma =  ( ~GVEV ) & theta & ( 'gamma' == GABSID ) & pT & P & pZ"
    , "isGoodAx = isAx & ( GNINTREE( isGoodDaughterGamma, 4) ==2 )"
    # , "isGoodAx = isAx"   
    ]
from Configurables import GenerationToSimulation
GenerationToSimulation("GenToSim").KeepCode = "in_list( GABSID, [ 'AxR0' ] )"

