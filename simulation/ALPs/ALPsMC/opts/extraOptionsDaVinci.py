import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = "dddb-20170721-3"
LHCbApp().CondDBtag  = "sim-20170721-2-vc-"+pol+"100"

from GaudiConf import IOHelper
data = ["Brunel.dst"]
IOHelper('ROOT').inputFiles(data)

