import os
pol   = str(os.environ['POLARITY'])

from Configurables import LHCbApp
LHCbApp().DDDBtag  = "dddb-20171126"
LHCbApp().CondDBtag  = "sim-20171127-vc-mu100"

from GaudiConf import IOHelper
data = ["Boole.digi"]
IOHelper('ROOT').inputFiles(data)

