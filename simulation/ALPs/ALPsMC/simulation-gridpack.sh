#!/bin/bash
# Start with qsub -q long -N Majorana10GeV10ps_md -t 1-200 < Majorana10GeV10ps_md.sh

export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_96b/MCGenerators/madgraph5amc/2.7.2.atlas3/x86_64-centos7-gcc9-opt/bin:$PATH
export CMTCONFIG=x86_64-centos7-gcc9-opt
source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
seed=$1
echo $seed
JOBNAME=default
evts=100
year=2018
magnet=md
EventType=49100040
export DDDBtag=dddb-20170721-3
export CondDBtag=sim-20190430-vc-${magnet}100
 
mainDir=/scratch03/adrian.casais/diphotonanalysis/simulation/ALPs/ALPsMC/opts
tmp=$HOME/tmp
output=/scratch17/adrian.casais/dsts/ALPs
export RUNNUMBER=$(($seed))
export EVTMAX=$evts
export MAINDIR=$mainDir
export POLARITY=$magnet
 
echo "================================================================================================================================"
echo "Starting on : $(date)"
echo "Running on node : $(hostname)"
echo "Current directory : $(pwd)"
echo "Working directory : $PBS_O_WORKDIR"
echo "Current job ID : $PBS_JOBID"
echo "Current job name : $JOBNAME"
echo "Job index number : $PBS_ARRAYID"
echo "Output directory: $output"
 
#mkdir ${output}
mkdir -p ${tmp}
mkdir -p ${tmp}/${EventType}_${magnet}_${year}_${seed}
cd ${tmp}/${EventType}_${magnet}_${year}_${seed}
 
APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r397/options
DECFILESROOT=/home3/adrian.casais/cmtuser/GaussDev_v54r2/Gen/DecFiles/options
LBMADGRAPH=/home3/adrian.casais/cmtuser/GaussDev_v54r2/Gen/LbMadgraph
export MADGRAPHDATAROOT=/home3/adrian.casais/cmtuser/GaussDev_v54r2/Gen/MadgraphData
#### GAUSS
lb_set_platform x86_64-centos7-gcc9-opt
/home3/adrian.casais/cmtuser/GaussDev_v54r2/run  gaudirun.py ${APPCONFIGOPTS}/Gauss/Beam6500GeV-${magnet}100-2018-nu1.6.py  ${APPCONFIGOPTS}/Gauss/DataType-2018.py ${APPCONFIGOPTS}/Gauss/RICHRandomHits.py ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmNoCuts.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py  ${APPCONFIGOPTS}/Gauss/EnableSpillover-25ns.py  ${DECFILESROOT}/${EventType}.py ${LBMADGRAPH}/options/MadgraphPythia8.py ${mainDir}/extraOptionsGauss.py
find . ! -name 'Gauss.sim' -type f -exec rm -f {} +

# ### BOOLE
lb_set_platform  x86_64-centos7-gcc49-opt
lb-run Boole/v30r4 gaudirun.py ${APPCONFIGOPTS}/Boole/Default.py ${APPCONFIGOPTS}/Boole/EnableSpillover.py ${APPCONFIGOPTS}/Boole/DataType-2015.py ${APPCONFIGOPTS}/Boole/Boole-SetOdinRndTrigger.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsBoole.py
find . ! -name 'Boole.digi' -type f -exec rm -f {} +

# ### MOORE
lb_set_platform x86_64-centos7-gcc62-opt
lb-run Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/L0App/L0AppSimProduction.py ${APPCONFIGOPTS}/L0App/L0AppTCK-0x18a4.py ${APPCONFIGOPTS}/L0App/ForceLUTVersionV8.py ${APPCONFIGOPTS}/L0App/DataType-2017.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL0.py
find . ! -name 'L0output.digi' -type f -exec rm -f {} +

lb-run Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x517a18a4.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt1.py ${APPCONFIGOPTS}/L0App/DataType-2017.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL1.py
# cp HLT1output.digi "${output}/Hlt1Moore_${EventType}_${magnet}_${year}_${PBS_ARRAYID}.digi"
find . ! -name 'HLT1output.digi' -type f -exec rm -f {} +

lb-run Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x617d18a4.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt2.py ${APPCONFIGOPTS}/L0App/DataType-2017.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsMooreL2.py
find . ! -name 'HLT2output.digi' -type f -exec rm -f {} +

# ### BRUNEL
lb_set_platform x86_64-centos7-gcc62-opt
lb-run Brunel/v54r3 gaudirun.py ${APPCONFIGOPTS}/Brunel/DataType-2018.py ${APPCONFIGOPTS}/Brunel/MC-WithTruth.py $APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py $APPCONFIGOPTS/Brunel/ldst.py ${APPCONFIGOPTS}/Brunel/noMergingGenFSR.py ${APPCONFIGOPTS}/Persistency/Compression-ZLIB-1.py ${mainDir}/extraOptionsBrunel.py
find . ! -name 'Brunel.ldst' -type f -exec rm -f {} +

mv Brunel.ldst ${output}/${EventType}_${magnet}_${year}_${seed}.dst
# # ### DAVINCI
# # # source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc49-opt
# # # lb-run DaVinci/v41r4p3 gaudirun.py ${APPCONFIGOPTS}/DaVinci/DV-Stripping28-Stripping-MC-NoPrescaling-DST.py ${APPCONFIGOPTS}/DaVinci/DataType-${year}.py ${APPCONFIGOPTS}/DaVinci/InputType-DST.py ${mainDir}/extraOptionsDaVinci.py

# # # mv 000000.AllStreams.dst "${output}/DaVinci_${EventType}_${magnet}_${year}_${PBS_ARRAYID}.dst"

# # cd $HOME
# # rm -rf ${tmp}/${EventType}_${magnet}_${year}_${seed}
