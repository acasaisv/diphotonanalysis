#include <algorithm>
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolFactory.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "GiGa/GiGaPhysConstructorBase.h"
#include "G4ParticleTable.hh"

#include "G4AxR0.h"

// ============================================================================
/** @class GiGaHiddenValleyParticles 
 *  simple class to construct Hidden Valley particles 
 *
 *  The major properties:
 *  
 *    - "HiddenValley" : list of hidden valley particles to be created.
 *                       the default value contains [ "pivDiag" , "pivUp", "pivDn", "rhovDiag", "rhovUp", "rhovDn", "Zv", "gv", "qv", "qvbar" ] 
 *
 *  It also prints (@ MSG::INFO) the properties of created particles.
 *  
 *  GiGaHiggsParticles used as a template to write this file (Vanya Belyaev).
 *
 *  @author Carlos VAZQUEZ SIERRA carlos.vazquez@cern.ch
 *  @date 2018-02-03
 */

class GiGaHiddenValleyParticles : public GiGaPhysConstructorBase
{
  // ==========================================================================
  /// friend factory for instantiation 
  friend class ToolFactory<GiGaHiddenValleyParticles>;
  // ==========================================================================
protected:
  // ==========================================================================
  /// constructor 
  GiGaHiddenValleyParticles    
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) 
    : GiGaPhysConstructorBase ( type , name , parent )
    , m_hiddenv ()
  {
    m_hiddenv.push_back ( "AxR0" ) ;
    declareProperty 
      ("HiddenValley" , m_hiddenv , 
       "The list of Hidden Valley particles to be instantiated") ;  
  }
  // virtual and protected destcructor 
  virtual ~GiGaHiddenValleyParticles () {} ;
  // ==========================================================================
public:
  // ==========================================================================
  /// construct the particles 
  virtual void ConstructParticle ()    ; // construct the particles 
  /// construct the processed  (empty) 
  virtual void ConstructProcess  () {} ; // construct the processed 
  // ==========================================================================
protected:
  // ==========================================================================  
  /// get the particle property for the given particle name 
  const LHCb::ParticleProperty* pp 
  ( const std::string&    n , 
    LHCb::IParticlePropertySvc* s ) const 
  {
    Assert ( 0 != s , "Invalid ParticleProperty Service") ;
    const LHCb::ParticleProperty* p = s->find ( n ) ;
    Assert ( 0 != p , "No information is available for '" + n + "'") ;
    return p ;
  }
  // ==========================================================================
private:
  // ==========================================================================
  /// The default constructor is disabled 
  GiGaHiddenValleyParticles () ; // The default constructor is disabled
  /// The copy constructor is disabled 
  GiGaHiddenValleyParticles ( const GiGaHiddenValleyParticles& ) ;     // No copy constructor
  /// The assigenement operator is disabled
  GiGaHiddenValleyParticles& operator=( const GiGaHiddenValleyParticles& ) ; // No assignenemt
  // ==========================================================================
private:
  // ==========================================================================
  typedef std::vector<std::string> Strings ;
  /// list of Hidden Valley particles to be instantiated
  Strings    m_hiddenv ; // list of Hidden Valley particles to be instantiated
  // ==========================================================================
};

// ============================================================================
// construct the particles 
// ============================================================================
void GiGaHiddenValleyParticles::ConstructParticle () // construct the particles 
{
  
  LHCb::IParticlePropertySvc* ppSvc = 
    svc<LHCb::IParticlePropertySvc> ("LHCb::ParticlePropertySvc") ;
  
  Strings tmp = m_hiddenv ;
  {
    // ========================================================================
    Strings::iterator it = std::find  ( tmp.begin() , tmp.end() , "AxR0" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* piv = pp ( "AxR0" , ppSvc ) ;
      G4AxR0* pi_v = G4AxR0::Create 
        ( piv -> mass     () ,       
          piv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != pi_v , "Unable to create AxR0" ) ;
      if ( msgLevel ( MSG::INFO )  ) { pi_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
        // ========================================================================
    Assert ( tmp.empty() , "Unknown Hidden Valley particles in the list!" ) ;
    // ========================================================================
  }
}


// ============================================================================
/// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY ( GiGaHiddenValleyParticles )

// The END 
// ============================================================================

