#______________________________________________



fileType     = 'LDST'
evtsPerJob   = 100
nJobs        = 1000
runNumber = 1000000
polarity = "d"
nEvents      = evtsPerJob*nJobs

from random import random
#runNumber = int(random()+random()*10+random()*1e2+random()*1e3+random()*1e4+1e5*random()+1e6*random()+1e7*random()+1)

print('***', nEvents, 'events will be generated ***')



APPCONFIGOPTS = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r393/options"
DECFILESROOT = "/home3/adrian.casais/simulation/dec_options/"
LBPYTHIA8ROOT = "/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v49r8/Gen/LbPythia8"
LBMADGRAPH = "/scratch03/adrian.casais/ALPs-gauss/tmp/GaussDev_v52r2/Gen/LbMadgraph"
mainDir = "/scratch03/adrian.casais/diphotonanalysis/simulation/ALPs/ALPsMC/opts"
# User workspace and release area
WORKSPACE     = "/home3/adrian.casais/simulation/steps_ganga"
RELEASE_AREA  = "/home3/adrian.casais/cmtuser3/"





cond = ''\
       'from Configurables import LHCbApp\n'\
       'LHCbApp().DDDBtag  = "dddb-20170721-3"\n'\
       'LHCbApp().CondDBtag  = "sim-20190430-vc-m{}100"\n'.format(polarity)



#    B r u n e l 

step6 = Job(name="Brunel-Task1")
step6.application = GaudiExec(directory="/home3/adrian.casais/cmtuser/BrunelDev_v54r3", platform = "x86_64-centos7-gcc62-opt")
step6.application.options =  [
                                APPCONFIGOPTS + "/Brunel/DataType-2018.py",
				APPCONFIGOPTS + "/Brunel/MC-WithTruth.py",

                                APPCONFIGOPTS + "/Brunel/SplitRawEventOutput.4.3.py",
                         	APPCONFIGOPTS + "/Persistency/Compression-ZLIB-1.py",
                                #APPCONFIGOPTS + "/Brunel/ldst.py"
                         ]
step6.backend=Dirac()
myfiles = [ DiracFile(lfn=f) for f in mylfns ]
datalink = LHCbCompressedDataset(myfiles)
step6.inputdata = datalink
step6.splitter=SplitByFiles(filesPerJob=1)
step6.application.extraOpts = cond
step6.outputfiles = [DiracFile("*dst"), LocalFile("*.root"), LocalFile("*.xml")]



step6.submit()
