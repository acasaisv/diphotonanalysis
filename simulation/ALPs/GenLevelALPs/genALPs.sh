#!/bin/bash
# Start with qsub -q long -N Majorana10GeV10ps_md -t 1-200 < Majorana10GeV10ps_md.sh


export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_93c/MCGenerators/madgraph5amc/2.6.1.atlas/x86_64-slc6-gcc62-opt/bin/:$PATH

JOBNAME=$(echo $PBS_JOBNAME | rev | cut -c 6- | rev)
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
seed=$1
echo $seed
JOBNAME=default
evts=1000
year=2016
 
magnet=md
EventType=5GeV
 
mainDir=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/opts
genDir=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/GenLevelALPs
#output=/eos/lhcb/user/a/acasaisv/ALPs/output2
output=$PWD
export RUNNUMBER=$(($seed))
export EVTMAX=$evts
export MAINDIR=$mainDir
export POLARITY=$magnet
export GAUSSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v52r2/Sim/Gauss/options 
echo "================================================================================================================================"
echo "Starting on : $(date)"
echo "Running on node : $(hostname)"
echo "Current directory : $(pwd)"
echo "Working directory : $PBS_O_WORKDIR"
echo "Current job ID : $PBS_JOBID"
echo "Current job name : $JOBNAME"
echo "Job index number : $PBS_ARRAYID"
echo "Output directory: $output"
 
mkdir ${output}/${EventType}_${magnet}_${year}_${seed}
cd ${output}/${EventType}_${magnet}_${year}_${seed}
 
APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r369/options
### decfiles xenericos
DECFILESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v30r9/options
### movidas tuas
#DECFILESROOT=/project/bfys/cvazquez/HiddenValleyProductions/productions/hiddenvalley
#####/scratch03/adrian.casais/ALPs-gauss/output##################
LBPYTHIA8ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v49r8/Gen/LbPythia8
#####
LBMADGRAPH=/afs/cern.ch/work/a/acasaisv/ALPs-gauss/gauss-madgraph/GaussDev_v52r2/Gen/LbMadgraph
#### GAUSS
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc62-opt
/afs/cern.ch/work/a/acasaisv/ALPs-gauss/gauss-madgraph/GaussDev_v52r2/run gaudirun.py $GAUSSOPTS/Gauss-2016.py $GAUSSOPTS/GenStandAlone.py  ${LBMADGRAPH}/options/MadgraphPythia8.py   ${mainDir}/makeALPs.py ${mainDir}/extraOptionsGauss.py  $genDir/madgraph.py  



