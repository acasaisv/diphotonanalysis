#!/bin/bash
export mydir=/lustre/LHCb/adrian.casais/sandboxes/491000$1/$2/$3
mkdir -p $mydir && cd $mydir
export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
bash $myroot/production_alp.sh $1 $2 $3
#echo "Writing to /lustre/LHCb/adrian.casais/sandboxes/491000$1/$2/$3"