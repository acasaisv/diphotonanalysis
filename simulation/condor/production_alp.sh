#!/bin/bash
wrapperfunction() {
    source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh 
}

wrapperfunction
#$export CMAKE_PREFIX_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb:/cvmfs/lhcb.cern.ch/lib/lcg/releases:/cvmfs/lhcb.cern.ch/lib/lcg/app/releases:/cvmfs/lhcb.cern.ch/lib/lcg/external:/cvmfs/lhcb.cern.ch/lib/contrib:/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2399/stable/linux-64/lib/python3.9/site-packages/LbDevTools/data/cmake

export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r407/options
export LBPYTHIA8ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v55r4/Gen/LbPythia8
export DECFILESROOT=/home3/adrian.casais/cmtuser/GaussDev_v55r4/Gen/DecFiles
export MADGRAPHDATAROOT=/home3/adrian.casais/cmtuser/GaussDev_v55r4/Gen/MadgraphData
export LBMADGRAPHROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v55r4/Gen/LbMadgraph
export cluster=$2
export proc=$3
export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
export DDDBTag="dddb-20210528-8"
export CondDB="sim-20201113-8-vc-md100-Sim10"
export extraOps=/home3/adrian.casais/diphotonanalysis/simulation/condor/options
# Gauss
#lb-set-platform x86_64-slc6-gcc48-opt
mkdir -p ./prod && rm -rf ./prod/* && cd ./prod
lb-run -c best gauss/v55r4 $myroot/wrap_gauss.sh gaudirun.py ${APPCONFIGOPTS}/Gauss/Beam6500GeV-md100-2018-nu1.6.py ${APPCONFIGOPTS}/Gauss/EnableSpillover-25ns.py ${APPCONFIGOPTS}/Gauss/DataType-2018.py ${APPCONFIGOPTS}/Gauss/RICHRandomHits.py $DECFILESROOT/options/491000$1.py ${LBMADGRAPHROOT}/options/MadgraphPythia8.py $extraOps/optsGauss.py ${APPCONFIGOPTS}/Gauss/G4PL_FTFP_BERT_EmOpt2.py 
find . ! -name 'Gauss.sim' -exec rm -rf {} +

# Boole
#lb-set-platform x86_64-slc6-gcc49-opt
lb-run -c best Boole/v33r3 gaudirun.py ${APPCONFIGOPTS}/Boole/Default.py ${APPCONFIGOPTS}/Boole/EnableSpillover.py ${APPCONFIGOPTS}/Boole/DataType-2015.py ${APPCONFIGOPTS}/Boole/Boole-SetOdinRndTrigger.py $extraOps/optsBoole.py
find . ! -name 'Boole.digi' -exec rm -rf {} +

# Moore L0
# L0Trig0x18a4
#lb-set-platform x86_64-slc6-gcc62-opt
lb-run -c best Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/L0App/L0AppSimProduction.py ${APPCONFIGOPTS}/L0App/L0AppTCK-0x18a4.py ${APPCONFIGOPTS}/L0App/ForceLUTVersionV8.py ${APPCONFIGOPTS}/L0App/DataType-2017.py $extraOps/optsMooreL0.py
find . ! -name 'L0.digi' -exec rm -rf {} +
# Moore HLT1
# Trig0x517a18a4
#lb-set-platform x86_64-slc6-gcc62-opt
lb-run -c best Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x517a18a4.py ${APPCONFIGOPTS}/Moore/DataType-2017.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt1.py $extraOps/optsMooreHLT1.py
find . ! -name 'HLT1.digi' -exec rm -rf {} +

# Moore HLT2
# Trig0x617d18a4
lb-set-platform x86_64-slc6-gcc62-opt
lb-run -c best Moore/v28r3p1 gaudirun.py ${APPCONFIGOPTS}/Moore/MooreSimProductionForSeparateL0AppStep2015.py ${APPCONFIGOPTS}/Conditions/TCK-0x617d18a4.py ${APPCONFIGOPTS}/Moore/DataType-2017.py ${APPCONFIGOPTS}/Moore/MooreSimProductionHlt2.py $extraOps/optsMooreHLT2.py
find . ! -name 'HLT2.digi'  -exec rm -rf {} +
# Brunel
#lb-set-platform x86_64-slc6-gcc62-opt
lb-run -c best Brunel/v54r4 gaudirun.py ${APPCONFIGOPTS}/Brunel/DataType-2018.py ${APPCONFIGOPTS}/Brunel/MC-WithTruth.py ${APPCONFIGOPTS}/Brunel/SplitRawEventOutput.4.3.py $extraOps/optsBrunel.py
find . ! -name 'Brunel.dst' -type f -exec rm -f {} +