#!/bin/bash
wrapperfunction() {
    source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh 
}
wrapperfunction

export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
cd /lustre/LHCb/adrian.casais/sandboxes/491000$1/$2
decay=$1 run=$2 lb-run davinci/v44r11p1 gaudirun.py $myroot/options/DVMergeDST.py

#echo "Merging files from /lustre/LHCb/adrian.casais/sandboxes/491000$1/$2"