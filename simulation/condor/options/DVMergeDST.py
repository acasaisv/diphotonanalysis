from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import (DaVinci, LumiAlgsConf)
import os
DaVinci().Simulation=True
DaVinci().DataType= '2018'
DaVinci().DDDBtag  ="dddb-20210528-8"
DaVinci().CondDBtag  = "sim-20201113-8-vc-md100-Sim10"

DaVinci().InputType = 'DST'
ioh = IOHelper()
#ioh.outStream('PFN:$PWD/myfile.dst')
myfiles = []
decay = os.environ['decay']
run = os.environ['run']
for r,d,f in os.walk(os.environ['PWD']):
    for name in f:
        if name=='Brunel.dst':
            myfiles.append(os.path.join(r,name))
DaVinci().Input = myfiles
root = os.environ['PWD']
name = "DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='REC'".format(root + '/491000{0}_{1}_Sim10a-priv.dst'.format(decay,run))
ics = InputCopyStream('mywriter')
ics.Output = name
DaVinci().UserAlgorithms = [ics]

