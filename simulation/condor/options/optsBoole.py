from Configurables import LHCbApp
import os
dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb


from GaudiConf import IOHelper
data = ['Gauss.sim']
IOHelper('ROOT').inputFiles(data)

from Configurables import Boole
Boole().DatasetName = "Boole"
