from Configurables import LHCbApp
import os

dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb


from Configurables import L0App
L0App().DDDBtag  = dddb
L0App().CondDBtag  = conddb

L0App().Simulation = True
L0App().outputFile = "L0.digi"
L0App().TCK = '0x18a4'

from GaudiConf import IOHelper
data = ["Boole.digi"]
IOHelper('ROOT').inputFiles(data)
