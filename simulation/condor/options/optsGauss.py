import random
from Configurables import LHCbApp
import os
cluster = int(os.environ['cluster'])
proc = 1
if 'proc' in list(os.environ.keys()):
    proc = int(os.environ['proc'])

dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb


from Configurables import Gauss
from Gauss.Configuration import *
LHCbApp().EvtMax = 1000 # edit this to 100

GaussGen = GenInit("GaussGen")
GaussGen.RunNumber        = cluster 
GaussGen.FirstEventNumber = proc*LHCbApp().EvtMax+1

OutputStream("GaussTape").Output = "DATAFILE='PFN:Gauss.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"