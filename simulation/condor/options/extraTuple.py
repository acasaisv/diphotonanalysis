from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import (DaVinci, LumiAlgsConf)
import os
DaVinci().Simulation=True
DaVinci().DataType= '2018'
DaVinci().DDDBtag  ="dddb-20210528-8"
DaVinci().CondDBtag  = "sim-20201113-8-vc-md100-Sim10"

DaVinci().InputType = 'DST'

decay = os.environ['decay']
run = os.environ['run']
DaVinci.Input = ['./491000{0}_{1}_Sim10a-priv.dst'.format(decay,run)]

