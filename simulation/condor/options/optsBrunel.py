from Configurables import LHCbApp
import os
dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb

from GaudiConf import IOHelper
data = ["HLT2.digi"]
IOHelper('ROOT').inputFiles(data)

from Configurables import Brunel
Brunel().DatasetName = "Brunel"
