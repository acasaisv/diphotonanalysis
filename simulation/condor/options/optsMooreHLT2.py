from Configurables import LHCbApp

import os

dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb

from Configurables import Moore

Moore().outputFile = "HLT2.digi"
Moore().InitialTCK = '0x617d18a4'

from GaudiConf import IOHelper
data = ["HLT1.digi"]
IOHelper('ROOT').inputFiles(data)
