from Configurables import LHCbApp
import os

dddb = os.environ['DDDBTag']
conddb=os.environ['CondDB']

LHCbApp().DDDBtag  = dddb
LHCbApp().CondDBtag  = conddb


from Configurables import Moore
Moore().outputFile = "HLT1.digi"
Moore().InitialTCK = '0x517a18a4'

from GaudiConf import IOHelper
data = ["L0.digi"]
IOHelper('ROOT').inputFiles(data)
