# file /home3/adrian.casais/tmp/GaussDev_v55r4/Gen/DecFiles/options/49100048.py generated: Tue, 26 Jul 2022 13:19:56
#
# Event Type: 49100048
#
# ASCII decay Descriptor: pp -> (ALP -> gamma gamma)
#
from Configurables import Generation
Generation().EventType = 49100048
Generation().SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Madgraph" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
#ToolSvc().addTool( EvtGenDecay )
#ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/ALP42gg.dec"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromAxInAcceptance"
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ "AxR0 54 54 0.0 4 0 AxR0 54 1.e-2" ]

#Configure the event type.
from Configurables import (Generation, Special, MadgraphProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions
# Generation options.
Generation().PileUpTool           = "FixedLuminosityForRareProcess"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"
# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
Generation().Special.ProductionTool = "MadgraphProduction"
# Madgraph options.
from Configurables import Gauss
Generation().Special.addTool(MadgraphProduction)
sampleGenToolsOpts = {
    "Commands" :["import model R_axion/",
                 "generate p p > R0 > a a DMS==2 @1",
                 " set wr0 Auto",
                 " set mr0 4.",
                 " set pta 0.0",
                 " set ptl 0.0",
                 " set ptlmax -1",
                 " set etaa -1",
                 " set etaamin 0",
                 " set etal -1",
                 " set etalmin 0",
                 " set etaj -1",
                 " set etajmin 0",
                 " set draa 0",
                 " set draamax -1",
                 " set draj 0",
                 " set drajmax -1",
                 " set lambdaa 1000",
                 " set cgg 10",
                 " set cww 10",
                 " set cbb 16.66667",
                 " set auto_convert_model T"
                 ],
    "DecEff": .15}
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
#Cuts
from Configurables import LoKi__FullGenEventCut, Generation
Generation().addTool( LoKi__FullGenEventCut, "GammasFromAxInAcceptance" )
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/GammasFromAxInAcceptance"
tracksInAcc = Generation().GammasFromAxInAcceptance
tracksInAcc.Code = " count ( isGoodAx) > 0 "
### - HepMC::IteratorRange::descendants   4
tracksInAcc.Preambulo += [
      "from GaudiKernel.SystemOfUnits import GeV, mrad, MeV"
    , "isAx= (54==GID)"
    ,  "pT = (GPT> 900*MeV)"
    , "P = (GP> 5500*MeV)"
    , "pZ = (GPZ> 0)"
    , "theta= ( (GTHETA < 400.0*mrad) & (GTHETA> 5.0*mrad) )"
    , "isGoodDaughterGamma =  ( ~GVEV ) & theta & ( 'gamma' == GABSID ) & pT & P & pZ"
    , "isGoodAx = isAx & ( GNINTREE( isGoodDaughterGamma, 4) ==2 )"
    # , "isGoodAx = isAx"   
    ]
from Configurables import GenerationToSimulation
GenerationToSimulation("GenToSim").KeepCode = "in_list( GABSID, [ 'AxR0' ] )"

