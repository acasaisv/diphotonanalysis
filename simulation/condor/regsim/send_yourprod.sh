#!/bin/bash
export dagout=/lustre/LHCb/adrian.casais/dag_outfiles/
export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor/regsim
#missing 44 45 46
for i in 11100400;
    do 
        export decay=$i
        mkdir -p $dagout/$decay && cd $dagout/$decay
        for j in {11..100}
            do 
                #then
                    echo $decay
                    export run=$((1000+$j))
                    cp $myroot/production_regsim.dag ./production.$run.dag
                    cp $myroot/*.sh .
                    cp $myroot/*.sub .
                    #cp $myroot/*.py .
                    condor_submit_dag  -f production.$run.dag
        done;
done
