#!/bin/bash
wrapperfunction() {
    source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh 
}
wrapperfunction

export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
export tupleProduction=/home3/adrian.casais/diphotonanalysis/tupleProduction
export davinci=/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1
export myopts=$myroot/options
cd /lustre/LHCb/adrian.casais/sandboxes/$1/$2
cp $tupleProduction/caloPostCalib.py $tupleProduction/makeALPs.py .
decay=$1 run=$2 $davinci/run gaudirun.py $myopts/extraTuple_regsim.py $tupleProduction/etammg_input/etammg.py

#echo "Tupling files from /lustre/LHCb/adrian.casais/sandboxes/491000$1/$2"