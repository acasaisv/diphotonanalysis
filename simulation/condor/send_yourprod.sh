#!/bin/bash
export dagout=/lustre/LHCb/adrian.casais/dag_outfiles/
export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
#missing 44 45 46
for i in {40..47};
    do 
        export decay=$i
        mkdir -p $dagout/$decay && cd $dagout/$decay
        for j in {0..99}
            do 
                if grep -Fxq "/lustre/LHCb/adrian.casais/sandboxes/491000$i/$((1000+$j))" $myroot/missing_files.txt
                then
                    echo $decay
                    export run=$((1000+$j))
                    cp $myroot/production.dag ./production.$run.dag
                    cp $myroot/*.sh .
                    cp $myroot/*.sub .
                    #cp $myroot/*.py .
                    condor_submit_dag  -f production.$run.dag
                fi
        done;
done
