import uproot3 as uproot

f_alp = uproot.open('/scratch47/adrian.casais/ntuples/signal/sim10/49100041_1000-1099_Sim10a-priv.root')
t_alp = f_alp['MCDecayTreeTuple/MCDecayTree']
f_h30 = uproot.open('/home3/adrian.casais/cmtuser/GaussDev_v55r4/a1_test/DVntuple.root')
t_h30 = f_h30['MCDecayTreeTuple/MCDecayTree']

df_alp = t_alp.pandas.df()
df_h30 = t_h30.pandas.df()
df_h30.eval('H_30_M=sqrt(H_30_TRUEP_E**2 - H_30_TRUEPT**2 - H_30_TRUEP_Z**2)',inplace=True)
import matplotlib.pyplot as plt

# plt.hist(df_alp['AxR0_TRUEPT'],bins=30,color='blue',histtype='step',density=True,range=[0,100e3])
# plt.hist(df_h30['H_30_TRUEPT'],bins=30,color='red',histtype='step',density=True,range=[0,100e3])
# plt.show()
plt.hist(df_h30['H_30_M'],bins=30,color='blue',histtype='step',density=True,range=[0,100e3])
plt.show()
