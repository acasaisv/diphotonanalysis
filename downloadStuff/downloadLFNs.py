import sys



from multiprocessing.pool import ThreadPool
from subprocess import call 
def run(cmd):
    return cmd, call(cmd)
    


def prepare_commands(file,output_location): 
    with open(file, 'r') as f:
        myFiles = f.readlines()
        
    myFiles = [i[:-1] for i in myFiles] #remove \n 
    
    def bashCommand(lfn,subdir):
        return "lb-dirac dirac-dms-get-file {0} -D {1}/{2}".format(lfn,output_location,subdir)
    
    cmds = map( bashCommand,myFiles,range(len(myFiles)))
    mycommand = map(lambda x: x.split(" "),cmds)
    # print (list(mycommand)[0])
    return list(mycommand)

def actual_run(runfun,cmds,limit):
    for cmd, rc in ThreadPool(limit).imap_unordered(runfun,cmds):
        if rc != 0:
            print('{cmd} failed with exit status: {rc}'.format(**vars()))


def main():

    root = '/scratch03/adrian.casais/diphotonanalysis/downloadStuff/'
    output_root = '/scratch47/adrian.casais/ntuples/turcal/beforemerge'
    mycmds=prepare_commands(file=root+'turcal_lfns.txt',output_location=output_root)
    limit = 5
    # print(mycmds)
    actual_run(run,mycmds,limit=20)
if __name__ == '__main__':
    
    main()
