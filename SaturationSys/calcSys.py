import pandas as pd
import numpy as np
from helpers import get_error,get_error_w,get_ALPsdf
from hep_ml.reweight import GBReweighter
import matplotlib.pyplot as plt
from uncertainties import ufloat


def test_saturation(df,dfMC,prefix,pts,regions,cut='mu2_L0MuonDecision_Dec and (nSPDHits <200)'):
    #cut = '(B_Hlt1TrackMVADecision_Dec and nSPDHits <450)'
    effs = {}
    effsMC = {}
    for ipt in range(len(pts)-1):
        pt = pts[ipt],pts[ipt+1]
        effs[pt]={}
        effsMC[pt] = {}
        for ireg in range(len(regions)-1):
            reg = regions[ireg],regions[ireg+1] 
            mydf = df.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_PP_CaloNeutralID > {reg[0]} and gamma_PP_CaloNeutralID < {reg[1]}')
            mydfMC = dfMC.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_PP_CaloNeutralID > {reg[0]} and gamma_PP_CaloNeutralID < {reg[1]}')
            print(f'Len of mydf: {len(mydf)}')
            eff = mydf.query(f'gamma_{prefix}_Saturation>0 and {cut}')['sweights'].sum()/mydf.query(cut)['sweights'].sum()
            err_eff = get_error_w(mydf.query(f'gamma_{prefix}_Saturation>0 and {cut}')['sweights'],mydf.query(cut)['sweights'])
            MCeff = mydfMC.query(f'gamma_{prefix}_Saturation>0 and {cut}')['sweights'].sum()/mydfMC.query(cut)['sweights'].sum()
            err_MCeff = get_error_w(mydfMC.query(f'gamma_{prefix}_Saturation>0 and {cut}')['sweights'],mydfMC.query(cut)['sweights'])
            #print(f"pt: {pt}. reg: {reg} Data eff: {100.*eff:.3f}+-{100.*err_eff:.3f}, MC eff: {100.*MCeff:.3f} +- {100.*err_MCeff:.3f}")
            effs[pt][reg] = ufloat(eff,err_eff)
            effsMC[pt][reg] = ufloat(MCeff,err_MCeff)
        
    return effs,effsMC
def test_on_alps(alps,effs,effsMC,pts,regions):
    myalps = alps.query('gamma_PT >3000 and gamma0_PT > 3000 and nSPDHits < 200')
    eff = 0
    effMC = 0
    for ipt in range(len(pts)-1):
        pt = pts[ipt],pts[ipt+1]
        for ireg in range(len(regions)-1):
            reg = regions[ireg],regions[ireg+1]
            alps_ = myalps.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_CaloHypo_CellID > {reg[0]} and gamma_CaloHypo_CellID < {reg[1]}')
            alps0_ = myalps.query(f'gamma0_PT >{pt[0]} and gamma0_PT < {pt[1]} and gamma_CaloHypo_CellID > {reg[0]} and gamma_CaloHypo_CellID < {reg[1]}')
            eff_alp_ = 100.*alps_.query('gamma_CaloHypo_Saturation >0').shape[0]/alps_.shape[0]
            err_alp_ = 100.*get_error(alps_.query('gamma_CaloHypo_Saturation >0').shape[0],alps_.shape[0])
            frac = alps_.shape[0]/alps.shape[0]
            frac0 = alps0_.shape[0]/alps.shape[0]
            if effs[pt][reg].n>0:
                eff += frac*(effs[pt][reg].n)
            if effsMC[pt][reg].n>0:
                effMC += frac*(effsMC[pt][reg].n)
            print(f'pt:{pt}. region: {reg} Eff on alp: {eff_alp_:.2f} +- {err_alp_:.2f}. frac %: {100.*frac:.2f} Data eff: {100.*effs[pt][reg].n:.2f}+-{100.*effs[pt][reg].s:.2f}, MC eff: {100.*effsMC[pt][reg].n:.2f} +- {100.*effsMC[pt][reg].s:.2f}')
            #print(f"pt: {pt}. reg: {reg} ")
    out_eff = eff + eff -eff*eff
    out_effMC = effMC + effMC - effMC*effMC
    mctruth_eff = myalps.query('gamma_CaloHypo_Saturation >0').shape[0]/myalps.shape[0]
    mctruth_eff0 = myalps.query('gamma0_CaloHypo_Saturation >0').shape[0]/myalps.shape[0]
    mc_truth_fulleff = myalps.query('gamma_CaloHypo_Saturation >0 or gamma0_CaloHypo_Saturation >0').shape[0]/myalps.shape[0]
    out_mc_eff = mctruth_eff + mctruth_eff0 - mctruth_eff*mctruth_eff0 

    print(f'Sampling from data{100.*out_eff:.2f} Sampling from MC {100.*out_effMC:.2f}.Efficiency from clusters {100.*out_mc_eff:.2f}')


def reweight_mc(df,dfMC,name='B'):
    
    vars = [f'{name}_PT',f'{name}_ETA',
            #'gamma_PP_IsNotH'
            #'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    reweighter.fit(original=dfMC[vars], original_weight=dfMC['sweights'],target=df[vars], target_weight=df['sweights'],)
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return dfMC
def plot_bin(df,dfMC,alps,pt,reg):
    mydf = df.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_PP_CaloNeutralID > {reg[0]} and gamma_PP_CaloNeutralID < {reg[1]}')
    mydfMC = dfMC.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_PP_CaloNeutralID > {reg[0]} and gamma_PP_CaloNeutralID < {reg[1]}')
    myalps = alps.query(f'gamma_PT >{pt[0]} and gamma_PT < {pt[1]} and gamma_CaloHypo_CellID > {reg[0]} and gamma_CaloHypo_CellID < {reg[1]}')

    #plt.hist(mydf['gamma_CaloHypo_Saturation'],histtype='step',color='red',weights=mydf['sweights'],density=True,label='data',bins=20)
    plt.hist(mydfMC['gamma_PT'],histtype='step',color='blue',weights=mydfMC['sweights'],density=True,label='mc',bins=20)
    plt.hist(myalps['gamma_PT'],histtype='step',color='green',density=True,label='alps',bins=20)
    plt.legend()
    plt.savefig(f'comparebin-{pt}{reg}.pdf')


if __name__=='__main__':

    root = '/scratch47/adrian.casais/ntuples/turcal/'
    #mydf =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')
    #mydfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='dfMC')


    
    # mydf = pd.read_hdf(root+'/phigData.h5',key='df')
    # mydfMC = pd.read_hdf(root+'/phigMC.h5',key='df')
    # mydfMC = reweight_mc(mydf,mydfMC)
    # plt.hist(mydf['gamma_PT'],bins=50,histtype='step',color='red',density=True,label='data',weights=mydf['sweights'])
    # plt.hist(mydfMC['gamma_PT'],bins=50,histtype='step',color='blue',density=True,label='mc',weights=mydfMC['sweights'])
    # plt.legend()
    # plt.savefig('compare_pt.pdf')

    mydf =pd.read_hdf(root+'etammgTurcalData.h5',key='df')
    mydfMC =pd.read_hdf(root+'etammgTurcalHardPhotonMC.h5',key='df')
    #mydf = pd.read_hdf(root+'/phigData.h5',key='df')
    #mydfMC = pd.read_hdf(root+'/phigMC.h5',key='df')
    #mydf =pd.read_hdf(root+'b2kstgamma.h5',key='df')
    #mydfMC =pd.read_hdf(root+'b2kstgamma.h5',key='dfMC')
    mydfMC = reweight_mc(mydf,mydfMC,name='eta')
    alps = get_ALPsdf(50,stripping=True,background = False)
    plot_bin(mydf,mydfMC,alps,[11.8e3,12.5e3],[37.e3,41.e3])
    pts = [11800,12500,
    15000,
    np.inf
    #15000,18000,20000,np.inf
    ]
    #pts=[0,11.e3,14.e3,np.inf]
    regions = [0,37.e3,
    41.e3,np.inf]
    effs,effsMC=test_saturation(mydf,mydfMC,'CaloHypo',pts,regions)
    test_on_alps(alps,effs,effsMC,pts,regions)

