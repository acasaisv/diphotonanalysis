#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh

export num1=20000
export res=$((num1 + $1))

/afs/cern.ch/work/a/acasaisv/ALPs-gauss/simulation-hardPi0.sh $res
#lb-run -c best DaVinci/v50r1 python /afs/cern.ch/work/a/acasaisv/ALPs-gauss/mergeDsts.py $1


exit
