#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh

export num1=11040001
export res=$((num1 + $1))

/afs/cern.ch/work/a/acasaisv/ALPs-gauss/simulation.sh $res
#lb-run -c best DaVinci/v50r1 python /afs/cern.ch/work/a/acasaisv/ALPs-gauss/mergeDsts.py $1


exit
