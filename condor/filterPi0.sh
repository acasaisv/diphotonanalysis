#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh -c x86_64-slc6-gcc62-opt
cd /tmp/acasaisv
mkdir hardPi0_$1
cd hardPi0_$1
/afs/cern.ch/work/a/acasaisv/ALPs-gauss/hardPi0/DaVinciDev_v44r10p2/run python /afs/cern.ch/work/a/acasaisv/ALPs-gauss/hardPi0/filterStrippingBs.py $1
xrdcp Bs2ggStripping.Leptonic.dst root://castorlhcb.cern.ch//castor/cern.ch/user/a/acasaisv/dsts/ALPs/49000005_md_2017/filtered/hardPi0_FilteredBs2ggStrip_$1.ldst
rm *.dst
exit
