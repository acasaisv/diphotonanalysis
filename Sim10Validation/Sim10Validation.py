from ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from kinematic_bins import pTs,etas
import pickle

def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)


def get_df(name,entrystop=0):

    root = '/scratch47/adrian.casais/ntuples/signal'
    f_s = uproot.open(root + name)
    t_s = f_s['B2GammaGammaTuple/DecayTree']
    
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            'gamma_CaloHypo_X',
            'gamma_CaloHypo_Y',
            'gamma0_CaloHypo_X',
            'gamma0_CaloHypo_Y',
            'gamma_CaloHypo_HypoE',
            'gamma_CaloHypo_HypoEt',
            'gamma0_CaloHypo_HypoE',
            'gamma0_CaloHypo_HypoEt',
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'B_s0_MM',
            'B_s0_PT',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
            'gamma_CaloHypo_isNotH',
            'gamma_PP_IsNotH',
            'gamma0_CaloHypo_isNotH',
            'gamma_CaloHypo_isNotE',
            'gamma_PP_IsNotE',
            'gamma0_CaloHypo_isNotE',
            'gamma_CaloHypo_isPhoton',
            'gamma0_CaloHypo_isPhoton',
            'B_s0_1.00_cc_asy_PT',
            'B_s0_1.35_cc_asy_PT',
            'B_s0_1.70_cc_asy_PT',
            'gamma0_1.00_cc_asy_PT',
            'gamma0_1.35_cc_asy_PT',
            'gamma0_1.70_cc_asy_PT',
            'gamma_1.00_cc_asy_PT',
            'gamma_1.35_cc_asy_PT',
            'gamma_1.70_cc_asy_PT',
            'nVeloTracks',
            'nUpstreamTracks',
            'nLongTracks',
            
            'nTracks',
            'nSPDHits',
            'PVX',
            'PVY',
            'PVZ'


            ]
    truth = "(abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22) & abs(B_s0_TRUEID)==531"
    if not entrystop:
        df = t_s.pandas.df(branches=vars)
    else:
        df = t_s.pandas.df(branches=vars,entrystop=entrystop)
    df.query(truth,inplace=True)
    df.query('subentry==0',inplace=True)
    df['weights']=1
    return df


def nearest_rect(n):
    sqn = np.sqrt(n)
    for i in range(2,int(sqn)+2):
        if not n % i:
            return (i,int(n/i))
    
def recalc_mass(df,zTrigger=12300,gamma0_factor=1,gamma_factor=1):
    df.eval('zTrigger = {}'.format(zTrigger),inplace=True)
    
    df.eval('tx  = gamma_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty  = gamma_CaloHypo_Y/zTrigger',inplace=True)
    df.eval('tx0  = gamma0_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty0  = gamma0_CaloHypo_Y/zTrigger',inplace=True)

    df.eval('tx  = gamma_PX/gamma_PZ',inplace=True)
    df.eval('ty  = gamma_PY/gamma_PZ',inplace=True)
    df.eval('tx0  = gamma0_PX/gamma0_PZ',inplace=True)
    df.eval('ty0  = gamma0_PY/gamma0_PZ',inplace=True)
    df.eval('gamma_EX = {}*gamma_PT * tx*sqrt(1/(tx**2+ty**2))'.format(gamma_factor),inplace=True)
    df.eval('gamma_EY = {}*gamma_PT * ty*sqrt(1/(tx**2+ty**2))'.format(gamma_factor),inplace=True)
    df.eval('gamma_EZ = {}*gamma_PT * 1/sqrt(tx**2+ty**2)'.format(gamma_factor),inplace=True)
    
    df.eval('gamma0_EX = {}*gamma0_PT * tx0*sqrt(1/(tx0**2+ty0**2))'.format(gamma0_factor),inplace=True)
    df.eval('gamma0_EY = {}*gamma0_PT * ty0*sqrt(1/(tx0**2+ty0**2))'.format(gamma0_factor),inplace=True)
    df.eval('gamma0_EZ = {}*gamma0_PT * 1/sqrt(tx0**2+ty0**2)'.format(gamma0_factor),inplace=True)

    df.eval('gamma_E = sqrt(gamma_EX**2 + gamma_EY**2 + gamma_EZ**2)',inplace=True)
    df.eval('gamma0_E = sqrt(gamma0_EX**2 + gamma0_EY**2 + gamma0_EZ**2)',inplace=True)

    df.eval('costheta = (gamma_EX*gamma0_EX + gamma_EY*gamma0_EY + gamma_EZ*gamma0_EZ)/(gamma_E*gamma0_E)',inplace=True)
    df.eval('B_s0_M_recal = sqrt(2*gamma_E*gamma0_E*(1-costheta))',inplace=True)

    return df

def recalc_mass2(df,gamma0_factor,gamma_factor):
    df.eval('costheta = (gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_P*gamma0_P)',inplace=True)

    df.eval('B_s0_M_recal = sqrt(2*{0}*{1}*gamma_P*gamma0_P*(1-costheta))'.format(gamma0_factor,gamma_factor),inplace=True)
    return df


def reweight(df09,df10,vars=['nSPDHits']):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    
    #reweighter = GBReweighter(max_depth=2,gb_args={'subsample': 0.5})
    reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=df10[vars], target=df09[vars])
    df10['weights']= reweighter.predict_weights(df10[vars],original_weight=df10['weights'])
    return
    
def hlt1_checks_plots(df09,df10,gamma0_scal,gamma_scal):
    df09.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) and B_s0_Hlt1B2GammaGammaDecision_TOS and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',inplace=True)
    df10.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) and B_s0_Hlt1B2GammaGammaDecision_TOS and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',inplace=True)
    vars = [#'B_s0_M',
            #'B_s0_PT',
            'gamma0_PT',
            'gamma_PT',
            #'gamma0_P',
            #'gamma_P',
            #'gamma0_PZ',
            #'gamma_PZ',
            'gamma0_CaloHypo_isNotH',
            'gamma0_CaloHypo_isNotE',
            'gamma0_CaloHypo_isPhoton',
            'gamma_CaloHypo_isNotH',
            'gamma_CaloHypo_isNotE',
            'gamma_CaloHypo_isPhoton',
            #'B_s0_1.00_cc_asy_PT',
            #'B_s0_1.35_cc_asy_PT',
            #'B_s0_1.70_cc_asy_PT',
            'gamma0_1.00_cc_asy_PT',
            'gamma0_1.35_cc_asy_PT',
            'gamma0_1.70_cc_asy_PT',
            'gamma_1.00_cc_asy_PT',
            'gamma_1.35_cc_asy_PT',
            'gamma_1.70_cc_asy_PT',
            "nSPDHits",
            'nVeloTracks',
            'nUpstreamTracks',
            'nLongTracks',
            'nTracks',
            #'PVX',
            #'PVY',
            #'PVZ',
            #'gamma0_CaloHypo_X',
            #'gamma0_CaloHypo_Y',
            #'gamma_CaloHypo_X',
            #'gamma_CaloHypo_Y',

            
            ]
    n_plots = len(vars)+1
    plots_fig = 6
    n_figs = int(n_plots/plots_fig)+1

    

    figs, ax_flat = [],np.array([])
    for i in range(n_figs):
        fig,ax = plt.subplots(*nearest_rect(plots_fig),figsize=(16,9),dpi=80)
        figs.append(fig)
        ax_flat = np.append(ax_flat,ax.flatten())
        
    
    #ax_flat =ax.flatten()
    alpha = 0.7

    ax_flat[0].set_title("Candidate mass: rescaled")
    
    ax_flat[0].set_xlabel('MeV',loc='right')
    ax_flat[0].hist(df09['B_s0_M_recal'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[0].hist(df10['B_s0_M_recal'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[0].legend(['Sim09','Sim10'],bbox_to_anchor=[0.05,0.95])
    
    ax_flat[1].set_title("gamma0 PT: rescaled")
    ax_flat[1].set_xlabel('MeV',loc='right')
    ax_flat[1].hist(df09['gamma0_PT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[1].hist(gamma0_scal*df10['gamma0_PT'],bins=50,density=True,alpha=alpha,histtype='step')

    ax_flat[2].set_title("gamma PT: rescaled")
    ax_flat[2].set_xlabel('MeV',loc='right')
    ax_flat[2].hist(df09['gamma_PT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[2].hist(gamma_scal*df10['gamma_PT'],bins=50,density=True,alpha=alpha,histtype='step')



    ax_flat[3].set_title('B_s0_M')
    ax_flat[3].set_xlabel('MeV',loc='right')
    ax_flat[3].hist(df09['B_s0_M'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[3].hist(df10['B_s0_M'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))

    for i,var in zip( range(4,len(vars)+3),vars):
        if 'PT' in var and not 'asy' in var:
            ax_flat[i].set_xlabel('MeV',loc='right')
        ax_flat[i].set_title(var)
        ax_flat[i].hist(df09[var],bins=50,density=True,alpha=alpha,histtype='step')
        ax_flat[i].hist(df10[var],bins=50,density=True,alpha=alpha,histtype='step')

   
    for i in range(len(figs)):
        figs[i].savefig('./validationplot_{}.pdf'.format(i))
    #plt.savefig('/home3/adrian.casais/hlt1_distribs{}.pdf'.format(id))
    
    plt.show()
def hlt1_checks_plots_reweight(df09,df10):
    df09.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) and B_s0_Hlt1B2GammaGammaDecision_TOS and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',inplace=True)
    df10.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) and B_s0_Hlt1B2GammaGammaDecision_TOS and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',inplace=True)
    vars = [#'B_s0_M',
            #'B_s0_PT',
            'gamma0_PT',
            'gamma_PT',
            #'gamma0_P',
            #'gamma_P',
            #'gamma0_PZ',
            #'gamma_PZ',
            'gamma0_CaloHypo_isNotH',
            'gamma0_CaloHypo_isNotE',
            'gamma0_CaloHypo_isPhoton',
            'gamma_CaloHypo_isNotH',
            'gamma_CaloHypo_isNotE',
            'gamma_CaloHypo_isPhoton',
            #'B_s0_1.00_cc_asy_PT',
            #'B_s0_1.35_cc_asy_PT',
            #'B_s0_1.70_cc_asy_PT',
            'gamma0_1.00_cc_asy_PT',
            'gamma0_1.35_cc_asy_PT',
            'gamma0_1.70_cc_asy_PT',
            'gamma_1.00_cc_asy_PT',
            'gamma_1.35_cc_asy_PT',
            'gamma_1.70_cc_asy_PT',
            "nSPDHits",
            'nVeloTracks',
            'nUpstreamTracks',
            'nLongTracks',
            'nTracks',
            #'PVX',
            #'PVY',
            #'PVZ',
            #'gamma0_CaloHypo_X',
            #'gamma0_CaloHypo_Y',
            #'gamma_CaloHypo_X',
            #'gamma_CaloHypo_Y',

            
            ]
    n_plots = len(vars)+1
    plots_fig = 6
    n_figs = int(n_plots/plots_fig)+1

    

    figs, ax_flat = [],np.array([])
    for i in range(n_figs):
        fig,ax = plt.subplots(*nearest_rect(plots_fig),figsize=(16,9),dpi=80)
        figs.append(fig)
        ax_flat = np.append(ax_flat,ax.flatten())
        
    
    #ax_flat =ax.flatten()
    alpha = 0.7

    ax_flat[0].set_title("Candidate mass: rescaled")
    
    ax_flat[0].set_xlabel('MeV',loc='right')
    ax_flat[0].hist(df09['B_s0_M'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[0].hist(df10['B_s0_M'],weights=df10['weights'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[0].legend(['Sim09','Sim10'],bbox_to_anchor=[0.05,0.95])
    
    ax_flat[1].set_title("gamma0 PT: rescaled")
    ax_flat[1].set_xlabel('MeV',loc='right')
    ax_flat[1].hist(df09['gamma0_PT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[1].hist(df10['gamma0_PT'],weights=df10['weights'],bins=50,density=True,alpha=alpha,histtype='step')

    ax_flat[2].set_title("gamma PT: rescaled")
    ax_flat[2].set_xlabel('MeV',loc='right')
    ax_flat[2].hist(df09['gamma_PT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[2].hist(df10['gamma_PT'],weights=df10['weights'],bins=50,density=True,alpha=alpha,histtype='step')



    ax_flat[3].set_title('B_s0_M')
    ax_flat[3].set_xlabel('MeV',loc='right')
    ax_flat[3].hist(df09['B_s0_M'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))
    ax_flat[3].hist(df10['B_s0_M'],bins=50,density=True,alpha=alpha,histtype='step',range=(4000,6500))

    for i,var in zip( range(4,len(vars)+3),vars):
        if 'PT' in var and not 'asy' in var:
            ax_flat[i].set_xlabel('MeV',loc='right')
        ax_flat[i].set_title(var)
        ax_flat[i].hist(df09[var],bins=50,density=True,alpha=alpha,histtype='step')
        ax_flat[i].hist(df10[var],weights=df10['weights'],bins=50,density=True,alpha=alpha,histtype='step')

   
    for i in range(len(figs)):
        figs[i].savefig('./validationplot_{}.pdf'.format(i))
    #plt.savefig('/home3/adrian.casais/hlt1_distribs{}.pdf'.format(id))
    
    plt.show()

def print_trigger(df):
    'B_s0_L0PhotonDecision_TOS',
    'B_s0_L0ElectronDecision_TOS',
    'B_s0_Hlt1B2GammaGammaDecision_TOS',
    'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
    'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
    print("L0")
    effl0 = len(df.query('B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS'))/len(df)
    print("{0:.2f} +- {1:.2f}%".format(100.*effl0,100*get_error(effl0,len(df))))
    print("HLT1")
    effhlt1=len(df.query('(B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and (B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'))/len(df.query('B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS'))
    print("{0:.2f} +- {1:.2f}%".format(100.* effhlt1,100.*get_error(effhlt1,len(df.query('B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS')))))
    print("HLT2")
    effhlt2=len(df.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and (B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'))/len(df.query('(B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and (B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'))
    print("{0:.2f} +- {1:.2f}%".format(100.*effhlt2,100.*get_error(effhlt2,len(df.query('(B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and (B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)')))))
    
if __name__ == '__main__':
    df10 = get_df('/b2gg-sim10.root',entrystop=500000)
    df09 = get_df('/b2gg-sim09.root',entrystop=len(df10))
    reweight(df09,df10,['nSPDHits'])
    #df09=df09.sample(frac = 0.2)
    print_trigger(df09)
    print_trigger(df10)
    print('Higher pt photon mean. Sim09: {0}, Sim10: {1}'.format(np.mean(df09['gamma_PT']),np.mean(df10['gamma_PT'])))
    
    gamma_scal = np.mean(df09['gamma_PT'])/np.mean(df10['gamma_PT'])
    gamma0_scal = np.mean(df09['gamma0_PT'])/np.mean(df10['gamma0_PT'])

    gamma_scal2 = np.mean(df09['gamma_P'])/np.mean(df10['gamma_P'])
    gamma0_scal2 = np.mean(df09['gamma0_P'])/np.mean(df10['gamma0_P'])



    #df09 = recalc_mass2(df09,1,1)
    #df10 = recalc_mass2(df10,gamma0_scal2,gamma_scal2)

    df09 = recalc_mass(df09,12300,1,1)
    #df10 = recalc_mass(df10,12300,gamma0_scal,gamma_scal)
    df10 = recalc_mass(df10,12300,1,1)
    
    #hlt1_checks_plots(df09,df10,gamma0_scal,gamma_scal)
    hlt1_checks_plots_reweight(df09,df10)
    
