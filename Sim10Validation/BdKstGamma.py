from ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from Sim10Validation import get_df
import pickle

def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)


def get_df_bdkst(name):


    f_s = uproot.open(name)
    t_s = f_s['kpiGTupleMC/DecayTree']
    
    vars = ['B_M',
            'Kst_892_0_PX',
            'Kst_892_0_PY',
            'Kst_892_0_PZ',
            'Kst_892_0_PE',
            
            'gamma_PX',
            'gamma_PY',
            'gamma_PZ',
            'gamma_PE',

            
            'gamma_PT',
            'Kst_892_0_PT',

            #'gamma_CaloHypo_X',
            #'gamma_CaloHypo_Y',

            


            ]
    truth = "abs(gamma_MC_MOTHER_ID) == 511 & abs(gamma0_MC_MOTHER_ID) == 511 & gamma_TRUEID==22 & abs(Kst_892_0_TRUEID)==313 & abs(B_TRUEID)==511"
    df = t_s.pandas.df(branches=vars)
    return df
def recalc_mass_bd(df,gamma_factor = 1.,zTrigger=12300):
    df.eval('zTrigger = {}'.format(zTrigger),inplace=True)
    
    df.eval('tx  = gamma_PX/gamma_PZ',inplace=True)
    df.eval('ty  = gamma_PY/gamma_PZ',inplace=True)

    

    df.eval('gamma_EX = {}*gamma_PT * tx*sqrt(1/(tx**2+ty**2))'.format(gamma_factor),inplace=True)
    df.eval('gamma_EY = {}*gamma_PT * ty*sqrt(1/(tx**2+ty**2))'.format(gamma_factor),inplace=True)
    df.eval('gamma_EZ = {}*gamma_PT * 1/sqrt(tx**2+ty**2)'.format(gamma_factor),inplace=True)
    
    df.eval('gamma_E = sqrt(gamma_EX**2 + gamma_EY**2 + gamma_EZ**2)',inplace=True)
    
    df.eval('B_M_recalc = sqrt( (Kst_892_0_PE +gamma_E)**2 - (Kst_892_0_PX +gamma_EX)**2 -(Kst_892_0_PY +gamma_EY)**2 -(Kst_892_0_PZ +gamma_EZ)**2)',inplace=True)
    return df

def plot_recalc_m(df):
    plt.hist(df['B_M'],bins=75,histtype='step',density=True,label ='Original mass')
    plt.hist(df['B_M_recalc'],bins=75,histtype='step',density=True,label='Corrected mass')
    plt.legend()
    plt.show()


def compare_bd_bs(dfbd,dfbs):
    plt.hist(dfbd['gamma_PT'],bins=50,histtype='step',density=True,label='BdKstgamma')
    plt.hist(dfbs['gamma_PT'],bins=50,histtype='step',density=True,label='Bs2GG')
    plt.legend()
    plt.show()
    
    
if __name__ == '__main__':
    df09 = get_df('/b2gg-sim09.root')
    df10 = get_df('/b2gg-sim10.root')

    df09bd=get_df_bdkst('/scratch47/adrian.casais/ntuples/BKstGammaRad/rad_2hg_tuple-sim09.root')
    df10bd=get_df_bdkst('/scratch47/adrian.casais/ntuples/BKstGammaRad/rad_2hg_tuple-sim10.root')
    
    print('B2gg:Higher pt photon mean. Sim09: {0}, Sim10: {1}'.format(np.mean(df09['gamma_PT']),np.mean(df10['gamma_PT'])))
    print('B2KstG:Higher pt photon mean. Sim09: {0}, Sim10: {1}'.format(np.mean(df09bd['gamma_PT']),np.mean(df10bd['gamma_PT'])))
    gamma_scal = np.mean(df09['gamma_PT'])/np.mean(df10['gamma_PT'])
    gamma0_scal = np.mean(df09['gamma0_PT'])/np.mean(df10['gamma0_PT'])
    gammabd_scal = np.mean(df09bd['gamma_PT'])/np.mean(df10bd['gamma_PT'])

    print("B2gg: subleading phtoon scaling {:.2f} %".format(100*gamma0_scal))
    print("B2gg: leading phtoon scaling {:.2f} %".format(100*gamma_scal))
    print("B2KstGamma: photon scaling {:.2f} %".format(100*gammabd_scal))

    df10bd = recalc_mass_bd(df10bd,gamma_factor=gammabd_scal)

    #plot_recalc_m(df10bd)

    plt.hist(df09bd['B_M'],bins=75,histtype='step',density=True,label ='Sim 09')
    plt.hist(df10bd['B_M'],bins=75,histtype='step',density=True,label='Sim10')
    plt.hist(df10bd['B_M_recalc'],bins=75,histtype='step',density=True,label='Sim10 rescaled')
    plt.legend()
    plt.show()

    compare_bd_bs(df09bd,df09)

    # df10['gamma_PT_scal']=gamma_scal*df10['gamma_PT']
    # df10['gamma0_PT_scal']=gamma0_scal*df10['gamma0_PT']
    # df09['gamma_PT_scal']=df09['gamma_PT']
    # df09['gamma0_PT_scal']=df09['gamma0_PT']

    # df09 = recalc_mass(df09,12300)
    # df10 = recalc_mass(df10,12300)
    # hlt1_checks_plots(df09,df10)
