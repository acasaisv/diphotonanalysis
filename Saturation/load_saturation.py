import sys
import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
from sklearn.metrics import roc_auc_score

import lhcbStyle
lhcbStyle.setLHCbStyle()


import json

import ROOT as R
R.RooMsgService.instance().setGlobalKillBelow(R.RooFit.ERROR)
R.RooMsgService.instance().setSilentMode(True)
R.gROOT.SetBatch()

variables =[
	'gamma_CaloHypo_Saturation',
	'gamma_PT',
	'gamma_ShowerShape',
	'gamma_PP_CaloNeutralHcal2Ecal',
	'gamma_PP_CaloNeutralID',
	'gamma_PP_IsNotH',
	'gamma_PP_IsNotE',
	'gamma_PP_IsPhoton'
	]
variables_2 =[
	'gamma0_CaloHypo_Saturation',
	'gamma0_PT',
	'gamma0_ShowerShape',
	'gamma0_PP_CaloNeutralHcal2Ecal',
	'gamma0_PP_CaloNeutralID',
	'gamma0_PP_IsNotH',
	'gamma0_PP_IsNotE',
	'gamma0_PP_IsPhoton'
	]

additional_vars = [	'B_s0_L0ElectronDecision_TOS',
					'B_s0_L0PhotonDecision_TOS',
					'B_s0_Hlt1B2GammaGammaDecision_TOS',
					'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
					'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
					'B_s0_M',
					'gamma_TRUEID',
					'gamma0_TRUEID',
					'B_s0_TRUEID',
					'B_s0_TRUEP_E',
					'B_s0_TRUEP_X',
					'B_s0_TRUEP_Y',
					'B_s0_TRUEP_Z'
					]

replace_dict = {}
for var in variables_2:
	replace_dict[var]=var.replace("gamma0", "gamma")

#cuts = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_TRUEID==22 and gamma0_TRUEID==22 and B_s0_TRUEID==54' 
cuts = 'gamma_PT>3000 & gamma0_PT>3000 & gamma_TRUEID==22 & gamma0_TRUEID==22 & B_s0_TRUEID==54'

with open("/scratch03/titus.mombacher/gammagamma/Saturation_classifier.pkl", "rb") as infile:
	clf = pickle.load(infile)

if not (sys.argv[1] in ["49100040","49100041","49100042","49100043","49100044","49100045","49100046","49100047","49100048","49100049","49100050", "49100051"]):
	print("File not found:",sys.argv[1])
	sys.exit(1)

for file in [sys.argv[1]]:

	events_alt = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/"+file+"_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree")

	# cuts = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT > 3000 and gamma0_PT > 3000'
	df_alt_raw = events_alt.arrays(variables+variables_2+additional_vars, library="pd")

	df_alt = df_alt_raw.query(cuts)

	X_1 = df_alt.drop(columns=additional_vars+variables_2+["gamma_CaloHypo_Saturation"])
	y_1 = (df_alt['gamma_CaloHypo_Saturation']>0)
	X_2 = df_alt.drop(columns=additional_vars+variables+["gamma0_CaloHypo_Saturation"])
	y_2 = (df_alt['gamma0_CaloHypo_Saturation']>0)
	X_2.rename(columns=replace_dict,inplace=True)

	scores_1 = clf.decision_function(X_1)
	scores_2 = clf.decision_function(X_2)
	print("############ Results of", file)
	print(clf.score(X_1,y_1), clf.score(X_2,y_2))
	print("ROC AUC:", roc_auc_score(y_1, scores_1), roc_auc_score(y_2, scores_2))
	df_alt.insert(0,"gamma_isSaturated",clf.predict_proba(X_1)[:,1])
	df_alt.insert(0,"gamma0_isSaturated",clf.predict_proba(X_2)[:,1])

	eff_true = len(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0'))/(1.0*len(df_alt))
	n_tot = len(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0'))
	width_true = np.std(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0')["B_s0_M"])

	width_none = np.std(df_alt["B_s0_M"])

	nominal_FOM = 1./np.sqrt(width_none)
	best_cutpoint = 1.0

	optimal_FOM = nominal_FOM

	FOM_true = eff_true/np.sqrt(width_true)
	FOM2_true = eff_true/np.sqrt(width_true*eff_true)

	print("\tTrue Saturation eff:",eff_true,"+/-",np.sqrt(eff_true*(1-eff_true)/n_tot),"width:",width_true,"FOM:",FOM_true,FOM2_true)
	for cut_point in np.linspace(0,1.0,20):
		eff_clf = len(df_alt.query('gamma_isSaturated<'+str(cut_point)+' & gamma0_isSaturated<'+str(cut_point)))/(1.0*len(df_alt))
		width_clf = np.std(df_alt.query('gamma_isSaturated<'+str(cut_point)+' & gamma0_isSaturated<'+str(cut_point))["B_s0_M"])
		FOM_clf = eff_clf/np.sqrt(width_clf)
		FOM2_clf = eff_clf/np.sqrt(width_clf*eff_clf)
		print("\tCut<"+str(cut_point)+" eff:",eff_clf,"width:",width_clf,"FOM:",FOM_clf,FOM2_clf)
		if FOM_clf>optimal_FOM:
			best_cutpoint=cut_point
			optimal_FOM=FOM_clf

	best_cutpoint=0.6

	print("Best cut point:",best_cutpoint)
	print("Data loss real saturation:",len(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0'))/(1.0*len(df_alt)),"vs. clf:",len(df_alt.query('gamma_isSaturated<'+str(best_cutpoint)+' & gamma0_isSaturated<'+str(best_cutpoint)))/(1.0*len(df_alt)))


	plt.hist(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0')["B_s0_M"], bins=100,  alpha=0.5,color="blue")
	plt.hist(df_alt.query('gamma_isSaturated<'+str(best_cutpoint)+ '& gamma0_isSaturated<'+str(best_cutpoint))["B_s0_M"], bins=100, alpha=0.5, color="green")
	plt.yscale("linear")
	plt.savefig("Mass_comparison"+file+".pdf")
	plt.clf()
	plt.hist(df_alt["B_s0_M"], bins=100, density=True, alpha=0.5, color="blue",label="all")
	plt.hist(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0')["B_s0_M"], bins=100, density=True, color="red", alpha=0.5,label="Without real sat")
	plt.hist(df_alt.query('gamma_isSaturated<'+str(best_cutpoint)+ '& gamma0_isSaturated<'+str(best_cutpoint))["B_s0_M"], bins=100, density=True, alpha=0.5, color="green",label="Classifier<"+str(best_cutpoint))
	plt.legend()
	plt.savefig("Mass_comparison"+file+"_normalised.pdf")
	plt.clf()

	truemass = np.mean(np.sqrt(df_alt["B_s0_TRUEP_E"]**2-df_alt["B_s0_TRUEP_X"]**2-df_alt["B_s0_TRUEP_Y"]**2-df_alt["B_s0_TRUEP_Z"]**2))

	mass = R.RooRealVar("B_s0_M", "#it{m}(#it{#gamma}#it{#gamma}) [MeV/#it{c}^{2}]", max(4800., truemass-7*np.std(df_alt["B_s0_M"])), min(20000.,truemass+7*np.std(df_alt["B_s0_M"])))
	mu = R.RooRealVar("mu", "mu", truemass*1.009, truemass*0.95,truemass*1.05)
	sigma = R.RooRealVar("sigma", "sigma", np.std(df_alt["B_s0_M"])/8, 10, 2000)
	delta_sigma = R.RooRealVar("delta_sigma", "#Delta#sigma", 1,1e-4,1000)
	sigma_l = R.RooFormulaVar("sigma_l", "@0-@1",R.RooArgList(sigma, delta_sigma))
	sigma_r = R.RooFormulaVar("sigma_r", "@0+@1",R.RooArgList(sigma, delta_sigma))
	# sigma_l = R.RooRealVar("sigma_l", "sigma_l", np.std(df_alt["B_s0_M"])/8, 10, 2000.)
	# sigma_r = R.RooRealVar("sigma_r", "sigma_r", np.std(df_alt["B_s0_M"])/8, 10, 2000.)
	alpha_l = R.RooRealVar("alpha_l", "alpha_l", 0.5, 0.01, 5.)
	alpha_r = R.RooRealVar("alpha_r", "alpha_r", 3.0, 0.01, 5.)
	n_l = R.RooRealVar("n_l", "n_l", 4.6)
	n_r = R.RooRealVar("n_r", "n_r", 0.26)
	cb = R.RooCrystalBall ("cb", "cb", mass, mu, sigma_l, sigma_r, alpha_l, n_l, alpha_r, n_r)

	mu_2 = R.RooRealVar("mu_2", "mu_2", truemass*0.923, truemass*0.9, truemass*0.94)
	sigma_2 = R.RooRealVar("sigma_2", "sigma_2", np.std(df_alt["B_s0_M"])/2, 10, 2000)
	delta_sigma_2 = R.RooRealVar("delta_sigma_2", "#Delta#sigma", 1,1e-4,1000)
	sigma_l_2 = R.RooFormulaVar("sigma_l", "@0-@1",R.RooArgList(sigma_2, delta_sigma_2))
	sigma_r_2 = R.RooFormulaVar("sigma_r", "@0+@1",R.RooArgList(sigma_2, delta_sigma_2))
	# sigma_l_2 = R.RooRealVar("sigma_l_2", "sigma_l_2", 2*np.std(df_alt["B_s0_M"]), 10, 5000.)
	# sigma_r_2 = R.RooRealVar("sigma_r_2", "sigma_r_2", np.std(df_alt["B_s0_M"])/5, 10, 5000.)
	alpha_l_2 = R.RooRealVar("alpha_l_2", "alpha_l_2", 4.3)#, 0.01, 5.)
	alpha_r_2 = R.RooRealVar("alpha_r_2", "alpha_r_2", 2., 0.01, 5.)
	n_l_2 = R.RooRealVar("n_l_2", "n_l_2", 1.2, 0.01, 10.)
	n_r_2 = R.RooRealVar("n_r_2", "n_r_2", 1., 0.001, 10.)
	cb_2 = R.RooCrystalBall ("cb_2", "cb_2", mass, mu_2, sigma_l_2, sigma_r_2, alpha_l_2, n_l_2, alpha_r_2, n_r_2)


	# Jlambda = R.RooRealVar("Jlambda", "Jlambda", np.std(df_alt["B_s0_M"]), 10, 10000)
	# Jgamma = R.RooRealVar("Jgamma", "Jgamma", 3, -20, 20)
	# Jdelta = R.RooRealVar("Jdelta", "Jdelta", 3, 0.001, 10.)

	# cb_2 = R.RooJohnson ("cb_2", "cb_2", mass, mu_2, Jlambda, Jgamma , Jdelta)

	# Argusc = R.RooRealVar("Argusc", "Argusc", -10, -1000, 1000.) 
	# Argusp = R.RooRealVar("Argusc", "Argusc", 0.5, -10, 10.) 
	# cb_2 = R.RooArgusBG ("cb_2", "cb_2", mass, mu_2, Argusc)

	# Basy = R.RooRealVar("Basy", "Basy", -0.5,-2,2)
	# Btailleft = R.RooRealVar("Btailleft", "Btailleft", -1,-3,0)
	# Btailright = R.RooRealVar("Btailright", "Btailright", 0.1,0,3)

	# cb_2 = R.RooBukinPdf ("cb_2", "cb_2", mass, mu_2, sigma_l_2, Basy, Btailleft, Btailright)

	coeff = R.RooRealVar("coeff", "coeff", eff_true,0.,1.)
	# coeff = R.RooRealVar("coeff", "coeff", 0.95,0.,1.)

	sumpdf = R.RooAddPdf("twodcb", "twodcb", cb,cb_2, coeff)

	df_cut = df_alt.query('gamma_isSaturated<'+str(best_cutpoint)+' & gamma0_isSaturated<'+str(best_cutpoint))

	data = R.RooDataSet.from_pandas(df_cut, R.RooArgSet(mass))

	fr = sumpdf.fitTo(data,R.RooFit.Strategy(2), R.RooFit.Offset(True),R.RooFit.Save())
	fr.Print("v")
	canv = R.TCanvas()
	frame = mass.frame()
	data.plotOn(frame, R.RooFit.Name("Data"))
	sumpdf.plotOn(frame,R.RooFit.Name("Total"))
	sumpdf.plotOn(frame, R.RooFit.Components("cb"), R.RooFit.LineColor(R.kRed),R.RooFit.LineStyle(R.kDashed))
	sumpdf.plotOn(frame, R.RooFit.Components("cb_2"), R.RooFit.LineColor(R.kOrange),R.RooFit.LineStyle(R.kDotted))

	## Draw with pulls
	main_frame_height = 0.75;
	pull_frame_height = 0.25;
	main_pull_ratio = main_frame_height / pull_frame_height;
	num_sigma_pull_range = 5.;
	danger_limit = 2.0;

	m_massbins = frame.GetNbinsX()
	# m_massbins = 100

	## Create RooPlot object for pulls
	frame_pulls = R.RooPlot(frame.GetXaxis().GetXmin(), frame.GetXaxis().GetXmax());
	frame_pulls.GetXaxis().SetTitle(frame.GetXaxis().GetTitle());

	## Main pad
	pad_main = R.TPad("pad_main", "The main pad", 0.0, 1 - main_frame_height, 1.0, 1.0);
	pad_main.SetBottomMargin(0.02); # Insert a spacing between main frame and pull frame, if spacing is set to True
	pad_main.cd();

	LHCbStyle = R.gROOT.GetStyle("lhcbStyle")
	Label = R.TPaveText(0.70 - LHCbStyle.GetPadRightMargin(),
						0.85 - LHCbStyle.GetPadTopMargin(),
						0.95 - LHCbStyle.GetPadRightMargin(),
						0.95 - LHCbStyle.GetPadTopMargin(),
						"BRNDC")
	# Label = R.TPaveText(0.15, 0.2, 0.45, 0.4,"BRNDC")
	# Label.SetBorderSize(1)
	# Label.SetFillColorAlpha(R.kWhite, 0.0)
	# Label.SetLineColorAlpha(R.kWhite, 0.0)
	# Label.AddText("Center at "+str(center)+":")
	# Label.AddText("["+str(round(beginpoint))+" - "+str(round(endpoint))+"]")


	## Manipulate and draw pad_main
	# frame->SetMinimum(0.5)
	frame.GetXaxis().SetLabelSize(0.0);
	frame.GetXaxis().SetTitleSize(0.0);       # Remove the x-axis title of the main frame
	frame.Draw();
	# Label.Draw()

	## Pull pad
	pad_pulls = R.TPad("pad_pulls", "The pull pad", 0.0, 0.0, 1.0, pull_frame_height);
	pad_pulls.SetTopMargin(0.06);
	pad_pulls.SetBottomMargin(0.4);  # The magic happens here!
	pad_pulls.cd();

	## Manipulate and draw pad_pulls
	hpull = frame.pullHist("Data","Total");
	if not hpull: print("ERROR: No RooCurve found!")

	range_min_x = frame.GetXaxis().GetXmin();
	range_max_x = frame.GetXaxis().GetXmax();

	## Create new TH1 objects for pulls

	pulls_all = R.TH1D("pulls_all", "pulls_all", m_massbins, range_min_x, range_max_x);
	pulls_ok = R.TH1D("pulls_ok", "pulls_ok", m_massbins, range_min_x, range_max_x);
	pulls_danger = R.TH1D("pulls_danger", "pulls_danger", m_massbins, range_min_x, range_max_x);

	pulls_all.SetFillStyle(0);
	pulls_ok.SetFillColor(R.kGray);
	pulls_danger.SetFillColor(R.kRed);

	for i in range(1,m_massbins+1):
		cur_x = hpull.GetPointX(i);
		cur_y = hpull.GetPointY(i);
		pulls_ok.SetBinContent(i, 0.);
		pulls_danger.SetBinContent(i, 0.);

		# add to total pulls data set
		# pull_var->setVal(cur_y);
		# pull_total->add(*pull_var_set);
		# Needed for plotting pulls_danger
		if abs(cur_y) > num_sigma_pull_range and cur_y >= 0.: cur_y = num_sigma_pull_range;
		elif abs(cur_y) > num_sigma_pull_range and cur_y < 0.: cur_y = -num_sigma_pull_range;

		pulls_all.SetBinContent(i, cur_y);
		if abs(cur_y) > danger_limit: pulls_danger.SetBinContent(i, cur_y);
		else: pulls_ok.SetBinContent(i, cur_y);

	## Configure frame
	frame_pulls.SetMinimum(-num_sigma_pull_range);
	frame_pulls.SetMaximum(num_sigma_pull_range);
	frame_pulls.SetTitle("");

	## scale frame
	frame_pulls.GetXaxis().SetTitleSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetXaxis().SetTickSize(0.05);

	frame_pulls.GetYaxis().SetTitleSize(0.072 * main_pull_ratio);
	frame_pulls.GetYaxis().SetTitleOffset(0.95 / main_pull_ratio);
	frame_pulls.GetYaxis().SetLabelSize(0.06 * main_pull_ratio);
	frame_pulls.GetYaxis().SetNdivisions(503);

	frame_pulls.GetYaxis().SetTitle("Pull");
	frame_pulls.Draw();

	## Add sigma lines
	zeroline = R.TLine(range_min_x, 0.0, range_max_x, 0.0);

	threesigmaline = R.TLine(range_min_x, 3.0, range_max_x, 3.0);
	threesigmaline.SetLineStyle(R.kDashed);
	threesigmaline.SetLineColor(R.kGray + 2);

	minusthreesigmaline = R.TLine(range_min_x, -3.0, range_max_x, -3.0);
	minusthreesigmaline.SetLineStyle(R.kDashed);
	minusthreesigmaline.SetLineColor(R.kGray + 2);

	## Draw all the stuff
	zeroline.Draw("SAME");
	threesigmaline.Draw("SAME");
	minusthreesigmaline.Draw("SAME");
	pulls_all.Draw("SAME");
	pulls_danger.Draw("SAME");
	pulls_ok.Draw("SAME");

	## Canvas
	canv.cd()
	pad_main.Draw()
	pad_pulls.Draw("SAME");
	canv.Draw()
	canv.SaveAs("Fit_"+file+"_incl_saturation_2DCB.pdf")



	# frame.Draw()
	# canv.SaveAs("Fit_"+file+"_incl_saturation_2DCB.pdf")

	par_dict = {str(round(truemass/1000)):{}}

	iterator = fr.floatParsFinal().createIterator()
	par = iterator.Next()
	while par:
		par_dict[str(round(truemass/1000))][par.GetName()]=[par.getVal(), par.getError()]
		par = iterator.Next()


	# jsonfile = open('signalparameters.json', 'r')

	# jsondata = json.load(jsonfile)
	# jsonfile.close()

	# jsonfile = open('signalparameters.json', 'w')	
	# jsondata.update(par_dict)
	# print(jsondata)
	# json.dump(jsondata,jsonfile)
	# jsonfile.close()
