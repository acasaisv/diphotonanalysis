import uproot3 as uproot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from hep_ml.reweight import GBReweighter, FoldingReweighter, BinsReweighter

import json

mass_label = {
	"49100030": "4.5 GeV",
	"49100040": "5 GeV",
	"49100041": "6 GeV",
	"49100042": "7 GeV",
	"49100043": "8 GeV",
	"49100044": "9 GeV",
	"49100045": "10 GeV",
	"49100046": "12 GeV",
	"49100047": "14 GeV",
	"49100048": "15 GeV",
	"49100049": "17 GeV",
	"49100050": "19 GeV",
	"49100051": "20 GeV",
	"Bsgg" : "$B^0_s\\to\gamma\gamma$"
	}

## load eta2mmg
simetammg = pd.read_hdf("/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5")
dataetammg = pd.read_hdf("/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5")

eff_dict = {}

for key in mass_label:

	## load signal
	path = f"/scratch47/adrian.casais/ntuples/signal/sim10/{key}_1000-1099_Sim10a-priv.root"
	if key=="Bsgg":
		path="/scratch47/adrian.casais/ntuples/signal/b2gg-sim10.root"
	eventssig = uproot.open(path)["DTTBs2GammaGamma/DecayTree"]

	variables_gamma = [	"gamma_PT",
				 		"gamma_PP_CaloNeutralHcal2Ecal",
				 		"gamma_P",
				 		"gamma_PP_IsNotH",
				 		"gamma_PP_IsPhoton",
				 		"gamma_TRUEID",
				 		"gamma_ETA",
				 		"gamma_PP_CaloNeutralID",
				 		"gamma_CaloHypo_Saturation",
				 		"gamma_Matching",
				 		"gamma_ShowerShape"
				 		]
	variables_gamma0 = []
	for var in variables_gamma:
		var2 = var.replace("gamma_", "gamma0_")
		variables_gamma0.append(var2)


	variables_eta = [	"mu2_L0MuonDecision_Dec",
					 	"mu1_L0DiMuonDecision_Dec",
					 	"eta_TRUEID",
					 	"mu1_TRUEID",
					 	"mu2_TRUEID",
					 	"nSPDHits",
					 	"eta_M"]

	variables_Bs = ["B_s0_TRUEID",
					"nSPDHits"]

	# print(variables_gamma+variables_gamma0+variables_Bs)

	df_sig = eventssig.pandas.df(branches=variables_gamma+variables_gamma0+variables_Bs)

	## cuts in principle already applied to the hdf to be sure:
	cuts_sweight1 = 'gamma_CL>0.3 & gamma_P> 6000 & eta_PT > 1000 & gamma_PP_CaloNeutralHcal2Ecal<0.1 and gamma_PT > 1000 and nSPDHits<450'
	cuts_sweight2 = 'mu1_PT > 500 & mu2_PT > 500 &  mu1_IPCHI2_OWNPV < 6 & mu2_IPCHI2_OWNPV <6 & mu1_ProbNNmu > 0.8 & mu2_ProbNNmu > 0.8 and nSPDHits<450'# and eta_ENDVERTEX_CHI2 < 20'

	## clean and truthmatch
	trigger_eta2mmg = '(mu2_L0MuonDecision_Dec|mu1_L0DiMuonDecision_Dec)&eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS'
	truthmatch_eta2mmg = 'abs(mu1_TRUEID)==13&abs(mu2_TRUEID)==13&abs(gamma_TRUEID)==22&abs(eta_TRUEID)==221'

	## stripping selection applied to all, also signal
	cuts_gamma0 = 'gamma0_PT > 3000 & gamma0_PP_CaloNeutralHcal2Ecal<0.1 & gamma0_P>6000 & gamma0_PP_IsNotH>0.3 & gamma0_PP_IsPhoton>0.85 & nSPDHits<450'
	cuts = 'gamma_PT > 3000 & gamma_PP_CaloNeutralHcal2Ecal<0.1 & gamma_P>6000 & gamma_PP_IsNotH>0.3 & gamma_PP_IsPhoton>0.85 & nSPDHits<450'

	## truthmatch signal
	truth_a2gg = 'abs(gamma0_TRUEID)==22&(abs(B_s0_TRUEID)==54|abs(B_s0_TRUEID)==531)&abs(gamma_TRUEID)==22'

	signal = df_sig.query(cuts+'&'+cuts_gamma0+'&'+truth_a2gg)
	eta2mmg_MC = simetammg.query(cuts+'&'+truthmatch_eta2mmg+'&'+trigger_eta2mmg+'&'+cuts_sweight1+'&'+cuts_sweight2)
	eta2mmg_data = dataetammg.query(cuts+'&'+trigger_eta2mmg+'&'+cuts_sweight1+'&'+cuts_sweight2)

	# print(signal.columns)

	signal["gamma_ECAL_region"] = ((signal["gamma_PP_CaloNeutralID"]-33000)/4000).astype(int)
	signal["gamma0_ECAL_region"] = ((signal["gamma0_PP_CaloNeutralID"]-33000)/4000).astype(int)
	eta2mmg_MC["gamma_ECAL_region"] = ((eta2mmg_MC["gamma_PP_CaloNeutralID"]-33000)/4000).astype(int)
	eta2mmg_data["gamma_ECAL_region"] = ((eta2mmg_data["gamma_PP_CaloNeutralID"]-33000)/4000).astype(int)

	trainingvars = ["gamma_PT","gamma_P","gamma_ECAL_region"]
	# trainingvars = ["gamma_P","gamma_PT","gamma_ECAL_region","gamma_Matching"]
	trainingvars0 = ["gamma0_PT","gamma0_P","gamma0_ECAL_region"]

	signal_train = signal[trainingvars]
	signal_train0 = signal[trainingvars0]
	signal_train0.rename(columns={"gamma0_P":"gamma_P", "gamma0_PT":"gamma_PT", "gamma0_ECAL_region":"gamma_ECAL_region", "gamma0_Matching":"gamma_Matching", "gamma0_ShowerShape":"gamma_ShowerShape"})

	etammg_train = eta2mmg_MC[trainingvars]

	reweighter_base = GBReweighter(max_depth=1,learning_rate=0.1,n_estimators=100,gb_args={'subsample': 0.8, 'random_state': 1235})
	reweighter = GBReweighter(max_depth=1,learning_rate=0.08,n_estimators=100,gb_args={'subsample': 0.3, 'random_state': 1235, 'max_features': 1.0})
	# reweighter_base = BinsReweighter(n_bins=100, n_neighs=3)
	# reweighter = FoldingReweighter(reweighter_base, n_folds=3, random_state = 1235)
	# reweighter.fit(original=etammg_train, target=signal_train, original_weight=eta2mmg_data["sweights"])
	reweighter.fit(original=etammg_train, target=signal_train, original_weight=eta2mmg_MC["sweights"])

	## the reweight branch is the product of the "sweights" and the reweighted branch
	eta2mmg_MC["reweight"] = reweighter.predict_weights(eta2mmg_MC[trainingvars], original_weight=eta2mmg_MC["sweights"])
	eta2mmg_data["reweight"] = reweighter.predict_weights(eta2mmg_data[trainingvars], original_weight=eta2mmg_data["sweights"])

	# reweighter.fit(original=etammg_train, target=signal_train0, original_weight=eta2mmg_data["sweights"])
	reweighter.fit(original=etammg_train, target=signal_train0, original_weight=eta2mmg_MC["sweights"])
	eta2mmg_MC["reweight0"] = reweighter.predict_weights(eta2mmg_MC[trainingvars], original_weight=eta2mmg_MC["sweights"])
	eta2mmg_data["reweight0"] = reweighter.predict_weights(eta2mmg_data[trainingvars], original_weight=eta2mmg_data["sweights"])

	## plot variables
	plt.hist(signal["gamma_PT"],range=(3000,25000), bins=50,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["gamma_PT"],range=(3000,25000), bins=50,density=True, weights=eta2mmg_MC["sweights"], histtype ='step',label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["gamma_PT"],range=(3000,25000), bins=50,density=True, weights=eta2mmg_MC["reweight"],histtype ='step',label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["gamma_PT"],range=(3000,25000), bins=50,density=True, weights=eta2mmg_data["reweight"],histtype ='step',label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["gamma_PT"],range=(3000,25000), bins=50,density=True, weights=eta2mmg_data["sweights"],histtype ='step',label="$\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("$\gamma$ pT [MeV]")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_PT_{key}.pdf")
	plt.clf()
	plt.hist(signal["gamma_P"],range=(6000,700000), bins=50,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["gamma_P"],range=(6000,700000), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["sweights"],label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["gamma_P"],range=(6000,700000), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["gamma_P"],range=(6000,700000), bins=50,density=True, histtype ='step', weights=eta2mmg_data["sweights"],label="$\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["gamma_P"],range=(6000,700000), bins=50,density=True, histtype ='step', weights=eta2mmg_data["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("$\gamma$ p [MeV]")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_P_{key}.pdf")
	plt.clf()
	plt.hist(signal["gamma_ETA"],range=(1.6,4.8), bins=50,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["gamma_ETA"],range=(1.6,4.8), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["sweights"],label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["gamma_ETA"],range=(1.6,4.8), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["gamma_ETA"],range=(1.6,4.8), bins=50,density=True, histtype ='step', weights=eta2mmg_data["sweights"],label="$\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["gamma_ETA"],range=(1.6,4.8), bins=50,density=True, histtype ='step', weights=eta2mmg_data["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("$\gamma$ $\eta$")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_eta_{key}.pdf")
	plt.clf()
	plt.hist(signal["gamma_PP_IsNotH"],range=(0,1.0), bins=50,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["gamma_PP_IsNotH"],range=(0,1.0), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["sweights"],label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["gamma_PP_IsNotH"],range=(0,1.0), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["gamma_PP_IsNotH"],range=(0,1.0), bins=50,density=True, histtype ='step', weights=eta2mmg_data["sweights"],label="$\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["gamma_PP_IsNotH"],range=(0,1.0), bins=50,density=True, histtype ='step', weights=eta2mmg_data["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("$\gamma$ IsNotH")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_IsNotH_{key}.pdf")
	plt.clf()
	plt.hist(signal["gamma_PP_IsPhoton"],range=(0,1.2), bins=50,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["gamma_PP_IsPhoton"],range=(0,1.2), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["sweights"],label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["gamma_PP_IsPhoton"],range=(0,1.2), bins=50,density=True, histtype ='step', weights=eta2mmg_MC["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["gamma_PP_IsPhoton"],range=(0,1.2), bins=50,density=True, histtype ='step', weights=eta2mmg_data["sweights"],label="$\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["gamma_PP_IsPhoton"],range=(0,1.2), bins=50,density=True, histtype ='step', weights=eta2mmg_data["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("$\gamma$ IsPhoton")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_IsPhoton_{key}.pdf")
	plt.clf()
	plt.hist(signal["nSPDHits"],range=(0,450), bins=20,density=True, histtype ='step',label="ALP "+mass_label[key])
	plt.hist(eta2mmg_MC["nSPDHits"],range=(0,450), bins=20,density=True, histtype ='step', weights=eta2mmg_MC["sweights"],label="$\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_MC["nSPDHits"],range=(0,450), bins=20,density=True, histtype ='step', weights=eta2mmg_MC["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ MC")
	plt.hist(eta2mmg_data["nSPDHits"],range=(0,450), bins=20,density=True, histtype ='step', weights=eta2mmg_data["sweights"],label="$\eta\\to\mu\mu\gamma$ data")
	plt.hist(eta2mmg_data["nSPDHits"],range=(0,450), bins=20,density=True, histtype ='step', weights=eta2mmg_data["reweight"],label="weighted $\eta\\to\mu\mu\gamma$ data")
	plt.xlabel("nSPDHits")
	plt.ylabel("Normalised yield [a.u.]")
	plt.legend()
	plt.savefig(f"Saturation_reweight_test_nSPDHits_{key}.pdf")
	plt.clf()


	worig_passed = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["sweights"])
	worig_passed2 = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["sweights"]**2)
	worig_all = np.sum(eta2mmg_MC["sweights"])
	worig_all2 = np.sum(eta2mmg_MC["sweights"]**2)

	worig_passed_data = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["sweights"])
	worig_passed_data2 = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["sweights"]**2)
	worig_all_data = np.sum(eta2mmg_data["sweights"])
	worig_all_data2 = np.sum(eta2mmg_data["sweights"]**2)

	w_passed = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["reweight"])
	w2_passed = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["reweight"]**2)
	w_all = np.sum(eta2mmg_MC["reweight"])
	w2_all = np.sum(eta2mmg_MC["reweight"]**2)

	w_passed_data = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["reweight0"])
	w_passed_data2 = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["reweight0"]**2)
	w_all_data = np.sum(eta2mmg_data["reweight0"])
	w_all_data2 = np.sum(eta2mmg_data["reweight0"]**2)

	w0_passed = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["reweight0"])
	w02_passed = np.sum(eta2mmg_MC.query("gamma_CaloHypo_Saturation<1")["reweight0"]**2)
	w0_all = np.sum(eta2mmg_MC["reweight0"])
	w02_all = np.sum(eta2mmg_MC["reweight0"]**2)

	w0_passed_data = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["reweight0"])
	w0_passed_data2 = np.sum(eta2mmg_data.query("gamma_CaloHypo_Saturation<1")["reweight0"]**2)
	w0_all_data = np.sum(eta2mmg_data["reweight0"])
	w0_all_data2 = np.sum(eta2mmg_data["reweight0"]**2)


	eff_eta2mmg_van = worig_passed/worig_all
	eff_eta2mmg_van_err = np.sqrt(((worig_passed**2*(worig_all2 - worig_passed2))+(worig_passed2*(worig_all - worig_passed)**2))/(worig_all**4))
	eff_eta2mmg_van_data = worig_passed_data/worig_all_data
	eff_eta2mmg_van_data_err = np.sqrt(((worig_passed_data**2*(worig_all_data2 - worig_passed_data2))+(worig_passed_data2*(worig_all_data - worig_passed_data)**2))/(worig_all_data**4))

	eff_alp = len(signal.query("gamma_CaloHypo_Saturation<1"))/(1.0*len(signal))
	eff0_alp = len(signal.query("gamma0_CaloHypo_Saturation<1"))/(1.0*len(signal))
	eff_alp_err = np.sqrt(eff_alp*(1-eff_alp)/len(signal))
	eff0_alp_err = np.sqrt(eff0_alp*(1-eff0_alp)/len(signal))

	eff_alp_2photon = len(signal.query("gamma_CaloHypo_Saturation<1 & gamma0_CaloHypo_Saturation<1"))/(1.0*len(signal))
	eff_alp_2photon_err = np.sqrt(eff_alp_2photon*(1- eff_alp_2photon)/len(signal))

	eff_eta2mmg_weight = w_passed/w_all
	eff_eta2mmg_err_weight = np.sqrt(((w_passed**2*(w2_all-w2_passed))+(w2_passed*(w_all - w_passed)**2))/(w_all**4))
	eff_eta2mmg_weight_data = w_passed_data/w_all_data
	eff_eta2mmg_err_data = np.sqrt(((w_passed_data**2*(w_all_data2 - w_passed_data2))+(w_passed_data2*(w_all_data - w_passed_data)**2))/(w_all_data**4))

	eff0_eta2mmg_weight = w0_passed/w0_all
	eff0_eta2mmg_err_weight = np.sqrt(((w0_passed**2*(w02_all-w02_passed))+(w02_passed*(w0_all - w0_passed)**2))/(w0_all**4))
	eff0_eta2mmg_weight_data = w0_passed_data/w0_all_data
	eff0_eta2mmg_err_data = np.sqrt(((w0_passed_data**2*(w0_all_data2 - w0_passed_data2))+(w0_passed_data2*(w0_all_data - w0_passed_data)**2))/(w0_all_data**4))


	combined_MC = eff_eta2mmg_weight*eff0_eta2mmg_weight
	combined_MC_err = np.sqrt(eff_eta2mmg_err_weight**2*(eff0_eta2mmg_weight)**2+eff0_eta2mmg_err_weight**2*(eff_eta2mmg_weight)**2)
	combined_data = eff_eta2mmg_weight_data*eff0_eta2mmg_weight_data
	combined_data_err = np.sqrt(eff_eta2mmg_err_data**2*(eff0_eta2mmg_weight_data)**2+eff0_eta2mmg_err_data**2*(eff_eta2mmg_weight_data)**2)


	print("Efficiency from vanilla eta2mmg MC:",eff_eta2mmg_van*100,"+/-",eff_eta2mmg_van_err*100,"%")
	print("Efficiency from vanilla eta2mmg Data:",eff_eta2mmg_van_data*100,"+/-",eff_eta2mmg_van_data_err*100,"%")
	print("Efficiency from reweighted eta2mmg MC:",eff_eta2mmg_weight*100,"+/-",eff_eta2mmg_err_weight*100,"%",eff0_eta2mmg_weight*100,"+/-",eff0_eta2mmg_err_weight*100,"% -> combined:",combined_MC*100,"+/-",combined_MC_err*100,"%")
	print("Efficiency from reweighted eta2mmg Data:",eff_eta2mmg_weight_data*100,"+/-",eff_eta2mmg_err_data*100,"%",eff_eta2mmg_weight_data*100,"+/-",eff_eta2mmg_err_data*100,"% -> combined:",combined_data*100,"+/-",combined_data_err*100,"%")
	print(f"Efficiency from vanilla ALP {mass_label[key]}:",eff_alp*100,"+/-",eff_alp_err*100,"% -> combined:",(eff_alp*eff0_alp)*100,"+/-",np.sqrt((eff_alp*eff0_alp_err)**2+(eff0_alp*eff_alp_err)**2)*100,"%")
	syst = abs((eff_alp*eff0_alp - eff_alp_2photon)/(eff_alp*eff0_alp)*combined_data)
	print(f"Efficiency from vanilla ALP {mass_label[key]} 2 photons:",eff_alp_2photon*100,"+/-",eff_alp_2photon_err*100,"+/-",syst*100)

	eff_dict[key] = {
			"van_eta2mmg_MC":[eff_eta2mmg_van,eff_eta2mmg_van_err],
			"van_eta2mmg_Data":[eff_eta2mmg_van_data,eff_eta2mmg_van_data_err],
			"rew_eta2mmg_MC":[eff_eta2mmg_weight,eff_eta2mmg_err_weight],
			"rew_eta2mmg_Data":[eff_eta2mmg_weight_data,eff_eta2mmg_err_data],
			"van_ALP": [eff_alp,eff_alp_err],
			"van_ALP2photon": [eff_alp_2photon,eff_alp_2photon_err],
			"comb_van_ALP": [eff_alp*eff0_alp, np.sqrt((eff_alp*eff0_alp_err)**2+(eff0_alp*eff_alp_err)**2)],
			"total_diphoton":[combined_data, combined_data_err, syst]
			}


with open("saturation_efficiencies.json","w") as outfile:
	json.dump(eff_dict,outfile)
