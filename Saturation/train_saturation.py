import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle

from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import StratifiedShuffleSplit, cross_val_score, train_test_split, HalvingRandomSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score

variables =[
	'gamma_CaloHypo_Saturation',
	'gamma_PT',
	'gamma_ShowerShape',
	'gamma_PP_CaloNeutralHcal2Ecal',
	'gamma_PP_CaloNeutralID',
	'gamma_PP_IsNotH',
	'gamma_PP_IsNotE',
	'gamma_PP_IsPhoton'
	]
variables_2 =[
	'gamma0_CaloHypo_Saturation',
	'gamma0_PT',
	'gamma0_ShowerShape',
	'gamma0_PP_CaloNeutralHcal2Ecal',
	'gamma0_PP_CaloNeutralID',
	'gamma0_PP_IsNotH',
	'gamma0_PP_IsNotE',
	'gamma0_PP_IsPhoton'
	]

additional_vars = [	'B_s0_L0ElectronDecision_TOS',
					'B_s0_L0PhotonDecision_TOS',
					'B_s0_Hlt1B2GammaGammaDecision_TOS',
					'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
					'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
					'B_s0_M',
					'gamma_TRUEID',
					'gamma0_TRUEID',
					'B_s0_TRUEID'
					]


replace_dict = {}
for var in variables_2:
	replace_dict[var]=var.replace("gamma0", "gamma")

print("Reading Events")
events = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/49100040_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree")

cuts = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_TRUEID==22 and gamma0_TRUEID==22 and B_s0_TRUEID==54' 
df_1 = events.arrays(variables+variables_2+additional_vars, library="pd")

df = df_1.query(cuts)
df_2 =df.drop(columns=variables+additional_vars)
df_2.rename(columns=replace_dict,inplace=True)
df.drop(columns=variables_2+additional_vars,inplace=True)
print(replace_dict)
print(df)
print(df_2)
# df.append(df_2)
df = pd.concat([df,df_2], ignore_index=True)

for file in ["49100041","49100042","49100043","49100044","49100045","49100046","49100047","49100048","49100049","49100050"]:

	events_alt = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/"+file+"_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree")
	df_alt_raw = events_alt.arrays(variables+variables_2+additional_vars, library="pd")
	df_alt = df_alt_raw.query(cuts)
	df_alt_2 = df_alt.drop(columns=variables+additional_vars)
	df_alt_2.rename(columns=replace_dict,inplace=True)
	df_alt.drop(columns=variables_2+additional_vars,inplace=True)
	# df_alt.append(df_alt_2)
	df = pd.concat([df,df_alt,df_alt_2], ignore_index=True)

df.reset_index(drop=True, inplace=True)

y = (df['gamma_CaloHypo_Saturation']>0)

X = df.drop(columns=["gamma_CaloHypo_Saturation"])
print(y.shape, X.shape)
# df.dropna(inplace=True)
# y = (df['gamma_CaloHypo_Saturation']>0)
# X = df.drop(columns=additional_vars+["gamma_CaloHypo_Saturation"])
# print(y.shape, X.shape)
print(X)
# sss = StratifiedShuffleSplit(n_splits=2, test_size=0.2, random_state=37)


y = y.to_numpy()
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=37,stratify=y)

# train_index, test_index = sss.split(X,y)
# # print(train_index[0])
# # y_train = np.take(y,train_index[0],1)
# y_train = y[train_index[0]].to_numpy()
# y_test = y[test_index[0]].to_numpy()
# X_train = X.iloc[train_index[0]]
# X_test = X.iloc[test_index[0]]



print(y_train.shape, y_test.shape)

print("Training")

estimator = GradientBoostingClassifier(random_state=37)
param_dist = {
	"n_estimators":[180,190,200,210,220,230],
	"learning_rate":[0.05,0.06,0.07,0.08,0.09,0.1],
	"max_depth":[1,2,3],
	"max_features": [None,'sqrt','log2']	
}

cv = StratifiedShuffleSplit(n_splits=5, random_state=37)

clf = HalvingRandomSearchCV(estimator=estimator, param_distributions=param_dist, factor=2, random_state=37, n_jobs=10, cv=cv)

clf.fit(X_train,y_train)
print("Best parameters of the grid search:",clf.best_params_)

scores_train = clf.decision_function(X_train)
scores_test = clf.decision_function(X_test)

sig_train = X_train[y_train] 
sig_test = X_test[y_test]
bkg_train = X_train[np.invert(y_train)]
bkg_test = X_test[np.invert(y_test)]

print(clf.predict_proba(X_train)[:,1])

plt.hist(clf.predict_proba(sig_train)[:,1],bins=100,density=True, color='blue', alpha=0.5)
plt.hist(clf.predict_proba(sig_test)[:,1],bins=100,density=True, color='darkblue', fill=False)
plt.hist(clf.predict_proba(bkg_test)[:,1],bins=100,density=True, color='red', alpha=0.5)
plt.hist(clf.predict_proba(bkg_train)[:,1],bins=100,density=True, color='darkred', fill=False)
plt.xlabel("Saturation classifier probability")
plt.yscale("log")
plt.savefig("Saturation_Classifier.pdf")
plt.clf()

print("Train results")
print(clf.score(X_train,y_train))
print("ROC AUC:", roc_auc_score(y_train,scores_train))
print("Test results")
print(clf.score(X_test,y_test))
print("ROC AUC:", roc_auc_score(y_test,scores_test))

for file in ["49100040","49100041","49100042","49100043","49100044","49100045","49100046","49100047","49100048","49100049","49100050"]:

	events_alt = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/"+file+"_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree")

	# cuts = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT > 3000 and gamma0_PT > 3000'
	df_alt_raw = events_alt.arrays(variables+variables_2+additional_vars, library="pd")

	df_alt = df_alt_raw.query(cuts)

	X_1 = df_alt.drop(columns=additional_vars+variables_2+["gamma_CaloHypo_Saturation"])
	y_1 = (df_alt['gamma_CaloHypo_Saturation']>0)
	X_2 = df_alt.drop(columns=additional_vars+variables+["gamma0_CaloHypo_Saturation"])
	y_2 = (df_alt['gamma0_CaloHypo_Saturation']>0)
	X_2.rename(columns=replace_dict,inplace=True)

	scores_1 = clf.decision_function(X_1)
	scores_2 = clf.decision_function(X_2)
	print("############ Results of", file)
	print(clf.score(X_1,y_1), clf.score(X_2,y_2))
	print("ROC AUC:", roc_auc_score(y_1, scores_1), roc_auc_score(y_2, scores_2))

	df_alt["gamma_isSaturated"]=clf.predict_proba(X_1)[:,1]
	df_alt["gamma0_isSaturated"]=clf.predict_proba(X_2)[:,1]

	plt.hist(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0')["B_s0_M"], bins=100, color="blue")
	plt.hist(df_alt.query('gamma_isSaturated<0.5 & gamma0_isSaturated<0.5')["B_s0_M"], bins=100, alpha=0.5, color="green")
	plt.yscale("linear")
	plt.savefig("Mass_comparison"+file+".pdf")
	plt.clf()
	plt.hist(df_alt["B_s0_M"], bins=100, density=True, color="blue",label="all")
	plt.hist(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0')["B_s0_M"], bins=100, density=True, color="red", alpha=0.5,label="Without real sat")
	plt.hist(df_alt.query('gamma_isSaturated<0.5 & gamma0_isSaturated<0.5')["B_s0_M"], bins=100, density=True, alpha=0.5, color="green",label="Classifier<0.5")
	plt.legend()
	plt.savefig("Mass_comparison"+file+"_normalised.pdf")
	plt.clf()

	print("Data loss real saturation:",len(df_alt.query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0'))/(1.0*len(df_alt)),"vs. clf:",len(df_alt.query('gamma_isSaturated<0.5 & gamma0_isSaturated<0.5'))/(1.0*len(df_alt)))

	file_clf = open("Saturation_classifier.pkl", "wb")
	pickle.dump(clf, file_clf)
	file_clf.close()