import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from uncertainties import ufloat as uf

#get data 

def get_eff(sample, cut_denominator, cut, weightsbranch=None):
		denominator = sample.query(cut_denominator)
		numerator = sample.query(cut_denominator+'&'+cut)
		if weightsbranch is not None:
			passed = np.sum(numerator[weightsbranch])
			passed2 = np.sum([w**2 for w in numerator[weightsbranch]])

			total = np.sum(denominator[weightsbranch])
			total2 = np.sum([w**2 for w in denominator[weightsbranch]])

			eff = passed/total
			unc = np.sqrt((passed2*(total-passed)**2+passed**2*(total2-passed2))/total**4)
			return uf(eff,unc)

		else:
			eff = len(numerator)*1.0/len(denominator)
			unc = np.sqrt(eff*(1-eff)/len(denominator))
			return uf(eff,unc)


root = '/scratch47/adrian.casais/ntuples/turcal/'
mydf =pd.read_hdf(root+'etammgTurcalData.h5',key='df')
mydfMC =pd.read_hdf(root+'etammgTurcalHardPhotonMC.h5',key='df')

print(mydf.columns)
print(mydfMC.columns)

mydf.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS ",inplace=True)
mydfMC.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS ",inplace=True)

##

pT_thresholds = [2500,5000,7000,9000,11000,12000,13000,15000,25000]
p_thresholds = [2500,5000,7000,9000,11000,12000,13000,15000,25000]
region_thresholds = [0,1,2]
matching_thresholds = [0,20,50,100,200,300,500]


ptvals = []
ptval_errors = []
sat_eff_MC_ptvals = []
sat_eff_MC_err_ptvals = []
sat_eff_data_ptvals = []
sat_eff_data_err_ptvals = []

matching = []
matching_errors = []
sat_eff_MC_match = []
sat_eff_MC_match_errors = []
sat_eff_data_match = []
sat_eff_data_match_errors = []

pvals = []
pval_errors = []
sat_eff_MC_pvals = []
sat_eff_MC_err_pvals = []
sat_eff_data_pvals = []
sat_eff_data_err_pvals = []

region_vals = region_thresholds
pval_errors = [1,1,1]
sat_eff_MC_region = []
sat_eff_MC_err_region = []
sat_eff_data_region = []
sat_eff_data_err_region = []


print("Saturation efficiencies PT")

for i in range(len(pT_thresholds)-1):

	ptvals.append((pT_thresholds[i]+pT_thresholds[i+1])/2.)
	ptval_errors.append((pT_thresholds[i+1]-pT_thresholds[i])/2.)
	eff_MC = get_eff(mydfMC,"gamma_PT>{0}&gamma_PT<{1}".format(pT_thresholds[i],pT_thresholds[i+1]), "gamma_CaloHypo_Saturation<1", "sweights")
	eff_data = get_eff(mydf,"gamma_PT>{0}&gamma_PT<{1}".format(pT_thresholds[i],pT_thresholds[i+1]), "gamma_CaloHypo_Saturation<1", "sweights")
	print(eff_MC, eff_data)
	sat_eff_MC_ptvals.append(eff_MC.n)
	sat_eff_MC_err_ptvals.append(eff_MC.s)
	sat_eff_data_ptvals.append(eff_data.n)
	sat_eff_data_err_ptvals.append(eff_data.s)

print("Saturation efficiencies Matching")

for i in range(len(matching_thresholds)-1):
	matching.append((matching_thresholds[i]+matching_thresholds[i+1])/2.)
	matching_errors.append((matching_thresholds[i+1]-matching_thresholds[i])/2.)
	eff_MC = get_eff(mydfMC,"gamma_PT>0&gamma_PT<25000&gamma_Matching>{0}&gamma_Matching<{1}".format(matching_thresholds[i],matching_thresholds[i+1]), "gamma_CaloHypo_Saturation<1", "sweights")
	eff_data = get_eff(mydf,"gamma_PT>0&gamma_PT<25000&gamma_Matching>{0}&gamma_Matching<{1}".format(matching_thresholds[i],matching_thresholds[i+1]), "gamma_CaloHypo_Saturation<1", "sweights")
	print(eff_MC, eff_data)
	sat_eff_MC_match.append(eff_MC.n)
	sat_eff_MC_match_errors.append(eff_MC.s)
	sat_eff_data_match.append(eff_data.n)
	sat_eff_data_match_errors.append(eff_data.s)

plt.errorbar(ptvals,sat_eff_MC_ptvals,sat_eff_MC_err_ptvals,ptval_errors, fmt=".",label="$\eta\\to\mu\mu\gamma$ MC efficiency")
plt.errorbar(ptvals,sat_eff_data_ptvals,sat_eff_data_err_ptvals,ptval_errors, fmt=".",color="red",label="$\eta\\to\mu\mu\gamma$ data efficiency")
plt.legend()
plt.xlabel("$\gamma$ $p_T$")
plt.ylabel("Saturation veto efficiency")
plt.savefig("etammg_sat_eff_pt.pdf")
plt.savefig("etammg_sat_eff_pt.png")
plt.clf()
plt.errorbar(matching,sat_eff_MC_match,sat_eff_MC_match_errors,matching_errors, fmt=".", label="$\eta\\to\mu\mu\gamma$ MC efficiency")
plt.errorbar(matching,sat_eff_data_match,sat_eff_data_match_errors,matching_errors, fmt=".", color="red",label="$\eta\\to\mu\mu\gamma$ data efficiency")
plt.legend()
plt.xlabel("$\gamma$ track-cluster match $\chi^2$")
plt.ylabel("Saturation veto efficiency")
plt.savefig("etammg_sat_eff_matching.pdf")
plt.savefig("etammg_sat_eff_matching.png")
plt.clf()
