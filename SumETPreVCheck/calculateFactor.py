import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from plot_helpers import *
hist_args = {'histtype':'step',
#'bins':30,
'density':True}
def load_dfs():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
    df.query('gamma_L0ElectronDecision_TOS==1 or gamma_L0PhotonDecision_TOS==1',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS==1 or gamma_L0PhotonDecision_TOS==1',inplace=True)

    #df.query('mu1_L0DiMuonDecision_Dec==1',inplace=True)
    #dfMC.query('mu1_L0DiMuonDecision_Dec==1',inplace=True)

    return df,dfMC

def load_dfs_phig():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/phigData.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/phigMC.h5',key='df')

    return df,dfMC

def load_dfs_kstg():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='dfMC')

    return df,dfMC

def plot(bins,factor):
    fig,ax = plt.subplots(1,1,figsize=(16,9),dpi=80)
    ax.hist(df['nSPDHits'],bins=bins,color='red',label='data',**hist_args)
    dfMC['myspds'] = factor*dfMC['nSPDHits']
    dfMC.query('myspds<900',inplace=True)
    ax.hist(dfMC['nSPDHits'],bins=bins,color='blue',label='mc',**hist_args)
    ax.legend()
    fig.savefig('spds_compare.pdf')
    plt.clf()
def reweight_mc_df(df,dfMC,vars = ['eta_PT','eta_ETA']):
    from hep_ml.reweight import GBReweighter

    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return dfMC
def mean(df,dfMC):
    mean_data = np.average(df['nSPDHits'],weights=df['sweights'])
    mean_mc = np.average(dfMC['nSPDHits'],weights=dfMC['sweights']) 

    print(f'Mean data: {mean_data}; mean mc: {mean_mc}')
    print(f"Correction factor: {mean_data/mean_mc}")
if __name__=='__main__':
    df,dfMC = load_dfs()
    phig,phigmc = load_dfs_phig()
    kstg,kstgmc = load_dfs_kstg()
    dfMC = reweight_mc_df(df,dfMC)
    phigmc = reweight_mc_df(phig,phigmc,vars=['B_ETA','B_PT'])
    kstgmc = reweight_mc_df(kstg,kstgmc,vars=['B_ETA','B_PT'])
    mean(df,dfMC)
    mean(phig,phigmc)
    mean(kstg,kstgmc)

    #spds_mc,edges_mc = np.histogram(dfMC['nSPDHits'],bins=30
    
    factor = 1.0
    chi2_min = np.inf
    factors = np.linspace(1.0,2.0,100)
    chi2s = []
    _,edges = np.histogram(dfMC['nSPDHits'],bins=30,weights=dfMC['sweights'])
    
    
    for f in factors:
        dfMC['myspds'] = f*dfMC['nSPDHits']
       #dfMC.query('myspds<900',inplace=True)

        
        spds_mc,edges_mc = np.histogram(dfMC.query('myspds<900')['myspds'],bins=edges,weights=dfMC.query('myspds<900')['sweights'])
        spds_data,edges_data = np.histogram(df['nSPDHits'],bins=edges,weights=df['sweights'])

        chi2 = np.sum( (spds_data - spds_mc)**2/spds_data )
        chi2s.append(chi2)
        if chi2<chi2_min:
            chi2_min = chi2
            factor = f
    plt.plot(factors,chi2s)
    plt.savefig('chi2s.pdf')
    plot(edges,1) 
    print("plot out")
    print(f'Best scaling factor: {factor}.')

        
    


    