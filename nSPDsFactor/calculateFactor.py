import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from plot_helpers import *
from helpers import get_ALPsdf
hist_args = {'histtype':'step',
#'bins':30,
'density':True}
def load_dfs():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
    df.query('gamma_L0ElectronDecision_TOS==1 or gamma_L0PhotonDecision_TOS==1',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS==1 or gamma_L0PhotonDecision_TOS==1',inplace=True)

    #df.query('mu1_L0DiMuonDecision_Dec==1',inplace=True)
    #dfMC.query('mu1_L0DiMuonDecision_Dec==1',inplace=True)

    return df,dfMC

def load_dfs_phig():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/phigData.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/phigMC.h5',key='df')

    return df,dfMC

def load_dfs_kstg():
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='dfMC')

    return df,dfMC

def plot(bins,factor):
    fig,ax = plt.subplots(1,1,figsize=(16,9),dpi=80)
    ax.hist(df['nSPDHits'],bins=bins,color='red',label='data',**hist_args)
    dfMC['myspds'] = factor*dfMC['nSPDHits']
    dfMC.query('myspds<900',inplace=True)
    ax.hist(dfMC['nSPDHits'],bins=bins,color='blue',label='mc',**hist_args)
    ax.legend()
    fig.savefig('spds_compare.pdf')
    plt.clf()
def reweight_mc_df(df,dfMC,vars = ['eta_PT','eta_ETA']):
    from hep_ml.reweight import GBReweighter

    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return dfMC
def mean(df,dfMC):
    mean_data = np.average(df['nSPDHits'],weights=df['sweights'])
    mean_mc = np.average(dfMC['nSPDHits'],weights=dfMC['sweights']) 

    print(f'Mean data: {mean_data}; mean mc: {mean_mc}')
    print(f"Correction factor: {mean_data/mean_mc}")

def fit_sumet(df,name):
    import zfit

    # create the space where the data will live
    space = zfit.Space(obs=["x"], limits=(400, 1000))

    nBkgComb = zfit.Parameter(name+'nBkgComb',len(df)/2,0,len(df))

    #Background: exponential
    lambda_Bs = zfit.Parameter(name+'lambda',-3e-3,-10,1)
    BsBkgComb = zfit.pdf.Exponential(lambda_Bs,space)

    BsBkgCombExtended = BsBkgComb.create_extended(nBkgComb)

    # create some fake data that follows a Gaussian distribution
    data = zfit.Data.from_numpy(array=df['L0Data_Sum_Et,Prev1'].values,obs=space,weights=df['sweights'])


    # create the loss
    loss = zfit.loss.ExtendedUnbinnedNLL(model=BsBkgCombExtended, data=data)

    # create the optimizer
    optimizer = zfit.minimize.Minuit(tol=1e-6,mode=1,gradient=True)

    # minimize the loss
    result = optimizer.minimize(loss)

    # get the fitted parameters
    params = result.params
    print(params)
    n,l = params[name+'nBkgComb']['value'],params[name+'lambda']['value']
    norm = l*n/(np.exp(l*1000)-np.exp(l*400))
    import scipy.integrate as integrate
    myfun = lambda x: norm*np.exp(l*x) 
    integral= integrate.quad(myfun, 1000, np.inf)
    print('SumEtPrev eff: {0:.3f}'.format(len(df)/(len(df)+integral[0])))
    x = np.linspace(400,2000,100)
    nbins=30
    minimum=np.min(df['L0Data_Sum_Et,Prev1'])
    fig,ax = plt.subplots(1,1)
    ax.plot(x,(1000-minimum)*myfun(x)/nbins)
    df_ = df.query(f'`L0Data_Sum_Et,Prev1`<1000 and `L0Data_Sum_Et,Prev1`>{minimum} ')
    ax.hist(df_['L0Data_Sum_Et,Prev1'],bins=nbins,color='red',weights=df_['sweights'])
    ax.set_xlabel('SumEtPrev [AU]')
    ax.set_ylabel('No. events [AU]')
    fig.savefig(name + 'sumetprev.pdf')
def plot_nspds(factor_phig,factor_kst):
    fig,ax = plt.subplots(1,1)
    phigmc['mynspds']=factor_phig*phigmc['nSPDHits']
    kstgmc['mynspds']=factor_kst*kstgmc['nSPDHits']
    ax.hist(phigmc['nSPDHits'],label=r'$B_s \to \phi \gamma$ Sim10',bins=35,color='red',**hist_args)
    ax.hist(phigmc.query('mynspds<450')['mynspds'],label=r'$B_s \to \phi \gamma$ Sim10 rescaled',bins=20,color='purple',**hist_args)
    ax.hist(kstgmc['nSPDHits'],label=r'$B \to K^* \gamma$ Sim10',bins=35,color='blue',**hist_args)
    ax.hist(kstgmc.query('mynspds<450')['mynspds'],label=r'$B \to K^* \gamma$ Sim10 rescaled',bins=20,color='yellow',**hist_args)

    ax.hist(phig['nSPDHits'],label=r'$B_s \to \phi \gamma$ Data',weights =phig['sweights'],bins=20,color='green',**hist_args)
    ax.hist(kstg['nSPDHits'],label=r'$B \to K^* \gamma$ Data',weights=kstg['sweights'],bins=20,color='black',**hist_args)
    ax.set_xlabel('nSPDHits')
    ax.set_ylabel('No. events [AU]')
    fig.legend(loc='upper right',bbox_to_anchor=(1, 0.5))
    fig.savefig('nSPDs-compare.pdf')
    plt.clf()



def calculate_best_factor(df,dfMC):
    factors = np.linspace(0.5,2.0,100)
    chi2_min = np.inf
    chi2s = []    
    _,edges = np.histogram(dfMC['nSPDHits'],bins=25,weights=dfMC['sweights'])
    factor = 1.0
    for f in factors:
        dfMC['myspds'] = f*dfMC['nSPDHits']
        mydfMC = dfMC.query('myspds<450')
        spds_mc,edges_mc = np.histogram(mydfMC['myspds'],bins=edges,weights=mydfMC['sweights'],density=True)
        spds_data,edges_data = np.histogram(df['nSPDHits'],bins=edges,weights=df['sweights'],density=True)
        
        chi2_array = (spds_data - spds_mc)**2/spds_data
        chi2 = chi2_array.sum()
        #print(spds_data)
        #print(spds_mc)
        chi2s.append(chi2)
        if chi2<chi2_min:
            chi2_min = chi2
            factor = f
    print(factor,chi2_min)
    
    plt.plot(factors,chi2s)
    plt.savefig('chi2s.pdf')


if __name__=='__main__':
    etammg,etammgmc = load_dfs()
    phig,phigmc = load_dfs_phig()
    kstg,kstgmc = load_dfs_kstg()
    plot_nspds(1.36,1.24)
    fit_sumet(phig,'sumet-phig.pdf')
    fit_sumet(kstg,'sumet-kstg.pdf')
    calculate_best_factor(etammg,etammgmc)
    calculate_best_factor(phig,phigmc)
    calculate_best_factor(kstg,kstgmc)    