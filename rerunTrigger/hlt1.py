################################################### NECESSARY BLURB #################################################
from Gaudi.Configuration import *
from Configurables import Moore

Moore().ForceSingleL0Configuration = False
Moore().UseTCK = True
Moore().Split="Hlt1"
Moore().InitialTCK="0x517a18a4"
Moore().CheckOdin = False
Moore().EnableDataOnDemand = True
Moore().WriterRequires = []
Moore().RemoveInputHltRawBanks=True
# Moore().DataType = '2017'
# Moore().DDDBtag  = "dddb-20170721-3"
# Moore().CondDBtag = "sim-20180411-vc-m"+sys.argv[1]+"100"
Moore().Simulation = True
#Moore().OutputLevel = 3
Moore().outputFile = 'Bs_hlt1.dst'
