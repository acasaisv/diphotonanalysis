
from   Gaudi.Configuration import *
import GaudiPython
from Configurables import DaVinci
import sys

### STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
                          
streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
#DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().ProductionType = "Stripping"




datatype=sys.argv[2]
#############
if datatype=="2016":
    DaVinci().Simulation = True
    DaVinci().DataType = "2016"
    DaVinci().DDDBtag  = "dddb-20150724"
    DaVinci().CondDBtag = "sim-20161124-2-vc-m"+sys.argv[1]+"100"
    DaVinci().InputType = "DST" # if running on stripped data... change


if datatype=="2017":
    DaVinci().Simulation = True
    DaVinci().DataType = "2017"
    DaVinci().DDDBtag  = "dddb-20170721-3"
    DaVinci().CondDBtag = "sim-20180411-vc-m"+sys.argv[1]+"100"
    DaVinci().InputType = "DST" # if running on stripped data... change
    
if datatype=="2016data":
    DaVinci().DataType = "2016"
    DaVinci().Simulation= False
    DaVinci().InputType="DST"
    DaVinci().Lumi = True
if datatype=="2017data":
    DaVinci().DataType = "2017"
    DaVinci().Simulation= False
    DaVinci().InputType="DST"
    DaVinci().Lumi = True
if datatype=="2018data":
    DaVinci().DataType = "2018"
    DaVinci().Simulation= False
    DaVinci().InputType="DST"
    DaVinci().Lumi = True



# root = "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00056609/0000/"
# files = filter(lambda x: ".dst" in x,os.listdir(root))
# DaVinci().Input = map(lambda x: root+x,files)

# =============================================================================

# =============================================================================

#from trigger_list import *

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
Butuple = DecayTreeTuple("DTTBs2GammaGamma")
Butuple.Decay = "B_s0 -> ^gamma ^gamma"
Butuple.Inputs = ["Phys/Bs2GammaGamma_NoConvLine/Particles"]

Butuple.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     "TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                     "TupleToolPhotonInfo",
                     "TupleToolCaloDigits",
                     "TupleToolCaloHypo",
                     ]

if DaVinci().getProp('Simulation'):
    mc_truth = Butuple.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    Butuple.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST": # doesnt work with mDST
        Butuple.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
        #Butupl.TupleToolMCBackgroundInfo.OutputLevel = VERBOSE

                                        
Butuple.addBranches({"B_s0": "B_s0 -> gamma gamma",
                     "gamma": "B_s0 -> ^gamma gamma",
                     "gamma0": "B_s0 -> gamma ^gamma",
                     })

Butuple.addTupleTool('TupleToolTISTOS/TISTOSTool')
Butuple.TISTOSTool.VerboseL0 = True
Butuple.TISTOSTool.VerboseHlt1 = True
Butuple.TISTOSTool.VerboseHlt2 = True
Butuple.TISTOSTool.TriggerList = ["L0ElectronDecision","L0PhotonDecision","Hlt1B2GammaGammaDecision","Hlt1B2GammaGammaHighMassDecision","Hlt1MBNoBiasDecision","Hlt2RadiativeB2GammaGammaDecision"]

Butuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
Butuple.LoKi_All.Variables =  {
    'TRUEID' : 'MCID',
    'M'      : 'MCMASS',
    'THETA'  : 'MCTHETA',
    'PT'     : 'MCPT',
    'PX'     : 'MCPX',
    'PY'     : 'MCPY',
    'PZ'     : 'MCPZ',
    'ETA'    : 'MCETA',
    'PHI'    : 'MCPHI'
        }


# ButupleStr = Butuple.clone("DTTBs2GammaGamma_Wide")
# ButupleStr.Inputs = ['Phys/Bs2GammaGamma_NoConvWideLine/Particles']

# DaVinci().UserAlgorithms +=[ButupleStr]
DaVinci().UserAlgorithms += [Butuple]

DaVinci().TupleFile = "Bu_tuple.root"


gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()

from GaudiPython import GaudiAlgs
class MyAlg(GaudiAlgs.TupleAlgo):

    def execute(self):
        tup=self.nTuple("Counters")
        val=0

        if TES["Rec/Track/Best"]:
           val= TES["Rec/Track/Best"].size()
           
        tup.column("NoTracks",int(val))
        if not "data" in datatype:
            tup.column("NoPars",TES["MC/Particles"].size())
        tup.column("DummyCounter",1.)
        

        tup.write()

        return GaudiPython.SUCCESS

gaudi.addAlgorithm(MyAlg("counters"))

gaudi.run(-1)
gaudi.stop()
gaudi.finalize()

# =============================================================================

# =============================================================================
## Job steering
# if __name__ == '__main__' :

#    ## make printout of the own documentations
#    print __doc__
#    import os
#    gaudi.run(1000)#2000)
#    TES = gaudi.evtsvc()
#    gaudi.stop()
#    gaudi.finalize()

# =============================================================================
# The END
# =============================================================================
