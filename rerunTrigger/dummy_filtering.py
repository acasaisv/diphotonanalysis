import sys

j = Job(name="dummy L0")

ConditionsL0=  'from Configurables import L0App\n'\
                 'L0App().DataType = "2017"\n'\
                 'L0App().DDDBtag  = "dddb-20170721-3"\n'\
                 'L0App().CondDBtag = "sim-20180411-vc-m{}100"\n'.format("d")

myApp = GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/DaVinciDev_v44r7")


j.application = myApp
j.application.useGaudiRun = False
j.application.options=["filterStrippingBs.py"]
j.application.extraArgs=["d","2017"]
j.inputdata=BKQuery(sys.argv[1]).getDataset()[0:1]
j.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
j.backend = Dirac()
j.outputfiles = [DiracFile('*.dst')]
j.submit()

