j = Job(name="dummy L0")

ConditionsL0=  'from Configurables import L0App\n'\
                 'L0App().DataType = "2017"\n'\
                 'L0App().DDDBtag  = "dddb-20170721-3"\n'\
                 'L0App().CondDBtag = "sim-20180411-vc-m{}100"\n'.format("d")

myApp = GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/MooreDev_v28r3")


j.application = myApp
j.application.useGaudiRun = True
j.application.options=["ReRun_L0_MgDn.py"]
    
j.application.extraOpts=ConditionsL0
datalink = LHCbDataset()
file=DiracFile(lfn=str(sys.argv[1]))
datalink.append(file)
j.inputdata=datalink
j.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
j.backend = Dirac()
j.outputfiles = [DiracFile('*.dst')]
j.submit()

