################################################### NECESSARY BLURB #################################################
from Gaudi.Configuration import *
from Configurables import Moore

Moore().ForceSingleL0Configuration = False
Moore().UseTCK = True
Moore().InitialTCK="0x617d18a4"
Moore().Split = "Hlt2"
Moore().CheckOdin = False
Moore().EnableDataOnDemand = True
Moore().WriterRequires = []
Moore().RemoveInputHltRawBanks=False
# Moore().DataType = '2017'
# Moore().DDDBtag  = "dddb-20170721-3"
# Moore().CondDBtag = "sim-20180411-vc-m"+sys.argv[1]+"100"
Moore().Simulation = True
#Moore().OutputLevel = 0
Moore().outputFile = 'Bs_hlt2.dst'

