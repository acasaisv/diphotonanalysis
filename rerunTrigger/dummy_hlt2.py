j = Job(name="dummy L0")

ConditionsL0=  'from Configurables import L0App\n'\
                 'L0App().DataType = "2017"\n'\
                 'L0App().DDDBtag  = "dddb-20170721-3"\n'\
                 'L0App().CondDBtag = "sim-20180411-vc-m{}100"\n'.format("d")

ConditionsMoore= 'from Configurables import Moore\n'\
                 'Moore().DataType = "2017"\n'\
                 'Moore().DDDBtag  = "dddb-20170721-3"\n'\
                 'Moore().CondDBtag = "sim-20180411-vc-m{}100"\n'.format("d")

myApp = GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/MooreDev_v28r3")


j.application = myApp
j.application.useGaudiRun = True
j.application.options=["hlt2.py"]
    
j.application.extraOpts=ConditionsMoore
datalink = LHCbDataset()
file=DiracFile(lfn=str(sys.argv[1]))
datalink.append(file)
j.inputdata=datalink
j.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
j.backend = Dirac()
j.outputfiles = [DiracFile('*.dst')]
#j.backend.settings['BannedSites']=["LCG.CNAF.it","LCG.CERN.cern"]
j.submit()

