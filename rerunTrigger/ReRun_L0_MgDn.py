# General configuration
import sys

from Configurables import L0App
app = L0App()
app.Simulation = True
app.ReplaceL0Banks = True
app.outputFile = ""
app.EvtMax = -1
app.TCK = '0x18a4'

#outputdir = ""
# app.DataType = '2017'
# app.DDDBtag  = "dddb-20170721-3"
# app.CondDBtag = "sim-20180411-vc-m"+sys.argv[1]+"100"


from Configurables import EventSelector
EventSelector().PrintFreq = 100

# InputCopyStream
from GaudiConf import IOHelper
from Configurables import InputCopyStream
input_copy = IOHelper().outputAlgs("%s_L0_0x1801.dst" % ("Bs"), InputCopyStream('CopyStream'), writeFSR = True)[0]

# Juggle raw event
from Configurables import GaudiSequencer
from Gaudi.Configuration import ApplicationMgr


juggleSeq = GaudiSequencer("JuggleSeq")
ApplicationMgr().TopAlg.insert(0, juggleSeq)
# Move the old Hlt banks to a new location
from Configurables import RawEventFormatConf
RawEventFormatConf().forceLoad()
myLocs = RawEventFormatConf().Locations
myNames= RawEventFormatConf().RecoDict
newName = "Custom"
oldName = "Moore"
myLocs[newName] = myLocs[myNames[oldName]]
for k in myLocs[newName].keys():
    if str(k).startswith('Hlt'):
        myLocs[newName][k] = 'PrevTrig/RawEvent'
        myNames[newName] = newName

from Configurables import RawEventJuggler
RawEventJuggler().Input = 4.1
RawEventJuggler().Output = newName
RawEventJuggler().Sequencer = juggleSeq
RawEventJuggler().WriterOptItemList = input_copy
RawEventJuggler().KillExtraNodes = True
RawEventJuggler().KillExtraBanks = True
RawEventJuggler().KillExtraDirectories = True
#RawEventJuggler().KillInputBanksBefore = "Hlt*"
                            
# Use RecEventTime to avoid problems with pre-juggled ODIN banks
from Configurables import DecodeRawEvent,RecombineRawEvent
DecodeRawEvent().EvtClockBank=""
RecombineRawEvent()
from Configurables import EventClockSvc
clockSvc = EventClockSvc(EventTimeDecoder = 'RecEventTime/RecEventTime:PUBLIC')

# Timing table to make sure things work as intended
from Configurables import AuditorSvc, LHCbTimingAuditor
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

# Output writers
# L0 filter
#from Configurables import L0DUFromRawAlg
#l0du_alg = L0DUFromRawAlg()
# from Configurables import LoKiSvc
# LoKiSvc().Welcome = False
# from Configurables import LoKi__L0Filter
# filter_code = "|".join(["L0_CHANNEL('%s')" % chan for chan in ['Electron','Photon','Hadron','Muon','DiMuon']])
# l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)
# MDF writer
# from Configurables import LHCb__MDFWriter as MDFWriter
# mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
# Connection = 'file://%s/%s_L0_0x1801.dst' % (outputdir, "Bs") )
# Sequence
writer_seq = GaudiSequencer('WriterSeq')
#l0_filter?
#writer_seq.Members = [mdf_writer, input_copy]
writer_seq.Members = [input_copy]
ApplicationMgr().OutStream = [writer_seq]
