import sys

fileType     = 'DST'


print('Rerunning Trigger ')

task         = LHCbTask()
task.name    = "Rerunning Trigger for Bs/ALP 2 gamma gamma"
task.comment = "Rerun trigger task"
task.float   = 10

HOME = "/scratch03/adrian.casais/DiPhotons/rerunTrigger/"

# argumento 1: bkk path

#    DaVinci: Filtering by stripping dsts
polarity = "u"
if "MagDown" in sys.argv[1]:
    polarity = "d"


ConditionsMoore= 'from Configurables import Moore\n'\
                 'Moore().DataType = "2017"\n'\
                 'Moore().DDDBtag  = "dddb-20170721-3"\n'\
                 'Moore().CondDBtag = "sim-20180411-vc-m{}100"\n'.format(polarity)
ConditionsL0=  'from Configurables import L0App\n'\
                 'L0App().DataType = "2017"\n'\
                 'L0App().DDDBtag  = "dddb-20170721-3"\n'\
                 'L0App().CondDBtag = "sim-20180411-vc-m{}100"\n'.format(polarity)

step1 = LHCbTransform(name="step1_DaVinci(filtering)",backend=Dirac())
step1.application = GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/DaVinciDev_v44r7")
step1.application.useGaudiRun = False
step1.application.options = [HOME + 'filterStrippingBs.py']
step1.addInputData(BKQuery(sys.argv[1]).getDataset())
step1.application.extraArgs = [polarity,"2017"]
step1.splitter = SplitByFiles ( filesPerJob = 8 , ignoremissing=True)
step1.outputfiles = [DiracFile("*.dst")]
task.appendTransform(step1)


#    Moore (Rerun L0)
step2 = LHCbTransform(name="step2_MooreL0",backend=Dirac())
step2.application =GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/MooreDev_v28r3p1")
step2.application.options = [HOME + "ReRun_L0_MgDn.py"]
#step2.application.extraArgs=[polarity]
step2.application.extraOpts=ConditionsL0
datalink = TaskChainInput()
datalink.include_file_mask = ['\.dst$']
datalink.input_trf_id = step1.getID()
step2.addInputData(datalink)
step2.splitter = SplitByFiles ( filesPerJob = 2 , ignoremissing=True)
step2.outputfiles = [DiracFile("*.dst")]
step2.delete_chain_input = True
task.appendTransform(step2)

#    Moore (Rerun HLT1)
step3 = LHCbTransform(name="step3_MooreHlt1",backend=Dirac())
step3.application =GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/MooreDev_v28r3p1")
step3.application.options = [HOME + "hlt1.py"]
step3.application.extraOpts=ConditionsMoore
datalink = TaskChainInput()
datalink.include_file_mask = ['\.dst$']
datalink.input_trf_id = step2.getID()
step3.addInputData(datalink)
step3.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
step3.outputfiles = [DiracFile("*.dst")]
step3.delete_chain_input = True
task.appendTransform(step3)

#    Moore (Rerun HLT2)
step4 = LHCbTransform(name="step4_MooreHlt2",backend=Dirac())
step4.application =GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/MooreDev_v28r3p1")
step4.application.options = [HOME + "hlt2.py"]
step4.application.extraOpts=ConditionsMoore
datalink = TaskChainInput()
datalink.include_file_mask = ['\.dst$']
datalink.input_trf_id = step3.getID()
step4.addInputData(datalink)
step4.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
step4.outputfiles = [DiracFile("*.dst")]
step4.delete_chain_input = True
task.appendTransform(step4)

#    DaVinci Produce final ntuples
step5 = LHCbTransform(name="step5_DaVinci(filtering)",backend=Dirac())
step5.application = GaudiExec(platform = "x86_64-slc6-gcc62-opt",directory="/scratch03/adrian.casais/DiPhotons/DaVinciDev_v44r7")
step5.application.useGaudiRun = False
step5.application.options = [HOME + 'DaVinciStep.py']
step5.application.extraArgs = [ polarity, "2017" ]
datalink = TaskChainInput()
datalink.include_file_mask = ['\.dst$']
datalink.input_trf_id = step4.getID()
step5.addInputData(datalink)
step5.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
step5.outputfiles = [DiracFile("*.root")]
task.appendTransform(step5)

step5.delete_chain_input = False




task.run()
