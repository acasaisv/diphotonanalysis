#2017-2018
def send_job(name,bkk,polarity,year):
    j = Job(name=name)
    myApp = GaudiExec()

    myApp.directory = "/scratch03/adrian.casais/DiPhotons/DaVinciDev_v44r7"

    j.application = myApp
    j.application.useGaudiRun = False
    j.application.platform = 'x86_64-slc6-gcc62-opt'
    j.application.options=['filterStrippingBs.py']
    
    j.application.extraArgs=[polarity,year]
    if polarity=="u":
        bkk.replace("MagDown","MagUp")
    j.inputdata=BKQuery(bkk).getDataset()
    j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
    j.backend = Dirac()
    
    j.outputfiles = [LocalFile('*.dst')]


    j.submit()

bkks=["/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/96000000/FULL.DST"]#,"/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/96000000/FULL.DST","/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/96000000/FULL.DST"]

bkks = ["/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/11154001/LDST","/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/13154001/LDST"]
#JpsiPhi-> ee : 13154001 : MagDown 00083795 : MagUp83787
#JPsiPhi - > mm : 13144001 : MagDown 00083721 : MagUp 00083723

#JPsiKstar -> ee : 11154001 : MagDown 00083517 : MagUp 00083519
#JPsiKstar - > mm : 11144001: MagDown 00083523 : MagUp 00083521
bkks=["/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/13144001/LDST"]
/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/11144001/LDST

for polarity in ["u","d"]:
    for bkk in bkks:
        year="2017"
        # if "2016" in bkk:
        #     year="2016data"
        # else:
        #     year="2018data"
        name='StrippingBs-Jpsikstar-phi-'+year+"-"+polarity+"-dst"
        send_job(name,bkk,polarity,year)
        
