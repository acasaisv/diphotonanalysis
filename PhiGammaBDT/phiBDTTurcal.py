import uproot3 as uproot
import matplotlib.pyplot as plt
import pandas as pd



var_list = [
    'B_DIRA_OWNPV',
    'phi_OWNPV_CHI2',
    #'phi_PT',
    'kplus_IPCHI2_OWNPV',
    'kminus_IPCHI2_OWNPV',
    #'gamma_1.00_cc_asy_PT',
    #'gamma_1.35_cc_asy_PT',
    #'gamma_1.70_cc_asy_PT',
    #'gamma_1.00_cc_asy_P',
    #'gamma_1.35_cc_asy_P',
    #'gamma_1.70_cc_asy_P',
    #'kminus_PT',
    #'kplus_PT',
    #'kminus_PIDK',
    #'kplus_PIDK',
    
     ]

all_cone_vars=[]
all_cone_vars_1=[]

trigger_vars = ['B_L0ElectronDecision_Dec','B_L0PhotonDecision_Dec',
                #'B_Hlt1TrackMVADecision_Dec',
                #'B_Hlt2RadiativeBs2PhiGammaDecision_Dec',
                'B_M','phi_M'
                ]


L0 = '(B_L0ElectronDecision_Dec | B_L0PhotonDecision_Dec)'
L1 = 'B_Hlt1TrackMVADecision_Dec'
L2 = 'B_Hlt2RadiativeBs2PhiGammaDecision_Dec'
trigger_cond = '{0} & {1} & {2}'.format(L0,L1,L2)
trigger_cond = L0


root = '/scratch47/adrian.casais/ntuples/turcal'
f_b = uproot.open(root + '/turcal.root')
t_b = f_b['Bs2phigamma_Tuple/DecayTree']

# f_b = uproot.open(root + '/turcal.root')
# t_b = f_b['Bs2phigamma_Tuple/DecayTree']
root = '/scratch47/adrian.casais/ntuples/turcal'            
f_s = uproot.open(root + '/b02phigammaMC-0smear.root')
t_s = f_s['DecayTree/DecayTree']



signal_df = t_s.pandas.df(branches=var_list+trigger_vars)
background_df  = t_b.pandas.df(branches=var_list+trigger_vars)

signal_df = signal_df.query(trigger_cond)
background_df.query(trigger_cond,inplace=True)
#background_df = background_df.query('({0}) & '.format(trigger_cond))
import xgboost as xgb
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

#signal_df = pd.DataFrame()
#var_list += ['category']
signal_df["category"]= 1 #Use 1 for signal
background_df["category"] = 0 #use 0 for bkg

#background_df.query('B_M <5150 or B_M>5700',inplace=True)
background_df.query('B_M>5700',inplace=True)
training_data = pd.concat([background_df[var_list+['category'] ],signal_df[var_list+['category']]],copy=True,ignore_index=True,sort=False)
training_data=training_data.sample(frac=1).reset_index(drop=True)
training_data = training_data.dropna()
mask = np.random.rand(len(training_data)) < 0.2

_train_data = training_data.iloc[mask]
_test_data = training_data.iloc[~mask]
x,y = _train_data[var_list],_train_data["category"]
x_test,y_test = _test_data[var_list],_test_data['category']

types = [
    'xgb',
    #'sk-GBC',
    #'sk-BDT',
    #'sk-MLP',
    #'sk-ADA'
    ]

plot = False
if plot:
    fig = plt.figure()
    j = 1
    for var in all_cone_vars:
        ax = fig.add_subplot(2,3,j)
        ax.set_title(var)
        ax.hist(signal_df[var],label='signal',bins=30)
        ax.hist(background_df[var],label='background',bins=50)
        j+=1

    plt.legend()
    plt.show()

#r = 'all'
indices = np.arange(0,len(training_data))
j = 1
#fig = plt.figure()
proba_sig_train = {}
proba_sig_test = {}
proba_bkg_train = {}
proba_bkg_test = {}
proba_test = {}
proba_train = {}
#x,y,x_test,y_test = {},{},{},{}         
for type in types:
        #training_data['proba-{}'.format(type)] = np.zeros(shape=(len(training_data),1))
        
        if type =='xgb':
            
            bdt = xgb.XGBClassifier(
                eta=0.7,
                objective='binary:logistic',)
                ##eval,
                #n_estimators=1000,learning_rate=1)
        if type == 'sk-MLP':
                
            bdt = MLPClassifier(
                activation='relu',
                solver='adam',
                alpha=2e-05,
                epsilon=1e-08,
                max_iter = 500,
                tol = 0.0001,
                max_fun = 20000
                )
            scaler = StandardScaler()
            scaler.fit(x)
            x = scaler.transform(x)
            x_test = scaler.transform(x_test)
        if type== 'sk-GBC':
                
            bdt = GradientBoostingClassifier(
                loss='deviance',
                learning_rate=0.1,
                min_samples_leaf=0.1,
                n_estimators=100,
                #subsample=0.5,
                criterion='mae',
                #min_samples_split=0.2
                    )
        if type=='sk-BDT':
            bdt = DecisionTreeClassifier(max_depth=5)

        if type == 'sk-ADA':
            bdt = AdaBoostClassifier(n_estimators=100)
                
        bdt.fit(x,y)
        proba_sig_test[type] = bdt.predict_proba(x_test[np.array(y_test,dtype='bool')])[:,1]
        proba_sig_train[type] = bdt.predict_proba(x[np.array(y,dtype='bool')])[:,1]
        proba_bkg_test[type] = bdt.predict_proba(x_test[~np.array(y_test,dtype='bool')])[:,1]
        proba_bkg_train[type] = bdt.predict_proba(x[~np.array(y,dtype='bool')])[:,1]

        proba_test[type] = bdt.predict_proba(x_test)[:,1]
        proba_train[type] = bdt.predict_proba(x)[:,1]
        #ax = fig.add_subplot(2,3,j)
        j+=1
        n_bins = 10
        marker='o'
        range = (0,1)
        alpha = 0.4
        
        proba_sig_test_hist, bins = np.histogram(proba_sig_test[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_sig_test_hist,marker=marker,linestyle='None',label='Signal test proba',alpha = alpha)
        
        proba_sig_train_hist,bins=np.histogram(proba_sig_train[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_sig_train_hist,marker=marker,linestyle='None',label='Signal train proba',alpha=alpha)
        
        proba_bkg_test_hist,bins=np.histogram(proba_bkg_test[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_bkg_test_hist,marker=marker,linestyle='None',label='Background test proba',alpha=alpha)
        
        proba_bkg_train_hist,bins = np.histogram(proba_bkg_train[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_bkg_train_hist,marker=marker,linestyle='None',label='Background train proba',color='black',alpha=alpha)

plt.legend(loc='center')
        
plt.show()

from sklearn.metrics import roc_curve,auc
from copy import copy
fprs,tprs = [],[]
for type in types:
        fpr,tpr,thresholds = roc_curve(y_test,proba_test[type])
        fprs.append(copy(fpr));tprs.append(copy(tpr))
        plt.plot(fprs[-1],tprs[-1],label = '{0}'.format(type))

plt.legend()
plt.show()        
bdt.save_model('phigamma.bst')
import joblib
#joblib.dump(bdt,'phigamma.bst')
import pickle
with open('mask.pickle','wb') as handle:
    pickle.dump(mask,handle)
 
#from sklearn.model_selection import cross_val_score
#scores = cross_val_score(bdt,x,y,cv=5)

#kfold strategy
x,y = training_data[var_list],training_data["category"]
from sklearn.model_selection import KFold
kf = KFold(n_splits=5)
classifiers = []
for tri,tti in kf.split(x):
    x_train,y_train = x.iloc[tri],y.iloc[tri]
    classifiers.append(xgb.XGBClassifier(
        eta=0.7,
        objective='binary:logistic',) )
    classifiers[-1].fit(x_train,y_train)

import pickle
with open('bdts_turcal.pickle','wb') as handle:
    pickle.dump(classifiers,handle)
    
    
