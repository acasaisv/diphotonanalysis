from ROOT import *

gROOT.ProcessLine('.x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C')
gStyle.SetOptStat('')
ntup='/scratch47/adrian.casais/ntuples/signal/'

name = '4910004{}_MagDown.root'
name = '49100040_MagDown-newL0Clusters.root'


indices = [0,2,4,6,7,8]
masses = {0:'5 GeV',
          2:'7 GeV',
          4:'9 GeV',
          6:'11 GeV',
          7:'13 GeV',
          8:'4 GeV'}
colors = {0:kBlack,
          2:kRed,
          4:kBlue,
          6:kOrange-3,
          7:kPink+3,
          8:kGreen}
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22) && B_s0_TRUEID==54"

L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
#HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS)" 
HLT2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
aa = "&&"
cut = truth
cut = L0+aa+HLT1
#cut = '1'
#cut = cut + "&& gamma_L0Calo_ECAL_TriggerET > 200 && gamma0_L0Calo_ECAL_TriggerET>200"
f = TFile(ntup+name.format(0))
t_ = f.Get('B2GammaGammaTuple/DecayTree')
dummy = TFile('dummy.root','RECREATE')
t = t_.CopyTree(cut)

# t.SetAlias('cos_theta','(gamma_TRUEP_X*gamma0_TRUEP_X+gamma_TRUEP_Y*gamma0_TRUEP_Y+gamma_TRUEP_Z*gamma0_TRUEP_Z)/gamma_TRUEP_E/gamma0_TRUEP_E')


# t.SetAlias('cos_theta0','gamma0_TRUEP_Z/gamma0_TRUEP_E')
# t.SetAlias('cos_theta1','gamma_TRUEP_Z/gamma_TRUEP_E')

# t.SetAlias('sin_theta0','sqrt(1-cos_theta0*cos_theta0)')
# t.SetAlias('sin_theta1','sqrt(1-cos_theta1*cos_theta1)')

t.SetAlias('cos_theta','(gamma_PX*gamma0_PX+gamma_PY*gamma0_PY+gamma_PZ*gamma0_PZ)/gamma_P/gamma0_P')
t.SetAlias('cos_theta0','gamma0_PZ/gamma0_P')
t.SetAlias('cos_theta1','gamma_PZ/gamma_P')

t.SetAlias('sin_theta0','sqrt(1-cos_theta0*cos_theta0)')
t.SetAlias('sin_theta1','sqrt(1-cos_theta1*cos_theta1)')



t.SetAlias('B_s0_pseudoM','sqrt( 2*gamma_PE*gamma0_PE*(1-cos_theta) )')
t.SetAlias('B_s0_pseudoM1','sqrt( 2*gamma_PT*gamma0_PT*(1-cos_theta)/(sin_theta1*sin_theta0) )')
t.SetAlias('B_s0_Hlt1M','sqrt( 2*gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(1-cos_theta)/(sin_theta1*sin_theta0) )')



t.SetAlias('deltaX','(gamma0_L0Calo_ECAL_xTrigger-gamma_L0Calo_ECAL_xTrigger)**2')
t.SetAlias('deltaY','(gamma0_L0Calo_ECAL_yTrigger-gamma_L0Calo_ECAL_yTrigger)**2')
t.SetAlias('sumX','(gamma0_L0Calo_ECAL_xTrigger**2+gamma0_L0Calo_ECAL_yTrigger**2)')
t.SetAlias('sumY','(gamma_L0Calo_ECAL_xTrigger**2+gamma_L0Calo_ECAL_yTrigger**2)')

t.SetAlias('B_s0_Hlt1M0','sqrt(gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(deltaX+deltaY)/sqrt(sumX*sumY))')


#Hlt1 efficiency recovered
print( 100.*t.GetEntries('B_s0_Hlt1M0 > 3500 && B_s0_Hlt1M0 < 6000 && (gamma_L0Calo_ECAL_TriggerET + gamma0_L0Calo_ECAL_TriggerET)>8000 && min(gamma_L0Calo_ECAL_TriggerET,gamma0_L0Calo_ECAL_TriggerET) > 3500')/t.GetEntries("B_s0_Hlt1M0 > 3500 && B_s0_Hlt1M0 < 6000 ") )

# Checking that angles don't have significant effect on the invariant mass
low = 2000
up =  7000
h = TH1F('h','h',100,low,up)
h0 = TH1F('h0','h0',100,low,up)
h1 = TH1F('h1','h1',100,low,up)
h2 = TH1F('h2','h2',100,low,up)
t.Project('h','B_s0_pseudoM')
t.Project('h0','B_s0_pseudoM1')
t.Project('h1','B_s0_Hlt1M0')
t.Project('h2','B_s0_Hlt1M')

h0.SetLineColor(kRed)
h.SetLineColor(kBlue)
h1.SetLineColor(kGreen)
h2.SetLineColor(kBlack)

c = TCanvas()
leg=TLegend(.3,.3,.3,.3)
leg.AddEntry(h1,'Reconstructed angle','L')
leg.AddEntry(h2,'True angle','L')
#h0.DrawNormalized()
h2.GetXaxis().SetTitle('M(#gamma #gamma)  [Mev]')
h2.GetYaxis().SetTitle('No. entries / 50 MeV')
#h2.DrawNormalized()
h1.DrawNormalized('same')
leg.Draw()
#h.DrawNormalized('same')



#Comparing p distributions of event matched and not matched

h3 = TH1F('h3','h3',100,0,400000)
h4 = TH1F('h4','h4',100,0,400000)
t.Project('h3','max(gamma0_TRUEP_E,gamma_TRUEP_E)','B_s0_Hlt1M0>200')
t.Project('h4','max(gamma0_TRUEP_E,gamma_TRUEP_E)','B_s0_Hlt1M0<200')

h3.SetLineColor(kGreen)
h4.SetLineColor(kBlack)

c2 = TCanvas()
leg2=TLegend(.3,.3,.3,.3)
leg2.AddEntry(h3,'Matched to L0 cluster','L')
leg2.AddEntry(h4,'Not matched to L0 cluster','L')

h4.GetXaxis().SetTitle('P [MeV]')
h4.GetYaxis().SetTitle('No. entries / 400 MeV')
h4.DrawNormalized()
h3.DrawNormalized('same')
leg2.Draw()





