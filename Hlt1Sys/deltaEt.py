from ROOT import *

#gROOT.ProcessLine('.x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C')
gStyle.SetOptStat('')
ntup='/scratch47/adrian.casais/ntuples/signal/'

name = '4910004{}_MagDown.root'

indices = [0,2,4,6,7,8]
masses = {0:'5 GeV',
          2:'7 GeV',
          4:'9 GeV',
          6:'11 GeV',
          7:'13 GeV',
          8:'4 GeV'}
colors = {0:kBlack,
          2:kRed,
          4:kBlue,
          6:kOrange-3,
          7:kPink+3,
          8:kGreen}
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22) && B_s0_TRUEID==54"

L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
#HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS)" 
HLT2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
aa = "&&"
cut = truth
cut = L0+aa+HLT1+aa+HLT2

f = TFile(ntup+name.format(0))
t_ = f.Get('B2GammaGammaTuple/DecayTree')
dummy = TFile('dummy.root','RECREATE')
t = t_.CopyTree(truth+"&& gamma_L0Calo_ECAL_TriggerET > 200 && gamma0_L0Calo_ECAL_TriggerET>200")
t = t_.CopyTree("gamma_L0Calo_ECAL_TriggerET > 200 && gamma0_L0Calo_ECAL_TriggerET>200")
t.SetAlias('gamma0_ETA','0.5*log((gamma0_P + gamma0_PZ)/(gamma0_P - gamma0_PZ))')


pts = [2000,4000,6000,8000,10000,12000]
pts = [2000,8000,12000]
etas = [2,3,5]

c = []
for i in range(len(pts)-1):
    for j in range(len(etas)-1):
        c.append(TCanvas())
        t.Draw('gamma0_PT - gamma0_L0Calo_ECAL_TriggerET','gamma0_PT > {0} && gamma0_PT < {1} && gamma0_ETA > {2} && gamma0_ETA < {3}'.format(pts[i],pts[i+1],etas[j],etas[j+1]))
