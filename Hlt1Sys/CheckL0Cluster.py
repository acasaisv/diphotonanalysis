import sys
sys.path.append('../')
from reweight.reweight import reweight_etammg_mc, dimuon_mass
import pandas as pd
from helpers import ufloat_eff, ufloat_eff_w, get_ALPsdf

import numpy as np
import uproot

def find_threshold(df):
    pts =np.linspace(6000,12000,40)
    for pt in pts:
        rat = ufloat_eff_w(df.query(f'gamma_PT > {pt} and gamma_L0Calo_ECAL_TriggerET <6119.5')['sweights'],df['sweights'])
        print(f'pt: {pt}. ratio = {100.*rat}')

root = '/scratch47/adrian.casais/ntuples/turcal'
dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')
df = pd.read_hdf(root+'/etammgTurcalData.h5',key='df')

df.query('gamma_PT > 3000 and gamma_P > 6000 and gamma_CaloHypo_Saturation==0',inplace=True)
#df.query('eta_M > 465 & eta_M < 630',inplace=True)
#df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
#df.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)

dfMC.query('gamma_PT > 3000 and gamma_P > 6000 and gamma_CaloHypo_Saturation==0',inplace=True)
#dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
#dfMC.query('eta_M > 465 & eta_M < 630',inplace=True)
#df = dimuon_mass(df)
#dfMC = dimuon_mass(dfMC)

variables = ['eta_PT','eta_ETA','eta_P','dimuon_M']

    
df.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & gamma_L0Global_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS",inplace=True)
dfMC.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & gamma_L0Global_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS",inplace=True)

# print('MC')
# find_threshold(dfMC)
# print('Data')
# find_threshold(df)

#f = uproot.open('/scratch47/adrian.casais/ntuples/signal/sim10/49100040_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree')
# dfalps = f.arrays(['B_s0_L0ElectronDecision_TOS','B_s0_L0PhotonDecision_TOS','gamma_L0Calo_ECAL_TriggerET','gamma_L0Global_TIS','gamma_PT','gamma_P','gamma0_L0Calo_ECAL_TriggerET','gamma0_L0Global_TIS','gamma_Hlt1Phys_TIS','gamma_Hlt2Phys_TIS','gamma_L0ElectronDecision_TOS','gamma_L0PhotonDecision_TOS'],library='pd')

dfalps = get_ALPsdf(41,extravars=['gamma_L0Global_TIS','gamma_Hlt1Phys_TIS','gamma_Hlt2Phys_TIS'],stripping=True)
truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
dfalps.query(truth,inplace=True)
dfalps.query('gamma_L0Global_TIS and gamma_Hlt1Phys_TIS and gamma_Hlt2Phys_TIS and'\
    ' gamma_PT > 3000 and gamma_P > 6000 and gamma_CaloHypo_Saturation==0 and gamma0_CaloHypo_Saturation==0 and'\
#' (B_s0_L0ElectronDecision_TOS or B_s0_L0PhotonDecision_TOS)'\
    ' (gamma_L0ElectronDecision_TOS==0 and gamma_L0PhotonDecision_TOS==0) and nSPDHits<450',inplace=True)
#dfalps.query('gamma_L0Global_TIS and gamma_Hlt1Phys_TIS and gamma_Hlt2Phys_TIS and gamma_PT > 3000 and gamma_P > 6000 and (gamma_L0ElectronDecision_TOS==0 and gamma_L0PhotonDecision_TOS==0) and gamma_CaloHypo_Saturation==0 and (gamma0_L0ElectronDecision_TOS or gamma0_L0PhotonDecision_TOS)',inplace=True)
df.query('gamma_PT > 3000 and gamma_P > 6000 and (gamma_L0ElectronDecision_TOS==0 and gamma_L0PhotonDecision_TOS==0 and nSPDHits<450 and `L0Data_Sum_Et,Prev1`<1000)',inplace=True)
dfMC.query('gamma_PT > 3000 and gamma_P > 6000 and (gamma_L0ElectronDecision_TOS==0 and gamma_L0PhotonDecision_TOS==0 and nSPDHits<450)',inplace=True)
import matplotlib.pyplot as plt
minpt=6000
maxpt=9000
plt.hist(dfalps.query(f'gamma_PT>{minpt} and gamma_PT <{maxpt}')['gamma_L0Calo_ECAL_TriggerET'],bins=50,density=True,range=(0,5000))
plt.hist(dfMC.query(f'gamma_PT>{minpt} and gamma_PT <{maxpt}')['gamma_L0Calo_ECAL_TriggerET'],bins=50,weights=dfMC.query(f'gamma_PT>{minpt} and gamma_PT <{maxpt}')
['sweights'],histtype='step',density=True,range=(0,5000))
plt.hist(df.query(f'gamma_PT>{minpt} and gamma_PT <{maxpt}')['gamma_L0Calo_ECAL_TriggerET'],bins=50,weights=df.query(f'gamma_PT>{minpt} and gamma_PT <{maxpt}')
['sweights'],histtype='step',density=True,range=(0,5000))
plt.show()
plt.show()
pts = [3000,6000,np.inf]
for  ipt in range(len(pts)-1):
    minpt = pts[ipt]
    maxpt = pts[ipt+1]
    print(minpt,maxpt)

    print('Data:',ufloat_eff_w(df.query(f'gamma_L0Calo_ECAL_TriggerET >0 and gamma_PT > {minpt} and gamma_PT<{maxpt}')['sweights'],df.query(f'gamma_PT > {minpt} and gamma_PT<{maxpt}')['sweights']))
    print('MC:',ufloat_eff_w(dfMC.query(f'gamma_L0Calo_ECAL_TriggerET >0 and gamma_PT > {minpt} and gamma_PT<{maxpt}')['sweights'],dfMC.query(f'gamma_PT > {minpt} and gamma_PT<{maxpt}')['sweights']))
    print('MC alps:',ufloat_eff(dfalps.query(f'gamma_L0Calo_ECAL_TriggerET >0 and gamma_PT > {minpt} and gamma_PT<{maxpt}').shape[0],dfalps.query(f'gamma_PT > {minpt} and gamma_PT<{maxpt}').shape[0]))


print('Fraction of ALPs in (3000,4000)',ufloat_eff(dfalps.query(f'gamma_PT > {3000} and gamma_PT<{4000}').shape[0],dfalps.shape[0]))
print('Fraction of ALPs in (4000,5000)',ufloat_eff(dfalps.query(f'gamma_PT > {4000} and gamma_PT<{5000}').shape[0],dfalps.shape[0]))
