from ROOT import *

#gROOT.ProcessLine('.x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C')
gStyle.SetOptStat('')
ntup='/scratch47/adrian.casais/ntuples/signal/'

name = '4910004{}_MagDown.root'
name = '49100040_MagDown-newL0Clusters.root'

indices = [0,2,4,6,7,8]
masses = {0:'5 GeV',
          2:'7 GeV',
          4:'9 GeV',
          6:'11 GeV',
          7:'13 GeV',
          8:'4 GeV'}
colors = {0:kBlack,
          2:kRed,
          4:kBlue,
          6:kOrange-3,
          7:kPink+3,
          8:kGreen}
truth="(abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22) && B_s0_TRUEID==54"

L0= "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"
HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
#HLT1 = "(B_s0_Hlt1B2GammaGammaDecision_TOS)" 
HLT2 = "B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
aa = "&&"
cut = truth
cut = L0+aa+HLT1+aa+HLT2

f = TFile(ntup+name.format(0))
t_ = f.Get('B2GammaGammaTuple/DecayTree')
dummy = TFile('dummy.root','RECREATE')
t = t_.CopyTree(truth)

def setTrues(tree,gamma,comp):
    tree.SetAlias('gamma{0}_true{1}'.format(gamma,comp),'(12720-PVZ[0])*gamma{0}_TRUEP_E/gamma{0}_TRUEP_{1} + PV{1}[0]'.format(gamma,comp))
for comp in 'X','Y','Z':
    for i in '0','':
        setTrues(t,i,comp)
        
t.SetAlias('deltaX','(gamma0_trueX-gamma_trueX)**2')
t.SetAlias('deltaY','(gamma0_trueY-gamma_trueY)**2')
t.SetAlias('sumX','(gamma0_trueX**2+gamma0_trueY**2)')
t.SetAlias('sumY','(gamma_trueX**2+gamma_trueY**2)')

t.SetAlias('deltaX0','(gamma0_CaloHypo_X-gamma_CaloHypo_X)**2')
t.SetAlias('deltaY0','(gamma0_CaloHypo_Y-gamma_CaloHypo_Y)**2')
t.SetAlias('sumX0','(gamma0_CaloHypo_X**2+gamma0_CaloHypo_Y**2)')
t.SetAlias('sumY0','(gamma_CaloHypo_X**2+gamma_CaloHypo_Y**2)')


#t.Draw('((gamma0_trueX-gamma0_CaloHypo_X))>>h(100,-10000,10000)')
t.SetAlias('B_s0_pseudoM','sqrt(gamma_TRUEPT*gamma0_TRUEPT*(deltaX+deltaY)/sqrt(sumX*sumY))')
t.SetAlias('B_s0_pseudoM0','sqrt(gamma_PT*gamma0_PT*(deltaX0+deltaY0)/sqrt(sumX0*sumY0))')
h = TH1F('h','h',100,0,20000)
h0 = TH1F('h0','h0',100,0,20000)
h1 = TH1F('h1','h1',100,0,20000)
t.Project('h','B_s0_pseudoM')
t.Project('h0','B_s0_pseudoM0')
t.Project('h1','B_s0_M')
h0.SetLineColor(kRed)
h.SetLineColor(kBlue)
h1.SetLineColor(kGreen)
h0.DrawNormalized()
h1.DrawNormalized('same')
h.DrawNormalized('same')

#t.Draw('gamma0_PT:gamma0_CaloHypo_Z>>h(100,12600,12800,100,0,15000)','1','colz')
