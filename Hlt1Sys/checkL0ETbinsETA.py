from ROOT import *
import uproot3 as uproot
import pandas
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from kinematic_bins import pTs,etas
import pickle

root = '/scratch47/adrian.casais/ntuples/turcal'
dfMC = pandas.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')
df = pandas.read_hdf(root+'/etammgTurcalData.h5',key='df')
df.query('gamma_L0Calo_ECAL_TriggerET > 10',inplace=True)
dfMC.query('gamma_L0Calo_ECAL_TriggerET > 10',inplace=True)


n_bins = 15 #has to be a square number
pts = np.linspace(2000,10000,n_bins + 1)
fig,ax = plt.subplots(5,3)
ax_flat =ax.flatten()
fig.suptitle("Blue: Data, Orange: MC")
fig.tight_layout()
n_subbins = 40
for i in range(n_bins):
    low = pts[i]
    high = pts[i+1]
    myMCdf = dfMC.query('gamma_PT > %i & gamma_PT < %i' % (low,high))
    mydf = df.query('gamma_PT > %i & gamma_PT < %i' % (low,high))
    ax_flat[i].set_title('PT in ({0:.2f},{1:.2f})'.format(low,high))
    ax_flat[i].hist(mydf.query('gamma_PT > %i & gamma_PT < %i' % (low,high))["gamma_L0Calo_ECAL_TriggerET"],bins=n_subbins,alpha=0.6,density=1,weights=mydf['sweights'])
    ax_flat[i].hist(myMCdf["gamma_L0Calo_ECAL_TriggerET"],bins=n_subbins,alpha=0.6,density=1,weights=myMCdf['sweights'])


ax_flat[0].legend(('Data','MC'))
plt.show()


