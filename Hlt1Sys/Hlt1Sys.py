
from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTsHlt1,etasHlt1,etas,pack_eff, get_error, ufloat_eff,pack_eff,ufloat_to_str
from uncertainties import ufloat
from plot_helpers import *
from helpers import get_ALPsdf, sim10_masses_map

pTs = pTsHlt1
etas = etasHlt1
import pickle


def get_df(i,bs=False):
    if type(i)!=str:
        root = '/scratch47/adrian.casais/ntuples/signal'
        f_s = uproot.open(root + '/491000{}_MagDown.root'.format(i))
    elif i=='bs':
        f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root')
    else:
        f_s = uproot.open('{}'.format(i))

    t_s = f_s['DTTBs2GammaGamma/DecayTree']
    
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            'gamma_CaloHypo_X',
            'gamma_CaloHypo_Y',
            'gamma0_CaloHypo_X',
            'gamma0_CaloHypo_Y',
            'gamma_CaloHypo_HypoE',
            'gamma_CaloHypo_HypoEt',
            'gamma0_CaloHypo_HypoE',
            'gamma0_CaloHypo_HypoEt',
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
       
            ]
    truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
    if bs:
        truth = "(abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22) & abs(B_s0_TRUEID)==531"
    df = t_s.pandas.df(branches=vars)
    df_original = t_s.pandas.df(branches=vars)
    df.query(truth,inplace=True)
    df.query('gamma_PT > 2500 & gamma0_PT > 2500',inplace=True)
    #probably above 7500 we don't have a systematic
    df.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS)',inplace=True)
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    df['gamma0_ETA'] = 0.5*np.log( (df['gamma0_P']+df['gamma0_PZ'])/(df['gamma0_P']-df['gamma0_PZ']) )
    
    df.reset_index(inplace=True)
    return df



def howmanyL0(df):
    print(10*"##")
    print("Fraction of events with cluster linking with TOS (simulation): {:.2f} %".format(100.*df.query('gamma_L0Calo_ECAL_TriggerET > 00 and gamma0_L0Calo_ECAL_TriggerET > 00 ').__len__()/df.__len__()))
    print(10*"##")
    num = df.query('gamma_L0Calo_ECAL_TriggerET > 00 and gamma0_L0Calo_ECAL_TriggerET > 00 ').shape[0]
    den = df.shape[0]
    return ufloat_eff(num,den)

def smear_pt(df,offset=0):
    with open ('distributions.p','rb') as handle:
        histograms,histogramsMC = pickle.load(handle)
        
    df['gamma_smearedPT']=6120
    df['gamma0_smearedPT']=6120
    
    df['gamma_MCsmearedPT']=6120
    df['gamma0_MCsmearedPT']=6120

 
    for ieta in range(len(etas)-1):
        eta = (etas[ieta],etas[ieta+1])
        for ipt in range(len(pTs)-1):
            pt = (pTs[ipt],pTs[ipt+1])
            condition = '{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma',pt[0],pt[1],eta[0],eta[1])
            condition0 = '{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma0',pt[0],pt[1],eta[0],eta[1])
            
            dflocal  = df.query(condition)
            dflocal0  = df.query(condition0)

            if pt[0] < 7500:
                offset = offset
                pk,xk=histogramsMC[eta][pt]
                
                dflocal.loc[:,'MCrvs'] = np.array(np.random.choice(xk,len(dflocal),p=pk) + offset)
                dflocal0.loc[:,'MCrvs'] = np.array(np.random.choice(xk,len(dflocal0),p=pk) + offset)

            
                # df.loc[dflocal.index,'gamma_MCsmearedPT'] = dflocal['gamma_PT'] - dflocal['MCrvs']
                # df.loc[dflocal0.index,'gamma0_MCsmearedPT'] = dflocal0['gamma0_PT'] - dflocal0['MCrvs']

                df.loc[dflocal.index,'gamma_MCsmearedPT'] = dflocal['gamma_PT']*(1 - dflocal['MCrvs'])
                df.loc[dflocal0.index,'gamma0_MCsmearedPT'] = dflocal0['gamma0_PT']*(1 - dflocal0['MCrvs'])

                #df.loc[dflocal.index,'gamma_MCsmearedPT'] = dflocal['MCrvs']
                #df.loc[dflocal0.index,'gamma0_MCsmearedPT'] = dflocal0['MCrvs']
                offset_data = offset
                pk,xk=histograms[eta][pt]
                
                dflocal.loc[:,'rvs'] = np.random.choice(xk,len(dflocal),p=pk) + offset_data
                dflocal0.loc[:,'rvs'] = np.random.choice(xk,len(dflocal0),p=pk) + offset_data
            
                df.loc[dflocal.index,'gamma_smearedPT'] = dflocal['gamma_PT']*(1 - dflocal['rvs'])
                df.loc[dflocal0.index,'gamma0_smearedPT'] = dflocal0['gamma0_PT']*(1 - dflocal0['rvs'])
                #df.loc[dflocal.index,'gamma_smearedPT'] = dflocal['rvs']
                #df.loc[dflocal0.index,'gamma0_smearedPT'] = dflocal0['rvs']

            else:
                continue
                # df.loc[dflocal.index,'gamma_smearedPT']=6120
                # df.loc[dflocal0.index,'gamma0_smearedPT']=6120

                # df.loc[dflocal.index,'gamma_MCsmearedPT']=6120
                # df.loc[dflocal0.index,'gamma0_MCsmearedPT']=6120



                  
    #df.query('gamma_smearedPT>2500 & gamma_MCsmearedPT>2500 & gamma0_smearedPT>2500 & gamma0_MCsmearedPT>2500',inplace=True)


    df.reset_index(inplace=True)
    for key in 'gamma_smearedPT','gamma_MCsmearedPT','gamma0_smearedPT','gamma0_MCsmearedPT':    
        df.loc[df[key]>=6119.99,key]=6120
    return df

def vetoNonMatched(df):
    df.query('gamma_L0Calo_ECAL_TriggerET > 10 & gamma0_L0Calo_ECAL_TriggerET > 10',inplace=True)
    return df

def build_mass(df):
    df.eval('cos_theta_true = (gamma_TRUEP_X*gamma0_TRUEP_X+gamma_TRUEP_Y*gamma0_TRUEP_Y+gamma_TRUEP_Z*gamma0_TRUEP_Z)/gamma_TRUEP_E/gamma0_TRUEP_E',inplace=True)
    df.eval('cos_theta = (gamma_PX*gamma0_PX+gamma_PY*gamma0_PY+gamma_PZ*gamma0_PZ)/gamma_P/gamma0_P',inplace=True)
    df.eval('sin_theta0_true = sqrt(1 - gamma0_TRUEP_Z**2/gamma0_TRUEP_E**2)',inplace=True)
    df.eval('sin_theta1_true = sqrt(1 - gamma_TRUEP_Z**2/gamma_TRUEP_E**2)',inplace=True)
    
    df.eval('sin_theta0 = sqrt(1 - gamma0_PZ**2/gamma0_P**2)',inplace=True)
    df.eval('sin_theta1 = sqrt(1 - gamma_PZ**2/gamma_P**2)',inplace=True)
    
    df.eval('cos_phi = gamma_PX/gamma_PT', inplace=True)
    df.eval('sin_phi = sqrt(1-gamma_PX**2/gamma_PT**2)', inplace=True)
    df.eval('cos_phi0 = gamma0_PX/gamma0_PT', inplace=True)
    df.eval('sin_phi0 = sqrt(1 - gamma0_PX**2/gamma0_PT**2)', inplace=True)

    df.eval('zTrigger = 12350.0',inplace=True)
    df.eval('txL0  = gamma_L0Calo_ECAL_xTrigger/zTrigger',inplace=True)
    df.eval('tyL0  = gamma_L0Calo_ECAL_yTrigger/zTrigger',inplace=True)
    df.eval('tx0L0  = gamma0_L0Calo_ECAL_xTrigger/zTrigger',inplace=True)
    df.eval('ty0L0  = gamma0_L0Calo_ECAL_yTrigger/zTrigger',inplace=True)

    df.eval('tx  = gamma_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty  = gamma_CaloHypo_Y/zTrigger',inplace=True)
    df.eval('tx0  = gamma0_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty0  = gamma0_CaloHypo_Y/zTrigger',inplace=True)

    

    

    #df.eval('gamma_L0Calo_ECAL_TriggerE = gamma_L0Calo_ECAL_TriggerET * sqrt(zTrigger**2/(gamma_L0Calo_ECAL_xTrigger**2+gamma_L0Calo_ECAL_yTrigger**2)+1 ) ',inplace=True)
    #df.eval('gamma0_L0Calo_ECAL_TriggerE = gamma0_L0Calo_ECAL_TriggerET * sqrt(zTrigger**2/(gamma0_L0Calo_ECAL_xTrigger**2+gamma0_L0Calo_ECAL_yTrigger**2)+1 ) ',inplace=True)
        
    df.eval('gamma_L0Calo_ECAL_TriggerEX = gamma_L0Calo_ECAL_TriggerET * txL0*sqrt(1/(txL0**2+tyL0**2)) ',inplace=True)
    df.eval('gamma_L0Calo_ECAL_TriggerEY = gamma_L0Calo_ECAL_TriggerET * tyL0*sqrt(1/(txL0**2+tyL0**2)) ',inplace=True)
    df.eval('gamma0_L0Calo_ECAL_TriggerEX = gamma0_L0Calo_ECAL_TriggerET * tx0L0*sqrt(1/(tx0L0**2+ty0L0**2)) ',inplace=True)
    df.eval('gamma0_L0Calo_ECAL_TriggerEY = gamma0_L0Calo_ECAL_TriggerET * ty0L0*sqrt(1/(tx0L0**2+ty0L0**2)) ',inplace=True)
    
    df.eval('gamma_L0Calo_ECAL_TriggerEXoffline = gamma_L0Calo_ECAL_TriggerET * tx*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma_L0Calo_ECAL_TriggerEYoffline = gamma_L0Calo_ECAL_TriggerET * ty*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma0_L0Calo_ECAL_TriggerEXoffline = gamma0_L0Calo_ECAL_TriggerET * tx0*sqrt(1/(tx0**2+ty0**2)) ',inplace=True)
    df.eval('gamma0_L0Calo_ECAL_TriggerEYoffline = gamma0_L0Calo_ECAL_TriggerET * ty0*sqrt(1/(tx0**2+ty0**2)) ',inplace=True)

    df.eval('gamma_MCSmearedEX = gamma_MCsmearedPT * tx*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma_MCSmearedEY = gamma_MCsmearedPT * ty*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma0_MCSmearedEX = gamma0_MCsmearedPT * tx0*sqrt(1/(tx0**2+ty0**2)) ',inplace=True)
    df.eval('gamma0_MCSmearedEY = gamma0_MCsmearedPT * ty0*sqrt(1/(tx0**2+ty0**2)) ',inplace=True)
    df.eval('gamma_SmearedEX = gamma_smearedPT * tx*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma_SmearedEY = gamma_smearedPT * ty*sqrt(1/(tx**2+ty**2)) ',inplace=True)
    df.eval('gamma0_SmearedEX = gamma0_smearedPT *tx0 *sqrt(1/(tx0**2+ty0**2)) ',inplace=True)
    df.eval('gamma0_SmearedEY = gamma0_smearedPT * ty0*sqrt(1/(tx0**2+ty0**2)) ',inplace=True)
  
        

    df.eval('deltaX=(gamma0_L0Calo_ECAL_xTrigger-gamma_L0Calo_ECAL_xTrigger)**2',inplace=True)
    df.eval('deltaY=(gamma0_L0Calo_ECAL_yTrigger-gamma_L0Calo_ECAL_yTrigger)**2',inplace=True)
    df.eval('sumX=(gamma0_L0Calo_ECAL_xTrigger**2+gamma0_L0Calo_ECAL_yTrigger**2)',inplace=True)
    df.eval('sumY=(gamma_L0Calo_ECAL_xTrigger**2+gamma_L0Calo_ECAL_yTrigger**2)',inplace=True)

    
    df.eval('deltaXoffline=(gamma0_CaloHypo_X-gamma_CaloHypo_X)**2',inplace=True)
    df.eval('deltaYoffline=(gamma0_CaloHypo_Y-gamma_CaloHypo_Y)**2',inplace=True)
    df.eval('sumXoffline=(gamma0_CaloHypo_X**2+gamma0_CaloHypo_Y**2)',inplace=True)
    df.eval('sumYoffline=(gamma_CaloHypo_X**2+gamma_CaloHypo_Y**2)',inplace=True)

    #df.eval('B_s0_Hlt1M0=',inplace=True)
    df.eval('B_s0_Hlt1ET0=sqrt((gamma_L0Calo_ECAL_TriggerEX+gamma0_L0Calo_ECAL_TriggerEX)**2+(gamma_L0Calo_ECAL_TriggerEY + gamma0_L0Calo_ECAL_TriggerEY)**2)',inplace=True)
    

    df.eval('B_s0_M_smearedMC = sqrt( 2*gamma_MCsmearedPT*gamma0_MCsmearedPT*(1-cos_theta)/(sin_theta1*sin_theta0) )',inplace=True)
    df.eval('B_s0_M_smearedMC = sqrt(gamma_MCsmearedPT*gamma0_MCsmearedPT*(deltaX+deltaY)/sqrt(sumX*sumY) )',inplace=True)
    df.eval('B_s0_M_smeared = sqrt( 2*gamma_smearedPT*gamma0_smearedPT*(1-cos_theta)/(sin_theta1*sin_theta0) )',inplace=True)
    df.eval('B_s0_M_smeared = sqrt(gamma_smearedPT*gamma0_smearedPT*(deltaXoffline+deltaYoffline)/sqrt(sumX*sumY) )',inplace=True)
    df.eval('B_s0_M_L0 = sqrt(gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(deltaX+deltaY)/sqrt(sumX*sumY))',inplace=True)
    df.eval('B_s0_M_L0_offline = sqrt(gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(deltaXoffline+deltaYoffline)/sqrt(sumXoffline*sumYoffline))',inplace=True)
    df.eval('B_s0_ET_L0 =sqrt((gamma_L0Calo_ECAL_TriggerEX+gamma0_L0Calo_ECAL_TriggerEX)**2+(gamma_L0Calo_ECAL_TriggerEY + gamma0_L0Calo_ECAL_TriggerEY)**2)',inplace=True)
    df.eval('B_s0_ET_L0_offline =sqrt((gamma_L0Calo_ECAL_TriggerEXoffline+gamma0_L0Calo_ECAL_TriggerEXoffline)**2+(gamma_L0Calo_ECAL_TriggerEYoffline + gamma0_L0Calo_ECAL_TriggerEYoffline)**2)',inplace=True)

    df.eval('B_s0_ET_smearedMC = sqrt((gamma_MCSmearedEX+gamma0_MCSmearedEX)**2 + (gamma_MCSmearedEY+gamma0_MCSmearedEY)**2)',inplace=True)
    df.eval('B_s0_ET_smeared = sqrt((gamma_SmearedEX+gamma0_SmearedEX)**2 + (gamma_SmearedEY+gamma0_SmearedEY)**2)',inplace=True)
    
    return df




def hlt1_checks_plots(df,id):
    fig,ax = plt.subplots(2,2,figsize=(16,9),dpi=80)
    n_plots = 4
    ax_flat =ax.flatten()
    alpha = 0.7
    ax_flat[0].set_title(r"$\gamma \gamma (p_T)$")
    ax_flat[0].hist(df['B_s0_ET_smearedMC'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[0].hist(df['B_s0_ET_smeared'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[0].hist(df['B_s0_ET_L0'],bins=50,density=True,alpha=alpha,histtype='step')
    #ax_flat[0].hist(df['B_s0_ET_L0_offline'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[0].legend(["Smeared MC","Smeared Data","HLT1",
                       #"HLT1 offline angles"
                       ],bbox_to_anchor=[0.05,0.95])
    
    ax_flat[1].set_title(r"$\gamma \gamma (M)$")
    ax_flat[1].hist(df['B_s0_M_smearedMC'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[1].hist(df['B_s0_M_smeared'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[1].hist(df['B_s0_M_L0'],bins=50,density=True,alpha=alpha,histtype='step')
    #ax_flat[1].hist(df['B_s0_M_L0_offline'],bins=50,density=True,alpha=alpha,histtype='step')

    ax_flat[2].set_title(r"$\gamma(p_T)$")
    ax_flat[2].hist(df['gamma_MCsmearedPT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[2].hist(df['gamma_smearedPT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[2].hist(df['gamma_L0Calo_ECAL_TriggerET'],bins=50,density=True,alpha=alpha,histtype='step')

    ax_flat[3].set_title(r"$\gamma_0(p_T)$")
    ax_flat[3].hist(df['gamma0_MCsmearedPT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[3].hist(df['gamma0_smearedPT'],bins=50,density=True,alpha=alpha,histtype='step')
    ax_flat[3].hist(df['gamma0_L0Calo_ECAL_TriggerET'],bins=50,density=True,alpha=alpha,histtype='step')
    #ax_flat[3].hist(df['gamma0_L0Calo_ECAL_TriggerET'],bins=50,density=True,alpha=alpha,histtype='step')

    plt.savefig('/home3/adrian.casais/hlt1_distribs{}.pdf'.format(id))
    #plt.show()

def calculate_efficiencies(df,dfeffs=pd.DataFrame(),dfxcheck=pd.DataFrame(),dftosmc=pd.DataFrame(),dfeffsdata=pd.DataFrame(),mass=5,match_eff = ufloat_eff(0.5,0.5)):

        aa = " & "
        oo = " or "
        pack = lambda x: "(" + x + ")"
        relative_err = lambda x,y,err_x,err_y: sqrt((err_x*y/x**2 )**2 + (err_y/x)**2)
        l0tos = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS)'
        hlt1tos = 'B_s0_Hlt1B2GammaGammaDecision_TOS'
        hlt1tos_hm = 'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS'
        hlt1tos2 = '( (B_s0_ET_L0_offline) > {0} & (B_s0_M_L0_offline > {1}) & (B_s0_M_L0_offline < {2}) & ( (gamma_L0Calo_ECAL_TriggerET + gamma0_L0Calo_ECAL_TriggerET)>{3}) & ( (gamma_L0Calo_ECAL_TriggerET>{4}) & (gamma0_L0Calo_ECAL_TriggerET>{4}) ))'
        hlt1tos3 = '((B_s0_ET_L0 > {0}) & (B_s0_M_L0 > {1}) & (B_s0_M_L0 < {2}) & ( (gamma_L0Calo_ECAL_TriggerET + gamma0_L0Calo_ECAL_TriggerET)>{3}) & (gamma_L0Calo_ECAL_TriggerET>{4}) & (gamma0_L0Calo_ECAL_TriggerET>{4}))'
        #hlt1tos3 = '( (B_s0_ET_L0) > {0} & (B_s0_Hlt1M0 > {1}) & (B_s0_Hlt1M0 < {2}) & ( (gamma_L0Calo_ECAL_TriggerET + gamma0_L0Calo_ECAL_TriggerET)>{3}) & ( (gamma_L0Calo_ECAL_TriggerET>{4}) & (gamma0_L0Calo_ECAL_TriggerET>{4}) ) )'
        hlt1tos_mcsmearing = '(B_s0_ET_smearedMC > {0} & B_s0_M_smearedMC > {1} & B_s0_M_smearedMC < {2} & (gamma_MCsmearedPT + gamma0_MCsmearedPT)>{3} & gamma_MCsmearedPT>{4} & gamma0_MCsmearedPT>{4} )'
        hlt1tos_datasmearing = '((B_s0_ET_smeared > {0}) & B_s0_M_smeared > {1} & B_s0_M_smeared < {2} & (gamma_smearedPT + gamma0_smearedPT)>{3} & (gamma_smearedPT>{4} & gamma0_smearedPT>{4}))'

        smear_cut = '(gamma_smearedPT>2500  & gamma0_smearedPT>2500)'
        smearMC_cut = '(gamma_MCsmearedPT>2500 & gamma0_MCsmearedPT>2500)'
        L0ET_cut =    '(gamma_L0Calo_ECAL_TriggerET > 2500 & gamma0_L0Calo_ECAL_TriggerET > 2500)'

        #smear_cut = 'true'
        #smearM C_cut = 'true'
        #L0ET_cut = 'true'
        soft = (2000,3500,6000,8000,3500)
        hard = (5000,6000,11000,11000,5000)
        l0_tos = len(df.query(l0tos))
        den = len(df.query(l0tos))

        tos_eff = ufloat_eff(len(df.query(hlt1tos.format(*soft) + aa + l0tos)),den)
        tos_eff_online_angles = ufloat_eff(len(df.query(hlt1tos3.format(*soft) + aa + l0tos)),den)
        tos_eff_offline_angles = ufloat_eff(len(df.query(hlt1tos2.format(*soft) + aa + l0tos)),den)
        tos_eff_mcsmearing = ufloat_eff(len(df.query(hlt1tos_mcsmearing.format(*soft) + aa + l0tos)),den)
        tos_eff_datasmearing = ufloat_eff(len(df.query(hlt1tos_datasmearing.format(*soft) + aa + l0tos)),den)
        
        tos_eff_hm = ufloat_eff(len(df.query(hlt1tos_hm + aa + l0tos) ) ,den)
        tos_eff_online_angles_hm = ufloat_eff(len(df.query(hlt1tos3.format(*hard) + aa + l0tos)),den)
        tos_eff_offline_angles_hm = ufloat_eff(len(df.query(hlt1tos2.format(*hard) + aa + l0tos)),den)
        tos_eff_mcsmearing_hm = ufloat_eff(len(df.query(hlt1tos_mcsmearing.format(*hard) + aa + l0tos)),den)
        tos_eff_datasmearing_hm = ufloat_eff(len(df.query(hlt1tos_datasmearing.format(*hard) + aa + l0tos)),den)

        tos_eff_or = ufloat_eff(len(df.query( pack(hlt1tos + oo +hlt1tos_hm ) + aa + l0tos) ) ,den)
        tos_eff_online_angles_or = ufloat_eff(len(df.query(pack(hlt1tos3.format(*hard)+oo+hlt1tos3.format(*soft)) + aa + l0tos)),den)
        tos_eff_offline_angles_or = ufloat_eff(len(df.query(pack(hlt1tos2.format(*hard)+oo+hlt1tos2.format(*soft)) + aa + l0tos)),den)
        tos_eff_mcsmearing_or = ufloat_eff(len(df.query(pack(hlt1tos_mcsmearing.format(*hard)+oo+hlt1tos_mcsmearing.format(*soft)) + aa + l0tos)),den)
        tos_eff_datasmearing_or = ufloat_eff(len(df.query(pack(hlt1tos_datasmearing.format(*hard)+oo+hlt1tos_datasmearing.format(*soft)) + aa + l0tos)),den)

        
        print('Old Hlt1 line: [3.5,6] GeV')
        print('TOS EFF: {0:.3f} +- {1:.3f}'.format(100.*tos_eff.n,100.*tos_eff.s))
        print('TOS EFF (L0 variables, online angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_online_angles.n,100.*tos_eff_online_angles.s))
        print('TOS EFF (L0 variables, offline angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_offline_angles.n,100.*tos_eff_offline_angles.s))
        print('MC smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_mcsmearing.n,100.*tos_eff_mcsmearing.s))
        print('Data smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_datasmearing.n,100.*tos_eff_datasmearing.s))
        print(" ")
        print('New Hlt1 Line: [6,11] GeV')
        print('TOS EFF: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_hm.n,100.*tos_eff_hm.s))
        print('TOS EFF (L0 variables, online angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_online_angles_hm.n,100.*tos_eff_online_angles_hm.s))
        print('TOS EFF (L0 variables, offline angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_offline_angles_hm.n,100.*tos_eff_offline_angles_hm.s))
        print('MC smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_mcsmearing_hm.n,100.*tos_eff_mcsmearing_hm.s))
        print('Data smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_datasmearing_hm.n,100.*tos_eff_datasmearing_hm.s))
        print("")
        print('Both lines: [3.5,11] GeV')
        print('TOS EFF: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_or.n,100.*tos_eff_or.s))
        print('TOS EFF (L0 variables, online angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_online_angles_or.n,100.*tos_eff_online_angles_or.s))
        print('TOS EFF (L0 variables, offline angles): {0:.3f} +- {1:.3f}'.format(100.*tos_eff_offline_angles_or.n,100.*tos_eff_offline_angles_or.s))
        print('MC smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_mcsmearing_or.n,100.*tos_eff_mcsmearing_or.s))
        print('Data smearing: {0:.3f} +- {1:.3f}'.format(100.*tos_eff_datasmearing_or.n,100.*tos_eff_datasmearing_or.s))
        rel = (tos_eff_mcsmearing_or - tos_eff_datasmearing_or)/tos_eff_mcsmearing_or
        print("Relative difference Data, MC: {0:.3f} % +- {1:.3f}".format(100.*rel.n,100.*rel.s))
        relmc = (tos_eff_mcsmearing_or - tos_eff_or)/tos_eff_or
        print("Relative difference MC,MCsmearing: {0:.3f} % +- {1:.3f}".format(100.*relmc.n,100*relmc.s))
        
        effarray = np.array((  mass,
                               ufloat_to_str(match_eff),
                               ufloat_to_str(tos_eff_or),
                               ufloat_to_str(tos_eff_online_angles_or),
                               ufloat_to_str(tos_eff_offline_angles_or),
                               
                               ufloat_to_str(tos_eff_mcsmearing_or),
                               ufloat_to_str(tos_eff_datasmearing_or),
                               ufloat_to_str(rel)
                               ))
        xcheckarray =np.array((mass,ufloat_to_str(tos_eff_or),
                               ufloat_to_str(tos_eff_online_angles_or)))
        tosmcarray =np.array((mass,
                               ufloat_to_str(tos_eff_online_angles_or),
                               ufloat_to_str(tos_eff_mcsmearing_or)))
        uncer = abs(tos_eff_datasmearing_or.n - tos_eff_mcsmearing_or.n)
        tos_eff_datasmearing_or_full = ufloat(0,uncer) + tos_eff_datasmearing_or
        effsdata = np.array((mass,ufloat_to_str(tos_eff_datasmearing_or_full) ))
        if dfeffs.shape != (0,0):
            dfeffs=dfeffs.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeffs)),ignore_index=True)
            dfxcheck=dfxcheck.append(pd.DataFrame(xcheckarray.reshape(1,-1),columns=list(dfxcheck)),ignore_index=True)
            dftosmc=dftosmc.append(pd.DataFrame(tosmcarray.reshape(1,-1),columns=list(dftosmc)),ignore_index=True)
            dfeffsdata = dfeffsdata.append(pd.DataFrame(effsdata.reshape(1,-1),columns=list(dfeffsdata)),ignore_index=True)
        return [effsdata[1],tos_eff_datasmearing_or_full,[100.*rel.n,100.*rel.s],dfeffs,dfxcheck,dftosmc,dfeffsdata]
        

############
def compare_2d(df):
    xlimits = ylimits = [1000,6000]
    H0,xedges0,yedges0    =np.histogram2d(df['gamma0_L0Calo_ECAL_TriggerET'],df['gamma_L0Calo_ECAL_TriggerET'],bins=50,density=True,range=(xlimits,ylimits))
    H1,xedges1,yedges1 = np.histogram2d(df['gamma0_MCsmearedPT'],df['gamma_MCsmearedPT'],bins=50,density=True,range=(xlimits,ylimits))
    H2,xedges2,yedges2 = np.histogram2d(df['gamma0_smearedPT'],df['gamma_smearedPT'],bins=50,density=True,range=(xlimits,ylimits))

    X,Y = np.meshgrid(xedges0,yedges0)
    fig, (ax0,ax1,ax2) = plt.subplots(1,3)
    ax0.title.set_text('Trigger ET')
    ax0.set_xlabel('gamma0 Trigger ET (MeV)')
    ax0.set_ylabel('gamma1 Trigger ET (MeV)')
    ax1.title.set_text('Smeared ET')
    ax1.set_xlabel('gamma0 Smeared ET (MeV)')
    ax1.set_ylabel('gamma1 Smeared ET (MeV)')
    ax2.title.set_text('Data Smeared ET')
    ax2.set_xlabel('gamma0 Data Smeared ET (MeV)')
    ax2.set_ylabel('gamma1 Data Smeared ET (MeV)')
    ax0.pcolormesh(X,Y,H0)
    ax1.pcolormesh(X,Y,H1)
    ax2.pcolormesh(X,Y,H2)
    plt.show()
    
def compare_1d(df):
    xlimits = ylimits = [1000,6120]
    fig, (ax0,ax1) = plt.subplots(1,2)
    ax0.title.set_text('Gamma0')
    ax0.set_xlabel('L0ET/SmearedET (MeV)')
    ax0.set_ylabel('No. counts')
    ax1.title.set_text('Gamma1')
    ax1.set_xlabel('gamma0 Smeared ET (MeV)')
    ax1.set_xlabel('L0ET/SmearedET (MeV)')
    ax1.set_ylabel('No. counts')
    ax0.hist(df['gamma0_MCsmearedPT'],bins=50,density=True,range=xlimits,alpha=0.6)
    ax0.hist(df['gamma0_smearedPT'],bins=50,density=True,range=xlimits,alpha=0.6)
    ax0.hist(df['gamma0_L0Calo_ECAL_TriggerET'],bins=50,density=True,range=xlimits,alpha=0.6)
    ax0.legend(["MC smeared","Data smeared","Original L0ET"],bbox_to_anchor=[0.75,1.15])
    ax1.hist(df['gamma_MCsmearedPT'],bins=50,density=True,range=xlimits,alpha=0.6)
    ax1.hist(df['gamma_smearedPT'],bins=50,density=True,range=xlimits,alpha=0.6)
    ax1.hist(df['gamma_L0Calo_ECAL_TriggerET'],bins=50,density=True,range=xlimits,alpha=0.6)
    
    plt.show()
    




def plot_et(df,xplots=2,yplots=7,eta=(1.7, 6),gamma='gamma',smear='Data'):

    fig,ax = plt.subplots(xplots,yplots,figsize=(21,10),dpi=80)
    n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    xlimits = 2000,6500
    nbins = 40
    prefix = ''
    if smear=='MC': prefix='MC'
    for pT,i in zip(pTs,range(n_plots)):
        ax_flat[i].set_title('PT in ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))

        df_  = df.query('{0}_PT > {1} & {0}_PT < {2}'.format(gamma,pT[0],pT[1]))
        ax_flat[i].hist(df_['{1}_{0}smearedPT'.format(prefix,gamma)],bins=nbins,alpha=0.6,
                        range=xlimits
                       )
        #ax_flat[i].hist(df_['{0}_smearedPT'.format(gamma)],bins=40,alpha=0.6)
        ax_flat[i].hist(df_['{0}_L0Calo_ECAL_TriggerET'.format(gamma)],bins=nbins,alpha=0.6,
                        range=xlimits
                        )
                
        
    ax_flat[0].legend(('Smeared ET',
                       #'Data smearing',
                       'Original L0ET'
                       ))
    plt.savefig("/home3/adrian.casais/ALPsL0ET-smear{}.pdf".format(smear))
    print("Saving {} plot file".format(smear))
    #plt.show()


def compare_mass(df,hlt1cuts = False):
    if hlt1cuts:
        df_ = df.query('B_s0_Hlt1B2GammaGammaDecision_TOS & (B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS)')
    else:
        df_ = df
    plt.hist(df_.query('B_s0_M_smearedMC<10000')['B_s0_M_smearedMC'],bins=50,density=True,alpha = 0.4,label="MC smeared")
    #plt.hist(df_.query('B_s0_M_smeared<10000')['B_s0_M_smeared'],bins=50,density=True,alpha = 0.4,label = "Data smeared")
    #plt.hist(df_.query('B_s0_M_L0_offline < 10000')['B_s0_M_L0'],bins=50,density=True,alpha=0.4,label="L0Mass offline positions")
    plt.hist(df_.query('B_s0_M_L0< 10000')['B_s0_M_L0'],bins=50,density=True,alpha = 0.4,label="L0Mass")
    plt.legend(bbox_to_anchor=[0.75,1.15])
    
    plt.show()

def print_hlt1column(masses=sim10_masses_map):
    hlt1_dic = {"HLT1":[]}
    root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    effs_ufloat = []
    for i in masses:
        if i=='gg':
            cut = f'{trigger_cuts["L0"]}  & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            df = get_ALPsdf_sim10b(myfile,extracut=cut,background=False,bs=True,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH','gamma0_PP_IsPhoton','gamma_PP_IsPhoton','gamma_PP_CaloNeutralID'])
        else:
            cut = f'{trigger_cuts["L0"]} & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
            df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv.root',False,background=False,extracut=cut)
        df = smear_pt(df,offset=0)
        df = vetoNonMatched(df)
        df = build_mass(df)
        match_eff = howmanyL0(df)
        output = calculate_efficiencies(df,mass = masses[i],match_eff=match_eff)
        effs_ufloat.append(output[1])
        hlt1_dic["HLT1"].append(output[0])
    return hlt1_dic["HLT1"],effs_ufloat

if __name__ == '__main__':
    
    masses = sim10_masses_map
    plot = []
    plot0 = []
    bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'
    
    dfeffs = pd.DataFrame({ 'Mass':[],
            'Matching Eff':[],
            '\efftos':[],
            'TOS EFF (L0 variables, online angles)':[],
            'TOS EFF (L0 variables, offline angles)':[],
            '\effsmearmc':[],
            '\effsmeardata': [],
            'Relative difference Data, MC':[]})
    dfxcheck = pd.DataFrame({ 'Mass':[],
                  r'\efftos':[],
                  r'\effptlzero':[]})
    dftosmc = pd.DataFrame({ 'Mass':[],
                             r'\effptlzero':[],
                             r'\effsmearmc':[],
                             })
    dfeffsdata = pd.DataFrame({ 'Mass':[],
                             r'\effsmeardata':[]
                             })


    for i in masses:
    #for i in [45]:
        print(10*"**")
        print(masses[i])
        bs = False
        if i=='bs':
            bs=True
        #df = get_df(i,bs)
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        
        df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv.root',bs,background=False,extracut='(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) & (gamma_PT > 3000 & gamma0_PT > 3000)')
        #i=40
        match_eff = howmanyL0(df)
        offset=0
        df = smear_pt(df,offset=offset)
        df = vetoNonMatched(df)
        # plot_et(df,smear='MC')
        # plot_et(df,smear='Data')
        df = build_mass(df)
        #compare_mass(df,hlt1cuts=True)
        _,_,eff,dfeffs,dfxcheck,dftosmc,dfeffsdata = calculate_efficiencies(df,dfeffs,dfxcheck,dftosmc,dfeffsdata,masses[i],match_eff)
        
        # if masses[i] != masses['bs']:
        #     eff.append(int(masses[i].split()[0]))
        #     plot.append(eff)
        eff.append(masses[i])
        plot.append(eff)
        hlt1_checks_plots(df,i)
        #plt.show()
        
        
        print(10*"**")
    print(dfeffs.to_latex(index=False,escape=False))
    print(dfxcheck.to_latex(index=False,escape=False))
    print(dftosmc.to_latex(index=False,escape=False))
    print(dfeffsdata.to_latex(index=False,escape=False))
    fig, ax = plt.subplots()
    plot = np.array(plot)
    plot_swapped = np.swapaxes(plot,0,1)
    ax.set_title('HLT1 systematic between MC and Data with EtaMuMuGamma study')
    ax.errorbar(plot_swapped[2],plot_swapped[0],yerr=plot_swapped[1],fmt='s',color='xkcd:red',
                )
    pol=np.polyfit(plot_swapped[2],plot_swapped[0],w=1/plot_swapped[1],deg=1)
    x_pol=np.linspace(4,17,100)
    y_pol=x_pol*pol[0]+pol[1]

    ax.plot(x_pol,y_pol,label="y = {0:.2f}x + {1:.2f}".format(pol[0],pol[1]),color='xkcd:red')
    ax.set_ylim(0,18)
    ax.set_ylabel('Relative MC/Data difference [%]')
    ax.set_xlabel('Mass [GeV]')
    ax.legend()
    plt.savefig("/home3/adrian.casais/linearfithlt1sys.pdf")
    plt.show()
