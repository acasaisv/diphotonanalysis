from ROOT import *
import uproot3 as uproot
import pandas
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from kinematic_bins import pTs,etas
import pickle

df = pandas.read_hdf('/scratch47/adrian.casais/ntuples/signal//49100040_MagDown_L0ETstudies.h5',key='ALPs')
#dfMCeta =pandas.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgDeltaET.h5',key='mc')
dfMCeta = pandas.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')

dfMCeta.query('gamma_L0Calo_ECAL_TriggerET > 0',inplace=True)
n_bins = 9 #has to be a square number
pts = np.linspace(2000,10000,n_bins + 1)
fig,ax = plt.subplots(3,3)
ax_flat =ax.flatten()
fig.suptitle("Blue: ALPs L0ET, Orange: smeared ET")
for i in range(n_bins):
    low = pts[i]
    high = pts[i+1]
    ax_flat[i].set_title('PT in ({0:.2f},{1:.2f})'.format(low,high))
    ax_flat[i].hist(df.query('gamma_PT > %i & gamma_PT < %i' % (low,high))["gamma_L0Calo_ECAL_TriggerET"],bins=50,alpha=0.6,density=1)
    mydf = dfMCeta.query('gamma_PT > %i & gamma_PT < %i' % (low,high))
    ax_flat[i].hist(mydf["gamma_L0Calo_ECAL_TriggerET"],bins=50,alpha=0.6,density=1,weights=mydf['sweights']

                                                                                               )




plt.show()


