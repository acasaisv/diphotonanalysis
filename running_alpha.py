import rundec
import numpy as np
import matplotlib.pyplot as plt
from plot_helpers import *

crd = rundec.CRunDec()
a = crd.AlphasExact(0.1185, 91.1876, 4.18, 5, 3)
q = []
alpha_s = []

for energy in np.linspace(0.1,1,50):
    q.append(energy)
    alpha_s.append(crd.AlphasExact(0.1185, 91.1876, energy, 6, 3))

fig,ax = plt.subplots(1,1)
ax.plot(q,alpha_s)
ax.set_xlabel('Q [GeV]')
ax.set_ylabel(r'$\alpha_s(Q)$')
plt.tight_layout()
plt.savefig('alpha_s-big.pdf')
