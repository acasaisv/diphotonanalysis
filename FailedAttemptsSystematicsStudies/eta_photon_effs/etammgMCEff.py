from __future__ import print_function
from ROOT import *
from math import sqrt
#gROOT.ProcessLine(".x ../../lhcbStyle.C")
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

# basecut = '(Bs_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.3 & gamma_PT > 2000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50)'

lowpT=3000
highpT = 4000
lowETA=3
highETA=4
import sys
if len(sys.argv)>4:
    lowpT = sys.argv[1]
    highpT = sys.argv[2]
    lowETA = sys.argv[3]
    highETA = sys.argv[4]



basecut = '(gamma_P>6000 && gamma_CL > 0.3 && eta_PT>2000 && eta_TRUEID == 221 && gamma_MC_MOTHER_ID==221)'

home = '/scratch47/adrian.casais/ntuples/turcal'
f = TFile(home + '/etammgMC2018.root')
#f = TFile(home + '/etammgMC2018withSPDs.root')
t=f.Get('DecayTree/DecayTree')
t.SetAlias('gamma_ETA','0.5*log( (gamma_P+gamma_PZ)/(gamma_P-gamma_PZ) )')
TIS = '(gamma_L0Global_TIS && gamma_Hlt1Phys_TIS && gamma_Hlt2Phys_TIS)'
TIS = 'gamma_Hlt1Phys_TIS && gamma_Hlt2Phys_TIS'
#TIS = '(muplus_L0MuonDecision_Dec || muminus_L0MuonDecision_Dec)'
TIS = '(eta_L0MuonDecision_TOS | eta_L0DiMuonDecision_TOS)'
#######################
TIS = '(gamma_L0Global_TIS && gamma_Hlt1Phys_TIS)'
TIS += '&& eta_L0DiMuonDecision_Dec'
#######################
TIS = '1'
aa = '&&'
bin = '(gamma_PT > {0} && gamma_PT < {1})'.format(lowpT,highpT)
bin += '& (gamma_ETA > {0} && gamma_ETA < {1})'.format(lowETA,highETA)
#num = t.GetEntries('{0} && {1} && {2} && Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'.format(basecut,bin,TIS))
num = t.GetEntries('{0} && {1} && {2} && (gamma_L0ElectronDecision_TOS || gamma_L0PhotonDecision_TOS)'.format(basecut,bin,TIS))
#num = t.GetEntries('{0} && {1} && {2} && (gamma_L0ElectronDecision_Dec || gamma_L0PhotonDecision_Dec)'.format(basecut,bin,TIS))
den = t.GetEntries('{0} && {1} && {2}'.format(basecut,bin,TIS))

print( 'Eff: {0} +- {1} %'.format(100.*num/den,100.*get_error(1.*num/den,den)) )

with open('/home3/adrian.casais/plots/etammg/MCEff/MCEffpt{0},{1}eta{2},{3}.txt'.format(lowpT,highpT,lowETA,highETA),'w') as handle:
          print('Eff: {0} +- {1} %'.format(100.*num/den,100.*get_error(1.*num/den,den)),file=handle)

draw = 0
if draw:
    t.Draw('gamma_PT','gamma_PT<10000 && eta_TRUEID==221')
    


    
