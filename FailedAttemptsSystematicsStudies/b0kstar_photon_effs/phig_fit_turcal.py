import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot
import numpy as np
import pandas


efficiency = True
lowpT = 000
highpT = 80000000
lowETA=1
highETA=6
num = True
hdf = True

import sys
if len(sys.argv)>4:
    lowpT =float(sys.argv[1])
    highpT = float(sys.argv[2])
    lowETA = float(sys.argv[3])
    highETA = float(sys.argv[4])
    num = bool(int(sys.argv[5]))


#B mass window
m1g,m2g = 4900,5600
#Phi mass window
m1k,m2k = 1006,1034

BMassRange = zfit.Space('B_MM',(m1g,m2g))
obs = BMassRange

root = '/scratch47/adrian.casais/ntuples/turcal'
if not hdf:
    f_s = uproot.open(root + '/turcal.root')
    t_s = f_s['Bd2kstgamma_Tuple/DecayTree']
    keys = [ i.decode('utf-8') for i in t_s.keys() ]
    variables = []

    variables += [
            'B_M',
            'B_PT',
            'gamma_CL',
            'gamma_P',
            'gamma_PZ',
            'gamma_PT',
            'B_L0ElectronDecision_TOS',
            'B_L0PhotonDecision_TOS',
            'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
            'B_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS',
            'gamma_L0Global_TIS',
            'gamma_Hlt1Phys_TIS',
            'gamma_Hlt2Phys_TIS',
            'gamma_L0ElectronDecision_TOS',
            'gamma_L0PhotonDecision_TOS',              
            ]
        
    df = t_s.pandas.df(branches=variables)
    df.dropna()
    
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    
    
    
    df.to_hdf(root+'/BKstarGammaData.h5',key='df',mode='w')

else:
    df =pandas.read_hdf(root+'/BKstarGammaData.h5',key='df')


L0 = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS)'
L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = 'B_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
if efficiency:
    trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS"
    trigger +=' & (gamma_PT > {0} & gamma_PT < {1})'.format(lowpT,highpT)
    trigger +=' & (gamma_ETA > {0} & gamma_ETA < {1})'.format(lowETA,highETA)
    #trigger +=' & (gamma_PT > 5000 & gamma_PT < 8000)'
    #trigger += ' & (gamma_PT > 8000)'
    if num:
       #trigger += " & B_Hlt1B2PhiGamma_LTUNBDecision_TOS"
       trigger += '& (gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
    

#df = df.query(mc)
df = df.query(trigger)
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))

# df = df.query('(B_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.25 & gamma_PT > 1000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50)')

df = df.query('(B_PT > 2000 & gamma_CL > 0.3 & gamma_PT > 2000 & gamma_P > 6000 )')



#df = df.query('gamma_PT > 2000 & gamma_PT < 6000')
#df = df.query('gamma_PT > 6000 & gamma_PT < 8000')
#B_s0



#Signal: Double Crystall Ball
name_prefix='B_'
BParameters = {}
BParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.01)
BParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',95.,10.,150.)
#BParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',5366.9,floating=False)
BParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BParameters['scale_m'] ,BParameters['m_PDG']])
BParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',2.455,
                                     #0.01,
                                     #5,
                                     floating=False)
BParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',12.39,
                                     #1,
                                     #10,
                                     floating=False)

BParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.72,
                                     #-5,
                                     #-0.01,
                                     floating=False)
BParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',27.39,
                                     #1,
                                     #10,
                                     floating=False)
BParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1)




BCBu = zfit.pdf.CrystalBall(mu =BParameters['m'],
                          sigma=BParameters['sigma'],
                          alpha=BParameters['a_u'],
                          n=BParameters['n_u'],
                          obs=BMassRange)
BCBd = zfit.pdf.CrystalBall(mu=BParameters['m'],
                          sigma=BParameters['sigma'],
                          alpha=BParameters['a_d'],
                          n=BParameters['n_d'],
                          obs=BMassRange)

fcb_B = zfit.Parameter(name_prefix+'fcb',0.59,0,1,floating=False)
BCB = zfit.pdf.SumPDF(pdfs=[BCBu,BCBd],fracs=fcb_B)
BCBExtended = BCB.create_extended(BParameters['nSig'])

#Background: exponential
lambda_B = zfit.Parameter(name_prefix+'lambda',-3e-3,-1,1)
BBkgComb = zfit.pdf.Exponential(lambda_B,BMassRange)
BBkgCombExtended = BBkgComb.create_extended(BParameters['nBkgComb'])



#PROD PDFs
nSigSig= zfit.Parameter('nSigSig',10000,0,len(df))
signal = BCB
signalExtended=signal.create_extended(nSigSig)


nBkgSig= zfit.Parameter('nBkgSig',10000,0,len(df))
bkgsignal = BBkgComb
bkgSignalExtended=bkgsignal.create_extended(nBkgSig)



model = zfit.pdf.SumPDF(pdfs = [signalExtended,
                                bkgSignalExtended,
                                ])



####               
df_masses = df['B_M']
# df_masses = df_masses.dropna()
data = zfit.Data.from_pandas(df_masses,obs=obs)
data_B = zfit.Data.from_numpy(array=df['B_M'].values,obs=BMassRange)
#CREATE LOSS FUNCTION

#Extended
#nll = zfit.loss.ExtendedUnbinnedNLL(model=modelB,
#                                    data=data_B)
nll = zfit.loss.ExtendedUnbinnedNLL(model=model,data=data)

#NonExtended
#nll = zfit.loss.UnbinnedNLL(model=[modelB,modelPhi],data=[data_B,data_Phi])

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
#result.errors()
result.hesse()
print(result.params)
#PLOT
plot= True
#     plt.show()

if plot:
    BParamsValues = {}
    for key in BParameters:
        BParamsValues[key] = zfit.run(BParameters[key])

    import matplotlib.pyplot as plt
    nbins=40
    nentries=len(df)
    xB = np.linspace(m1g,m2g,1000)
    xPhi=np.linspace(m1k,m2k,1000)
    countsB, bin_edgesB = np.histogram(df['B_M'], nbins, range=(m1g, m2g))
    
    bin_centresB = (bin_edgesB[:-1] + bin_edgesB[1:])/2.
    
    errB = np.sqrt(countsB)
    yB =(m2g-m1g)/nbins*zfit.run(
        (nSigSig )*BCB.pdf(xB)+
        (nBkgSig )*BBkgComb.pdf(xB)
        )
    yBSig = (m2g-m1g)/nbins*zfit.run(
        (nSigSig)*BCB.pdf(xB)
        )
    yBBkg = (m2g-m1g)/nbins*zfit.run(
        (nBkgSig)*BBkgComb.pdf(xB)
        )
    
    
    fig,ax = plt.subplots(1,figsize=(8,6))
    #ax[0].set_ylim([0,250e3])
    ax.errorbar(bin_centresB, countsB, yerr=errB, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax.plot(xB,yB,'-',linewidth=2,color='blue')
    ax.plot(xB,yBSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax.plot(xB,yBBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    fig.text(0.05,0.95,'Sig. yield = {:.2f}'.format(float(nSigSig)))
    ax.legend()
    #plt.show()

    #plt.show()
    _den ='den'
    if num: _den ='num'
    
    fig.savefig('/home3/adrian.casais/plots/BKstarGamma/pt{0},{1}eta{2},{3}L0{4}.pdf'.format(lowpT,highpT,lowETA,highETA,_den))
    with open('/home3/adrian.casais/plots/BKstarGamma/pt{0},{1}eta{2},{3}L0{4}_params.txt'.format(lowpT,highpT,lowETA,highETA,_den),'w') as handle:
        print(result.params,file=handle)






#TOS: 1713 +- 46
#!TOS: 2624 +- 450
s = 525
b= 641
s_err = 1.3
b_err =450
eff = s/(b)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
print('HLT1 eff (MC): 0.4944 +- 0.0012')
