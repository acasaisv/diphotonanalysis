import sys
import subprocess
from math import sqrt
from kinematic_bins import pTs,etas
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

nums = {}
dens = {}
err_nums = {}
err_dens = {}
effs = {}
err_effs = {}
MCeffs = {}
err_MCeffs = {}
for pT in pTs:
    for eta in etas:
        key = '{0} {1} {2} {3}'.format(pT[0],pT[1],eta[0],eta[1])
        effs[key]=1        
        for num in 1,0:
            den = 'den'
            if num:
                den='num'
                with open('/home3/adrian.casais/plots/BsPhiGamma/MCEff/MCEffpt{0},{1}eta{2},{3}hlt1.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                    lines = handle.readlines()
                MCeffs[key] = float(lines[0].rsplit()[1])/100.
                err_MCeffs[key] = float(lines[0].rsplit()[3])/100.
            with open('/home3/adrian.casais/plots/BsPhiGamma/pt{0}.0,{1}.0eta{2}.0,{3}.0L0{4}_paramshlt1.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                lines = handle.readlines()
            value = float(lines[2].rsplit()[1])
            err= float(lines[2].rsplit()[3])
            if num:
                nums[key]=value
                err_nums[key]=err
            else:
                dens[key]=value
                err_dens[key]=err
                effs[key]=nums[key]/dens[key]
                err_effs[key] = get_error(effs[key],value/1.)
            
                      
            
                      
for key in effs.keys():
    
    split = key.rsplit()
    split += [MCeffs[key],err_MCeffs[key],effs[key],err_effs[key]]
    print('pT in [{0},{1}]; eta in [{2},{3}]. MC: {4:.2f} +- {5:.2f}, data {6:.2f}+-{7:.2f}'.format(*split))
        
        
