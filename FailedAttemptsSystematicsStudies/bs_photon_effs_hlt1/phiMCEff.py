from ROOT import *
from math import sqrt
gROOT.ProcessLine(".x ../../lhcbStyle.C")
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

lowpT=8000
highpT = 9000
lowETA=2
highETA=5
import sys
if len(sys.argv)>3:
    lowpT = sys.argv[1]
    highpT = sys.argv[2]
    lowETA = sys.argv[3]
    highETA = sys.argv[4]


basecut = 'Bs_PT > 2000   && (kminus_PIDK > 5 && kplus_PIDK >5) & kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV > 55 & gamma_CL > 0.3 && gamma_PT > 2000 & gamma_P > 6000 && abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50 && phi_PT > 1800 && (phi_PT + gamma_PT) > 4000'
basecut += '&& abs(Bs_TRUEID)==531 && abs(phi_MC_MOTHER_ID) == 531 && abs(gamma_MC_MOTHER_ID) == 531 && phi_TRUEID==333 && gamma_TRUEID==22 '


basecut = '({})'.format(basecut)
home = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f = TFile(home + '/phiGammaMC.root')
t=f.Get('DTTBs2phig/DecayTree')

TIS = '(gamma_L0Global_TIS && gamma_Hlt1Phys_TIS && gamma_Hlt2Phys_TIS)'
TIS = '1'
aa = '&&'
bin = '(gamma_PT > {0} && gamma_PT < {1})'.format(lowpT,highpT)
bin += '& (gamma_ETA > {0} && gamma_ETA < {1})'.format(lowETA,highETA)

#bin = '(gamma_PT > 5000 && gamma_PT < 8000)'
#bin = '(gamma_PT > 8000)'
#Bin = '1'
#bin = 'gamma_PT > 4000'
num = t.GetEntries('{0} && {1} && {2} && Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'.format(basecut,bin,TIS))
#num = t.GetEntries('{0} && {1} && {2} && (gamma_L0ElectronDecision_TOS || gamma_L0PhotonDecision_TOS)'.format(basecut,bin,TIS))
den = t.GetEntries('{0} && {1} && {2}'.format(basecut,bin,TIS))

print('Eff: {0} +- {1} %'.format(100.*num/den,100.*get_error(1.*num/den,den)))

with open('/home3/adrian.casais/plots/BsPhiGamma/MCEff/MCEffpt{0},{1}eta{2},{3}hlt1.txt'.format(lowpT,highpT,lowETA,highETA),'w') as handle:
          print('Eff: {0} +- {1} %'.format(100.*num/den,100.*get_error(1.*num/den,den)),file=handle)

draw = 0
if draw:
    t.Draw('gamma_PT','gamma_PT<20000 && abs(Bs_TRUEID)==531')
