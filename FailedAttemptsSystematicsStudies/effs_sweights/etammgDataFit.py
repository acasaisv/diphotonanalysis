import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import pandas
from ROOT import TFile,TTree
import sys

root = '/scratch47/adrian.casais/ntuples/turcal'
#Bs mass window
m1eta,m2eta = 465,630
hdf = 0
efficiency=0
if not hdf:
    
    f_s = uproot.open(root + '/turcal.root')
    t_s = f_s['Eta2MuMuGamma_Tuple/DecayTree']
    f = TFile(root+'/turcal.root')
    t = f.Get('Eta2MuMuGamma_Tuple/DecayTree')

    variables = ['eta_M',
                 'gamma_L0PhotonDecision_TOS',
                 'gamma_L0ElectronDecision_TOS',
                 'gamma_L0Global_TIS',
                 'gamma_Hlt1Phys_TIS',
                 'gamma_Hlt2Phys_TIS',
                 'mu1_L0DiMuonDecision_Dec',
                 'mu2_L0MuonDecision_Dec',
                 'gamma_PT',
                 'gamma_ETA',
                 'gamma_CL',
                 'gamma_P',
                 'gamma_PZ',
                 'gamma_L0Calo_ECAL_realET',
                 'gamma_L0Calo_ECAL_TriggerET',
                 'eta_PT'
                 ]
    df = t_s.pandas.df(branches=variables)
    #df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )

    df.dropna()
    df.to_hdf(root+'/etammgTurcalData.h5',key='df',mode='w')
else:
    df =pandas.read_hdf(root+'/etammgTurcalData.h5',key='df')


df = df.query('eta_M > {0} & eta_M < {1}'.format(m1eta,m2eta))
df = df.query('gamma_CL>0.3 & gamma_P> 6000 & eta_PT>2000')
if efficiency:
    trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS"
    #trigger = '(mu1_L0MuonDecision_Dec | mu1_L0DiMuonDecision_Dec)'
    ####################
    trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS"
    trigger += '& (mu1_L0MuonDecision_Dec)'
    ####################
    trigger = '(eta_L0DiMuonDecision_TOS | eta_L0MuonDecision_TOS)'
    trigger = 'mu2_L0MuonDecision_Dec'
    trigger +=' & (gamma_PT > {0} & gamma_PT < {1})'.format(lowpT,highpT)
    trigger +=' & (gamma_ETA > {0} & gamma_ETA < {1})'.format(lowETA,highETA)
    #trigger +=' & (gamma_PT > 5000 & gamma_PT < 8000)'
    #trigger += ' & (gamma_PT > 8000)'
    if num:       
       trigger += ' & (gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
       #trigger += ' & (gamma_L0ElectronDecision_Dec | gamma_L0PhotonDecision_Dec)'

    df = df.query(trigger)
#eta0





Eta1MassRange = zfit.Space('eta_M',(m1eta,m2eta))



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.05)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',5.,1.,80.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',
                                     0.7896,
                                     0.01,
                                     5,
                                     floating=False
                                     )
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',
                                     91.83,
                                     0.1,
                                     150,
                                     floating=False
                                     )

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',
                                     -0.69,
                                     -5,
                                     -0.01,
                                     floating=False
                                     )
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',
                                     122.1,
                                     0.5,
                                     150,
                                     floating=False
                                     )
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.1,0.001,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=Eta1MassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=Eta1MassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.3907,0,1,
                        floating=False)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,0,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
BsBkgComb = zfit.pdf.Legendre(Eta1MassRange,[firstOrder,
                                             #secondOrder,
                                             #thirdOrder,
                                             #fourthOrder
                                             ])
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])



#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,
                                BsBkgCombExtended
                                ])


data_Bs = zfit.Data.from_numpy(array=df['eta_M'].values,obs=Eta1MassRange)
#CREATE LOSS FUNCTION

#Extended
nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,
                                    data=data_Bs)

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)

from hepstats.splot import compute_sweights
sweights = compute_sweights(modelBs, data_Bs)
signal_sweights = sweights[list(sweights.keys())[0]]
df['sweights'] = signal_sweights
print(sweights)
df.to_hdf(root+'/etammgTurcalData.h5',key='df',mode='w')

#PLOT
plot= False
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=100
    nentries=len(df)
    xBs = np.linspace(m1eta,m2eta,1000)
    countsBs, bin_edgesBs = np.histogram(df['eta_M'], nbins, range=(m1eta, m2eta))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2eta-m1eta)/nbins*zfit.run(
        BsParameters['nSig']*BsCBExtended.pdf(xBs)+
        BsParameters['nBkgComb']*BsBkgCombExtended.pdf(xBs)
        )
    yBsSig = (m2eta-m1eta)/nbins*zfit.run(BsParameters['nSig']*BsCBExtended.pdf(xBs))
    yBsBkg = (m2eta-m1eta)/nbins*zfit.run(BsParameters['nBkgComb']*BsBkgCombExtended.pdf(xBs))
    
    fig,ax = plt.subplots(1)
    fig.text( 0.05,0.95,'Sig. yield = {0:.2f}'.format(float(BsParameters['nSig'])) )
    #ax.set_ylim([0,250e3])
    ax.errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax.set_yscale('log')
    ax.plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax.plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Bs CB')
    ax.plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax.legend()
    
    plt.show()
    _den ='den'
    if num: _den ='num'    


