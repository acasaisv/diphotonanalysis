import sys
import subprocess
import numpy as np
from math import sqrt
from kinematic_bins import pTs,etas
from ROOT import TEfficiency
import pandas 
def get_error0(eff,n):
    if n<0:
        return 0
    if n == 0:
        return 0
    if eff==0 or eff==1 or eff<0:
        return sqrt(1/n)
    else:
        return sqrt(eff*(1.-eff)/n)

def get_error(num,den,err_num,err_den):
    f = num/den
    if f==0 or f==1 or f<0:
        return sqrt(1/den)
    error = f * sqrt( (err_num/num)**2 + (err_den/den**2)*(err_den-2*err_num ) )
    return error


df =pandas.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
dfMC =pandas.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')

nums = {}
dens = {}
err_nums = {}
err_dens = {}
effs = {}
err_effs = {}
covs = {}
MCeffs = {}
err_MCeffs = {}
MCeffs2 = {}
err_MCeffs2 = {}
for pT in pTs:
    for eta in etas:
        key = '{0} {1} {2} {3}'.format(pT[0],pT[1],eta[0],eta[1])
        effs[key]=1        
                
        kin_cut = ' (gamma_PT > {0} & gamma_PT < {1})'.format(pT[0],pT[1])
        kin_cut +=' & (gamma_ETA > {0} & gamma_ETA < {1})'.format(eta[0],eta[1])
        L0cut = ' (gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
        TIS = ' mu2_L0MuonDecision_Dec'
        
        den = df.query( "({0}) & ({1})".format(kin_cut,TIS) )['sweights'].sum()    
        num = df.query( "({0}) & ({1}) & ({2})".format(kin_cut,TIS,L0cut) )['sweights'].sum()

        denMC = dfMC.query( "({0}) & ({1})".format(kin_cut,TIS) )['sweights'].sum()    
        numMC = dfMC.query( "({0}) & ({1}) & ({2})".format(kin_cut,TIS,L0cut) )['sweights'].sum()

        MCeffs[key] = numMC/denMC
        print(MCeffs[key])
        err_MCeffs[key] = get_error0(MCeffs[key],denMC)

        effs[key] = num/den
        err_effs[key] = get_error0(effs[key],den)
            
                      
                      
for key in effs.keys():
    
    split = key.rsplit()
    split += [100*MCeffs[key],100*err_MCeffs[key],100*effs[key],100*err_effs[key]]
    print('pT in [{0},{1}]; eta in [{2},{3}]. MC: {4:.2f} +- {5:.2f}, data {6:.2f}+-{7:.2f}'.format(*split))
        
        
if True:
    import matplotlib.pyplot as plt
    plt.rc('text',usetex=True)
    plt.rc('font',family='sans-serif')
    plots = {}
    for key in effs.keys():
        split = key.rsplit()
        eff_key = '{0} {1}'.format(split[2],split[3])
        if eff_key not in plots:
            plots[eff_key] = ( [],#pT
                               
                               [],[],#MCEff with error
                               [],[],# Eff with error,
                               [] # xerr
                               )
        plots[eff_key][0].append( ( float(split[0]) + float(split[1]) )/2 )
        xerr = ( float(split[0]) - float(split[1]) )/2
        plots[eff_key][1].append( MCeffs[key] )
        plots[eff_key][2].append( err_MCeffs[key] )
        
        plots[eff_key][3].append( effs[key] )
        plots[eff_key][4].append( err_effs[key] )

        plots[eff_key][5].append( xerr)
              
    fig,ax = plt.subplots(1,len( plots.keys() ),figsize=(20,7) )
    ax = np.array(ax)
    ax = ax.reshape(-1)
    for a,eta in zip( ax,plots.keys() ):
        eff = plots[eta]
        a.set_title('ETA in [{0},{1}]'.format(eta.split()[0],eta.split()[1]))
        a.errorbar(eff[0],eff[1],xerr=eff[5], yerr=eff[2],fmt='s',color='xkcd:blue',label='etaMuMuGamma MC')
        a.set_ylabel('Efficiency')
        a.set_xlabel(r'p$_T$ [MeV/c]')
        
        a.errorbar(eff[0],eff[3],xerr=eff[5], yerr=eff[4],fmt='s',color='xkcd:red',label='EtaMuMuGamma 2018 Turcal Data')
        
        a.set_ylim(0,1)
        #a.set_xticks(eff[0])
        #a.grid(True)
    ax[0].legend()
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.1)
    plt.show()
    
        
        
    
for key in err_nums.keys():
    print('In bin {0}; num: {1} +- {2}; den: {3} +- {4}'.format(key,nums[key],err_nums[key],dens[key],err_dens[key]))
