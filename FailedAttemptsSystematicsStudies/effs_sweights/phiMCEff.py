from ROOT import *
from math import sqrt
from kinematic_bins import pTs, etas
#gROOT.ProcessLine(".x ../../lhcbStyle.C")
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)



basecut = 'Bs_PT > 2000   && (kminus_PIDK > 5 && kplus_PIDK >5) & kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV > 55 & gamma_CL > 0.3 && gamma_PT > 2000 & gamma_P > 6000 && abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50 && phi_PT > 1800 && (phi_PT + gamma_PT) > 4000'
basecut += '&& abs(Bs_TRUEID)==531 && abs(phi_MC_MOTHER_ID) == 531 && abs(gamma_MC_MOTHER_ID) == 531 && phi_TRUEID==333 && gamma_TRUEID==22 '

basecut = basecut.replace('Bs','B_s0')
basecut = basecut.replace('kplus','Kplus')
basecut = basecut.replace('kminus','Kminus')
basecut = basecut.replace('phi','phi_1020')



basecut = '({})'.format(basecut)
home = '/scratch47/adrian.casais/ntuples/turcal/'
f = TFile(home + 'b2phigammaMC.root')
t=f.Get('DecayTree/DecayTree')

eff_MC = {}
error_eff_MC = {}

TIS = '(gamma_L0Global_TIS && gamma_Hlt1Phys_TIS && gamma_Hlt2Phys_TIS)'
TIS = '1'
aa = '&&'
bin = '(gamma_PT > {0} && gamma_PT < {1})'.format(lowpT,highpT)
bin += '& (gamma_ETA > {0} && gamma_ETA < {1})'.format(lowETA,highETA)

num = t.GetEntries('{0} && {1} && {2} && (gamma_L0ElectronDecision_TOS || gamma_L0PhotonDecision_TOS)'.format(basecut,bin,TIS))
den = t.GetEntries('{0} && {1} && {2}'.format(basecut,bin,TIS))
eff_MC = 100.*num/den
error_eff_MC = 100.*get_error(1.*num/den,den)
