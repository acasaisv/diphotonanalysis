from ROOT import *
from math import sqrt
#gROOT.ProcessLine(".x ../../lhcbStyle.C")
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

lowpT=000
highpT = 4000000000000000
lowETA=0
highETA=1000
import sys
if len(sys.argv)>3:
    lowpT = sys.argv[1]
    highpT = sys.argv[2]
    lowETA = sys.argv[3]
    highETA = sys.argv[4]


basecut = 'B0_PT > 2000   && '\
'gamma_PT > 2000 && gamma_CL > 0.3 && '\
'Kplus_IPCHI2_OWNPV > 16 && piminus_IPCHI2_OWNPV > 16 && '\
'abs(Kst_892_0_M-892) < 150 & Kst_892_0_OWNPV_CHI2/Kst_892_0_OWNPV_NDOF < 9'
#'Kplus_TRACK_CHI2NDOF<3  && piminus_TRACK_CHI2NDOF<3 && '\
#'gamma_CL > 0.3 &&  gamma_P > 6000 && '\




# basecut = basecut.replace('B0_','B_')
# basecut = basecut.replace('Kplus_','kplus_')
# basecut = basecut.replace('Kst_892_0_','kst_')
# basecut += '&& abs(Bs_TRUEID)==511 && abs(phi_MC_MOTHER_ID) == 511 && abs(gamma_MC_MOTHER_ID) == 511 && phi_TRUEID==313 && gamma_TRUEID==22 '


basecut = '({})'.format(basecut)
home = '/scratch47/adrian.casais/ntuples/turcal'
f = TFile(home + '/b02kstargamma.root')
t=f.Get('DecayTree/DecayTree')
#t.SetAlias('gamma_ETA','0.5*log( (gamma_P+gamma_PZ)/(gamma_P-gamma_PZ) )')
t.Draw('gamma_PT - gamma_L0Calo_ECAL_realET','(gamma_L0PhotonDecision_TOS || gamma_L0ElectronDecision_TOS) && gamma_PT < 10000')
t.Draw('gamma_PT','abs(Bs_TRUEID)==511  && abs(gamma_MC_MOTHER_ID) == 511 &&  gamma_TRUEID==22'.replace('Bs','B0'))
