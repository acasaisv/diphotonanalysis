import sys
import subprocess
import numpy as np
from math import sqrt
from kinematic_bins import pTs,etas
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

nums = {}
dens = {}
err_nums = {}
err_dens = {}
effs = {}
err_effs = {}
MCeffs = {}
err_MCeffs = {}
for pT in pTs:
    for eta in etas:
        key = '{0} {1} {2} {3}'.format(pT[0],pT[1],eta[0],eta[1])
        effs[key]=1        
        for num in 1,0:
            den = 'den'
            if num:
                den='num'
                with open('/home3/adrian.casais/plots/etammgTIS/MCEff/MCEffpt{0},{1}eta{2},{3}.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                    lines = handle.readlines()
                MCeffs[key] = float(lines[0].rsplit()[1])/100.
                err_MCeffs[key] = float(lines[0].rsplit()[3])/100.
            with open('/home3/adrian.casais/plots/etammgTIS/pt{0}.0,{1}.0eta{2}.0,{3}.0L0{4}_params.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                lines = handle.readlines()
            value = float(lines[2].rsplit()[1])
            err= float(lines[2].rsplit()[3])
            if num:
                nums[key]=value
                err_nums[key]=err
            else:
                dens[key]=value
                err_dens[key]=err
                effs[key]=nums[key]/dens[key]
                err_effs[key] = get_error(effs[key],value/1.)
            
                      
            
                      
for key in effs.keys():
    
    split = key.rsplit()
    split += [100*MCeffs[key],100*err_MCeffs[key],100*effs[key],100*err_effs[key]]
    print('pT in [{0},{1}]; eta in [{2},{3}]. MC: {4:.2f} +- {5:.2f}, data {6:.2f}+-{7:.2f}'.format(*split))
        
        
if True:
    import matplotlib.pyplot as plt
    plt.rc('text',usetex=True)
    plt.rc('font',family='sans-serif')
    plots = {}
    for key in effs.keys():
        split = key.rsplit()
        eff_key = '{0} {1}'.format(split[2],split[3])
        if eff_key not in plots:
            plots[eff_key] = ( [],#pT
                               
                               [],[],#MCEff with error
                               [],[]# Eff with error
                               )
        plots[eff_key][0].append( ( float(split[0]) + float(split[1]) )/2 )
            
        plots[eff_key][1].append( MCeffs[key] )
        plots[eff_key][2].append( err_MCeffs[key] )
        
        plots[eff_key][3].append( effs[key] )
        plots[eff_key][4].append( err_effs[key] )

    fig,ax = plt.subplots(1,len( plots.keys() ),figsize=(7,7) )
    ax = np.array(ax)
    ax =ax.reshape(-1)
    for a,eta in zip(ax,plots.keys() ):
        eff = plots[eta]
        a.set_title('ETA in [{0},{1}]'.format(eta.split()[0],eta.split()[1]))
        a.errorbar(eff[0],eff[1], yerr=eff[2],fmt='s',color='xkcd:blue',label='MC efficiency')
        a.set_ylabel('Efficiency')
        a.set_xlabel(r'p$_T$ [MeV/c]')
        
        a.errorbar(eff[0],eff[3], yerr=eff[4],fmt='s',color='xkcd:red',label='Data efficiency')
        a.set_xticks(eff[0])
        #a.grid(True)
    ax[0].legend()
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.1)
    plt.show()
    
        
        
    
