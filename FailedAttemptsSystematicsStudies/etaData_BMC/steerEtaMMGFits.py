import sys
#import subprocess
from subprocess import STDOUT, call,PIPE
from kinematic_bins import pTs,etas
fits = []
cmds = []
limit=12
for pT in pTs:
    for eta in etas:
        for num in [0,1]:
            fits.append(['python', 'etammgDataFit.py',str(pT[0]),str(pT[1]),str(eta[0]),str(eta[1]),str(num)])
            fits.append(['python','phiMCEff.py',str(pT[0]),str(pT[1]),str(eta[0]),str(eta[1])])
            fits.append(['python','BkstMCEff.py',str(pT[0]),str(pT[1]),str(eta[0]),str(eta[1])])
            #subprocess.Popen(['python', 'etammgDataFit.py',str(pT[0]),str(pT[1]),str(eta[0]),str(eta[1]),str(num)],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            #subprocess.Popen(['python','etammgMCEff.py',str(pT[0]),str(pT[1]),str(eta[0]),str(eta[1])],stdout=subprocess.PIPE,stderr=subprocess.PIPE)

from multiprocessing.pool import ThreadPool
def run(cmd):
    return cmd, call(cmd)

for cmd, rc in ThreadPool(limit).imap_unordered(run,fits):
    if rc != 0:
        print('{cmd} failed with exit status: {rc}'.format(**vars()))
