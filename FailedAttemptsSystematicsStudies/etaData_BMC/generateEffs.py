import sys
import subprocess
import numpy as np
from math import sqrt
from kinematic_bins import pTs,etas
from ROOT import TEfficiency
def get_error(eff,n):
    if eff==0 or eff==1 or eff<0:
        return sqrt(1/n)
    else:
        return sqrt(eff*(1.-eff)/n)

def get_error(num,den,err_num,err_den):
    f = num/den
    if f==0 or f==1 or f<0:
        return sqrt(1/den)
    error = f * sqrt( (err_num/num)**2 + (err_den/den**2)*(err_den-2*err_num ) )
    return error

nums = {}
dens = {}
err_nums = {}
err_dens = {}
effs = {}
err_effs = {}
covs = {}
MCeffs = {}
err_MCeffs = {}
MCeffs2 = {}
err_MCeffs2 = {}
for pT in pTs:
    for eta in etas:
        key = '{0} {1} {2} {3}'.format(pT[0],pT[1],eta[0],eta[1])
        effs[key]=1        
        for num in 1,0:
            den = 'den'
            if num:
                den='num'
                with open('/home3/adrian.casais/plots/BsPhiGamma/MCEff/MCEffpt{0},{1}eta{2},{3}.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                    lines = handle.readlines()
                MCeffs[key] = float(lines[0].rsplit()[1])/100.
                err_MCeffs[key] = float(lines[0].rsplit()[3])/100.

                with open('/home3/adrian.casais/plots/BKstarGamma/MCEff/MCEffpt{0},{1}eta{2},{3}.txt'.format(pT[0],pT[1],eta[0],eta[1],den),'r') as handle:
                    lines = handle.readlines()
                MCeffs2[key] = float(lines[0].rsplit()[1])/100.
                err_MCeffs2[key] = float(lines[0].rsplit()[3])/100.
            with open('/home3/adrian.casais/plots/etammg/pt{0},{1}eta{2},{3}L0{4}_params.txt'.format(float(pT[0]),float(pT[1]),float(eta[0]),float(eta[1]),den),'r') as handle:
                lines = handle.readlines()
            value = float(lines[2].rsplit()[1])
            err= float(lines[2].rsplit()[3])
            
            if num:
                nums[key]=value
                err_nums[key]=err
            else:
                dens[key]=value
                err_dens[key]=err
                effs[key]=nums[key]/dens[key]
                if effs[key] >1: effs[key]=1
                #err_effs[key] = get_error(nums[key],dens[key],err_nums[key],err_dens[key])
                err_effs[key] = effs[key] - TEfficiency.Wilson(dens[key],nums[key],0.68,0)
            
                      
                      
for key in effs.keys():
    
    split = key.rsplit()
    split += [100*MCeffs[key],100*err_MCeffs[key],100*effs[key],100*err_effs[key]]
    print('pT in [{0},{1}]; eta in [{2},{3}]. MC: {4:.2f} +- {5:.2f}, data {6:.2f}+-{7:.2f}'.format(*split))
        
        
if True:
    import matplotlib.pyplot as plt
    plt.rc('text',usetex=True)
    plt.rc('font',family='sans-serif')
    plots = {}
    for key in effs.keys():
        split = key.rsplit()
        eff_key = '{0} {1}'.format(split[2],split[3])
        if eff_key not in plots:
            plots[eff_key] = ( [],#pT
                               
                               [],[],#MCEff with error
                               [],[],# Eff with error,
                               [],[], #MCEff2 with error
                               [] # xerr
                               )
        plots[eff_key][0].append( ( float(split[0]) + float(split[1]) )/2 )
        xerr = ( float(split[0]) - float(split[1]) )/2
        plots[eff_key][1].append( MCeffs[key] )
        plots[eff_key][2].append( err_MCeffs[key] )
        
        plots[eff_key][3].append( effs[key] )
        plots[eff_key][4].append( err_effs[key] )

        plots[eff_key][5].append( MCeffs2[key] )
        plots[eff_key][6].append( err_MCeffs2[key] )
        plots[eff_key][7].append( xerr)
              
    fig,ax = plt.subplots(1,len( plots.keys() ),figsize=(20,7) )
    ax = np.array(ax)
    ax = ax.reshape(-1)
    for a,eta in zip( ax,plots.keys() ):
        eff = plots[eta]
        a.set_title('ETA in [{0},{1}]'.format(eta.split()[0],eta.split()[1]))
        a.errorbar(eff[0],eff[1],xerr=eff[7], yerr=eff[2],fmt='s',color='xkcd:blue',label='phiGamma MC efficiency')
        a.errorbar(eff[0],eff[5],xerr=eff[7], yerr=eff[6],fmt='s',color='xkcd:purple',label='KstGamma MC efficiency')
        a.set_ylabel('Efficiency')
        a.set_xlabel(r'p$_T$ [MeV/c]')
        
        a.errorbar(eff[0],eff[3],xerr=eff[7], yerr=eff[4],fmt='s',color='xkcd:red',label='Data efficiency')
        
        a.set_ylim(0,1)
        #a.set_xticks(eff[0])
        #a.grid(True)
    ax[0].legend()
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.1)
    plt.show()
    
        
        
    
for key in err_nums.keys():
    print('In bin {0}; num: {1} +- {2}; den: {3} +- {4}'.format(key,nums[key],err_nums[key],dens[key],err_dens[key]))
