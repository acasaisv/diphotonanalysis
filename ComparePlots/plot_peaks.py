from helpers import get_ALPsdf, masses_map, sim10_masses_map, nearest_rect
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import crystalball as cb
from plot_helpers import *
from bdt_helpers import process_variables
#from energy_helpers import * 

hist_args = {'histtype':'step','bins':50,'density':True}

def plot_vars(dfalp6,dfbs,kine_vars,name='name.pdf'):
    #
    x,y = nearest_rect(len(kine_vars))
    fig,ax = plt.subplots(x,y,figsize=(16,9),dpi=80)
    ax = ax.flatten()
    for a,var in zip(ax,kine_vars):
        a.hist(dfalp6[var],color='red',label='ALP5',**hist_args)
        a.hist(dfbs[var],color='blue',label='Bs',**hist_args)
        a.set_title(var)
    ax[0].legend()
    fig.savefig(name)



if __name__ == '__main__':
    bdt_vars = process_variables()[2]
    dfalp6 = get_ALPsdf('/scratch47/adrian.casais/ntuples/signal/sim10/new/49100040_1000-1099_Sim10a-priv-new.root',bs=False,extravars=bdt_vars)
    dfbs = get_ALPsdf('/scratch47/adrian.casais/ntuples/signal/sim10/new/b2gg-sim10.root',bs=True,extravars=bdt_vars)
    kine_vars = ['B_s0_PT','B_s0_P','gamma_PT','gamma_P','gamma_CL','gamma_PP_CaloNeutralHcal2Ecal',]
    plot_vars(dfalp6,dfbs,kine_vars,name='kine.pdf')
    plot_vars(dfalp6,dfbs,bdt_vars,name='iso.pdf')
