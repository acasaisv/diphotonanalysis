from ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import subprocess
from kinematic_bins import pTs,etas
from helpers import pTs,etas,pTsHlt2,etasHlt2,spds,spdsHlt2,get_ALPsdf,trigger_cuts,sim10_masses_map,get_error,get_error_w,ufloat_to_str
from uncertainties import ufloat
#pTs,etas,spds = pTsHlt2,etasHlt2,spdsHlt2
import pickle


def get_df(i,bs=False):
    if type(i)!=str:
        root = '/scratch47/adrian.casais/ntuples/signal'
        f_s = uproot.open(root + '/491000{}_MagDown.root'.format(i))
    elif i=='bs':
        f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root')
    t_s = f_s['DTTBs2GammaGamma/DecayTree']
    
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            'gamma_CaloHypo_X',
            'gamma_CaloHypo_Y',
            'gamma0_CaloHypo_X',
            'gamma0_CaloHypo_Y',
            'gamma_CaloHypo_HypoE',
            'gamma_CaloHypo_HypoEt',
            'gamma0_CaloHypo_HypoE',
            'gamma0_CaloHypo_HypoEt',
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'B_s0_PT',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
            'gamma0_ShowerShape',
            'gamma_ShowerShape',
            'gamma0_PP_IsNotH',
            'gamma_PP_IsNotH',
            'nSPDHits'
            ]
    truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
    if bs:
        truth = "(abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22) & abs(B_s0_TRUEID)==531"
    df = t_s.pandas.df(branches=vars)
    df_original = t_s.pandas.df(branches=vars)
    #df.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',inplace=True)
    df.query(truth,inplace=True)
    df.query('gamma_PT > 2500 & gamma0_PT > 2500',inplace=True)
    #probably above 7500 we don't have a systematic
    df.query('(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)',inplace=True)
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    df['gamma0_ETA'] = 0.5*np.log( (df['gamma0_P']+df['gamma0_PZ'])/(df['gamma0_P']-df['gamma0_PZ']) )

    df.reset_index(inplace=True)
    return df 
def load_dfs():
    root = '/scratch47/adrian.casais/ntuples/turcal'
    dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df')
    df = pd.read_hdf(root+'/etammgTurcalDataStripping.h5',key='df')
    df.query('gamma_PT > 2500',inplace=True)
    df.query('eta_M > 465 & eta_M < 630',inplace=True)
    df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    df.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    dfMC.query('gamma_PT > 2500',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    dfMC.query('eta_M > 465 & eta_M < 630',inplace=True)
    
    # for d in df,dfMC:
    #     d['deltaET'] = d['gamma_PT'] - d['gamma_L0Calo_ECAL_TriggerET']
    
    df.reset_index(inplace=True)
    dfMC.reset_index(inplace=True)
    
    return df,dfMC

def print_to_text(df,file):
    string=""
    df['minShowerShape'] = np.minimum(df['gamma_ShowerShape'].array,df['gamma0_ShowerShape'].array)
    df['maxShowerShape'] = np.maximum(df['gamma_ShowerShape'],df['gamma0_ShowerShape'])
    df['minIsNotH'] = np.minimum(df['gamma_PP_IsNotH'],df['gamma0_PP_IsNotH'])
    df['maxIsNotH'] = np.maximum(df['gamma_PP_IsNotH'],df['gamma0_PP_IsNotH'])
    
    for _,row in df.iterrows():
        string+="{0} {1} {2} {3} {4}\n".format(row["B_s0_PT"],
                                             row["minIsNotH"],
                                             row["maxIsNotH"],
                                             row["minShowerShape"],
                                             row["maxShowerShape"])                                             
    with open(file,'w') as handle:
        handle.write(string)
        
    
def precuts_hlt2(df):
    trimmed_df = df.query('B_s0_M > 3500 and B_s0_M <20000 and (gamma_PT + gamma0_PT) > 6000 and B_s0_PT > 2000')
    eff = len(trimmed_df)/len(df)
    return trimmed_df,ufloat(eff,get_error(len(trimmed_df),len(df)))

def open_outputs(file):
    df = pd.read_csv(file,sep=' ',header=None)
    return df
def weights(root):
    o=subprocess.run([root+'lwtnn-test-arbitrary-net',root+'nn.json',root+'labels.txt',root+'values.txt'],capture_output=True,text=True)
    #print(o.stdout)
    with open(root+'output.txt','w') as handle:
        handle.write(str(o.stdout))


def recalc_pt(df,zTrigger=12300):
    df.eval('zTrigger = {}'.format(zTrigger),inplace=True)
    
    df.eval('tx  = gamma_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty  = gamma_CaloHypo_Y/zTrigger',inplace=True)
    df.eval('tx0  = gamma0_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty0  = gamma0_CaloHypo_Y/zTrigger',inplace=True)
    
    df.eval('tx  = gamma_PX/gamma_PZ',inplace=True)
    df.eval('ty  = gamma_PY/gamma_PZ',inplace=True)
    df.eval('tx0  = gamma0_PX/gamma0_PZ',inplace=True)
    df.eval('ty0  = gamma0_PY/gamma0_PZ',inplace=True)
    df.eval('gamma_PX = gamma_PT * tx*sqrt(1/(tx**2+ty**2))',inplace=True)
    df.eval('gamma_PY = gamma_PT * ty*sqrt(1/(tx**2+ty**2))',inplace=True)
    df.eval('gamma_PZ = gamma_PT * 1/sqrt(tx**2+ty**2)',inplace=True)
    
    df.eval('gamma0_PX = gamma0_PT * tx0*sqrt(1/(tx0**2+ty0**2))',inplace=True)
    df.eval('gamma0_PY = gamma0_PT * ty0*sqrt(1/(tx0**2+ty0**2))',inplace=True)
    df.eval('gamma0_PZ = gamma0_PT * 1/sqrt(tx0**2+ty0**2)',inplace=True)

    df.eval('gamma_PE = sqrt(gamma_PX**2 + gamma_PY**2 + gamma_PZ**2)',inplace=True)
    df.eval('gamma0_PE = sqrt(gamma0_PX**2 + gamma0_PY**2 + gamma0_PZ**2)',inplace=True)
    df.eval('costheta = (gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_PE*gamma0_PE)',inplace=True)
    df.eval('B_s0_PT = sqrt((gamma0_PX+gamma_PX)**2 + (gamma_PY + gamma0_PY)**2)',inplace=True)
    df.eval('B_s0_M = sqrt(2*gamma_PE*gamma0_PE*(1-costheta))',inplace=True)
    return df

def rescale_vars(df,pTs,etas,spds,rescales):
    for ipt in range(len(pTs)-1):
        pt = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                df_ = df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA > {2} and gamma_ETA < {3} and nSPDHits >= {4} and nSPDHits< {5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA > {2} and gamma0_ETA < {3} and nSPDHits >= {4} and nSPDHits< {5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                
                for g in ('gamma','gamma_0'):
                    for var in ['_ShowerShape',
                                #'_PP_IsNotH',
                                '_PT']:
                        if g=='gamma':
                            df.loc[df_.index,'gamma'+var] = df_['gamma'+var]*rescales['gamma'+var][pt][eta][spd]
                        else:
                            df.loc[df0_.index,'gamma0'+var] = df0_['gamma0'+var]*rescales['gamma'+var][pt][eta][spd]
            
    recalc_pt(df)
    
def sample_is_not_h(dfetammg,df,pTs,etas,spds):
    df['IsNotHOld'] = df['gamma_PP_IsNotH']
    df['IsNotH0Old'] = df['gamma0_PP_IsNotH']
    for ipt in range(len(pTs)-1):
        pt = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])

                dfetammg_ = dfetammg.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5} & gamma_PP_IsNotH > 0.3 & gamma_P>6000.'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                weights = dfetammg_['sweights']
                offset = min(weights)
                weights = np.array([z - offset for z in weights])
                df_ = df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >={2} and gamma_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))

                df.loc[df_.index,'gamma_PP_IsNotH'] = np.random.choice(dfetammg_['gamma_PP_IsNotH'],size = len(df_),p =weights/np.sum(weights))
                df.loc[df0_.index,'gamma0_PP_IsNotH'] = np.random.choice(dfetammg_['gamma_PP_IsNotH'],size = len(df0_),p=weights/np.sum(weights))
                
                #df.loc[df_.index,'gamma_PP_ShowerShape'] = np.random.choice(dfetammg_['gamma_ShowerShape'],size = len(df_),p =weights/np.sum(weights))
                #df.loc[df0_.index,'gamma0_PP_ShowerShape'] = np.random.choice(dfetammg_['gamma_ShowerShape'],size = len(df0_),p=weights/np.sum(weights))
                
                #df.loc[df_.index,'gamma_PT'] = np.random.choice(dfetammg_['gamma_PT'],size = len(df_),p =weights/np.sum(weights))
                #df.loc[df0_.index,'gamma0_PT'] = np.random.choice(dfetammg_['gamma_PT'],size = len(df0_),p=weights/np.sum(weights))
                #recalc_pt(df)
def compare_is_not_h(df,i):
    fig,ax = plt.subplots(1,1)
    ax.hist(df['IsNotHOld'],histtype='step',label='Simulation',bins=50,density=True)
    ax.hist(df['gamma_PP_IsNotH'],histtype='step',label='Data',bins=50,density=True)
    ax.legend()
    fig.savefig(f'/home3/adrian.casais/figstoscp/hlt2_isnoth_{i}.pdf')
    #plt.show()
    
             
    
if __name__ == '__main__':
    masses = sim10_masses_map
    bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'
    dfetammg= load_dfs()[0]
    dfeff = pd.DataFrame({ 'Mass':[],
                           r'\efftos':[],
                           r'\effrescale':[]})
    for i in sim10_masses_map:
      print("Mass is {0}".format(masses[i]))
      #df = get_df(i,False)
      cut = '{0} & {1} & (gamma_PT > 2500 & gamma0_PT > 2500)'.format(trigger_cuts['L0'],trigger_cuts['HLT1'])
      rootalp = '/scratch47/adrian.casais/ntuples/signal/sim10/'
      df = get_ALPsdf(rootalp+f'491000{i}_1000-1099_Sim10a-priv.root',False,background=False,extracut=cut,sample=int(10e4),extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH'])
      
      sample_is_not_h(dfetammg,df,pTs,etas,spds)
      compare_is_not_h(df,i)

      #print("HLT2 TOS efficiency")
      tos_eff = ufloat(len(df.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'))/len(df),get_error(len(df.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS')),len(df)))
      #print("{:.3f} %".format(100.*tos_eff))
      with open("rescales.p","rb") as handle:
        rescales = pickle.load(handle)
      rescale_vars(df,pTs,etas,spds,rescales)
    

      df,pre_eff = precuts_hlt2(df)
      root = '/scratch47/adrian.casais/lwtnn/build/bin/'
      print_to_text(df,root+'values.txt')
      weights(root)
      myarr = open_outputs(root+'output.txt')
      myarr = np.array(myarr[1].array)
      myarr_cut = myarr[myarr>0.8]
      #print("HLT2 efficiency as obtained with lwtnn")
      lwtnn_eff = pre_eff *ufloat(len(myarr_cut)/len(myarr),get_error(len(myarr_cut),len(myarr)))
      effarray = np.array([masses[i],ufloat_to_str(tos_eff),ufloat_to_str(lwtnn_eff)])
      dfeff=dfeff.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeff)),ignore_index=True)
      #print("{:.3f} %".format(100.*lwtnn_eff))
      #print("Relative systematic")
      #print("{:.3f} %".format(100.*abs(tos_eff-lwtnn_eff)/tos_eff))
      print(f"HLT2 TOS efficiency {100.*tos_eff.n} +- {100.*tos_eff.s} %. HLT2 efficiency after rescaling: {100.*lwtnn_eff.n} +- {100.*lwtnn_eff.s} %.")
      print(8*"==")

print(dfeff.to_latex(index=False,escape=False))
