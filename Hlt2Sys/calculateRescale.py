
from ROOT import *
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTs,etas,spds,get_ALPsdf,trigger_cuts
from plot_helpers import *
import pickle
def nearest_rect(n):
    sqn = np.sqrt(n)
    for i in range(2,int(sqn)+2):
        if not n % i: return (i,int(n/i))

def load_dfs():
    root = '/scratch47/adrian.casais/ntuples/turcal'
    #dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df')
    #df = pd.read_hdf(root+'/etammgTurcalDataStripping.h5',key='df')

    dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')
    df = pd.read_hdf(root+'/etammgTurcalData.h5',key='df')
        
    df.query('gamma_PT > 2500',inplace=True)
    df.query('eta_M > 465 & eta_M < 630',inplace=True)
    df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    df.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    dfMC.query('gamma_PT > 2500',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    dfMC.query('eta_M > 465 & eta_M < 630',inplace=True)
    dfMC.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    # for d in df,dfMC:
    #     d['deltaET'] = d['gamma_PT'] - d['gamma_L0Calo_ECAL_TriggerET']
    
    df.reset_index(inplace=True)
    dfMC.reset_index(inplace=True)
    rootalp = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    cut = '{0} & {1} & (gamma_PT > 2500 & gamma0_PT > 2500)'.format(trigger_cuts['L0'],trigger_cuts['HLT1'])
    dfALP = get_ALPsdf(
        rootalp+f'49100041_1000-1099_Sim10a-priv.root',
        #40,
        False,background=False,extracut=cut,sample=int(10e4),extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH'])

    return df,dfMC, dfALP

def reweight_mc_df(df,dfMC):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = ['eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]

    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return dfMC

def plots(df,dfMC,dfALP,rescale=False):
    fig,ax = plt.subplots(2,2,figsize=(16,9),dpi=80)
    n_plots = 4
    ax_flat =ax.flatten()
    alpha = 0.7
    
    ax_flat[0].set_title("ShowerShape")
    ax_flat[0].hist(df['gamma_ShowerShape'],bins=35,density=True,alpha=alpha,histtype='step',weights=df['sweights'],range=(0,8000))
    ax_flat[0].hist(dfMC['gamma_ShowerShape'],bins=35,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'],range=(0,8000))
    ax_flat[0].hist(dfALP['gamma_ShowerShape'],bins=35,density=True,alpha=alpha,histtype='step',range=(0,8000))
    
    #ax_flat[0].hist(dfMC['gamma_ShowerShape_rescale'],bins=35,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'],range=(0,8000))
    
    ax_flat[0].legend(["2018 Turcal Data",
                       "sim09 MC",
                       "ALP MC"
                       ],bbox_to_anchor=[0.05,0.95])
    
    ax_flat[1].set_title("IsNotH")
    ax_flat[1].hist(df['gamma_PP_IsNotH'],bins=50,density=True,alpha=alpha,histtype='step',weights=df['sweights'])
    ax_flat[1].hist(dfMC['gamma_PP_IsNotH'],bins=50,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'])
    ax_flat[1].hist(dfALP['gamma_PP_IsNotH'],bins=50,density=True,alpha=alpha,histtype='step')
    #ax_flat[1].hist(dfMC['gamma_PP_IsNotH_rescale'],bins=50,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'])
    
        
    ax_flat[2].set_title(r"$\gamma p_T$")
    ax_flat[2].hist(df['gamma_PT'],bins=35,density=True,alpha=alpha,histtype='step',weights=df['sweights'],range=(0,15000))
    ax_flat[2].hist(dfMC['gamma_PT'],bins=35,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'],range=(0,15000))
    ax_flat[2].hist(dfALP['gamma_PT'],bins=35,density=True,alpha=alpha,histtype='step',range=(0,15000))
    ax_flat[2].set_xlabel(r'$\gamma(p_T)$ [MeV]',horizontalalignment='right', x=1.0,fontsize=13)
    #ax_flat[2].hist(dfMC['gamma_PT_rescale'],bins=35,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'],range=(0,15000))

    ax_flat[3].set_title("gamma ETA")
    ax_flat[3].hist(df['gamma_ETA'],bins=50,density=True,alpha=alpha,histtype='step',weights=df['sweights'])
    ax_flat[3].hist(dfMC['gamma_ETA'],bins=50,density=True,alpha=alpha,histtype='step',weights=dfMC['sweights'])
    ax_flat[3].hist(dfALP['gamma_ETA'],bins=50,density=True,alpha=alpha,histtype='step')
    plt.savefig('/home3/adrian.casais/hlt2_distribs.pdf'.format(id))
    plt.show()
    
def check_mean(bins,df,dfMC,variable = 'gamma_ShowerShape'):
    width = bins[1]-bins[0]
    means = []
    meansMC = []
    std = []
    stdMC = []
    for bin in bins:
        df_tmp = df.query('gamma_PT > {0} and gamma_PT < {1}'.format(bin-width/2,bin+width/2))
        dfMC_tmp = dfMC.query('gamma_PT > {0} and gamma_PT < {1}'.format(bin-width/2,bin+width/2))
        means.append(np.mean(df_tmp[variable].array))
        std.append(np.std(df_tmp[variable].array))
        stdMC.append(np.std(dfMC_tmp[variable].array))
        meansMC.append(np.mean(dfMC_tmp[variable].array))
    return means,meansMC,std,stdMC

def plot_in_bins(bins,df,dfMC,variable='gamma_ShowerShape'):
    n_bins = len(bins)
    n,m = nearest_rect(n_bins)
    fig,ax = plt.subplots(n,m,figsize=(16,9),dpi=80)
    ax =ax.flatten()
    width = bins[1]-bins[0]
    alpha = 0.6
    #fig.set_title(variable)
    fig.suptitle(variable, fontsize=16)
    for bin,i in zip(bins,range(len(bins))):
        df_tmp = df.query('gamma_PT > {0} and gamma_PT < {1}'.format(bin-width/2,bin+width/2))
        dfMC_tmp = dfMC.query('gamma_PT > {0} and gamma_PT < {1}'.format(bin-width/2,bin+width/2))
        ax[i].set_title("gammaPT in ({0:.2f},{1:.2f}) MeV".format(bin-width/2,bin+width/2))
        ax[i].hist(df_tmp[variable],bins=20,density=True,alpha=alpha,histtype='step',weights=df_tmp['sweights'])
        ax[i].hist(dfMC_tmp[variable],bins=20,density=True,alpha=alpha,histtype='step',weights=dfMC_tmp['sweights'])
    ax[0].legend(["Data","MC"],bbox_to_anchor=[0.05,0.95])
    
def plot_means(bins,means,meansMC,std,stdMC):
    fig, ax = plt.subplots(1,1,figsize=(16,9),dpi=80)
    ax.errorbar(bins,means,xerr=(bins[1]-bins[0])/2, yerr=stdMC,fmt='s',color='xkcd:red')
    ax.errorbar(bins,meansMC,xerr=(bins[1]-bins[0])/2, yerr=stdMC,fmt='s',color='xkcd:blue')
    plt.show()

def rescaleMC(df,dfMC,variable):
    rescale=np.average(df[variable],weights=df['sweights'])/np.average(dfMC[variable],weights=dfMC['sweights'])
    dfMC[variable+'_rescale']=df[variable]*rescale
    print("{0} rescale is {1:.3f}".format(variable,rescale))

def printRescaleMC(df,dfMC,variable,pt,eta,spd):
    df_ = df.query("gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA>= {2} and gamma_ETA<{3} and nSPDHits >= {4} and nSPDHits < {5}".format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
    dfMC_ = dfMC.query("gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA>= {2} and gamma_ETA<{3} and nSPDHits >= {4} and nSPDHits < {5}".format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
    rescale=np.average(df_[variable],weights=df_['sweights'])/np.average(dfMC_[variable],weights=dfMC_['sweights'])
    
    print("{0} rescale is {1:.3f}. PT in ({2},{3})".format(variable,rescale,pt[0],pt[1]))
    return rescale
if __name__=='__main__':
    df,dfMC,dfALP = load_dfs()
    dfMC = reweight_mc_df(df,dfMC)
    df.query('gamma_PT >5000 and gamma_PT < 6000',inplace=True)
    df.query('gamma_ETA >3 and gamma_ETA < 3.5',inplace=True)
    df.query('nSPDHits>100 and nSPDHits <200',inplace=True)
    dfMC.query('gamma_PT >5000 and gamma_PT < 6000',inplace=True)
    dfMC.query('gamma_ETA >3 and gamma_ETA < 3.5',inplace=True)
    dfMC.query('nSPDHits>100 and nSPDHits <200',inplace=True)
    dfALP.query('nSPDHits>100 and nSPDHits <200',inplace=True)
    dfALP.query('gamma_ETA >3 and gamma_ETA < 3.5',inplace=True)
    dfALP.query('gamma_PT >5000 and gamma_PT < 6000',inplace=True)
    plots(df,dfMC,dfALP,rescale=False)
    # centers = lambda bins: (bins[:-1] + bins[1:])/2
    # bins=centers(np.linspace(2500,12000,50))
    # variable = 'gamma_PP_IsNotH'
    # means,meansMC,std,stdMC = check_mean(bins,df,dfMC,'gamma_ShowerShape')
    # #plot_means(bins,means,meansMC,std,stdMC)
    # variables=['gamma_ShowerShape',
    #            'gamma_PP_IsNotH',
    #            'gamma_PT']
    # rescales  = {}
    # for var in variables:
    #     rescaleMC(df,dfMC,var)
    #     rescales[var]={}
    #     for ipt in range(len(pTs)-1):
    #         pt = (pTs[ipt],pTs[ipt+1])
    #         rescales[var][pt]={}
    #         for ieta in range(len(etas)-1):
    #             eta = (etas[ieta],etas[ieta+1])
    #             rescales[var][pt][eta]={}
    #             for ispd in range(len(spds)-1):
    #                 spd = (spds[ispd],spds[ispd+1])
                    
    #                 rescales[var][pt][eta][spd]=printRescaleMC(df,dfMC,var,pt,eta,spd)
    # with open("rescales.p","wb") as handle:
    #     pickle.dump(rescales,handle)

        
    # #wider_bins = centers(np.linspace(2500,12000,16))
    # # for var in 'gamma_PP_IsNotH','gamma_ShowerShape':
    # #     plot_in_bins(wider_bins,df,dfMC,var)
    # plt.show()
