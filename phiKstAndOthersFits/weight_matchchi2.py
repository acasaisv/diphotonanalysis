import numpy as np
import scipy
####IMPORT DATA
import uproot3 as uproot
import numpy as np
#from ROOT import *
from hep_ml.reweight import GBReweighter, FoldingReweighter
import pickle

#Bs mass window
m1g,m2g = 5000,5800
#Phi mass window
m1k,m2k = 1006,1034


root = '/scratch47/adrian.casais/ntuples/turcal'

f_s = uproot.open(root + '/b02phigammaMC-sim10.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))
variables += ['gamma_MC_MOTHER_ID',
              'gamma_TRUEID',
              'gamma_PP_Saturation',
              'phi_TRUEID',
              'gamma_TRUEP_E',
              'kplus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kplus_TRUEID',
              'kminus_TRUEID',
              'B_M',
              'B_MM',
              'B_PT',
              'B_ETA',
              'B_DIRA_OWNPV',
              'nSPDHits',
              #'B_ENDVERTEX_X',
              #'B_ENDVERTEX_Y',
              #'B_ENDVERTEX_Z',
              'phi_M',
              'phi_MM',
              'phi_OWNPV_CHI2',
              'phi_PT',
              'kplus_IPCHI2_OWNPV',
              'kminus_IPCHI2_OWNPV',
              'kminus_PT',
              'kplus_PT',
              'kminus_PIDK',
              'kplus_PIDK',
              'gamma_CL',
              'gamma_PT',
              'gamma_P',
              'gamma_PP_IsPhoton',
              'gamma_L0ElectronDecision_TOS',
              'gamma_L0PhotonDecision_TOS',
              'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_Hlt1TrackMVADecision_Dec',
              'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS',              
              'gamma_1.00_cc_asy_PT',
              'gamma_1.35_cc_asy_PT',
              'gamma_1.70_cc_asy_PT',
              'gamma_1.00_cc_asy_P',
              'gamma_1.35_cc_asy_P',
              'gamma_1.70_cc_asy_P',
              'gamma_PP_CaloTrMatch'
        ]

    
L0 = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L1 = 'B_Hlt1TrackMVADecision_Dec'
L2 = 'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
#trigger=L0
mc ='abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and abs(phi_TRUEID)==333 and abs(kplus_TRUEID) == 321 and abs(kminus_TRUEID)==321'

df = t_s.pandas.df(branches=set(variables))
df = df.query(mc)
df = df.query(trigger)
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))
def load_bdt(mydf):
    import pickle
    vars_bdt = [
        'B_DIRA_OWNPV',
        'phi_OWNPV_CHI2',
        #'phi_PT',
        'kplus_IPCHI2_OWNPV',
        'kminus_IPCHI2_OWNPV',
        # 'gamma_1.00_cc_asy_PT',
        # 'gamma_1.35_cc_asy_PT',
        # 'gamma_1.70_cc_asy_PT',
        # 'gamma_1.00_cc_asy_P',
        # 'gamma_1.35_cc_asy_P',
        # 'gamma_1.70_cc_asy_P',
        #'kminus_PT',
        #'kplus_PT',
        #'kminus_PIDK',
        #'kplus_PIDK',
        
        ]
    with open('../PhiGammaBDT/bdt.pickle','rb') as handle:
        bdts = pickle.load(handle)

    mean_bdt=np.zeros(len(df))
    for i in range(len(bdts)):
        mean_bdt += bdts[i].predict_proba(df[vars_bdt])[:,1]/len(bdts)
    
    mydf['bdt']=mean_bdt
    return mydf
load_bdt(df)


# df.query('B_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV >55)  & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46)<100 & phi_OWNPV_CHI2 < 30 & phi_PT > 1800 & (phi_PT + gamma_PT) >3000',inplace=True)
# df.query('gamma_PT > 3000 and gamma_P > 6000  & gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)

#df.query('B_PT > 2000 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 5 & kplus_PIDK >5)  & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_PT > 1800 & (phi_PT + gamma_PT) > 3000',inplace=True)
#df.query('B_PT > 2000 and gamma_PT > 3000 and gamma_P > 6000 and  gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)
#df.query('gamma_PP_Saturation <0',inplace=True)
df.query('bdt>0.6 & gamma_CL > 0.3',inplace=True)


alptype = 49100048

path = f"/scratch47/adrian.casais/ntuples/signal/sim10/{alptype}_1000-1099_Sim10a-priv.root"
eventsalp = uproot.open(path)["DTTBs2GammaGamma/DecayTree"]

variables_gamma = [ "gamma_PT",
                    "gamma_PP_CaloNeutralHcal2Ecal",
                    "gamma_P",
                    "gamma_PP_IsNotH",
                    "gamma_PP_IsPhoton",
                    "gamma_TRUEID",
                    "gamma_ETA",
                    "gamma_PP_CaloNeutralID",
                    "gamma_CaloHypo_Saturation",
                    "gamma_Matching"
                    ]
variables_gamma0 = []
for var in variables_gamma:
    var2 = var.replace("gamma_", "gamma0_")
    variables_gamma0.append(var2)

variables_Bs = ["B_s0_TRUEID",
                "nSPDHits"]

df_alp = eventsalp.pandas.df(branches=variables_gamma+variables_gamma0+variables_Bs)
cuts = 'gamma_PT > 3000 & gamma_PP_CaloNeutralHcal2Ecal<0.1 & gamma_P>6000 & gamma_PP_IsNotH>0.3 & nSPDHits<450'
cuts_gamma0 = 'gamma0_PT > 3000 & gamma0_PP_CaloNeutralHcal2Ecal<0.1 & gamma0_P>6000 & gamma0_PP_IsNotH>0.3'
truth_a2gg = 'abs(gamma0_TRUEID)==22&(abs(B_s0_TRUEID)==54|abs(B_s0_TRUEID)==531)&abs(gamma_TRUEID)==22'

alp_signal = df_alp.query(cuts+'&'+cuts_gamma0+'&'+truth_a2gg)

df["gamma_Matching"]=df["gamma_PP_CaloTrMatch"]


reweighter = GBReweighter(gb_args={'subsample': 0.5, 'random_state': 1235})
reweighter.fit(original=df["gamma_Matching"], target=alp_signal["gamma_Matching"])
df["reweight"]=reweighter.predict_weights(df["gamma_Matching"])

with open(f"calomatch_reweight_{alptype}.pkl", "wb") as file:
    pickle.dump(reweighter,file)