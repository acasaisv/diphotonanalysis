import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
from fit_helpers import create_double_cb

def res_fit(df):
    import zfit
    range = (-0.14,0.14)
    obs = zfit.Space('reso',range)
    BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.03,0.01,0.1),nevs=len(df)+100,extended=True)
    model_Bs=BsCBExtended
    df.eval('gamma_resolution = (-gamma_P + gamma_TRUEP_E)/gamma_TRUEP_E',inplace=True)
    df.query(f'gamma_resolution > {range[0]} and gamma_resolution < {range[1]}')

    data = zfit.Data.from_numpy(array=df['gamma_resolution'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =BsCBExtended,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=7000000)
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    reload(plot_helpers)
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['gamma_resolution'],
                        full_model=BsCBExtended,
                        components=[BsCBExtended],
                        yields=[nSig],
                        labels=[r'$B_s$ signal double Crystal Ball'],
                        colors=['blue'],
                        nbins=30,
                        myrange=range,
                        xlabel=r'Resolution',
                        savefile=f'./resolution-kstgamma.pdf',
                        plot=False)


#Bs mass window
jpsi=1
m1g,m2g = 4900,5900
mass = 5366.88
myfile = '/monitoring-bsmumu.root'
mylabel=r'$B^0_s$ signal double Crystal Ball'
if jpsi:
    m1g,m2g = 2900,3400
    mass = 3096.916
    myfile = '/monitoring.root'
    mylabel = r'$J/\psi$ signal double Crystal Ball'

#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))

obs = BsMassRange

# root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
# f_s = uproot.open(root + '/phiGammaMC.root')


root = '/scratch47/adrian.casais/muontracks/'
#f_s = uproot.open(root + '/b2phigammaMC.root'
f_s = uproot.open(root + myfile)
t_s = f_s['Hlt1DiMuonLowMassVM/monitor_tree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
print(keys)
variables = []

#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))


df = t_s.pandas.df()

df.query('Hlt1DiMuonLowMassVM__dev_m_t > {0} & Hlt1DiMuonLowMassVM__dev_m_t < {1}'.format(m1g,m2g,m1k,m2k),inplace=True)

# df.query('gamma_PT > 3000 and gamma_PP_Saturation<1 and gamma_CL > 0.5 and gamma_PP_CaloTrMatch <0',inplace=True)


#res_fit(df)
#######################




name_prefix='Bs_'
BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,mass_range=BsMassRange,mass =mass,sigma=(50,40,200),nevs=1.1*len(df))
#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,0,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
nBkgComb = zfit.Parameter(name_prefix + 'nBkg',len(df)/2,0,len(df))
BsBkgCombExtended = BsBkgComb.create_extended(nBkgComb)
f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
model_Bs=BsCBExtended


data = zfit.Data.from_numpy(array=df['Hlt1DiMuonLowMassVM__dev_m_t'].to_numpy(),obs=obs)

nll = zfit.loss.ExtendedUnbinnedNLL(model =model_Bs,data=data)
#nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)

minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
if model_Bs !=BsCBExtended:
    from hepstats.splot import compute_sweights
    sweights = compute_sweights(model_Bs, data)
    signal_sweights = sweights[list(sweights.keys())[0]]
    df['sweights'] = signal_sweights
    # #print(sweights)
    #df['sweights'] = np.ones(len(df))
else:
    df['sweights']=np.ones(len(df))
#result.error()
print(result)
#df.to_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='dfMC')


#print(result.params)
#result.
#PLOT
plot= True
#     plt.show()
nSig = BsParameters['nSig']
nSig=len(df)
import plot_helpers
nSig=zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['Hlt1DiMuonLowMassVM__dev_m_t'],
                      full_model=model_Bs,
                      components=[BsCBExtended,
                      #BsBkgCombExtended
                      ],
                      yields=[nSig,
                      #nBkgComb
                      ],
                      labels=[mylabel],
                      colors=['blue'],
                      nbins=30,
                      myrange=(m1g,m2g),
                      xlabel=r'$M(\mu^+ \mu^-)$ [MeV]',
                      savefile='./muon_track_fit.pdf')

import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
params = [BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],BsParameters['a_l'],BsParameters['a_r'],BsParameters['n_l'],BsParameters['n_r']]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':[#$'n_\\textrm{Signal}',
                    #'n_\\textrm{Background}',
                    '$m/m_\\textrm{PDG}$',
                    '$\\sigma$',
                    '$\\Delta(\\sigma)$',
                    '$\\alpha_L$',
                    '$\\alpha_R$',
                    '$n_L$',
                    '$n_R$',]
       ,'Units':['','MeV','MeV','','','',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))


