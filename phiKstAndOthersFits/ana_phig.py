from ROOT import *
from get_binomial import *
gROOT.ProcessLine(".x /afs/cern.ch/lhcb/software/releases/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

f1,t1,tlumi,h1 = {},{},{},{}
f1 = TFile(os.environ["EHOME"]+"/ntuples/Data2016_S28_Radiative.root")
t1 = f1.Get("DTTBs2phig/DecayTree")
tlumi = f1.Get("GetIntegratedLuminosity/LumiTuple")
lumitot = 0.
for el in tlumi: lumitot+=el.IntegratedLuminosity
print "Tot lumi",lumitot
#Tot lumi Radiative  1645.89727263
trigger = "(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS) && (Bs_Hlt1TrackMVADecision_TOS) && Bs_Hlt2RadiativeBs2PhiGammaDecision_TOS"
t1.Draw("Bs_M",trigger)
