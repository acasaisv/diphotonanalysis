import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot
import numpy as np
import pandas


efficiency = True
lowpT = 8000
highpT = 800000000000
num = True


#Bs mass window
m1eta,m2eta = 465,630


Eta1MassRange = zfit.Space('eta_M',(m1eta,m2eta))

root = '/scratch47/adrian.casais/ntuples/turcal'
f_s = uproot.open(root + '/turcal.root')
t_s = f_s['Eta2MuMuGamma_Tuple/DecayTree']

variables = ['eta_M',
             'eta_L0PhotonDecision_TOS',
             'eta_L0ElectronDecision_TOS',
             'gamma_L0PhotonDecision_TOS',
             'gamma_L0ElectronDecision_TOS',
             'gamma_L0Global_TIS',
             'gamma_Hlt1Phys_TIS',
             'gamma_Hlt2Phys_TIS',
             'gamma_PT',
             'gamma_CL',
             'gamma_P',
             'eta_PT'
             ]
df = t_s.pandas.df(branches=variables)
df.dropna()
#df=df.sample(n=int(1e6),random_state=100)
df = df.query('eta_M > {0} & eta_M < {1}'.format(m1eta,m2eta))
df = df.query('gamma_CL>0.3 & gamma_P> 6000 & eta_PT>2000')
if efficiency:
    trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS & gamma_Hlt2Phys_TIS"
    trigger +=' & (gamma_PT > {0} & gamma_PT < {1})'.format(lowpT,highpT)
    #trigger +=' & (gamma_PT > 5000 & gamma_PT < 8000)'
    #trigger += ' & (gamma_PT > 8000)'
    if num:
       #trigger += " & Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS"
       trigger += '& (gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'

df = df.query(trigger)
#eta0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.05)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',5.,1.,80.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',0.433,
                                     0.01,
                                     5,
                                     floating=False
                                     )
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',101,
                                     0.1,
                                     150,
                                     floating=False
                                     )

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.69,
                                     -5,
                                     -0.01,
                                     floating=False
                                     )
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',37.4,
                                     0.5,
                                     150,
                                     floating=False
                                     )
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.1,0.001,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=Eta1MassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=Eta1MassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.2138,0,1,floating=False)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.5,0,1)
secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
#BsBkgComb = zfit.pdf.Legendre(Eta1MassRange,[firstOrder,secondOrder,thirdOrder,fourthOrder])
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])



#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,
                                BsBkgCombExtended
                                ])


data_Bs = zfit.Data.from_numpy(array=df['eta_M'].values,obs=Eta1MassRange)
#CREATE LOSS FUNCTION

#Extended
nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,
                                    data=data_Bs)

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)
#PLOT
plot= True
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=100
    nentries=len(df)
    xBs = np.linspace(m1eta,m2eta,1000)
    countsBs, bin_edgesBs = np.histogram(df['eta_M'], nbins, range=(m1eta, m2eta))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2eta-m1eta)/nbins*zfit.run(
        BsParameters['nSig']*BsCBExtended.pdf(xBs)+
        BsParameters['nBkgComb']*BsBkgCombExtended.pdf(xBs)
        )
    yBsSig = (m2eta-m1eta)/nbins*zfit.run(BsParameters['nSig']*BsCBExtended.pdf(xBs))
    yBsBkg = (m2eta-m1eta)/nbins*zfit.run(BsParameters['nBkgComb']*BsBkgCombExtended.pdf(xBs))
    
    fig,ax = plt.subplots(1)
    #ax.set_ylim([0,250e3])
    ax.errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax.set_yscale('log')
    ax.plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax.plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Bs signal')
    ax.plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax.legend()
    
    
    
    
    fig.savefig('/home3/adrian.casais/plots/pt{0},{1}L0{2}.pdf'.format(lowpT,highpT,_den))
    plt.show()



#TOS: 1713 +- 46
#!TOS: 2624 +- 450
s = 12650
b= 58280
s_err = 1.3
b_err =450
eff = s/(b)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
