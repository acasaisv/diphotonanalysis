from ROOT import *
from math import sqrt
def get_error(eff,n):
    return sqrt(eff*(1.-eff)/n)

# basecut = '(Bs_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.3 & gamma_PT > 2000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50)'

basecut = 'Bs_PT > 2000   && (kminus_PIDK > 5 && kplus_PIDK >5) & kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV > 55 & gamma_CL > 0.3 && gamma_PT > 2000 & gamma_P > 6000 && abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50 && phi_PT > 1800 && (phi_PT + gamma_PT) > 4000'

basecut += '&& abs(Bs_TRUEID)==531 && abs(phi_MC_MOTHER_ID) == 531 && abs(gamma_MC_MOTHER_ID) == 531 && phi_TRUEID==333 && gamma_TRUEID==22 '

basecut += ' && gamma_ETA>3 && gamma_ETA<4'
basecut = '({})'.format(basecut)
home = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f = TFile(home + '/phiGammaMC.root')
t=f.Get('DTTBs2phig/DecayTree')

TIS = '(gamma_L0Global_TIS && gamma_Hlt1Phys_TIS && gamma_Hlt2Phys_TIS)'
TIS = '1'
aa = '&&'
bin = '(gamma_PT > 3000 && gamma_PT < 4000)'
#bin = '(gamma_PT > 5000 && gamma_PT < 8000)'
#bin = '(gamma_PT > 8000)'
#Bin = '1'
#bin = 'gamma_PT > 4000'
num = t.GetEntries('{0} && {1} && {2} && Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'.format(basecut,bin,TIS))
num = t.GetEntries('{0} && {1} && {2} && (gamma_L0ElectronDecision_TOS || gamma_L0PhotonDecision_TOS)'.format(basecut,bin,TIS))
den = t.GetEntries('{0} && {1} && {2}'.format(basecut,bin,TIS))

print('Eff: {0} +- {1} %'.format(100.*num/den,100.*get_error(1.*num/den,den)))
