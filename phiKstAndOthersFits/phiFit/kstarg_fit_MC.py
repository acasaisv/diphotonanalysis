import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot
import numpy as np

#Bs mass window
m1g,m2g = 4900,5600
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('Bs_M',(m1g,m2g))
obs = BsMassRange 

# root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
# f_s = uproot.open(root + '/phiGammaMC.root')

root = '/scratch47/adrian.casais/ntuples/turcal'
f_s = uproot.open(root + '/b02kstargamma.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

variables += [
              'B0_M',
              'B0_TRUEID',
              # 'Bs_MM',
              'B0_PT',
              'gamma_CL',
              'gamma_PT',
              'gamma_P',
              'gamma_MC_MOTHER_ID',
              'B0_L0ElectronDecision_TOS',
              'B0_L0PhotonDecision_TOS',
              'B0_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              #'B0_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
              ]


L0 = '(B0_L0ElectronDecision_TOS | B0_L0PhotonDecision_TOS)'
L1 = 'B0_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = '1'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
mc ='abs(gamma_MC_MOTHER_ID)==511 and abs(B0_TRUEID)==511'

df = t_s.pandas.df(branches=variables)
#df = df.query(mc)
#df = df.query(trigger)
df = df.query('B0_M > {0} & B0_M < {1} '.format(m1g,m2g))
df = df.query('(B0_PT > 2000  & gamma_CL > 0.3 & gamma_PT > 1000 & gamma_P > 6000 )')
df = df.query('gamma_PT > 5000 & gamma_PT < 8000')
#######################

#B_s0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.6,1.01)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',25.,10.,150.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',5366.9,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',6.6,0.001,10.)
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',1.5,-1,250.)

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-7,-10,-.001)
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',2.,0.5,250.)
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',len(df)/2,0,len(df))


BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=BsMassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=BsMassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
BsCB = BsCB
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])
#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)

f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs = zfit.pdf.SumPDF(pdfs=[BsCB,BsBkg],fracs=f_Bs)
#model_Bs=BsCB



#PROD PDFs
nSigSig= zfit.Parameter('nSigSig',10000,0,len(df))
signal = BsCB
signalExtended=signal.create_extended(nSigSig)

nBkgSig= zfit.Parameter('nBkgSig',10000,0,len(df))
bkgsignal = BsBkg
bkgSignalExtended=bkgsignal.create_extended(nBkgSig)

model = zfit.pdf.SumPDF(pdfs = [signalExtended,
                                bkgSignalExtended,
                                ])
               
df_masses = df['B0_M']
# df_masses = df_masses.dropna()
data = zfit.Data.from_pandas(df_masses,obs=obs)
#CREATE LOSS FUNCTION

#nll = zfit.loss.UnbinnedNLL(model=[model_Bs,PhiCB],data=[data_Bs,data_Phi])
nll = zfit.loss.ExtendedUnbinnedNLL(model =model,data=data)
#nll= zfit.loss.UnbinnedNLL(model=model,data=data)

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.error()
print(result.params)
#result.
#PLOT
plot= True
#     plt.show()

if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=40
    nentries=len(df)
    xBs = np.linspace(m1g,m2g,1000)
    countsBs, bin_edgesBs = np.histogram(df['B0_M'], nbins, range=(m1g, m2g))
    
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2g-m1g)/nbins*zfit.run(
        (nSigSig)*BsCB.pdf(xBs)+
        (nBkgSig)*BsBkg.pdf(xBs)
        )
    yBsSig = (m2g-m1g)/nbins*zfit.run(
        (nSigSig)*BsCB.pdf(xBs)
        )
    yBsBkg = (m2g-m1g)/nbins*zfit.run(
        (nBkgSig)*BsBkg.pdf(xBs)
        )
    
    
    fig,ax = plt.subplots(1)
    #ax[0].set_ylim([0,250e3])
    ax.errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax.plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax.plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax.plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax.legend()
    
    
    
    

    plt.show()
