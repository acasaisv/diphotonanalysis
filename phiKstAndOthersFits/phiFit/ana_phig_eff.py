from ROOT import *
from get_binomial import *
gROOT.ProcessLine(".x /afs/cern.ch/lhcb/software/releases/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

f1 = TFile("/eos/lhcb/user/j/jcidvida//ntuples/MC2016_Sim09b_Bs2phig.root")
#t1=f1.Get("DTTExoticaGG/DecayTree")
t1=f1.Get("DTTBs2phig/DecayTree")
m1,m2 = 5000,5700
mass = "Bs_M>"+str(m1)+" && Bs_M<"+str(m2)

trigger = "(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS) && (Bs_Hlt1TrackMVADecision_TOS) && Bs_Hlt2RadiativeBs2PhiGammaDecision_TOS"

tots = pickleLoad("/afs/cern.ch/user/j/jcidvida/cmtuser/Exotica/NewIdeas/gamma/tuples/MC_tot_ents.pickle")["Bs2phig"]["tot"]


def get_eff(a,b,opt=0):
    if opt: a1,b1 = a,b
    else: a1,b1 = get_binomial(a,b)
    v,ev = map(lambda x: round(x*100,2),[a1,b1])
    return "%.2f +- %.2f" %(v,ev)


print "*************\nSTRIP EFF"
print get_eff(t1.GetEntries(mass),tots)
print "*************\nTRIG EFF"
print get_eff(t1.GetEntries(trigger+aa+mass),t1.GetEntries(mass))
print "*************\nTOT EFF"
print get_eff(t1.GetEntries(trigger+aa+mass),tots)

#*************
#STRIP EFF
#6.71 +- 0.02
#*************
#TRIG EFF
#33.74 +- 0.11
#*************
#TOT EFF
#2.26 +- 0.01
