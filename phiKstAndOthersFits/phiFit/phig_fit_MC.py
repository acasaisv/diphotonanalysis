import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot
import numpy as np
from ROOT import *
f=TFile('/scratch47/adrian.casais/ntuples/turcal/b2phigammaMC.root')
t = f.Get('DecayTree/DecayTree')

#Bs mass window
m1g,m2g = 5000,5700
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('Bs_M',(m1g,m2g))
PhiMassRange = zfit.Space('phi_M',(m1k,m2k))
obs = BsMassRange * PhiMassRange

# root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
# f_s = uproot.open(root + '/phiGammaMC.root')

root = '/scratch04/adrian.casais/ntuples'
root = '/scratch47/adrian.casais/ntuples/turcal'
f_s = uproot.open(root + '/b2phigammaMC.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))
variables += ['gamma_MC_MOTHER_ID',
              'gamma_TRUEID',
              'phi_1020_TRUEID',
              'Kplus_MC_MOTHER_ID',
              'Kminus_MC_MOTHER_ID',
              'Kminus_MC_MOTHER_ID',
              'Kplus_TRUEID',
              'Kminus_TRUEID',
              'B_s0_M',
              'B_s0_MM',
              'B_s0_PT',
              #'B_s0_ENDVERTEX_X',
              #'B_s0_ENDVERTEX_Y',
              #'B_s0_ENDVERTEX_Z',
              'phi_1020_M',
              'phi_1020_MM',
              'phi_1020_OWNPV_CHI2',
              'phi_1020_PT',
              'Kplus_IPCHI2_OWNPV',
              'Kminus_IPCHI2_OWNPV',
              'Kminus_PT',
              'Kplus_PT',
              'Kminus_PIDK',
              'Kplus_PIDK',
              'gamma_CL',
              'gamma_PT',
              'gamma_P',
              'B_s0_L0ElectronDecision_TOS',
              'B_s0_L0PhotonDecision_TOS',
              'B_s0_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_s0_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS']

    
L0 = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS)'
L1 = 'B_s0_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = 'B_s0_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
mc ='abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and phi_1020_TRUEID==333 and Kplus_TRUEID == 321 and Kminus_TRUEID==-321'

df = t_s.pandas.df(branches=variables)
df = df.query(mc)
#df = df.query(trigger)
df = df.query('B_s0_M > {0} & B_s0_M < {1} and phi_1020_M > {2} and phi_1020_M < {3}'.format(m1g,m2g,m1k,m2k))
df = df.query('(B_s0_PT > 2000  & (Kminus_PT>500 & Kplus_PT > 500) & (Kminus_PIDK > 5 & Kplus_PIDK >5) & (Kplus_IPCHI2_OWNPV > 55 & Kminus_IPCHI2_OWNPV >55) & gamma_CL > 0.3 & gamma_PT > 1000 & gamma_P > 6000 & abs(phi_1020_M-1019.46)<100 & phi_1020_OWNPV_CHI2 < 50) & phi_1020_PT > 1800 & (phi_1020_PT + gamma_PT) >4000')
#df = df.query('gamma_PT > 5000 & gamma_PT < 8000')
#######################

def CB1(x,mu,sigma,alpha,n):
    
    t = (x-mu)/sigma
    if alpha<0: t=-t
    
    absAlpha = abs(alpha)
    c = n/absAlpha/(n-1)*np.exp(-0.5*absAlpha**2)
    d = np.sqrt(np.pi/2)*(1+scipy.special.erf(absAlpha/np.sqrt(2)))
    N = 1/(sigma*(c+d))
    if t >= -absAlpha:
        return N*np.exp(-0.5*t**2);
    
    else: 
        a =  (n/absAlpha)**n*np.exp(-0.5*absAlpha**2);
        b= n/absAlpha - absAlpha;
        
        return N*a/(b - t)**n

x = np.linspace(-10,10,1000)
y = [CB1(i,0,10,3,10) for i in x]
#plt.plot(x,y)
#plt.show()

#B_s0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.6,1.01)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',25.,10.,150.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',5366.9,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',6.6,0.001,10.)
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',1.5,-1,250.)

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-7,-10,-.001)
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',2.,0.5,250.)
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',len(df)/2,0,len(df))


BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=BsMassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=BsMassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
BsCB = BsCB
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])
#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)

f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs = zfit.pdf.SumPDF(pdfs=[BsCB,BsBkg],fracs=f_Bs)
#model_Bs=BsCB


#################

#Phi

#Signal: Double Crystall Ball
PhiParameters = {}
name_prefix='Phi_'
PhiParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.98,1.01)
PhiParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',2.2,1.,5.)
#PhiParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
PhiParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',1019.46,floating=False)
PhiParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[PhiParameters['scale_m'] ,PhiParameters['m_PDG']])
PhiParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',.7,.1,5.)
PhiParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',5.,0.8,150.)

PhiParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-.43,-6,-.1)
PhiParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',7.4,0.8,150.)

PhiParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',len(df)/2,0,len(df))

PhiCBu = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_u'],
                          n=PhiParameters['n_u'],
                          obs=PhiMassRange)

PhiCBd = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_d'],
                          n=PhiParameters['n_d'],
                          obs=PhiMassRange)
fcb_phi = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
PhiCB = zfit.pdf.SumPDF(pdfs=[PhiCBu,PhiCBd],fracs=fcb_phi)
PhiCB = PhiCB
PhiCBExtended = PhiCB.create_extended(PhiParameters['nSig'])

model = zfit.pdf.ProductPDF(pdfs=[model_Bs,PhiCB])

#PROD PDFs
nSigSig= zfit.Parameter('nSigSig',10000,0,len(df))
signal = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiCB])
signalExtended=signal.create_extended(nSigSig)

# nSigBkg= zfit.Parameter('nSigBkg',10000,0,len(df))
# signalbkg = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiBkgComb])
# signalBkgExtended=signalbkg.create_extended(nSigBkg)

nBkgSig= zfit.Parameter('nBkgSig',10000,0,len(df))
bkgsignal = zfit.pdf.ProductPDF(pdfs=[BsBkg,PhiCB])
bkgSignalExtended=bkgsignal.create_extended(nBkgSig)

# nBkgBkg= zfit.Parameter('nBkgBkg',10000,0,len(df))
# bkg = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiBkgComb])
# bkgExtended=bkg.create_extended(nBkgBkg)

model = zfit.pdf.SumPDF(pdfs = [signalExtended,
                                #signalBkgExtended,
                                bkgSignalExtended,
                                #bkgExtended
                                ])
               
df_masses = df[['B_s0_M','phi_1020_M']]
# df_masses = df_masses.dropna()
data = zfit.Data.from_pandas(df_masses,obs=obs)
data_Bs = zfit.Data.from_numpy(array=df['B_s0_M'].values,obs=BsMassRange)
data_Phi = zfit.Data.from_numpy(array=df['phi_1020_M'].values,obs=PhiMassRange)
#CREATE LOSS FUNCTION

#nll = zfit.loss.UnbinnedNLL(model=[model_Bs,PhiCB],data=[data_Bs,data_Phi])
nll = zfit.loss.ExtendedUnbinnedNLL(model =model,data=data)
#nll= zfit.loss.UnbinnedNLL(model=model,data=data)

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.error()
print(result.params)
#result.
#PLOT
plot= True
#     plt.show()

if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=40
    nentries=len(df)
    xBs = np.linspace(m1g,m2g,1000)
    xPhi=np.linspace(m1k,m2k,1000)
    countsBs, bin_edgesBs = np.histogram(df['B_s0_M'], nbins, range=(m1g, m2g))
    countsPhi, bin_edgesPhi = np.histogram(df['phi_1020_M'], nbins, range=(m1k, m2k))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2g-m1g)/nbins*zfit.run(
        (nSigSig)*BsCB.pdf(xBs)+
        (nBkgSig)*BsBkg.pdf(xBs)
        )
    yBsSig = (m2g-m1g)/nbins*zfit.run(
        (nSigSig)*BsCB.pdf(xBs)
        )
    yBsBkg = (m2g-m1g)/nbins*zfit.run(
        (nBkgSig)*BsBkg.pdf(xBs)
        )
    
    errPhi = np.sqrt(countsPhi)
    yPhi =(m2k-m1k)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi) 
        #+ (nSigBkg)*PhiBkgComb.pdf(xPhi)
        )
    yPhiSig =(m2k-m1k)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi)
        )
    
    fig,ax = plt.subplots(2)
    #ax[0].set_ylim([0,250e3])
    ax[0].errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax[0].plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax[0].plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax[0].plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax[0].legend()
    
    ax[1].errorbar(bin_centresPhi, countsPhi, yerr=errPhi, fmt='o', color='xkcd:black')
    #ax[1].set_yscale('log')
    ax[1].plot(xPhi,yPhi,'-',linewidth=2,color='blue')
    ax[1].plot(xPhi,yPhiSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    #ax[1].plot(xPhi,yPhiBkgComb,'--',linewidth=1,color='orange',label='Combinatorial background')
    #ax[1].plot(xPhi,yPhiBkgNonRes,'--',linewidth=1,color='green',label='Non resonant B meson')
    ax[1].legend()
    
    

    plt.show()
