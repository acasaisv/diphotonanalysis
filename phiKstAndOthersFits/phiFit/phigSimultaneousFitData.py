import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot
import numpy as np

#Bs mass window
m1g,m2g = 5000,5700
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('Bs_MM',(m1g,m2g))
PhiMassRange = zfit.Space('phi_MM',(m1k,m2k))
obs = BsMassRange * PhiMassRange

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f_s = uproot.open(root + '/phiGammaData_postCalib.root')
#f_s = uproot.open(root + '/phiGammaData.root')
t_s = f_s['DTTBs2phig/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

for par in ['gamma','phi']:
    variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
    variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))
variables += [#'gamma_MC_MOTHER_ID',
              #'gamma_TRUEID',
              #'phi_TRUEID',
              #'kplus_MC_MOTHER_ID',
              #'kminus_MC_MOTHER_ID',
              #'kminus_MC_MOTHER_ID',
              #'kplus_TRUEID',
              #'kminus_TRUEID',
              'Bs_M',
              'Bs_MM',
              'Bs_PT',
              #'Bs_ENDVERTEX_X',
              #'Bs_ENDVERTEX_Y',
              #'Bs_ENDVERTEX_Z',
              'phi_M',
              'phi_MM',
              'phi_OWNPV_CHI2',
              'kplus_IPCHI2_OWNPV',
              'kminus_IPCHI2_OWNPV',
              'kminus_PT',
              'kplus_PT',
              'kminus_PIDK',
              'kplus_PIDK',
              'gamma_CL',
              'Bs_L0ElectronDecision_TOS',
              'Bs_L0PhotonDecision_TOS',
              'Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'Bs_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS',
              'gamma_L0Global_TIS',
              'gamma_Hlt1Phys_TIS',
              'gamma_Hlt2Phys_TIS',
              'Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS']


L0 = '(Bs_L0ElectronDecision_TOS | Bs_L0PhotonDecision_TOS)'
L1 = 'Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = 'Bs_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
efficiency = False
if efficiency:
    trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS & gamma_Hlt2Phys_TIS"
    #trigger += " & not Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS"
    #trigger += " & not (Bs_L0ElectronDecision_TOS | Bs_L0PhotonDecision_TOS)"
#mc ='gamma_MC_MOTHER_ID==531  and  gamma_TRUEID==22 and phi_TRUEID==333 and kplus_TRUEID == 321 and kminus_TRUEID==-321'

df = t_s.pandas.df(branches=variables)
#df = df.query(mc)
df = df.query(trigger)
df = df.query('Bs_MM > {0} & Bs_MM < {1} and phi_MM > {2} and phi_MM < {3}'.format(m1g,m2g,m1k,m2k))
df = df.query('(Bs_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.25 & gamma_PT > 1000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50)')

#bin  ='gamma_PT > 0  & gamma_PT < 4000'
#bin = 'gamma_PT > 4000'
#odf = df.query(bin)

#B_s0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.01)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',80.45,10.,150.,
                                       floating=True)

BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',5366.9,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',2.66,
                                     #0.01,
                                     #5,
                                     floating=False)
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',0.26,
                                     #1,
                                     #10,
                                     floating=False)

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.65,
                                     #-5,
                                     #-0.01,
                                     floating=False)
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',20,
                                     #1,
                                     #10,
                                     floating=False)
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=BsMassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=BsMassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-3,-1,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])



#################

#Phi

#Signal: Double Crystall Ball
PhiParameters = {}
name_prefix='Phi_'
PhiParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.98,1.01)
PhiParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',2.164,1.,10.,
                                        floating=True)

PhiParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',1019.46,floating=False)
PhiParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[PhiParameters['scale_m'] ,PhiParameters['m_PDG']])
PhiParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',0.63,
                                      #0,
                                      #5.,
                                      floating=False)
PhiParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',20,
                                      #1,
                                      #50.
                                      floating=False)

PhiParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.6,
                                      #-10,
                                      #-0.01,
                                      floating=False)
PhiParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',3.23,
                                      #1.,
                                      #50.,
                                      floating=False)

PhiParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
PhiParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))
PhiParameters['nBkgNonRes'] = zfit.Parameter(name_prefix+'nBkgNonRes',50000,0,len(df))

PhiParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1)
PhiParameters['fBkgComb'] = zfit.Parameter(name_prefix+'fBkgComb',0.5,0,1)


PhiCBu = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_u'],
                          n=PhiParameters['n_u'],
                          obs=PhiMassRange)

PhiCBd = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_d'],
                          n=PhiParameters['n_d'],
                          obs=PhiMassRange)
fcb_phi = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
PhiCB = zfit.pdf.SumPDF(pdfs=[PhiCBu,PhiCBd],fracs=fcb_phi)
PhiCBExtended = PhiCB.create_extended(PhiParameters['nSig'])

lambda_Phi = zfit.Parameter(name_prefix+'lambda',-3e-3,-1,1)
PhiBkgComb = zfit.pdf.Exponential(lambda_Phi,PhiMassRange)
PhiBkgCombExtended = PhiBkgComb.create_extended(PhiParameters['nBkgComb'])


lambda_PhiNonRes = zfit.Parameter(name_prefix+'lambdaNonRes',1.5e-2,-1,1)
PhiBkgNonRes = zfit.pdf.Exponential(lambda_PhiNonRes,PhiMassRange)
PhiBkgNonResExtended = PhiBkgNonRes.create_extended(PhiParameters['nBkgNonRes'])


#modelPhi = zfit.pdf.SumPDF(pdfs=[PhiCB,PhiBkg])

# PhiCB= zfit.pdf.DoubleCB(PhiParameters['m'],
#                          PhiParameters['sigma'],
#                          PhiParameters['a_u'],
#                          PhiParameters['n_u'],
#                          PhiParameters['a_d'],
#                          PhiParameters['n_d'],
#                          PhiMassRange)
    

#####PROD PDFS

## signal: Bs and Phi

#signalExtended = zfit.pdf.ProductPDF(pdfs=[BsCBExtended,PhiCBExtended])
signal = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiCB])
nsigBsPhi= zfit.Parameter('nSigSig',10000,0,len(df))
signalExtended=signal.create_extended(nsigBsPhi)

##double combinatorial
#doubleCombExtended = zfit.pdf.ProductPDF(pdfs=[BsBkgCombExtended,PhiBkgExtended])
doubleComb = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiBkgComb])
nbkgBsPhi= zfit.Parameter('nBkgBkg',10000,0,len(df))
doubleCombExtended = doubleComb.create_extended(nbkgBsPhi)

## combinatorial and real Phi
#combRealPhiExtended = zfit.pdf.ProductPDF(pdfs=[BsBkgCombExtended,PhiCBExtended])
combRealPhi = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiCB])
nbkgsigBsPhi= zfit.Parameter('nBkgSig',10000,0,len(df))
combRealPhiExtended = combRealPhi.create_extended(nbkgsigBsPhi)

## Bs with non resonant KK
#BsNonResKKExtended = zfit.pdf.ProductPDF(pdfs=[BsCBExtended,PhiBkgExtended])
BsNonResKK = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiBkgNonRes])
nsigbkgBsPhi= zfit.Parameter('nSigBkg',10000,0,len(df))
BsNonResKKExtended = BsNonResKK.create_extended(nsigbkgBsPhi)

model = zfit.pdf.SumPDF(pdfs=[signalExtended,doubleCombExtended,combRealPhiExtended,BsNonResKKExtended])
##############################################################################

#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
modelPhi = zfit.pdf.SumPDF(pdfs=[PhiCBExtended,PhiBkgCombExtended,PhiBkgNonResExtended])

#NonExtended
#modelBs = zfit.pdf.SumPDF(pdfs=[BsCB,BsBkgComb],fracs=BsParameters['fSig'])

#modelPhi = zfit.pdf.SumPDF(pdfs=[PhiCB,PhiBkgComb,PhiBkgNonRes],
#                           fracs=[PhiParameters['fSig'],PhiParameters['fBkgComb']])
####               
df_masses = df[['Bs_MM','phi_MM']]
# df_masses = df_masses.dropna()
data = zfit.Data.from_pandas(df_masses,obs=obs)
data_Bs = zfit.Data.from_numpy(array=df['Bs_MM'].values,obs=BsMassRange)
data_Phi = zfit.Data.from_numpy(array=df['phi_MM'].values,obs=PhiMassRange)
#CREATE LOSS FUNCTION

#Extended
#nll = zfit.loss.ExtendedUnbinnedNLL(model=[modelBs,modelPhi],data=[data_Bs,data_Phi])
nll = zfit.loss.ExtendedUnbinnedNLL(model=model,data=data)

#NonExtended
#nll = zfit.loss.UnbinnedNLL(model=[modelBs,modelPhi],data=[data_Bs,data_Phi])

minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)
#PLOT
plot= True
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=100
    nentries=len(df)
    xBs = np.linspace(m1g,m2g,1000)
    xPhi=np.linspace(m1k,m2k,1000)
    countsBs, bin_edgesBs = np.histogram(df['Bs_MM'], nbins, range=(m1g, m2g))
    countsPhi, bin_edgesPhi = np.histogram(df['phi_MM'], nbins, range=(m1k, m2k))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2g-m1g)/nbins*zfit.run(
        (nsigBsPhi+nsigbkgBsPhi)*BsCBExtended.pdf(xBs)
        
        +(nbkgBsPhi+nbkgsigBsPhi)*BsBkgCombExtended.pdf(xBs)
        )
    yBsSig = (m2g-m1g)/nbins*zfit.run(
        (nsigBsPhi+nsigbkgBsPhi)*BsCBExtended.pdf(xBs)
        )
    yBsBkg = (m2g-m1g)/nbins*zfit.run(
        (nbkgBsPhi+nbkgsigBsPhi)*BsBkgCombExtended.pdf(xBs)
        )
    
    errPhi = np.sqrt(countsPhi)
    yPhi =(m2k-m1k)/nbins*zfit.run(
        (nsigBsPhi+nbkgsigBsPhi)*PhiCBExtended.pdf(xPhi)
        + nbkgBsPhi*PhiBkgCombExtended.pdf(xPhi)
        + nsigbkgBsPhi*PhiBkgNonResExtended.pdf(xPhi)
        )
    yPhiSig =(m2k-m1k)/nbins*zfit.run(
        (nsigBsPhi+nbkgsigBsPhi)*PhiCBExtended.pdf(xPhi)
     )
    yPhiBkgComb =(m2k-m1k)/nbins*zfit.run(
        nbkgBsPhi*PhiBkgCombExtended.pdf(xPhi)
        )
    yPhiBkgNonRes =(m2k-m1k)/nbins*zfit.run(
        nsigbkgBsPhi*PhiBkgNonResExtended.pdf(xPhi)
        )
    
    fig,ax = plt.subplots(2)
    #ax[0].set_ylim([50,450])
    ax[0].errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax[0].plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax[0].plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Bs mass')
    ax[0].plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax[0].legend()
    
    ax[1].errorbar(bin_centresPhi, countsPhi, yerr=errPhi, fmt='o', color='xkcd:black')
    #ax[1].set_yscale('log')
    ax[1].plot(xPhi,yPhi,'-',linewidth=2,color='blue')
    ax[1].plot(xPhi,yPhiSig,'--',linewidth=1,color='red',label='Phi mass')
    ax[1].plot(xPhi,yPhiBkgComb,'--',linewidth=1,color='orange',label='Combinatorial background')
    ax[1].plot(xPhi,yPhiBkgNonRes,'--',linewidth=1,color='green',label='Non resonant B meson')
    ax[1].legend()
    
    

    plt.show()


#TOS: 1662 +- 110
#!TOS: 1037 +- 230

#L0 TOS 1854 +- 130
#L0 !TOS 1113 +- 200

#L0 TOS > 3000 MeV 1829 +- 130
#L0 !TOS > 3000 MeV 557 +- 210

#L0 TOS > 2000 MeV 1854 +- 130

#L0 !TOS > 3000 MeV 557 +- 210


s = 1854
b= 1813
s_err = 130
b_err = 210
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
print('HLT1 eff (MC): 0.4944 +- 0.0012')
