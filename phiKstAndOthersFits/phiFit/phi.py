from ROOT import *
from get_binomial import *
aa = "&&"
gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f1 = TFile(root+"/phiGammaMC.root")
#t1=f1.Get("DTTExoticaGG/DecayTree")
t1=f1.Get("DTTBs2phig/DecayTree")
gSystem.Load("libRooFit")
gROOT.ProcessLine(".L $URANIAROOT/src/RooIpatia2.cxx")


m1g,m2g = 5000.,5700.
stripg = "Bs_MM>"+str(m1g)+" && Bs_MM<"+str(m2g)

m1k,m2k = 1006.,1034.
stripk = "phi_MM>"+str(m1k)+" && phi_MM<"+str(m2k)

trigger = "(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS) && (Bs_Hlt1TrackMVADecision_TOS) && Bs_Hlt2RadiativeBs2PhiGammaDecision_TOS"
mcid = "gamma_TRUEID==22&&abs(gamma_MC_MOTHER_ID)==531&&kplus_TRUEID==321&&kminus_TRUEID==-321&&kplus_MC_MOTHER_KEY==kminus_MC_MOTHER_KEY&&kplus_MC_MOTHER_ID==333&&abs(kplus_MC_GD_MOTHER_ID)==531"
#mcid ='1'
#ff = TFile("/tmp/jcidvida/borrame_ho_mc.root","recreate")
ff = TFile("/tmp/adrian.casais/borrame_ho_mc.root",'RECREATE')
t = t1.CopyTree(trigger+aa+stripg+aa+stripk+aa+mcid)
#
#t = ff.Get("DecayTree")

mvar1 = "Bs_MM"
mvar2 = "phi_MM"

mass1 = RooRealVar(mvar1,"m_{#gamma#gamma}",m1g,m2g,"MeV/c^{2}")
mass2 = RooRealVar(mvar2,"m_{KK}",m1k,m2k,"MeV/c^{2}")
data = RooDataSet("data","data", t, RooArgSet(mass2))

#####
#PHI
gscale_m = RooRealVar('gscale_m','gscale_m',1,.99,1.01)
cb_mPDG1 = RooRealVar('cb_mpdg1','cb_mpdg1',1019.46)
cb_m1 = RooFormulaVar("cb_m1","cb_m1",'cb_mpdg1*gscale_m',RooArgList(cb_mPDG1, gscale_m))


s_d1 = RooRealVar("s_d1","s_d1",0)#,-1,1)
a11 = RooRealVar("a11","a11" , .7,.1,2)
a21 = RooRealVar("a21","a21",-.43,-2.,-.1)
cb_s11 = RooRealVar("cb_s11","cb_s11", 2.23,1., 5.)
cb_s21 = RooFormulaVar("cb_s21","cb_s21", "cb_s11+s_d1",RooArgList(cb_s11,s_d1))
n11 = RooRealVar("n11","n11",5.,.1,10.)
n21 = RooRealVar("n21","n21",7.43,.1,15.)
CBu1 = RooCBShape("CBu1","CBu1", mass2, cb_m1, cb_s11, a11, n11)
CBd1 = RooCBShape("CBd1","CBd1", mass2, cb_m1, cb_s21, a21, n21)

fcb1 = RooRealVar("fcb1","fcb1",0.5,0,1)
signal1 = RooAddPdf("signal1","signal1",CBu1,CBd1,fcb1)


width1 = RooRealVar("width1","width1",4.266)
sigma1 = RooRealVar("sigma1","sigma1",3.,0.5,8.)
#signal1 = RooVoigtian("signal1","signal1",mass2,cb_m1,width1,sigma1)

###
f1 = RooRealVar("f1","f1",0.5,0,1)
expi1 = RooRealVar("expi1","expi1",1e-4,-1,1)
bkg1 = RooExponential("bkg1","bkg1",mass2,expi1)

model1 = RooAddPdf("model1","model1",signal1,bkg1,f1)
#model1 = signal1
#if not FIX:
result = model1.fitTo(data,RooFit.Save(True))
result.Print()

gStyle.SetOptLogy(1)
frame1 = mass2.frame()
data.plotOn(frame1)
model1.plotOn(frame1)
model1.plotOn(frame1,RooFit.Components("signal1"), RooFit.LineStyle(kDashed))
model1.plotOn(frame1,RooFit.Components("bkg1"), RooFit.LineStyle(kDashed),RooFit.LineColor(kRed))
#frame.GetYaxis().SetRangeUser(1,3e6)
frame1.Draw()

