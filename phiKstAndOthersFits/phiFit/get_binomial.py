from math import sqrt

def get_binomial(a,b):
    p=a/float(b)
    ep=sqrt((p*(1-p))/float(b))
    return p,ep
