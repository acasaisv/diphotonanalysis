from ROOT import *
from get_binomial import *
gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
aa="&&"

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f1 = TFile(root+"/phiGammaMC.root")

# root = '/scratch27/adrian.casais/ntuples/DiPhotons'
# f1 = TFile(root+'Data2016_S28_Radiative_withConv.root')

t1=f1.Get("DTTBs2phig/DecayTree")
gSystem.Load("libRooFit")
#gROOT.ProcessLine(".L $URANIAROOT/src/RooIpatia2.cxx")


m1g,m2g = 5000,5700
stripg = "Bs_MM>"+str(m1g)+" && Bs_MM<"+str(m2g)

m1k,m2k = 1006,1034
stripk = "phi_MM>"+str(m1k)+" && phi_MM<"+str(m2k)

trigger = "(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS) && (Bs_Hlt1TrackMVADecision_TOS) && Bs_Hlt2RadiativeBs2PhiGammaDecision_TOS"
mcid = "gamma_TRUEID==22&&abs(gamma_MC_MOTHER_ID)==531&&kplus_TRUEID==321&&kminus_TRUEID==-321&&kplus_MC_MOTHER_KEY==kminus_MC_MOTHER_KEY&&kplus_MC_MOTHER_ID==333&&abs(kplus_MC_GD_MOTHER_ID)==531"

res = True
kinematic = '1'
#mcid= '1'
if res:
    L0 = '(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS)'
    L1 = 'Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'
    L2 = 'Bs_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
    trigger = '({0} && {1} && {2})'.format(L0,L1,L2)
    kinematic = '(Bs_PT > 2000  && (kminus_PT>500 && kplus_PT > 500) && (kminus_PIDK > 5 && kplus_PIDK >5) && (kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV >55) && gamma_CL > 0.25 & gamma_PT > 1000 && gamma_P > 6000 && abs(phi_MM-1019.46)<100 & phi_OWNPV_CHI2 < 50)'
        
ff = TFile("/tmp/adrian.casais/merda.root","recreate")
t = t1.CopyTree(trigger+aa+stripg+aa+stripk+aa+mcid+aa+kinematic)
#ff = TFile("/tmp/jcidvida/borrame_ho_mc.root")
#t = ff.Get("DecayTree")

mvar1 = "Bs_MM"
mvar2 = "phi_MM"

mass1 = RooRealVar(mvar1,"m_{#gamma#gamma}",m1g,m2g,"MeV/c^{2}")
mass2 = RooRealVar(mvar2,"m_{KK}",m1k,m2k,"MeV/c^{2}")
data = RooDataSet("data","data", t, RooArgSet(mass1,mass2))

## BS
gscale_m = RooRealVar("gscale_m","gscale_m",1.,0.60,1.01)
cb_s1 = RooRealVar("cb_s1","cb_s1", 25.,10., 150.)
s_d = RooRealVar("s_d","s_d",0)
cb_s2 = RooFormulaVar("cb_s2","cb_s2", "cb_s1+s_d",RooArgList(cb_s1,s_d))
cb_mPDG = RooRealVar('cb_mpdg','cb_mpdg',5366.9)
cb_m = RooFormulaVar("cb_m","cb_m",'cb_mpdg*gscale_m',RooArgList(cb_mPDG, gscale_m))
a1 = RooRealVar("a1","a1" , 6.6,1.,10.)
n1 = RooRealVar("n1","n1",1.5,1.,10.)
a2 = RooRealVar("a2","a2",-7.,-10.,-.1)
n2 = RooRealVar("n2","n2",2.,1.,20.)

fcb = RooRealVar("fcb","fcb",0.5,0,1)

CBu = RooCBShape("CBu","CBu", mass1, cb_m, cb_s1, a1, n1)
CBd = RooCBShape("CBd","CBd", mass1, cb_m, cb_s2, a2, n2)
signal = RooAddPdf("signal","signal",CBu,CBd,fcb)


###
f = RooRealVar("f","f",0.5,0,1)
expi = RooRealVar("expi","expi",-3e-3,-9e-3,1e-3)
bkg = RooExponential("bkg","bkg",mass1,expi)

#####
#PHI
cb_mPDG1 = RooRealVar('cb_mpdg1','cb_mpdg1',1019.46)
gscale_m1 = RooRealVar("gscale_m1","gscale_m1",1.,0.98,1.01)
cb_m1 = RooFormulaVar("cb_m1","cb_m1",'cb_mpdg1*gscale_m1',RooArgList(cb_mPDG1, gscale_m1))

s_d1 = RooRealVar("s_d1","s_d1",0)
a11 = RooRealVar("a11","a11" , .7,.1,2)
a21 = RooRealVar("a21","a21",-.43,-2.,-.1)
cb_s11 = RooRealVar("cb_s11","cb_s11", 2.23,1., 5.)
cb_s21 = RooFormulaVar("cb_s21","cb_s21", "cb_s11+s_d1",RooArgList(cb_s11,s_d1))
n11 = RooRealVar("n11","n11",5.,.1,50.)
n21 = RooRealVar("n21","n21",7.43,.1,50.)
CBu1 = RooCBShape("CBu1","CBu1", mass2, cb_m1, cb_s11, a11, n11)
CBd1 = RooCBShape("CBd1","CBd1", mass2, cb_m1, cb_s21, a21, n21)

fcb1 = RooRealVar("fcb1","fcb1",0.5,0,1)
signal1 = RooAddPdf("signal1","signal1",CBu1,CBd1,fcb1)

###

FIX = 0
if FIX:
    for el in vals: exec(el+".setVal("+str(vals[el])+")");exec(el+".setConstant()")

model = RooAddPdf("model","model",signal,bkg,f)

modelprod = RooProdPdf("prodpdf","prodpdf",
                       RooArgList(model,signal1))

#if not FIX:
result = modelprod.fitTo(data,RooFit.Save(True))
result.Print()

gStyle.SetOptLogy(1)
c1 = TCanvas();c1.Divide(1,2)
c1.cd(1)
frame = mass1.frame()
data.plotOn(frame)
model.plotOn(frame)
model.plotOn(frame,RooFit.Components("signal"), RooFit.LineStyle(kDashed))
model.plotOn(frame,RooFit.Components("bkg"), RooFit.LineStyle(kDashed),RooFit.LineColor(kRed))
#frame.GetYaxis().SetRangeUser(1,3e6)
frame.Draw()

frame = mass1.frame()

c1.cd(2)
frame1 = mass2.frame()
data.plotOn(frame1)
modelprod.plotOn(frame1)
#frame.GetYaxis().SetRangeUser(1,3e6)
frame1.Draw()

# a1     1.0168e+00 +/-  4.72e+00
# a11    
# a2   -1.8979e+00 +/-  9.27e-04
# a21   
# cb_s1    9.8186e+01 +/-  1.62e-02
# cb_s11    2.1452e+00 +/-  5.14e-04
# expi   -3.7019e-03 +/-  1.25e-05
# f    9.7664e-01 +/-  5.29e-05
# fcb    4.1341e-07 +/-  2.58e-03
# fcb1    4.7857e-01 +/-  2.06e-04
# gscale_m    9.9999e-01 +/-  6.06e-07
# n1    9.9219e+00 +/-  6.63e+00
# n11    
# n2    1.8353e+00 +/-  7.14e-03
# n21    
