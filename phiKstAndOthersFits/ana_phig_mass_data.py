from __future__ import print_function
from ROOT import *
from get_binomial import *
gSystem.Load("libRooFit")


#gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
aa="&&"

root = '/scratch09/adrian.casais/ntuples/Bs2PhiGamma'
root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f1 = TFile(root+"/phiGammaData.root")

#2016 data 
# root = '/scratch27/adrian.casais/ntuples/DiPhotons'
# f1 = TFile(root+'/Data2016_S28_Radiative_withConv.root')



t1=f1.Get("DTTBs2phig/DecayTree")
res = False

# lumi = 0
# for el in tl: lumi+=el.IntegratedLuminosity
# print "lumi=",lumi
#lumi = 1589

m1g,m2g = 5000,5700
stripg = "(Bs_MM>{0} && Bs_MM<{1})".format(m1g,m2g)

m1k,m2k = 1006,1034
stripk = "(phi_MM>{0} && phi_MM<{1})".format(m1k,m2k)

strip = stripg+aa+stripk

trigger = "gamma_L0Global_TIS & gamma_Hlt1Phys_TIS & gamma_Hlt2Phys_TIS"
kinematic = '1'
############# SELECTION FOR MASS RESOLUTION 
if res:
    
    L0 = '(Bs_L0ElectronDecision_TOS || Bs_L0PhotonDecision_TOS)'
    L1 = 'Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS'
    L2 = 'Bs_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
    trigger = '({0} && {1} && {2})'.format(L0,L1,L2)
    #kinematic = '(Bs_PT > 2000  && (kminus_PT>500 && kplus_PT > 500) && (kminus_PIDK > 5 && kplus_PIDK >5) && (kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV >55) && gamma_CL > 0.25 && gamma_PT > 1000 && gamma_P > 6000 && abs(phi_MM-1019.46)<100 & phi_OWNPV_CHI2 < 50)'
    
##############

ff = TFile("/tmp/adrian.casais/massresolution.root","recreate")
t2 = t1.CopyTree(trigger+aa+strip+aa+kinematic)
#t2 = t1.CopyTree('( ({0}) && ({1}) && ({2}) )'.format(trigger,strip,kinematic))
#ff = TFile("/tmp/adrian.casais/massresolution.root")
#ff = TFile("/tmp/adrian.casais/borrame_ho.root")
#t2 = ff.Get("DecayTree")

kinematic = '(Bs_PT > 2000  && (kminus_PT>500 && kplus_PT > 500) && (kminus_PIDK > 5 && kplus_PIDK >5) && (kplus_IPCHI2_OWNPV > 55 && kminus_IPCHI2_OWNPV >55) && gamma_CL > 0.25 && gamma_PT > 1000 && gamma_P > 6000 && abs(phi_MM-1019.46)<100 && phi_OWNPV_CHI2 < 50)'
trig = "Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS"
#trig = '1'
#kinematic = '1'

ffff=TFile('/tmp/adrian.casais/eraseme.root','recreate')
t = t2.CopyTree(kinematic+aa+trig)


mvar1 = "Bs_MM"
mvar2 = "phi_MM"

mass1 = RooRealVar(mvar1,"m_{KK#gamma}",m1g,m2g,"MeV/c^{2}")
mass2 = RooRealVar(mvar2,"m_{KK}",m1k,m2k,"MeV/c^{2}")
data = RooDataSet("data","data", t, RooArgSet(mass1,mass2))

# BS
gscale_m = RooRealVar("gscale_m","gscale_m",9.9757e-01,0.98,1.01)
cb_s1 = RooRealVar("cb_s1","cb_s1", 9.4900e+01,25., 125.)
s_d = RooRealVar("s_d","s_d",0)
cb_s2 = RooFormulaVar("cb_s2","cb_s2", "cb_s1+s_d",RooArgList(cb_s1,s_d))
cb_mPDG = RooRealVar('cb_mpdg','cb_mpdg',5366.9)
cb_m = RooFormulaVar("cb_m","cb_m",'cb_mpdg*gscale_m',RooArgList(cb_mPDG, gscale_m))
a1 = RooRealVar("a1","a1" , 1.0168)
n1 = RooRealVar("n1","n1",9.9219)
a2 = RooRealVar("a2","a2",-1.8979)
n2 = RooRealVar("n2","n2",1.8353)

fcb = RooRealVar("fcb","fcb",0.9,0,1)

CBu = RooCBShape("CBu","CBu", mass1, cb_m, cb_s1, a1, n1)
CBd = RooCBShape("CBd","CBd", mass1, cb_m, cb_s2, a2, n2)
signal = RooAddPdf("signal","signal",CBu,CBd,fcb)

#####
#PHI
gscale_m1 = RooRealVar("gscale_m1","gscale_m1",9.9757e-01,0.98,1.01)
cb_mPDG1 = RooRealVar('cb_mpdg1','cb_mpdg1',1019.46)
cb_m1 = RooFormulaVar("cb_m1","cb_m1",'cb_mpdg1*gscale_m1',RooArgList(cb_mPDG1, gscale_m1))

s_d1 = RooRealVar("s_d1","s_d1",0)
a11 = RooRealVar("a11","a11" , 6.8165e-01)
a21 = RooRealVar("a21","a21",-3.8554e-01)
cb_s11 = RooRealVar("cb_s11","cb_s11", 2.23,1., 5.)
cb_s21 = RooFormulaVar("cb_s21","cb_s21", "cb_s11+s_d1",RooArgList(cb_s11,s_d1))
n11 = RooRealVar("n11","n11",4.0345e+01)
n21 = RooRealVar("n21","n21",4.6874e+01)
CBu1 = RooCBShape("CBu1","CBu1", mass2, cb_m1, cb_s11, a11, n11)
CBd1 = RooCBShape("CBd1","CBd1", mass2, cb_m1, cb_s21, a21, n21)

fcb1 = RooRealVar("fcb1","fcb1",0.5,0,1)
signal1 = RooAddPdf("signal1","signal1",CBu1,CBd1,fcb1)

sigprod = RooProdPdf("signalprod","signalprod",


                     RooArgList(signal,signal1))

## bkg model
expi = RooRealVar("expi","expi",-3e-3,-1,1)
bkg = RooExponential("bkg","bkg",mass1,expi)

expi1 = RooRealVar("expi1","expi1",-3e-3,-1,1)
bkg1 = RooExponential("bkg1","bkg1",mass2,expi1)

## different shape for non resonant Bs->kkgamma
#expi2 = RooRealVar("expi2","expi2",-3e-3,-1,1)
expi2 = RooRealVar("expi2","expi2",1.5e-2)#,0,5e-2)
bkg2 = RooExponential("bkg2","bkg2",mass2,expi2)

## double combinatorial
bkgprod1 = RooProdPdf("bkgprod1","bkgprod1",
                      RooArgList(bkg,bkg1))
## combinatorial with real phi
bkgprod2 = RooProdPdf("bkgprod2","bkgprod2",
                      RooArgList(bkg,signal1))
## Bs with non resonant KK
bkgprod3 = RooProdPdf("bkgprod3","bkgprod3",
                      RooArgList(signal,bkg2))
###
nsig = RooRealVar("nsig","nsig",10000,0,t.GetEntries())
nbkg1 = RooRealVar("nbkg1","nbkg1",50000,0,t.GetEntries())
nbkg2 = RooRealVar("nbkg2","nbkg2",50000,0,t.GetEntries())
nbkg3 = RooRealVar("nbkg3","nbkg3",50000,0,t.GetEntries())

#model = RooAddPdf("model","model",sig,bkg,f)
model =  RooAddPdf("model","model",RooArgList(bkgprod1,bkgprod2,bkgprod3,sigprod),RooArgList(nbkg1,nbkg2,nbkg3,nsig))
#if not FIX:
result = model.fitTo(data,RooFit.Save(True),RooFit.Extended(True))
result.Print()

gStyle.SetOptLogy(1)
c1 = TCanvas();c1.Divide(1,2)
c1.cd(1)
frame = mass1.frame()
data.plotOn(frame)
model.plotOn(frame)
model.plotOn(frame,RooFit.Components("signal"), RooFit.LineStyle(kDashed))
model.plotOn(frame,RooFit.Components("bkg"), RooFit.LineStyle(kDashed),RooFit.LineColor(kRed))
frame.GetYaxis().SetRangeUser(50,70000)
frame.Draw()

c1.cd(2)
frame1 = mass2.frame()
data.plotOn(frame1)
model.plotOn(frame1)
model.plotOn(frame1,RooFit.Components("signal1"), RooFit.LineStyle(kDashed))
model.plotOn(frame1,RooFit.Components("bkg1"), RooFit.LineStyle(kDashed),RooFit.LineColor(kRed))
model.plotOn(frame1,RooFit.Components("bkg2"), RooFit.LineStyle(kDashed),RooFit.LineColor(kGreen))
frame1.GetYaxis().SetRangeUser(1,70000)
frame1.Draw()

print(nsig.getVal())
#denominador: 2957
# con TOS:   1730.88
# con !TOS:  1259.08
#=====================
import math as m
root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
fmc = TFile(root+"/phiGammaMC.root")
tmc = fmc.Get('DTTBs2phig/DecayTree')
truth = "abs(phi_MC_MOTHER_ID)==531 && abs(gamma_MC_MOTHER_ID)==531"
trig = "Bs_Hlt1B2PhiGamma_LTUNBDecision_TOS"
eff_mc = tmc.GetEntries(trig+aa+kinematic+aa+truth)/1./tmc.GetEntries(truth+aa+kinematic)
err_mc = m.sqrt(eff_mc*(1-eff_mc)/tmc.GetEntries(truth+aa+kinematic))
b = 1259.09
b_err = 251
s = 1730.88
s_err = 176
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = m.sqrt(err2)
print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
print('HLT1 eff (MC): {0} +- {1}'.format(eff_mc,err_mc))
# sigma MC: cb_s1    9.9033e+01 +/-  3.53e-01
# sigma DATA: cb_s1    9.9845e+01 +/-  1.30e+01
