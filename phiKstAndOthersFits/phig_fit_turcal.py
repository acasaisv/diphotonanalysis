import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import pandas


efficiency = False
lowpT = 0000
highpT = 100000000000000000
lowETA=0
highETA=10
num = True
hdf = False

looping = False
if looping:
    lowpT =float(sys.argv[1])
    highpT = float(sys.argv[2])
    lowETA = float(sys.argv[3])
    highETA = float(sys.argv[4])
    num = bool(int(sys.argv[5]))


#B mass window
m1g,m2g = 5050,5700
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))
PhiMassRange = zfit.Space('phi_M',(m1k,m2k))
obs = BsMassRange

root = '/scratch47/adrian.casais/ntuples/turcal'
if not hdf:
    f_s = uproot.open(root + '/turcal.root')
    t_s = f_s['Bs2phigamma_Tuple/DecayTree']
    keys = [ i.decode('utf-8') for i in t_s.keys() ]
    variables = []
    vars_bdt = [
            'B_DIRA_OWNPV',
            'phi_OWNPV_CHI2',
            #'phi_PT',
            'kplus_IPCHI2_OWNPV',
            'kminus_IPCHI2_OWNPV',
            #'gamma_1.00_cc_asy_PT',
            #'gamma_1.35_cc_asy_PT',
            #'gamma_1.70_cc_asy_PT',
            #'gamma_1.00_cc_asy_P',
            #'gamma_1.35_cc_asy_P',
            #'gamma_1.70_cc_asy_P',
            #'kminus_PT',
            #'kplus_PT',
            #'kminus_PIDK',
            #'kplus_PIDK',
            
            ]

    
    variables += [
            'B_M',
            'phi_M',
            'B_PT',
            'phi_MM',
            'phi_PT',
            'phi_OWNPV_CHI2',
            'kplus_IPCHI2_OWNPV',
            'kminus_IPCHI2_OWNPV',
            'kminus_PT',
            'kplus_PT',
            'kminus_PIDK',
            'kplus_PIDK',
            'gamma_CL',
            'gamma_P',
            'gamma_PZ',
            'gamma_PT',
            'B_L0ElectronDecision_TOS',
            'B_L0PhotonDecision_TOS',
            'B_L0ElectronDecision_Dec',
            'B_L0PhotonDecision_Dec',
            'B_L0MuonDecision_Dec',
            'B_L0DiMuonDecision_Dec',
            'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
            'B_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS',
            'gamma_L0Global_TIS',
            'gamma_Hlt1Phys_TIS',
            'gamma_Hlt2Phys_TIS',
            'gamma_L0ElectronDecision_TOS',
            'gamma_L0PhotonDecision_TOS',
            'gamma_CaloHypo_isPhoton',
            'nSPDHits'
            ]

    variables = np.unique(vars_bdt+variables)
    df = t_s.pandas.df(branches=variables)
    df.dropna()
    
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    
    
    
    df.to_hdf(root+'/BPhiGammaData.h5',key='df',mode='w')

else:
    df =pandas.read_hdf(root+'/BPhiGammaData.h5',key='df')


L0 = 'not(B_L0ElectronDecision_Dec) & not(B_L0PhotonDecision_Dec) & not(B_L0MuonDecision_Dec) & not(B_L0DiMuonDecision_Dec)'
L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = 'B_Hlt2RadiativeB2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
trigger = '{0} & {1}'.format(L0,L1)
trigger = L0
import pickle
with open('bdts_turcal.pickle','rb') as handle:
    bdts = pickle.load(handle)

mean_bdt=np.zeros(len(df))
for i in range(5):
    mean_bdt += bdts[i].predict_proba(df[vars_bdt])[:,1]/5
    
df['bdt']=mean_bdt

#df = df.query(mc)
df = df.query(trigger)
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))


# df = df.query('(B_PT > 2000   & (kminus_PT>500 & kplus_PT>500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.3 & gamma_PT > 2000 & gamma_P > 5000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50) & phi_PT > 1800 & (phi_PT + gamma_PT) > 4000')

#df = df.query('(B_PT > 2000   & (kminus_PT>500 & kplus_PT>500) & (kminus_PIDK > 3 & kplus_PIDK >3) & (kplus_IPCHI2_OWNPV > 45 & kminus_IPCHI2_OWNPV > 45) & gamma_CL > 0.3 & gamma_PT > 2000 & gamma_P > 5000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50) & phi_PT > 1800 & (phi_PT + gamma_PT) > 3000')

#df.query('gamma_PT > 2500 and bdt > 0.6  & gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)

df.query('gamma_PT > 2500 and bdt > 0.5',inplace=True)


#df = df.query('gamma_PT > 2000 & gamma_PT < 6000')
#df = df.query('gamma_PT > 6000 & gamma_PT < 8000')
#B_s0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.01)

BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',75,50,120)
BsParameters['dSigma'] = zfit.Parameter(name_prefix+'dsigma',0,-20,20,floating=True)
BsParameters['sigmaR'] = zfit.ComposedParameter(name_prefix+'sigmaR',lambda x,y: x+y,params =[BsParameters['sigma'] ,BsParameters['dSigma']])
BsParameters['sigmaL'] = zfit.ComposedParameter(name_prefix+'sigmaL',lambda x,y: x-y,params =[BsParameters['sigma'] ,BsParameters['dSigma']])

#BsParameters['sigma_r'] = zfit.Parameter(name_prefix+'sigma_r',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',5366.9,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_l'] = zfit.Parameter(name_prefix+'a_l',2.285,
                                     0.01,
                                     5,
                                     floating=False)
BsParameters['n_l'] = zfit.Parameter(name_prefix+'n_l',0.82,
                                     0,
                                     150,
                                     floating=False)

BsParameters['a_r'] = zfit.Parameter(name_prefix+'a_r',1.69,
                                     0,
                                     20,
                                     floating=False)
BsParameters['n_r'] = zfit.Parameter(name_prefix+'n_r',3.7,
                                     1,
                                     100,
                                     floating=False)

BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',len(df)/2,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',len(df)/2,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1)


BsCB = zfit.pdf.DoubleCB(mu=BsParameters['m'],
                          sigmar=BsParameters['sigmaR'],
                          sigmal=BsParameters['sigmaL'],
                          alphar=BsParameters['a_r'],
                          nr=BsParameters['n_r'],
                          alphal=BsParameters['a_l'],
                          nl=BsParameters['n_l'],
                          obs=BsMassRange)

BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
lambda_B = zfit.Parameter(name_prefix+'lambda',-3e-3,-1,1)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,-10,10,floating=True)
BsBkgComb = zfit.pdf.Exponential(lambda_B,BsMassRange)

BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])






#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])

####               
df_masses = df[['B_M','phi_M']]
# df_masses = df_masses.dropna()
data = zfit.Data.from_pandas(df['B_M'],obs=obs)
data_B = zfit.Data.from_numpy(array=df['B_M'].values,obs=BsMassRange)
data_Phi = zfit.Data.from_numpy(array=df['phi_M'].values,obs=PhiMassRange)
#CREATE LOSS FUNCTION

#Extended
#nll = zfit.loss.ExtendedUnbinnedNLL(model=modelB,
#                                    data=data_B)
nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,data=data)

#NonExtended
#nll = zfit.loss.UnbinnedNLL(model=[modelB,modelPhi],data=[data_B,data_Phi])

minimizer = zfit.minimize.Minuit(tol=1.e-5)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
#result.errors()
result.hesse()
print(result.params)
#PLOT
plot= True
#     plt.show()

if plot:
    BParamsValues = {}
    for key in BsParameters:
        BParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=40
    nentries=len(df)
    xB = np.linspace(m1g,m2g,1000)
    xPhi=np.linspace(m1k,m2k,1000)
    countsB, bin_edgesB = np.histogram(df['B_M'], nbins, range=(m1g, m2g))
    countsPhi, bin_edgesPhi = np.histogram(df['phi_M'], nbins, range=(m1k, m2k))
    bin_centresB = (bin_edgesB[:-1] + bin_edgesB[1:])/2.
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    errB = np.sqrt(countsB)
    yB =(m2g-m1g)/nbins*zfit.run(
        (BsParameters['nSig'])*BsCB.pdf(xB)+
        (BsParameters['nBkgComb'])*BsBkgComb.pdf(xB)
        )
    yBSig =(m2g-m1g)/nbins*zfit.run(
        (BsParameters['nSig'])*BsCB.pdf(xB))
    yBBkg=(m2g-m1g)/nbins*zfit.run(
        (BsParameters['nBkgComb'])*BsBkgComb.pdf(xB))
  


    
    fig,ax = plt.subplots(1,figsize=(8,6))
    #ax[0].set_ylim([0,250e3])
    ax.errorbar(bin_centresB, countsB, yerr=errB, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    
    ax.plot(xB,yB,'-',linewidth=2,color='blue')
    ax.plot(xB,yBSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax.plot(xB,yBBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax.plot(xB,yB,'-',linewidth=2,color='blue')
    fig.text(0.05,0.95,'Sig. yield = {:.2f}'.format(float(BsParameters['nSig'])))
    ax.legend()

    
    

    plt.show()





