import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
from energy_helpers import *

def smear_photon(df,b=0.0335):
    df['gamma_smearP'] = smear(df['gamma_TRUEP_E'],b=b)
    return df
def smear_mass(df,hadron='phi',b=0.335):
    df = smear_photon(df,b=b)
    for comp in 'X','Y','Z':
        df[f'gamma_P{comp}'] = df['gamma_smearP']/df['gamma_P']*df[f'gamma_P{comp}']
        df[f'B_P{comp}'] = df[f'{hadron}_P{comp}'] + df[f'gamma_P{comp}']
    df['gamma_P'] = df['gamma_smearP']
    df.eval("B_P = sqrt(B_PX**2 + B_PY**2 + B_PZ**2)",inplace=True)
    df.eval("B_PE = gamma_P + phi_PE",inplace=True)
    df.eval("B_M = sqrt(B_PE**2 - B_P**2)",inplace=True)
    return df
    

#Bs mass window
m1g,m2g = 5000,5800
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))

obs = BsMassRange


root = '/scratch04/adrian.casais/ntuples'
root = '/scratch47/adrian.casais/ntuples/turcal'

f_s = uproot.open(root + '/b02phigammaMC-0smear.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))
variables += ['gamma_MC_MOTHER_ID',
              'gamma_TRUEID',
              'phi_TRUEID',
              'gamma_TRUEP_E',
              'kplus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kplus_TRUEID',
              'kminus_TRUEID',
              'B_M',
              'B_MM',
              'B_PT',
              'B_PE',
              'phi_PE',
              #'B_ENDVERTEX_X',
              #'B_ENDVERTEX_Y',
              #'B_ENDVERTEX_Z',
              'phi_M',
              'phi_MM',
              'phi_OWNPV_CHI2',
              'phi_PT',
              'phi_PX', 'phi_PY', 'phi_PZ',
              'kplus_IPCHI2_OWNPV',
              'kminus_IPCHI2_OWNPV',
              'kminus_PT',
              'kplus_PT',
              'kminus_PIDK',
              'kplus_PIDK',
              'gamma_CL',
              'gamma_PT',
              'gamma_P',
               'gamma_PX', 'gamma_PY', 'gamma_PZ',
              'gamma_PP_IsPhoton',
              'B_L0ElectronDecision_TOS',
              'B_L0PhotonDecision_TOS',
              'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS']

    
L0 = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS)'
L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L2 = 'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
#trigger='B_PT >0'
mc ='abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and phi_TRUEID==333 and kplus_TRUEID == 321 and kminus_TRUEID==-321'

df = t_s.pandas.df(branches=variables)
df = df.query(mc)
#df = df.query(trigger)
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))
df = df.query('B_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV >55) & gamma_CL > 0.3 & gamma_PT > 2500 & gamma_P > 3000 & abs(phi_M-1019.46)<100 & phi_OWNPV_CHI2 < 50 & phi_PT > 1800 & (phi_PT + gamma_PT) >4000')
df = smear_mass(df,b=0.0319)
#df.query('B_PT > 2000 and gamma_PT > 3000 and  gamma_CL > 0.1 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)

#df.query('gamma_PT > 3000',inplace=True)


#Signal: Double Crystall Ball
from fit_helpers import create_double_cb
name_prefix='Bs_'
BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,mass_range=BsMassRange,mass =5366.9,sigma=(88,75,95),nevs=len(df))

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,0,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
nBkgComb = zfit.Parameter(name_prefix + 'nBkg',len(df)/2,0,len(df))
BsBkgCombExtended = BsBkgComb.create_extended(nBkgComb)
f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs=BsCBExtended


data = zfit.Data.from_numpy(array=df['B_M'].to_numpy(),obs=obs)

nll = zfit.loss.ExtendedUnbinnedNLL(model =model_Bs,data=data)
#nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)

minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=1000000)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.error()
print(result)
plot= False
import plot_helpers
nSig=zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=BsCBExtended,
                      components=[BsCBExtended],
                      yields=[nSig],
                      labels=[r'$B_s$ signal double Crystal Ball'],
                      colors=['blue'],
                      nbins=30,
                      myrange=(m1g,m2g),
                      xlabel=r'M($K^+K^-\gamma$)',
                      savefile='/home3/adrian.casais/figstoscp/phig-fit-MC.pdf',
                      plot=False)
import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
params = [BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],BsParameters['a_l'],BsParameters['a_r'],BsParameters['n_l'],BsParameters['n_r']]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':[#$'n_\\textrm{Signal}',
                    #'n_\\textrm{Background}',
                    '$m/m_\\textrm{PDG}$',
                    '$\\sigma$',
                    '$\\Delta(\\sigma)$',
                    '$\\alpha_L$',
                    '$\\alpha_R$',
                    '$n_L$',
                    '$n_R$',]
       ,'Units':['','MeV','MeV','','','',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))

