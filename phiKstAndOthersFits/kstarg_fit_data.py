import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
#from ROOT import *
import pandas
#Bs mass window
m1g,m2g = 5050,5650
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))

obs = BsMassRange

# root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
# f_s = uproot.open(root + '/phiGammaMC.root')
hdf=False



if not hdf:
    root = '/scratch47/adrian.casais/ntuples/turcal'
    #f_s = uproot.open(root + '/b2phigammaMC.root')
    #f_s = uproot.open(root + '/b02kstargamma.root')
    f_s = uproot.open(root + '/b02kstargammaS34r0p1.root')
    t_s = f_s['DecayTree/DecayTree']
    keys = [ i.decode('utf-8') for i in t_s.keys() ]
    variables = []


    variables += [
        'B_DIRA_OWNPV',
        'B_M',
        'B_MM',
        'B_PT',
        'B_ETA',
        'nSPDHits',
        #'B_ENDVERTEX_X',
        #'B_ENDVERTEX_Y',
        #'B_ENDVERTEX_Z',
        'kst_M',
        'kst_MM',
        'kst_OWNPV_CHI2',
        'kst_PT',
        'kplus_IPCHI2_OWNPV',
        'piminus_IPCHI2_OWNPV',
        'piminus_PT',
        'kplus_PT',
        'piminus_PIDK',
        'kplus_PIDK',
        'kplus_ProbNNpi',
        'piminus_ProbNNpi',
        'kplus_ProbNNk',
        'piminus_ProbNNk',
        
        'gamma_CL',
        'gamma_PT',
        'gamma_P',
        'gamma_PP_IsPhoton',
        'gamma_L0ElectronDecision_TOS',
        'gamma_L0PhotonDecision_TOS',
        'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
        'B_Hlt1TrackMVADecision_TOS' ,
        'B_Hlt1TrackMVADecision_Dec',
        'B_Hlt2Bd2KstGammaDecision_TOS',
        'B_Hlt2RadiativeBd2KstGammaDecision_TOS',
        'gamma_1.00_cc_asy_PT',
        'gamma_1.35_cc_asy_PT',
        'gamma_1.70_cc_asy_PT',
        'gamma_1.00_cc_asy_P',
        'gamma_1.35_cc_asy_P',
        'gamma_1.70_cc_asy_P',
        'gamma_1.70_cc_mult',
        'gamma_1.70_cc_vPT',
        'gamma_1.70_cc_PZ',
        'gamma_1.35_cc_mult',
        'gamma_1.35_cc_vPT',
        'gamma_1.35_cc_PZ',
        'gamma_1.00_cc_vPT',
        'gamma_1.00_cc_PZ',  
        'gamma_1.00_cc_mult',


        'L0Data_Sum_Et,Prev1',
        'L0Data_Sum_Et,Prev2',
        'gamma_PP_Saturation',
        'gamma_PP_CaloTrMatch'
        #'B_Hlt2RadiativeBs2PhiGammaDecision_TOS',\
        ]
    
    df = t_s.pandas.df(branches=variables)
    L0 = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
    L1 = 'B_Hlt1TrackMVADecision_TOS' 
    L2 = 'B_Hlt2RadiativeBd2KstGammaDecision_TOS'
    trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
    # trigger = L0
    df = df.query(trigger)
    df.to_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')






else:
    import pandas
    df = pandas.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))
# df = df.query('B_PT > 2000  & (piminus_PT>500 & kplus_PT > 500) & (kplus_ProbNNk > 0.2 & kplus_PIDK >5) & (piminus_ProbNNpi > 0.2 & piminus_ProbNNk < 0.2) & (kplus_IPCHI2_OWNPV > 55 & piminus_IPCHI2_OWNPV >55) & gamma_CL > 0.3 & gamma_PT > 2500 & gamma_P > 3000 & abs(kst_M-1019.46)<100 & kst_OWNPV_CHI2 < 50 & kst_PT > 1800 & (kst_PT + gamma_PT) >4000')
#df.query('B_PT > 2000 and gamma_PT > 3000 and  gamma_CL > 0.1 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)
import pickle
vars_bdt = [
        'B_DIRA_OWNPV',
        'kst_OWNPV_CHI2',
        #'phi_PT',
        'kplus_IPCHI2_OWNPV',
        'piminus_IPCHI2_OWNPV',
        #'gamma_1.00_cc_asy_PT',
        #'gamma_1.35_cc_asy_PT',
        #'gamma_1.70_cc_asy_PT',
        #'gamma_1.00_cc_asy_P',
        #'gamma_1.35_cc_asy_P',
        #'gamma_1.70_cc_asy_P',
            #'kminus_PT',
            #'kplus_PT',
            #'kminus_PIDK',
            #'kplus_PIDK',

             ]
with open('../PhiGammaBDT/bdts_kst.pickle','rb') as handle:
    bdts = pickle.load(handle)

mean_bdt=np.zeros(len(df))
for i in range(5):
    mean_bdt += bdts[i].predict_proba(df[vars_bdt])[:,1]/len(bdts)
    
df['bdt']=mean_bdt

# df.query('gamma_PT > 3000 and bdt > 0.6 and gamma_CL >0.5 and gamma_P > 6000 and gamma_PP_CaloTrMatch <0',inplace=True)
df.query('bdt > 0.6 and gamma_CL >0.3',inplace=True)
#######################



from fit_helpers import create_double_cb
name_prefix='Bs_'
BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,
                                             mass_range=BsMassRange,
                                             mass =5279.3,
                                             sigma=(88,70,95),
                                             nevs=len(df),
                                             al = 1.55,
                                             ar=1.76,
                                             nl=5.0,
                                             nr=5.2,)
#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',-0.1,-10,0,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
nBkgComb = zfit.Parameter(name_prefix + 'nBkg',len(df)/2,0,len(df))
BsBkgCombExtended = BsBkgComb.create_extended(nBkgComb)
f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
#model_Bs=BsCBExtended


data = zfit.Data.from_numpy(array =df['B_M'].to_numpy(),obs=obs)

nll = zfit.loss.ExtendedUnbinnedNLL(model =model_Bs,data=data)
#nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)
from hepstats.splot import compute_sweights
import matplotlib.pyplot as plt
minimizer = zfit.minimize.Minuit(tol=1e-4,mode=1,gradient=True,)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
sweights = compute_sweights(model_Bs, data)
df['sweights']=sweights[list(sweights.keys())[0]]
df.to_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')

#result.error()
print(result)
#print(result.params)
#result.
#PLOT
plot= True
#     plt.show()
nSig = BsParameters['nSig']
#nBkgComb = BsParameters['nBkg']
#nSig=len(df)
import plot_helpers
nSig=zfit.run(BsParameters['nSig'])
#nBkg=zfit.run(BsParameters['nBkgComb'])
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=model_Bs,
                      components=[BsCBExtended,BsBkgCombExtended],
                      yields=[nSig,nBkgComb],
                      labels=[r'$B^0$ signal: double Crystal Ball','Combinatorial background: straightline'],
                      colors=['red','green'],
                      nbins=30,
                      myrange=(m1g,m2g),
                      xlabel=r'M($K^{\pm}\pi^{\mp}\gamma$) [MeV]',
                      savefile='./kstarg-fit-stripping34r0p1.pdf')

import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
params = [BsParameters['nSig'],nBkgComb,BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],firstOrder]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':['$n_\\textrm{Signal}$',
                    '$n_\\textrm{Background}$',
                    '$m/m_\\textrm{PDG}$',
                    '$\\sigma$',
                    '$\\Delta(\\sigma)$',
                    '$a$'],
       'Units':['','','','MeV','MeV',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))

