
# from helpers import ufloat_to_str
from uncertainties import ufloat
# from sigfig import round
# print(ufloat_to_str(ufloat(1.0012,0.0025),mult=1.))

import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import xgboost
efficiency = False
lowpT = 0
highpT = 10000000000000
num = True

import joblib
import sklearn
#bdt = joblib.load('phigamma.bst')
vars_bdt = [
    'B_DIRA_OWNPV',
    'phi_OWNPV_CHI2',
    #'phi_PT',
    'kplus_IPCHI2_OWNPV',
    'kminus_IPCHI2_OWNPV',
    # 'gamma_1.00_cc_asy_PT',
    # 'gamma_1.35_cc_asy_PT',
    # 'gamma_1.70_cc_asy_PT',
    # 'gamma_1.00_cc_asy_P',
    # 'gamma_1.35_cc_asy_P',
    # 'gamma_1.70_cc_asy_P',
    #'kminus_PT',
    #'kplus_PT',
    #'kminus_PIDK',
    #'kplus_PIDK',
    
    ]

#Bs mass window
m1g,m2g = 5000,5800
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))
PhiMassRange = zfit.Space('phi_M',(m1k,m2k))
obs = BsMassRange



root = '/scratch47/adrian.casais/ntuples/turcal'
f_s = uproot.open(root + '/b02phigammaS34r0p1.root')
root = '/scratch47/adrian.casais/ntuples/turcal'
#f_s = uproot.open(root + '/phiGammaData_postCalib.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []


#for par in ['gamma','phi']:
    #variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
    #variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
#variables += list(filter(lambda x: 'B' in x and 'Decision' in x, keys))

variables += [#'gamma_MC_MOTHER_ID',
              #'gamma_TRUEID',
              #'phi_TRUEID',
              #'kplus_MC_MOTHER_ID',
              #'kminus_MC_MOTHER_ID',
              #'kminus_MC_MOTHER_ID',
              #'kplus_TRUEID',
              #'kminus_TRUEID',
              'B_M',
              'B_MM',
              'phi_M',
              'B_PT',
              'B_ETA',
              'B_DIRA_OWNPV',
              'nSPDHits',
              'L0Data_Sum_Et,Prev1',
              'L0Data_Sum_Et,Prev2',
              #'B_ENDVERTEX_X',
              #'B_ENDVERTEX_Y',
              #'B_ENDVERTEX_Z',
              'phi_MM',
              #'phi_MM',
              'phi_PT',
              'phi_OWNPV_CHI2',
              'kplus_IPCHI2_OWNPV',
              'kminus_IPCHI2_OWNPV',
              'kminus_PT',
              'kplus_PT',
              'kminus_PIDK',
              'kplus_PIDK',
              'gamma_CL',
              'gamma_PT',
              'gamma_L0ElectronDecision_TOS',
              'gamma_L0PhotonDecision_TOS',
              'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_Hlt1TrackMVADecision_TOS',
              'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS',
              'B_Hlt2RadiativeBs2PhiGammaDecision_TOS',
              'gamma_L0Global_TIS',
              'gamma_Hlt1Phys_TIS',
              'gamma_Hlt2Phys_TIS',
              'gamma_L0ElectronDecision_TOS',
              'gamma_L0PhotonDecision_TOS',
              'gamma_P',
              'gamma_PT',
              'gamma_PP_Saturation',
              'gamma_PP_CaloTrMatch',
              'B_Hlt1B2PhiGamma_LTUNBDecision_TOS']

variables = np.unique(vars_bdt+variables)
L0 = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
# L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L1 = 'B_Hlt1TrackMVADecision_TOS'
L2 = 'B_Hlt2RadiativeBs2PhiGammaDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)

#trigger ='{0} & {1}'.format(L0,L1)
#trigger = L0
df = t_s.pandas.df(branches=variables)
df.query(trigger,inplace=True)
# df_sides = df.query('(B_M<5150 | B_M>5700)')
# df_peak =df.query('(B_M>=5150 & B_M<=5700)')

import pandas as pd
#df = df.query(mc)
df = df.query(trigger)
df.query('B_M > {0} and B_M < {1}'.format(m1g,m2g),inplace=True)
import pickle
with open('../PhiGammaBDT/bdt.pickle','rb') as handle:
    bdts = pickle.load(handle)

mean_bdt=np.zeros(len(df))
for i in range(len(bdts)):
        mean_bdt += bdts[i].predict_proba(df[vars_bdt])[:,1]/len(bdts)
    
df['bdt']=mean_bdt
# df = df.query('(B_PT > 2000 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.3 & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 30) & phi_PT > 1800 & (phi_PT + gamma_PT) > 3000')

#df.query('B_PT > 2000  & (kplus_IPCHI2_OWNPV > 20 & kminus_IPCHI2_OWNPV > 20) & gamma_PT > 3000 & gamma_P > 3000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50 & phi_PT > 1500 & (phi_PT + gamma_PT) > 3000',inplace=True)
#df.query('B_PT > 2000 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 5 & kplus_PIDK >5)  & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_PT > 1800 & (phi_PT + gamma_PT) > 3000',inplace=True)


#df.query('gamma_PT > 3000 and gamma_P > 6000 and bdt > 0.75  & gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)
# df.query('gamma_PT > 3000 and gamma_P > 6000 and bdt > 0.6 and gamma_CL > 0.5 and gamma_PP_CaloTrMatch<0',inplace=True)
df.query('bdt > 0.6 and gamma_CL > 0.3',inplace=True)
#df.query('gamma_PP_CaloTrMatch>400 or gamma_PP_CaloTrMatch<0',inplace=True)
#df.query('gamma_PP_Saturation<0',inplace=True)

from fit_helpers import create_double_cb
name_prefix='Bs_'

BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,
                                            mass_range=BsMassRange,
                                            mass =5366.9,
                                            sigma=(88,70,95),
                                            al = 1.667,
                                            ar=1.943,
                                            nl=2.23,
                                            nr=3.50,
                                            nevs=len(df),
                                            extended=True)


BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',len(df)/2,0,len(df))

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-3,-10,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)

firstOrder = zfit.Parameter(name_prefix+'LegFirst',0,-20,0,floating=True)
BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])

#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])

####               
df_masses = df[['B_M','phi_M']]
# df_masses = df_masses.dropna()
#data = zfit.Data.from_pandas(df_masses,obs=obs)
data_Bs = zfit.Data.from_numpy(array=df['B_M'].values,obs=BsMassRange)
data_Phi = zfit.Data.from_numpy(array=df['phi_M'].values,obs=PhiMassRange)

# #### matchchi2 test, else comment

# alptype = 49100048
# with open(f'calomatch_reweight_{alptype}.pkl','rb') as handle:
#     reweighter = pickle.load(handle)
# df["gamma_Matching"]=df["gamma_PP_CaloTrMatch"]
# df["reweight"]=reweighter.predict_weights(df["gamma_Matching"])
# data_Bs = zfit.Data.from_numpy(array=df['B_M'].to_numpy(),obs=BsMassRange, weights=df["reweight"])

# ######################

#sCREATE LOSS FUNCTION

#Extended
#nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,xz
#                                    data=data_Bs)
nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,data=data_Bs)

#NonExtended
#nll = zfit.loss.UnbinnedNLL(model=[modelBs,modelPhi],data=[data_Bs,data_Phi])

minimizer = zfit.minimize.Minuit(tol=1e-6,mode=1,gradient=True)

#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.errors()
result.hesse()
print(result)

#PLOT
plot= True
from hepstats.splot import compute_sweights
import matplotlib.pyplot as plt
sweights = compute_sweights(modelBs, data_Bs)
df['sweights']=sweights[list(sweights.keys())[0]]
df.to_hdf(root+'/phigData.h5',key='df',mode='w')
import plot_helpers
nSig=zfit.run(BsParameters['nSig'])
nBkg=zfit.run(BsParameters['nBkgComb'])
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=modelBs,
                      components=[BsCBExtended,BsBkgComb],
                      yields=[nSig,nBkg],
                      labels=[r'$B_s$ signal: double Crystal Ball','Combinatorial background: straightline'],
                      colors=['red','green'],
                      nbins=30,
                      myrange=(m1g,m2g),
                      xlabel=r'M($K^+K^-\gamma$) [MeV]',
                      savefile='./phig-fit-stripping34r0p1.pdf')

import pandas as pd
import helpers
import importlib
import sigfig
importlib.reload(helpers)

hessians = result.hesse()
params = [BsParameters['nSig'],BsParameters['nBkgComb'],BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],firstOrder]
param_labels=['$n_\\textrm{Signal}$',
              '$n_\\textrm{Background}$',
              '$m/m_\\textrm{PDG}$',
              '$\\sigma$',
              '$\\Delta(\\sigma)$',
              '$a$']
units = ['','','','MeV','MeV','']

helpers.latex_fit_parameters(result,params,param_labels,units)

