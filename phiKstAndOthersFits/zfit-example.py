import zfit
import numpy as np

obs = zfit.Space('x', limits=(-10, 10))

# create the model
mu    = zfit.Parameter("mu"   , 2.4, -1, 5)
sigma = zfit.Parameter("sigma", 1.3,  0, 5)
gauss = zfit.pdf.Gauss(obs=obs, mu=mu, sigma=sigma)

# load the data
data_np = np.random.normal(size=10000)
data = zfit.Data.from_numpy(obs=obs, array=data_np)

# build the loss
nll = zfit.loss.UnbinnedNLL(model=gauss, data=data)

# minimize
minimizer = zfit.minimize.Minuit()
result = minimizer.minimize(nll)

# calculate errors
param_errors = result.error()

import matplotlib.pyplot as plt
import scipy.stats as stats
x = np.linspace(-10,10,1000)
nentries=data_np.shape[0]
nbins=100
counts, bin_edges = np.histogram(data_np, nbins, range=(-10, 10))
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2.
err = np.sqrt(counts)
plt.errorbar(bin_centres, counts, yerr=err, fmt='o', color='xkcd:black')
y = zfit.run(gauss.pdf(x,norm_range=obs))
#data_np = zfit.run(data)
plt.plot(x,zfit.run(obs.area()/nbins*y*float(nentries)))#bin_area*normalised_height*n_entries
#plt.hist(data_np,bins=50)
plt.show()
