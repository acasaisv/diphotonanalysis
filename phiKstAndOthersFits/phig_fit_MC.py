import zfit
import numpy as np
import scipy
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
#from ROOT import *
from fit_helpers import create_double_cb
import pickle

def res_fit(df):
    import zfit
    range = (-0.14,0.14)
    obs = zfit.Space('reso',range)
    BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(2,0.01,10),nevs=len(df)+100,extended=True)
    model_Bs=BsCBExtended
    df.eval('gamma_resolution = (-gamma_P + gamma_TRUEP_E)/gamma_TRUEP_E',inplace=True)
    df.query(f'gamma_resolution > {range[0]} and gamma_resolution < {range[1]}')

    data = zfit.Data.from_numpy(array=df['gamma_resolution'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =BsCBExtended,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=7000000)
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    reload(plot_helpers)
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['gamma_resolution'],
                        full_model=BsCBExtended,
                        components=[BsCBExtended],
                        yields=[nSig],
                        labels=[r'$B_s$ signal double Crystal Ball'],
                        colors=['blue'],
                        nbins=30,
                        myrange=range,
                        xlabel=r'Resolution',
                        savefile=f'./resolution-phig.pdf',
                        plot=False)

#Bs mass window
m1g,m2g = 5000,5800
#Phi mass window
m1k,m2k = 1006,1034

BsMassRange = zfit.Space('B_M',(m1g,m2g))

obs = BsMassRange


root = '/scratch04/adrian.casais/ntuples'
root = '/scratch47/adrian.casais/ntuples/turcal'

f_s = uproot.open(root + '/b02phigammaMC-sim10.root')
t_s = f_s['DecayTree/DecayTree']
keys = [ i.decode('utf-8') for i in t_s.keys() ]
variables = []

#variables += list(filter(lambda x: 'Bs' in x and 'Decision' in x, keys))
variables += ['gamma_MC_MOTHER_ID',
              'gamma_TRUEID',
              'gamma_PP_Saturation',
              'phi_TRUEID',
              'gamma_TRUEP_E',
              'kplus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kminus_MC_MOTHER_ID',
              'kplus_TRUEID',
              'kminus_TRUEID',
              'B_M',
              'B_MM',
              'B_PT',
              'B_ETA',
              'B_DIRA_OWNPV',
              'nSPDHits',
              #'B_ENDVERTEX_X',
              #'B_ENDVERTEX_Y',
              #'B_ENDVERTEX_Z',
              'phi_M',
              'phi_MM',
              'phi_OWNPV_CHI2',
              'phi_PT',
              'kplus_IPCHI2_OWNPV',
              'kminus_IPCHI2_OWNPV',
              'kminus_PT',
              'kplus_PT',
              'kminus_PIDK',
              'kplus_PIDK',
              'gamma_CL',
              'gamma_PT',
              'gamma_P',
              'gamma_PP_IsPhoton',
              'gamma_L0ElectronDecision_TOS',
              'gamma_L0PhotonDecision_TOS',
              'B_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_Hlt1TrackMVADecision_Dec',
              'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS',              
              'gamma_1.00_cc_asy_PT',
              'gamma_1.35_cc_asy_PT',
              'gamma_1.70_cc_asy_PT',
              'gamma_1.00_cc_asy_P',
              'gamma_1.35_cc_asy_P',
              'gamma_1.70_cc_asy_P',
              'gamma_PP_CaloTrMatch'
        ]

    
L0 = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
L1 = 'B_Hlt1B2PhiGamma_LTUNBDecision_TOS'
L1 = 'B_Hlt1TrackMVADecision_Dec'
L2 = 'B_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS'
trigger = '{0} & {1} & {2}'.format(L0,L1,L2)
#trigger=L0
mc ='abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and abs(phi_TRUEID)==333 and abs(kplus_TRUEID) == 321 and abs(kminus_TRUEID)==321'

df = t_s.pandas.df(branches=set(variables))
df = df.query(mc)
df = df.query(trigger)
df = df.query('B_M > {0} & B_M < {1}'.format(m1g,m2g,m1k,m2k))
def load_bdt(mydf):
    vars_bdt = [
        'B_DIRA_OWNPV',
        'phi_OWNPV_CHI2',
        #'phi_PT',
        'kplus_IPCHI2_OWNPV',
        'kminus_IPCHI2_OWNPV',
        # 'gamma_1.00_cc_asy_PT',
        # 'gamma_1.35_cc_asy_PT',
        # 'gamma_1.70_cc_asy_PT',
        # 'gamma_1.00_cc_asy_P',
        # 'gamma_1.35_cc_asy_P',
        # 'gamma_1.70_cc_asy_P',
        #'kminus_PT',
        #'kplus_PT',
        #'kminus_PIDK',
        #'kplus_PIDK',
        
        ]
    with open('../PhiGammaBDT/bdt.pickle','rb') as handle:
        bdts = pickle.load(handle)

    mean_bdt=np.zeros(len(df))
    for i in range(len(bdts)):
        mean_bdt += bdts[i].predict_proba(df[vars_bdt])[:,1]/len(bdts)
    
    mydf['bdt']=mean_bdt
    return mydf
load_bdt(df)


# df.query('B_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV >55)  & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46)<100 & phi_OWNPV_CHI2 < 30 & phi_PT > 1800 & (phi_PT + gamma_PT) >3000',inplace=True)
# df.query('gamma_PT > 3000 and gamma_P > 6000  & gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)

#df.query('B_PT > 2000 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 5 & kplus_PIDK >5)  & gamma_PT > 3000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_PT > 1800 & (phi_PT + gamma_PT) > 3000',inplace=True)
#df.query('B_PT > 2000 and gamma_PT > 3000 and gamma_P > 6000 and  gamma_CL > 0.3 & (kminus_PT>500 & kplus_PT>500)   & (kminus_PIDK > 3 & kplus_PIDK >3)',inplace=True)
#df.query('gamma_PP_Saturation <0',inplace=True)
df.query('bdt>0.6 & gamma_CL > 0.3',inplace=True)
#df.query('gamma_PP_CaloTrMatch>400 or gamma_PP_CaloTrMatch<0',inplace=True)
# df.query('gamma_PT > 3000 and gamma_P > 6000 and bdt>0.6 and gamma_PP_Saturation<1 & gamma_CL > 0.5 & gamma_PP_CaloTrMatch<0',inplace=True)

### comment that for matchchi2 test, else uncomment
# res_fit(df)

#Signal: Double Crystall Ball

name_prefix='Bs_'
BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,mass_range=BsMassRange,mass =5366.9,sigma=(88,70,110),nevs=1.1*(len(df)))

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-5,-9e-5,1e-5)
BsBkg = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,0,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder])
nBkgComb = zfit.Parameter(name_prefix + 'nBkg',len(df)/2,0,len(df))
BsBkgCombExtended = BsBkgComb.create_extended(nBkgComb)
f_Bs= zfit.Parameter('f_Bs',0.5,0,1)
model_Bs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
model_Bs=BsCBExtended

data = zfit.Data.from_numpy(array=df['B_M'].to_numpy(),obs=obs)

# #### matchchi2 test, else comment
# alptype = 49100048
# with open(f'calomatch_reweight_{alptype}.pkl','rb') as handle:
#     reweighter = pickle.load(handle)
# df["gamma_Matching"]=df["gamma_PP_CaloTrMatch"]
# df["reweight"]=reweighter.predict_weights(df["gamma_Matching"])
# data = zfit.Data.from_numpy(array=df['B_M'].to_numpy(),obs=obs, weights=df["reweight"])
# #################


nll = zfit.loss.ExtendedUnbinnedNLL(model =model_Bs,data=data)
#nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)

minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=1000000)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
print(result)
print(result.errors())
#result.error()


if model_Bs !=BsCBExtended:
    from hepstats.splot import compute_sweights
    sweights = compute_sweights(model_Bs, data)
    signal_sweights = sweights[list(sweights.keys())[0]]
    df['sweights'] = signal_sweights
    # #print(sweights)
    #df['sweights'] = np.ones(len(df))
else:
    df['sweights']=np.ones(len(df))
df.to_hdf(root+'/phigMC.h5',key='df',mode='w')


print(result)
plot= False
import plot_helpers
nSig=zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['B_M'],
                      full_model=BsCBExtended,
                      components=[BsCBExtended],
                      yields=[nSig],
                      labels=[r'$B_s$ signal double Crystal Ball'],
                      colors=['blue'],
                      nbins=30,
                      myrange=(m1g,m2g),
                      xlabel=r'M($K^+K^-\gamma$) [MeV]',
                      savefile='./phig-fit-MC.pdf')
import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
params = [BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],BsParameters['a_l'],BsParameters['a_r'],BsParameters['n_l'],BsParameters['n_r']]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':[#$'n_\\textrm{Signal}',
                    #'n_\\textrm{Background}',
                    '$m/m_\\textrm{PDG}$',
                    '$\\sigma$',
                    '$\\Delta(\\sigma)$',
                    '$\\alpha_L$',
                    '$\\alpha_R$',
                    '$n_L$',
                    '$n_R$',]
       ,'Units':['','MeV','MeV','','','',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))

