from ROOT import *
from math import sqrt

def get_error(eff,n):
    return sqrt(eff*(1-eff)/n)


gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

fBs=TFile("/afs/cern.ch/work/a/acasaisv/ALPs-gauss/GenLevelBs/DVntuple.root");tBs=fBs.Get("MCDecayTreeTuple/MCDecayTree")
fALPs=TFile("/afs/cern.ch/work/a/acasaisv/ALPs-gauss/tuplesALPs/ALP5GeV.root");tALPs=fALPs.Get("MCDecayTreeTuple/MCDecayTree")


#PLOT ETA
ALPs_eta=TH1F("ALPs_eta","",50,0,8)
Bs_eta=TH1F("Bs_eta","",50,0,8)
tALPs.Project("ALPs_eta","AxR0_ETA")
tBs.Project("Bs_eta","B_s0_ETA")

ALPs_eta.SetLineColor(kRed)

# ALPs_eta.DrawNormalized()
# Bs_eta.DrawNormalized("same")

#PLOT PT
ALPs_pt=TH1F("ALPspt","ALPspt",70,0,30000)
Bs_pt=TH1F("Bspt","Bspt",70,0,30000)
tALPs.Project("ALPspt","AxR0_TRUEPT")
tBs.Project("Bspt","B_s0_TRUEPT")

ALPs_pt.SetLineColor(kRed)

leg=TLegend(.6,.8,.6,.8)
leg.AddEntry(ALPs_pt,"ALPs","L")
leg.AddEntry(Bs_pt,"B_{s}","L")


Bs_pt.GetXaxis().SetTitle("p_{T} [MeV/c]")
Bs_pt.GetYaxis().SetTitle("Normalized Entries / 428.57 MeV/c")
Bs_pt.DrawNormalized()
ALPs_pt.DrawNormalized("same")
leg.Draw()


#CUTTING ON CANDIDATE PT
aa = "&&"
print "CANDIDATE PT"
value=10000
print 1.*tALPs.GetEntries("AxR0_TRUEPT>{0}".format(value))/tALPs.GetEntries()
print 1.*tBs.GetEntries("B_s0_TRUEPT>{0}".format(value))/tBs.GetEntries()


#CUTTING ON PHOTONS PT
value_pt = 1100
print "PHOTONS PT"
gammas_pt = "(gamma0_TRUEPT>{0} && gamma_TRUEPT>{0})".format(value_pt)
print "ALPs", 1.*tALPs.GetEntries(gammas_pt)/tALPs.GetEntries()
print "Bs", 1.*tBs.GetEntries(gammas_pt)/tBs.GetEntries()


#CUTTING ON PHOTONS P
value_p = 6000
print "PHOTONS P"
gammas_p="(sqrt(gamma0_PT**2 +gamma0_PZ**2)>{0} &&sqrt(gamma_PT**2 +gamma_PZ**2)>{0})".format(value_p)
print "ALPs", 1.*tALPs.GetEntries(gammas_p)/tALPs.GetEntries()
print "Bs",1.*tBs.GetEntries(gammas_p)/tBs.GetEntries()



#STRIPPING CUTS
print "STRIPPING CUTS"
theta_max = 5
theta_min = 2

acceptance = "((gamma0_ETA>{0} && gamma_ETA>{0}) && (gamma0_ETA<{1} && gamma_ETA<{1})) ".format(theta_min,theta_max)

print "ALPs",1.*tALPs.GetEntries(acceptance)/tALPs.GetEntries()



print "ALPs",1.*tALPs.GetEntries(gammas_p+aa+gammas_pt+aa+acceptance)/tALPs.GetEntries(),"+-",get_error(1.*tALPs.GetEntries(gammas_p+aa+gammas_pt+aa+acceptance)/tALPs.GetEntries(),tALPs.GetEntries())
print "Bs",0.5*tBs.GetEntries(gammas_p+aa+gammas_pt+aa+acceptance)/tBs.GetEntries(),"+-",get_error(0.5*tBs.GetEntries(gammas_p+aa+gammas_pt+aa+acceptance)/tBs.GetEntries(),tBs.GetEntries())





#COMPARING HLT1 EFF
value_pt = 3500#3500
value_sumpt= 8000#8000
value_candpt=2000#2000
hlt1 = "gamma0_TRUEPT>{0} && gamma_TRUEPT>{0} && (gamma0_TRUEPT + gamma_TRUEPT)>{1} && {2}_TRUEPT>{3}"
print "HLT1 cut"
print "ALPs",1.*tALPs.GetEntries(hlt1.format(value_pt,value_sumpt,"AxR0",value_candpt))/tALPs.GetEntries(),"+-",get_error(1.*tALPs.GetEntries(hlt1.format(value_pt,value_sumpt,"AxR0",value_candpt))/tALPs.GetEntries(),tALPs.GetEntries())
print "Bs",1.*tBs.GetEntries(hlt1.format(value_pt,value_sumpt,"B_s0",value_candpt))/tBs.GetEntries(),"+-",get_error(1.*tBs.GetEntries(hlt1.format(value_pt,value_sumpt,"B_s0",value_candpt))/tBs.GetEntries(),tBs.GetEntries())

# ALPs_eta=TH1F("ALPs_eta","",50,0,30000)
# Bs_eta=TH1F("Bs_eta","",50,0,30000)
# tALPs.Project("ALPs_eta","gamma0_PT",hlt1.format(value_pt,value_sumpt,"AxR0",value_candpt))
# tBs.Project("Bs_eta","gamma0_PT",hlt1.format(value_pt,value_sumpt,"B_s0",value_candpt))


# leg=TLegend(.6,.8,.6,.8)

# ALPs_eta.SetLineColor(kRed)
# leg.AddEntry(ALPs_eta,"ALPs","L")
# leg.AddEntry(Bs_eta,"B_{s}","L")
# Bs_eta.GetXaxis().SetTitle("p_{T}")
# Bs_eta.GetYaxis().SetTitle("Normalized Entries / 600 MeV")
# Bs_eta.DrawNormalized()
# ALPs_eta.DrawNormalized("same")

# leg.Draw()







