import numpy as np
from uncertainties import ufloat as uf

import uproot3 as uproot
import pickle

def get_eff(sample, cut_denominator, cut, weightsbranch=None):
		denominator = sample.query(cut_denominator)
		numerator = sample.query(cut_denominator+'&'+cut)
		if weightsbranch is not None:
			passed = np.sum(numerator[weightsbranch])
			passed2 = np.sum([w**2 for w in numerator[weightsbranch]])

			total = np.sum(denominator[weightsbranch])
			total2 = np.sum([w**2 for w in denominator[weightsbranch]])

			eff = passed/total
			unc = np.sqrt((passed2*(total-passed)**2+passed**2*(total2-passed2))/total**4)
			return uf(eff,unc)

		else:
			eff = len(numerator)*1.0/len(denominator)
			unc = np.sqrt(eff*(1-eff)/len(denominator))
			return uf(eff,unc)

def get_eff_truth(sample,cut,weightsbranch=None):
		denominator = sample
		numerator = sample.query(cut)

		if weightsbranch is not None:
			passed = np.sum(numerator[weightsbranch])
			total = len(denominator)

			eff = passed/total
			unc = np.sqrt(eff*(1-eff)/len(denominator))
			return uf(eff,unc)

		else:
			eff = len(numerator)*1.0/len(denominator)
			unc = np.sqrt(eff*(1-eff)/len(denominator))
			return uf(eff,unc)


truth_vars_kstgamma = [	'gamma_MC_MOTHER_ID',
          		'gamma_TRUEID',
          		'kst_TRUEID',
          		'kplus_MC_MOTHER_ID',
          		'piminus_MC_MOTHER_ID',
           		'B_TRUEID',
          		'kplus_TRUEID',
          		'piminus_TRUEID',
          		'B_TRUEID',
          		'B_M',
          		'kst_M',
          		'gamma_CL'
          	]
truth_vars_phigamma = [	'gamma_MC_MOTHER_ID',
          		'gamma_TRUEID',
          		'phi_TRUEID',
          		'kplus_MC_MOTHER_ID',
          		'kminus_MC_MOTHER_ID',
           		'B_TRUEID',
          		'kplus_TRUEID',
          		'kminus_TRUEID',
          		'B_TRUEID',
          		'B_M',
          		'phi_M',
          		'gamma_CL'
          	]
trigger_vars = [ 'gamma_L0ElectronDecision_TOS',
				 'gamma_L0PhotonDecision_TOS',
				 'B_Hlt1TrackMVADecision_TOS',
				 'B_Hlt2RadiativeBd2KstGammaDecision_TOS',
				 'B_Hlt2RadiativeBs2PhiGammaDecision_TOS',
				 'nSPDHits'
				]

bdt_vars_kstargamma = [	'B_DIRA_OWNPV',
    					'kst_OWNPV_CHI2',
    					'kplus_IPCHI2_OWNPV',
    					'piminus_IPCHI2_OWNPV'
    					]

bdt_vars_phigamma = [	'B_DIRA_OWNPV',
    					'phi_OWNPV_CHI2',
    					'kplus_IPCHI2_OWNPV',
    					'kminus_IPCHI2_OWNPV'
    				]

print("Loading MC")

events_kstgamma = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02kstargammaMC-sim10.root")
tree_kstgamma = events_kstgamma["DecayTree/DecayTree"]
events_phigamma = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02phigammaMC-sim10.root")
tree_phigamma = events_phigamma["DecayTree/DecayTree"]

df_kstgamma = tree_kstgamma.pandas.df(branches=truth_vars_kstgamma+trigger_vars+bdt_vars_kstargamma)
df_phigamma = tree_phigamma.pandas.df(branches=truth_vars_phigamma+trigger_vars+bdt_vars_phigamma)

print("Loading and writing BDTs")

with open('../PhiGammaBDT/bdts_kst.pickle','rb') as handle:
    bdts_kstgamma = pickle.load(handle)

mean_bdt_kstgamma=np.zeros(len(df_kstgamma))
for i in range(5):
    mean_bdt_kstgamma += bdts_kstgamma[i].predict_proba(df_kstgamma[bdt_vars_kstargamma])[:,1]/len(bdts_kstgamma)
    
df_kstgamma['bdt']=mean_bdt_kstgamma


with open('../PhiGammaBDT/bdt.pickle','rb') as handle:
    bdts_phigamma = pickle.load(handle)

mean_bdt_phigamma=np.zeros(len(df_phigamma))
for i in range(len(bdts_phigamma)):
        mean_bdt_phigamma += bdts_phigamma[i].predict_proba(df_phigamma[bdt_vars_phigamma])[:,1]/len(bdts_phigamma)
    
df_phigamma['bdt']=mean_bdt_phigamma


print("Calculate efficiencies")

cut_truth_kstgamma = 'abs(gamma_MC_MOTHER_ID)==511  &  gamma_TRUEID==22 & abs(kst_TRUEID)==313 & ((kplus_TRUEID==321&piminus_TRUEID==-211)|(kplus_TRUEID==-321&piminus_TRUEID==211)) & abs(B_TRUEID)==511'
cut_truth_phigamma = 'abs(gamma_MC_MOTHER_ID)==531  &  gamma_TRUEID==22 & phi_TRUEID==333 & kplus_TRUEID == 321 & kminus_TRUEID==-321 & abs(B_TRUEID)==531'

cut_PID = 'gamma_CL>0.3'

cut_L0 = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
cut_Hlt1 = 'B_Hlt1TrackMVADecision_TOS'
cut_Hlt2_kstargamma = 'B_Hlt2RadiativeBd2KstGammaDecision_TOS'
cut_Hlt2_phigamma = 'B_Hlt2RadiativeBs2PhiGammaDecision_TOS'
cut_BDT_kstargamma = "bdt>0.6"
cut_BDT_phigamma = "bdt>0.6"

## write down the generator efficiency

gen_kstgamma = (uf(0.13197,0.00025)+uf(0.13204,0.00025))/2
gen_phigamma = (uf(0.14020,0.00027)+uf(0.14022,0.00026))/2

print("GenLevel:")
print(gen_kstgamma,"\t",gen_phigamma)

## get the stripping filter efficiency

stripfilter_kstgamma = (uf(0.0795,0)+uf(0.0794,0))/2
stripfilter_phigamma = (uf(0.0856,0)+uf(0.0854,0))/2

print("StripFilter:")
print(stripfilter_kstgamma,"\t",stripfilter_phigamma)

## get the truth matching efficiency

truth_kstgamma = get_eff_truth(df_kstgamma, cut_truth_kstgamma)
truth_phigamma = get_eff_truth(df_phigamma, cut_truth_phigamma)

print("Truth:")
print(truth_kstgamma,"\t",truth_phigamma)


## reweight the B kinematics

## photon ID (with eta2mmg proxy necessary)
PID_kstargamma = get_eff(df_kstgamma,cut_truth_kstgamma,cut_PID)
PID_phigamma = get_eff(df_phigamma,cut_truth_phigamma,cut_PID)

print("PID:")
print(PID_kstargamma,"\t",PID_phigamma)

## get the L0 efficiency (with eta2mmg proxy, also nSPDHits correction and SumEtPrev correction necessary)

scale_kstagmma = 1.23
scale_phigamma = 1.36 

nspdhits_kstargamma = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID, "nSPDHits<450")
nspdhits_kstargamma_corr = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID, f"nSPDHits<450/{scale_kstagmma}")

nspdhits_phigamma = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID, "nSPDHits<450")
nspdhits_phigamma_corr = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID, f"nSPDHits<450/{scale_phigamma}")


L0_kstargamma = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID, cut_L0)*nspdhits_kstargamma_corr/nspdhits_kstargamma*0.94
L0_phigamma = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID, cut_L0)*nspdhits_phigamma_corr/nspdhits_phigamma*0.94

print("L0:")
print(L0_kstargamma,"\t",L0_phigamma)

## get the HLT1 and HLT2 efficiencies (well described?)

Hlt1_kstargamma = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID+"&"+cut_L0, cut_Hlt1)
Hlt1_phigamma = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID+"&"+cut_L0, cut_Hlt1)

print("Hlt1:")
print(Hlt1_kstargamma,"\t",Hlt1_phigamma)

Hlt2_kstargamma = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID+"&"+cut_L0+"&"+cut_Hlt1, cut_Hlt2_kstargamma)
Hlt2_phigamma = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID+"&"+cut_L0+"&"+cut_Hlt1, cut_Hlt2_phigamma)

print("Hlt2:")
print(Hlt2_kstargamma,"\t",Hlt2_phigamma)

## get the BDT efficiency (variables well described?)

bdt_kstargamma = get_eff(df_kstgamma, cut_truth_kstgamma+"&"+cut_PID+"&"+cut_L0+"&"+cut_Hlt1+"&"+cut_Hlt2_kstargamma, cut_BDT_kstargamma)
bdt_phigamma = get_eff(df_phigamma, cut_truth_phigamma+"&"+cut_PID+"&"+cut_L0+"&"+cut_Hlt1+"&"+cut_Hlt2_phigamma, cut_BDT_phigamma)

print("BDT:")
print(bdt_kstargamma,"\t",bdt_phigamma)

## get the normalisation yield (needs proper fit that includes non-resonant and part-reco components!?)

yield_kstargamma = uf(58017.3,6.7e+02)
yield_phigamma = uf(9204.75,1.8e+02)


total_kstargamma = gen_kstgamma*stripfilter_kstgamma*truth_kstgamma*PID_kstargamma*L0_kstargamma*Hlt1_kstargamma*Hlt2_kstargamma*bdt_kstargamma
total_phigamma = gen_phigamma*stripfilter_phigamma*truth_phigamma*PID_phigamma*L0_phigamma*Hlt1_phigamma*Hlt2_phigamma*bdt_phigamma

fs_fd_13tev = uf(0.2539,0.0079) #taking from https://arxiv.org/2103.06810

kstoverphi = (yield_kstargamma/yield_phigamma) * (total_phigamma/total_kstargamma) * fs_fd_13tev

kstoverphi_7tev = uf(1.23,np.sqrt(0.06**2+0.004**2+0.1**2)) #https://arxiv.org/pdf/1209.0313.pdf (independent LHCb measurement at 7 tev)

BF_Bsphigamma = uf(3.4,0.4)*1e-5*uf(49.1,0.5)*1e-2

eff_Bsgg = uf(0.031,0.006)*1e-2

alpha = BF_Bsphigamma/yield_phigamma*total_phigamma/eff_Bsgg

print("Efficiencies:", total_kstargamma, total_phigamma)
print("yields:", yield_kstargamma, yield_phigamma)
print("BF ratio: our measurement is",kstoverphi,"vs. old LHCb:",kstoverphi_7tev)
print("Diff:", kstoverphi-kstoverphi_7tev, "Ratio of ratios:", kstoverphi/kstoverphi_7tev)


print("Single event sensitivity BS2GG:",alpha)
print("Single event sensitivity B02GG:",alpha*fs_fd_13tev)





