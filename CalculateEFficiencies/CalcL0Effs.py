from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTs,etas,spds,get_error,get_error_w,pack_eff,get_ALPsdf,get_ALPsdf_sim10b,sim10_masses_map,ufloat_to_str,bins
import pickle
from uncertainties import ufloat

key = 'l0'
pTs = np.array(bins[key]['pts'])
etas = bins[key]['etas']
spds = bins[key]['spds']
def convolute_eff(m,df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs):
    eff_gamma=0
    erreff_gamma2=0
    effMC_gamma=0
    erreffMC_gamma2=0
    eff_gamma0=0
    erreff_gamma02=0
    effMC_gamma0=0
    erreffMC_gamma02=0
    
    sum_fractions = 0

    eff_bs =0
    erreff_bs2=0
    
    den = df.query('gamma_PT >= 3000')['weights']
    den0 = df.query('gamma0_PT >= 3000')['weights']
    den = den0 = denbs = df.query('gamma0_PT >= 3000 and gamma_PT >=3000')['weights']
    for ipt in range(len(pTs)-1):
        pT = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                
                df_ =  df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =  df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                
                num = df_['weights']
                fraction = num.sum()/den.sum()
                err_fraction = get_error_w(num,den)

                eff_gamma += fraction*efficiencies[pT][eta][spd]
                erreff_gamma2 += err_fraction**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction**2

                effMC_gamma += fraction*efficienciesMC[pT][eta][spd]
                erreffMC_gamma2 += err_fraction**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction**2
                
                num0 = df0_['weights']
                fraction0 = num0.sum()/den0.sum()
                err_fraction0 = get_error_w(num0,den0)

                eff_gamma0 += fraction0*efficiencies[pT][eta][spd]
                erreff_gamma02 += err_fraction0**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction0**2

                effMC_gamma0 += fraction0*efficienciesMC[pT][eta][spd]
                erreffMC_gamma02 += err_fraction0**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction0**2


                
    cutl0 = '(gamma_L0PhotonDecision_TOS | gamma_L0ElectronDecision_TOS)'
    cutl00 = '(gamma0_L0PhotonDecision_TOS | gamma0_L0ElectronDecision_TOS)'
    cutbs = '(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS)'
    
    cutkin =  '(gamma_PT >= 3000)'
    cutkin0 = '(gamma0_PT >= 3000)'
    cutkin = cutkin0 = cutkinbs = '(gamma0_PT >= 3000 and gamma_PT >=3000)'

    num_w = df.query("{0} & {1} ".format(cutl0,cutkin))['weights']
    den_w = df.query(cutkin)['weights']
    l0tos =  num_w.sum()/den_w.sum()
    errl0tos = get_error_w(num_w,den_w)

    num0_w = df.query("{0} & {1} ".format(cutl00,cutkin0))['weights']
    den0_w = df.query(cutkin0)['weights']
    l0tos0 =  num0_w.sum()/den0_w.sum()
    errl0tos0 = get_error_w(num0_w,den0_w)
    
    numbs_w = df.query("{0} & {1} ".format(cutbs,cutkinbs))['weights']
    denbs_w = df.query(cutkinbs)['weights']
    l0tosbs =  numbs_w.sum()/denbs_w.sum()
    errl0tosbs = get_error_w(numbs_w,denbs_w)




    bsmcefferror = np.sqrt(erreffMC_gamma02 +erreffMC_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)
    bsefferror = np.sqrt(erreff_gamma02 +erreff_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)

    
    effbsmc =  (effMC_gamma0+effMC_gamma) - effMC_gamma0*effMC_gamma
    effbs = (eff_gamma0+eff_gamma) - eff_gamma0*eff_gamma

    #nSPDHits
    
    
    emcg0 = ufloat(effMC_gamma0,np.sqrt(erreffMC_gamma02))
    emcg1 = ufloat(effMC_gamma,np.sqrt(erreffMC_gamma2))
    print(emcg0)
    edatag0 = ufloat(eff_gamma0,np.sqrt(erreff_gamma02))
    edatag1 = ufloat(eff_gamma,np.sqrt(erreff_gamma2))
    el0tos0 = ufloat(l0tos0,errl0tos0)
    el0tos1 = ufloat(l0tos,errl0tos)
    el0tosbs = ufloat(l0tosbs,errl0tosbs)

    print(el0tosbs)
    print(el0tos0 + el0tos1 - (el0tos0*el0tos1))


    # print(f"p(A) = {l0tos0}. p(B) = {l0tos}")
    # num0_w = df.query(f"{cutl00} & {cutkin0} & {cutl0}")['weights']
    # den0_w = df.query(f"{cutkin0} & {cutl00}")['weights']
    # l0tos0 =  num0_w.sum()/den0_w.sum()
    # errl0tos0 = get_error_w(num0_w,den0_w)

    # num_w = df.query(f"{cutl00} & {cutkin0} & {cutl0}")['weights']
    # den_w = df.query(f"{cutkin0} & {cutl0}")['weights']
    # l0tos =  num_w.sum()/den_w.sum()
    # errl0tos = get_error_w(num_w,den_w)
    # print(f"p(A/B) = {l0tos0}. p(B/A) = {l0tos}")

    # num_w = df.query(f"{cutl00} & {cutkin0} & {cutl0}")['weights']
    # den_w = df.query(f"{cutkin0}")['weights']
    # l0tosbs =  num_w.sum()/den_w.sum()
    # errl0tos = get_error_w(num_w,den_w)
    # print(f"p(A & B) = {l0tos0*l0tos}. p(bs) = {l0tosbs}")



    #bsmc = (emcg0+emcg1)*el0tosbs/(el0tos0+el0tos1)
    #bsdata = (edatag0+edatag1)*el0tosbs/(el0tos0+el0tos1)

    bsmc = (emcg0+emcg1) -(emcg0*emcg1)
    bsdata = (edatag0+edatag1) - (edatag0*edatag1)

    bsmc += ufloat(0,abs(bsmc.n - el0tosbs.n))
    bsdata += ufloat(0,abs(bsmc.n - el0tosbs.n))

    fulll0mc = bsmc
    fulll0data = bsdata

    
    
    eff_gamma = ufloat(eff_gamma,np.sqrt(erreff_gamma2))
    eff_gamma0 = ufloat(eff_gamma0,np.sqrt(erreff_gamma02))
    eff_gamma = (eff_gamma + eff_gamma0)/2.

    effMC_gamma = ufloat(effMC_gamma,np.sqrt(erreffMC_gamma2))
    effMC_gamma0 = ufloat(effMC_gamma0,np.sqrt(erreffMC_gamma02))
    effMC_gamma = (effMC_gamma + effMC_gamma0)/2.
    effarray = np.array((m,
                           pack_eff(l0tos,errl0tos),
                           #pack_eff(100.*l0tos0,100.*errl0tos0),
                           ufloat_to_str(eff_gamma),
                           #pack_eff(100.*eff_gamma0,100.*np.sqrt(erreff_gamma02)),
                           ufloat_to_str(effMC_gamma),
                           #pack_eff(100.*effMC_gamma0,100.*np.sqrt(erreffMC_gamma02)),
                           ufloat_to_str(el0tosbs),
                           ufloat_to_str(bsdata),
                           ufloat_to_str(bsmc),
                           #ufloat_to_str(fulll0data),
                           #ufloat_to_str(fulll0mc))
                           ))
    
    if dfeffs.shape == (0,0):
        pass
    else:
       dfeffs = dfeffs.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeffs)),ignore_index=True) 

    return fulll0data, dfeffs
    
def reweight_alps_df(dfalps,dfeta):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = [#'eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5,'random_state':2456})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfalps['nSPDHits'], target=1.7*dfalps['nSPDHits'], target_weight=dfalps['weights'])
    dfalps['weights']= reweighter.predict_weights(dfalps['nSPDHits'])
    return

    
    #print('Fraction sum = {}'.format(sum_fractions))

def nspds_eff(df,factor_u=1.09,factor_l=1.15):

    def helpme(df,factor):
        df['nSPDHits']*=factor
        nSPDhits_den = df['weights']
        nSPDhits_num = df.query('nSPDHits < 450')['weights']
        eff = nSPDhits_num.sum()/nSPDhits_den.sum()
        err_eff = get_error_w(nSPDhits_num,nSPDhits_den)

        df['nSPDHits']/=factor

        return df,eff,err_eff

    df,eff_u,err_eff_u = helpme(df,factor_u)
    df,eff_l,err_eff_l = helpme(df,factor_l)
    df,eff_c,err_eff_c = helpme(df,(factor_u+factor_l)/2.)
    myerr = np.max([abs(eff_u-eff_c),abs(eff_l-eff_c)])
    full_eff = ufloat(eff_c,err_eff_c) + ufloat(0,abs(myerr))

    

    return df,full_eff.n,full_eff.s

def print_l0column(masses=sim10_masses_map,print_table = False):
    import pickle
    with open('L0Efficiencies.p','rb') as file:
        efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    l0dic = {'L0':[]}
    l0s = []
    dic = { 'Mass':[],
            
            '$\\epsilon(\\gamma)$':[],
            '$E_{\\textrm{Data}} (\\gamma)$':[],
            '$E_{\\textrm{MC}} (\\gamma)$':[],
            '$\epsilon (\\aa)$':[],
            '$E_{\\textrm{Data}} (\\aa)$':[],
            '$E_{\\textrm{MC}} (\\aa)$':[]}
    dfeffs = pd.DataFrame(dic)
    for i in masses:
        #df = get_df(i,bs,background=False)
        #root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        if i=='gg':
            cut = '(gamma_PT > 3000 & gamma0_PT > 3000) & gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000 and nSPDHits < 450'
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            df = get_ALPsdf_sim10b(myfile,extracut=cut,background=False,bs=True) 
            bs = True
        else:
            cut = '(gamma0_PT >= 3000 and gamma_PT >=3000) and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000 and nSPDHits < 450'
            root='/scratch47/adrian.casais/ntuples/signal/sim10/'
            df = get_ALPsdf_sim10b(i,bs=False,background=False,extracut=cut)

        
        
        l0data,dfeffs=convolute_eff(masses[i],df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs)
        
        l0dic['L0'].append(pack_eff(l0data.nominal_value,l0data.std_dev))
        l0s.append(l0data)
    l0df = pd.DataFrame(l0dic)
    if print_table:
        print(dfeffs.to_latex(index=False,escape=False))
    return l0dic['L0'],l0s

if __name__ == '__main__':
    masses = sim10_masses_map
    del masses['pi0pi0']

    #masses = {40:5}
    print_l0column(masses=masses,print_table = True)

    # import pickle
    # with open('L0Efficiencies.p','rb') as file:
    #     efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    # # masses = {40:'5 GeV',
    # #           42:'7 GeV',
    # #           44:'9 GeV',
    # #           46:'11 GeV',
    # #           47:'13 GeV',
    # #           50:'15 GeV',
    # #           48:'4 GeV',
    # #           'bs':'Bs0 (5 GeV)'}
    # masses = sim10_masses_map
    # plot = []
    # plot0 = []
    # bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'

    # dic = { 'Mass':[],
            
    #         '$\\epsilon(\\gamma)$':[],
    #         #'$\\epsilon(\\gamma^0)$':[],
    #         '$E_{\\textrm{Data}} (\\gamma)$':[],
    #         #'$E_{\\textrm{Data}} (\\gamma^0)$':[],
    #         '$E_{\\textrm{MC}} (\\gamma)$':[],
    #         #'$E_{\\textrm{MC}} (\\gamma^0)$':[],
    #         '$\epsilon (\\aa)$':[],
    #         '$E_{\\textrm{Data}} (\\aa)$':[],
    #         '$E_{\\textrm{MC}} (\\aa)$':[],}
    #         #'$E_{\\textrm{Data}} (\\textrm{L0})$':[],
    #         #'$E_{\\textrm{MC}} (\\textrm{L0})$':[]}
    # dfeffs = pd.DataFrame(dic)
    # #for i in sim10_masses_map:
    # for i in [40,41,45]:
        
    # #for i in ['/scratch46/adrian.casais/49100040_1000-1009_Sim10a-priv.root']:
    # #for i in [50]:
    #           #,'bs']:
    # #for i in [41,42]:
    #     if i == 'sim10': continue
    #     print(10*"**")
    #     print(masses[i])
    #     bs = False
    #     if i=='bs':
    #         bs=True
        
    #     root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    #     if i=='gg':
    #         cut = '(gamma_PT > 3000 & gamma0_PT > 3000) & gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000 and nSPDHits < 450'
    #         myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
    #         df = get_ALPsdf_sim10b(myfile,extracut=cut,background=False,bs=True) 
    #         bs = True
    #     else:
    #         cut = '(gamma0_PT >= 3000 and gamma_PT >=3000) and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000 and nSPDHits < 450'
    #         df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv.root',bs=False,background=False,extracut=cut)
        
        
    #     #df.query(cut,inplace=True)
    #     l0data,dfeffs=convolute_eff(masses[i],df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs)
    # print(dfeffs.to_latex(index=False,escape=False))
        


