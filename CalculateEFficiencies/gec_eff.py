from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTs,etas,spds,get_error,get_error_w,pack_eff,get_ALPsdf,get_ALPsdf_sim10b,sim10_masses_map,ufloat_to_str,bins
import pickle
from uncertainties import ufloat


def nspds_eff(df,den,factor_u=1.09,factor_l=1.15,):

    def helpme(df,den,factor):
        df['nSPDHits']*=factor
        #nSPDhits_den = df['weights']
        nSPDhits_den = np.ones(den)
        nSPDhits_num = df.query('nSPDHits < 450')['weights']
        eff = nSPDhits_num.sum()/nSPDhits_den.sum()
        err_eff = get_error_w(nSPDhits_num,nSPDhits_den)

        df['nSPDHits']/=factor

        return df,eff,err_eff

    df,eff_u,err_eff_u = helpme(df,den,factor_u)
    df,eff_l,err_eff_l = helpme(df,den,factor_l)
    df,eff_c,err_eff_c = helpme(df,den,(factor_u+factor_l)/2.)
    myerr = np.max([abs(eff_u-eff_c),abs(eff_l-eff_c)])
    full_eff = ufloat(eff_c,err_eff_c) + ufloat(0,abs(myerr))

    return df,full_eff.n,full_eff.s

def get_geccolumn(masses=sim10_masses_map,print_table = False):
    
    
    gecdic = {'Mass':[],'GEC':[]}
    gecs = []
    nspdseff = {'Mass':[],'nSPDHits':[]}
    
    for i in masses:
        #df = get_df(i,bs,background=False)
        #root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        root='/scratch47/adrian.casais/ntuples/signal/sim10/new/'
        if i=='gg':
            cut = '(gamma0_PT >= 3000 and gamma_PT >=3000) and B_s0_M >4800 and B_s0_M<20000 and gamma_P > 6000 and gamma0_P > 6000 and B_s0_P > 2000'
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            df = get_ALPsdf_sim10b(myfile,background=False,bs=True) 
            bs = True
        else:
            cut = '(gamma0_PT >= 3000 and gamma_PT >=3000) and B_s0_M >4800 and B_s0_M<20000 and gamma_P > 6000 and gamma0_P > 6000 and B_s0_P > 2000'
            df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root',bs=False,background=False)

        l = 1.23
        u = 1.36
        df,eff,err_eff = nspds_eff(df,df.shape[0],factor_u=u,factor_l=l)
        if i=='gg':
            df,eff,err_eff = nspds_eff(df,df.shape[0],factor_u=u+0.01,factor_l=u-0.01) ## for Bs2gg only use the Bs2phigamma correction
        
        
        #SumEtPRev
        sumetprev = 0.940
        esumetprev= ufloat(sumetprev,0.002)
        
        gec_eff = ufloat(eff,err_eff)*esumetprev
        print('GEC eff=',gec_eff)
        gecdic['GEC'].append(pack_eff(gec_eff.n,gec_eff.s))
        nspdseff['Mass'].append(masses[i])
        nspdseff['nSPDHits'].append(ufloat_to_str(ufloat(eff,err_eff)))
        gecs.append(gec_eff)
    #l0df = pd.DataFrame(l0dic)
    dfeffs = pd.DataFrame(nspdseff)
    if print_table:
        print(dfeffs.to_latex(index=False,escape=False))
    return gecdic['GEC'],gecs

if __name__=='__main__':
    masses = sim10_masses_map
    del masses['pi0pi0']
    #masses = {40:5,41:6}
    get_geccolumn(masses,print_table=True)