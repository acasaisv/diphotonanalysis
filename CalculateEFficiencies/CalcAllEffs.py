import sys
sys.path.append('../')
import pandas as pd
from gen_eff import gen_column
from reco_eff import get_recocolumn
from gec_eff import get_geccolumn
from CalcL0Effs import print_l0column
from StrippingSys import stripping_column
from Hlt1Sys import print_hlt1column
from Hlt2Sys import hlt2_column
from bdt_eff import bdt_column
from helpers import sim10_masses_map,ufloat_eff,ufloat_to_str

from ROOT import *
import numpy as np
import matplotlib.pyplot as plt

import json

from uncertainties import ufloat

def make_plots(effs_dic,generation,reco,gec,strippings,offlines,l0,hlt1,hlt2,bdt,total):
    plt.clf()
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in generation], "-o", label="Generation")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in generation], [eff.n+eff.s for eff in generation], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in reco], "-o", label="Reconstruction")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in reco], [eff.n+eff.s for eff in reco], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in gec], "-o", label="GEC")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in gec], [eff.n+eff.s for eff in gec], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in strippings], "-o", label="PID")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in strippings], [eff.n+eff.s for eff in strippings], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in offlines], "-o", label="Saturation")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in offlines], [eff.n+eff.s for eff in offlines], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in l0], "-o", label="L0")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in l0], [eff.n+eff.s for eff in l0], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in hlt1], "-o", label="HLT1")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in hlt1], [eff.n+eff.s for eff in hlt1], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in hlt2], "-o", label="HLT2")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in hlt2], [eff.n+eff.s for eff in hlt2], alpha=0.25)
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in bdt], "-o", label="BDT")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in bdt], [eff.n+eff.s for eff in bdt], alpha=0.25)
    # plt.plot(effs_dic['Mass [GeV]'][:-1],[eff.n for eff in total[:-1]], "-o", label="Total")
    # plt.fill_between(effs_dic['Mass [GeV]'][:-1],[eff.n-eff.s for eff in total[:-1]], [eff.n+eff.s for eff in total[:-1]], alpha=0.25)
    plt.xlabel("$m_{\gamma\gamma}$ [GeV$/c^2$]")
    plt.ylabel("Efficiency")
    plt.legend()
    plt.tight_layout()
    plt.savefig("efficiency_dependence.pdf")
    plt.savefig("efficiency_dependence.jpg")
    plt.clf()
    plt.plot(effs_dic['Mass [GeV]'],[eff.n for eff in total], "-o", label="Total")
    plt.fill_between(effs_dic['Mass [GeV]'],[eff.n-eff.s for eff in total], [eff.n+eff.s for eff in total], alpha=0.25)
    plt.xlabel("$m_{\gamma\gamma}$ [GeV$/c^2$]")
    plt.ylabel("Efficiency")
    plt.legend()
    plt.tight_layout()
    plt.savefig("efficiency_dependence_total.pdf")
    plt.savefig("efficiency_dependence_total.jpg")


def calc_all_effs(masses):
    effs_dic = {'Mass [GeV]':[]}
    effs_dic['Generation'],generation = gen_column(masses)
    effs_dic['Reconstruction'],reco = get_recocolumn(masses)
    effs_dic['GEC'],gec = get_geccolumn(masses)
    effs_dic['Stripping'] = []
    effs_dic['Saturation'] = []
    #effs_dic['BDT'] = []
    strippings = []
    offlines = []
    aa=" && "
    #effs_dic['Total'] = []
    jsonfile = open("../Saturation/saturation_efficiencies.json", "r") ## can be created with ../Saturation/saturation_systematic.py
    saturation_dict = json.load(jsonfile)
    jsonfile.close()
    for i in masses:
        effs_dic['Mass [GeV]'].append(masses[i])
        root = '/scratch47/adrian.casais/ntuples/signal/'
        if i=='sim10':
            filename = f'b2gg-sim10.root'
            truth = "abs(gamma_MC_MOTHER_ID) == 531 && abs(gamma0_MC_MOTHER_ID) == 531 && gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==531"
        elif i=='gg':
            truth = "abs(gamma_MC_MOTHER_ID) == 531 && abs(gamma0_MC_MOTHER_ID) == 531 && gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==531"
            filename = f'sim10/b2gg-stripping.root'
        else:
            filename = f'sim10/491000{i}_1000-1099_Sim10a-priv.root'
            
            truth="((abs(gamma_MC_MOTHER_ID) == 54 && abs(gamma0_MC_MOTHER_ID) == 54 && gamma_TRUEID==22 && gamma0_TRUEID==22) && B_s0_TRUEID==54)"
            
        f = TFile(root+ filename)
        t0 = f.Get('DTTBs2GammaGamma/DecayTree')
        t = f.Get('MCDecayTreeTuple/MCDecayTree')
        #offline_cut = '(gamma_CaloHypo_Saturation==0 && gamma0_CaloHypo_Saturation==0 && gamma_PT > 3000 && gamma0_PT > 3000)'
        offline_cut = ' (gamma_CaloHypo_Saturation<1 && gamma0_CaloHypo_Saturation<1)'
        pt_cut = ' gamma_PT > 3000 && gamma_PT > 3000'  
        if i=='sim10':
            stripping = ufloat_eff(1064848,7789083)*ufloat_eff(t0.GetEntries(f'{truth} && {pt_cut}'),t.GetEntries())
            #stripping = ufloat_eff(1064848,7789083)
        elif i =='gg':
            offline_cut = ' (gamma_PP_Saturation<1 && gamma0_PP_Saturation<1)'
            stripping = ufloat_eff(t0.GetEntries(f'{truth} && {pt_cut}'),t.GetEntries())
            offline = ufloat(saturation_dict["Bsgg"]["total_diphoton"][0],np.sqrt(saturation_dict["Bsgg"]["total_diphoton"][1]**2+saturation_dict["Bsgg"]["total_diphoton"][2]**2))

        else:
            stripping = ufloat_eff(t0.GetEntries(f'{truth} && {pt_cut}'),t.GetEntries())
            offline = ufloat(saturation_dict["491000"+str(i)]["total_diphoton"][0],np.sqrt(saturation_dict["491000"+str(i)]["total_diphoton"][1]**2+saturation_dict["491000"+str(i)]["total_diphoton"][2]**2))
            
        
        offlines.append(offline)
        #effs_dic['Stripping'].append(ufloat_to_str(stripping))
        effs_dic['Saturation'].append(ufloat_to_str(offline))
    effs_dic['Stripping'],strippings = stripping_column(masses)
    df_effs = pd.DataFrame(effs_dic)
    df_effs['L0'],l0=print_l0column(masses)
    df_effs['HLT1'],hlt1 = print_hlt1column(masses)
    df_effs['HLT2'],hlt2 = hlt2_column(masses)
    df_effs['BDT'],bdt = bdt_column(masses)
    total = np.array(generation)*np.array(reco)*np.array(gec)*np.array(strippings)*np.array(offlines)*np.array(l0)*np.array(hlt1)*np.array(hlt2)*np.array(bdt)
    df_effs['Total'] = [ufloat_to_str(t) for t in total]

    print(effs_dic['Mass [GeV]'])

    make_plots(effs_dic,generation,reco,gec,strippings,offlines,l0,hlt1,hlt2,bdt,total)

    print(df_effs.to_latex(index=False,escape=False))
    with open('effs.txt','w') as handle:
        print(df_effs.to_latex(index=False,escape=False),file=handle)
if __name__ == '__main__':
    masses = sim10_masses_map
    #masses = {
         #'sim10':5.3,
         #40:5,
         #41:6
         #}
    #masses = {'sim10':5.367}
    #del masses[40]
    #del masses[51]
    # del masses['gg']
    del masses['pi0pi0']
    # masses = {
    # #     #41:6,
    # # 41:6,
    # # #51:20
    # 'gg':5.3
    # }
    calc_all_effs(masses)
