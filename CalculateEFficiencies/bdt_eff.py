import xgboost as xgb
import pickle
import uproot3 as uproot
from uncertainties import ufloat
import pandas as pd
from helpers import ufloat_to_str,sim10_masses_map,ufloat_eff
from bdt_helpers import process_variables, get_cuts, relabel_signal
import numpy as np




def bdt_column(masses):
    vars_b,trigger_vars_b,vars_s,trigger_vars_s,rs,rs_1 = process_variables()
    cut_b,cut_s = get_cuts()


    root = '/scratch47/adrian.casais/ntuples/signal/'
    df = {}

    eff_str = []
    eff = []
    for i in masses:
        vars_b,trigger_vars_b,vars_s,trigger_vars_s,rs,rs_1 = process_variables()
        cut_b,cut_s = get_cuts()
        if i=='gg':
            cut_s = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT  > 3000 and gamma0_PT > 3000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1'
            vars_s = list(map(lambda x: x.replace('CaloHypo','PP'),vars_s ))
            trigger_vars_s = list(map(lambda x: x.replace('CaloHypo','PP'),trigger_vars_s ))

            f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root')
        elif i=='pi0pi0':
            cut_s = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT  > 3000 and gamma0_PT > 3000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1'
            vars_s = list(map(lambda x: x.replace('CaloHypo','PP'),vars_s ))
            trigger_vars_s = list(map(lambda x: x.replace('CaloHypo','PP'),trigger_vars_s ))

            f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/sim10/b2pi0pi0.root')

        else:
            cut_s = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT  > 3000 and gamma0_PT > 3000 and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1'
            f_s = uproot.open(root + f'sim10/491000{i}_1000-1099_Sim10a-priv.root')
        
        t_s = f_s['DTTBs2GammaGamma/DecayTree']
        df= t_s.pandas.df(branches=vars_s+trigger_vars_s+['gamma_P','gamma0_P','B_s0_PT'])
        df.query(cut_s,inplace=True)
        if i=='gg' or i=='pi0pi0':
            df["MinPT"]=np.minimum(df["gamma_PT"],df["gamma0_PT"])
            df["MaxPT"]=np.maximum(df["gamma_PT"],df["gamma0_PT"])
        df = relabel_signal(df,rs_1,rs)
        
        if i=='gg' or i=='pi0pi0':
            with open("bdt_Bs.pickle", "rb") as handle:
                bdt = pickle.load(handle)
            df['bdt']=0
            for ibdt in range(len(bdt)):
                df['bdt']+=bdt[ibdt].predict_proba(df[vars_b+["MinPT","MaxPT"]])[:,1]/len(bdt)
        else:
            with open("bdt.pickle", "rb") as handle:
                bdt = pickle.load(handle)            
            df['bdt']=0
            for ibdt in range(len(bdt)):
                df['bdt']+=bdt[ibdt].predict_proba(df[vars_b])[:,1]/len(bdt)


        myeff = df.query('bdt>0.9').shape[0]/df.shape[0]
        myeff =ufloat_eff(df.query('bdt>0.9').shape[0],df.shape[0])
        myeff+= ufloat(0,myeff.n*0.01)
        if i=='gg' or i=='pi0pi0':
            myeff = df.query('bdt>0.85').shape[0]/df.shape[0]
            myeff =ufloat_eff(df.query('bdt>0.85').shape[0],df.shape[0])
            myeff+= ufloat(0,myeff.n*0.01)            
        eff.append(myeff)
        eff_str.append(ufloat_to_str(myeff))
    return eff_str,eff

if __name__=='__main__':
    # print(bdt_column(sim10_masses_map))
    print(bdt_column(['gg','pi0pi0']))

