from uncertainties import ufloat

gen = ufloat(18.6,0.5)/100.
stripping = ufloat(16.235,0)/100.
offline = ufloat(66.153,0)/100.
l0 = ufloat(45.73,0)/100.
hlt1 = ufloat(31.1,2.0)/100.
hlt2 = ufloat(73,5)/100.
bdt = ufloat(20.8,0)/100.

print(100.*gen*stripping*offline*l0*hlt1*hlt2*bdt)