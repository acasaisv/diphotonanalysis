#!/bin/bash
wrapperfunction() {
    source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh 
}

wrapperfunction

export APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r407/options
export LBPYTHIA8ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v55r4/Gen/LbPythia8
export DECFILESROOT=/home3/adrian.casais/cmtuser/GaussDev_v55r4/Gen/DecFiles
export MADGRAPHDATAROOT=/home3/adrian.casais/cmtuser/GaussDev_v55r4/Gen/MadgraphData
export LBMADGRAPHROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v55r4/Gen/LbMadgraph
export GAUSSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v55r4/Sim/Gauss/options
export myroot=/home3/adrian.casais/diphotonanalysis/simulation/condor
export extraOps=/home3/adrian.casais/diphotonanalysis/simulation/condor/options
export DDDBTag="dddb-20210528-8"
export CondDB="sim-20201113-8-vc-md100-Sim10"
export cluster=1000
export proc=1
export i=$1
echo $1
mkdir -p gen_eff/$i && cd gen_eff/$i
lb-run -c best gauss/v55r4 $myroot/wrap_gauss.sh gaudirun.py ${GAUSSOPTS}/Gauss-2018.py $DECFILESROOT/options/491000$i.py ${LBMADGRAPHROOT}/options/MadgraphPythia8.py $extraOps/optsGauss.py $GAUSSOPTS/GenStandAlone.py