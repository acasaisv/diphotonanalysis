from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTsHlt2,etasHlt2,spdsHlt2,get_error,get_error_w,pack_eff,get_ALPsdf,sim10_masses_map,bins,ufloat_to_str,ufloat_eff,get_ALPsdf_sim10b
import pickle
from uncertainties import ufloat
# pTs = pTsHlt2
# etas = etasHlt2
# spds = spdsHlt2

def convolute_eff(nspdseff,nspdserr,m,df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs):
    eff_gamma=0
    erreff_gamma2=0
    effMC_gamma=0
    erreffMC_gamma2=0
    eff_gamma0=0
    erreff_gamma02=0
    effMC_gamma0=0
    erreffMC_gamma02=0
    
    sum_fractions = 0

    eff_bs =0
    erreff_bs2=0
    
    #den = df.query('gamma_PT >= 3000')['weights']
    #den0 = df.query('gamma0_PT >= 3000')['weights']
    df.query('gamma0_PT >= 3000 and gamma_PT >=3000',inplace=True)
    den = den0 = denbs = df.query('B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and gamma0_P>6000 and B_s0_M > 4800 and B_s0_M < 20000')['weights']
    
    total = 0
    for ipt in range(len(pTs)-1):
        pT = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                
                

                df_ =  df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & gamma_Matching >= {4} & gamma_Matching< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =  df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & gamma_Matching >= {4} & gamma_Matching< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                
                # df_ =  df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                # df0_ =  df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                
                num = df_['weights']
                fraction = num.sum()/den.sum()
                total +=fraction
                err_fraction = get_error_w(num,den)

                eff_gamma += fraction*efficiencies[pT][eta][spd]
                erreff_gamma2 += err_fraction**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction**2

                effMC_gamma += fraction*efficienciesMC[pT][eta][spd]
                
                erreffMC_gamma2 += err_fraction**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction**2
                print(f"Fraction:{100.*fraction} \%. pt: {pT},eta:{eta},nsdps:{spd}")
                print(f"EffMC: {100.*effMC_gamma}. Eff: {100.*eff_gamma}")
                num0 = df0_['weights']
                fraction0 = num0.sum()/den0.sum()
                err_fraction0 = get_error_w(num0,den0)

                eff_gamma0 += fraction0*efficiencies[pT][eta][spd]
                erreff_gamma02 += err_fraction0**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction0**2

                effMC_gamma0 += fraction0*efficienciesMC[pT][eta][spd]
                erreffMC_gamma02 += err_fraction0**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction0**2


    print("Fraction total",total)          
    cutl0 = '(gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma_PP_IsPhoton > 0.85)'
    cutl00 = '(gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsPhoton > 0.85)'
    cutbs = f'{cutl0} and {cutl00}'
    cutkin =  '(gamma_PT >= 3000)'
    cutkin0 = '(gamma0_PT >= 3000)'
    cutkin = cutkin0 = cutkinbs = 'B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and gamma0_P>6000 and B_s0_M > 4800 and B_s0_M < 20000'


    num_w = df.query("{0} & {1} ".format(cutl0,cutkin))['weights']
    den_w = df.query(cutkin)['weights']
    l0tos =  num_w.sum()/den_w.sum()
    errl0tos = get_error_w(num_w,den_w)

    num0_w = df.query("{0} & {1} ".format(cutl00,cutkin0))['weights']
    den0_w = df.query(cutkin0)['weights']
    l0tos0 =  num0_w.sum()/den0_w.sum()
    errl0tos0 = get_error_w(num0_w,den0_w)
    
    numbs_w = df.query("{0} & {1} ".format(cutbs,cutkinbs))['weights']
    denbs_w = df.query(cutkinbs)['weights']
    l0tosbs =  numbs_w.sum()/denbs_w.sum()
    errl0tosbs = get_error_w(numbs_w,denbs_w)


    bsmcefferror = np.sqrt(erreffMC_gamma02 +erreffMC_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)
    bsefferror = np.sqrt(erreff_gamma02 +erreff_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)

    # effbsmc =  (effMC_gamma0+effMC_gamma)*l0tosbs/(l0tos+l0tos0)
    # effbs = (eff_gamma0+eff_gamma)*l0tosbs/(l0tos+l0tos0)
    
    #effbsmc =  (effMC_gamma0+effMC_gamma) - effMC_gamma0*effMC_gamma
    #effbs = (eff_gamma0+eff_gamma) - effMC_gamma0+effMC_gamma

    espds = ufloat(nspdseff,nspdserr)
    emcg0 = ufloat(effMC_gamma0,np.sqrt(erreffMC_gamma02))
    emcg1 = ufloat(effMC_gamma,np.sqrt(erreffMC_gamma2))
    edatag0 = ufloat(eff_gamma0,np.sqrt(erreff_gamma02))
    edatag1 = ufloat(eff_gamma,np.sqrt(erreff_gamma2))
    el0tos0 = ufloat(l0tos0,errl0tos0)
    el0tos1 = ufloat(l0tos,errl0tos)
    el0tosbs = ufloat(l0tosbs,errl0tosbs)
    print("Cut based combined:",l0tosbs)
    print("Cut based photon product:",l0tos*l0tos0)
    print("Product calibrated from etammg MC:",emcg0*emcg1)
    print("Product calibrated from etammg Data:",edatag0*edatag1)

    bsmc = (emcg0*emcg1)
    bsdata = (edatag0*edatag1)

    fulll0mc = bsmc*espds
    fulll0data = bsdata*espds
    
    output_eff = espds*(bsdata + ufloat(0,bsmc.s/bsmc.n*bsdata.n)+ufloat(0,abs((el0tos0*el0tos1).n - l0tosbs)/(el0tos0*el0tos1).n))

    effarray = np.array((   m,
                            #pack_eff(100*nspdseff,100*nspdserr),
                            pack_eff(l0tos,errl0tos), # plain MC eff photon 1
                            pack_eff(l0tos0,errl0tos0), # plain MC eff photon 2
                            pack_eff(effMC_gamma,np.sqrt(erreffMC_gamma2)), # eff calibrated from MC photon 1
                            pack_eff(effMC_gamma0,np.sqrt(erreffMC_gamma02)), # eff calibrated from MC photon 1
                            pack_eff(eff_gamma,np.sqrt(erreff_gamma2)), # eff calibrated from data photon 1
                            pack_eff(eff_gamma0,np.sqrt(erreff_gamma02)), # eff calibrated from data photon 2
                            pack_eff(l0tosbs,errl0tosbs), # plain MC eff both photons together
                            pack_eff((el0tos0*el0tos1).n,(el0tos0*el0tos1).s), # plain MC eff product of individual photons
                            pack_eff(bsmc.nominal_value,bsmc.std_dev), # eff calibrated from MC product of both photons
                            pack_eff(bsdata.nominal_value,bsdata.std_dev), # eff calibrated from data product of both photons
                            #pack_eff((espds*el0tosbs).n,(espds*el0tosbs).s), # plain MC eff both photons together, incl. kin cut
                            #pack_eff(fulll0mc.nominal_value,fulll0mc.std_dev), # eff calibrated from MC product of both photons, including kin cut
                            # pack_eff(fulll0data.nominal_value,fulll0data.std_dev) # eff calibrated from data product of both photons, including kin cut
                            #pack_eff(output_eff.n,output_eff.s) # eff calibrated from data product of both photons, including kin cut
                            )
                            )
    output_eff = espds*(bsdata + ufloat(0,bsmc.s/bsmc.n*bsdata.n)+ufloat(0,abs((el0tos0*el0tos1).n - l0tosbs)/(el0tos0*el0tos1).n))
    if dfeffs.shape == (0,0):
        pass
    else:
       dfeffs = dfeffs.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeffs)),ignore_index=True) 
    return output_eff,dfeffs
    
def reweight_alps_df(dfalps,dfeta):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = [#'eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfalps['nSPDHits'], target=1.7*dfalps['nSPDHits'], target_weight=dfalps['weights'])
    dfalps['weights']= reweighter.predict_weights(dfalps['nSPDHits'])
    return dfalps

def stripping_column(masses):
    effs_ufloat = []
    mybin = bins['stripping']
    spds,etas,pts = mybin['matching'],mybin['etas'],mybin['pts']
    with open('StrippingEfficiencies.p','rb') as file:
        efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    for i in masses:
        if i=='sim10':
            root = '/scratch47/adrian.casais/ntuples/signal/'
            filename = f'b2gg-sim10.root'
            truth = "abs(gamma_MC_MOTHER_ID) == 531 && abs(gamma0_MC_MOTHER_ID) == 531 && gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==531"
            f = TFile(root+ filename)
            t0 = f.Get('DTTBs2GammaGamma/DecayTree')
            t = f.Get('MCDecayTreeTuple/MCDecayTree')
            strip_eff = ufloat_eff(1064848,7789083)*ufloat_eff(t0.GetEntries(f'{truth} && gamma_PT > 3000 && gamma0_PT > 3000' ),t.GetEntries())*1.048 # correcting for data-sim differences for now
            strip_eff += ufloat(0,np.sqrt((0.07/0.60)**2+(0.005/0.7195)**2)*strip_eff.n) # correcting for data-sim differences for now
        else:
            kin_cut = 'B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and gamma0_P>6000 and B_s0_M > 4800 and B_s0_M < 20000 and nSPDHits <450'
            root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
            if i=='gg':
                myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
                
                df = get_ALPsdf_sim10b(myfile,background=False,bs=True,
                extravars=['gamma_CL',
                           'gamma_PP_CaloNeutralHcal2Ecal',
                           'gamma_PP_IsPhoton',
                           'gamma0_CL',
                           'gamma0_PP_CaloNeutralHcal2Ecal',
                           'gamma0_PP_IsPhoton' 
                            ],stripping=False,extracut=kin_cut)
            else:
                df = get_ALPsdf_sim10b(i,bs=False,background=False,
                extravars=['gamma_CL',
                'gamma_PP_CaloNeutralHcal2Ecal',
                'gamma_PP_IsPhoton',
                'gamma0_CL',
                'gamma0_PP_CaloNeutralHcal2Ecal',
                'gamma0_PP_IsPhoton' 
                ],stripping=False,extracut=kin_cut)
            pre_den = df['weights']
            pre_num = df.query(kin_cut)['weights']
            
            #n_truth = t_t.GetEntries()
            #weights_truth = np.ones(n_truth)
            #pre_den = weights_truth
            # eff = pre_num.sum()/pre_den.sum()
            # err_eff = get_error_w(pre_num,pre_den)
            eff,err_eff = 1.0,0.0
            strip_eff,_ = convolute_eff(eff,err_eff,masses[i],df.query(kin_cut),pts,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,pd.DataFrame())
        effs_ufloat.append(strip_eff)
    return [ufloat_to_str(u) for u in effs_ufloat],effs_ufloat

def plots(alps,etammg,etammgMC):
    plt.clf()
    fig,ax = plt.subplots(1,1)
    cut  = 'gamma_PT > 3000 and gamma_PT < 10000 and gamma_ETA >2 and gamma_ETA <3 and nSPDHits <450'
    cut = 'gamma_PT >0'
    myalps = alps.query(cut)
    myetammgmc = etammgMC.query(cut)
    myetammg = etammg.query(cut)
    ax.hist(myalps['gamma_CL'],bins=50,weights=myalps['weights'],color='red',density=True,histtype='step',label='alps')
    ax.hist(myetammg['gamma_PP_IsNotH'],bins=50,weights=myetammg['sweights'],color='blue',density=True,histtype='step',label='etammgdata')
    ax.hist(myetammgmc['gamma_PP_IsNotH'],bins=50,weights=myetammgmc['sweights'],color='green',density=True,histtype='step',label='etammgmc')
    plt.legend(loc='upper left')
    fig.savefig('isnoth.pdf')
    #print('Fraction sum = {}'.format(sum_fractions))

if __name__ == '__main__':
    import pickle
    with open('StrippingEfficiencies.p','rb') as file:
        efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    # masses = {40:'5 GeV',
    #           42:'7 GeV',
    #           44:'9 GeV',
    #           46:'11 GeV',
    #           47:'13 GeV',
    #           50:'15 GeV',
    #           48:'4 GeV',
    #           'bs':'Bs0 (5 GeV)'}
    masses = sim10_masses_map
    del masses['pi0pi0']
    plot = []
    plot0 = []
    bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'
    mybin = bins['stripping']
    pts,etas,spds = mybin['pts'],mybin['etas'],mybin['matching']
    dic = { 'Mass [GeV]':[],
            #'nSPDHits':[] ,
            '$\\epsilon^{\\textrm{plain}}(\\gamma)$':[],
            '$\\epsilon^{\\textrm{plain}}(\\gamma^0)$':[],
            '$E^{\\textrm{calib}}_{\\textrm{MC}} (\\gamma)$':[],
            '$E^{\\textrm{calib}}_{\\textrm{MC}} (\\gamma^0)$':[],
            '$E^{\\textrm{calib}}_{\\textrm{Data}} (\\gamma)$':[],
            '$E^{\\textrm{calib}}_{\\textrm{Data}} (\\gamma^0)$':[],
            '$\epsilon^{\\textrm{plain}}(\\aa)$':[],
            '$(\epsilon(\\gamma_1) \\times \\epsilon(\\gamma_2))^{\\textrm{plain}}$':[],
            '$E^{\\textrm{calib}}_{\\textrm{MC}} (\\aa)$}':[],
            '$E^{\\textrm{calib}}_{\\textrm{Data}} (\\aa)$':[],
            #'$\epsilon^{\\textrm{plain}}(\\textrm{PID,kin})$':[],
            #'$E^{\\textrm{calib}}_{\\textrm{MC}} (\\textrm{PID,kin})$':[],
            #'$E^{\\textrm{calib}}_{\\textrm{Data}} (\\textrm{PID,kin})$':[],
            }
    dfeffs = pd.DataFrame(dic)
    for i in masses:
    #for i in [40]:
    #for i in ['/scratch46/adrian.casais/49100040_1000-1009_Sim10a-priv.root']:
    #for i in [50]:
              #,'bs']:
    #for i in [40,45,51]:
        print(10*"**")
        print(masses[i])
        bs = False
        kin_cut = 'B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and gamma0_P>6000 and B_s0_M > 4800 and B_s0_M < 20000 and nSPDHits <450'
        if i=='bs':
            bs=True
        
        if i=='sim10':
            continue
        #df = get_df(i,bs,background=False)
        if i=='gg':
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            df = get_ALPsdf_sim10b(myfile,background=False,bs=True,
     extravars=['gamma_CL',
                'gamma_PP_CaloNeutralHcal2Ecal',
                'gamma_PP_IsPhoton',
                'gamma0_CL',
                'gamma0_PP_CaloNeutralHcal2Ecal',
                'gamma0_PP_IsPhoton' 
                ],stripping=False,extracut=kin_cut)
            root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
            f_t = TFile(root+f'../b2gg-stripping.root')
            t_t = f_t.Get('MCDecayTreeTuple/MCDecayTree')
        else:
            root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
            df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root',bs,background=False,extravars=['gamma_CL',
            'gamma_PP_CaloNeutralHcal2Ecal',
            'gamma_PP_IsPhoton',
            'gamma0_CL',
            'gamma0_PP_CaloNeutralHcal2Ecal',
            'gamma0_PP_IsPhoton' 
            ],stripping=False,extracut=kin_cut)
            f_t = TFile(root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root')
            t_t = f_t.Get('MCDecayTreeTuple/MCDecayTree')
        #df.query('nSPDHits < 450',inplace=True)
        
        #n_truth = t_t.GetEntries()
        #weights_truth = np.ones(n_truth)
        
        stripping_cut = 'B_s0_PT > 2000 and gamma_PT > 3000 and gamma0_PT > 3000 and gamma_P > 6000 and gamma0_P > 6000 and B_s0_M > 4800 and B_s0_M < 20000'
        nSPDhits_num = df.query(stripping_cut)['weights']
        nSPDhits_den = df['weights']
        # eff = nSPDhits_num.sum()/nSPDhits_den.sum()
        # err_eff = get_error_w(nSPDhits_num,nSPDhits_den)
        eff,err_eff = 1.0,0.0
        print('kinematic eff= {0:.2f} +- {1:.2f}'.format(100.*eff,100.*err_eff))
        dfeta =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalDataStripping.h5',key='df')
        dfetamc =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMCStripping.h5',key='df')
        plots(df,dfeta,dfetamc)
        #df.query('nSPDHits<450',inplace=True)

        _,dfeffs=convolute_eff(eff,err_eff,masses[i],df.query(stripping_cut ),pts,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs)

        
    dfeffs.drop(columns=[
        '$\\epsilon^{\\textrm{plain}}(\\gamma^0)$',
        '$E^{\\textrm{calib}}_{\\textrm{MC}} (\\gamma^0)$',
        #'$E^{\\textrm{calib}}_{\\textrm{MC}} (\\aa)$}',
        #'$\epsilon^{\\textrm{plain}}(\\textrm{PID,kin})$',
        #'$E^{\\textrm{calib}}_{\\textrm{MC}} (\\textrm{PID,kin})$',
        '$E^{\\textrm{calib}}_{\\textrm{Data}} (\\gamma^0)$'
        ],inplace=True)
    print(dfeffs.to_latex(index=False,escape=False))