from ROOT import *
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
#from kinematic_bins import pTs,etas
from helpers import pTsHlt1,etasHlt1,etas,nearest_rect
from plot_helpers import *
#import mplhep
#mplhep.style.use("LHCb2")

import pickle

pTs = pTsHlt1
etas = etasHlt1
def load_dfs():
    root = '/scratch47/adrian.casais/ntuples/turcal'
    dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')
    df = pd.read_hdf(root+'/etammgTurcalData.h5',key='df')
    df.query('gamma_PT > 3000',inplace=True)
    df.query('eta_M > 465 & eta_M < 630',inplace=True)
    
    df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    #df.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    dfMC.query('gamma_PT > 3000',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    dfMC.query('eta_M > 465 & eta_M < 630',inplace=True)
    #dfMC.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    for d in df,dfMC:
        d['deltaET'] = d['gamma_PT'] - d['gamma_L0Calo_ECAL_TriggerET']
        #d['deltaET'] = d['gamma_L0Calo_ECAL_TriggerET']
    
    
    df.reset_index(inplace=True)
    dfMC.reset_index(inplace=True)
    
    #df.query('deltaET < 5000',inplace=True)
    #dfMC.query('deltaET < 5000',inplace=True)
    #final_df.query('deltaET > 2400',inplace=True)
    return df,dfMC

def reweight_mc_df(df,dfMC):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = ['eta_PT','eta_ETA',#'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]

    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],
                                                 original_weight=dfMC['sweights']
                                                 )
    return

def mcvsdatatest(df,dfMC):
    xlimits = ylimits = [1000,6120]
    fig, ax = plt.subplots(2,4,figsize=(16,9),dpi=80)
    ax = ax.flatten()
    nbins=50
    ax[0].set_title('gamma PT')
    ax[0].hist(df['gamma_PT'],
             weights=df['sweights'],
             bins=nbins,range=(2000,10000),density=True,alpha=0.5,histtype='step')
    ax[0].hist(dfMC['gamma_PT'],
             weights=dfMC['sweights'],
             bins=nbins,range=(2000,10000),density=True,alpha=0.5,histtype='step')
    
    ax[0].legend(('Data','MC'))
    ax[1].set_title('trigger ET')
    ax[1].hist(df['gamma_L0Calo_ECAL_TriggerET'],
             weights=df['sweights'],
             bins=nbins,range=xlimits,density=True,alpha=0.5,histtype='step')
    ax[1].hist(dfMC['gamma_L0Calo_ECAL_TriggerET'],
             weights=dfMC['sweights'],
             bins=nbins,range=xlimits,density=True,alpha=0.5,histtype='step')
    ax[2].set_title('gamma P')
    ax[2].hist(df['gamma_P'],
             weights=df['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[2].hist(dfMC['gamma_P'],
             weights=dfMC['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[3].set_title('eta P')
    ax[3].hist(df['eta_P'],
             weights=df['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[3].hist(dfMC['eta_P'],
             weights=dfMC['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[4].set_title('eta PT')
    ax[4].hist(df['eta_PT'],
             weights=df['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[4].hist(dfMC['eta_PT'],
             weights=dfMC['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[5].set_title('eta ETA')
    ax[5].hist(df['eta_ETA'],
             weights=df['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    ax[5].hist(dfMC['eta_ETA'],
             weights=dfMC['sweights'],
             bins=nbins,
               #range=xlimits,
               density=True,alpha=0.5,histtype='step')
    
    
def trim_df(df,pTs):
    thresholds = 0 
    final_df = pd.DataFrame()
    for pt in pTs:
        df_ = df.query('gamma_PT > {0} & gamma_PT < {1}'.format(pt[0],pt[1]))
        if pt[1] <= 4500:
            df_.query('deltaET < 2000',inplace=True)
        elif pt[1] <= 5500:
            df_.query('deltaET < 2500',inplace=True)
        elif pt[1] <= 7500:
            df_.query('deltaET < 4000',inplace=True)
        elif pt[1] <= 9500:
            df_.query('deltaET < 5000',inplace=True)
        elif pt[1]<= 13500:
            df_.query('deltaET < 7000',inplace=True)
        elif pt[1]<= 16500:
            df_.query('deltaET < 12000',inplace=True)
        final_df = final_df.append(df_)
    final_df.reset_index(inplace=True)

    return final_df

            
    

    
    

def get_distrib(var,weights,name='name'):
    pk,xk = np.histogram(var,
                         bins=30,
                         #density=True,
                         weights=weights
                         )
    width = np.diff(xk)
    xk = (xk[1:] + xk[:-1])/2
    dx = xk[1]-xk[0]
    
    pk = pk*dx
    
    offset = min(pk)
    pk = np.array([z - offset +1 for z in pk])
    pk = pk/sum(pk)
    return (pk,xk)

def plot_smearing_distribs(histograms,histogramsMC,n_plots=len(pTs)-1,eta=(1, np.inf),savefile=''):
    
    xplots,yplots = nearest_rect(n_plots)
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(xplots,yplots,dpi=dpi,figsize=(8,8))
    #n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    
    #for pT,i in zip(pTs,range(n_plots)):
    for ipt in range(n_plots):
        i=ipt
        pT = (pTs[ipt],pTs[ipt+1])
        myhist = histogramsMC[eta][pT]
        myhistData = histograms[eta][pT]
        ax_flat[i].set_title(r'$p_T$ in ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))
        ax_flat[i].step(x=myhist[1],y=myhist[0],where='mid',alpha=0.7,color='red')
        
        ax_flat[i].step(x=myhistData[1],y=myhistData[0],where='mid',alpha=0.7)
        ax_flat[i].fill_between(myhistData[1],myhistData[0],step='mid',alpha=0.7)
        ax_flat[i].set_xlabel(r'$p_T(\mathrm{offline}) - p_T(L0)$ [MeV]',horizontalalignment='right',x=1.0)

        #ax_flat[i].step(myhist[1],myhist[0],width=myhist[2],alpha=0.6)
        #ax_flat[i].step(myhistData[1],myhistData[0],width=myhistData[2],alpha=0.6)


    ax_flat[0].legend(('sim10 2018 MC','TURCAL 2018 Data'),fontsize=10)
    plt.tight_layout()
    plt.savefig(savefile)

def plot_deltaet_distribs(df,dfMC,savefile='hlt1_deltaetdistribs.pdf',n_plots=len(pTs)-1):
    # xplots,yplots = nearest_rect(n_plots)
    # dpi = 80.
    # px = 1./dpi
    fig,ax = plt.subplots(1,3,figsize=(16,6))
    #n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    for iplot in range(n_plots-1):
        pT = (pTs[iplot],pTs[iplot+1])
        ax_flat[iplot].set_title(r'$p_T$ in ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))
        ax_flat[iplot].hist(df.query(f'gamma_PT > {pT[0]} and gamma_PT <{pT[1]}')['deltaET'],label='TURCAL 2018 Data',density=True,bins=35,alpha=0.7)
        ax_flat[iplot].hist(dfMC.query(f'gamma_PT > {pT[0]} and gamma_PT <{pT[1]}')['deltaET'],label='sim10 2018 MC',density=True,color='red',histtype='step',bins=35)
        ax_flat[iplot].set_xlabel(r'$p_T(\mathrm{offline}) - p_T(L0)$ [MeV]',horizontalalignment='right',x=1.0)
    ax_flat[0].legend()
    plt.tight_layout()
    plt.savefig(savefile)



def output_distrib(df_,dfMC_):
    df = df_.query('gamma_L0Calo_ECAL_TriggerET > 10')
    dfMC = dfMC_.query('gamma_L0Calo_ECAL_TriggerET > 10')
    
    distributions = {}
    distributionsMC = {}
    histograms = {}
    histogramsMC = {}
    for ieta in range(len(etas)-1):
        eta = (etas[ieta],etas[ieta+1])
        distributions[eta] = {}
        distributionsMC[eta] = {}
        histograms[eta] = {}
        histogramsMC[eta] = {}
        for ipt in range(len(pTs)-1):
            pt = (pTs[ipt],pTs[ipt+1])
            
            df_ = df.query('gamma_PT > {0} & gamma_PT < {1} & gamma_ETA > {2} & gamma_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))
            dfMC_ = dfMC.query('gamma_PT > {0} & gamma_PT < {1} & gamma_ETA > {2} & gamma_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))
            print(len(dfMC_))
            histograms[eta][pt] = get_distrib(df_['deltaET'],df_['sweights'],name='eta={0},pt={1}'.format(eta,pt))
            histogramsMC[eta][pt] = get_distrib(dfMC_['deltaET'],dfMC_['sweights'],name='MCeta={0},pt={1}'.format(eta,pt))

            #histograms[eta][pt] = get_distrib(df_['gamma_L0Calo_ECAL_TriggerET'],df_['sweights'],name='eta={0},pt={1}'.format(eta,pt))
            #histogramsMC[eta][pt] = get_distrib(dfMC_['gamma_L0Calo_ECAL_TriggerET'],dfMC_['sweights'],name='MCeta={0},pt={1}'.format(eta,pt))
        
        with open ('distributions.p','wb') as handle:
            pickle.dump( [histograms,histogramsMC],handle)
    
    return histograms,histogramsMC



#Testing functions
def test_smearing(df_,dfMC_,histograms,histogramsMC):
    df = df_.query('gamma_L0Calo_ECAL_TriggerET > 10')
    dfMC = dfMC_.query('gamma_L0Calo_ECAL_TriggerET > 10')
    df.reset_index(inplace=True)
    dfMC.reset_index(inplace=True)
    
    gamma = 'gamma'
    df_output = pd.DataFrame()
    dfMC_output = pd.DataFrame()

    for ieta in range(len(etas)-1):
        eta = (etas[ieta],etas[ieta+1])

        for ipt in range(len(pTs)-1):
            pt = (pTs[ipt],pTs[ipt+1])
            dfloc  = df.query('{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format(gamma,pt[0],pt[1],eta[0],eta[1]))
            dfMCloc  = dfMC.query('{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format(gamma,pt[0],pt[1],eta[0],eta[1]))
            pk,xk = histograms[eta][pt]
            pkMC,xkMC = histogramsMC[eta][pt]
            dfloc.loc[:,'rvs'] = np.random.choice(xk,len(dfloc),p=pk)
            dfMCloc.loc[:,'rvs'] = np.random.choice(xkMC,len(dfMCloc),p=pkMC)
            df_output = df_output.append(dfloc.eval('smearedPT=gamma_PT -rvs') )
            dfMC_output = df_output.append(dfMCloc.eval('smearedPT=gamma_PT -rvs') )
            #df_output = df_output.append(dfloc.eval('smearedPT=rvs') )
            #dfMC_output = df_output.append(dfMCloc.eval('smearedPT=rvs') )


            
            
        
    df_output.query('smearedPT>2380',inplace=True)
    dfMC_output.query('smearedPT>2380',inplace=True)

    df_output.loc[df_output['smearedPT']>=6119.99,'smearedPT']=6120
    dfMC_output.loc[dfMC_output['smearedPT']>=6119.99,'smearedPT']=6120


    return df_output,dfMC_output
    
def plot_et(df,n_plots=len(pTs)-1,eta=(1.7, 6),sub='MC',savedir=''):
    xplots,yplots = nearest_rect(n_plots)
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(xplots,yplots,figsize=(10,10))
    n_plots = xplots*yplots

    n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    
    for ipt,i in zip(range(len(pTs)-1),range(n_plots)):
        pT = (pTs[ipt],pTs[ipt+1])
        ax_flat[i].set_title(r'$p_T \ \in$ ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))

        df_  = df.query('gamma_PT > {0} & gamma_PT < {1}'.format(pT[0],pT[1]))
        ax_flat[i].hist(df_['smearedPT'],bins=40,alpha=0.6,
                         #histtype='step',
                         density=True,
                         #color='blue'
                         )
        #ax_flat[i].hist(df_['smearedPT'.format(gamma)],bins=40,alpha=0.6)
        ax_flat[i].hist(df_['gamma_L0Calo_ECAL_TriggerET'],bins=40,alpha=0.6,
                        histtype='step',density=True,color='red'
                        )
                
        ax_flat[i].set_xlabel(r'$p_T(L0)$ [MeV]')
    ax_flat[0].legend((
                       rf'$\Pi_T^\mathrm{{{sub}}}$',
                       #'Data smearing',
                       r'$p_T^{\mathrm{L0}}$'
                       ))
    
    #.set_xlabel('MeV')
    plt.tight_layout()
    plt.savefig(savedir)

def plot_both_et(dfMC,dfData,n_plots=len(pTs)-1,savedir=''):
    xplots,yplots = nearest_rect(n_plots)
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(xplots,yplots,dpi=dpi,figsize=(9,9))
    n_plots = xplots*yplots

    n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    
    for ipt,i in zip(range(len(pTs)-1),range(n_plots)):
        pT = (pTs[ipt],pTs[ipt+1])
        ax_flat[i].set_title(r'$p_T$ in ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))

        dfData_  = dfData.query('gamma_PT > {0} & gamma_PT < {1}'.format(pT[0],pT[1]))
        dfMC_  = dfMC.query('gamma_PT > {0} & gamma_PT < {1}'.format(pT[0],pT[1]))
        #ax_flat[i].hist(df_['smearedPT'],bins=40,alpha=0.6,
                        #histtype='step')
        #ax_flat[i].hist(df_['smearedPT'.format(gamma)],bins=40,alpha=0.6)
        ax_flat[i].hist(dfData_['gamma_L0Calo_ECAL_TriggerET'],bins=25,alpha=0.6,
                        #histtype='step',
                        density=True
                        )
        ax_flat[i].hist(dfMC_['gamma_L0Calo_ECAL_TriggerET'],bins=25,alpha=0.6,
                        histtype='step',density=True,color='red'
                        )
                
        ax_flat[i].set_xlabel(r'$p_T(L0)$ [MeV]',horizontalalignment='right', x=1.0,fontsize=13)
    ax_flat[-1].legend((
                       '2018 TURCAL Data',
                       'sim10 MC'
                       ),loc='center',
                       #bbox_to_anchor=(0.5,0.5)
                       framealpha=0.6 
                       )
    
    #.set_xlabel('MeV')
    plt.tight_layout()
    plt.savefig(savedir)
    



def howmanyL0(df,dfMC):
    dfTOS = df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS')
    dfMCTOS = dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS')
    print("Fraction of events with cluster linking with TOS (data): {:.2f} %".format(100.*np.sum(dfTOS.query('gamma_L0Calo_ECAL_TriggerET > 0')['sweights'])/np.sum(dfTOS['sweights'])))
    print("Fraction of events with cluster linking with TOS (simulation): {:.2f} %".format(100.*np.sum(dfMCTOS.query('gamma_L0Calo_ECAL_TriggerET > 0')['sweights'])/np.sum(dfMCTOS['sweights'])))

def compare_1d(df,dfMC):
    dfLinked =df.query('gamma_L0Calo_ECAL_TriggerET > 2900')
    dfMCLinked =dfMC.query('gamma_L0Calo_ECAL_TriggerET > 2900')
    xlimits = ylimits = [1000,12000]
    fig, (ax0,ax1) = plt.subplots(1,2)
    ax0.title.set_text('MC')
    ax0.set_xlabel('PT(MeV)')
    ax0.set_ylabel('No. counts')
    ax1.title.set_text('Data')
    ax1.set_xlabel('PT (MeV)')
    ax1.set_ylabel('No. counts')
    ax0.hist(df['gamma_PT'],bins=50,density=True,weights=df['sweights'],range=xlimits,alpha=0.4,histtype='step')
    ax0.hist(dfLinked['gamma_PT'],bins=50,density=True,weights=dfLinked['sweights'],range=xlimits,alpha=0.4,histtype='step')
    ax0.legend(["L0TOS events","L0TOS & Linked"],bbox_to_anchor=[0.75,1.15])
    ax1.hist(dfMC['gamma_PT'],bins=50,density=True,range=xlimits,alpha=0.4,weights=dfMC['sweights'],histtype='step')
    ax1.hist(dfMCLinked['gamma_PT'],bins=50,density=True,range=xlimits,alpha=0.4,weights=dfMCLinked['sweights'],histtype='step')
    ax1.legend(["L0TOS events","L0TOS & Linked"],bbox_to_anchor=[0.75,1.15])
    

if __name__=='__main__':
    df,dfMC = load_dfs()
    #df.query('gamma_L0Calo_ECAL_TriggerET > 2500',inplace=True)
    #dfMC.query('gamma_L0Calo_ECAL_TriggerET > 2500',inplace=True)
    print(dfMC.keys())
    #reweight_mc_df(df,dfMC)
    #mcvsdatatest(df,dfMC)
    #df,dfMC = trim_df(df,pTs),trim_df(dfMC,pTs)

    histograms,histogramsMC=output_distrib(df,dfMC)
    print("distribs out")
    #plot_smearing_distribs(histograms,histogramsMC,n_plots=len(pTs)-1,savefile='./hlt1-deltaetdistribs.pdf')
    plot_deltaet_distribs(df,dfMC,n_plots=len(pTs)-1,savefile='./hlt1-deltaetdistribs.pdf')
    rootsave='/home3/adrian.casais/figstoscp/'
    plot_both_et(dfMC,df,n_plots=len(pTs)-1,savedir='./hlt1-l0etCompar.pdf')
    #tests
    newdf,newdfMC = test_smearing(df,dfMC,histograms,histogramsMC)
    plot_et(newdfMC,savedir='./hlt1_mcsmear_comparison.pdf')

    #compare_1d(df,dfMC)
    howmanyL0(df,dfMC)
    #mcvsdatatest(df,dfMC)

    


