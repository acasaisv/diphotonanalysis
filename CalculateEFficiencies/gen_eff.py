from bs4 import BeautifulSoup 
from subprocess import call
from concurrent.futures.thread import ThreadPoolExecutor
from helpers import sim10_masses_map, ufloat_to_str
from uncertainties import ufloat
from concurrent.futures import wait
def gen_xmls(masses):
    processes = []
    for i in masses:
        processes.append([
            './gen_eff/gen_eff.sh',f'{i}'])
        #subprocess.run(['./gen_eff.sh',f'{i}'])
    return processes

def run(cmd):
    print(cmd)
    call(cmd)
    return 0

def actual_run(runfun,cmds,limit):
    futures = []
    with ThreadPoolExecutor(max_workers=limit) as executor:
        for cmd in cmds:
            print(cmd)
            futures.append(executor.submit(runfun, cmd))
    print("All xml produced")
    return futures

def read_xml(i):

    # Reading the data inside the xml file to a variable under the name  data
    with open(f'./gen_eff/{i}/GeneratorLog.xml', 'r') as f:
        data = f.read() 

    # Passing the stored data inside the beautifulsoup parser 
    bs_data = BeautifulSoup(data, 'xml') 
    # Using find() to extract attributes of the first instance of the tag
    if i=='gg' or i=='pi0pi0':
        b_name = bs_data.find('efficiency', {'name':'generator level cut'}) 
    else:
        b_name = bs_data.find('efficiency', {'name':'full event cut'})
    value = float(b_name.find("value").text)
    error = float(b_name.find("error").text)
    eff = ufloat(value, error)
    return ufloat_to_str(eff),eff
def gen_column(masses):
    effs_ufloat = []
    effs = []
    for i in masses:
        # if i=='sim10':
        #     eff = ufloat(0.483,0.015)
        #     eff_ufloat = ufloat_to_str(eff)
        # elif i=='gg':
        #     eff = ufloat(0.17,0.025)
        #     eff_ufloat = ufloat_to_str(eff)
        # else:
        eff_ufloat,eff = read_xml(i)
        effs_ufloat.append(eff_ufloat)
        effs.append(eff)
    return effs_ufloat,effs
if __name__ == '__main__':
    effs = []
    gen =False
    masses=sim10_masses_map
    #masses = [40]
    if gen:
        procs = gen_xmls(masses)
        print(procs)
        futures = actual_run(run,procs,20)
        wait(futures)
    for i in masses:
        effs.append(read_xml(i))
    print(effs)
