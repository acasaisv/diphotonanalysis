import os
from ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import subprocess
from helpers import pTs,etas,pTsHlt2,etasHlt2,spds,spdsHlt2,get_ALPsdf,trigger_cuts,sim10_masses_map,get_error,get_error_w,ufloat_to_str, bins, get_ALPsdf_sim10b
from uncertainties import ufloat
from plot_helpers import *
#pTs,etas,spds = pTsHlt2,etasHlt2,spdsHlt2
import pickle

def load_dfs():
    root = '/scratch47/adrian.casais/ntuples/turcal'
    #dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMCStripping.h5',key='df')
    #df = pd.read_hdf(root+'/etammgTurcalDataStripping.h5',key='df')
    dfMC = pd.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')
    df = pd.read_hdf(root+'/etammgTurcalData.h5',key='df')
    df.query('gamma_PT > 3000 and gamma_PP_IsPhoton>0.85',inplace=True)
    df.query('eta_M > 465 & eta_M < 630',inplace=True)
    df.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    #df.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    dfMC.query('gamma_PT > 3000 and gamma_PP_IsPhoton>0.85',inplace=True)
    dfMC.query('gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS',inplace=True)
    dfMC.query('eta_M > 465 & eta_M < 630',inplace=True)
    #dfMC.query('gamma_L0Calo_ECAL_TriggerET>2500',inplace=True)
    
    # for d in df,dfMC:
    #     d['deltaET'] = d['gamma_PT'] - d['gamma_L0Calo_ECAL_TriggerET']
    
    df.reset_index(inplace=True)
    dfMC.reset_index(inplace=True)
    
    return df,dfMC


def print_to_text(df,file):
    string=""
    df['minShowerShape'] = np.minimum(df['gamma_ShowerShape'],df['gamma0_ShowerShape'])
    df['maxShowerShape'] = np.maximum(df['gamma_ShowerShape'],df['gamma0_ShowerShape'])
    df['minIsNotH'] = np.minimum(df['gamma_PP_IsNotH'],df['gamma0_PP_IsNotH'])
    df['maxIsNotH'] = np.maximum(df['gamma_PP_IsNotH'],df['gamma0_PP_IsNotH'])
    
    for _,row in df.iterrows():
        string+="{0} {1} {2} {3} {4}\n".format(row["B_s0_PT"],
                                             row["minIsNotH"],
                                             row["maxIsNotH"],
                                             row["minShowerShape"],
                                             row["maxShowerShape"])                                             
    with open(file,'w') as handle:
        handle.write(string)
        
    
def precuts_hlt2(df):
    trimmed_df = df.query('B_s0_M > 3500 and B_s0_M <22000 and (gamma_PT + gamma0_PT) > 6000 and B_s0_PT > 2000')
    eff = len(trimmed_df)/len(df)
    return trimmed_df,ufloat(eff,get_error(len(trimmed_df),len(df)))

def open_outputs(file):
    df = pd.read_csv(file,sep=' ',header=None)
    return df
def weights(root,rootwrite):
    o=subprocess.run([root+'lwtnn-test-arbitrary-net',root+'nn.json',root+'labels.txt',root+'values.txt'],capture_output=True,text=True)
    #print(o.stdout)
    with open(rootwrite+'output.txt','w') as handle:
        handle.write(str(o.stdout))


def recalc_pt(df,zTrigger=12300):
    df.eval('zTrigger = {}'.format(zTrigger),inplace=True)
    
    df.eval('tx  = gamma_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty  = gamma_CaloHypo_Y/zTrigger',inplace=True)
    df.eval('tx0  = gamma0_CaloHypo_X/zTrigger',inplace=True)
    df.eval('ty0  = gamma0_CaloHypo_Y/zTrigger',inplace=True)
    
    df.eval('tx  = gamma_PX/gamma_PZ',inplace=True)
    df.eval('ty  = gamma_PY/gamma_PZ',inplace=True)
    df.eval('tx0  = gamma0_PX/gamma0_PZ',inplace=True)
    df.eval('ty0  = gamma0_PY/gamma0_PZ',inplace=True)
    df.eval('gamma_PX = gamma_PT * tx*sqrt(1/(tx**2+ty**2))',inplace=True)
    df.eval('gamma_PY = gamma_PT * ty*sqrt(1/(tx**2+ty**2))',inplace=True)
    df.eval('gamma_PZ = gamma_PT * 1/sqrt(tx**2+ty**2)',inplace=True)
    
    df.eval('gamma0_PX = gamma0_PT * tx0*sqrt(1/(tx0**2+ty0**2))',inplace=True)
    df.eval('gamma0_PY = gamma0_PT * ty0*sqrt(1/(tx0**2+ty0**2))',inplace=True)
    df.eval('gamma0_PZ = gamma0_PT * 1/sqrt(tx0**2+ty0**2)',inplace=True)

    df.eval('gamma_PE = sqrt(gamma_PX**2 + gamma_PY**2 + gamma_PZ**2)',inplace=True)
    df.eval('gamma0_PE = sqrt(gamma0_PX**2 + gamma0_PY**2 + gamma0_PZ**2)',inplace=True)
    df.eval('costheta = (gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_PE*gamma0_PE)',inplace=True)
    df.eval('B_s0_PT = sqrt((gamma0_PX+gamma_PX)**2 + (gamma_PY + gamma0_PY)**2)',inplace=True)
    df.eval('B_s0_M = sqrt(2*gamma_PE*gamma0_PE*(1-costheta))',inplace=True)
    return df

def rescale_vars(df,pTs,etas,spds,rescales,list_vars = ['_ShowerShape'] ):
    for ipt in range(len(pTs)-1):
        pt = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                df_ = df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} and nSPDHits >= {4} and nSPDHits< {5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} and nSPDHits >= {4} and nSPDHits< {5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                
                for g in ('gamma','gamma_0'):
                    for var in list_vars:
                        if g=='gamma':
                            #df.loc[df_.index,'gamma'+var] = df_['gamma'+var]*rescales['gamma'+var][pt][eta][spd]
                            df.loc[df_.index,'gamma'+var] = df_['gamma'+var]*1.2
                        else:
                            #df.loc[df0_.index,'gamma0'+var] = df0_['gamma0'+var]*rescales['gamma'+var][pt][eta][spd]
                            df.loc[df_.index,'gamma0'+var] = df_['gamma0'+var]*1.2
            
    #recalc_pt(df)
    return df

def sample_from_2dhist(x,y,x_bins,y_bins,n_values=1000,weights=weights):
    hist, x_bins, y_bins = np.histogram2d(x, y, bins=(x_bins, y_bins),weights=weights)
    x_bin_midpoints = (x_bins[:-1] + x_bins[1:])/2
    y_bin_midpoints = (y_bins[:-1] + y_bins[1:])/2

    x_width = abs(x_bins[:-1] - x_bins[1:])/2
    y_width = abs(y_bins[:-1] - y_bins[1:])/2
    #print(x_width)

    
    cdf = np.cumsum(hist.flatten())
    cdf = cdf / cdf[-1]

    values = np.random.rand(n_values)
    value_bins = np.searchsorted(cdf, values)
    x_idx, y_idx = np.unravel_index(value_bins,
                                    (len(x_bin_midpoints),
                                    len(y_bin_midpoints)))
    random_from_cdf = np.column_stack((x_bin_midpoints[x_idx],
                                    y_bin_midpoints[y_idx]))

    new_x, new_y = random_from_cdf.T

    sorter = np.argsort(x_bin_midpoints)
    x_idx= sorter[np.searchsorted(x_bin_midpoints, new_x, sorter=sorter)]
   
    sorter = np.argsort(y_bin_midpoints)
    y_idx = sorter[np.searchsorted(y_bin_midpoints, new_y, sorter=sorter)]
    x_w,y_w = x_width[x_idx],y_width[y_idx]
    
    new_x = 2*x_w*np.random.random_sample(len(new_x))+new_x-x_w
    new_y = 2*y_w*np.random.random_sample(len(new_y))+new_y-y_w
    

    return new_x,new_y

def sample_is_not_h(dfetammg,df,pTs,etas,spds):
    print("enter sampel is noth")
    for ipt in range(len(pTs)-1):
        pt = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])

                dfetammg_ = dfetammg.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5} and gamma_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma_P>6000.'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))


                if len(dfetammg_)==0:
                    print("THIS IS NOT GOOOD")
                    continue
                # weights = dfetammg_['sweights']
                # offset = min(weights)
                # weights = np.array([z - offset +1 for z in weights])

                df_ = df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >={2} and gamma_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits>= {4} & nSPDHits<{5}'.format(pt[0],pt[1],eta[0],eta[1],spd[0],spd[1]))

                # index = np.random.choice(len(dfetammg_),size = len(df_),p=weights/np.sum(weights))
                # index0 = np.random.choice(len(dfetammg_),size = len(df0_),p=weights/np.sum(weights))
                
                # df.loc[df_.index,'gamma_PP_IsNotH'] = dfetammg_['gamma_PP_IsNotH'].to_numpy()[index]
                # df.loc[df0_.index,'gamma0_PP_IsNotH'] = dfetammg_['gamma_PP_IsNotH'].to_numpy()[index0]
                # df.loc[df_.index,'gamma_PP_ShowerShape'] = dfetammg_['gamma_ShowerShape'].to_numpy()[index]
                # df.loc[df0_.index,'gamma0_PP_ShowerShape'] = dfetammg_['gamma_ShowerShape'].to_numpy()[index0]
                isnoth,showershape = sample_from_2dhist(dfetammg_['gamma_PP_IsNotH'],dfetammg_['gamma_ShowerShape'],
                [0.3,0.5,0.7,0.8,0.85,0.9,1.0],[0,1e3,1.5e3,2e3,2.5e3,3e3,5e3,7e3,9e3,np.inf],n_values=len(df_),weights=dfetammg_['sweights'])
                isnoth0,showershape0 = sample_from_2dhist(dfetammg_['gamma_PP_IsNotH'],dfetammg_['gamma_ShowerShape'],
                [0.3,0.5,0.7,0.8,0.85,0.9,1.0],[0,1e3,1.5e3,2e3,2.5e3,3e3,5e3,7e3,9e3,np.inf],n_values=len(df0_),weights=dfetammg_['sweights'])


                df.loc[df_.index,'gamma_PP_IsNotH'] = isnoth
                df.loc[df0_.index,'gamma0_PP_IsNotH'] = isnoth0
                df.loc[df_.index,'gamma_ShowerShape'] = showershape
                df.loc[df0_.index,'gamma0_ShowerShape'] = showershape0


            
    #recalc_pt(df)
    return df

def get_reweight_to_alp(dfeta,dfalp,vars=['gamma_PT',
                                          'nSPDHits',
                                          'gamma_ETA',
                                          'gamma_PP_CaloNeutralID',
                                          'gamma_P']):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter

    reweighter = GBReweighter(max_depth=2, n_estimators=100,gb_args={'subsample': 0.5,'random_state':2456})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfeta[vars], target=dfalp[vars], original_weight=dfeta['sweights']
    #target_weight=df['sweights']
    )
    return reweighter
    
def sample_2d(dfetammg,dfalp):
    #bins_isnoth = [0.3,0.4,0.7,0.8,0.85,0.9,0.95,0.975,1.0]
    bins_isnoth = np.linspace(0.9,1,num=5)
    bins_isnoth = np.concatenate(([0.3,0.7,0.85],bins_isnoth))

    #bins_showershape = [250,500,1000,1.5e3,1.75e3,2e3,2.3e3,2.7e3,3e3,3.5e3,4e3,4.5e3,5e3,5.5e3,6e3,6.57e3,8e3,9e3,np.inf]
    bins_showershape = np.linspace(250,11e3,num=40)
    isnoth,showershape = sample_from_2dhist(dfetammg['gamma_PP_IsNotH'],dfetammg['gamma_ShowerShape'],
    bins_isnoth,bins_showershape,n_values=len(dfalp),weights=dfetammg['alp_weights'])
    isnoth0,showershape0 = sample_from_2dhist(dfetammg['gamma_PP_IsNotH'],dfetammg['gamma_ShowerShape'],
    bins_isnoth,bins_showershape,n_values=len(dfalp),weights=dfetammg['alp_weights'])


    dfalp.loc[dfalp.index,'gamma_PP_IsNotH'] = isnoth
    dfalp.loc[dfalp.index,'gamma0_PP_IsNotH'] = isnoth0
    dfalp.loc[dfalp.index,'gamma_ShowerShape'] = showershape
    dfalp.loc[dfalp.index,'gamma0_ShowerShape'] = showershape0


            
    #recalc_pt(df)
    return dfalp


def compare_is_not_h(df,i):
    fig,ax = plt.subplots(1,2,figsize=(10,5))
    alpha = 0.7
    ax[0].hist(df['IsNotH0Sim'],label='Simulation',bins=50,density=True,alpha=alpha)
    ax[0].hist(df['IsNotH0SimSample'],histtype='step',label='Sampled from Simulation',bins=50,density=True,color='green',alpha=alpha)
    ax[0].hist(df['gamma0_PP_IsNotH'],histtype='step',label='Sampled from Data',bins=50,density=True,color='red',alpha=alpha)
    ax[1].hist(df['ShowerShapeSim'],label='Simulation',bins=50,density=True,alpha=alpha)
    ax[1].hist(df['ShowerShapeSimSample'],histtype='step',label='Sampled from Simulation',bins=50,density=True,color='green',alpha=alpha)
    ax[1].hist(df['gamma_ShowerShape'],histtype='step',label='Sampled from Data',bins=50,density=True,color='red',alpha=alpha)
    ax[0].set_xlabel(r'IsNotH',horizontalalignment='left',x=0.7)
    ax[0].set_ylabel(r'A.U.')
    ax[1].set_xlabel(r'ShowerShape',horizontalalignment='left',x=0.7)
    ax[1].set_ylabel(r'A.U.')
    ax[0].legend()
    fig.tight_layout()
    fig.savefig(f'./hlt2/hlt2_isnoth_{i}.pdf')
    #plt.show()

#def compare_correlation(df,i):


def compare_is_not_h2(etammg,etammgMC,df):
    plt.clf()
    plt.hist(etammg['gamma_PP_IsNotH'],bins=38,histtype='step',density=True,color='red',weights=etammg['sweights'],label='etammg')
    plt.hist(etammgMC['gamma_PP_IsNotH'],bins=38,histtype='step',density=True,color='green',weights=etammgMC['sweights'],label='etammg mc')
    plt.hist(df['gamma_CL'],bins=38,histtype='step',density=True,color='blue',weights=df['weights'],label='alp')
    plt.legend(loc='upper left')
    plt.savefig('etammg vs alp.pdf')

def isnoth_showershape_2d(df,label='original'):
    fig,ax = plt.subplots(1,1)
    ax.hist2d(df['gamma_PP_IsNotH'],df['gamma_ShowerShape'],bins=50,range=[[0.85,1.0],[100,5000]])
    ax.set_xlabel(r'$\gamma$ IsNotH')
    ax.set_ylabel(r'$\gamma$ ShowerShape')
    plt.savefig(f'hlt2/IsNotHvShowerShape-{label}.pdf')
             
def hlt2_column(masses,printtable=True):    
    root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    effs_ufloat = []
    mybin = bins['low']
    spds,etas,pts = mybin['spds'],mybin['etas'],mybin['pts']
    myetammg,myetammgMC= load_dfs()
    dfeff = pd.DataFrame({ 'Mass':[],
                           r'\efftos':[],
                           r'$\epsilon(\textrm{MC rescale})$':[],
                           r'$\epsilon(\textrm{Data rescale})$':[]})
    vars = [
            #'gamma_PT',
            'gamma_Matching',
            #'nSPDHits',
            #'gamma_ETA',
            #'gamma_PP_CaloNeutralID',
            #'gamma_P',
            
             ]
    for i in masses:
        cut = f"{trigger_cuts['L0']} & {trigger_cuts['HLT1']} & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000"
        rootalp = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        if i=='gg':
            cut = f'{trigger_cuts["L0"]}  & {trigger_cuts["HLT1"]} & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            mydf = get_ALPsdf_sim10b(rootalp+'b2gg-stripping.root',bs=True,background=False,extracut=cut,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_CL','gamma0_CL','gamma0_PP_IsNotH','gamma_PP_IsNotH','gamma_PP_CaloNeutralID','gamma0_PP_CaloNeutralID'])
        else:
            mydf = get_ALPsdf_sim10b(i,False,background=False,extracut=cut,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_CL','gamma0_CL','gamma0_PP_IsNotH','gamma_PP_IsNotH','gamma_PP_CaloNeutralID','gamma0_PP_CaloNeutralID'])
        
        reweighter = get_reweight_to_alp(myetammg,mydf,vars = vars)
        myetammg['alp_weights']= reweighter.predict_weights(myetammg[vars],
                                                  original_weight=myetammg['sweights']
                                                  )
        reweighterMC = get_reweight_to_alp(myetammgMC,mydf,vars = vars)
        myetammgMC['alp_weights']= reweighterMC.predict_weights(myetammgMC[vars],
                                                  original_weight=myetammgMC['sweights'])
        #mydf.query('gamma_Matching >1000 and gamma0_Matching > 1000',iplace=True)
        
        tos_eff = ufloat(len(mydf.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'))/len(mydf),get_error(len(mydf.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS')),len(mydf)))
        lwtnn_eff_MC = get_lwtnn_eff(mydf,myetammgMC,i,pts,etas,spds)
        lwtnn_eff_data = get_lwtnn_eff(mydf,myetammg,i,pts,etas,spds)
        #d_eff = abs(lwtnn_eff.n - tos_eff.n)
        #d_eff = np.sqrt(abs(lwtnn_eff_MC.n - tos_eff.n)**2 + lwtnn_eff_MC.s**2 + tos_eff.s**2)
        d_eff = np.sqrt(abs(lwtnn_eff_MC.n - tos_eff.n)**2 )
        #d_eff = 0.06*tos_eff.n
        lwtnn_eff_data += ufloat(0,d_eff)
        effs_ufloat.append(lwtnn_eff_data)
        effarray = np.array([masses[i],ufloat_to_str(tos_eff),ufloat_to_str(lwtnn_eff_MC),ufloat_to_str(lwtnn_eff_data)])
        dfeff=dfeff.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeff)),ignore_index=True)
    if printtable:
        print(dfeff.to_latex(index=False,escape=False))
    return [ufloat_to_str(u) for u in effs_ufloat],effs_ufloat

def get_lwtnn_eff(df,dfetammg,i,pTs,etas,spds):
    # with open("rescales.p","rb") as handle:
    #     rescales = pickle.load(handle)
    #df_ = df
    #df_ = rescale_vars(df,pTs,etas,spds,rescales)
    #df_ = sample_is_not_h(dfetammg,df,pTs,etas,spds)

    df_ = sample_2d(dfetammg,df)
    df_,pre_eff = precuts_hlt2(df_)
    root = '/scratch47/adrian.casais/lwtnn/build/bin/'
    rootwrite = 'lwtnn/build/bin/'
    rootwrite=root
    if not os.path.exists(rootwrite):
        os.makedirs(rootwrite)
    print_to_text(df,rootwrite+'values.txt')
    weights(root, rootwrite)
    myarr = open_outputs(rootwrite+'output.txt')
    myarr = np.array(myarr[1].array)
    myarr_cut = myarr[myarr>0.8]
    lwtnn_eff = pre_eff *ufloat(len(myarr_cut)/len(myarr),get_error(len(myarr_cut),len(myarr)))
    return lwtnn_eff



if __name__ == '__main__':
    masses = sim10_masses_map
    del masses['pi0pi0']
    masses = {40:5,41:6}
    hlt2_column(masses,True)


    # bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'
    # myetammg,myetammgMC= load_dfs()
    
    # vars = [
    #         #'gamma_PT',
    #         'gamma_Matching',
    #         #'nSPDHits',
    #         #'gamma_ETA',
    #         #'gamma_PP_CaloNeutralID',
    #         #'gamma_P',
            
    #          ]
    
    
    
    # #reweight_mc_df(etammg,etammgMC)
    # #myetammg.query('gamma_Matching >2',inplace=True)
    # #myetammgMC.query('gamma_Matching >2',inplace=True)
    # dfeff = pd.DataFrame({ 'Mass':[],
    #                        r'\efftos':[],
    #                        r'$\epsilon(\textrm{MC rescale})$':[],
    #                        r'$\epsilon(\textrm{Data rescale})$':[]})
    # vlow,low,medium,high = bins['vlow'],bins['low'],bins['medium'],bins['high']
    # mybin = bins['low']
    # pts,etas,spds = mybin['pts'],mybin['etas'] ,mybin['spds']
    # print('TOS Eff | Rescale MC | Rescale data | MC/Data rescaling | MC/MCrescaling')   
    
    # del masses['pi0pi0']
    # #for i in sim10_masses_map:
    # for i in [41]:
    #   if i=='sim10': continue
    #   print("Mass is {0}".format(masses[i]))
    #   #df = get_df(i,False)
    #   if i=='gg':
    #     cut = f'{trigger_cuts["L0"]} & {trigger_cuts["HLT1"]} & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
    #     myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
    #     mydf = get_ALPsdf_sim10b(myfile,extracut=cut,background=False,bs=True,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH','gamma0_PP_IsPhoton','gamma_PP_IsPhoton','gamma_PP_CaloNeutralID']) 
    #   else:
        

    #     cut = f'{trigger_cuts["L0"]} & {trigger_cuts["HLT1"]} & (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
    #     rootalp = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    #     mydf = get_ALPsdf(rootalp+f'491000{i}_1000-1099_Sim10a-priv.root',False,background=False,extracut=cut,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH','gamma0_PP_IsPhoton','gamma_PP_IsPhoton','gamma_PP_CaloNeutralID'])

      
    #   reweighter = get_reweight_to_alp(myetammg,mydf,vars = vars)
    #   myetammg['alp_weights']= reweighter.predict_weights(myetammg[vars],
    #                                               original_weight=myetammg['sweights']
    #                                               )
    #   reweightermc = get_reweight_to_alp(myetammgMC,mydf,vars = vars)
    #   myetammgMC['alp_weights']= reweightermc.predict_weights(myetammgMC[vars],
    #                                               original_weight=myetammgMC['sweights']
    #                                               )
    #   #compare_is_not_h2(myetammg,myetammgMC,df)

    #   #print("HLT2 TOS efficiency")
    #   tos_eff = ufloat(len(mydf.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'))/len(mydf),get_error(len(mydf.query('B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS')),len(mydf)))
    #   print("{:.3f} %".format(100.*tos_eff))

    #   mydf['IsNotHSim'] = mydf['gamma_PP_IsNotH']
    #   mydf['IsNotH0Sim'] = mydf['gamma0_PP_IsNotH']
    #   mydf['ShowerShapeSim'] = mydf['gamma_ShowerShape']
    #   mydf['ShowerShape0Sim'] = mydf['gamma0_ShowerShape']
    #   isnoth_showershape_2d(mydf,label=f'{i}-alps')
    #   lwtnn_eff_MC = get_lwtnn_eff(mydf,myetammgMC,i,pts,etas,spds)
    #   isnoth_showershape_2d(mydf,label=f'{i}-sim')
    #   mydf['IsNotHSimSample'] = mydf['gamma_PP_IsNotH']
    #   mydf['IsNotH0SimSample'] = mydf['gamma0_PP_IsNotH']
    #   mydf['ShowerShapeSimSample'] = mydf['gamma_ShowerShape']
    #   mydf['ShowerShape0SimSample'] = mydf['gamma0_ShowerShape']
    #   lwtnn_eff_data = get_lwtnn_eff(mydf,myetammg,i,pts,etas,spds)
    #   isnoth_showershape_2d(mydf,label=f'{i}-data')
    #   compare_is_not_h(mydf,i)
    #   effarray = np.array([masses[i],ufloat_to_str(tos_eff),ufloat_to_str(lwtnn_eff_MC),ufloat_to_str(lwtnn_eff_data)])
    #   dfeff=dfeff.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeff)),ignore_index=True)
      
    #   print(f'{100.*tos_eff.n:.2f} +- {100.*tos_eff.s:.2f}| {100.*lwtnn_eff_MC.n:.2f} +- {100.*lwtnn_eff_MC.s:.2f}|{100.*lwtnn_eff_data.n:.2f} +- {100.*lwtnn_eff_data.s:.2f}|{100.*(lwtnn_eff_data.n - lwtnn_eff_MC.n)/lwtnn_eff_MC.n:.2f} | {100.*(tos_eff.n - lwtnn_eff_MC.n)/tos_eff.n:.2f}')
    #   print(8*"==")


    # print(dfeff.to_latex(index=False,escape=False))
