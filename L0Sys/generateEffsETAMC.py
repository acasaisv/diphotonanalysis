import sys
import subprocess
import numpy as np
from math import sqrt
from helpers import pTsHlt2,etasHlt2,spdsHlt2, get_error_w
from ROOT import TEfficiency
import pandas as pd
import matplotlib.pyplot as plt


def reweight_mc_df(df,dfMC):
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = ['eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return

def plot_reweight(df,dfMC):
    fig,ax = plt.subplots(7,2,figsize=(9,16),dpi=80)
    n_plots = 4
    ax =ax.flatten()
    alpha = 0.7
    vars = ['nSPDHits','eta_ETA','eta_PT','gamma_PT','gamma_ETA','gamma_CL','gamma_PP_CaloNeutralHcal2Ecal']
    for i,var in zip(range(len(vars)),vars):
        
        ax[i].set_title(var)
        ax[i].hist(df[var],bins=50,weights=df['sweights'],density=True,alpha=0.7)
        ax[i].hist(dfMC[var],bins=50,weights=dfMC['sweights'],density=True,alpha=0.7)

    ax[len(vars)+1].set_title('eta vs nSPDHits')
    ax[len(vars)+1].hist2d(df['eta_ETA'],df['nSPDHits'],bins=30,density=True,weights=df['sweights'])
    ax[len(vars)+2].hist2d(dfMC['eta_ETA'],dfMC['nSPDHits'],bins=30,density=True,weights=dfMC['sweights'])
    ax[len(vars)+3].set_title('pt vs nSPDHits')
    ax[len(vars)+3].hist2d(df['eta_PT'],df['nSPDHits'],bins=30,density=True,weights=df['sweights'],range=([0,2e4],[0,1e3]))
    ax[len(vars)+4].hist2d(dfMC['eta_PT'],dfMC['nSPDHits'],bins=30,density=True,weights=dfMC['sweights'],range=([0,2e4],[0,1e3]))
    ax[len(vars)+5].set_title('eta vs pt')
    ax[len(vars)+5].hist2d(df['eta_PT'],df['eta_ETA'],bins=30,density=True,weights=df['sweights'],range=([0,2e4],[0,6]))
    ax[len(vars)+6].hist2d(dfMC['eta_PT'],dfMC['eta_ETA'],bins=30,density=True,weights=dfMC['sweights'],range=([0,2e4],[0,6]))
    
    
    plt.show()
    
def plot_pid(df,dfMC):
    fig,ax = plt.subplots(1,3,figsize=(9,16),dpi=80)
    n_plots = 4
    ax =ax.flatten()
    alpha = 0.7
    ax[0].set_title('gamma_CL')
    ax[0].hist(df['gamma_CL'],bins=50,weights=df['sweights'],density=True,alpha=0.7,label='Data')
    ax[0].hist(dfMC['gamma_CL'],bins=50,weights=dfMC['sweights'],density=True,alpha=0.7,label='MC')

    ax[1].set_title('HCAL2ECAL')
    ax[1].hist(df['gamma_PP_CaloNeutralHcal2Ecal'],bins=50,weights=df['sweights'],density=True,alpha=0.7,range=(0,0.3))
    ax[1].hist(dfMC['gamma_PP_CaloNeutralHcal2Ecal'],bins=50,
               weights=dfMC['sweights'],
               density=True,alpha=0.7,range=(0,0.3))

    ax[2].set_title('sWeights')
    ax[2].hist(df['eta_M'],bins=50,density=True,alpha=0.7,weights=df['sweights'])
    ax[2].hist(dfMC['eta_M'],bins=50,density=True,alpha=0.7,weights=dfMC['sweights'])
    ax[2].set_xlabel('Mass [MeV]')
    ax[0].legend()
    plt.show()

#plot_reweight(df,dfMC)
# spdhits='gamma_ETA > 3.5 & gamma_PT >2000 & gamma_PT < 6000'
# dfMC.query(spdhits, inplace=True)
# df.query(spdhits, inplace=True)

# trig = '(gamma_L0PhotonDecision_TOS | gamma_L0ElectronDecision_TOS)'
# dfMC.query(trig,inplace=True)
# df.query(trig,inplace=True)

#plot_pid(df,dfMC)
#THIS IS SHIT
#dfMC.query('gamma_CL>0.05',inplace=True)
#df.query('gamma_CL>0.05',inplace=True)


#####
def calc_efficiencies(df,dfMC):
    nums = {}
    dens = {}
    err_nums = {}
    err_dens = {}
    effs = {}
    err_effs = {}
    covs = {}
    MCeffs = {}
    err_MCeffs = {}
    MCeffs2 = {}
    err_MCeffs2 = {}
    
    efficiencies={}
    efficienciesMC={}
    
    error_efficiencies = {}
    error_MCefficiencies = {}
    
    for ipt in range(len(pTs)-1):
        pt = (pTs[ipt],pTs[ipt+1])
        efficiencies[pt]={}
        efficienciesMC[pt]={}

        error_efficiencies[pt] = {}
        error_MCefficiencies[pt] = {}
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            efficiencies[pt][eta]={}
            efficienciesMC[pt][eta]={}

            error_efficiencies[pt][eta] = {}
            error_MCefficiencies[pt][eta] = {}

            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])


                kin_cut = ' (gamma_PT >= {0} & gamma_PT < {1})'.format(pt[0],pt[1])
                kin_cut +=' & (gamma_ETA >= {0} & gamma_ETA < {1})'.format(eta[0],eta[1])
                kin_cut +=' & (nSPDHits >= {0} & nSPDHits < {1})'.format(spd[0],spd[1])
                L0cut = ' (gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS)'
                #L0cut = '(gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1)'
                TIS = ' mu2_L0MuonDecision_Dec'
                #TIS = 'gamma_P>0'

                den_weights = df.query( "({0}) & ({1})".format(kin_cut,TIS) )['sweights']
                num_weights = df.query( "({0}) & ({1}) & ({2})".format(kin_cut,TIS,L0cut) )['sweights']
                den = den_weights.sum()    
                num = num_weights.sum()
                den_weightsMC = dfMC.query( "({0}) & ({1})".format(kin_cut,TIS) )['sweights']
                num_weightsMC =dfMC.query( "({0}) & ({1}) & ({2})".format(kin_cut,TIS,L0cut) )['sweights']
                denMC = den_weightsMC.sum()
                numMC = num_weightsMC.sum()


                efficienciesMC[pt][eta][spd]=numMC/denMC
                error_MCefficiencies[pt][eta][spd] = get_error_w(num_weightsMC,den_weightsMC)



                efficiencies[pt][eta][spd]=num/den

                error_efficiencies[pt][eta][spd] = get_error_w(num_weights,den_weights)
    return efficiencies,error_efficiencies,efficienciesMC,error_MCefficiencies
                      

def make_plot(efficienciesMC,error_MCefficiencies,efficiencies,error_efficiencies):

    plt.rc('text',usetex=True)
    plt.rc('font',family='sans-serif')
    plots = {}
    for ispd in range(len(spds)-1):
        spd_key = (spds[ispd],spds[ispd+1])
        plots[spd_key] = {}
        for ieta in range(len(etas)-1):
            eta_key = (etas[ieta],etas[ieta+1])
            
            mceffs = [efficienciesMC[(pTs[i],pTs[i+1])][eta_key][spd_key] for i in range(len(pTs)-1)]
            mcerrs = [error_MCefficiencies[(pTs[i],pTs[i+1])][eta_key][spd_key] for i in range(len(pTs)-1)]
            
            effs = [efficiencies[(pTs[i],pTs[i+1])][eta_key][spd_key] for i in range(len(pTs)-1)]
            errs = [error_efficiencies[(pTs[i],pTs[i+1])][eta_key][spd_key] for i in range(len(pTs)-1)]

            mypts = np.where(pTs<np.inf,pTs,max(pTs[pTs<np.inf])+3000)
            x = (mypts[:-1] +mypts [1:])/2
            
            xerr = abs(mypts[:-1] - mypts [1:])/2
            key = (eta_key,spd_key)
            plots[spd_key][eta_key] = (      x,#pt
                                             
                                             mceffs,mcerrs,#MCEff with error
                                             effs,errs,# Eff with error,
                                             xerr # xerr
                                             )
    dpi = 80
    fig,ax = plt.subplots(len(plots.keys()),len( plots[spd_key].keys() ),dpi =dpi,figsize=(1280./dpi,960./dpi) )
    ax = np.array(ax)
    for row,spd in zip(ax,plots.keys()):
        effs = plots[spd]
        #if len(plots.keys())==1:
        #    row = np.array([row])
        print(row)
        for a,eta in zip(row,plots[spd].keys()):
            
            eff = effs[eta]
            a.set_title('ETA in [{0},{1}]. nSPDHits in [{2},{3}]'.format(eta[0],eta[1],spd[0],spd[1]))
            a.errorbar(eff[0],eff[1],xerr=eff[5], yerr=eff[2],fmt='s',color='xkcd:blue',label=r'$\eta \to  \mu^+ \mu^- \gamma$ sim09 MC')
            a.set_ylabel('Efficiency')
            a.set_xlabel(r'p$_T$ [MeV/c]')

            a.errorbar(eff[0],eff[3],xerr=eff[5], yerr=eff[4],fmt='s',color='xkcd:red',label=r'$\eta \to  \mu^+ \mu^- \gamma$ TURCAL Data')

            a.set_ylim(0,1)
            #a.set_xticks(eff[0])
            #a.grid(True)
    if len(plots.keys())==1:
        ax[0].legend()
    else:
        
        ax[0][0].legend()
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.1)
    plt.savefig('/home3/adrian.casais/figstoscp/l0sys.pdf')
    plt.show()
    
        
        
    
if __name__=='__main__':
    df =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
    dfMC =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
    dfMC.query('nSPDHits<450',inplace=True)
    df.query('nSPDHits<450',inplace=True)
    reweight_mc_df(df,dfMC)
    effs,err,effsMC,errMC=calc_efficiencies(df,dfMC)
    import pickle
    with open('L0Efficiencies.p','wb') as file:
        pickle.dump([effs,err,effsMC,errMC],file)

    make_plot(effsMC,errMC,effs,err)
        
