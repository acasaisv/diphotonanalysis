from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTs,etas,spds,get_error,get_error_w,pack_eff,get_ALPsdf,sim10_masses_map
import pickle
from uncertainties import ufloat

def get_df(i,bs=False,background=False,extravars = []):
    if type(i)!=str:
        root = '/scratch47/adrian.casais/ntuples/signal'
        f_s = uproot.open(root + '/491000{}_MagDown.root'.format(i))
    elif i=='bs':
        f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root')
    else:
        f_s = uproot.open('{}'.format(i))
    t_s = f_s['DTTBs2GammaGamma/DecayTree']
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            'gamma_CaloHypo_X',
            'gamma_CaloHypo_Y',
            'gamma0_CaloHypo_X',
            'gamma0_CaloHypo_Y',
            'gamma_CaloHypo_HypoE',
            'gamma_CaloHypo_HypoEt',
            'gamma0_CaloHypo_HypoE',
            'gamma0_CaloHypo_HypoEt',
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
            'nSPDHits',
            'gamma_CL',
            'gamma0_CL'
            #'HeaviestQuark'
         
       
            ]
    vars = set(vars + extravars)
    truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
    if bs:
        truth = "(abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22) & abs(B_s0_TRUEID)==531"
    df = t_s.pandas.df(branches=vars)
    if not background:
        df.query(truth,inplace=True)
    df.query('gamma_PT > 2500 | gamma0_PT > 2500',inplace=True)
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    df['gamma0_ETA'] = 0.5*np.log( (df['gamma0_P']+df['gamma0_PZ'])/(df['gamma0_P']-df['gamma0_PZ']) )
    df['weights'] = np.ones(len(df))
    df.eval('dist2 = (gamma_CaloHypo_X - gamma0_CaloHypo_X)**2 + (gamma_CaloHypo_Y - gamma0_CaloHypo_Y)**2',inplace=True)
    #df.query('nSPDHits<450',inplace=True)
    df.reset_index(inplace=True)
    return df

def convolute_eff(nspdseff,nspdserr,m,df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs):
    eff_gamma=0
    erreff_gamma2=0
    effMC_gamma=0
    erreffMC_gamma2=0
    eff_gamma0=0
    erreff_gamma02=0
    effMC_gamma0=0
    erreffMC_gamma02=0
    
    sum_fractions = 0

    eff_bs =0
    erreff_bs2=0
    
    den = df.query('gamma_PT >= 2500')['weights']
    den0 = df.query('gamma0_PT >= 2500')['weights']
    denbs = df.query('gamma0_PT >= 2500 and gamma_PT >=2500')['weights']
    for ipt in range(len(pTs)-1):
        pT = (pTs[ipt],pTs[ipt+1])
        for ieta in range(len(etas)-1):
            eta = (etas[ieta],etas[ieta+1])
            for ispd in range(len(spds)-1):
                spd = (spds[ispd],spds[ispd+1])
                
                df_ =  df.query('gamma_PT >= {0} and gamma_PT < {1} and gamma_ETA >= {2} and gamma_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                df0_ =  df.query('gamma0_PT >= {0} and gamma0_PT < {1} and gamma0_ETA >= {2} and gamma0_ETA < {3} & nSPDHits >= {4} & nSPDHits< {5}'.format(pT[0],pT[1],eta[0],eta[1],spd[0],spd[1]))
                
                num = df_['weights']
                fraction = num.sum()/den.sum()
                err_fraction = get_error_w(num,den)

                eff_gamma += fraction*efficiencies[pT][eta][spd]
                erreff_gamma2 += err_fraction**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction**2

                effMC_gamma += fraction*efficienciesMC[pT][eta][spd]
                erreffMC_gamma2 += err_fraction**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction**2
                
                num0 = df0_['weights']
                fraction0 = num0.sum()/den0.sum()
                err_fraction0 = get_error_w(num0,den0)

                eff_gamma0 += fraction0*efficiencies[pT][eta][spd]
                erreff_gamma02 += err_fraction0**2*efficiencies[pT][eta][spd]**2 + err_effs[pT][eta][spd]**2*fraction0**2

                effMC_gamma0 += fraction0*efficienciesMC[pT][eta][spd]
                erreffMC_gamma02 += err_fraction0**2*efficienciesMC[pT][eta][spd]**2 + err_MCeffs[pT][eta][spd]**2*fraction0**2


                
    cutl0 = '(gamma_L0PhotonDecision_TOS | gamma_L0ElectronDecision_TOS)'
    cutl00 = '(gamma0_L0PhotonDecision_TOS | gamma0_L0ElectronDecision_TOS)'
    cutbs = '(B_s0_L0PhotonDecision_TOS | B_s0_L0ElectronDecision_TOS)'
    
    cutkin =  '(gamma_PT >= 2500)'
    cutkin0 = '(gamma0_PT >= 2500)'
    cutkinbs = '(gamma0_PT >= 2500 or gamma_PT >=2500)'

    num_w = df.query("{0} & {1} ".format(cutl0,cutkin))['weights']
    den_w = df.query(cutkin)['weights']
    l0tos =  num_w.sum()/den_w.sum()
    errl0tos = get_error_w(num_w,den_w)

    num0_w = df.query("{0} & {1} ".format(cutl00,cutkin0))['weights']
    den0_w = df.query(cutkin0)['weights']
    l0tos0 =  num0_w.sum()/den0_w.sum()
    errl0tos0 = get_error_w(num0_w,den0_w)
    
    numbs_w = df.query("{0} & {1} ".format(cutbs,cutkinbs))['weights']
    denbs_w = df.query(cutkinbs)['weights']
    l0tosbs =  num0_w.sum()/den0_w.sum()
    errl0tosbs = get_error_w(num0_w,den0_w)


    bsmcefferror = np.sqrt(erreffMC_gamma02 +erreffMC_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)
    bsefferror = np.sqrt(erreff_gamma02 +erreff_gamma2 +errl0tos**2 + errl0tos**2 + errl0tosbs**2)

    effbsmc =  (effMC_gamma0+effMC_gamma)*l0tosbs/(l0tos+l0tos0)
    effbs = (eff_gamma0+eff_gamma)*l0tosbs/(l0tos+l0tos0)

    espds = ufloat(nspdseff,nspdserr)
    emcg0 = ufloat(effMC_gamma0,np.sqrt(erreffMC_gamma02))
    emcg1 = ufloat(effMC_gamma,np.sqrt(erreffMC_gamma2))
    edatag0 = ufloat(eff_gamma0,np.sqrt(erreff_gamma02))
    edatag1 = ufloat(eff_gamma,np.sqrt(erreff_gamma2))
    el0tos0 = ufloat(l0tos0,errl0tos0)
    el0tos1 = ufloat(l0tos,errl0tos)
    el0tosbs = ufloat(l0tosbs,errl0tosbs)

    bsmc = (emcg0+emcg1)*el0tosbs/(el0tos0+el0tos1)
    bsdata = (edatag0+edatag1)*el0tosbs/(el0tos0+el0tos1)

    fulll0mc = bsmc*espds
    fulll0data = bsdata*espds
    
    effarray = np.array((m,pack_eff(100*nspdseff,100*nspdserr),pack_eff(100.*l0tos,100.*errl0tos),pack_eff(100.*l0tos0,100.*errl0tos0),pack_eff(100.*eff_gamma,100.*np.sqrt(erreff_gamma2)),pack_eff(100.*eff_gamma0,100.*np.sqrt(erreff_gamma02)),pack_eff(100.*effMC_gamma,100.*np.sqrt(erreffMC_gamma2)),pack_eff(100.*effMC_gamma0,100.*np.sqrt(erreffMC_gamma02)),pack_eff(100.*l0tosbs,100.*errl0tosbs),pack_eff(100.*bsdata.nominal_value,100.*bsdata.std_dev),pack_eff(100.*bsmc.nominal_value,100.*bsmc.std_dev),pack_eff(100.*fulll0data.nominal_value,100.*fulll0data.std_dev),pack_eff(100.*fulll0mc.nominal_value,100.*fulll0mc.std_dev)))
    
    return fulll0data,fuldfeffs.append(pd.DataFrame(effarray.reshape(1,-1),columns=list(dfeffs)),ignore_index=True)
    
def reweight_alps_df(dfalps,dfeta):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    vars = [#'eta_PT','eta_ETA',
            'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfalps['nSPDHits'], target=1.7*dfalps['nSPDHits'], target_weight=dfalps['weights'])
    dfalps['weights']= reweighter.predict_weights(dfalps['nSPDHits'])
    return

    
    #print('Fraction sum = {}'.format(sum_fractions))
if __name__ == '__main__':
    import pickle
    with open('L0Efficiencies.p','rb') as file:
        efficiencies,err_effs,efficienciesMC,err_MCeffs = pickle.load(file)
    # masses = {40:'5 GeV',
    #           42:'7 GeV',
    #           44:'9 GeV',
    #           46:'11 GeV',
    #           47:'13 GeV',
    #           50:'15 GeV',
    #           48:'4 GeV',
    #           'bs':'Bs0 (5 GeV)'}
    masses = sim10_masses_map
    plot = []
    plot0 = []
    bsroot = '/scratch47/adrian.casais/ntuples/signal/b2gg-sim09.root'

    dic = { 'Mass':[],
            'nSPDHits':[] ,
            '$\\epsilon(\\gamma)$':[],
            '$\\epsilon(\\gamma^0)$':[],
            '$E_{\\textrm{Data}} (\\gamma)$':[],
            '$E_{\\textrm{Data}} (\\gamma^0)$':[],
            '$E_{\\textrm{MC}} (\\gamma)$':[],
            '$E_{\\textrm{MC}} (\\gamma^0)$':[],
            '$\epsilon (\\aa)$':[],
            '$E_{\\textrm{Data}} (\\aa)$':[],
            '$E_{\\textrm{MC}} (\\aa)$}':[],
            '$E_{\\textrm{Data}} (\\textrm{L0})$':[],
            '$E_{\\textrm{MC}} (\\textrm{L0})$':[]}
    dfeffs = pd.DataFrame(dic)
    for i in sim10_masses_map:
    #for i in ['/scratch46/adrian.casais/49100040_1000-1009_Sim10a-priv.root']:
    #for i in [50]:
              #,'bs']:
    #for i in [40]:
        print(10*"**")
        print(masses[i])
        bs = False
        if i=='bs':
            bs=True
        

        #df = get_df(i,bs,background=False)
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        df = get_ALPsdf(root+f'491000{i}_1000-1099_Sim10a-priv.root',bs,background=False,sample=int(10e4))
        plt.hist(np.sqrt(df['dist2']),bins=50,histtype='step',density=True,label=str(i))
        
        df['nSPDHits'] *= 1.66
        
        nSPDhits_den = df['weights']
        nSPDhits_num = df.query('nSPDHits < 450')['weights']
        eff = nSPDhits_num.sum()/nSPDhits_den.sum()
        err_eff = get_error_w(nSPDhits_num,nSPDhits_den)
        print('nSPDs eff= {0:.2f} +- {1:.2f}'.format(100.*eff,100.*err_eff))
        dfetamc =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
        dfeta =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
        df['nSPDHits'] /= 1.66
        dfeta.query('nSPDHits <450',inplace=True)
        reweight_alps_df(df,dfeta)
        
        df.query('nSPDHits<450',inplace=True)
        dfeffs=convolute_eff(eff,err_eff,masses[i],df,pTs,etas,spds,efficiencies,err_effs,efficienciesMC,err_MCeffs,dfeffs)
    print(dfeffs.to_latex(index=False,escape=False))
        


# def small_test(df):
#     et0 = 'gamma0_L0Calo_ECAL_TriggerET>gamma_L0Calo_ECAL_TriggerET'
#     et = 'gamma0_L0Calo_ECAL_TriggerET<gamma_L0Calo_ECAL_TriggerET'

#     gamma0l0 = '(gamma0_L0PhotonDecision_TOS or  gamma0_L0ElectronDecision_TOS)'
#     gammal0 = '(gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS)'
#     bsl0 ='(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'
#     effbs = len(df.query(bsl0))/len(df)
#     eff = 0
#     for d in (gamma0l0,gammal0):
#             eff += len(df.query('{0}'.format(d)))/len(df)
#     eff -= len(df.query(' {0} and {1}'.format(gamma0l0,gammal0)))/len(df)
#     return eff-effbs
# print(small_test(df))                   


# a = df.query('(gamma0_L0Calo_ECAL_TriggerET>gamma_L0Calo_ECAL_TriggerET and (gamma0_L0PhotonDecision_TOS or  gamma0_L0ElectronDecision_TOS))').shape[0]
# b = df.query('(gamma0_L0Calo_ECAL_TriggerET<=gamma_L0Calo_ECAL_TriggerET and (gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS))').shape[0]
# c = df.query('((B_s0_L0PhotonDecision_TOS or  B_s0_L0ElectronDecision_TOS))').shape[0]
# print((a+b)/c)

# aa ='and'
# bin = '(gamma_PT > 2500 and gamma_PT < 10000)'
# bin0 = '(gamma0_PT > 2500 and gamma0_PT < 10000)'
# binor = '({0} or {1})'.format(bin,bin0)
# binand = '({0} and {1})'.format(bin,bin0)
# order0 ='(gamma0_L0Calo_ECAL_TriggerET>=gamma_L0Calo_ECAL_TriggerET)'
# order = '(gamma0_L0Calo_ECAL_TriggerET<=gamma_L0Calo_ECAL_TriggerET)'

# l0g0 = '(gamma0_L0PhotonDecision_TOS or  gamma0_L0ElectronDecision_TOS)'
# l0g  = '(gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS)'
# l0 = '(gamma0_L0PhotonDecision_TOS or  gamma0_L0ElectronDecision_TOS) and (gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS)'
# l0b  = '(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'

# dfbin = df.query(binor)
# effl0 = df.query(bin+aa+l0g).shape[0]/df.query(bin).shape[0]
# effl00 = df.query(bin0+aa+l0g0).shape[0]/df.query(bin0).shape[0]
# effgg = df.query(binor+aa+l0b).shape[0]/df.query(binor).shape[0]

# print(effl0+effl00-effgg)
# a  =  df.query('{0} and {1}'.format(bin0,order0)).shape[0]
# b  =  df.query('{0} and {1}'.format(bin,order)).shape[0]
# a0 = df.query('{0} and {1}'.format(bin,order0)).shape[0]
# b0 = df.query('{0} and {1}'.format(bin0,order)).shape[0]
# inter0 = df.query('{0} and {1} and {2}  '.format(bin,bin0,order0)).shape[0]
# inter = df.query('{0} and {1} and {2}'.format(bin,bin0,order)).shape[0]

# b0tosden  = dfbin.query('{0} and {1}'.format(binor,l0b)).shape[0]
# print(effl0*(a+b+a0+b0-inter-inter0)/b0tosden)
# print((a+b+a0+b0-inter-inter0)/dfbin.shape[0])
#c = dfbin.query('{0} and {1} and {2}'.format(bin,l0g))



# cut00 = '(gamma0_L0Calo_ECAL_TriggerET>=gamma_L0Calo_ECAL_TriggerET and )'
# cut01 = '(gamma0_L0Calo_ECAL_TriggerET<gamma_L0Calo_ECAL_TriggerET and (gamma0_L0PhotonDecision_TOS or  gamma0_L0ElectronDecision_TOS))'
# cut10 ='(gamma0_L0Calo_ECAL_TriggerET>=gamma_L0Calo_ECAL_TriggerET and (gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS))'
# cut11 = '(gamma0_L0Calo_ECAL_TriggerET<gamma_L0Calo_ECAL_TriggerET and (gamma_L0PhotonDecision_TOS or  gamma_L0ElectronDecision_TOS))'
# cutden='(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)'
# denominator = df.query('{0} and {1}'.format(cutden,binor)).shape[0] 

# comp0 = df.query('{0} and {1}'.format(cut00,bin0)).shape[0]  + df.query('{0} and {1}'.format(cut01,bin0)).shape[0]  
# comp = df.query('{0} and {1}'.format(cut10,bin)).shape[0]  + df.query('{0} and {1}'.format(cut11,bin)).shape[0] 

# print((comp+comp0)/denominator)

