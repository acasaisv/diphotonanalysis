import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import pandas
# from ROOT import TFile,TTree
import sys
root = '/scratch47/adrian.casais/ntuples/turcal'
#Bs mass window
m1eta,m2eta = 480,640
hdf = 0
efficiency=0
if not hdf:
    
    #f_s = uproot.open(root + '/etammgHardPhotonMC-all.root')
    f_s = uproot.open(root + '/etammgHardPhotonMC.root')
    t_s = f_s['DecayTree/DecayTree']
    # f = TFile(root+'/etammgHardPhotonMC.root')
    # t = f.Get('DecayTree/DecayTree')

    variables = ['eta_M',
                 'eta_P',
                 'eta_PT',
                 'eta_ETA',
                 'eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS',
                 'gamma_L0PhotonDecision_TOS',
                 'gamma_L0ElectronDecision_TOS',
                 'gamma_L0Global_TIS',
                 'gamma_Hlt1Phys_TIS',
                 'gamma_Hlt2Phys_TIS',
                 'mu1_L0DiMuonDecision_Dec',
                 'mu2_L0MuonDecision_Dec',
                 'mu1_IPCHI2_OWNPV',
                 'mu2_IPCHI2_OWNPV',
                 'mu1_ProbNNmu',
                 'mu2_ProbNNmu',
                 'mu1_PT',
                 'mu2_PT',
                 'mu1_P',
                 'mu2_P',
                 'mu1_PX',
                 'mu2_PX',
                 'mu1_PY',
                 'mu2_PY',
                 'mu1_PZ',
                 'mu2_PZ',
                 'mu1_PE',
                 'mu2_PE',
                 'gamma_PT',
                 'gamma_ETA',
                 'gamma_CL',
                 'gamma_P',
                 'gamma_PZ',
                 'gamma_L0Calo_ECAL_realET',
                 'gamma_L0Calo_ECAL_TriggerET',
                 'gamma_ShowerShape',
                 'gamma_PP_IsNotH',
                 'gamma_PP_IsNotE',
                 'gamma_PP_IsPhoton',
                 'gamma_Matching',
                 'gamma_PP_CaloNeutralHcal2Ecal',
                 'gamma_PP_CaloNeutralID',
                 'nSPDHits',
                 'gamma_CaloHypo_X',
                 'gamma_CaloHypo_Y',
                 'gamma_CaloHypo_Saturation',


                 'gamma_TRUEID',
                 'gamma_MC_MOTHER_ID',
                 'mu1_TRUEID',
                 'mu1_MC_MOTHER_ID',
                 'mu2_TRUEID',
                 'mu2_MC_MOTHER_ID',
                 'eta_TRUEID',
                 'eta_MC_MOTHER_ID',
                 'eta_ENDVERTEX_CHI2',
                 ]
    df = t_s.pandas.df(branches=variables)
    #df['eta_ETA'] = 0.5*np.log( (df['eta_P']+df['eta_PZ'])/(df['eta_P']-df['eta_PZ']) )

    df.dropna()
    df.to_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df',mode='w')
else:
    df =pandas.read_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df')


df.query('eta_M > {0} & eta_M < {1}'.format(m1eta,m2eta),inplace=True)

df.query('gamma_CL>0.3 & gamma_P> 6000 & eta_PT > 1000 & gamma_PP_CaloNeutralHcal2Ecal<0.1 and nSPDHits<800',inplace=True)
mc ='abs(gamma_MC_MOTHER_ID)==221  and  gamma_TRUEID==22 and mu1_TRUEID==-13 and mu2_TRUEID==13 and eta_TRUEID==221'
df.query(mc,inplace=True)
df = df.query('mu1_PT > 500 & mu2_PT > 500 &  mu1_IPCHI2_OWNPV < 6 & mu2_IPCHI2_OWNPV <6 & mu1_ProbNNmu > 0.8 & mu2_ProbNNmu > 0.8 and eta_ENDVERTEX_CHI2 < 10')
df = df.query('gamma_PT>3000')
#df = df.query('eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS')
#eta0
Eta1MassRange = zfit.Space('eta_M',(m1eta,m2eta))


#Signal: Double Crystall Ball
from fit_helpers import create_double_cb
name_prefix='Bs_'
BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,mass_range=Eta1MassRange,mass =547.862,sigma=(15,0,20),nevs=len(df)+10)
#Background: exponential
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',len(df)/2,0,len(df))
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
firstOrder = zfit.Parameter(name_prefix+'LegFirst',0.45,-10,10,floating=True)
#secondOrder = zfit.Parameter(name_prefix+'LegSecond',0.5,0,1)
#thirdOrder =zfit.Parameter(name_prefix+'LegThird',0.5,0,1)
#fourthOrder=zfit.Parameter(name_prefix+'LegFourth',0.5,0,1)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
BsBkgComb = zfit.pdf.Legendre(Eta1MassRange,[firstOrder,
                                             #secondOrder,
                                             #thirdOrder,
                                             #fourthOrder
                                             ])
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])



#Extended
modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,
                                BsBkgCombExtended
                                ])
#modelBs = BsCBExtended

data_Bs = zfit.Data.from_numpy(array=df['eta_M'].values,obs=Eta1MassRange)
#CREATE LOSS FUNCTION
modelBs = BsCBExtended
#Extended
nll = zfit.loss.ExtendedUnbinnedNLL(model=modelBs,
                                    data=data_Bs)
#nll = zfit.loss.UnbinnedNLL(model=BsCB,
#                                    data=data_Bs)

minimizer = zfit.minimize.Minuit(tol=1e-4,mode=2,gradient=True,maxiter=int(1e8))
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
result.errors()
print(result)

from hepstats.splot import compute_sweights
#sweights = compute_sweights(modelBs, data_Bs)
#signal_sweights = sweights[list(sweights.keys())[0]]
#df['sweights'] = signal_sweights
#print(sweights)
df['sweights'] = np.ones(len(df))
df.to_hdf(root+'/etammgTurcalHardPhotonMC.h5',key='df',mode='w')

#PLOT

import plot_helpers
import importlib
importlib.reload(plot_helpers)
nSig=zfit.run(BsParameters['nSig'])
plot_helpers.plot_fit(mass=df['eta_M'],
                      full_model=modelBs,
                      components=[BsCBExtended],
                      yields=[nSig],
                      labels=[r'$\eta$ signal double Crystal Ball'],
                      colors=['red'],
                      nbins=30,
                      myrange=(m1eta,m2eta),
                      xlabel=r'M($\mu^+\mu^-\gamma$)',
                      savefile='./etammgMCFit-forL0sys.pdf')
                      # savefile='/home3/adrian.casais/figstoscp/etammgMCFit-forL0sys.pdf')


import pandas as pd
from helpers import pack_eff
hessians = result.hesse()
params = [BsParameters['scale_m'],BsParameters['sigma'],BsParameters['dSigma'],BsParameters['a_l'],BsParameters['a_r'],BsParameters['n_l'],BsParameters['n_r']]
errors = [hessians[param]['error'] for param in params]
params = [zfit.run(param) for param in params]
values = []
for val,err in zip(params,errors):
    values.append(pack_eff(val,err))
    
dic = {'Parameter':[#$'n_\\textrm{Signal}',
                    #'n_\\textrm{Background}',
                    '$m/m_\\textrm{PDG}$',
                    '$\\sigma$',
                    '$\\Delta(\\sigma)$',
                    '$\\alpha_L$',
                    '$\\alpha_R$',
                    '$n_L$',
                    '$n_R$',]
       ,'Units':['','MeV','MeV','','','',''],
       'Value':values}


dflatex = pd.DataFrame(dic)
print(dflatex.to_latex(index=False,escape=False))

