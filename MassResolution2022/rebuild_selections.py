from helpers import get_ALPsdf, masses_map, sim10_masses_map
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import crystalball as cb
from plot_helpers import *
from energy_helpers import *

                                    
def res_fit(df,range=(-0.15,0.15)):
    from fit_helpers import create_double_cb
    import zfit
    obs = zfit.Space('reso',range)
    #BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.01,0.001,1),nevs=len(df)+100,extended=True)
    BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.2,0.01,1),nevs=len(df)+100,extended=True)
    model_Bs=BsCBExtended
    df.eval('gamma_resolution = (-gamma_P + gamma_TRUEP_E)/gamma_TRUEP_E',inplace=True)
    df.query(f'gamma_resolution > {range[0]} and gamma_resolution < {range[1]}')

    data = zfit.Data.from_numpy(array=df['gamma_resolution'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =BsCBExtended,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=800000000)
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    reload(plot_helpers)
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['gamma_resolution'],
                        full_model=BsCBExtended,
                        components=[BsCBExtended],
                        yields=[nSig],
                        labels=[r'$B_s$ signal double Crystal Ball'],
                        colors=['blue'],
                        nbins=30,
                        myrange=range,
                        xlabel=r'Resolution',
                        savefile=f'./resolution.pdf',
                        plot=False,
                        pull_plot=False)

def smear_photons(df,b=0.0335,mult=1.0):
    df['gamma_smearP'] = smear(df['gamma_TRUEP_E'],b=b,mult=mult)
    df['gamma0_smearP'] = smear(df['gamma0_TRUEP_E'],b=b,mult=mult) 
    return df
def smear_mass(df,b=0.335,mult=1.0):
    df = smear_photons(df,b=b,mult=mult)
    for comp in 'X','Y','Z':
        df[f'gamma_smearP{comp}'] = df['gamma_smearP']/df['gamma_P']*df[f'gamma_P{comp}']
        df[f'gamma0_smearP{comp}'] = df['gamma0_smearP']/df['gamma0_P']*df[f'gamma0_P{comp}']
        df[f'B_s0_smearP{comp}'] = df[f'gamma0_smearP{comp}'] + df[f'gamma_smearP{comp}']

    df.eval('gamma_smearPT = sqrt(gamma_smearPX**2 + gamma_smearPY**2)',inplace=True)
    df.eval('gamma0_smearPT = sqrt(gamma0_smearPX**2 + gamma0_smearPY**2)',inplace=True)
    df.eval('B_s0_smearPT = sqrt(B_s0_smearPX**2 + B_s0_smearPY**2)',inplace=True)
    df.eval("B_s0_smearP = sqrt(B_s0_smearPX**2 + B_s0_smearPY**2 + B_s0_smearPZ**2)",inplace=True)
    df.eval("B_s0_smearPE = gamma_smearP + gamma0_smearP",inplace=True)
    df.eval("B_s0_smearM = sqrt(B_s0_smearPE**2 - B_s0_smearP**2)",inplace=True)
    print(f'smear = {b}')
    return df

def plot_all_peaks(sat=False):
    df = {}
    sim10root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    fig, ax = plt.subplots(1,1)
    ax = ax.flatten()
    ax = np.array(ax)
    ax = ax.flatten()
    ax[0].set_xlim(4500,21500)
    ax[0].set_xlabel(r"$M(\gamma \gamma)$ [GeV]",horizontalalignment='right',x=1.0)
    ax[0].set_ylabel(r"AU",loc='top')
    for key in range(40,51):
        df[key] = get_ALPsdf(key,bs=False,background=False,extravars=['gamma_CaloHypo_Saturation','gamma0_CaloHypo_Saturation'],sample=int(10e4))
        if sat:
            df[key].query('gamma_CaloHypo_Saturation ==0  and gamma0_CaloHypo_Saturation==0',inplace=True)
        ax[0].hist(df[key]['B_s0_M'],bins=50,histtype='step',density=True)
    fig.savefig(f"./peaks-sat={sat}.pdf")
def plot_smeared_variables(df,b):
    fig,ax = plt.subplots(2,2)
    ax = ax.flatten()
    rangep = [0,200e3]
    rangepbs=[0,400e3]
    rangept = [0,20e3]
    rangeptbs = [0,35e3]
    color = 'red'
    color2 = 'green'
    ax[0].hist(df['gamma_P'],bins=50,label='Regular',density=True,range=rangep)
    ax[0].hist(df['gamma_TRUEP_E'],bins=50,histtype='step',label='TRUE',density=True,range=rangep,color=color2)
    ax[0].hist(df['gamma_smearP'],bins=50,histtype='step',label='Smear',density=True,range=rangep,color=color)
    ax[0].set_xlabel(r'$\gamma \ p$ [MeV]',horizontalalignment='left')
    ax[1].hist(df['gamma_PT'],bins=50,density=True,range=rangept)
    ax[1].hist(df['gamma_TRUEPT'],bins=50,density=True,range=rangept,color=color2,histtype='step')
    ax[1].hist(df['gamma_smearPT'],bins=50,histtype='step',density=True,range=rangept,color=color)
    ax[1].set_xlabel(r'$\gamma \ p_T$ [MeV]',horizontalalignment='left')
    ax[2].hist(df['B_s0_P'],bins=50,density=True,range=rangepbs)
    ax[2].hist(df.eval('sqrt((gamma_TRUEP_X+gamma0_TRUEP_X)**2 +(gamma_TRUEP_Y+gamma0_TRUEP_Y)**2 + (gamma_TRUEP_Z+gamma0_TRUEP_Z)**2)'),bins=50,density=True,range=rangepbs,color=color2,histtype='step')
    ax[2].hist(df['B_s0_smearP'],bins=50,histtype='step',density=True,range=rangepbs,color=color)
    ax[2].set_xlabel(r'$\gamma\gamma \ p$ [MeV]',horizontalalignment='left')
    ax[3].hist(df['B_s0_PT'],bins=50,density=True,range=rangeptbs)
    ax[3].hist(df.eval('sqrt((gamma_TRUEP_X+gamma0_TRUEP_X)**2 +(gamma_TRUEP_Y+gamma0_TRUEP_Y)**2 )'),bins=50,density=True,range=rangeptbs,color=color2,histtype='step')
    ax[3].hist(df['B_s0_smearPT'],bins=50,histtype='step',density=True,range=rangeptbs,color=color)
    ax[3].set_xlabel(r'$\gamma\gamma \ p_T$ [MeV]',horizontalalignment='left')
    plt.tight_layout()
    ax[0].legend()
    plt.savefig(f'compare_smear-{b}.pdf')

def check_selection(df):
    vanilla_eff = df.query('gamma_P>6000 and gamma_PT > 3000 and gamma0_P>6000 and gamma0_PT > 3000 and B_s0_PT > 2000').shape[0]/df.shape[0]
    smear_eff = df.query('gamma_smearP>6000 and gamma_smearPT > 3000 and gamma0_smearP>6000 and gamma0_smearPT > 3000 and B_s0_smearPT > 2000').shape[0]/df.shape[0] 

    print(f'Vanilla eff: {vanilla_eff:.3f}')
    print(f'smear eff: {smear_eff:.3f}')

if __name__ == '__main__':
    #plot_all_peaks(False)
    #plot_all_peaks(True)
    #quit()
    masses_map = sim10_masses_map
    df = {}
    offset = {}
    offset_smear = {}
    center_masses = {}
    for key in masses_map.keys():
    #for key in [41]:
        
        #center_masses[key]= 1000.*float("".join(list(filter(lambda x: x.isdigit(),masses_map[key]))))
        if not (type(masses_map[key]) is str):
            center_masses[key] = masses_map[key]*1000.
    center_masses['bs']= 5366

    #fig,ax = plt.subplots(1,3)
    fig, ax = plt.subplots(1,1)
    ax = np.array(ax)
    ax = ax.flatten()
    ax[0].set_xlim(4500,21500)
    #ax[0].set_xlim(-0.1,1)
    sim10root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
    #for key in sim10_masses_map.keys():
    #for key in range(40,51):
    for key in [45]:
        
        df[key] = get_ALPsdf(root+f'491000{key}_1000-1099_Sim10a-priv-v44r11p1.root',bs=False,background=False,extravars=['gamma_CaloHypo_Saturation','gamma0_CaloHypo_Saturation','gamma_TRUEP_E','B_s0_TRUEP_E','gamma_TRUEPT','B_s0_TRUEPT',
        'gamma_TRUEP_X','gamma0_TRUEP_X','gamma_TRUEP_Y','gamma0_TRUEP_Y','gamma_TRUEP_Z','gamma0_TRUEP_Z'],sample=int(10e4),stripping=False)
        df[key]['B_s0_TRUEP_M']= sim10_masses_map[key]*1000
        print(df[key].keys())
        df[key].query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0',inplace=True)
        #res_fit(df[key])
        # b = 0.0380
        # df[key] = smear_mass(df[key],b=0.0380)
        # plot_smeared_variables(df[key],b)
        # check_selection(df[key],b)
        b=0.0335
        mult = 1.003
        df[key] = smear_mass(df[key],b=b,mult=mult)
        check_selection(df[key])
        plot_smeared_variables(df[key],b)
        b=b/1.09
        df[key] = smear_mass(df[key],b=b,mult=mult)
        plot_smeared_variables(df[key],b)
        check_selection(df[key])



