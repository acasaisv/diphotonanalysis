import uproot3 as uproot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle 
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import xgboost as xgb

from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score

from sklearn.metrics import roc_curve,auc

from helpers import sim10_masses_map,nearest_rect
import plot_helpers


def get_vars():
    vars_bdt = []
    var_list = [
        'gamma_{}_cc_mult',
        'gamma_{}_cc_vPT',
        'gamma_{}_cc_PZ',
        'gamma_{}_cc_asy_P',
        'gamma_{}_cc_asy_PT' 
        ]
    for r '1.00','1.35','1.70':
        for var in var_list:
            vars_bdt.append(var_list.format(r))
    

    trigger_vars = ['B_L0ElectronDecision_Dec','B_L0PhotonDecision_Dec',
                    'B_Hlt1TrackMVADecision_Dec',
                    'B_Hlt2RadiativeBs2PhiGammaDecision_Dec',
                    'B_M','phi_M'
                    ]
    return vars_bdt,trigger_vars
    
def get_cut():
    L0 = '(B_L0ElectronDecision_Dec | B_L0PhotonDecision_Dec)'
    L1 = 'B_Hlt1TrackMVADecision_Dec'
    L2 = 'B_Hlt2RadiativeBs2PhiGammaDecision_Dec'
    trigger_cond = '{0} & {1} & {2}'.format(L0,L1,L2)
    trigger_cond = L0
    return trigger_cond

def signal_df(vars,cut,sample=10.e4):
    root = '/scratch47/adrian.casais/ntuples/turcal'            
    f_s = uproot.open(root + '/b02phigammaMC-0smear.root')
    t_s = f_s['DecayTree/DecayTree']
    df = t_s.pandas.df(branches=vars)
    df.query(cut,inplace=True)
    return df

def background_df(vars,cut,sample=10.e4):
    root = '/scratch47/adrian.casais/ntuples/turcal'
    f_b = uproot.open(root + '/b02phigammaS34r0p1.root')
    t_b = f_b['DecayTree/DecayTree']
    df  = t_b.pandas.df(branches=vars)
    df.query('B_M>5800',inplace=True) # only right sideband
    df.query(cut,inplace=True)
    return df 


def get_training_data(signal_df,background_df,bdt_vars):
    signal_df["category"]= 1 #Use 1 for signal
    background_df["category"] = 0 #use 0 for bkg
    training_data = pd.concat([signal_df[bdt_vars+['category']],background_df[bdt_vars+['category']]],copy=True,ignore_index=True,sort=False)
    training_data=training_data.sample(frac=1).reset_index(drop=True)
    training_data = training_data.dropna()

    return training_data


def train_classifier(x,x_test,y):

    bdt = xgb.XGBClassifier(
                eta=0.7,
                objective='binary:logistic',
                #eval
                )
    bdt.fit(x,y)

    return bdt,x,x_test
def make_roc_curve(y_test,x_test,bdt):
    fig,ax = plt.subplots(1,1)
    type = 'xgb'
    proba_test = bdt.predict_proba(x_test)[:,1]
    fpr,tpr,_ = roc_curve(y_test,proba_test)
    ax.plot(fpr,tpr,label = f'{type}')
    ax.legend()
    fig.savefig('roc.pdf')




def kfold(training_data,var_list,scores=True):
    kf = KFold(n_splits=5)
    x,y = training_data[var_list],training_data["category"]
    if scores:
        bdt = xgb.XGBClassifier(
            eta=0.7,
            objective='binary:logistic',) 
        scores = cross_val_score(bdt,x,y,cv=kf)
    print(5*"#")
    print("Cross validation scores:")
    print(scores)
    print(5*"#")
    classifiers = []
    for tri,tti in kf.split(x):
        x_train,y_train = x.iloc[tri],y.iloc[tri]
        classifiers.append(xgb.XGBClassifier(
            eta=0.7,
            objective='binary:logistic',) )
        classifiers[-1].fit(x_train,y_train)
    return classifiers

def performance_test(bdts,x,y):
    fig,ax = plt.subplots(1,1)
    sig = np.zeros(len(x[np.array(y,dtype='bool')]))
    bkg = np.zeros(len(x[~np.array(y,dtype='bool')]))
    for bdt in bdts:
        sig += bdt.predict_proba(x[np.array(y,dtype='bool')])[:,1]/len(bdts)
        bkg += bdt.predict_proba(x[~np.array(y,dtype='bool')])[:,1]/len(bdts)
    ax.hist(sig,bins=50,color='red',histtype='step',density=True,label='Signal')
    ax.hist(bkg,bins=50,color='blue',histtype='step',density=True,label='Background')
    ax.legend()
    fig.savefig("overtraining-phi.pdf")





if __name__ == '__main__':

    train_bdt = True
    bdt_vars,trigger_vars = get_vars()
    cut = get_cut()
    df_background = background_df(bdt_vars+trigger_vars,cut,sample=np.inf)
    df_signal = signal_df(vars = bdt_vars+trigger_vars, cut = cut,sample=np.inf)
    training_data=get_training_data(df_signal,df_background,bdt_vars)
    x,y = training_data[bdt_vars],training_data["category"]

    #plot_variables(df_signal,df_background,vars_b)
    plt.clf()
    if train_bdt:
        bdts =kfold(training_data,bdt_vars,scores=True)
        with open('bdt.pickle','wb') as handle:
            pickle.dump(bdts,handle)
        #make_roc_curve(y_test,x_test,bdt)
        #plt.clf()
        #x,y = training_data[bdt_vars],training_data["category"]
        #performance_test(bdts,x,y)
        #plt.clf()
    else:
        with open("bdt.pickle", "rb") as handle:
            bdts = pickle.load(handle)
    performance_test(bdts,x,y) 

    # df_background['bdt']=bdt.predict_proba(df_background[vars_b])[:,1]
    # df_signal['bdt']=bdt.predict_proba(df_signal[vars_b])[:,1]
    
        
    # ####stupid tests
    # fig,ax = plt.subplots(1,1)
    # punzi_figure(df_signal,df_background,[6000-3*150,6000+3*150])
    # punzi_figure(df_signal,df_background,[17000 -3*400, 17000 + 3*400])
    # plt.savefig("punzi.pdf")
    # plt.clf()
    # plt.hist(df_background.query('bdt>0.6')['B_M'],bins=100,range=[4800,6300],histtype='step')
    # plt.savefig("mass_spectrum_bdt.pdf")