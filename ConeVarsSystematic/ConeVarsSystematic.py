import uproot3 as uproot
import matplotlib.pyplot as plt
import pandas as pd

import xgboost as xgb
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from bdt_helpers import get_cuts,process_variables
from helpers import get_ALPsdf,nearest_rect
import plot_helpers
        
name_map = {
    'gamma_1.00_cc_mult':r'$\gamma$ Cone$_{1.0}$ Mult.',
    'gamma_1.00_cc_vP':r'$\gamma$ Cone$_{1.0}$ $p$',
    'gamma_1.00_cc_vPT':r'$\gamma$ Cone$_{1.0}$ $p$',
    'gamma_1.00_cc_asy_P':r'$\gamma$ Cone$_{1.0}$ $p$ Asym.',
    'gamma_1.00_cc_asy_PT':r'$\gamma$ Cone$_{1.0}$ $p_T$ Asym.',

    'gamma_1.35_cc_mult':r'$\gamma$ Cone$_{1.35}$ Mult.',
    'gamma_1.35_cc_vP':r'$\gamma$ Cone$_{1.35}$ $p$',
    'gamma_1.35_cc_vPT':r'$\gamma$ Cone$_{1.35}$ $p_T$',
    'gamma_1.35_cc_asy_P':r'$\gamma$ Cone$_{1.35}$ $p$ Asym.',
    'gamma_1.35_cc_asy_PT':r'$\gamma$ Cone$_{1.35}$ $p_T$ Asym.',

    'gamma_1.70_cc_mult':r'$\gamma$ Cone$_{1.7}$ Mult.',
    'gamma_1.70_cc_vP':r'$\gamma$ Cone$_{1.7}$ $p$',
    'gamma_1.70_cc_vPT':r'$\gamma$ Cone$_{1.7}$ $p_T$',
    'gamma_1.70_cc_asy_P':r'$\gamma$ Cone$_{1.7}$ $p$ Asym.',
    'gamma_1.70_cc_asy_PT':r'$\gamma$ Cone$_{1.35}$ $p_T$ Asym.',
}

def load_dfs():
    df_data = pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='df')
    df_MC = pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/b2kstgamma.h5',key='dfMC')
    return df_data,df_MC

def print_isolation(mc,data):
    data = data.query('B_M>5700')
    args = {'histtype':'step','bins':25}
    vars = ['gamma_1.00_cc_asy_PT',
            'gamma_1.35_cc_asy_PT',
            'gamma_1.70_cc_asy_PT',
            'gamma_1.00_cc_asy_P',
            'gamma_1.35_cc_asy_P',
            'gamma_1.70_cc_asy_P',]
    mydpi = 80.
    fig,ax =plt.subplots(2,3,figsize=(1280/mydpi,720/mydpi),dpi=mydpi)
    ax =ax.flatten()
    for i in range(6):
        var = vars[i]
        ax[i].hist(data[var],
                   #weights=1-data['sweights'],
                   density=True,**args,label='s34r0p1')
        ax[i].hist(mc[var],density=True,**args,label='sim10')
        ax[i].set_title(var,loc='right')

    ax[0].legend(bbox_to_anchor=(0.1,1.10))
    plt.show()


def get_bdt_vars():
    bdt_vars = []
    for r in '1.00','1.35','1.70':
        bdt_vars +=[
        f'gamma_{r}_cc_vP',
        f'gamma_{r}_cc_asy_PT',
        f'gamma_{r}_cc_asy_P',
        f'gamma_{r}_cc_mult',
        f'gamma_{r}_cc_vPT'
        ] 
    return bdt_vars
def load_training_dfs(bdt_vars):
    var_list = [
     #'B_DIRA_OWNPV',
       #'kst_OWNPV_CHI2',
       #'phi_PT',
       #'kplus_IPCHI2_OWNPV',
       #'piminus_IPCHI2_OWNPV',
        'gamma_1.00_cc_asy_PT',
        'gamma_1.35_cc_asy_PT',
        'gamma_1.70_cc_asy_PT',
        'gamma_1.00_cc_asy_P',
        'gamma_1.35_cc_asy_P',
        'gamma_1.70_cc_asy_P',
        'gamma_1.70_cc_mult',
        'gamma_1.70_cc_vPT',
        'gamma_1.70_cc_PZ',
        'gamma_1.35_cc_vPT',
        'gamma_1.35_cc_PZ',
        'gamma_1.35_cc_mult',
        'gamma_1.00_cc_vPT',
        'gamma_1.00_cc_PZ',  
        'gamma_1.00_cc_mult',
        

        ]

    truth_vars = ['gamma_MC_MOTHER_ID',
        'gamma_TRUEID',
        'kst_TRUEID',
        'kplus_TRUEID',
        'piminus_TRUEID',
        'B_TRUEID']
    all_cone_vars=[]
    all_cone_vars_1=[]

    trigger_vars = ['B_L0ElectronDecision_Dec','B_L0PhotonDecision_Dec',
                    'B_Hlt1TrackMVADecision_Dec',
                    'B_Hlt2RadiativeBd2KstGammaDecision_Dec',
                    'B_M','kst_M','gamma_PT','gamma_P','gamma_PP_Saturation'
                    ]


    L0 = '(B_L0ElectronDecision_Dec | B_L0PhotonDecision_Dec)'
    L1 = 'B_Hlt1TrackMVADecision_Dec'
    L2 = 'B_Hlt2RadiativeBd2KstGammaDecision_Dec'
    trigger_cond = f'{L0} & {L1} & {L2} & gamma_PP_Saturation<1 & gamma_PT > 3000 & gamma_P>6000'
    #trigger_cond = L0
    

    root = '/scratch47/adrian.casais/ntuples/turcal'
    f_b = uproot.open(root + '/b02kstargammaS34r0p1.root')
    t_b = f_b['DecayTree/DecayTree']


    root = '/scratch47/adrian.casais/ntuples/turcal'            
    f_s = uproot.open(root + '/b02kstargammaMC-sim10.root')
    t_s = f_s['DecayTree/DecayTree']



    signal_df = t_s.pandas.df(branches=var_list+trigger_vars+truth_vars)
    
    background_df  = t_b.pandas.df(branches=var_list+trigger_vars)
    mc ='abs(gamma_MC_MOTHER_ID)==511  and  gamma_TRUEID==22 and kst_TRUEID==313 and kplus_TRUEID == 321 and piminus_TRUEID==-211 and abs(B_TRUEID)==511'
    signal_df.query(trigger_cond + ' and ' + mc,inplace=True)
    background_df.query(trigger_cond,inplace=True)
    background_df.query('B_M>5700',inplace=True)

    min_len = min(signal_df.shape[0],background_df.shape[0])
    signal_df = signal_df.sample(n=min_len)
    background_df = background_df.sample(n=min_len)

    #signal_df = signal_df.sample(
    #background_df = background_df.sample(n=10000)

    
    for r in '1.00','1.35','1.70':
        signal_df[f'gamma_{r}_cc_vP'] = np.sqrt(signal_df[f'gamma_{r}_cc_vPT']**2 + signal_df[f'gamma_{r}_cc_PZ']**2)
        background_df[f'gamma_{r}_cc_vP'] = np.sqrt(background_df[f'gamma_{r}_cc_vPT']**2 + background_df[f'gamma_{r}_cc_PZ']**2)
    
    return signal_df,background_df
def make_roc_curve(y_test,x_test,bdt,name='roc.pdf'):
    from sklearn.metrics import roc_curve,auc
    fig,ax = plt.subplots(1,1)
    type = 'xgb'
    proba_test = bdt.predict_proba(x_test)[:,1]
    fpr,tpr,_ = roc_curve(y_test,proba_test)
    ax.plot(fpr,tpr,label = f'{type}')
    ax.legend()
    fig.savefig(name)
def load_for_bdt(signal_df,background_df,bdt_vars):

    signal_df["category"]= 1 #Use 1 for signal
    background_df["category"] = 0 #use 0 for bkg
    background_df.query('B_M>5700',inplace=True)
    training_data = pd.concat([background_df[bdt_vars+['category'] ],signal_df[bdt_vars+['category']]],copy=True,ignore_index=True,sort=False)
    training_data=training_data.sample(frac=1).reset_index(drop=True)
    training_data = training_data.dropna()

    x,y = training_data[bdt_vars],training_data["category"]
    from sklearn.model_selection import KFold
    kf = KFold(n_splits=5)
    classifiers = []
    counter =0
    for tri,tti in kf.split(x):
        counter+=1
        x_train,y_train = x.iloc[tri],y.iloc[tri]
        x_test,y_test = x.iloc[tti],y.iloc[tti]
        classifiers.append(xgb.XGBClassifier(
            eta=0.7,
            objective='binary:logistic',) )
        classifiers[-1].fit(x_train,y_train)
        make_roc_curve(y_test,x_test,classifiers[-1],f'roc-{counter}.pdf')
    return classifiers

    
def calculate_bdt(mc,data,alps,bkg,bdts,vars_bdt):
    mean_bdtmc=np.zeros(len(mc))
    mean_bdtdata=np.zeros(len(data))
    mean_bdtalps=np.zeros(len(alps))
    mean_bdtbkg=np.zeros(len(bkg)) 
    print(mc[vars_bdt]) 
    print(data[vars_bdt])
    for i in range(len(bdts)):
        mean_bdtmc += bdts[i].predict_proba(mc[vars_bdt])[:,1]/len(bdts)
        mean_bdtdata += bdts[i].predict_proba(data[vars_bdt])[:,1]/len(bdts)
        mean_bdtalps += bdts[i].predict_proba(alps[vars_bdt])[:,1]/len(bdts)
        mean_bdtbkg += bdts[i].predict_proba(bkg[vars_bdt])[:,1]/len(bdts)

    mc['bdt']= mean_bdtmc
    data['bdt']= mean_bdtdata
    alps['bdt'] = mean_bdtalps
    bkg['bdt'] = mean_bdtbkg
def process_vars(mc,data,bkg):
    for r in '1.00','1.35','1.70':
        mc[f'gamma_{r}_cc_vP'] = np.sqrt(mc[f'gamma_{r}_cc_vPT']**2 + mc[f'gamma_{r}_cc_PZ']**2)
        data[f'gamma_{r}_cc_vP'] = np.sqrt(data[f'gamma_{r}_cc_vPT']**2 + data[f'gamma_{r}_cc_PZ']**2)
        bkg[f'gamma_{r}_cc_vP'] = np.sqrt(bkg[f'gamma_{r}_cc_vPT']**2 + bkg[f'gamma_{r}_cc_PZ']**2)

def process_alps(alps):
    for r in '1.00','1.35','1.70':
        alps[f'gamma_{r}_cc_vP'] = np.sqrt(alps[f'B_s0_{r}_cc_vPT']**2 + alps[f'B_s0_{r}_cc_PZ']**2)
        alps[f'gamma_{r}_cc_asy_PT'] = alps[f'B_s0_{r}_cc_asy_PT']
        alps[f'gamma_{r}_cc_asy_P'] = alps[f'B_s0_{r}_cc_asy_P'] 
        alps[f'gamma_{r}_cc_mult'] = alps[f'B_s0_{r}_cc_mult']
        alps[f'gamma_{r}_cc_vPT'] = alps[f'B_s0_{r}_cc_vPT']
    return alps
    

    
def get_error_w(weights_num,weights_den):
    num= weights_num.sum()
    den= weights_den.sum()
    W1 = num
    W2 = den-num
    num2 = np.square(weights_num)
    den2 = np.square(weights_den)
    num2_sum = num2.sum()
    den2_sum = den2.sum()
    sigma1 = num2_sum
    sigma2 = den2_sum - num2_sum
    
    return np.sqrt((W1**2*sigma2 + W2**2*sigma1)/(W1 + W2)**4)
def get_error(num,den):
    return np.sqrt(num*(den-num)/den**3)
def plot_cone_vars(df_s,df_b,df_alps,vars_bdt):
    i,j = nearest_rect(len(vars_bdt))
    if j>i:
        i,j=j,i
    i,j =4,4
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(i,j,figsize=(13,13))
    ax = ax.flatten()
    # for r in '1.00','1.35','1.70':
        
    #     df_b = df_b.query(f'`gamma_{r}_cc_vP` < 500000')
    #     df_b = df_b.query(f'`gamma_{r}_cc_vPT` < 50000')

    #     df_s = df_s.query(f'`gamma_{r}_cc_vP` < 500000')
    #     df_s = df_s.query(f'`gamma_{r}_cc_vPT` < 50000')

    #     df_alps = df_alps.query(f'`gamma_{r}_cc_vPT` < 500000')
    #     df_alps = df_alps.query(f'`gamma_{r}_cc_vPT` < 500000') 
    for a,var in zip(ax,vars_bdt):
        print(var)
        myrange=None
        a.set_xlabel(name_map[var])
        a.set_ylabel('A.U.')
        if 'mult' in var:
            myrange = (0,50)
        if 'cc_vP' in var and not 'asym' in var:
            myrange = (0,500.e3)
        if 'gamma_1.00_cc_vP' in var:
            myrange = (0,200.e3)
        if 'gamma_1.00_cc_vP' in var:
            myrange = (0,200.e3)
        if 'gamma_1.35_cc_vP' in var:
            myrange = (0,350.e3)
        if 'cc_vPT' in var and not 'asym'  in var:
            myrange = (0,25.e3)

        a.hist(df_s[var],bins=50,label='Signal',density=True,range=myrange)
        a.hist(df_b[var],bins=50,label='Background',color='red',histtype='step',density=True,range=myrange)
        #a.hist(df_alps[var],bins=50,label='ALPs MC',color='green',histtype='step',density=True)
         
        
    ax[0].legend()
    plt.tight_layout()
    fig.savefig("variables.pdf")

def plot_compare_sig(df_s,df_b,vars_bdt):
    i,j = nearest_rect(len(vars_bdt))
    if j>i:
        i,j=j,i
    dpi = 80.
    i,j =4,4
    px = 1./dpi
    fig,ax = plt.subplots(i,j,figsize=(13,13))
    ax = ax.flatten()
    # for r in '1.00','1.35','1.70':
        
    #     df_b = df_b.query(f'`gamma_{r}_cc_vP` < 500000')
    #     df_b = df_b.query(f'`gamma_{r}_cc_vPT` < 50000')

    #     df_s = df_s.query(f'`gamma_{r}_cc_vP` < 500000')
    #     df_s = df_s.query(f'`gamma_{r}_cc_vPT` < 50000')

    for a,var in zip(ax,vars_bdt):
        myrange=None
        a.set_xlabel(name_map[var])
        a.set_ylabel('A.U.')
        if 'mult' in var:
            myrange = (0,50)
        if 'cc_vP' in var and not 'asym' in var:
            myrange = (0,500.e3)
        if 'gamma_1.00_cc_vP' in var:
            myrange = (0,200.e3)
        if 'gamma_1.00_cc_vP' in var:
            myrange = (0,200.e3)
        if 'gamma_1.35_cc_vP' in var:
            myrange = (0,350.e3)
        if 'cc_vPT' in var and not 'asym'  in var:
            myrange = (0,25.e3)

        a.hist(df_s[var],bins=50,label='Signal MC',density=True,range=myrange)
        a.hist(df_b[var],bins=50,label='Signal Data',color='red',histtype='step',density=True,weights=df_b['sweights'],range=myrange)
        #a.hist(df_alps[var],bins=50,label='ALPs MC',color='green',histtype='step',density=True)
         
        
    ax[0].legend()
    plt.tight_layout()
    fig.savefig("variables-mcdata.pdf")
def plot_bdt(sig_mc,sig_data,background):
    fig,ax = plt.subplots(1,1)
    ax.hist(sig_mc['bdt'],bins=50,color='red',density=True,label='Signal MC')
    ax.hist(sig_data['bdt'],weights=sig_data['sweights'],bins=50,color='green',histtype='step',density=True,label='Signal Data')

    ax.hist(background['bdt'],bins=50,color='blue',density=True,label='Background data')
    ax.legend()
    fig.savefig("compare-sig-bkg.pdf")
if __name__=='__main__':
    load = False
    data,mc = load_dfs()
    # print_isolation(mc,data)
    import pickle
    vars_bdt=get_bdt_vars()
    signal_df,background_df = load_training_dfs(vars_bdt)
    extravars=process_variables()[2]
    _,cut=get_cuts()
    #alps = get_ALPsdf(41,background=False,extracut=cut,extravars=extravars)
    #alps = process_alps(alps)
    process_vars(mc,data,background_df)
    plot_cone_vars(signal_df,background_df,None,vars_bdt)
    plot_compare_sig(mc,data,vars_bdt)
    print("plots out")
    if load:
       with open('bdts.pickle','rb') as handle:
        bdts = pickle.load(handle)
    else:
        bdts=load_for_bdt(signal_df,background_df,vars_bdt) 
        with open('bdts.pickle','wb') as handle:
            pickle.dump(bdts,handle)
    calculate_bdt(mc,data,alps,background_df,bdts,vars_bdt)
    plot_bdt(mc,data,background_df)
    for i in np.linspace(0.1,1,20):
        numMC = len(mc.query('bdt>{}'.format(i)))
        denMC = len(mc)
        effMC = numMC/denMC
        errorMC = get_error(numMC,denMC)
        numData_weights = data.query('bdt>{}'.format(i))['sweights']
        denData_weights = data['sweights']
        effData = numData_weights.sum()/denData_weights.sum()
        errData = get_error_w(numData_weights,denData_weights)
        effALPs = alps.query(f'bdt>{i}').shape[0]/alps.shape[0]
        print(fr"{i:.2f} & \num{{{100*effMC:.2f}({100*errorMC:.2f})}} & \num{{{100*effData:.2f}({100*errorData:.2f})}}")
