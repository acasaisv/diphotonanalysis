from  ROOT import *
import uproot3 as uproot
import uproot as u
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from helpers import pTsHlt2,etasHlt2,spdsHlt2,get_error,get_error_w,pack_eff,get_ALPsdf,sim10_masses_map,bins,ufloat_to_str,ufloat_eff,get_ALPsdf_sim10b
import pickle
from uncertainties import ufloat

if __name__ == '__main__':
    
    for i in [43]:
      
        bs = False
        if i=='bs':
            bs=True
        
        if i=='sim10':
            continue
        #df = get_df(i,bs,background=False)
        extravars = ['B_s0_PX','B_s0_PY','B_s0_PZ']
        if i=='gg':
            myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
            df = get_ALPsdf_sim10b(myfile,background=False,bs=True,
     extravars=extravars,stripping=False)
            root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
            f_t = TFile(root+f'../b2gg-stripping.root')
            t_t = f_t.Get('MCDecayTreeTuple/MCDecayTree')
        else:
            root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
            myfile=root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root'
            df = get_ALPsdf(myfile,bs,background=False,extravars=extravars,stripping=False)
            f_t = TFile(root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root')
            t_t = f_t.Get('MCDecayTreeTuple/MCDecayTree')
    f_truth = u.open(f'{myfile}:MCDecayTreeTuple/MCDecayTree')
    keys=[]
    for comp in 'X','Y','Z','E':
        for i in '','0':
            keys.append(f'gamma{i}_TRUEP_{comp}')
                
    df_t = f_truth.arrays(keys,library='pd')
    theta_cut='(gamma_PT >0'
    kinematic_cut = '(gamma_PT >0'
    

    for comp in 'X','Y','Z','E':
        df_t.eval(f'B_s0_P{comp} = gamma_TRUEP_{comp} + gamma0_TRUEP_{comp}',inplace=True)
        df_t.eval(f'gamma_P{comp} = gamma_TRUEP_{comp}',inplace=True)
        df_t.eval(f'gamma0_P{comp} = gamma_TRUEP_{comp}',inplace=True)
    for i in '','0':
        
        df_t.eval(f'gamma{i}_THETA  = arctan(sqrt(gamma{i}_TRUEP_X**2+gamma{i}_TRUEP_Y**2)/gamma{i}_TRUEP_Z)',inplace=True)
        df.eval(f'gamma{i}_THETA  = arctan(sqrt(gamma{i}_PX**2+gamma{i}_PY**2)/gamma{i}_PZ)',inplace=True)
        df_t.eval(f'gamma{i}_PHI  = arctan(gamma{i}_TRUEP_Y/gamma{i}_TRUEP_X)',inplace=True)
        df.eval(f'gamma{i}_PHI  = arctan(gamma{i}_TRUEP_Y/gamma{i}_TRUEP_X)',inplace=True)

        df_t.eval(f'gamma{i}_PT=sqrt(gamma{i}_TRUEP_X**2 + gamma{i}_TRUEP_Y**2)',inplace=True)
        df_t[f'gamma{i}_P']=df_t[f'gamma{i}_TRUEP_E']
        theta_cut += f' and gamma{i}_THETA >0.07 and gamma{i}_THETA <0.2'
        #theta_cut += f'and abs(gamma{i}_PX/gamma{i}_PZ) <(4.5/12.5) and abs(gamma{i}_PY/gamma{i}_PZ) <(3.5/12.5)'
        #theta_cut += f'and abs(gamma{i}_PX/gamma{i}_PZ) >(0.25/12.5) and abs(gamma{i}_PY/gamma{i}_PZ) >(0.25/12.5)'
        kinematic_cut += f' and gamma{i}_PT>3000 and gamma{i}_P>6000'
    df_t.eval('B_s0_PT=sqrt(B_s0_PX**2 + B_s0_PY**2)',inplace=True)
    df_t.eval('B_s0_P=sqrt(B_s0_PX**2 + B_s0_PY**2+B_s0_PZ**2)',inplace=True)
    
    theta_cut +=')'
    kinematic_cut += 'and B_s0_PT >2000 and (gamma_PT + gamma0_PT) >10000)'
    print('theta and kinematic: ',df.query(f'{theta_cut} and {kinematic_cut}').shape[0]/df_t.query(f'{theta_cut} and {kinematic_cut}').shape[0])
    print('theta only:',df.query(f'{theta_cut}').shape[0]/df_t.query(f'{theta_cut}').shape[0])
    print('nothing:',df.shape[0]/df_t.shape[0])
    #plt.hist(df['B_s0_M'],bins=50)
    # plt.hist(df_t['gamma_THETA'],bins=50,histtype='step',color='blue',density=True)
    # plt.hist(df['gamma_THETA'],bins=50,density=True,color='red')
    fig,ax = plt.subplots(2,2)
    ax = ax.flatten()
    ax[0].hist(df_t['gamma_PT'],bins=50,histtype='step',color='blue',density=True,range=(0,30e3))
    ax[0].hist(df['gamma_PT'],bins=50,density=True,color='red',range=(0,30e3))

    ax[1].hist(df_t['gamma_THETA'],bins=50,histtype='step',color='blue',density=True)
    ax[1].hist(df['gamma_THETA'],bins=50,density=True,color='red')

    ax[2].hist(df_t['gamma_TRUEP_E'],bins=50,histtype='step',color='blue',density=True,range=(0,500e3))
    ax[2].hist(df['gamma_P'],bins=50,density=True,color='red',range=(0,500e3))

    ax[3].hist(df_t['gamma_PHI'],bins=50,histtype='step',color='blue',density=True)
    ax[3].hist(df['gamma_PHI'],bins=50,density=True,color='red')

    
    plt.tight_layout()
    plt.show()
    