import zfit
import numpy as np
import tensorflow as tf
####IMPORT DATA
import uproot3 as uproot
import numpy as np
#from ROOT import *
from helpers import get_ALPsdf, sim10_masses_map
from plot_helpers import *
from energy_helpers import smear
from fit_helpers import create_double_cb, create_cb, create_gauss
import pandas as pd
def smear_photons(df,b=0.0335):
    df['gamma_smearP'] = smear(df['gamma_TRUEP_E'],b=b)
    df['gamma0_smearP'] = smear(df['gamma0_TRUEP_E'],b=b)
    return df
def smear_mass(df,b=0.0335):
    df = smear_photons(df,b=b)
    df.eval('cosgamma = (gamma_PX*gamma0_PX +gamma0_PZ*gamma_PZ + gamma_PY*gamma0_PY)/gamma_P/gamma0_P',inplace=True)
    df.eval("B_s0_M = sqrt(2*gamma_smearP*gamma0_smearP*(1-cosgamma))",inplace=True)
    return df


def plot_widths(m,sigma,errors):
    m,sigma = np.array(m),np.array(sigma)
    #m = np.array([1000.*sim10_masses_map[key] for key in range(40,51)])
    
    #sigma = np.array([120.8,146.9,172,196.9,228,234.5,255.8,305,357,422,436])
    errors = [1 for s in sigma]
    fig,ax = plt.subplots(1,1)
    ax.scatter(m,sigma,marker='o')
    pol=np.polyfit(m,sigma,deg=1,w=[1./x for x in errors])
    
    a,b=pol
    #a,_,_,_ = np.linalg.lstsq(m[:,np.newaxis],sigma)
    #b=0
    x_pol=np.linspace(0,21000,100)
    y_pol0=x_pol*a+b
    y_pol =x_pol*pol[0] + pol[1]
    ax.plot(x_pol,y_pol0,label=r"$\sigma = {0:.4f}m$".format(float(a),b))
    ax.plot(x_pol,y_pol,label=r"$\sigma = {0:.4f}m + {1:.2f}$".format(pol[0],pol[1]))
    plt.legend()
    ax.set_xlabel(r'$M(\gamma\gamma)$[MeV]',horizontalalignment='right', x=1.0,fontsize=13)
    ax.set_ylabel(r'$\sigma(M)$[MeV]', horizontalalignment='left',fontsize=13)
    plt.savefig('./sigma_regression.pdf')
    #plt.show()

def res_fit(df):
    import zfit
    range = (-0.15,0.15)
    obs = zfit.Space('reso',range)
    BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.03,0.01,0.1),nevs=len(df)+100,extended=True)
    model_Bs=BsCBExtended
    df.eval('gamma_resolution = (-gamma_P + gamma_TRUEP_E)/gamma_TRUEP_E',inplace=True)
    df.query(f'gamma_resolution > {range[0]} and gamma_resolution < {range[1]}')

    data = zfit.Data.from_numpy(array=df['gamma_resolution'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =BsCBExtended,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,maxiter=7000000)
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['gamma_resolution'],
                        full_model=BsCBExtended,
                        components=[BsCBExtended],
                        yields=[nSig],
                        labels=[r'$B_s$ signal double Crystal Ball'],
                        colors=['blue'],
                        nbins=30,
                        myrange=range,
                        xlabel=r'Resolution',
                        savefile=f'./resolution-phig.pdf',
                        plot=False)
def fit_my_alp(key,name_prefix,plot=False,left=True,sample=1000e4):
    root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    file = root + f'/491000{key}_1000-1099_Sim10a-priv.root'
    file = f'/scratch47/adrian.casais/ntuples/signal/sim10/new/491000{key}_1000-1099_Sim10a-priv-v44r11p1.root'
    bs=False
    if key == 'sim10' or key == 'sim09':
        file = fr'/scratch47/adrian.casais/ntuples/signal/b2gg-{key}.root'
        bs=True
    cut='(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS) and (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_CaloHypo_Saturation==0 and gamma0_CaloHypo_Saturation==0 and gamma_CL >0.95 and gamma0_CL >0.95'

    df = get_ALPsdf(file,sample=sample,background=True,
    extracut=cut,
    bs=bs,
    stripping=True)
    #res_fit(df)
    ranges = {
            'sim09':(4950,5860),
            'sim10':(4970,5990),
            40:(4800,5450),
            41:(5600,6550),
            #41:(5000,7550),
            42:(6500,7600),
            #42:(6554, 7445),
            43:(7450,8700),
            44:(8450,9700),
            45:(9400,10800),
            46:(10400,11920),
            47:(12390,14000),
            48:(13850,16350),
            49:(15650,18550),
            50:(18400,20400),
            51:(19495,20400)
            }
    for mykey in sim10_masses_map:
        center_mass = sim10_masses_map[mykey]*1010.
        sigma = center_mass*3./100./np.sqrt(2)
        mymin = 4800
        mymax = 22500
        nsigma = 6
        if center_mass-nsigma*sigma > mymin:
            mymin = int(center_mass-nsigma*sigma)
        if center_mass + 4*sigma < mymax:
            mymax = int(center_mass + nsigma*sigma)
        ranges[mykey]=(mymin,mymax)
    double = {
            #40:False,
            41:True,
            42:True,
            43:True,
            44:True,
            45:True,
            46:True,
            47:True,
            48:True,
            49:True,
            50:False,
            #51:False,
            'sim09':True,
            'sim10':True}

    left = {40:False,50:True,51:True}
    # for mykey in sim10_masses_map:
    #     double[mykey]=False
    #     left[mykey]=True

    sigmas = {50:(450,400,600)}
    nl = {50:(2,2,150)}

    m1g,m2g = ranges[key]
    print(m1g,m2g)
    BsMassRange = zfit.Space('B_M',(m1g,m2g))
    df.query(f'B_s0_M >= {m1g} and B_s0_M <= {m2g}',inplace=True)
    df.dropna(inplace=True)
    #############
    #df =smear_mass(df,b=np.sqrt(2)*0.0216)
    #df =smear_mass(df,b=0.0335) 
    ############
    obs = BsMassRange

    #Signal: Double Crystall Ball
    
    sigma = (200,100,600)
    if key in sigmas:
        sigma =sigmas[key]
    elif double[key]:
        BsCBExtended,BsParameters = create_double_cb(name_prefix=name_prefix,
                                                     mass_range=BsMassRange,
                                                     mass =sim10_masses_map[key]*1000,
                                                     sigma=sigma,
                                                     nevs=len(df)+100,
                                                     nl=1.77,
                                                     al=1.98,
                                                     ar=1.82,
                                                     nr=5.2
                                                     )
    else:
        BsCBExtended,BsParameters = create_cb(name_prefix=name_prefix,
                                              mass_range=BsMassRange,
                                              mass =sim10_masses_map[key]*1000,
                                              sigma=sigma,
                                              nevs=len(df)+100,
                                              left=left[key])
        BsParameters['dSigma'] = np.nan
        BsParameters['a_r']=np.nan
        BsParameters['n_r']=np.nan

    nBkg = zfit.Parameter(name_prefix+'nBkgComb',len(df)/2,0,len(df))
     
    #Background: exponential
    lambda_Bs = zfit.Parameter(name_prefix+'lambda',-3e-3,-10,1)
    BsBkgComb = zfit.pdf.Exponential(lambda_Bs,BsMassRange)
    firstOrder = zfit.Parameter(name_prefix+'LegFirst',0,-20,0,floating=True)
    BsBkgComb = zfit.pdf.Legendre(BsMassRange,[firstOrder]) 
    BsBkgCombExtended = BsBkgComb.create_extended(nBkg)

    #Extended
    model_Bs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
    #model_Bs=BsCBExtended


    data = zfit.Data.from_numpy(array=df['B_s0_M'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =model_Bs,data=data)
    #nll = zfit.loss.UnbinnedNLL(model =BsCB,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-4,mode=1,gradient=True,
    maxiter=7000000
    )
    #minimizer = zfit.minimize.BFGS()
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    reload(plot_helpers)
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['B_s0_M'],
                        full_model=model_Bs,
                        components=[BsCBExtended,
                        #BsBkgCombExtended
                        ],
                        yields=[nSig,
                        #nBkg
                        ],
                        labels=[r'$B_s$ signal double Crystal Ball',
                        #'Background'
                        ],
                        colors=['blue',
                        #'red'
                        ],
                        nbins=30,
                        myrange=(m1g,m2g),
                        xlabel=r'M($\gamma\gamma$)',
                        savefile=f'./alp_{key}_fit.pdf',
                        plot=plot)
    
    from helpers import pack_eff
    hessians = result.hesse()
    params = [BsParameters['scale_m'],
              BsParameters['sigma'],
              BsParameters['dSigma'],
              BsParameters['a_l'],
              BsParameters['a_r'],
              BsParameters['n_l'],
              BsParameters['n_r']]
    errors = []
    for param in params:
        try:
            errors.append(hessians[param]['error'])
        except:
            print('wroooooooong!')
            errors.append(0)
    params = [zfit.run(param) for param in params]
    values = [sim10_masses_map[key]]
    for val,err in zip(params,errors):
        print(val,err)
        values.append(pack_eff(val,err))
    return np.array(values),np.float64(BsParameters['sigma']),np.float64(hessians[BsParameters['sigma']]['error'])

dflatex = pd.DataFrame({ r'Mass [\gev]':[],
            r'$m/m_\textrm{PDG}$':[],
            r'$\sigma$ [GeV]':[],
            r'$\Delta \sigma$ [GeV]':[],
            r'$\alpha_L$':[],
            r'$\alpha_R$':[],
            r'$n_L$': [],
            r'$n_R$':[]})

sigmas = []
errors = []
masses = []
bsroot = '/scratch47/adrian.casais/ntuples/signal'
bs=False
#for key in sim10_masses_map
for key in range(45,46):
#for key in [45]:
#for key in ['sim09']:
    values,sigma,error= fit_my_alp(key,f"ALP_{key}_",
                                    plot=False,
                                    left=True,sample=int(500000e3))
    dflatex = dflatex.append(pd.DataFrame(values.reshape(1,-1),columns=list(dflatex)),ignore_index=True)
    masses.append(sim10_masses_map[key]*1000.)
    sigmas.append(sigma)
    errors.append(error)
print(dflatex.to_latex(index=False,escape=False))
#plot_widths(masses,sigmas,errors)P