import uproot3 as uproot
import matplotlib.pyplot as plt
import pandas as pd
rootalps = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/'
rootalps = '/scratch47/adrian.casais/ntuples/signal/sim10/'
rootbkg = '/scratch47/adrian.casais/ntuples/background/'


rs = [
    '1_0',
    '1_35',
    '1_7',
    ]

rs_1 =[
       '1.00',
       '1.35',
       '1.70']


_var_list = [
            #'B_s0_CONEANGLE_{}',
            'B_s0_CONEMULT_{}',
            'B_s0_CONEP_{}',
            'B_s0_CONEPASYM_{}',
            'B_s0_CONEPT_{}',
            'B_s0_CONEPTASYM_{}']
_var_list = [
            #'B_s0_CONEANGLE_{}',
            'B_B_CONEMULT_{}',
            'B_B_CONEP_{}',
            'B_B_CONEPASYM_{}',
            'B_B_CONEPT_{}',
            'B_B_CONEPTASYM_{}']
_var_list_1 = [
              'B_s0_{}_cc_mult',
              'B_s0_{}_cc_vPT',
              'B_s0_{}_cc_PZ',
              'B_s0_{}_cc_asy_P',
              'B_s0_{}_cc_asy_PT'
    
    ]
all_cone_vars=[]
all_cone_vars_1=[]

trigger_vars = ['B_L0ElectronDecision_TOS','B_L0PhotonDecision_TOS','B_Hlt1B2GammaGammaDecision_TOS','B_Hlt1B2GammaGammaHighMassDecision_TOS','B_Hlt2RadiativeB2GammaGammaDecision_TOS'
                ]
trigger_vars_1 = ['B_s0_L0ElectronDecision_TOS','B_s0_L0PhotonDecision_TOS','B_s0_Hlt1B2GammaGammaDecision_TOS','B_s0_Hlt1B2GammaGammaHighMassDecision_TOS','B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'
                ]
for r in rs:
    all_cone_vars+= [i.format(r) for i in _var_list]
for r in rs_1:
    all_cone_vars_1 += [i.format(r) for i in _var_list_1]


trigger_cond = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS'
trigger_cond_1  = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'

#trigger_cond = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS)'
#f_s = uproot.open(root + '/ALPs-5GeV-withcones.root')
#f_s = uproot.open(root+'/49100040_MagDown.root')
f_s = uproot.open(rootalps+'49100040_1000-1099_Sim10a-priv.root')
#f_s = uproot.open(root + '/13100212.root')
#f_b = uproot.open(rootalps + '/hardPhoton-withcones.root')
f_b = uproot.open(rootbkg + 'Stripping34.root')


t_s = f_s['DTTBs2GammaGamma/DecayTree']
t_b = f_b['DecayTree/DecayTree']

signal_df_dummy = t_s.pandas.df(branches=all_cone_vars_1+trigger_vars_1)
background_df  = t_b.pandas.df(branches=all_cone_vars+trigger_vars)

signal_df_dummy = signal_df_dummy.query(trigger_cond_1)
background_df = background_df.query(trigger_cond)
#import xgboost as xgb
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import xgboost as xgb

signal_df = pd.DataFrame()

for i,j in zip(rs_1,rs):
    signal_df['B_B_CONEMULT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_mult'.format(i)]
    signal_df['B_B_CONEP_{}'.format(j)] = np.sqrt(np.array(signal_df_dummy['B_s0_{}_cc_vPT'.format(i)])**2 + np.array(signal_df_dummy['B_s0_{}_cc_PZ'.format(i)])**2)
    signal_df['B_B_CONEPASYM_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_asy_P'.format(i)]
    signal_df['B_B_CONEPT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_vPT'.format(i)]
    signal_df['B_B_CONEPTASYM_{}'.format(j)]= signal_df_dummy['B_s0_{}_cc_asy_PT'.format(i)]
signal_df["category"]= 1 #Use 1 for signal
background_df["category"] = 0 #use 0 for bkg
training_data = pd.concat([signal_df,background_df[all_cone_vars+['category']]],copy=True,ignore_index=True,sort=False)
training_data=training_data.sample(frac=1).reset_index(drop=True)
training_data = training_data.dropna()

plt.hist(signal_df['B_B_CONEPTASYM_1_7'],bins=50,density=True,label='signal')
plt.hist(background_df['B_B_CONEPTASYM_1_7'],bins=50,density=True,label='background')
plt.legend()
plt.show()

types = [
    'xgb',
    #'sk-GBC',
    'sk-BDT',
    'sk-MLP',
    'sk-ADA'
    ]

plot = False
if plot:
    fig = plt.figure()
    j = 1
    for var in all_cone_vars:
        ax = fig.add_subplot(2,3,j)
        ax.set_title(var)
        ax.hist(signal_df[var],label='signal',bins=30)
        ax.hist(background_df[var],label='background',bins=50)
        j+=1

    plt.legend()
    plt.show()

#r = 'all'
indices = np.arange(0,len(training_data))
j = 1
#fig = plt.figure()
proba_sig_train = {}
proba_sig_test = {}
proba_bkg_train = {}
proba_bkg_test = {}
proba_test = {}
proba_train = {}
x,y,x_test,y_test = {},{},{},{}
for type in types:
        #training_data['proba-{}'.format(type)] = np.zeros(shape=(len(training_data),1))
        var_list = all_cone_vars
        mask = np.random.rand(len(training_data)) < 0.8
        _train_data = training_data.iloc[mask]
        _test_data = training_data.iloc[~mask]
        x[type],y[type] = _train_data[var_list],_train_data["category"]
        x_test[type],y_test[type] = _test_data[var_list],_test_data['category']
        if type =='xgb':
            
            bdt = xgb.XGBClassifier(
                eta=0.7,
                objective='binary:logistic',
                #eval
                )#n_estimators=1000,learning_rate=1)
        if type == 'sk-MLP':
                
            bdt = MLPClassifier(
                activation='relu',
                solver='adam',
                alpha=2e-05,
                epsilon=1e-08,
                max_iter = 500,
                tol = 0.0001,
                max_fun = 20000
                )
            scaler = StandardScaler()
            scaler.fit(x[type])
            x[type] = scaler.transform(x[type])
            x_test[type] = scaler.transform(x_test[type])
        if type== 'sk-GBC':
                
            bdt = GradientBoostingClassifier(
                loss='deviance',
                learning_rate=0.1,
                min_samples_leaf=0.1,
                n_estimators=100,
                #subsample=0.5,
                criterion='mae',
                #min_samples_split=0.2
                    )
        if type=='sk-BDT':
            bdt = DecisionTreeClassifier(max_depth=5)

        if type == 'sk-ADA':
            bdt = AdaBoostClassifier(n_estimators=100)
                
        bdt.fit(x[type],y[type])
        proba_sig_test[type] = bdt.predict_proba(x_test[type][np.array(y_test[type],dtype='bool')])[:,1]
        proba_sig_train[type] = bdt.predict_proba(x[type][np.array(y[type],dtype='bool')])[:,1]
        proba_bkg_test[type] = bdt.predict_proba(x_test[type][~np.array(y_test[type],dtype='bool')])[:,1]
        proba_bkg_train[type] = bdt.predict_proba(x[type][~np.array(y[type],dtype='bool')])[:,1]

        proba_test[type] = bdt.predict_proba(x_test[type])[:,1]
        proba_train[type] = bdt.predict_proba(x[type])[:,1]
        #ax = fig.add_subplot(2,3,j)
        j+=1
        n_bins = 10
        marker='o'
        range = (0,1)
        alpha = 0.4
        
        proba_sig_test_hist, bins = np.histogram(proba_sig_test[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_sig_test_hist,marker=marker,linestyle='None',label='Signal test proba',alpha = alpha)
        
        proba_sig_train_hist,bins=np.histogram(proba_sig_train[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_sig_train_hist,marker=marker,linestyle='None',label='Signal train proba',alpha=alpha)
        
        proba_bkg_test_hist,bins=np.histogram(proba_bkg_test[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_bkg_test_hist,marker=marker,linestyle='None',label='Background test proba',alpha=alpha)
        
        proba_bkg_train_hist,bins = np.histogram(proba_bkg_train[type],bins=n_bins,density=True,range=range)
        bins = (bins[1:] + bins[:-1])/2
        plt.plot(bins,proba_bkg_train_hist,marker=marker,linestyle='None',label='Background train proba',color='black',alpha=alpha)

plt.legend(loc='center')
        
plt.show()

from sklearn.metrics import roc_curve,auc
from copy import copy
fprs,tprs = [],[]
for type in types:
        fpr,tpr,thresholds = roc_curve(y_test[type],proba_test[type])
        fprs.append(copy(fpr));tprs.append(copy(tpr))
        plt.plot(fprs[-1],tprs[-1],label = f'{type}')

plt.legend()
plt.show()        
