import uproot3 as uproot
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib as mpl
import numpy as np
import pandas as pd
import pickle 
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from catboost import CatBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
import xgboost as xgb

from sklearn.metrics import roc_curve,auc
from sklearn.model_selection import cross_val_score
from helpers import sim10_masses_map,nearest_rect
import plot_helpers

from bdt_helpers import process_variables, get_cuts
rootalps = '/scratch47/adrian.casais/ntuples/signal/sim10/'

name_map = {
    'B_B_CONEMULT_1_0':r'$\gamma\gamma$ Cone$_{1.0}$ Mult.',
    'B_B_CONEP_1_0':r'$\gamma\gamma$ Cone$_{1.0}$ $p$',
    'B_B_CONEPT_1_0':r'$\gamma\gamma$ Cone$_{1.0}$ $p$',
    'B_B_CONEPASYM_1_0':r'$\gamma\gamma$ Cone$_{1.0}$ $p$ Asym.',
    'B_B_CONEPTASYM_1_0':r'$\gamma\gamma$ Cone$_{1.0}$ $p_T$ Asym.',

    'B_B_CONEMULT_1_35':r'$\gamma\gamma$ Cone$_{1.35}$ Mult.',
    'B_B_CONEP_1_35':r'$\gamma\gamma$ Cone$_{1.35}$ $p$',
    'B_B_CONEPT_1_35':r'$\gamma\gamma$ Cone$_{1.35}$ $p_T$',
    'B_B_CONEPASYM_1_35':r'$\gamma\gamma$ Cone$_{1.35}$ $p$ Asym.',
    'B_B_CONEPTASYM_1_35':r'$\gamma\gamma$ Cone$_{1.35}$ $p_T$ Asym.',

    'B_B_CONEMULT_1_7':r'$\gamma\gamma$ Cone$_{1.7}$ Mult.',
    'B_B_CONEP_1_7':r'$\gamma\gamma$ Cone$_{1.7}$ $p$',
    'B_B_CONEPT_1_7':r'$\gamma\gamma$ Cone$_{1.7}$ $p_T$',
    'B_B_CONEPASYM_1_7':r'$\gamma\gamma$ Cone$_{1.7}$ $p$ Asym.',
    'B_B_CONEPTASYM_1_7':r'$\gamma\gamma$ Cone$_{1.7}$ $p_T$ Asym.',
}

def signal_df(masses,vars,cut,sample=10.e4):
    root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    df = pd.DataFrame()
    for i in masses:
        if i=='gg':
            continue
        f_s = uproot.open(root + f'491000{i}_1000-1099_Sim10a-priv.root')
        t_s = f_s['DTTBs2GammaGamma/DecayTree']
        df_temp = t_s.pandas.df(branches=vars,entrystop=sample)
        df_temp.query(cut,inplace=True)
        if df.shape == (0,0):
            df = df_temp
        else:
            df= pd.concat([df,df_temp])
    return df

def background_df(vars,cut,sample=10.e4):
    rootbkg = '/scratch47/adrian.casais/ntuples/background/'
    f_b = uproot.open(rootbkg + 'Stripping34-2022.root')
    t_b = f_b['DecayTree/DecayTree']
    df = t_b.pandas.df(branches=vars,entrystop=sample)
    df.query(cut,inplace=True)

    return df 

def relabel_signal(signal_df_dummy,rs_1,rs):
    signal_df = pd.DataFrame()

    for i,j in zip(rs_1,rs):
        signal_df['B_B_CONEMULT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_mult'.format(i)]
        signal_df['B_B_CONEP_{}'.format(j)] = np.sqrt(np.array(signal_df_dummy['B_s0_{}_cc_vPT'.format(i)])**2 + np.array(signal_df_dummy['B_s0_{}_cc_PZ'.format(i)])**2)
        signal_df['B_B_CONEPASYM_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_asy_P'.format(i)]
        signal_df['B_B_CONEPT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_vPT'.format(i)]
        signal_df['B_B_CONEPTASYM_{}'.format(j)]= signal_df_dummy['B_s0_{}_cc_asy_PT'.format(i)]
    signal_df['gamma_PP_IsPhoton'] = signal_df_dummy['gamma_PP_IsPhoton']
    signal_df['gamma0_PP_IsPhoton'] = signal_df_dummy['gamma0_PP_IsPhoton']
    signal_df['B_M']=signal_df_dummy['B_s0_M']
    return signal_df

def get_training_data(signal_df,background_df,all_cone_vars):
    signal_df["category"]= 1 #Use 1 for signal
    background_df["category"] = 0 #use 0 for bkg
    training_data = pd.concat([signal_df[all_cone_vars+['category']],background_df[all_cone_vars+['category']]],copy=True,ignore_index=True,sort=False)
    training_data=training_data.sample(frac=1).reset_index(drop=True)
    training_data = training_data.dropna()

    return training_data

def separate_train_test(training_data,var_list,f_train=0.8):
    mask = np.random.rand(len(training_data)) < f_train
    _train_data = training_data.iloc[mask]
    _test_data = training_data.iloc[~mask]
    x,y = _train_data[var_list],_train_data["category"]
    x_test,y_test = _test_data[var_list],_test_data['category']

    return x,x_test,y,y_test



def train_classifier(x,x_test,y,bdt= xgb.XGBClassifier(eta=0.7,objective='binary:logistic')):
    # bdt = MLPClassifier(activation='relu',
    #                     solver='adam',
    #                     alpha=2e-05,
    #                     epsilon=1e-08,
    #                     max_iter = 500,
    #                     tol = 0.0001,
    #                     max_fun = 20000)
    # scaler = StandardScaler()
    # scaler.fit(x)
    # x = scaler.transform(x)
    # x_test = scaler.transform(x_test)

    bdt = AdaBoostClassifier(n_estimators=100, random_state=0,learning_rate=0.1)
    bdt = CatBoostClassifier()
    
    bdt.fit(x,y)

    return bdt,x,x_test
def make_roc_curve(y_test,x_test,bdt,name='roc.pdf'):
    fig,ax = plt.subplots(1,1)
    mytype = 'xgb'
    proba_test = bdt.predict_proba(x_test)[:,1]
    fpr,tpr,_ = roc_curve(y_test,proba_test)
    area = auc(fpr,tpr)
    print(f'AUC: {area}')
    ax.plot(fpr,tpr,label = f'{mytype}')
    ax.set_xlabel(r'1 - Bkg rejection')
    ax.set_ylabel(r'Signal efficiency')
    ax.legend()
    fig.savefig(name)

def overtraining_test(bdt,x,x_test,y,y_test,name="overtraining.pdf"):
    fig,ax = plt.subplots(1,1)
    sig_test = bdt.predict_proba(x_test[np.array(y_test,dtype='bool')])[:,1]
    sig_train = bdt.predict_proba(x[np.array(y,dtype='bool')])[:,1]
    bkg_test = bdt.predict_proba(x_test[~np.array(y_test,dtype='bool')])[:,1]
    bkg_train = bdt.predict_proba(x[~np.array(y,dtype='bool')])[:,1]

    ax.hist(sig_test,bins=50,color='red',density=True,label='Signal test',alpha=0.7)
    ax.hist(sig_train,bins=50,color='red',histtype='step',density=True,label='Signal train')

    ax.hist(bkg_test,bins=50,density=True,label='Background test',alpha=0.7,color='C0')
    ax.hist(bkg_train,bins=50,histtype='step',density=True,label='Background signal',alpha=0.7,color='C0')
    ax.set_xlabel(r'Classifier output')
    ax.set_ylabel('A.U.')
    ax.legend()
    fig.savefig(name)

def punzi_figure(df_signal,df_background,range,label='punzi'):
    bdt = np.linspace(0,1,100)
    df_signal = df_signal.query(f'B_M>{range[0]} and B_M<{range[1]}')
    df_background = df_background.query(f'B_M>{range[0]} and B_M<{range[1]}')
    punzi = []
    for cut in bdt:
        try:
            epsilon=df_signal.query(f'bdt>{cut}').shape[0]/df_signal.shape[0]
        except:
            epsilon =0 
        B = 25*df_background.query(f'bdt>{cut}').shape[0] 
        p = epsilon/(3.5 + np.sqrt(B))
        punzi.append(p)
    # print(punzi)
    xmax = bdt[np.argmax(punzi)]
    ymax = np.max(punzi)
    #xmax = 0.95
    print(f"bdt that maximizes punzi: {xmax}")
    print(f"max punzi value: {ymax}")
    bkg_rej = 1-df_background.query(f'bdt> {xmax}').shape[0]/df_background.shape[0]
    signal_eff = df_signal.query(f'bdt> {xmax}').shape[0]/df_signal.shape[0]
    print(f"background rejection: bdt>{xmax} {bkg_rej}. signal eff: {signal_eff}")
    plt.plot(bdt,punzi/sum(punzi),label=label)
    #plt.show()


def punzi_figure_2D(df_signal,df_background,range,label='punzi'):
    bdt = np.linspace(0.8,1,21)
    isphoton = np.linspace(0.5,1,51)
    df_signal = df_signal.query(f'B_M>{range[0]} and B_M<{range[1]}')
    df_background = df_background.query(f'B_M>{range[0]} and B_M<{range[1]}')
    punzi = []
    for cut in bdt:
        punzi.append([])
        for pidcut in isphoton:
            try:
                epsilon=df_signal.query(f'bdt>{cut} & gamma_PP_IsPhoton>{pidcut} & gamma0_PP_IsPhoton>{pidcut}').shape[0]/df_signal.shape[0]
            except:
                epsilon=0 
            B = 25*df_background.query(f'bdt>{cut} & Gamma1_PP_IsPhoton>{pidcut} & Gamma2_PP_IsPhoton>{pidcut}').shape[0] 
            p = epsilon/(3.5 + np.sqrt(B))
            punzi[-1].append(p)
    xmax = bdt[int(np.argmax(punzi)/len(isphoton))]
    ymax = isphoton[int(np.argmax(punzi)%len(isphoton))]
    zmax = np.max(punzi)
    #xmax = 0.95
    print(f"bdt that maximizes punzi: {xmax}")
    print(f"isphoton that maximises punzi: {ymax}")
    print(f"max punzi value: {zmax}")
    bkg_rej = 1 - df_background.query(f'bdt > {xmax} & Gamma1_PP_IsPhoton > {ymax} & Gamma2_PP_IsPhoton > {ymax}').shape[0]/df_background.shape[0]
    signal_eff = df_signal.query(f'bdt>{cut} & gamma_PP_IsPhoton>{pidcut} & gamma0_PP_IsPhoton>{pidcut}').shape[0]/df_signal.shape[0]
    print(f"background rejection: bdt >{xmax} isphoton>{ymax} {bkg_rej}. signal eff: {signal_eff}")
    X,Y = np.meshgrid(bdt,isphoton)
    im = plt.pcolormesh(X,Y,np.transpose(punzi))
    cbar = plt.colorbar(im)
    plt.xlabel("BDT")
    plt.ylabel("IsPhoton")
    cbar.set_label("Punzi FoM")

    plt.savefig(f"2D_optimisation_{label}.pdf")
    plt.savefig(f"2D_optimisation_{label}.png")
    plt.clf()

    #plt.show()



def print_vars(vars_b):
    print('vars = [')
    for var in vars_b:
        print (f'"{var}",')
    print(']')


def plot_variables(signal_df,background_df,vars_b):
    i,j = nearest_rect(len(vars_b))
    if j>i:
        i,j=j,i
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(i,j,figsize=(9,12))
    ax = ax.flatten()

    # for r in ['1_7','1_35','1_0']:
    #     background_df = background_df.query(f"B_B_CONEPT_{r}< 50000")
    #     background_df = background_df.query(f"B_B_CONEP_{r}< 500000")

    #     signal_df = signal_df.query(f"B_B_CONEPT_{r}< 50000")
    #     signal_df = signal_df.query(f"B_B_CONEP_{r}< 500000")
    
    for a,var in zip(ax,vars_b):

        a.set_xlabel(name_map[var])
        a.set_ylabel('A.U.')
        myrange = None
        if 'MULT' in var:
            myrange = (0,50)
        if 'CONEP' in var and 'ASYM' not in var:
            myrange = (0,500.e3)
        if 'CONEPT' in var and 'ASYM' not in var:
            myrange = (0,70.e3)
        a.hist(signal_df[var],bins=50,label='Signal',alpha=0.7,density=True,range=myrange)
        a.hist(background_df[var],bins=50,label='Background',color='red',histtype='step',density=True,range=myrange)
        
    ax[0].legend()
    plt.tight_layout()
    fig.savefig("variables.pdf")

def mass_correlation(df_,range_mass = [4800,6300],extra='below'):
    fig,ax = plt.subplots(1,1)
    ax.set_yscale('log')
    ax.hist2d(df_['B_M'],df_['bdt'],bins=[50,10],range=[range_mass,[0,1.0]],norm=mpl.colors.LogNorm())
    ax.set_xlabel(r"$\gamma\gamma(M)$")
    ax.set_ylabel(r"MVA response")
    fig.savefig(f"mass-correlation-{extra}.pdf")
    plt.clf()

def mass_correlation2(df):
    fig,ax = plt.subplots(1,1)
    for i in [0.6,0.8,0.9,0.95]:
        ax.hist(df.query(rf'bdt > {i}')['B_M'],bins=50,histtype='step',density=True,label=rf'BDT$>{i:0.2f}$')
        ax.set_xlabel(r"$\gamma\gamma(M)$")
        ax.set_ylabel('A.U.')

    ax.legend()
    fig.savefig('Mass-shape-bdt.pdf')
    plt.clf()


def mass_before_after(df):
    fig,ax = plt.subplots(1,1)
    ax.hist(df.query('bdt>0.6')['B_M'],bins=100,histtype='step',color='red',label = 'BDT ON',density=True,range=[0,0.2])
    ax.hist(df.query('bdt>0.0')['B_M'],bins=100,histtype='step',color='blue',label = 'BDT OFF',density=True,range=[0,0.2])
    ax.set_xlabel(r"$M(\gamma\gamma)$ [MeV]")
    ax.set_ylabel("A.U.")
    ax.legend()
    fig.savefig("mass-beforeafter-bdt.pdf")
    plt.clf()

def kfold(training_data,var_list,scores=False,imbalance=1.0):
    kf = KFold(n_splits=5)
    x,y = training_data[var_list],training_data["category"]
    if scores:
        bdt = xgb.XGBClassifier(
            eta=0.1,
            objective='binary:logistic',
            scale_pos_weight=imbalance,
            ) 
        scores = cross_val_score(bdt,x,y,cv=kf,scoring='roc_auc')
    print(5*"#")
    print("Cross validation scores:")
    print(scores)
    print(5*"#")
    classifiers = []
    counter = 0
    for tri,tti in kf.split(x):
        counter+=1
        x_train,y_train = x.iloc[tri],y.iloc[tri]
        x_test,y_test = x.iloc[tti],y.iloc[tti]
        bdt = xgb.XGBClassifier(
            eta=0.4,
            objective='binary:logistic',
            scale_pos_weight=imbalance,
            )
        #bdt = CatBoostClassifier(silent=True)
        classifiers.append(bdt)
        classifiers[-1].fit(x_train,y_train)
        make_roc_curve(y_test,x_test,classifiers[-1],f'roc-{counter}.pdf')
        overtraining_test(classifiers[-1],x_train,x_test,y_train,y_test,name=f"overtraining-{counter}.pdf")
        # x_signal = x_test[np.array(y_test,dtype='bool')]
        # x_bkg = x_test[~np.array(y_test,dtype='bool')]
        # punzi_figure(x_signal,x_background,[6000-3*150,6000+3*150])
        # punzi_figure(x_signal,x_background,[17000 -3*400, 17000 + 3*400])
        # plt.savefig(f"punzi-{counter}.pdf")
    return classifiers
def plot_corr_matrix(df):
    df_copy = df.copy()
    df_copy.rename(columns=name_map,inplace=True)
    corr_matrix = df_copy.corr()
    fig,ax = plt.subplots(1,1,figsize=(10,10))
    sn.heatmap(corr_matrix, annot=True,ax=ax)
    plt.tight_layout()
    fig.savefig('corr_matrix.pdf')

if __name__ == '__main__':
    train_bdt = True
    vars_b,trigger_vars_b,vars_s,trigger_vars_s,rs,rs_1 = process_variables()
    print_vars(vars_b)
    cut_b,cut_s = get_cuts()
    df_background = background_df(vars_b+trigger_vars_b,cut_b,sample=np.inf)
    print(f"no entries df_background {df_background.shape[0]}")
    del sim10_masses_map['pi0pi0']
    del sim10_masses_map['gg']
    df_signal = signal_df(
                            masses = sim10_masses_map,
                            #masses = [40],
                          vars = vars_s+trigger_vars_s+['gamma_MC_MOTHER_ID','gamma0_MC_MOTHER_ID','gamma_TRUEID','gamma0_TRUEID','B_s0_TRUEID'], cut = cut_s+" and (abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54",sample=np.inf)
    
    df_signal = relabel_signal(df_signal,rs_1,rs)
    min_len = min(df_signal.shape[0],df_background.shape[0])
    max_len = max(df_signal.shape[0],df_background.shape[0])
    #df_signal_train = df_signal.sample(n=min_len)
    #df_background_train = df_background.sample(n=min_len)
    #msk = np.random.rand(len(df_background)) < 0.5
    #df_background_train = df_background[msk]
    #df_background = df_background[~msk]

    df_signal_train = df_signal
    df_background_train = df_background
    
    plot_variables(df_signal,df_background,vars_b)
    plot_corr_matrix(df_signal[vars_b])
    print("plottt")
    plt.clf()
    if train_bdt:
        training_data=get_training_data(df_signal_train,df_background_train,vars_b)
        #x,x_test,y,y_test = separate_train_test(training_data,vars_b)
        bdts = kfold(training_data,vars_b,imbalance = max_len/min_len,scores=True)
        with open('bdt.pickle','wb') as handle:
            pickle.dump(bdts,handle)
    else:
        with open("bdt.pickle", "rb") as handle:
            bdts = pickle.load(handle)
    print(vars_b)
    bdt_bkg = np.zeros(len(df_background))
    bdt_sig =np.zeros(len(df_signal)) 
    for i in range(len(bdts)):
        bdt_bkg += bdts[i].predict_proba( df_background[vars_b])[:,1]/len(bdts)
        bdt_sig += bdts[i].predict_proba( df_signal[vars_b])[:,1]/len(bdts)
    df_background['bdt']=bdt_bkg
    df_signal['bdt']=bdt_sig
    mass_correlation2(df_background)
    # df_ = df_background.query("B_M<11000")
    # mass_correlation(df_,range_mass=[4800,11000],extra='below')
    # df_ = df_background.query("B_M>11000")
    # mass_correlation(df_,range_mass=[11000,20000],extra='above')
    # mass_before_after(df_background)
    # plt.clf()
    plt.hist(df_background.query('bdt>0.9')['B_M'],bins=100,range=[4800,20000],histtype='step')
    plt.savefig("mass_spectrum_bdt.pdf")
    plt.clf()
    ####
    fig,ax = plt.subplots(1,1)
    # print(df_signal.columns)
    # print(df_background.columns)
    punzi_figure(df_signal,df_background,[6000  -3*150,6000   +3*150],label='6 GeV')
    punzi_figure(df_signal,df_background,[10000 -3*300,10000  +3*300],label='10 GeV')
    punzi_figure(df_signal,df_background,[17000 -3*400, 17000 +3*400],label='17 GeV')
    plt.legend()
    plt.savefig("punzi.pdf")
    plt.clf()
    punzi_figure_2D(df_signal,df_background,[6000  -3*150,6000  +3*150],label='6GeV')
    punzi_figure_2D(df_signal,df_background,[10000 -3*300,10000 +3*300],label='10GeV')
    punzi_figure_2D(df_signal,df_background,[17000 -3*400,17000 +3*400],label='17GeV')


    