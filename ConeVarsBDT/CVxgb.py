import uproot
import matplotlib.pyplot as plt
import pandas as pd
root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'



rs = [
    '1_35',
    '1_7',
    '1_0'
    ]

rs_1 =['1.00','1.35','1.70']

_var_list = [
            #'B_s0_CONEANGLE_{}',
            'B_s0_CONEMULT_{}',
            'B_s0_CONEP_{}',
            'B_s0_CONEPASYM_{}',
            'B_s0_CONEPT_{}',
            'B_s0_CONEPTASYM_{}']
_var_list_1 = [
              'B_s0_{}_cc_mult',
              'B_s0_{}_cc_vPT',
              'B_s0_{}_cc_PZ',
              'B_s0_{}_cc_asy_P',
              'B_s0_{}_cc_asy_PT'
    
    ]
all_cone_vars=[]
all_cone_vars_1=[]

for r in rs:
    all_cone_vars+= [i.format(r) for i in _var_list]
for r in rs_1:
    all_cone_vars_1 += [i.format(r) for i in _var_list_1]



#f_s = uproot.open(root + '/ALPs-5GeV-withcones.root')
f_s = uproot.open(root+'/49100040_MagDown.root')
#f_s = uproot.open(root + '/13100212.root')
f_b = uproot.open(root + '/hardPhoton-withcones.root')

t_s = f_s['B2GammaGammaTuple/DecayTree']
t_b = f_b['B2GammaGammaTuple/DecayTree']

signal_df_dummy = t_s.pandas.df(branches=all_cone_vars_1)
background_df  = t_b.pandas.df(branches=all_cone_vars)

import xgboost as xgb
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

signal_df = pd.DataFrame()

for i,j in zip(['1.00','1.35','1.70'],['1_0','1_35','1_7']):
    signal_df['B_s0_CONEMULT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_mult'.format(i)]
    signal_df['B_s0_CONEP_{}'.format(j)] = np.sqrt(signal_df_dummy['B_s0_{}_cc_vPT'.format(i)].array**2 + signal_df_dummy['B_s0_{}_cc_PZ'.format(i)].array**2)
    signal_df['B_s0_CONEPASYM_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_asy_P'.format(i)]
    signal_df['B_s0_CONEPT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_vPT'.format(i)]
    signal_df['B_s0_CONEPTASYM_{}'.format(j)]= signal_df_dummy['B_s0_{}_cc_asy_P'.format(i)]
signal_df["category"]= 1 #Use 1 for signal
background_df["category"] = 0 #use 0 for bkg
training_data = pd.concat([signal_df,background_df],copy=True,ignore_index=True,sort=False)
training_data=training_data.sample(frac=1).reset_index(drop=True)

kf = KFold(n_splits=5)
types = [
    'xgb',

    'sk-GBC',
    'sk-BDT',
    'sk-MLP',
    'sk-ADA'

    ]

plot = False
if plot:
    fig = plt.figure()
    j = 1
    for var in all_cone_vars:
        ax = fig.add_subplot(2,3,j)
        ax.set_title(var)
        ax.hist(signal_df[var],label='signal',bins=30)
        ax.hist(background_df[var],label='background',bins=50)
        j+=1

    plt.legend()
    plt.show()

#r = 'all'
for type in types:
    for r in rs:
        training_data['proba-{}_{}'.format(type,r)] = np.zeros(shape=(len(training_data),1))
        var_list = [i.format(r) for i in _var_list]
        for train_indices, test_indices in kf.split(training_data):
            _train_data = training_data.iloc[train_indices]
            _test_data = training_data.iloc[test_indices]
            x,y = _train_data[var_list],_train_data["category"]
            x_test,y_test = _test_data[var_list],_test_data['category']
            if type =='xgb':
                
                bdt = xgb.XGBClassifier(
                    eta=0.7,
                    objective='binary:logistic',
                    #eval
                    )#n_estimators=1000,learning_rate=1)
            if type == 'sk-MLP':
                
                bdt = MLPClassifier(
                    activation='relu',
                    solver='adam',
                    alpha=2e-05,
                    epsilon=1e-08,
                    max_iter = 500,
                    tol = 0.0001,
                    max_fun = 20000
                    )
                scaler = StandardScaler()
                scaler.fit(x)
                x = scaler.transform(x)
                x_test = scaler.transform(x_test)
            if type== 'sk-GBC':
                
                bdt = GradientBoostingClassifier(
                    loss='deviance',
                    learning_rate=0.1,
                    min_samples_leaf=0.1,
                    n_estimators=100,
                    #subsample=0.5,
                    criterion='mae',
                    #min_samples_split=0.2
                    )
            if type=='sk-BDT':
                bdt = DecisionTreeClassifier(max_depth=5)

            if type == 'sk-ADA':
                bdt = AdaBoostClassifier(n_estimators=100)
                
            bdt.fit(x,y)
            training_data.iloc[test_indices,training_data.columns.get_loc('proba-{0}_{1}'.format(type,r))]=bdt.predict_proba(x_test)[:,1]
                       

sig = training_data.query('category==1')
bkg = training_data.query('category==0')





# def plot_comparison(var,mc_df,bkg_df):
#     _,bins,_ = plt.hist(mc_df[var],bins=100,histtype="step",label="Signal",density=1)
#     _,bins,_ = plt.hist(bkg_df[var],bins=bins,histtype="step",label="Background",density=1)
    
#     plt.grid()
#     plt.xlabel(var)
#     plt.xlim(bins[0],bins[-1])
#     plt.legend(loc="best")

# plot_comparison('proba',sig,bkg)
# plt.show()


from sklearn.metrics import roc_curve,auc
from copy import copy
fprs,tprs = [],[]
for type in types:
    for r in rs:
        fpr,tpr,thresholds = roc_curve(training_data['category'],training_data['proba-{0}_{1}'.format(type,r)])
        fprs.append(copy(fpr));tprs.append(copy(tpr))
        plt.plot(fprs[-1],tprs[-1],label = '{0}-r={1}'.format(type,r))
        
plt.legend(loc='best')
plt.plot([0,1],[0,1])
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.show()
#apparently MLP is what best behaves
#victor managed to do it better with GradientBoost with deafult parameters (ROC AUC: 0.95)
