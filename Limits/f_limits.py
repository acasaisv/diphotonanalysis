import csv
import numpy as np
import matplotlib.pyplot as plt
from plot_helpers import *
def set_limit(file,f_a=True,delimiter=','):
    a = []
    with open(file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=delimiter)
        if not f_a: next(spamreader)
        for row in spamreader:
            a.append(row)
    # print(a)
    
    a = np.array(a,dtype=np.float64)
    # print(a)
    if not f_a:
        a = a[:,[0,4]]
        a[:,0] = a[:,0]/1000
    a = a[a[:, 0].argsort()] # reorder!!!
    
    xsecs = []
    with open('xsections.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        #if not f_a: next(spamreader)
        for row in spamreader:
            xsecs.append(row)
    xsecs= np.array(xsecs,dtype=np.float64)
    
    xsecs = xsecs[xsecs[:, 0].argsort()] # reorder!!!
    m_theory,theory_sigma =np.swapaxes(xsecs,0,1)
    theory_sigma*=5.5e-2
    
    if f_a:
        m,f = np.swapaxes(a,0,1)
        k = np.interp(m,m_theory,theory_sigma)
        sigma = k/f**2
    else:
        m,sigma = np.swapaxes(a,0,1)
        k = np.interp(m,m_theory,theory_sigma)
        f = np.sqrt(k/sigma)
    return m,f,sigma

    

if __name__=='__main__':
    import pandas as pd
    font = {'family' : 'serif',
                #'weight' : 'bold',
        'size'   : 24}

    plt.rc('font', **font)

    #df = pd.read_csv('lhcb.csv',engine='python',delimiter=' ')
    #m_lhcb,f_lhcb,sigma_lhcb=set_limit('lhcb-new.csv',f_a=False)
    m_lhcb,f_lhcb,sigma_lhcb=set_limit('../limitcalculation/expected_limit.csv',f_a=False)
    m_lhcbold,f_lhcbold,sigma_lhcbold=set_limit('lhcb-old.csv')
    m_atlas,f_atlas,sigma_atlas=set_limit('atlas2022.csv')
    m_atlas2018,f_atlas2018,sigma_atlas2018=set_limit('atlas.csv')
    m_btoka,f_btoka,sigma_btoka=set_limit('btoka.csv')
    m_btoka,f_btoka,sigma_btoka=set_limit('babar2.csv')
    f_btoka = f_btoka*47/1000
    m_babar,f_babar,sigma_babar=set_limit('babar.csv')
    m_dijet,f_dijet,_ = set_limit('dijetlhc.csv')
    m_nonres,f_nonres,_ = set_limit('non-resonant2.csv')
    fig,ax = plt.subplots(1,1,figsize=(10,8))
    ax.set_xlabel(r"$m_a$ [GeV]")
    ax.set_ylabel(r"$f_a$ [TeV]")
    ax.set_yscale('log')
    ax.set_xlim([1,19])
    ax.set_ylim([1.e-3,1000])
    def f2g(x):
        return (10+50./3)/127./np.pi/x/1000


    g2f = f2g

    print(g2f(1.24e-4))
    secax = ax.secondary_yaxis('right', functions=(g2f,f2g))
    secax.set_ylabel(r"$g_{a\gamma\gamma}$ [$\mathrm{GeV}^{-1}$]")

    ax.fill_between(m_btoka,0,f_btoka,alpha=0.4,color='purple',label=r'BaBar $(B \to K a$)')
    ax.fill_between(m_babar,0,f_babar,alpha=0.6,color='pink',label=r'BaBar (2018)')
    ax.fill_between(m_lhcb,0,f_lhcb,alpha=0.4,color='blue',label='LHCb sensitivity')
    ax.fill_between(m_atlas,0,f_atlas,alpha=0.4,color='red',label='ATLAS')
    ax.fill_between(m_atlas2018,0,f_atlas2018,alpha=0.4,color='brown',label='ATLAS (2018)')
    #ax.fill_between(m_lhcbold,0,f_lhcbold/np.sqrt(0.14/0.016),alpha=0.8,color='green',label='LHCb old updated effs')
    ax.fill_between(m_lhcbold,0,f_lhcbold,alpha=0.4,color='green',label='LHCb (2019)')
    #ax.fill_between(m_dijet,0,f_dijet,alpha=0.8,color='cornflowerblue',label='Dijet LHC/Tevatron')
    #ax.fill_between(m_nonres,0,f_nonres,alpha=0.6,color='none',hatch='//',edgecolor='black',label='Non resonant limit')

    from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
    ax.xaxis.set_major_locator(MultipleLocator(5))
    ax.xaxis.set_major_formatter('{x:.0f}')

# For the minor ticks, use no labels; default NullFormatter.
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    fig.legend(ncol=2,prop={'size': 18},loc='upper center')
    plt.tight_layout()
    fig.savefig('plots/limits.pdf')

    #plot_region(m_lhcb,f_lhcb,m_lhcbold,f_lhcbold,m_atlas,f_atlas,m_babar,f_babar)
