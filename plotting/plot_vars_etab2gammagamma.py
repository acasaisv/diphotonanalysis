import uproot
import numpy as np
import matplotlib.pyplot as plt

print("open events")
events = uproot.open("/scratch47/adrian.casais/ntuples/background/Stripping34-2022.root:DecayTree/DecayTree")

print("make data frame")
# variables_BDT = []
variables_BDT = [
	"B_B_CONEMULT_1_0",
	"B_B_CONEP_1_0",
	"B_B_CONEPASYM_1_0",
	"B_B_CONEPT_1_0",
	"B_B_CONEPTASYM_1_0",
	"B_B_CONEMULT_1_35",
	"B_B_CONEP_1_35",
	"B_B_CONEPASYM_1_35",
	"B_B_CONEPT_1_35",
	"B_B_CONEPTASYM_1_35",
	"B_B_CONEMULT_1_7",
	"B_B_CONEP_1_7",
	"B_B_CONEPASYM_1_7",
	"B_B_CONEPT_1_7",
	"B_B_CONEPTASYM_1_7"
	]

variables_BDT_sig = [
	"B_s0_1.00_cc_mult",
	"B_s0_1.00_cc_PX",
	"B_s0_1.00_cc_PY",
	"B_s0_1.00_cc_PZ",
	"B_s0_1.00_cc_asy_P",
	"B_s0_1.00_cc_vPT",
	"B_s0_1.00_cc_asy_PT",
	"B_s0_1.35_cc_mult",
	"B_s0_1.35_cc_PX",
	"B_s0_1.35_cc_PY",
	"B_s0_1.35_cc_PZ",
	"B_s0_1.35_cc_asy_P",
	"B_s0_1.35_cc_vPT",
	"B_s0_1.35_cc_asy_PT",
	"B_s0_1.70_cc_mult",
	"B_s0_1.70_cc_PX",
	"B_s0_1.70_cc_PY",
	"B_s0_1.70_cc_PZ",
	"B_s0_1.70_cc_asy_P",
	"B_s0_1.70_cc_vPT",
	"B_s0_1.70_cc_asy_PT"
	]


additional_vars = ['B_L0ElectronDecision_TOS','B_L0PhotonDecision_TOS','B_Hlt1B2GammaGammaDecision_TOS','B_Hlt1B2GammaGammaHighMassDecision_TOS','B_Hlt2RadiativeB2GammaGammaDecision_TOS','Gamma1_PT','Gamma2_PT','B_M', "Gamma1_PX", "Gamma1_PY", "Gamma1_PZ", "Gamma1_P", "Gamma2_PX", "Gamma2_PY", "Gamma2_PZ", "Gamma2_P", "Gamma1_PP_IsPhoton", "Gamma2_PP_IsPhoton", "Gamma1_PP_IsNotH", "Gamma2_PP_IsNotH",'Gamma1_PP_Saturation','Gamma2_PP_Saturation']
variables = variables_BDT+ additional_vars
df = events.arrays(variables, library="pd")

print("cut saturation")
cuts = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS and Gamma1_PT > 3000 and Gamma2_PT > 3000 and Gamma1_PP_Saturation<1 and Gamma2_PP_Saturation<1'
df = df.query(cuts)

eventssig = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/49100041_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree")
eventsBs = uproot.open("/scratch47/adrian.casais/ntuples/signal/etab2gg-sim09.root:DTTBs2GammaGamma/DecayTree")
variables_sig = ["B_s0_TRUEID", "gamma_TRUEID", "gamma0_TRUEID"]
for var in additional_vars:
	var = var.replace("B_M", "B_s0_M")
	var = var.replace("B_B_", "B_s0_")
	var = var.replace("B_L", "B_s0_L")
	var = var.replace("B_H", "B_s0_H")
	var = var.replace("Gamma1_PP_Saturation", "gamma_CaloHypo_Saturation")
	var = var.replace("Gamma2_PP_Saturation", "gamma0_CaloHypo_Saturation")
	var = var.replace("Gamma1", "gamma")
	var = var.replace("Gamma2", "gamma0")
	print(var)
	variables_sig.append(var)
variables_sig=variables_sig+variables_BDT_sig


df_sig = eventssig.arrays(variables_sig, library="pd")
df_bs = eventsBs.arrays(variables_sig, library="pd")
# cuts_sig = cuts
cuts_sig = cuts.replace("B_", "B_s0_").replace("Gamma1", "gamma").replace("Gamma2", "gamma0").replace("PP_Saturation","CaloHypo_Saturation")
cuts_bs = cuts_sig + "& abs(B_s0_TRUEID)==553 & gamma0_TRUEID==22 & gamma_TRUEID==22"
cuts_sig = cuts_sig + "& B_s0_TRUEID==54 & gamma0_TRUEID==22 & gamma_TRUEID==22"
df_sig = df_sig.query(cuts_sig)
df_bs = df_bs.query(cuts_bs)

print(len(df_sig), len(df_bs))

df["OpeningAngle"]=np.arccos((df["Gamma1_PX"]*df["Gamma2_PX"]+df["Gamma1_PY"]*df["Gamma2_PY"]+df["Gamma1_PZ"]*df["Gamma2_PZ"])/(df["Gamma1_P"]*df["Gamma2_P"]))
df_sig["OpeningAngle"]=np.arccos((df_sig["gamma_PX"]*df_sig["gamma0_PX"]+df_sig["gamma_PY"]*df_sig["gamma0_PY"]+df_sig["gamma_PZ"]*df_sig["gamma0_PZ"])/(df_sig["gamma_P"]*df_sig["gamma0_P"]))
df_bs["OpeningAngle"]=np.arccos((df_bs["gamma_PX"]*df_bs["gamma0_PX"]+df_bs["gamma_PY"]*df_bs["gamma0_PY"]+df_bs["gamma_PZ"]*df_bs["gamma0_PZ"])/(df_bs["gamma_P"]*df_bs["gamma0_P"]))

##

df_sig["B_B_CONEP_1_0"]=np.sqrt(df_sig["B_s0_1.00_cc_PX"]**2+df_sig["B_s0_1.00_cc_PY"]**2+df_sig["B_s0_1.00_cc_PZ"]**2)
df_bs["B_B_CONEP_1_0"]=np.sqrt(df_bs["B_s0_1.00_cc_PX"]**2+df_bs["B_s0_1.00_cc_PY"]**2+df_bs["B_s0_1.00_cc_PZ"]**2)

df_sig["B_B_CONEP_1_35"]=np.sqrt(df_sig["B_s0_1.35_cc_PX"]**2+df_sig["B_s0_1.35_cc_PY"]**2+df_sig["B_s0_1.35_cc_PZ"]**2)
df_bs["B_B_CONEP_1_35"]=np.sqrt(df_bs["B_s0_1.35_cc_PX"]**2+df_bs["B_s0_1.35_cc_PY"]**2+df_bs["B_s0_1.35_cc_PZ"]**2)

df_sig["B_B_CONEP_1_7"]=np.sqrt(df_sig["B_s0_1.70_cc_PX"]**2+df_sig["B_s0_1.70_cc_PY"]**2+df_sig["B_s0_1.70_cc_PZ"]**2)
df_bs["B_B_CONEP_1_7"]=np.sqrt(df_bs["B_s0_1.70_cc_PX"]**2+df_bs["B_s0_1.70_cc_PY"]**2+df_bs["B_s0_1.70_cc_PZ"]**2)

plt.hist(df["OpeningAngle"], bins=100, range=(0,0.7),density=True, label="Data")
plt.hist(df_sig["OpeningAngle"], bins=100, range=(0,0.7),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["OpeningAngle"], bins=100, range=(0,0.7),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Opening Angle [rad]")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("OpeningAngle_etabcomp.pdf")
plt.savefig("OpeningAngle_etabcomp.png")
plt.clf()
plt.hist(np.maximum(df["Gamma1_PT"],df["Gamma2_PT"]),range=(3000,30000), bins=100,density=True, label="Data")
plt.hist(np.maximum(df_sig["gamma_PT"],df_sig["gamma0_PT"]),range=(3000,30000), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("max(PT) [MeV]")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MaxPT_etabcomp.pdf")
plt.savefig("MaxPT_etabcomp.png")
plt.clf()
plt.hist(abs(df["Gamma1_PT"]-df["Gamma2_PT"])/(df["Gamma1_PT"]+df["Gamma2_PT"]),range=(0,1), bins=100,density=True, label="Data")
plt.hist(abs(df_sig["gamma_PT"]-df_sig["gamma0_PT"])/(df_sig["gamma_PT"]+df_sig["gamma0_PT"]),range=(0,1), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("PT asymmetry")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("PTasymmetry_etabcomp.pdf")
plt.savefig("PTasymmetry_etabcomp.png")
plt.clf()
plt.hist(np.minimum(df["Gamma1_PT"],df["Gamma2_PT"]),range=(3000,30000), bins=100,density=True, label="Data")
plt.hist(np.minimum(df_sig["gamma_PT"],df_sig["gamma0_PT"]),range=(3000,30000), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("min(PT) [MeV]")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MinPT_etabcomp.pdf")
plt.savefig("MinPT_etabcomp.png")
plt.clf()
plt.hist(np.maximum(df["Gamma1_PP_IsPhoton"],df["Gamma2_PP_IsPhoton"]),range=(0,1.0), bins=100,density=True, label="Data")
plt.hist(np.maximum(df_sig["gamma_PP_IsPhoton"],df_sig["gamma0_PP_IsPhoton"]),range=(0,1.0), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("max(IsPhoton)")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MaxIsPhoton_etabcomp.pdf")
plt.savefig("MaxIsPhoton_etabcomp.png")
plt.clf()
plt.hist(abs((df["Gamma1_PP_IsPhoton"]-df["Gamma2_PP_IsPhoton"])/(df["Gamma1_PP_IsPhoton"]+df["Gamma2_PP_IsPhoton"])),range=(0,1.0), bins=100,density=True, label="Data")
plt.hist(abs((df_sig["gamma_PP_IsPhoton"]-df_sig["gamma0_PP_IsPhoton"])/(df_sig["gamma_PP_IsPhoton"]+df_sig["gamma0_PP_IsPhoton"])),range=(0,1.0), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("IsPhoton asymmetry")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("IsPhoton_asym_etabcomp.pdf")
plt.savefig("IsPhoton_asym_etabcomp.png")
plt.clf()
plt.hist(np.minimum(df["Gamma1_PP_IsPhoton"],df["Gamma2_PP_IsPhoton"]),range=(0,1.0), bins=100,density=True, label="Data")
plt.hist(np.minimum(df_sig["gamma_PP_IsPhoton"],df_sig["gamma0_PP_IsPhoton"]),range=(0,1.0), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("min(IsPhoton)")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MinIsPhoton_etabcomp.pdf")
plt.savefig("MinIsPhoton_etabcomp.png")
plt.clf()
plt.hist(np.maximum(df["Gamma1_PP_IsNotH"],df["Gamma2_PP_IsNotH"]),range=(0.8,1.0), bins=100,density=True, label="Data")
plt.hist(np.maximum(df_sig["gamma_PP_IsNotH"],df_sig["gamma0_PP_IsNotH"]),range=(0.8,1.0), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("max(IsNotH)")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MaxIsNotH_etabcomp.pdf")
plt.savefig("MaxIsNotH_etabcomp.png")
plt.clf()
plt.hist(np.minimum(df["Gamma1_PP_IsNotH"],df["Gamma2_PP_IsNotH"]),range=(0.28,1.0), bins=100,density=True, label="Data")
plt.hist(np.minimum(df_sig["gamma_PP_IsNotH"],df_sig["gamma0_PP_IsNotH"]),range=(0.28,1.0), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.legend()
plt.xlabel("min(IsNotH)")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("MinIsNotH_etabcomp.pdf")
plt.savefig("MinIsNotH_etabcomp.png")
plt.clf()

## BDT variables
plt.hist(df["B_B_CONEMULT_1_0"], bins=40, range=(0,40),density=True, label="Data")
plt.hist(df_sig["B_s0_1.00_cc_mult"], bins=40, range=(0,40),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.00_cc_mult"], bins=40, range=(0,40),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone multiplicity (size 1.0) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("Conemult_1_0_etabcomp.pdf")
plt.savefig("Conemult_1_0_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEMULT_1_35"], bins=40, range=(0,40),density=True, label="Data")
plt.hist(df_sig["B_s0_1.35_cc_mult"], bins=40, range=(0,40),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.35_cc_mult"], bins=40, range=(0,40),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone multiplicity (size 1.35) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("Conemult_1_35_etabcomp.pdf")
plt.savefig("Conemult_1_35_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEMULT_1_7"], bins=40, range=(0,40),density=True, label="Data")
plt.hist(df_sig["B_s0_1.70_cc_mult"], bins=40, range=(0,40),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.70_cc_mult"], bins=40, range=(0,40),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone multiplicity (size 1.7) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("Conemult_1_7_etabcomp.pdf")
plt.savefig("Conemult_1_7_etabcomp.png")
plt.clf()

plt.hist(df["B_B_CONEP_1_0"], bins=100, range=(0,300000),density=True, label="Data")
plt.hist(df_sig["B_B_CONEP_1_0"], bins=100, range=(0,300000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_B_CONEP_1_0"], bins=100, range=(0,300000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone momentum (size 1.0) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEP_1_0_etabcomp.pdf")
plt.savefig("CONEP_1_0_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEP_1_35"], bins=100, range=(0,400000),density=True, label="Data")
plt.hist(df_sig["B_B_CONEP_1_35"], bins=100, range=(0,400000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_B_CONEP_1_35"], bins=100, range=(0,400000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone momentum (size 1.35) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEP_1_35_etabcomp.pdf")
plt.savefig("CONEP_1_35_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEP_1_7"], bins=100, range=(0,400000),density=True, label="Data")
plt.hist(df_sig["B_B_CONEP_1_7"], bins=100, range=(0,400000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_B_CONEP_1_7"], bins=100, range=(0,400000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone momentum (size 1.7) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEP_1_7_etabcomp.pdf")
plt.savefig("CONEP_1_7_etabcomp.png")
plt.clf()


plt.hist(df["B_B_CONEPT_1_0"], bins=100, range=(0,30000),density=True, label="Data")
plt.hist(df_sig["B_s0_1.00_cc_vPT"], bins=100, range=(0,30000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.00_cc_vPT"], bins=100, range=(0,30000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone transverse momentum (size 1.0) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPT_1_0_etabcomp.pdf")
plt.savefig("CONEPT_1_0_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPT_1_35"], bins=100, range=(0,40000),density=True, label="Data")
plt.hist(df_sig["B_s0_1.35_cc_vPT"], bins=100, range=(0,40000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.35_cc_vPT"], bins=100, range=(0,40000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone transverse momentum (size 1.35) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPT_1_35_etabcomp.pdf")
plt.savefig("CONEPT_1_35_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPT_1_7"], bins=100, range=(0,40000),density=True, label="Data")
plt.hist(df_sig["B_s0_1.70_cc_vPT"], bins=100, range=(0,40000),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.70_cc_vPT"], bins=100, range=(0,40000),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone transverse momentum (size 1.7) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPT_1_7_etabcomp.pdf")
plt.savefig("CONEPT_1_7_etabcomp.png")
plt.clf()

plt.hist(df["B_B_CONEPTASYM_1_0"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.00_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.00_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone PT asymmetry (size 1.0) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPTASYM_1_0_etabcomp.pdf")
plt.savefig("CONEPTASYM_1_0_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPTASYM_1_35"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.35_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.35_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone PT asymmetry (size 1.35) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPTASYM_1_35_etabcomp.pdf")
plt.savefig("CONEPTASYM_1_35_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPTASYM_1_7"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.70_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.70_cc_asy_PT"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone PT asymmetry (size 1.7) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPTASYM_1_7_etabcomp.pdf")
plt.savefig("CONEPTASYM_1_7_etabcomp.png")
plt.clf()

plt.hist(df["B_B_CONEPASYM_1_0"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.00_cc_asy_P"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.00_cc_asy_P"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone P asymmetry (size 1.0) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPASYM_1_0_etabcomp.pdf")
plt.savefig("CONEPASYM_1_0_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPASYM_1_35"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.35_cc_asy_P"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.35_cc_asy_P"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone P asymmetry (size 1.35) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPASYM_1_35_etabcomp.pdf")
plt.savefig("CONEPASYM_1_35_etabcomp.png")
plt.clf()
plt.hist(df["B_B_CONEPASYM_1_7"], bins=100, range=(-1,1),density=True, label="Data")
plt.hist(df_sig["B_s0_1.70_cc_asy_P"], bins=100, range=(-1,1),density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_bs["B_s0_1.70_cc_asy_P"], bins=100, range=(-1,1),density=True, color="orange", alpha=0.6, label="$\eta_b\\to\gamma\gamma$")
plt.legend()
plt.xlabel("Cone P asymmetry (size 1.7) ")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("CONEPASYM_1_7_etabcomp.pdf")
plt.savefig("CONEPASYM_1_7_etabcomp.png")
plt.clf()