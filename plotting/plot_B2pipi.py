import uproot
import numpy as np
import matplotlib.pyplot as plt

print("open events")

eventssig = uproot.open("/scratch47/adrian.casais/ntuples/signal/b2gg-sim10.root:DTTBs2GammaGamma/DecayTree")
variables_sig = [
	"gamma_PE",
	"gamma0_PE",
	"gamma_PX",
	"gamma0_PX",
	"gamma_PY",
	"gamma0_PY",
	"gamma_PZ",
	"gamma0_PZ",
	"B_s0_M",
	"gamma_TRUEID",
	"gamma0_TRUEID",
	"B_s0_TRUEID"
	]

df = eventssig.arrays(variables_sig, library="pd")

cuts = 'gamma_TRUEID==22 & gamma0_TRUEID==22 & (B_s0_TRUEID==531 | B_s0_TRUEID==-531)'

df = df.query(cuts)

# sqrt((gamma_PE+gamma0_PE-2*135)**2-(gamma_PX+gamma0_PX)**2-(gamma_PY+gamma_PY)**2-(gamma_PZ+gamma_PZ)**2) 
df["misID_mass"]=np.sqrt((df["gamma_PE"]+df["gamma0_PE"]-2*135)**2-(df["gamma_PX"]+df["gamma0_PX"])**2-(df["gamma_PY"]+df["gamma0_PY"])**2-(df["gamma_PZ"]+df["gamma0_PZ"])**2)-87.4

plt.hist(df["B_s0_M"], bins=100, range=(4800,10000), label="$B^0_s\\to\gamma\gamma$")
plt.hist(df["misID_mass"], bins=100, range=(4800,10000), color="red", alpha=0.6, label="$B^0\\to\pi^0\pi^0$")
plt.xlabel("$m(\gamma\gamma)$ [MeV]")
plt.ylabel("Decays/(52 MeV)")
plt.legend()
plt.tight_layout()
plt.savefig("BTopipi_mass.pdf")

plt.clf()
plt.hist(df["B_s0_M"], bins=100, range=(0,10000), label="$B^0_s\\to\gamma\gamma$")
plt.hist(df["misID_mass"], bins=100, range=(0,10000), color="red", alpha=0.6, label="$B^0\\to\pi^0\pi^0$")
plt.axvline(x=4800, color="orange", label="Stripping threshold")
plt.xlabel("$m(\gamma\gamma)$ [MeV]")
plt.ylabel("Decays/(100 MeV)")
plt.legend()
plt.tight_layout()
plt.savefig("BTopipi_mass_largerange.pdf")

