import uproot
import numpy as np
import matplotlib.pyplot as plt

variables_Bsphigamma = [
	"B_PT",
	"phi_PT",
	"gamma_PT",
	"phi_P",
	"gamma_P",
	"gamma_MC_MOTHER_ID",
	"gamma_TRUEID",
	"phi_TRUEID",
	"kplus_TRUEID",
	"kminus_TRUEID",
	"B_TRUEID",
	"gamma_L0ElectronDecision_TOS",
	"gamma_L0PhotonDecision_TOS",
	"B_Hlt1TrackMVADecision_TOS",
	"B_Hlt2RadiativeBs2PhiGammaDecision_TOS",
	"gamma_CL",
	"B_ETA"
	]

variables_Bsgammagamma = [
	"B_s0_PT",
	"gamma0_PT",
	"gamma_PT",
	"gamma0_P",
	"gamma_P",
	"gamma_MC_MOTHER_ID",
	"gamma0_MC_MOTHER_ID",
	"gamma_TRUEID",
	"gamma0_TRUEID",
	"B_s0_TRUEID",
	"gamma_L0ElectronDecision_TOS",
	"gamma_L0PhotonDecision_TOS",
	"B_s0_Hlt1B2GammaGammaDecision_TOS",
	"B_s0_Hlt1B2GammaGammaHighMassDecision_TOS",
	"B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS",
	"B_s0_ETA"
	]


eventsBsphigamma = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02phigammaMC-sim10.root:DecayTree/DecayTree")
eventsBsgammagamma = uproot.open("/scratch47/adrian.casais/ntuples/signal/b2gg-sim10.root:DTTBs2GammaGamma/DecayTree")


df_Bsphigamma = eventsBsphigamma.arrays(variables_Bsphigamma, library="pd")
df_Bsgammagamma = eventsBsgammagamma.arrays(variables_Bsgammagamma, library="pd")
# cuts_sig = cuts
cuts_Bsphigamma = 'abs(gamma_MC_MOTHER_ID)==531  &  gamma_TRUEID==22 & phi_TRUEID==333 & kplus_TRUEID == 321 & kminus_TRUEID==-321 & abs(B_TRUEID)==531 & gamma_PT>3000 & phi_PT>3000 & B_PT>2500'
cuts_Bsgammagamma = "abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22 & abs(B_s0_TRUEID)==531 & gamma_PT>3000 & gamma0_PT>3000 & B_s0_PT>3000"
df_Bsphigamma = df_Bsphigamma.query(cuts_Bsphigamma)
df_Bsgammagamma = df_Bsgammagamma.query(cuts_Bsgammagamma)

# print(len(df_sig), len(df_bs))




plt.hist(df_Bsgammagamma["B_s0_PT"], bins=100, range=(0,50000),density=True, label="$B^0_s\\to\gamma\gamma$ MC")
plt.hist(df_Bsphigamma["B_PT"], bins=100, range=(0,50000),density=True, color="red", alpha=0.6, label="$B^0_s\\to\phi\gamma$ MC")
plt.legend()
plt.xlabel("$B^0_s$ $p_T$ [MeV]")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("BPT_Bsnorm.pdf")
plt.savefig("BPT_Bsnorm.png")
plt.clf()
plt.hist(df_Bsgammagamma["B_s0_ETA"], bins=100, range=(0,5),density=True, label="$B^0_s\\to\gamma\gamma$ MC")
plt.hist(df_Bsphigamma["B_ETA"], bins=100, range=(0,5),density=True, color="red", alpha=0.6, label="$B^0_s\\to\phi\gamma$ MC")
plt.legend()
plt.xlabel("$B^0_s$ $\eta$ [MeV]")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("BETA_Bsnorm.pdf")
plt.savefig("BETA_Bsnorm.png")
plt.clf()
