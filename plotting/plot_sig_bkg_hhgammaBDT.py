import uproot
import numpy as np
import matplotlib.pyplot as plt
import plot_helpers

variables= [
        'B_DIRA_OWNPV',
        'kplus_IPCHI2_OWNPV',
		'gamma_L0ElectronDecision_TOS',
		'gamma_L0PhotonDecision_TOS',
        'B_Hlt1TrackMVADecision_TOS',
        'B_M',
        ]

variables_Bsphigamma = ['phi_OWNPV_CHI2',
        				#'phi_PT',
        				'kminus_IPCHI2_OWNPV',
        				'phi_M',
        				'B_Hlt2RadiativeBs2PhiGammaDecision_TOS',
						]

truthvars_Bsphigamma = ['gamma_MC_MOTHER_ID',
        				'gamma_TRUEID',
        				'gamma_TRUEP_E',
        				'kplus_TRUEID',
        				'phi_TRUEID',
        				'kminus_TRUEID'			
						]

variables_Bkstargamma = ['kst_OWNPV_CHI2',
        				#'phi_PT',
        				'piminus_IPCHI2_OWNPV',
        				'kst_M',
        				'B_Hlt2RadiativeBd2KstGammaDecision_TOS',
						]

truthvars_Bkstargamma = ['gamma_MC_MOTHER_ID',
        				'gamma_TRUEID',
        				'gamma_TRUEP_E',
        				'kplus_TRUEID',
        				'kst_TRUEID',
        				'piminus_TRUEID'			
						]


eventsBsphigamma_MC = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02phigammaMC-sim10.root:DecayTree/DecayTree")
eventsBsphigamma_Data = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02phigammaS34r0p1.root:DecayTree/DecayTree")
eventsBkstargamma_MC = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02kstargammaMC-sim10.root:DecayTree/DecayTree")
eventsBkstargamma_Data = uproot.open("/scratch47/adrian.casais/ntuples/turcal/b02kstargammaS34r0p1.root:DecayTree/DecayTree")


df_Bsphigamma_MC = eventsBsphigamma_MC.arrays(variables+variables_Bsphigamma+truthvars_Bsphigamma, library="pd")
df_Bdkstargamma_MC = eventsBkstargamma_MC.arrays(variables+variables_Bkstargamma+truthvars_Bkstargamma, library="pd")
df_Bsphigamma_Data = eventsBsphigamma_Data.arrays(variables+variables_Bsphigamma, library="pd")
df_Bdkstargamma_Data = eventsBkstargamma_Data.arrays(variables+variables_Bkstargamma, library="pd")
# cuts_sig = cuts

truthmatching_kstg = 'abs(gamma_MC_MOTHER_ID)==511 & gamma_TRUEID==22 & abs(kst_TRUEID)==313 & ((kplus_TRUEID==321&piminus_TRUEID==-211)|(kplus_TRUEID==-321&piminus_TRUEID==211))'
truthmatching_phig = 'abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and phi_TRUEID==333 and ((kplus_TRUEID==321 & kminus_TRUEID==-211)|(kplus_TRUEID==-321&kminus_TRUEID==211))'

trigger_phig = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS) & B_Hlt1TrackMVADecision_TOS & B_Hlt2RadiativeBs2PhiGammaDecision_TOS'
trigger_kstg = '(gamma_L0ElectronDecision_TOS | gamma_L0PhotonDecision_TOS) & B_Hlt1TrackMVADecision_TOS & B_Hlt2RadiativeBd2KstGammaDecision_TOS'


MC_phig = df_Bsphigamma_MC.query(truthmatching_phig+'&'+trigger_phig)
MC_kstg = df_Bdkstargamma_MC.query(truthmatching_kstg+'&'+trigger_kstg)

Data_phig = df_Bsphigamma_Data.query(trigger_phig+'& B_M>5800')
Data_kstg = df_Bdkstargamma_Data.query(trigger_kstg+'& B_M>5700')


plt.hist(df_Bsphigamma_MC["B_DIRA_OWNPV"], bins=100, range=(0.998,1), histtype='bar', alpha=0.7 ,density=True, label="$B^0_s\\to\phi\gamma$ MC")
plt.hist(df_Bsphigamma_Data["B_DIRA_OWNPV"], bins=100, range=(0.998,1), histtype='step',density=True, color="red", label="$B^0_s\\to\phi\gamma$ upper SB")
plt.legend()
plt.xlabel("$B^0_s$ DIRA")
plt.ylabel("Normalised yield [a.u.]")
plt.yscale('log')
plt.tight_layout()
plt.savefig("BsPhiGamma_DIRA.pdf")
plt.savefig("BsPhiGamma_DIRA.png")
plt.clf()
plt.hist(df_Bdkstargamma_MC["B_DIRA_OWNPV"], bins=100, range=(0.998,1), histtype='bar', alpha=0.7 ,density=True, label="$B^0\\to K^{*0}\gamma$ MC")
plt.hist(df_Bdkstargamma_Data["B_DIRA_OWNPV"], bins=100, range=(0.998,1), histtype='step',density=True, color="red", label="$B^0\\to K^{*0}\gamma$ upper SB")
plt.legend()
plt.xlabel("$B^0$ DIRA")
plt.ylabel("Normalised yield [a.u.]")
plt.yscale('log')
plt.tight_layout()
plt.savefig("BdKstGamma_DIRA.pdf")
plt.savefig("BdKstGamma_DIRA.png")
plt.clf()

plt.hist(df_Bsphigamma_MC["phi_OWNPV_CHI2"], bins=100, range=(0,130), histtype='bar', alpha=0.7 ,density=True, label="$B^0_s\\to\phi\gamma$ MC")
plt.hist(df_Bsphigamma_Data["phi_OWNPV_CHI2"], bins=100, range=(0,130), histtype='step',density=True, color="red", label="$B^0_s\\to\phi\gamma$ upper SB")
plt.legend()
plt.xlabel("$\phi \chi^2_{vtx}$")
plt.ylabel("Normalised yield [a.u.]")
plt.yscale('linear')
plt.tight_layout()
plt.savefig("BsPhiGamma_hhvtxchi2.pdf")
plt.savefig("BsPhiGamma_hhvtxchi2.png")
plt.clf()
plt.hist(df_Bdkstargamma_MC["kst_OWNPV_CHI2"], bins=100, range=(0,130), histtype='bar', alpha=0.7 ,density=True, label="$B^0\\to K^{*0}\gamma$ MC")
plt.hist(df_Bdkstargamma_Data["kst_OWNPV_CHI2"], bins=100, range=(0,130), histtype='step',density=True, color="red", label="$B^0\\to K^{*0}\gamma$ upper SB")
plt.legend()
plt.xlabel("$K^{*0} \chi^2_{vtx}$")
plt.ylabel("Normalised yield [a.u.]")
plt.tight_layout()
plt.savefig("BdKstGamma_hhvtxchi2.pdf")
plt.savefig("BdKstGamma_hhvtxchi2.png")
plt.clf()

plt.hist(df_Bsphigamma_MC["kplus_IPCHI2_OWNPV"], bins=100, range=(0,400),histtype='bar', alpha=0.7 ,density=True, label="$B^0_s\\to\phi\gamma$ MC")
plt.hist(df_Bsphigamma_Data["kplus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='step',density=True, color="red", label="$B^0_s\\to\phi\gamma$ upper SB")
plt.legend()
plt.xlabel("$K^+ \chi^2_{IP}$")
plt.ylabel("Normalised yield [a.u.]")
plt.tight_layout()
plt.savefig("BsPhiGamma_kplusipchi2.pdf")
plt.savefig("BsPhiGamma_kplusipchi2.png")
plt.clf()
plt.hist(df_Bdkstargamma_MC["kplus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='bar', alpha=0.7,density=True, label="$B^0\\to K^{*0}\gamma$ MC")
plt.hist(df_Bdkstargamma_Data["kplus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='step',density=True, color="red", label="$B^0\\to K^{*0}\gamma$ upper SB")
plt.legend()
plt.xlabel("$K^+ \chi^2_{IP}$")
plt.ylabel("Normalised yield [a.u.]")
plt.tight_layout()
plt.savefig("BdKstGamma_kplusipchi2.pdf")
plt.savefig("BdKstGamma_kplusipchi2.png")
plt.clf()

plt.hist(df_Bsphigamma_MC["kminus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='bar', alpha=0.7 ,density=True, label="$B^0_s\\to\phi\gamma$ MC")
plt.hist(df_Bsphigamma_Data["kminus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='step',density=True, color="red", label="$B^0_s\\to\phi\gamma$ upper SB")
plt.legend()
plt.xlabel("$K^- \chi^2_{IP}$")
plt.ylabel("Normalised yield [a.u.]")
plt.tight_layout()
plt.savefig("BsPhiGamma_kminusipchi2.pdf")
plt.savefig("BsPhiGamma_kminusipchi2.png")
plt.clf()
plt.hist(df_Bdkstargamma_MC["piminus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='bar', alpha=0.7 ,density=True, label="$B^0\\to K^{*0}\gamma$ MC")
plt.hist(df_Bdkstargamma_Data["piminus_IPCHI2_OWNPV"], bins=100, range=(0,400), histtype='step',density=True, color="red", label="$B^0\\to K^{*0}\gamma$ upper SB")
plt.legend()
plt.xlabel("$\pi^- \chi^2_{IP}$")
plt.ylabel("Normalised yield [a.u.]")
plt.tight_layout()
plt.savefig("BdKstGamma_piminusipchi2.pdf")
plt.savefig("BdKstGamma_piminusipchi2.png")
plt.clf()
