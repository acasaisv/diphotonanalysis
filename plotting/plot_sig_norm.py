import uproot3 as uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

print("open events")
events_file = uproot.open("/scratch47/adrian.casais/ntuples/background/Stripping34-2022.root")
events = events_file["DecayTree/DecayTree"]

print("make data frame")
# variables_BDT = []
variables_BDT = [
	"B_B_CONEMULT_1_0",
	"B_B_CONEP_1_0",
	"B_B_CONEPASYM_1_0",
	"B_B_CONEPT_1_0",
	"B_B_CONEPTASYM_1_0",
	"B_B_CONEMULT_1_35",
	"B_B_CONEP_1_35",
	"B_B_CONEPASYM_1_35",
	"B_B_CONEPT_1_35",
	"B_B_CONEPTASYM_1_35",
	"B_B_CONEMULT_1_7",
	"B_B_CONEP_1_7",
	"B_B_CONEPASYM_1_7",
	"B_B_CONEPT_1_7",
	"B_B_CONEPTASYM_1_7"
	]

variables_BDT_sig = [
	"B_s0_1.00_cc_mult",
	"B_s0_1.00_cc_PX",
	"B_s0_1.00_cc_PY",
	"B_s0_1.00_cc_PZ",
	"B_s0_1.00_cc_asy_P",
	"B_s0_1.00_cc_vPT",
	"B_s0_1.00_cc_asy_PT",
	"B_s0_1.35_cc_mult",
	"B_s0_1.35_cc_PX",
	"B_s0_1.35_cc_PY",
	"B_s0_1.35_cc_PZ",
	"B_s0_1.35_cc_asy_P",
	"B_s0_1.35_cc_vPT",
	"B_s0_1.35_cc_asy_PT",
	"B_s0_1.70_cc_mult",
	"B_s0_1.70_cc_PX",
	"B_s0_1.70_cc_PY",
	"B_s0_1.70_cc_PZ",
	"B_s0_1.70_cc_asy_P",
	"B_s0_1.70_cc_vPT",
	"B_s0_1.70_cc_asy_PT"
	]


additional_vars = ['B_L0ElectronDecision_TOS','B_L0PhotonDecision_TOS','B_Hlt1B2GammaGammaDecision_TOS','B_Hlt1B2GammaGammaHighMassDecision_TOS','B_Hlt2RadiativeB2GammaGammaDecision_TOS','Gamma1_PT','Gamma2_PT','B_M', "Gamma1_PX", "Gamma1_PY", "Gamma1_PZ", "Gamma1_P", "Gamma2_PX", "Gamma2_PY", "Gamma2_PZ", "Gamma2_P", "Gamma1_PP_IsPhoton", "Gamma2_PP_IsPhoton", "Gamma1_PP_IsNotH", "Gamma2_PP_IsNotH",'Gamma1_PP_Saturation','Gamma2_PP_Saturation']
variables = variables_BDT+ additional_vars
df = events.pandas.df(branches=variables)

print("cut saturation")
cuts = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS and Gamma1_PT > 3000 and Gamma2_PT > 3000 and Gamma1_PP_Saturation<1 and Gamma2_PP_Saturation<1'
df = df.query(cuts)

eventssig_file = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/49100041_1000-1099_Sim10a-priv.root")
eventssig = eventssig_file["DTTBs2GammaGamma/DecayTree"]
eventssig2_file = uproot.open("/scratch47/adrian.casais/ntuples/signal/sim10/49100049_1000-1099_Sim10a-priv.root")
eventssig2 = eventssig_file["DTTBs2GammaGamma/DecayTree"]
eventsBs_file = uproot.open("/scratch47/adrian.casais/ntuples/signal/b2gg-sim10.root")
eventsBs = eventsBs_file["DTTBs2GammaGamma/DecayTree"]
variables_sig = ["B_s0_TRUEID", "gamma_TRUEID", "gamma0_TRUEID"]
for var in additional_vars:
	var = var.replace("B_M", "B_s0_M")
	var = var.replace("B_B_", "B_s0_")
	var = var.replace("B_L", "B_s0_L")
	var = var.replace("B_H", "B_s0_H")
	var = var.replace("Gamma1_PP_Saturation", "gamma_CaloHypo_Saturation")
	var = var.replace("Gamma2_PP_Saturation", "gamma0_CaloHypo_Saturation")
	var = var.replace("Gamma1", "gamma")
	var = var.replace("Gamma2", "gamma0")
	print(var)
	variables_sig.append(var)
variables_sig=variables_sig+variables_BDT_sig


df_sig = eventssig.pandas.df(branches=variables_sig)
df_sig2 = eventssig2.pandas.df(branches=variables_sig)
df_bs = eventsBs.pandas.df(branches=variables_sig)
# cuts_sig = cuts
cuts_sig = cuts.replace("B_", "B_s0_").replace("Gamma1", "gamma").replace("Gamma2", "gamma0").replace("PP_Saturation","CaloHypo_Saturation")
cuts_bs = cuts_sig + "& abs(B_s0_TRUEID)==531 & gamma0_TRUEID==22 & gamma_TRUEID==22"
cuts_sig = cuts_sig + "& B_s0_TRUEID==54 & gamma0_TRUEID==22 & gamma_TRUEID==22"
df_sig = df_sig.query(cuts_sig)
df_sig2 = df_sig2.query(cuts_sig)
df_bs = df_bs.query(cuts_bs)

print(len(df_sig), len(df_bs))


root = '/scratch47/adrian.casais/ntuples/turcal/'
etammg =pd.read_hdf(root+'etammgTurcalData.h5',key='df')
etammgMC =pd.read_hdf(root+'etammgTurcalHardPhotonMC.h5',key='df')

plt.clf()
plt.hist(df_sig["gamma_PP_IsPhoton"],range=(0,1.2), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_sig2["gamma_PP_IsPhoton"],range=(0,1.2), bins=100,density=True, color="blue", alpha=0.6, label="ALP 17 GeV")
plt.hist(etammg["gamma_PP_IsPhoton"],range=(0,1.2), bins=100,density=True, weights=etammg["sweights"], color="green", alpha=0.6, label="$\eta\\to\mu\mu\gamma$ data")
plt.hist(etammgMC["gamma_PP_IsPhoton"],range=(0,1.2), bins=100,density=True, weights=etammgMC["sweights"], color="orange", alpha=0.6, label="$\eta\\to\mu\mu\gamma$ MC")
plt.legend()
plt.xlabel("IsPhoton")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("IsPhoton_sig_norm.pdf")
plt.savefig("IsPhoton_sig_norm.png")
plt.clf()
plt.hist(df_sig["gamma_PP_IsNotH"],range=(0,1.2), bins=100,density=True, color="red", alpha=0.6, label="ALP 6 GeV")
plt.hist(df_sig2["gamma_PP_IsNotH"],range=(0,1.2), bins=100,density=True, color="blue", alpha=0.6, label="ALP 17 GeV")
plt.hist(etammg["gamma_PP_IsNotH"],range=(0,1.2), bins=100,density=True, weights=etammg["sweights"], color="green", alpha=0.6, label="$\eta\\to\mu\mu\gamma$ data")
plt.hist(etammgMC["gamma_PP_IsNotH"],range=(0,1.2), bins=100,density=True, weights=etammgMC["sweights"], color="orange", alpha=0.6, label="$\eta\\to\mu\mu\gamma$ MC")
plt.legend()
plt.xlabel("IsNotH")
plt.ylabel("Normalised yield [a.u.]")
plt.savefig("IsNotH_sig_norm.pdf")
plt.savefig("IsNotH_sig_norm.png")
