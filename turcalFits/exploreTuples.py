from ROOT import TFile,TChain,TTree,TLegend,kRed,kBlue, TH1F, TCanvas
from os import listdir
from os.path import isfile, join

f = []

name = 'B2GammaGammaFromEta2ggTuple/DecayTree'
#name = 'B2GammaGammaFromPi02ggTuple/DecayTree'
#name = 'B2GammaGammaFromEtaTuple/DecayTree'
#name = 'B2GammaGammaFromCharmTuple/DecayTree'
chain = TChain(name)

# home_dir = '/scratch04/adrian.casais/ntuples/turcal'
# for i in range(47):
#     i = str(i)
#     f.append(TFile(home_dir+'/ntuple_{}.root'.format(i.zfill(2))))
#     chain.Add(home_dir +'/ntuple_{}.root'.format(i.zfill(2)))
    
root = '/scratch17/adrian.casais/ntuples/turcal'
files = [join(root,f) for f in listdir(root) if isfile(join(root, f))]
files = list(filter(lambda x: '.root' in x,files))
for i in files:
    #f.append(TFile(i))
    chain.Add(i)
    
leg=TLegend(.6,.6,.8,.8)
def event_match(t,cand='pi0'):
    if cand!='pi0': return False
    cond00 = (t.gamma0_PT == t.gamma_eta_PT)
    cond010 = (t.gamma1_PT == t.gamma_pi00_PT)
    cond011= (t.gamma1_PT == t.gamma_pi01_PT)
    
    cond01 = (t.gamma1_PT == t.gamma_eta_PT)
    cond110 = (t.gamma0_PT == t.gamma_pi00_PT)
    cond111 = (t.gamma0_PT == t.gamma_pi01_PT)
    
    if cond00 and cond010: return 1
    elif cond00 and cond011: return 2

    elif cond01 and cond110: return 3
    elif cond01 and cond111: return 4
    else: return 0
    
entry = 0

for t in chain:
    entry+=1
    if not event_match(t):
     print(entry)
    #print(event_match(t))
def draw(name,tree,nEntries,xMin,xMax,variable,cut,label,color,same=False,leg=0,leg_name=0):
    globals()[name] = TH1F(name,name,nEntries,xMin,xMax)
    tree.Project(name,variable,cut)
    globals()[name].GetXaxis().SetTitle(label)
    globals()[name].SetLineColor(color)
    if not same:
        globals()[name].DrawNormalized('')
    else:
        globals()[name].DrawNormalized('same')
    if leg:
	    leg.AddEntry(globals()[name],leg_name,'L')

chain.GetEntry(0)
# chain.SetAlias('Jpsi_PE','muplus_charm_PE+muminus_charm_PE')
# chain.SetAlias('Jpsi_P','sqrt( (muplus_charm_PX+muminus_charm_PX)**2 + (muplus_charm_PY+muminus_charm_PY)**2 + (muplus_charm_PZ+muminus_charm_PZ)**2 ) ')
# chain.SetAlias('Jpsi_PT','sqrt( (muplus_charm_PX+muminus_charm_PX)**2 + (muplus_charm_PY+muminus_charm_PY)**2 ) ')
# chain.SetAlias('Jpsi_M','sqrt( Jpsi_PE**2 - Jpsi_P**2  )')

# draw('eta1_M-cut',chain,100,400,650,'eta1_M','gamma_eta1_PT > 1100 && eta1_PT > 2000 && gamma_eta1_P > 6000 ','eta1M',kBlue,True,leg,'eta1M')
# draw('eta1_M',chain,100,400,650,'eta1_M','1','eta1M',kRed,True,leg,'eta1M')

# draw('eta0_M-cut',chain,100,400,650,'eta0_M','gamma_eta0_PT > 1100 ','eta0M',kBlue,True,leg,'eta0M')
# draw('eta0_M',chain,100,400,650,'eta0_M','1','eta0M',kRed,True,leg,'eta0M')

# draw('pi0_M-cut',chain,100,400,650,'pi0_M','gamma_pi00_PT > 1100 && gamma_pi01_PT>1100 ','pi0M',kBlue,True,leg,'pi0M')
draw('pi0_M',chain,100,400,650,'pi0_M','1','pi0M',kRed,True,leg,'pi0M')


