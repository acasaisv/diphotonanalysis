import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot
import numpy as np
import pandas

#Bs mass window
m1eta,m2eta = 1750,2000


Eta1MassRange = zfit.Space('eta_M',(m1eta,m2eta))
Eta2MassRange = zfit.Space('eta1_M',(m1eta,m2eta))
obs = Eta1MassRange * Eta2MassRange

root = '/scratch04/adrian.casais/ntuples'
f_s = uproot.open(root + '/dplusetappi.root')
t_s = f_s['DecayTree/DecayTree']


variables = ['Dplus_M',
             'gamma_PT',
             'Dplus_TRUEID',
             
             ]

df = t_s.pandas.df(branches=variables)
df = df.query('Dplus_TRUEID==411 | Dplus_TRUEID==-411 ')
df = df.query('gamma_PT > 1100 & Dplus_M > {0} & Dplus_M < {1}'.format(m1eta,m2eta))

#eta0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.05)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',5.,1.,80.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',1869.65,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',1.25,
                                     0.01,
                                     5,
                                     floating=True
                                     )
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',2,
                                     0.1,
                                     150,
                                     floating=True
                                     )

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-1.25,
                                     -5,
                                     -0.01,
                                     floating=True
                                     )
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',5,
                                     0.1,
                                     150,
                                     floating=True
                                     )
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.1,0.001,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=Eta1MassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=Eta1MassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
#BsCB = BsCBd
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
BsBkgComb = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])




modelBs = zfit.pdf.SumPDF(pdfs = [BsCBExtended,
                                  BsBkgCombExtended
                                  ])
modelBs = BsCBExtended

data_Bs = zfit.Data.from_numpy(array=df['Dplus_M'].values,obs=Eta1MassRange)
#CREATE LOSS FUNCTION

#Extended
nll = zfit.loss.ExtendedUnbinnedNLL(model=[modelBs],
                                    data=[data_Bs])

#nll = zfit.loss.ExtendedUnbinnedNLL(model=model, data=data)
minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)
#PLOT
plot= True
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=70
    nentries=len(df)
    xBs = np.linspace(m1eta,m2eta,nbins)
    countsBs, bin_edgesBs = np.histogram(df['Dplus_M'], nbins, range=(m1eta, m2eta))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2eta-m1eta)/nbins*zfit.run(
        (BsParameters['nSig'])*BsCB.pdf(xBs)
        #+ (BsParameters['nBkgComb'])*BsBkgComb.pdf(xBs)
        )
    yBsSig = (m2eta-m1eta)/nbins*zfit.run(
        (BsParameters['nSig'])*BsCB.pdf(xBs)
        )
    yBsBkg = (m2eta-m1eta)/nbins*zfit.run(
        (BsParameters['nBkgComb'])*BsBkgComb.pdf(xBs)
        )
    
    fig,ax = plt.subplots(1)
    #ax[0].set_ylim([0,250e3])
    ax.errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax.plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax.plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Bs signal')
    #ax.plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax.legend()
    
    
    

    plt.show()


