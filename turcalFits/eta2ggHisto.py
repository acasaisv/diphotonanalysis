import numpy as np
import scipy
####IMPORT DATA
import uproot
import numpy as np
import pandas


from os import listdir
from os.path import isfile, join

#Bs mass window
m1eta,m2eta = 1750,2100

m1pi0,m2pi0 = 460,630





variables = ['charm_M',
             'etap_M',
             'eta_M',
             'gamma_etap_PT',
             'gamma_eta[01]_PT',
             'B_s0_M',
             'B_s0_PT',
             #'B_s0_P',
             'gamma[01]_PT',
             'gamma[01]_P',
             'gamma[01]_CL',
             'B_s0_Hlt1B2GammaGammaDecision_TOS',
             'B_s0_Hlt1Phys_TIS',
             'B_s0_Hlt2Phys_TIS',
             'B_s0_L0Global_TIS',
             'B_s0_L0PhotonDecision_TOS',
             'B_s0_L0ElectronDecision_TOS',
             ]
 
root = '/scratch33/adrian.casais/ntuples/turcal'

f_s = uproot.open(root + '/turcal.root')
tCharm = f_s['B2GammaGammaFromEta2ggCharmTuple/DecayTree']
tEta = f_s['B2GammaGammaFromEta2ggTuple/DecayTree']
dfCharm = tCharm.pandas.df(branches=['B_s0_Hlt1B2GammaGammaDecision_TOS','eta_M'])
dfEta = tEta.pandas.df(branches=['B_s0_Hlt1B2GammaGammaDecision_TOS','eta1_M'])
#dfCharm = dfCharm.query('B_s0_Hlt1B2GammaGammaDecision_TOS')
#dfEta = dfEta.query('B_s0_Hlt1B2GammaGammaDecision_TOS')
plot=True
if plot:
    BsParamsValues = {}

    import matplotlib.pyplot as plt
    nbins=5
    xPhi=np.linspace(m1pi0,m2pi0,1000)
    countsPhi, bin_edgesPhi = np.histogram(np.concatenate((dfCharm['eta_M'].array,dfEta['eta1_M'].array)), int(nbins), range=(m1pi0, m2pi0))
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    
    errPhi = np.sqrt(countsPhi)
    fig,ax = plt.subplots(2)
    
    ax[1].errorbar(bin_centresPhi, countsPhi, yerr=errPhi, fmt='o', color='xkcd:black')
    

    plt.show()


#max(PT) in [2000,3000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 102
b= 3270
s_err = 78
b_err = 390
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
print('HLT1 eff (MC): 0.4944 +- 0.0012')

#max(PT) in [3000,4000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 3004
s = 2102
b= 2696
b = 1811
s_err = 360
s_err = 260
b_err = 400
b_err=280
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [4000,5000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s=1615
b= 337
s_err = 370
b_err = 260
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [5000,6000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 1301
b= 743
s_err = 200
b_err = 160
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [6000,inf]

#TOS: 102 +- 78
#!TOS: 4
s = 1419
b= 412.8
s_err = 260
b_err = 210
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
