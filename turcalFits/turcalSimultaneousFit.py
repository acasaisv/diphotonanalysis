import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot
import numpy as np
import pandas
from os import listdir
from os.path import isfile, join

#Bs mass window
m1eta,m2eta = 460,630


Eta1MassRange = zfit.Space('eta0_M',(m1eta,m2eta))
Eta2MassRange = zfit.Space('eta1_M',(m1eta,m2eta))
obs = Eta1MassRange * Eta2MassRange

# root = '/scratch04/adrian.casais/ntuples/turcal'
# files= [root +'/ntuple_{}.root'.format(str(i).zfill(2)) for i in (list(range(11))+list(range(12,47)))]

root = '/scratch17/adrian.casais/ntuples/turcal'
files = [join(root,f) for f in listdir(root) if isfile(join(root, f))]
files = list(filter(lambda x: '.root' in x,files))

# root = '/scratch33/adrian.casais/ntuples/turcal'
# files = [root+'/turcal.root']


variables = ['eta[01]_M',
             'gamma_eta[01]_PT',
             #'eta1_M',
             #'eta[01]_PT',
             #'eta1_PT',
             #'muplus[01]_PIDmu',
             #'muminus[01]_PIDmu',
             #'muplus1_PIDmu',
             #'muminus1_PIDmu'
             ]
mycache = uproot.cache.ArrayCache(100*(1024)**3)
data = uproot.daskframe(files,
                        #'B2gammagammafrompi02ggtuple/DecayTree',
                        'B2GammaGammaFromEtaTuple/DecayTree',
                        variables,
                        cache=mycache,
                        entrysteps = 500,
                        #outputtype=pandas.DataFrame
                         )
 
data.compute()
data.head()
df = data.compute()
#df = t_s.pandas.df(branches=variables)
#df = df.query(mc)
#df = df.query(trigger)
df = df.query('gamma_eta0_PT > 1100 & gamma_eta1_PT > 1100')
df = df.query('eta0_M > {0} & eta0_M < {1} & eta1_M > {0} & eta1_M < {1}'.format(m1eta,m2eta))
#df = df.query('(Bs_PT > 2000  & (kminus_PT>500 & kplus_PT > 500) & (kminus_PIDK > 5 & kplus_PIDK >5) & (kplus_IPCHI2_OWNPV > 55 & kminus_IPCHI2_OWNPV > 55) & gamma_CL > 0.25 & gamma_PT > 1000 & gamma_P > 6000 & abs(phi_M-1019.46) < 100 & phi_OWNPV_CHI2 < 50)')


#eta0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.05)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',5.,1.,80.)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',1.732,
                                     0.01,
                                     5,
                                     floating=False
                                     )
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',0.827,
                                     0.1,
                                     50,
                                     floating=False
                                     )

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.758,
                                     -5,
                                     -0.01,
                                     floating=False
                                     )
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',1.482,
                                     0.1,
                                     50,
                                     floating=False
                                     )
#BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
#BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.1,0.001,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=Eta1MassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=Eta1MassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
#BsCB = BsCBd
#BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential
#BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])

fbkg_bs = zfit.Parameter(name_prefix+'fbkg',0.5,0,1)
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
BsBkgComb1 = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
lambda2_Bs = zfit.Parameter(name_prefix+'lambda2',-0.1,-5,0)
BsBkgComb2 = zfit.pdf.Exponential(lambda2_Bs,Eta1MassRange)

BsBkgComb = zfit.pdf.SumPDF(pdfs=[BsBkgComb1,BsBkgComb2],fracs=fbkg_bs)


#################

#Eta2

#Signal: Double Crystall Ball
PhiParameters = {}
name_prefix='Phi_'
PhiParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1,0.95,1.05)
PhiParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',5.,1,80.)
#PhiParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
PhiParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
PhiParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[PhiParameters['scale_m'] ,PhiParameters['m_PDG']])
PhiParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',1.732,
                                      0.01,
                                      5,
                                      floating=False
                                      )
PhiParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',0.8333,
                                      0.1,
                                      50.,
                                      floating=False
                                      )

PhiParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.758,
                                      -5,
                                      -0.01,
                                      floating=False
                                      )
PhiParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',1.482,
                                      0.1,
                                      50.,
                                      floating=False
                                      )

#PhiParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
#PhiParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))
#PhiParameters['nBkgNonRes'] = zfit.Parameter(name_prefix+'nBkgNonRes',50000,0,len(df))

PhiParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1)
PhiParameters['fBkgComb'] = zfit.Parameter(name_prefix+'fBkgComb',0.1,0.001,1)


PhiCBu = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_u'],
                          n=PhiParameters['n_u'],
                          obs=Eta2MassRange)

PhiCBd = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_d'],
                          n=PhiParameters['n_d'],
                          obs=Eta2MassRange)
fcb_phi = zfit.Parameter(name_prefix+'fcb',0.5,0,1)
PhiCB = zfit.pdf.SumPDF(pdfs=[PhiCBu,PhiCBd],fracs=fcb_phi)
#PhiCB = PhiCBd
#PhiCBExtended = PhiCB.create_extended(PhiParameters['nSig'])

fbkg_phi = zfit.Parameter(name_prefix+'fbkg',0.5,0,1)
lambda_Phi = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
PhiBkgComb1 = zfit.pdf.Exponential(lambda_Phi,Eta2MassRange)
lambda2_Phi = zfit.Parameter(name_prefix+'lambda2',-0.1,-5,0)
PhiBkgComb2 = zfit.pdf.Exponential(lambda2_Phi,Eta2MassRange)

PhiBkgComb = zfit.pdf.SumPDF(pdfs=[PhiBkgComb1,PhiBkgComb2],fracs=fbkg_phi)

#PhiBkgCombExtended = PhiBkgComb.create_extended(PhiParameters['nBkgComb'])


lambda_PhiNonRes = zfit.Parameter(name_prefix+'lambdaNonRes',-0.1,-5,0)
PhiBkgNonRes = zfit.pdf.Exponential(lambda_PhiNonRes,Eta2MassRange)
#PhiBkgNonResExtended = PhiBkgNonRes.create_extended(PhiParameters['nBkgNonRes'])


#PROD PDFs
nSigSig= zfit.Parameter('nSigSig',10000,0,len(df))
signal = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiCB])
signalExtended=signal.create_extended(nSigSig)

nSigBkg= zfit.Parameter('nSigBkg',10000,0,len(df))
signalbkg = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiBkgComb])
signalBkgExtended=signalbkg.create_extended(nSigBkg)

nBkgSig= zfit.Parameter('nBkgSig',10000,0,len(df))
bkgsignal = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiCB])
bkgSignalExtended=bkgsignal.create_extended(nBkgSig)

nBkgBkg= zfit.Parameter('nBkgBkg',10000,0,len(df))
bkg = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiBkgComb])
bkgExtended=bkg.create_extended(nBkgBkg)


model = zfit.pdf.SumPDF(pdfs = [signalExtended,
                                signalBkgExtended,
                                bkgSignalExtended,
                                bkgExtended])

df_masses = df[['eta0_M','eta1_M']]
data = zfit.Data.from_pandas(df_masses,obs=obs)
data_Bs = zfit.Data.from_numpy(array=df['eta0_M'].values,obs=Eta1MassRange)
data_Phi = zfit.Data.from_numpy(array=df['eta1_M'].values,obs=Eta2MassRange)
#CREATE LOSS FUNCTION

#Extended
# nll = zfit.loss.ExtendedUnbinnedNLL(model=[modelBs,
#                                            modelPhi
#                                            ],
#                                     data=[data_Bs,
#                                           data_Phi
#                                           ])

nll = zfit.loss.ExtendedUnbinnedNLL(model=model, data=data)
minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)
#PLOT
plot= True
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=50
    nentries=len(df)
    xBs = np.linspace(m1eta,m2eta,1000)
    xPhi=np.linspace(m1eta,m2eta,1000)
    countsBs, bin_edgesBs = np.histogram(df['eta0_M'], nbins, range=(m1eta, m2eta))
    countsPhi, bin_edgesPhi = np.histogram(df['eta1_M'], nbins, range=(m1eta, m2eta))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nSigBkg)*BsCB.pdf(xBs)+
        (nBkgSig + nBkgBkg)*BsBkgComb.pdf(xBs)
        )
    yBsSig = (m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nSigBkg)*BsCB.pdf(xBs)
        )
    yBsBkg = (m2eta-m1eta)/nbins*zfit.run(
        (nBkgSig + nBkgBkg)*BsBkgComb.pdf(xBs)
        )
    
    errPhi = np.sqrt(countsPhi)
    yPhi =(m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi) +
        (nBkgBkg + nSigBkg)*PhiBkgComb.pdf(xPhi)
        )
    yPhiSig =(m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi)
        )
    yPhiBkgComb =(m2eta-m1eta)/nbins*zfit.run(
        (nBkgBkg + nSigBkg)*PhiBkgComb.pdf(xPhi)
        )
    
    fig,ax = plt.subplots(2)
    #ax[0].set_ylim([0,250e3])
    ax[0].errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax[0].plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax[0].plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax[0].plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')

    ax[0].legend()
    
    ax[1].errorbar(bin_centresPhi, countsPhi, yerr=errPhi, fmt='o', color='xkcd:black')
    #ax[1].set_yscale('log')
    ax[1].plot(xPhi,yPhi,'-',linewidth=2,color='blue')
    ax[1].plot(xPhi,yPhiSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax[1].plot(xPhi,yPhiBkgComb,'--',linewidth=1,color='orange',label='Combinatorial background')

    #ax[1].plot(xPhi,yPhiBkgNonRes,'--',linewidth=1,color='green',label='Non resonant B meson')
    ax[1].legend()
    
    

    plt.show()




