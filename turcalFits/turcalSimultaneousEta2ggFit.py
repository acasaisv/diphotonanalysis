import zfit
import numpy as np
import scipy
####IMPORT DATA
import uproot
import numpy as np
import pandas


from os import listdir
from os.path import isfile, join

#Bs mass window
m1eta,m2eta = 460,630
m1pi0,m2pi0 = m1eta,m2eta

Eta1MassRange = zfit.Space('eta0_M',(m1eta,m2eta))
Eta2MassRange = zfit.Space('eta1_M',(m1pi0,m2pi0))
obs = Eta1MassRange * Eta2MassRange

# root = '/scratch04/adrian.casais/ntuples/turcal'
# files= [root +'/ntuple_{}.root'.format(str(i).zfill(2)) for i in (list(range(11))+list(range(12,47)))]
#files = files[0:3]

root = '/scratch17/adrian.casais/ntuples/turcal'
files = [join(root,f) for f in listdir(root) if isfile(join(root, f))]
files = list(filter(lambda x: '.root' in x,files))

# root = '/scratch33/adrian.casais/ntuples/turcal'
# files = [root+'/turcal.root']

variables = ['eta[01]_M',
             'gamma_eta[01]_PT',
             'gamma_eta_PT',
             'B_s0_M',
             'B_s0_PT',
             #'B_s0_P',
             'gamma[01]_PT',
             'gamma[01]_P',
             'gamma[01]_CL',
             'B_s0_Hlt1B2GammaGammaDecision_TOS',
             'B_s0_Hlt1Phys_TIS',
             'B_s0_Hlt2Phys_TIS',
             'B_s0_L0Global_TIS',
             'B_s0_L0PhotonDecision_TOS',
             'B_s0_L0ElectronDecision_TOS',
             ]
mycache = uproot.cache.ArrayCache(30*(1024)**6)
data = uproot.daskframe(files,
                        #'B2gammagammafrompi02ggtuple/DecayTree',
                        'B2GammaGammaFromEta2ggTuple/DecayTree',
                        variables,
                        cache=mycache,
                        entrysteps = 5000,
                        #outputtype=pandas.DataFrame
                         )
 
data.compute()
data.head()
df = data.compute()
#df = t_s.pandas.df(branches=variables)
#df = df.query(mc)
#df = df.query(trigger)
df = df.query('gamma_eta0_PT > 1100 & gamma_eta_PT > 1100 & gamma_eta1_PT> 1100')
df = df.query('eta0_M > {0} & eta0_M < {1} & eta1_M > {0} & eta1_M < {1}'.format(m1eta,m2eta))
df = df.query('B_s0_M > 4000 & B_s0_PT > 2000 & gamma0_PT > 1100 & gamma1_PT>1100 & gamma0_CL > 0.3 & gamma1_CL > 0.3 & gamma0_P > 6000 & gamma1_P > 6000')
df=df.query('gamma0_PT > 2000 & gamma1_PT > 2000')

# ptHigh= 40000000000000000000000000000000000
# ptLow = 6000
# df['gamma_MaxPT'] = df[['gamma0_PT','gamma1_PT']].max(axis=1)
# #df = df.query('gamma_MaxPT > {0} & gamma_MaxPT < {1}'.format(ptLow,ptHigh))
# #df = df.query('(B_s0_L0ElectronDecision_TOS==1 | B_s0_L0PhotonDecision_TOS==1)')

# df = df.query('B_s0_Hlt2Phys_TIS==1 & B_s0_Hlt1Phys_TIS==1 & B_s0_L0Global_TIS==1')

#df = df.query('(B_s0_Hlt1B2GammaGammaDecision_TOS==1)')


#eta0



#Signal: Double Crystall Ball
name_prefix='Bs_'
BsParameters = {}
BsParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1.004,0.95,1.05,
                                         floating=False)
BsParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',16.08,1.,120.,
                                       floating= False)
#BsParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
BsParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
BsParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[BsParameters['scale_m'] ,BsParameters['m_PDG']])
BsParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',1.413,
                                     0.1,
                                     5,
                                     floating=False
                                     )
BsParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',6.905,
                                     0.1,
                                     50,
                                     floating=False
                                     )

BsParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',-0.724,
                                     -5,
                                     -0.1,
                                     floating=False
                                     )
BsParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',2.334,
                                     0.1,
                                     50,
                                     floating=False
                                     )
BsParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
BsParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))

BsParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.1,0.001,1)




BsCBu = zfit.pdf.CrystalBall(mu =BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_u'],
                          n=BsParameters['n_u'],
                          obs=Eta1MassRange)
BsCBd = zfit.pdf.CrystalBall(mu=BsParameters['m'],
                          sigma=BsParameters['sigma'],
                          alpha=BsParameters['a_d'],
                          n=BsParameters['n_d'],
                          obs=Eta1MassRange)

fcb_Bs = zfit.Parameter(name_prefix+'fcb',0.4081,0,1,floating=False)
BsCB = zfit.pdf.SumPDF(pdfs=[BsCBu,BsCBd],fracs=fcb_Bs)
#BsCB = BsCBd
BsCBExtended = BsCB.create_extended(BsParameters['nSig'])

#Background: exponential

#fbkg_bs = zfit.Parameter(name_prefix+'fbkg',0.5,0,1)
lambda_Bs = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
BsBkgComb1 = zfit.pdf.Exponential(lambda_Bs,Eta1MassRange)
#lambda2_Bs = zfit.Parameter(name_prefix+'lambda2',-0.1,-5,0)
#BsBkgComb2 = zfit.pdf.Exponential(lambda2_Bs,Eta1MassRange)

#BsBkgComb = zfit.pdf.SumPDF(pdfs=[BsBkgComb1,BsBkgComb2],fracs=fbkg_bs)
BsBkgComb = BsBkgComb1 
BsBkgCombExtended = BsBkgComb.create_extended(BsParameters['nBkgComb'])

#################

#Eta2

#Signal: Double Crystall Ball
PhiParameters = {}
name_prefix='Phi_'
PhiParameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',0.9998,0.95,1.05,floating=False)
PhiParameters['sigma'] = zfit.Parameter(name_prefix+'sigma',
                                        20.4,
                                        1.,
                                        80.,
                                        floating=False)
#PhiParameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
PhiParameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',547.862,floating=False)
PhiParameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[PhiParameters['scale_m'] ,PhiParameters['m_PDG']])
PhiParameters['a_u'] = zfit.Parameter(name_prefix+'a_u',
                                      #2.178,
                                      1.901,
                                      0.01,
                                      5,
                                      floating=False
                                      )
PhiParameters['n_u'] = zfit.Parameter(name_prefix+'n_u',
                                      53.26,
                                      0.8,
                                      20.,
                                      floating=False
                                      )

PhiParameters['a_d'] = zfit.Parameter(name_prefix+'a_d',
                                      -0.4619,
                                      -5,
                                      -0.01,
                                      floating=False
                                      )
PhiParameters['n_d'] = zfit.Parameter(name_prefix+'n_d',
                                      #129.1,
                                      113.5,
                                      0.8,
                                      150,
                                      floating=False
                                      )

PhiParameters['nSig'] = zfit.Parameter(name_prefix+'nSig',10000,0,len(df))
PhiParameters['nBkgComb'] = zfit.Parameter(name_prefix+'nBkgComb',50000,0,len(df))
#PhiParameters['nBkgNonRes'] = zfit.Parameter(name_prefix+'nBkgNonRes',50000,0,len(df))

PhiParameters['fSig'] = zfit.Parameter(name_prefix+'fSig',0.5,0,1,floating=True)
PhiParameters['fBkgComb'] = zfit.Parameter(name_prefix+'fBkgComb',0.1,0.001,1)


PhiCBu = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          alpha=PhiParameters['a_u'],
                          n=PhiParameters['n_u'],
                          obs=Eta2MassRange)

PhiCBd = zfit.pdf.CrystalBall(mu=PhiParameters['m'],
                          sigma=PhiParameters['sigma'],
                          
                          alpha=PhiParameters['a_d'],
                          n=PhiParameters['n_d'],
                          obs=Eta2MassRange)


fcb_phi = zfit.Parameter(name_prefix+'fcb',
                         0.21,
                         0,
                         1,
                         floating = False
                         )
PhiCB = zfit.pdf.SumPDF(pdfs=[PhiCBu,PhiCBd],fracs=fcb_phi)
#PhiCB = PhiCBd
PhiCBExtended = PhiCB.create_extended(PhiParameters['nSig'])

fbkg_phi = zfit.Parameter(name_prefix+'fbkg',0.5,0,1)
lambda_Phi = zfit.Parameter(name_prefix+'lambda',0.1,0,5)
PhiBkgComb1 = zfit.pdf.Exponential(lambda_Phi,Eta2MassRange)
# lambda2_Phi = zfit.Parameter(name_prefix+'lambda2',-0.1,-5,0)
# PhiBkgComb2 = zfit.pdf.Exponential(lambda2_Phi,Eta2MassRange)

# PhiBkgComb = zfit.pdf.SumPDF(pdfs=[PhiBkgComb1,PhiBkgComb2],fracs=fbkg_phi)
PhiBkgComb = PhiBkgComb1
PhiBkgCombExtended = PhiBkgComb.create_extended(PhiParameters['nBkgComb'])


lambda_PhiNonRes = zfit.Parameter(name_prefix+'lambdaNonRes',-0.1,-5,0)
PhiBkgNonRes = zfit.pdf.Exponential(lambda_PhiNonRes,Eta2MassRange)
#PhiBkgNonResExtended = PhiBkgNonRes.create_extended(PhiParameters['nBkgNonRes'])


#PROD PDFs
nSigSig= zfit.Parameter('nSigSig',10000,0,len(df))
signal = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiCB])
signalExtended=signal.create_extended(nSigSig)

nSigBkg= zfit.Parameter('nSigBkg',10000,0,len(df))
signalbkg = zfit.pdf.ProductPDF(pdfs=[BsCB,PhiBkgComb])
signalBkgExtended=signalbkg.create_extended(nSigBkg)

nBkgSig= zfit.Parameter('nBkgSig',10000,0,len(df))
bkgsignal = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiCB])
bkgSignalExtended=bkgsignal.create_extended(nBkgSig)

nBkgBkg= zfit.Parameter('nBkgBkg',10000,0,len(df))
bkg = zfit.pdf.ProductPDF(pdfs=[BsBkgComb,PhiBkgComb])
bkgExtended=bkg.create_extended(nBkgBkg)


model = zfit.pdf.SumPDF(pdfs = [signalExtended,
                                signalBkgExtended,
                                bkgSignalExtended,
                                bkgExtended])

df_masses = df[['eta0_M','eta1_M']]
data = zfit.Data.from_pandas(df_masses,obs=obs)

data_Bs = zfit.Data.from_numpy(array=df['eta0_M'].values,obs=Eta1MassRange)
data_Phi = zfit.Data.from_numpy(array=df['eta1_M'].values,obs=Eta2MassRange)
#CREATE LOSS FUNCTION

modelBs = zfit.pdf.SumPDF(pdfs=[BsCBExtended,BsBkgCombExtended])
modelPhi = zfit.pdf.SumPDF(pdfs=[PhiCBExtended,PhiBkgCombExtended])

#Extended
# nll = zfit.loss.ExtendedUnbinnedNLL(model=[
#                                             #modelBs,
#                                             modelPhi
#                                             ],
#                                      data=[#data_Bs,
#                                            data_Phi
#                                            ])

nll = zfit.loss.ExtendedUnbinnedNLL(model=model, data=data)
minimizer = zfit.minimize.Minuit(tolerance=1e-4)
#minimizer = zfit.minimize.BFGS()
result = minimizer.minimize(nll)
result.hesse()
#result.errors()
print(result.params)
#PLOT
plot= True
if plot:
    BsParamsValues = {}
    for key in BsParameters:
        BsParamsValues[key] = zfit.run(BsParameters[key])

    import matplotlib.pyplot as plt
    nbins=50
    nentries=len(df)
    xBs = np.linspace(m1eta,m2eta,1000)
    xPhi=np.linspace(m1pi0,m2pi0,1000)
    countsBs, bin_edgesBs = np.histogram(df['eta0_M'], nbins, range=(m1eta, m2eta))
    countsPhi, bin_edgesPhi = np.histogram(df['eta1_M'], int(nbins), range=(m1pi0, m2pi0))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    bin_centresPhi = (bin_edgesPhi[:-1] + bin_edgesPhi[1:])/2.
    errBs = np.sqrt(countsBs)
    yBs =(m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nSigBkg)*BsCB.pdf(xBs)+
        (nBkgSig + nBkgBkg)*BsBkgComb.pdf(xBs)
        )
    yBsSig = (m2eta-m1eta)/nbins*zfit.run(
        (nSigSig + nSigBkg)*BsCB.pdf(xBs)
        )
    yBsBkg = (m2eta-m1eta)/nbins*zfit.run(
        (nBkgSig + nBkgBkg)*BsBkgComb.pdf(xBs)
        )
    
    errPhi = np.sqrt(countsPhi)
    yPhi =(m2pi0-m1pi0)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi) +
        (nBkgBkg + nSigBkg)*PhiBkgComb.pdf(xPhi)
        )
    yPhiSig =(m2pi0-m1pi0)/nbins*zfit.run(
        (nSigSig + nBkgSig)*PhiCB.pdf(xPhi)
        )
    yPhiBkgComb =(m2pi0-m1pi0)/nbins*zfit.run(
        (nBkgBkg + nSigBkg)*PhiBkgComb.pdf(xPhi)
        )
    
    fig,ax = plt.subplots(2)
    #ax[0].set_ylim([0,250e3])
    ax[0].errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    #ax[0].set_yscale('log')
    ax[0].plot(xBs,yBs,'-',linewidth=2,color='blue')
    ax[0].plot(xBs,yBsSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax[0].plot(xBs,yBsBkg,'--',linewidth=1,color='green',label='Combinatorial background')
    ax[0].legend()
    
    ax[1].errorbar(bin_centresPhi, countsPhi, yerr=errPhi, fmt='o', color='xkcd:black')
    #ax[1].set_yscale('log')
    #ax[1].set_ylim([000,1200])
    ax[1].plot(xPhi,yPhi,'-',linewidth=2,color='blue')
    ax[1].plot(xPhi,yPhiSig,'--',linewidth=1,color='red',label='Signal: Double CB')
    ax[1].plot(xPhi,yPhiBkgComb,'--',linewidth=1,color='orange',label='Combinatorial background')
    #ax[1].plot(xPhi,yPhiBkgNonRes,'--',linewidth=1,color='green',label='Non resonant B meson')
    ax[1].legend()
    

    plt.show()


#max(PT) in [2000,3000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 102
b= 3270
s_err = 78
b_err = 390
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
print('HLT1 eff (MC): 0.4944 +- 0.0012')

#max(PT) in [3000,4000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 3004
s = 2102
b= 2696
b = 1811
s_err = 360
s_err = 260
b_err = 400
b_err=280
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [4000,5000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s=1615
b= 337
s_err = 370
b_err = 260
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [5000,6000]

#TOS: 102 +- 78
#!TOS: 3270 +- 390
s = 1301
b= 743
s_err = 200
b_err = 160
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))

#max(PT) in [6000,inf]

#TOS: 102 +- 78
#!TOS: 4
s = 1419
b= 412.8
s_err = 260
b_err = 210
eff = s/(b+s)
err2 = (s_err**2 * b**2 + b_err**2 * s**2)/(s+b)**4
err = np.sqrt(err2)

print('HLT1 eff (DATA): {0} +- {1}'.format(eff,err))
