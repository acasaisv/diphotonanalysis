#! /usr/bin/env python

## IMPORT NECESSARY MODULES AND CONFIGURABLES
import GaudiPython, sys
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer,DaVinci
from Configurables import InputCopyStream

## CONFIGURE DaVINCI
DaVinci().DataType = "2016"
DaVinci().HistogramFile = "DVHistos_1.root"    # Histogram file
DaVinci().TupleFile = "DVNtuples.root"         # Ntuple
DaVinci().MoniSequence = []
DaVinci().Simulation   = True
#DaVinci().DDDBtag = "head-20100518"
#DaVinci().CondDBtag = "head-20100531"

#decns = ["23175001","23175000","12115070"]
ind1,ind2 = 0,0

if len(sys.argv)>1:
	ind1 = int(sys.argv[1])
	ind2 = int(sys.argv[1]) #no need for ind1 ....

def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in xrange(0, len(l), n):
		yield l[i:i + n]

## 20 chunks
#decn = decns[ind1]
# HOME = "/eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/"
# files = filter(lambda x: ".dst" in x,os.listdir(HOME))

root="root://castorlhcb.cern.ch//castor/cern.ch/user/a/acasaisv/dsts/ALPs/49000005_md_2017/"
files = map(lambda x: root+"Brunel_49000005_md_2017_{0}.dst".format(x),range(int(2e4)))
#mychunks = list(chunks(filter(lambda y: ".dst" in y,os.listdir(HOME)),1000))
mychunks = list(chunks(files,1000))
if ind2>=len(mychunks): exit()
inpdsts = mychunks[ind2]
#DaVinci().Input = map(lambda x: HOME+x,inpdsts)
DaVinci().Input = inpdsts




## CONFIGURE DST WRITER
DstWriter = InputCopyStream("DstWriter")
#DstWriter.Output = "DATAFILE=\'PFN:/eos/home-j/jcidvida/dsts/"+decn+"/MC2016_Priv_MagDown_"+decn+"_"+str(ind2)+".ldst\' TYP=\'POOL_ROOTTREE\' OPT=\'REC\' "
current_off = 0
condor_root ="root://castorlhcb.cern.ch//castor/cern.ch/user/a/acasaisv/dsts/ALPs/49000005_md_2017/merged"
DstWriter.Output = "DATAFILE=\'PFN:"+condor_root+"/MC2017_Priv_MagDown_hardPi0_"+str(ind2+current_off)+".ldst ' TYP=\'POOL_ROOTTREE\' OPT=\'REC\' "
DstWriter_GaSeq = GaudiSequencer("DstWriter_GaSeq") # create GaudiSequencer with the DstWriter
DstWriter_GaSeq.Members =[DstWriter]
DaVinci().UserAlgorithms = [DstWriter_GaSeq]

## SOME EXTRA CONFIGURATION AND INITIALIZATION OF GAUDI
appConf = ApplicationMgr()
appConf.HistogramPersistency = 'ROOT'
appConf.ExtSvc += ['DataOnDemandSvc']

gaudi = GaudiPython.AppMgr(outputlevel=3)
gaudi.initialize() 

## RUN ON SOME EVENTS AND FINALIZE
TES = gaudi.evtsvc()

def finalize(endPy=False) :
	print "done, quit"
	gaudi.stop()
	gaudi.finalize()
	if(endPy): exit()
		
gaudi.run(-1)
finalize()

