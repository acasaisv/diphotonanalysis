#Test your line(s) of the stripping
#  
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#

#CONFIGURED FOR Bs change to Magnet Down when using with ALPs

from Gaudi.Configuration import *
from Configurables import DaVinci,ChargedPP2MC,ChargedProtoParticleMaker
from StrippingConf.Configuration import StrippingConf

confname='Bs2GammaGamma' 

from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder

streams = buildStreamsFromBuilder(confs,confname)

#clone lines for CommonParticles overhead-free timing
print "Creating line clones for timing"
for s in streams:
    for l in s.lines:
        if "_TIMING" not in l.name():
            cloned = l.clone(l.name().strip("Stripping")+"_TIMING")
            s.appendLines([cloned])

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = -1,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#
# Configure the dst writers for the output
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
        }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=False)
        }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Bs2ggStripping',
                          SelectionSequences = sc.activeStreams()
                          )
ind1,ind2 = 0,0

if len(sys.argv)>1:
	ind1 = int(sys.argv[1])
	ind2 = int(sys.argv[1]) #no need for ind1 ....

def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in xrange(0, len(l), n):
		yield l[i:i + n]

condor_root ="root://castorlhcb.cern.ch//castor/cern.ch/user/a/acasaisv/dsts/ALPs/49000005_md_2017/merged/"
files = map(lambda x: condor_root+"MC2017_Priv_MagDown_hardPi0_{0}.ldst".format(x), range(20))
mychunks = list(chunks(files,2)) #10 chunks
inpdsts = mychunks[ind2]
DaVinci().Input =inpdsts
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])
DaVinci().DataType = "2017"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20190430-1-vc-md100"

import GaudiPython
from Gaudi.Configuration import *
gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
ToolSvc = gaudi.toolSvc()
gaudi.run(-1)
gaudi.stop()
gaudi.finalize()
