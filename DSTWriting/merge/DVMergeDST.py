from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import (DaVinci, LumiAlgsConf)
import os
DaVinci().Simulation=True
DaVinci().DataType= '2018'
DaVinci().DDDBtag  ="dddb-20210528-8"
DaVinci().CondDBtag  = "sim-20201113-8-vc-md100-Sim10"

DaVinci().InputType = 'DST'
ioh = IOHelper()
#ioh.outStream('PFN:$PWD/myfile.dst')
mylist = []
chunk = os.environ['myvar']
with open('files_0{}'.format(chunk),'rb') as file:
    for line in file:
        mylist.append(str(line[0:-1]))
    
DaVinci().Input = mylist
root = '/scratch47/adrian.casais/dsts/alps-sim10-priv/' 
name = "DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='REC'".format(root + '49100040_{}_Sim10a-priv.dst'.format(chunk))
ics = InputCopyStream('mywriter')
ics.Output = name
DaVinci().UserAlgorithms = [ics]

