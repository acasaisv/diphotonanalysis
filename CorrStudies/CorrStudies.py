from  ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy import stats
from helpers import pTs,etas,spds,get_error,get_error_w,pack_eff,get_ALPsdf,sim10_masses_map,nearest_rect,ufloat_eff
import pickle
from uncertainties import ufloat
import plot_helpers

def corr_dist(df,vars,nbins=50,labels=[], weights=[],ranges = []):

    if not len(weights):
        weights = np.ones(len(df))
    i,j = nearest_rect(len(vars))
    dpi = 80.
    px = 1./dpi
    fig,ax=plt.subplots(i,j,dpi=dpi,figsize=(1920*px,1080*px))
    ax = ax.flatten()
    
    for var,ax,label,range in zip(vars,ax,labels,ranges):
        #ax.set_yscale('log')
        ax.hist2d(df['dist'],
                  df.eval(var),
                  bins=nbins,
                  weights=weights,
                  range = [[0,8500],range],
                  norm=mpl.colors.LogNorm()
        )
        ax.set_xlabel('dist [mm]')
        ax.set_ylabel (label)

    #plt.legend()
    plt.show()

def reweight(df):
    from sklearn.tree import DecisionTreeClassifier
    from hep_ml.reweight import BinsReweighter, GBReweighter
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=df['dist'], target=3000*np.random.rand(50*len(df)))
    
    return reweighter.predict_weights(df['dist'])

def pteff(df):
    eff = len(df.query('gamma_PT > 3500 and gamma0_PT > 3500'))/len(df)
    print(f'Eff {100.*eff:.2f}')

def apply_size(df):
    df.loc[df['gamma_CaloHypo_CellID']>=41.e3, 'gamma_CellSize'] = 40.4
    df.loc[(df['gamma_CaloHypo_CellID']<41.e3) & (df['gamma_CaloHypo_CellID']>=37.e3), 'gamma_CellSize'] = 60.6
    df.loc[df['gamma_CaloHypo_CellID']<37.e3, 'gamma_CellSize'] = 121.2

    df.loc[df['gamma0_CaloHypo_CellID']>=41.e3, 'gamma0_CellSize'] = 40.4
    df.loc[(df['gamma0_CaloHypo_CellID']<41.e3) & (df['gamma0_CaloHypo_CellID']>=37.e3), 'gamma0_CellSize'] = 60.6
    df.loc[df['gamma0_CaloHypo_CellID']<37.e3, 'gamma0_CellSize'] = 121.2

    return df

def overlap(df):
    df.eval('one = (gamma_CaloHypo_X + 1.5*gamma_CellSize) > (gamma0_CaloHypo_X - 1.5*gamma0_CellSize) ',inplace=True)
    df.eval('two = (gamma0_CaloHypo_X + 1.5*gamma0_CellSize) > (gamma_CaloHypo_X - 1.5*gamma_CellSize)',inplace=True)
    df.eval('three = (gamma0_CaloHypo_Y - 1.5*gamma0_CellSize) <(gamma_CaloHypo_Y + 1.5*gamma_CellSize) ',inplace=True)
    df.eval('four = (gamma_CaloHypo_Y - 1.5*gamma_CellSize) < (gamma0_CaloHypo_Y + 1.5*gamma0_CellSize)',inplace=True)
    df.eval('overlap = one and two  and three and four ',inplace=True)
    return df


def lzero_check(df):
    print('L0')
    cut = "(gamma_L0ElectronDecision_TOS or gamma_L0PhotonDecision_TOS)"
    cut0 = "(gamma0_L0ElectronDecision_TOS or gamma0_L0PhotonDecision_TOS)"
    cutbs = "(B_s0_L0ElectronDecision_TOS or B_s0_L0PhotonDecision_TOS)"
    eff = ufloat_eff(df.query(cut).shape[0],df.shape[0])
    eff0 = ufloat_eff(df.query(cut0).shape[0],df.shape[0])
    effbs = ufloat_eff(df.query(cutbs).shape[0],df.shape[0])
    effaa = eff + eff0 - eff*eff0
    dfl0 = df.query(cut0)

    eff_rel0 = ufloat_eff(dfl0.query(cut).shape[0],dfl0.shape[0])
    print('effbs',effbs)
    print('effaa',effaa)
    print(f"Relative difference: {(effbs-effaa)/effbs}")
    print(f"eff(l0gamma/l0gamma0) = {eff_rel0}. eff(l0gamma)={eff}")

def stripping_check(df):
    print("Stripping")
    cut = '(gamma_CL > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1)'
    cut0 = '(gamma0_CL > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1)'
    cutbs = f'{cut} and {cut0}'
    eff = ufloat_eff(df.query(cut).shape[0],df.shape[0])
    eff0 = ufloat_eff(df.query(cut0).shape[0],df.shape[0])
    effbs = ufloat_eff(df.query(cutbs).shape[0],df.shape[0])
    effaa = eff*eff0
    dfl0 = df.query(cut0)

    eff_rel0 = ufloat_eff(dfl0.query(cut).shape[0],dfl0.shape[0])
    print('effbs',effbs)
    print('effaa',effaa)
    print(f"Relative difference: {(effbs-effaa)/effbs}")
    print(f"eff(l0gamma/l0gamma0) = {eff_rel0}. eff(l0gamma)={eff}")


truth="(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) and B_s0_TRUEID==54 "

L0= "(B_s0_L0ElectronDecision_TOS or B_s0_L0PhotonDecision_TOS)"
HLT1 = " (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) "
HLT2 = " B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS "
aa = "and"
cut = truth
cut = truth + aa + L0+aa+HLT1+aa+HLT2
saturation = " gamma_CaloHypo_Saturation==0 and gamma0_CaloHypo_Saturation==0 "
pt = ' gamma_PT > 2500 and gamma0_PT > 2500 '
nspds = ' nSPDHits < 450 '
cut+=aa + saturation + aa + pt

#cut += aa+truth
if __name__ == '__main__':
    masses = sim10_masses_map
    for i in [40,50]:
        print(10*"**")
        print(masses[i])
        bs = False
        if i=='bs':
            bs=True
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/new/'
        tupfile = root+f'491000{i}_1000-1099_Sim10a-priv-v44r11p1.root'
        df = get_ALPsdf(tupfile,bs,background=False,sample=int(1000e4),extravars=['gamma_CaloHypo_ClusterFrac',
            'gamma_CaloHypo_Spread',
            'gamma_CaloHypo_PrsE49',
            'gamma_CaloHypo_PrsE4Max',
            ],stripping=False)
        df = apply_size(df)
        df = overlap(df)
        print(f"Overlap rate: {100.*len(df.query('overlap==True'))/len(df)} %")
        #lzero_check(df.query(truth+aa+saturation+aa+pt+aa+nspds))
        df.eval('dist=sqrt((gamma_CaloHypo_X-gamma0_CaloHypo_X)**2 + (gamma_CaloHypo_Y-gamma0_CaloHypo_Y)**2)',inplace=True)
        new = df.query(truth+aa+nspds+aa+saturation+aa+pt)
        # plt.hist(new['B_s0_M'],bins=50,histtype='step',color='red',density=True)
        # plt.savefig(f'mass{i}.pdf')
        lzero_check(new)
        stripping_check(new)

        #df.query(truth,inplace=True)
        
        
        #Lzero or



        # df.eval('L0PT_res=(gamma_TRUEPT - gamma_L0Calo_ECAL_TriggerET)/gamma_TRUEPT',inplace=True)
        # df.eval('E_res=(gamma_TRUEP_E - gamma_P)/gamma_TRUEP_E',inplace=True)
        # df.eval('PT_res=(gamma_TRUEPT - gamma_PT)/gamma_TRUEPT',inplace=True)
        # df.query('abs(L0PT_res)<1000 and abs(PT_res)<1000 and abs(E_res)<1000',inplace=True)
        # plt.yscale('log')
        # plt.xscale('log')
        # plt.hist2d(1-df['gamma_CL'],1-df['gamma0_CL'],bins=50,norm=mpl.colors.LogNorm())
        # corr_dist(df,
        #     [
        #     'gamma_CaloHypo_isNotE',
        #     'gamma_CaloHypo_isPhoton',
        #     'gamma_CL',
        #     'gamma_CaloHypo_ClusterFrac',
        #     'gamma_CaloHypo_Spread',
        #     'gamma_CaloHypo_PrsE49',
        #     'gamma_CaloHypo_PrsE4Max',
        #     'L0PT_res',
        #     'E_res',
        #     'PT_res'
        #     ],
        # labels= [
        #         'isNotE',
        #         'isPhoton',
        #         'isNotH',
        #         'CusterFrac',
        #         'Spread',
        #         'gamma_CaloHypo_PrsE49',
        #         'gamma_CaloHypo_PrsE4Max',
        #         'L0PT_res',
        #         'E_res',
        #         'PT_res'
        #         ],
        # ranges = [[0,1.1],
        #           [0,1.6],
        #           [0,1.1],
        #           [0,1.1],
        #           [0,5000],
        #           [0,1.1],
        #           [-20,500],
        #           [-5,5 ],
        #           [-5,5 ],
        #           [-5,5 ]])

