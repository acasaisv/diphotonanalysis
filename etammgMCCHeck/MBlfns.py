#-- GAUDI jobOptions generated on Wed Jun 22 15:44:11 2022
#-- Contains event types :
#--   30000000 - 20 files - 65028 events - 7.82 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18'

#--  StepId : 137780
#--  StepName : Digi14c for 2015 - 25ns spillover
#--  ApplicationName : Boole
#--  ApplicationVersion : v30r4
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/Boole/EnableSpillover.py;$APPCONFIGOPTS/Boole/DataType-2015.py;$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py
#--  DDDB : fromPreviousStep
#--  CONDDB : fromPreviousStep
#--  ExtraPackages : AppConfig.v3r374
#--  Visible : N

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18'

#--  StepId : 138052
#--  StepName : L0 emulation for 2018 - TCK 0x18a4 - DIGI
#--  ApplicationName : Moore
#--  ApplicationVersion : v28r3p1
#--  OptionFiles : $APPCONFIGOPTS/L0App/L0AppSimProduction.py;$APPCONFIGOPTS/L0App/L0AppTCK-0x18a4.py;$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py;$APPCONFIGOPTS/L0App/DataType-2017.py
#--  DDDB : fromPreviousStep
#--  CONDDB : fromPreviousStep
#--  ExtraPackages : AppConfig.v3r374
#--  Visible : N

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18'

#--  StepId : 137782
#--  StepName : TCK-0x517a18a4 (HLT1) Flagged for 2018 - DIGI
#--  ApplicationName : Moore
#--  ApplicationVersion : v28r3p1
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x517a18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py
#--  DDDB : fromPreviousStep
#--  CONDDB : fromPreviousStep
#--  ExtraPackages : AppConfig.v3r374
#--  Visible : N

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18'

#--  StepId : 137783
#--  StepName : TCK-0x0x617d18a4 (HLT2) Flagged for 2018 - DIGI
#--  ApplicationName : Moore
#--  ApplicationVersion : v28r3p1
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py;$APPCONFIGOPTS/Conditions/TCK-0x617d18a4.py;$APPCONFIGOPTS/Moore/DataType-2017.py;$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py
#--  DDDB : fromPreviousStep
#--  CONDDB : fromPreviousStep
#--  ExtraPackages : AppConfig.v3r374
#--  Visible : Y

#--  Processing Pass: '/Sim09k/Trig0x617d18a4/Reco18'

#--  StepId : 138796
#--  StepName : Reco18 for MC 2018 - DST
#--  ApplicationName : Brunel
#--  ApplicationVersion : v54r2
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2018.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py
#--  DDDB : fromPreviousStep
#--  CONDDB : fromPreviousStep
#--  ExtraPackages : AppConfig.v3r374;Det/SQLDDDB.v7r10
#--  Visible : Y

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000045_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000021_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000013_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000006_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000001_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000034_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000037_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000005_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000009_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000026_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000028_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000025_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000007_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000020_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000041_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000004_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000022_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000016_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000010_5.dst',
    'LFN:/lhcb/MC/2018/DST/00127194/0000/00127194_00000017_5.dst',
    ], clear=True)
