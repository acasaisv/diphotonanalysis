from hep_ml.reweight import BinsReweighter, GBReweighter
import mplhep
import matplotlib.pyplot as plt
mplhep.style.use("LHCb2")
import pandas as pd
import plot_helpers
import numpy as np

def reweight_mc_df(df,dfMC):
    
    vars = ['eta_PT',
            'eta_ETA',
            'nSPDHits',
            'dimuon_M'
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    #reweighter = BinsReweighter(n_bins=100, n_neighs=3)
    reweighter.fit(original=dfMC[vars], target=df[vars], target_weight=df['sweights'])
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return
def dimuon_mass(df):
    df['dimuon_M2'] = np.zeros(len(df))
    for c in 'X','Y','Z':
        df['dimuon_M2'] -= df.eval(f'(mu1_P{c}+mu2_P{c})**2')
    df['dimuon_M2'] += df.eval(f'(mu1_PE+mu2_PE)**2')
    df['dimuon_M'] = np.sqrt(df['dimuon_M2'])
    return df
def plot_kin(data,mc,name):
    fig,ax= plt.subplots(1,3,figsize=(20,9))
    ax[0].hist(data['eta_PT'],bins=50,color='red',histtype='step',density=True,label='Data',weights=data['sweights'])
    ax[0].hist(mc['eta_PT'],bins=50,color='blue',histtype='step',density=True,label='MC',weights=mc['sweights'])
    ax[0].set_xlabel(r"$p_T$ [MeV]")
    ax[0].set_ylabel(rf"A.U.")

    ax[1].hist(data['eta_ETA'],bins=50,color='red',histtype='step',density=True,label='Data',weights=data['sweights'])
    ax[1].hist(mc['eta_ETA'],bins=50,color='blue',histtype='step',density=True,label='MC',weights=mc['sweights'])
    ax[1].set_xlabel(r"$\eta$")
    ax[1].set_ylabel(rf"A.U.")

    ax[2].hist(data['dimuon_M'],bins=50,color='red',histtype='step',density=True,label='Data',weights=data['sweights'])
    ax[2].hist(mc['dimuon_M'],bins=50,color='blue',histtype='step',density=True,label='MC',weights=mc['sweights'])
    ax[2].set_xlabel(r"$M(\mu^+\mu^-)$")
    ax[2].set_ylabel(rf"A.U.")


    ax[0].legend()

    fig.savefig(name)




if __name__=='__main__':
    data =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalData.h5',key='df')
    mc =pd.read_hdf('/scratch47/adrian.casais/ntuples/turcal/etammgTurcalHardPhotonMC.h5',key='df')
    mc.query('nSPDHits<450',inplace=True)
    data.query('nSPDHits<450',inplace=True)
    mc = dimuon_mass(mc)
    data = dimuon_mass(data)
    plot_kin(data,mc,'kin-before-reweight.pdf')
    reweight_mc_df(data,mc)
    plot_kin(data,mc,'kin-after-reweight.pdf') 
