import numpy as np
import scipy
####IMPORT DATA
import uproot3 as uproot
import numpy as np
import pandas
import vector
from ROOT import TFile,TTree,Math
import sys
import matplotlib.pyplot as plt

root = '/scratch47/adrian.casais/ntuples/turcal'

    
#f_s = uproot.open(root + '/etammgHardPhotonMC-all.root')
f_s = uproot.open(root + '/etammgHardPhotonMC-.root')
t_s = f_s['MCDecayTreeTuple/MCDecayTree']
vars = ['_M',
        '_TRUEP_X',
        '_TRUEP_Y',
        '_TRUEP_Z',
        '_TRUEP_E']
pars = ['eta','muplus','muminus','gamma']
variables = []
for par in pars:
    for var in vars:
        variables.append(par+var)
df = t_s.pandas.df(branches= variables)
df.eval('m2mumu = (muplus_TRUEP_E+muminus_TRUEP_E)**2 - (muplus_TRUEP_X+muminus_TRUEP_X)**2 - (muplus_TRUEP_Y+muminus_TRUEP_Y)**2 - (muplus_TRUEP_Z+muminus_TRUEP_Z)**2',inplace=True)
df.eval('m2mugamma = (muplus_TRUEP_E+gamma_TRUEP_E)**2 - (muplus_TRUEP_X+gamma_TRUEP_X)**2 - (muplus_TRUEP_Y+gamma_TRUEP_Y)**2 - (muplus_TRUEP_Z+gamma_TRUEP_Z)**2',inplace=True)
plt.hist2d(df['m2mumu'],df['m2mugamma'],bins=500)
plt.show()
