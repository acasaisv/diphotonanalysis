from Configurables import DaVinci
import GaudiPython
from Gaudi.Configuration import *
import numpy as np
DaVinci().EvtMax = 0
DaVinci().DataType = "2018"
DaVinci().Simulation = True
## These are for data tags (magnet up or down?)
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20190430-vc-md100"
importOptions("MBlfns.py")
importOptions("pool_xml_catalog.py")
gaudi = GaudiPython.AppMgr()
gaudi.initialize()
TES = gaudi.evtsvc()
n_evs = int(1e4)

bigger_pt = []
rand_pt = []
above_thres = []
threshold = 2500
for i in range(n_evs):
    gaudi.run(1)
    mcpars = TES['MC/Particles']
    etas = list(filter(lambda x: abs(x.particleID().pid()) == 221, mcpars))
    
    pts = list(map(lambda x: x.momentum().pt(),etas))

    if not len(etas): continue
    bigger_pt += [max(pts)]
    rand_pt += [np.random.choice(pts,1)[0]]
    
    harder_etas = list(filter(lambda x: x.momentum().pt() > threshold, etas))
    if not len(harder_etas): continue

    pts_harder=list(map(lambda x: x.momentum().pt(),harder_etas))
    above_thres += [np.random.choice(pts_harder,1)[0]]
    

import pickle
with open("pts.pickle",'wb') as file:
    myarray = np.array([rand_pt,bigger_pt])
    np.save(file,myarray)
with open("above_thres.pickle",'wb') as file:
    myarray = np.array(above_thres)
    np.save(file,myarray)

    pickle.dump(np.array([rand_pt,bigger_pt,above_thres]),file)
