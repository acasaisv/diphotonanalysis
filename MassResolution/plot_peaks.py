from helpers import get_ALPsdf, masses_map, sim10_masses_map
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import crystalball as cb
from plot_helpers import *
from energy_helpers import *
from fit_helpers import *

                                    
def plot_offsets(ax,offset,center_masses,label,color='red'):
    x = []
    y = []
    for key in offset.keys():
        x.append(center_masses[key])
        y.append(offset[key])

    x = np.array(x)
    y=np.array(y)
    ax.plot(x,y,marker='o',linestyle='None',color=color,label=label)
    slo,ord = np.polyfit(x,y,1)
    ax.plot(x,x*slo + ord,color=color,linestyle='dashed',label=r'$\sigma$ = {0:.4f}$m$ + {1:.4f}'.format(slo,ord))



def sadf(df,dfbs):
    #plt.hist(df.eval('(gamma_PT - gamma_TRUEPT)/gamma_TRUEPT'),bins=50,histtype='step',density=True,label= 'alp')
    #plt.hist(dfbs.eval('(gamma_PT - gamma_TRUEPT)/gamma_TRUEPT'),bins=50,histtype='step',density=True,label='bs')
    plt.hist(df.eval('(gamma_PT - gamma_L0Calo_ECAL_TriggerET)/gamma_PT'),bins=50,histtype='step',density=True,label='bs')
    plt.legend()
    plt.show()

def res_fit(df,range=(-0.15,0.15)):
    import zfit
    obs = zfit.Space('reso',range)
    #BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.01,0.001,1),nevs=len(df)+100,extended=True)
    BsCBExtended,BsParameters = create_double_cb(name_prefix='resolution_',scale_mu =False,mass_range=obs,mass=0,sigma=(0.001,0.0001,1),nevs=len(df)+100,extended=True)
    model_Bs=BsCBExtended
    df.eval('gamma_resolution = (-gamma_P + gamma_TRUEP_E)/gamma_TRUEP_E',inplace=True)
    #df.eval('gamma_resolution = (-gamma_PT + gamma_TRUEPT)/gamma_TRUEPT',inplace=True)
    #df.eval('gamma_resolution = (gamma_PT - gamma_L0Calo_ECAL_TriggerET)/gamma_PT',inplace=True)
    df.query(f'gamma_resolution > {range[0]} and gamma_resolution < {range[1]}')

    data = zfit.Data.from_numpy(array=df['gamma_resolution'].to_numpy(),obs=obs)

    nll = zfit.loss.ExtendedUnbinnedNLL(model =BsCBExtended,data=data)

    minimizer = zfit.minimize.Minuit(tol=1e-3,mode=1,gradient=True,
    maxiter=800000000
    )
    result = minimizer.minimize(nll)
    result.hesse()
    #result.error()
    print(result)
    #plot= True
    import plot_helpers
    from importlib import reload
    reload(plot_helpers)
    nSig=zfit.run(BsParameters['nSig'])
    plot_helpers.plot_fit(mass=df['gamma_resolution'],
                        full_model=BsCBExtended,
                        components=[BsCBExtended],
                        yields=[nSig],
                        labels=[r'$B_s$ signal double Crystal Ball'],
                        colors=['blue'],
                        nbins=30,
                        myrange=range,
                        xlabel=r'Resolution',
                        savefile=f'/home3/adrian.casais/figstoscp/resolution.pdf',
                        plot=True,
                        pull_plot=True)


def plot_all_peaks(sat=False):
    df = {}
    sim10root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    fig, ax = plt.subplots(1,1)
    ax = np.array(ax)
    ax = ax.flatten()
    ax[0].set_xlim(4500,21500)
    ax[0].set_xlabel(r"$M(\gamma \gamma)$ [GeV]",horizontalalignment='right',x=1.0)
    ax[0].set_ylabel(r"AU",loc='top')
    for key in range(40,51):
        df[key] = get_ALPsdf(sim10root+f'491000{key}_1000-1099_Sim10a-priv.root',bs=False,background=False,extravars=['gamma_CaloHypo_Saturation','gamma0_CaloHypo_Saturation'],sample=int(10e4))
        if sat:
            df[key].query('gamma_CaloHypo_Saturation ==0  and gamma0_CaloHypo_Saturation==0',inplace=True)
        ax[0].hist(df[key]['B_s0_M'],bins=50,histtype='step',density=True)
    fig.savefig(f"./peaks-sat={sat}.pdf")


if __name__ == '__main__':
    #plot_all_peaks(False)
    #plot_all_peaks(True)
    #quit()
    masses_map = sim10_masses_map
    df = {}
    offset = {}
    offset_smear = {}
    center_masses = {}
    for key in masses_map.keys():
    #for key in [41]:
        
        #center_masses[key]= 1000.*float("".join(list(filter(lambda x: x.isdigit(),masses_map[key]))))
        center_masses[key] = masses_map[key]*1000.
    center_masses['bs']= 5366

    #fig,ax = plt.subplots(1,3)
    fig, ax = plt.subplots(1,1)
    ax = np.array(ax)
    ax = ax.flatten()
    low = 4800
    high = 11000
    ax[0].set_xlim(low,high)
    #ax[0].set_xlim(-0.1,1)
    sim10root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
    #for key in sim10_masses_map.keys():
    #for key in range(40,51):
    for key in [44]:
        df[key] = get_ALPsdf(sim10root+f'491000{key}_1000-1099_Sim10a-priv.root',bs=False,background=False,extravars=['gamma_CaloHypo_Saturation','gamma0_CaloHypo_Saturation'],sample=int(10e4))
        df[key].query('gamma_CaloHypo_Saturation==0 & gamma0_CaloHypo_Saturation==0',inplace=True)
        #res_fit(df[key],range=(-.01,0.3))
        #res_fit(df[key].query('gamma_L0Calo_ECAL_TriggerET>00 and gamma_PT < 6000'),range=(-.01,0.3))
        #res_fit(df[key].query('gamma_L0Calo_ECAL_TriggerET>00 and gamma_PT < 6000'),range=(-0.15,0.11))
        b = 0.0335
        mult = 1.003
        df[key]['gamma_smearP'] = smear(df[key]['gamma_TRUEP_E'],b,mult=mult)
        df[key]['gamma0_smearP'] = smear(df[key]['gamma0_TRUEP_E'],b,mult=mult)
        
        df[key].eval('cosgamma = (gamma_PX*gamma0_PX +gamma0_PZ*gamma_PZ + gamma_PY*gamma0_PY)/gamma_P/gamma0_P',inplace=True)
        df[key].eval('B_s0_M_smear = sqrt(2*gamma_smearP*gamma0_smearP*(1-cosgamma))',inplace=True)
        
        ax[0].hist(df[key]['B_s0_M'],bins=500,label='Reconstruction',histtype='step',density=True,color='blue',range=(low,high))
        ax[0].hist(df[key]['B_s0_M_smear'],bins=500,histtype='step',density=True,label="Smearing".format(masses_map[key]),color='red',range=(low,high))
        ax[0].set_xlabel(r"$M(\gamma \gamma)$ [MeV]",horizontalalignment='right',x=1.0)
        ax[0].set_ylabel(r"AU",loc='top')
        offset[key] = np.mean(df[key]['B_s0_M']-center_masses[key])
        if key == 40:
            ax[0].legend()
        #offset_smear[key] = np.mean(df[key]['B_s0_M_smear']-center_masses[key])
        

        
        
    #plot_offsets(ax[1],offset_smear,center_masses,color='red',label='Model offset')
    #plot_offsets(ax[1],offset,center_masses,color='blue',label='Full-reco offset')
    #key = 'bs'
    #ax[2].hist(df[key]['gamma_P'],bins=50,histtype='step',label='Full reco',density=True)
    #ax[2].hist(df[key]['gamma_smearP'],bins=50,histtype='step',label='smear',density=True)
    #ax[2].hist(df[key]['gamma_TRUEP_E'],bins=50,histtype='step',label='truth',density=True)
    #ax[0].legend()
    #ax[1].legend()
    #ax[2].legend()
    #ax[0].legend()
    #plt.legend()
    fig.savefig("./real_v_smear.pdf")
    #plt.show()
