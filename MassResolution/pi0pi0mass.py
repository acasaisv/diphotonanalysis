import uproot
import pandas as pdf
from energy_helpers import smear
import matplotlib.pyplot as plt
import numpy as np


if __name__=='__main__':

    root = '/home3/adrian.casais/diphotonanalysis/truth_studies/DVntuple.root'
    f = uproot.open(f'{root}:MCDecayTreeTuple/MCDecayTree')


    keys = ['pi0_TRUEP_E', 'pi0_TRUEP_X', 'pi0_TRUEP_Y', 'pi0_TRUEP_Z','pi00_TRUEP_E', 'pi00_TRUEP_X', 'pi00_TRUEP_Y', 'pi00_TRUEP_Z']
    df = f.arrays(keys,library='pd')
    df.eval('pi0_P = sqrt(pi0_TRUEP_X**2 + pi0_TRUEP_Y**2 + pi0_TRUEP_Z**2)',inplace=True)
    df.eval('pi00_P = sqrt(pi00_TRUEP_X**2 + pi00_TRUEP_Y**2 + pi00_TRUEP_Z**2)',inplace=True)
    df['pi0_smearP'] = smear(df['pi0_P'])
    df['pi00_smearP'] = smear(df['pi00_P'])

    df.eval('pi0_smearE = sqrt(134.98**2 + pi0_smearP**2)',inplace=True)
    
    df.eval('pi00_smearE = sqrt(134.98**2 + pi00_smearP**2)',inplace=True) 

    df.eval('pi0_smearEk = sqrt(493**2 + pi0_smearP**2)',inplace=True)
    
    df.eval('pi00_smearEk = sqrt(493**2 + pi00_smearP**2)',inplace=True) 
    
    df.eval('cospi0 = (pi0_TRUEP_X*pi00_TRUEP_X +pi00_TRUEP_Z*pi0_TRUEP_Z + pi0_TRUEP_Y*pi00_TRUEP_Y)/pi0_P/pi00_P',inplace=True)
    df.eval('B0_M_smear_gg = sqrt(2*pi0_smearP*pi00_smearP*(1-cospi0))',inplace=True)
    df.eval('B0_M_smear = sqrt(134.98**2 + 134.98**2 +2*(pi0_smearE*pi00_smearE - cospi0*pi0_smearP*pi00_smearP))',inplace=True)
    df.eval('B0_M_smeark = sqrt(493**2 + 493**2 +2*(pi0_smearEk*pi00_smearEk - cospi0*pi0_smearP*pi00_smearP))',inplace=True)
    plt.hist(df['B0_M_smear_gg'],bins=50,histtype='step',color='red',density=True,label='gg mass hypo',range=[4000,6000])
    plt.hist(df['B0_M_smear'],bins=50,histtype='step',color='blue',density=True,label='pi0pi0 mass hypo',range=[4000,6000])
    plt.hist(df['B0_M_smeark'],bins=50,histtype='step',color='green',density=True,label='pi0pi0 mass hypo',range=[4000,6000])
    plt.legend()
    print(f"Mean mass pi0pi0 = {np.mean(df['B0_M_smear'])}")
    print(f"Mean mass gg = {np.mean(df['B0_M_smear_gg'])}")
    plt.show()


