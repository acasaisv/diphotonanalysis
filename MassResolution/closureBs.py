#AS IT IS RIGHT NOW WORKS FOR PHOTONS UP TO 100 GeV of energy
import uproot3 as uproot
import numpy as np
import matplotlib.pyplot as plt
import math as m

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'

#f_s = uproot.open(root + '/ALPs-5GeV-withcones.root')
f_s = uproot.open(root + '/13100212.root')
#f_s = uproot.open(root + '/hardPhoton-withcones.root')

t_s = f_s['B2GammaGammaTuple/DecayTree']
#t_s = f_b['B2GammaGammaTuple/DecayTree']

keys = [ i.decode('utf-8') for i in t_s.keys() ]

variables = []


for par in ['gamma','gamma0','B_s0']:
    variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
    variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
    
variables += ['gamma_MC_MOTHER_ID','gamma0_MC_MOTHER_ID','gamma_TRUEID','gamma0_TRUEID','B_s0_M','B_s0_MM']

mc ='gamma_MC_MOTHER_ID==54 and gamma0_MC_MOTHER_ID==54 and  gamma_TRUEID==22 and gamma0_TRUEID==22'
mc ='gamma_MC_MOTHER_ID==531 and gamma0_MC_MOTHER_ID==531 and  gamma_TRUEID==22 and gamma0_TRUEID==22'
#mc = 'gamma_TRUEP_Z == gamma_TRUEP_Z'
#############################
df = t_s.pandas.df(branches=variables)
df = df.query(mc)
df.eval('gamma_TRUEP = sqrt(gamma_TRUEP_Z**2 +gamma_TRUEPT**2)',inplace=True)
df.eval('gamma0_TRUEP = sqrt(gamma0_TRUEP_Z**2 +gamma0_TRUEPT**2)',inplace=True)
df.eval('true_angle = (gamma_TRUEP_X*gamma0_TRUEP_X + gamma_TRUEP_Y*gamma0_TRUEP_Y + gamma_TRUEP_Z*gamma0_TRUEP_Z)/(gamma_TRUEP*gamma0_TRUEP) ',inplace = True)
df.eval('angle =(gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_P*gamma0_P) ',inplace=True)
df['angle'] = np.arccos(df['angle'])
df['true_angle'] = np.arccos(df['true_angle'])
df.eval('res_angle = (angle-true_angle)/true_angle',inplace=True)

factor_resolution = 1
df.eval('res = {}*(gamma_TRUEP - gamma_P)/(gamma_TRUEP)'.format(factor_resolution),inplace=True)
df.eval('res0 = {}*(gamma0_TRUEP - gamma0_P)/(gamma0_TRUEP)'.format(factor_resolution),inplace=True)
df = df.query('res > -0.25 and res < 0.25')
df = df.query('res0 > -0.25 and res0 < 0.25')

##CONSTRUCT SMEARED ENERGY
def sample_resolution(df,res_variable,n_bins):
    if '0' in res_variable:
        res_variable1 = res_variable.replace('0','')
    else:
        res_variable1 = res_variable+'0'
    
    if res_variable1 in  df.keys():
        H,X = np.histogram(np.concatenate((df[res_variable],df[res_variable1])),bins=2*n_bins,density = True)
    else:
        H,X = np.histogram(df[res_variable],bins=n_bins,density = True)
    
    dx = X[1] - X[0]
    X = (X[1:] + X[:-1])/2 #bin centers instead of edges
    cummulative= np.cumsum(H)*dx
    random_seed = np.random.random_sample(len(df))
    
    df['{}_sampled'.format(res_variable)] = X[np.digitize(random_seed,cummulative)]




def histedges_equalN(x, nbin):
        npt = len(x)
        return np.interp(np.linspace(0, npt, nbin + 1),
                         np.arange(npt),
                         np.sort(x))

def histedges_equalA(x, nbin):
    pow = 0.5
    dx = np.diff(np.sort(x))
    tmp = np.cumsum(dx ** pow)
    tmp = np.pad(tmp, (1, 0), 'constant')
    return np.interp(np.linspace(0, tmp.max(), nbin + 1),
                     tmp,
                     np.sort(x))
                    

max_E = 100e3
min_E= 6000.

n_bins = 10
ratio_bins_sample= 10
width = max_E/n_bins
factor_res=1

#offset_res=(1+(factor_res-1)/2)/10
#offset_res = 0.095
offset_res = -0.011
#offset_res = 0

fig = plt.figure()
res_hists = []
for i in ('','0'):
    df_ = df.query('gamma{0}_TRUEP_E < {1} & gamma{0}_TRUEP_E > {2}'.format(i,max_E,min_E))
    E_bins =np.linspace(min_E,max_E,n_bins)
    #E_bins =histedges_equalA(df_['gamma{}_TRUEP_E'.format(i)],n_bins-1)
    for j in range(n_bins-1):
        E1 = E_bins[j]
        E2 = E_bins[j+1]
        
        _df = df_.query('gamma{0}_TRUEP_E > {1} & gamma{0}_TRUEP_E < {2}'.format(i,E1,E2))
        if not len(_df): continue
        n_bins_sample = m.floor((len(_df)/ratio_bins_sample))
        
        #print("N_bins_sample: {}".format(n_bins_sample))
        
        sample_resolution(_df,'res{}'.format(i),n_bins_sample)
        df.loc[_df.index,'res{}_sampled'.format(i)] = factor_res*_df['res{}_sampled'.format(i)]+offset_res #offset to account for res centered not in 0

        #####
        if i=='0':
            ax = fig.add_subplot(2,5,j+1)
            #res_hists.append(plt.hist(df['res{}_sampled'.format(i)],bins=n_bins_sample))
            plt.hist(df['res{}_sampled'.format(i)],bins=n_bins_sample)
        ####

        
        sample_resolution(_df,'res_angle',n_bins_sample)
        df.loc[_df.index,'res_angle_sampled'] = _df['res_angle_sampled']
        #E = E+width

#plt.show()
for gamma in ('','0'):
    df.eval('gamma{0}_PT_smeared = (1+res{0}_sampled)*gamma{0}_TRUEPT'.format(gamma),inplace=True)
    df.eval('gamma{0}_P_smeared =  (1+res{0}_sampled)*gamma{0}_TRUEP'.format(gamma),inplace=True)
    df.eval('gamma{0}_PE_smeared =  (1+res{0}_sampled)*gamma{0}_TRUEP_E'.format(gamma),inplace=True)

    
df.eval('angle_smeared = (1+res_angle_sampled)*true_angle',inplace=True)
df.eval('B_s0_M_smeared = sqrt( 2*gamma_PE_smeared*gamma0_PE_smeared*( 1-cos(angle_smeared)) )',inplace=True)
df.eval('B_s0_M_smeared = sqrt( 2*gamma_PE_smeared*gamma0_PE_smeared*( 1-cos(angle_smeared)) )',inplace=True)
df.eval('B_s0_M_recal = sqrt( 2*gamma_PE*gamma0_PE*(1 - cos(angle)))',inplace=True)
df.query('(gamma_TRUEP_E < {0} & gamma_TRUEP_E > {1}) | (gamma0_TRUEP_E < {0} & gamma0_TRUEP_E > {1})'.format(max_E,min_E))
plt.figure()
plt.hist(df['B_s0_M_smeared'],bins=50,alpha=0.5,density=True)
plt.hist(df['B_s0_MM'],bins=50,alpha =0.5,density=True)
plt.hist(df['B_s0_M_recal'],bins=50,alpha =0.5,density=True)

plt.show()

