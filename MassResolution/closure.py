#AS IT IS RIGHT NOW WORKS FOR PHOTONS UP TO 100 GeV of energy
import uproot3 as uproot
import numpy as np
import matplotlib.pyplot as plt

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'

f_s = uproot.open(root + '/ALPs-5GeV-withcones.root')
f_s = uproot.open(root + '/13100212.root')
f_b = uproot.open(root + '/hardPhoton-withcones.root')

t_s = f_s['B2GammaGammaTuple/DecayTree']
t_b = f_b['B2GammaGammaTuple/DecayTree']

keys = [ i.decode('utf-8') for i in t_s.keys() ]

variables = []


for par in ['gamma','gamma0','B_s0']:
    variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
    variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
    
variables += ['gamma_MC_MOTHER_ID','gamma0_MC_MOTHER_ID','gamma_TRUEID','gamma0_TRUEID','B_s0_M','B_s0_MM']

mc ='gamma_MC_MOTHER_ID==54 and gamma0_MC_MOTHER_ID==54 and  gamma_TRUEID==22 and gamma0_TRUEID==22'
mc = 'gamma_MC_MOTHER_ID==531 and gamma0_MC_MOTHER_ID==531 and  gamma_TRUEID==22 and gamma0_TRUEID==22'

df = t_s.pandas.df(branches=variables)
df = df.query(mc)
df.eval('gamma_TRUEP = sqrt(gamma_TRUEP_Z**2 +gamma_TRUEPT**2)',inplace=True)
df.eval('gamma0_TRUEP = sqrt(gamma0_TRUEP_Z**2 +gamma0_TRUEPT**2)',inplace=True)
df.eval('true_angle = (gamma_TRUEP_X*gamma0_TRUEP_X + gamma_TRUEP_Y*gamma0_TRUEP_Y + gamma_TRUEP_Z*gamma0_TRUEP_Z)/(gamma_TRUEP*gamma0_TRUEP) ',inplace = True)
df.eval('angle =(gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_P*gamma0_P) ',inplace=True)
df['angle'] = np.arccos(df['angle'])
df['true_angle'] = np.arccos(df['true_angle'])
df.eval('res_angle = (angle-true_angle)/true_angle',inplace=True)


df.eval('res = (gamma_TRUEP - gamma_P)/(gamma_TRUEP)',inplace=True)
df.eval('res0 = (gamma0_TRUEP - gamma0_P)/(gamma0_TRUEP)',inplace=True)
df = df.query('res > -0.25 and res < 0.25')
df = df.query('res0 > -0.25 and res0 < 0.25')

##CONSTRUCT SMEARED ENERGY
def sample_resolution(df,res_variable,n_bins):
    H,X = np.histogram(df[res_variable],bins=n_bins,density = True)
    dx = X[1] - X[0]
    X = (X[1:] + X[:-1])/2 #bin centers instead of edges
    cummulative= np.cumsum(H)*dx
    random_seed = np.random.random_sample(len(df))
    df['{}_sampled'.format(res_variable)] = X[np.digitize(random_seed,cummulative)]

max_E = 100.e3
n_bins = 3.
#Df = Df.Query('gamma0_TRUEP_E < 200e3 & gamma_TRUEP_E < {}'.format(max_E))

width = max_E/n_bins
E = 0
n_bins_sample = 50
while E < max_E:
    _df = df.query('(gamma0_TRUEP_E > {0} & gamma_TRUEP_E > {0}) & (gamma_TRUEP_E<{1} & gamma_TRUEP_E < {1})'.format(E,E+width))
    for i in ('','0'):
        sample_resolution(_df,'res{}'.format(i),n_bins_sample)
        df.loc[_df.index,'res{}_sampled'.format(i)] = _df['res{}_sampled'.format(i)]+0.1
    sample_resolution(_df,'res_angle',n_bins_sample)
    df.loc[_df.index,'res_angle_sampled'] = _df['res_angle_sampled']
    E = E+width


for gamma in ('','0'):
    for c in ('X','Y','Z'):
        df.eval('gamma{0}_P{1}_smeared = (1+res{0}_sampled)*gamma{0}_TRUEP_{1}'.format(gamma,c),inplace=True)
    df.eval('gamma{0}_PT_smeared = sqrt(gamma{0}_PX_smeared**2 + gamma{0}_PY_smeared**2)'.format(gamma),inplace=True)
    df.eval('gamma{0}_P_smeared = sqrt(gamma{0}_PZ_smeared**2 + gamma{0}_PT_smeared**2)'.format(gamma),inplace=True)

    
df.eval('angle_smeared = (1+res_angle_sampled)*true_angle',inplace=True)
df.eval('B_s0_M_smeared = sqrt( 2*gamma_P_smeared*gamma0_P_smeared*( 1-cos(angle)) )',inplace=True)

plt.hist(df.query('gamma0_TRUEP_E < {0} and gamma_TRUEP_E < {0}'.format(max_E))['B_s0_M_smeared'],bins=50,alpha=0.5)
plt.hist(df.query('gamma0_TRUEP_E < {0} and gamma_TRUEP_E < {0}'.format(max_E))['B_s0_MM'],bins=50,alpha =0.5)
plt.show()
# probs = X[np.digitize(np.random.random_sample(15000),cummulative)]
# plt.hist(probs,bins=50,density=True)
# plt.show()

# def histedges_equalN(x, nbin):
#     pow = 0.5
#     dx = np.diff(np.sort(x))
#     tmp = np.cumsum(dx ** pow)
#     tmp = np.pad(tmp, (1, 0), 'constant')
#     return np.interp(np.linspace(0, tmp.max(), nbin + 1),
#                      tmp,
#                      np.sort(x))

# x = df.query('gamma0_TRUEP_E < 250000')['gamma0_TRUEP_E']

# n, bins, patches = plt.hist(
#     x,
#     histedges_equalN(x, 20)
#     #20
#     )
# plt.show()
