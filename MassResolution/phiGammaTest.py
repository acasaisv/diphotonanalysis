
import uproot3 as uproot
import numpy as np
import matplotlib.pyplot as plt
import math as m

root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
f_s = uproot.open(root + '/phiGammaMC.root')
t_s = f_s['DTTBs2phig/DecayTree']
root = '/scratch47/adrian.casais/ntuples/turcal/'

f_s = uproot.open(root+'/b2phigammaMC.root')
t_s = f_s['DecayTree/DecayTree']


#t_b = f_b['B2GammaGammaTuple/DecayTree']

keys = [ i.decode('utf-8') for i in t_s.keys() ]
#keys = [  for i in t_s.keys() ]
variables = []


for par in ['gamma','phi_1020']:
    variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
    variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
    
variables += ['gamma_MC_MOTHER_ID',
              'gamma_TRUEID',
              'phi_1020_TRUEID',
              'Kplus_MC_MOTHER_ID',
              'Kminus_MC_MOTHER_ID',
              'Kplus_TRUEID',
              'Kminus_TRUEID',
              'B_s0_M',
              'B_s0_MM',
              'B_s0_PT',
              #'B_s0_ENDVERTEX_X',
              #'B_s0_ENDVERTEX_Y',
              #'B_s0_ENDVERTEX_Z',
              'phi_1020_M',
              'phi_1020_MM',
              'phi_1020_OWNPV_CHI2',
              'Kplus_IPCHI2_OWNPV',
              'Kminus_IPCHI2_OWNPV',
              'Kminus_PT',
              'Kplus_PT',
              'Kminus_PIDK',
              'Kplus_PIDK',
              'gamma_CL',
              'B_s0_L0ElectronDecision_TOS',
              'B_s0_L0PhotonDecision_TOS',
              'B_s0_Hlt1B2PhiGamma_LTUNBDecision_TOS',
              'B_s0_Hlt2RadiativeBs2PhiGammaUnbiasedDecision_TOS']

mc ='abs(gamma_MC_MOTHER_ID)==531  and  gamma_TRUEID==22 and phi_1020_TRUEID==333 and Kplus_TRUEID == 321 and Kminus_TRUEID==-321'

#############################
df = t_s.pandas.df(branches=variables)
df = df.query(mc)
df.eval('gamma_TRUEP = sqrt(gamma_TRUEP_Z**2 +gamma_TRUEPT**2)',inplace=True)
df.eval('phi_1020_TRUEP = sqrt(phi_1020_TRUEP_Z**2 +phi_1020_TRUEPT**2)',inplace=True)
df.eval('true_angle = (gamma_TRUEP_X*phi_1020_TRUEP_X + gamma_TRUEP_Y*phi_1020_TRUEP_Y + gamma_TRUEP_Z*phi_1020_TRUEP_Z)/(gamma_TRUEP*phi_1020_TRUEP) ',inplace = True)
df.eval('angle =(gamma_PX*phi_1020_PX + gamma_PY*phi_1020_PY + gamma_PZ*phi_1020_PZ)/(gamma_P*phi_1020_P) ',inplace=True)
df['angle'] = np.arccos(df['angle'])
df['true_angle'] = np.arccos(df['true_angle'])
df.eval('res_angle = (angle-true_angle)/true_angle',inplace=True)

factor_resolution = 1
df.eval('res = {}*(gamma_TRUEP - gamma_P)/(gamma_TRUEP)'.format(factor_resolution),inplace=True)



##CONSTRUCT SMEARED ENERGY
def sample_resolution(df,res_variable,n_bins):

    H,X = np.histogram(df[res_variable],bins=n_bins,density = True)
    
    dx = X[1] - X[0]
    X = (X[1:] + X[:-1])/2 #bin centers instead of edges
    cummulative= np.cumsum(H)*dx
    random_seed = np.random.random_sample(len(df))
    
    df['{}_sampled'.format(res_variable)] = X[np.digitize(random_seed,cummulative)]




def histedges_equalN(x, nbin):
        npt = len(x)
        return np.interp(np.linspace(0, npt, nbin + 1),
                         np.arange(npt),
                         np.sort(x))

def histedges_equalA(x, nbin):
    pow = 0.5
    dx = np.diff(np.sort(x))
    tmp = np.cumsum(dx ** pow)
    tmp = np.pad(tmp, (1, 0), 'constant')
    return np.interp(np.linspace(0, tmp.max(), nbin + 1),
                     tmp,
                     np.sort(x))
                    

max_E = 100e3
min_E= 6000.

#n_bins = 10
#ratio_bins_sample= 10
n_bins = 10
ratio_bins_sample= 10


width = max_E/n_bins
factor_res=0.90

#offset_res=(1+(factor_res-1)/2)/10
#offset_res = -0.001
offset_res = 0
#offset_res = -0.009

fig = plt.figure()
res_hists = []
for i in ['']:
    df_ = df.query('gamma{0}_TRUEP_E < {1} & gamma{0}_TRUEP_E > {2}'.format(i,max_E,min_E))
    E_bins =np.linspace(min_E,max_E,n_bins)
    #E_bins =histedges_equalA(df_['gamma{}_TRUEP_E'.format(i)],n_bins-1)
    for j in range(n_bins-1):
        E1 = E_bins[j]
        E2 = E_bins[j+1]
        
        _df = df_.query('gamma{0}_TRUEP_E > {1} & gamma{0}_TRUEP_E < {2}'.format(i,E1,E2))
        if not len(_df): continue
        n_bins_sample = m.floor((len(_df)/ratio_bins_sample))
        
        #print("N_bins_sample: {}".format(n_bins_sample))
        
        sample_resolution(_df,'res{}'.format(i),n_bins_sample)
        df.loc[_df.index,'res{}_sampled'.format(i)] = factor_res*_df['res{}_sampled'.format(i)]+offset_res #offset to account for res centered not in 0

        #####
    
        ax = fig.add_subplot(2,5,j+1)
        ax.set_title('{0:.2f} GeV < E <{1:.2f} GeV'.format(E1/1000.,E2/1000.))
        #res_hists.append(plt.hist(df['res{}_sampled'.format(i)],bins=n_bins_sample))
        plt.hist(df['res{}_sampled'.format(i)],bins=n_bins_sample)
        ####

        
        sample_resolution(_df,'res_angle',n_bins_sample)
        df.loc[_df.index,'res_angle_sampled'] = _df['res_angle_sampled']
        #E = E+width

#plt.show()

df.eval('gamma_PT_smeared = (1-res_sampled)*gamma_TRUEPT',inplace=True)
df.eval('gamma_P_smeared =  (1-res_sampled)*gamma_TRUEP',inplace=True)
df.eval('gamma_PE_smeared =  (1-res_sampled)*gamma_TRUEP_E',inplace=True)

for comp in ['X','Y','Z']:
    df.eval('gamma_P{0}_smeared = (1-res_sampled)*gamma_TRUEP_{0}'.format(comp),inplace=True)
df.eval('angle_smeared =(gamma_PX_smeared*phi_1020_PX + gamma_PY_smeared*phi_1020_PY + gamma_PZ_smeared*phi_1020_PZ)/(gamma_P_smeared*phi_1020_P) ',inplace=True)
df['angle_smeared'] = np.arccos(df['angle_smeared'])
#df.eval('angle_smeared = (1+res_angle_sampled)*true_angle',inplace=True)
#df.eval('B_s0_M_smeared = sqrt( 2*gamma_P_smeared*phi_1020_PE*( 1-cos(angle_smeared)) )',inplace=True)
df.eval('B_s0_M_smeared = sqrt((gamma_PE_smeared+phi_1020_PE)**2 - (gamma_PX_smeared+phi_1020_PX)**2 - (gamma_PY_smeared+phi_1020_PY)**2 - (gamma_PZ_smeared+phi_1020_PZ)**2)',inplace=True)
df.eval('B_s0_M_recal = sqrt((gamma_PE+phi_1020_PE)**2 - (gamma_PX+phi_1020_PX)**2 - (gamma_PY+phi_1020_PY)**2 - (gamma_PZ+phi_1020_PZ)**2)',inplace=True)

df.query('(gamma_TRUEP_E < {0} & gamma_TRUEP_E > {1}) '.format(max_E,min_E),inplace=True)
fig1 = plt.figure()
ax = fig1.add_subplot(1, 1, 1)
#ax.set_yscale('log')
plt.hist(df['B_s0_M_smeared'],bins=50,alpha=0.3,density=True,label='M_smeared')
#plt.hist(df['B_s0_M'],bins=50,alpha =0.3,density=True,label='M')
plt.hist(df['B_s0_M_recal'],bins=50,alpha =0.3,density=True,label='M')
# plt.legend()
#plt.show()

#np.save('PhiGammaSmearedMass-1.0',df['B_s0_M_smeared'].values)
#df = df.dropna()
df.reset_index(inplace=True)
df.to_hdf(root+'/PhiGammaMassSmeared.h5',key='df',mode='w',
          #format='table',
          dropna=True)
