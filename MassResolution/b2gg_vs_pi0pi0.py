import os
from ROOT import *
import uproot3 as uproot
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import subprocess
from helpers import pTs,etas,pTsHlt2,etasHlt2,spds,spdsHlt2,get_ALPsdf,trigger_cuts,sim10_masses_map,get_error,get_error_w,ufloat_to_str, bins, get_ALPsdf_sim10b
from uncertainties import ufloat
from plot_helpers import *
#pTs,etas,spds = pTsHlt2,etasHlt2,spdsHlt2
import pickle
from plot_helpers import *

if __name__=='__main__':
    cut = f'{trigger_cuts["L0"]} & {trigger_cuts["HLT1"]} & {trigger_cuts["HLT2"]} and (gamma_PT > 3000 & gamma0_PT > 3000) and gamma_P > 6000 and gamma0_P > 6000 and B_s0_PT > 2000 and gamma_PP_Saturation<1 and gamma0_PP_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and B_s0_M >4800 and B_s0_M<20000'
    myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2gg-stripping.root'
    gg = get_ALPsdf_sim10b(myfile,extracut=cut,background=True,bs=True,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH','gamma0_PP_IsPhoton','gamma_PP_IsPhoton','gamma_PP_CaloNeutralID'])
    myfile = '/scratch47/adrian.casais/ntuples/signal/sim10/b2pi0pi0.root'
    pi0pi0 =get_ALPsdf_sim10b(myfile,extracut=cut,background=True,bs=True,extravars=['gamma_ShowerShape','gamma0_ShowerShape','gamma_PP_IsNotH','gamma0_PP_IsNotH','gamma0_PP_IsPhoton','gamma_PP_IsPhoton','gamma_PP_CaloNeutralID'])

    myrange = (4800,6100)
    fig,ax = plt.subplots(1,1)
    ax.hist(gg['B_s0_M'],bins=50,density=True,label=r'$B_s^0 \to \gamma \gamma$',range=myrange)
    ax.hist(pi0pi0['B_s0_M'],bins=50,density=True,histtype='step',color='red',label=r'$B^0 \to \pi^0\pi^0$',range=myrange)
    ax.set_xlabel('M [MeV]',horizontalalignment='right',x=1.0)
    ax.set_ylabel('Yield A.U.')
    plt.tight_layout()
    plt.legend()
    plt.savefig('./gg_v_pi0pi0.pdf')