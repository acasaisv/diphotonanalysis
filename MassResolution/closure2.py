#AS IT IS RIGHT NOW WORKS FOR PHOTONS UP TO 100 GeV of energy
import uproot3 as uproot
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import math as m
import pickle

def nearest_rect(n):
    sqn = m.sqrt(n)
    for i in range(2,int(sqn)+2):
        if not n % i:
            return (i,int(n/i))
        
def hist_to_pairs(hist):
    pairs = []
    for i in range(len(hist)-1):
        pairs.append((hist[i],hist[i+1]))
    return pairs

def get_df():
    root = '/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation'
    root = '/scratch47/adrian.casais/ntuples/signal'
    f_s = uproot.open(root + '/49100040_MagDown.root')
    #f_s = uproot.open(root + '/13100212.root')


    t_s = f_s['B2GammaGammaTuple/DecayTree']
    
    keys = [ i.decode('utf-8') for i in t_s.keys() ]

    variables = []

    for par in ['gamma','gamma0','B_s0']:
        variables += list(filter( lambda x: ( '{}_TRUEP'.format(par) in x ) , keys))
        variables += list(filter(lambda x: '{}_P'.format(par) in x and not ('PP' in x), keys))
        
    variables += ['gamma_MC_MOTHER_ID','gamma0_MC_MOTHER_ID','gamma_TRUEID','gamma0_TRUEID','B_s0_M','B_s0_MM']

    mc ='gamma_MC_MOTHER_ID==54 and gamma0_MC_MOTHER_ID==54 and  gamma_TRUEID==22 and gamma0_TRUEID==22'
    #mc ='gamma_MC_MOTHER_ID==531 and gamma0_MC_MOTHER_ID==531 and  gamma_TRUEID==22 and gamma0_TRUEID==22'
 
    #############################
    df = t_s.pandas.df(branches=variables)
    df = df.query(mc)
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    df['gamma0_ETA'] = 0.5*np.log( (df['gamma0_P']+df['gamma0_PZ'])/(df['gamma0_P']-df['gamma0_PZ']) )
    df.eval('gamma_TRUEP = sqrt(gamma_TRUEP_Z**2 +gamma_TRUEPT**2)',inplace=True)
    df.eval('gamma0_TRUEP = sqrt(gamma0_TRUEP_Z**2 +gamma0_TRUEPT**2)',inplace=True)
    df.eval('costrue_angle = (gamma_TRUEP_X*gamma0_TRUEP_X + gamma_TRUEP_Y*gamma0_TRUEP_Y + gamma_TRUEP_Z*gamma0_TRUEP_Z)/(gamma_TRUEP*gamma0_TRUEP) ',inplace = True)
    df.eval('cosangle =(gamma_PX*gamma0_PX + gamma_PY*gamma0_PY + gamma_PZ*gamma0_PZ)/(gamma_P*gamma0_P) ',inplace=True)
    df['angle'] = np.arccos(df['cosangle'])
    df['true_angle'] = np.arccos(df['costrue_angle'])
    df.eval('res_angle = (angle-true_angle)/true_angle',inplace=True)
    
    factor_resolution = 1
    df.eval('res = {}*(gamma_TRUEP_E - gamma_P)/gamma_TRUEP_E'.format(factor_resolution),inplace=True)
    df.eval('res0 = {}*(gamma0_TRUEP_E - gamma0_P)/gamma0_TRUEP_E'.format(factor_resolution),inplace=True)
    df.eval('delta = gamma_TRUEPT - gamma_PT'.format(factor_resolution),inplace=True)
    df.eval('delta0 = gamma0_TRUEPT - gamma0_PT'.format(factor_resolution),inplace=True)

    return df

##CONSTRUCT SMEARED ENERGY
def sample_resolution(df,res_variable,n_bins):
    if '0' in res_variable:
        res_variable1 = res_variable.replace('0','')
    else:
        res_variable1 = res_variable+'0'
    
    if res_variable1 in  df.keys():
        H,X = np.histogram(np.concatenate((df[res_variable],df[res_variable1])),bins=2*n_bins,density = True)
    else:
        H,X = np.histogram(df[res_variable],bins=n_bins,density = True)
    
    dx = X[1] - X[0]
    X = (X[1:] + X[:-1])/2 #bin centers instead of edges
    cummulative= np.cumsum(H)*dx
    random_seed = np.random.random_sample(len(df))
    
    df['{}_sampled'.format(res_variable)] = X[np.digitize(random_seed,cummulative)]


def get_distrib(var,name='name'):
    pk,xk = np.histogram(var,
                         bins=30,
                         #density=True,
                         #weights=weights
                         )
    width = np.diff(xk)
    xk = (xk[1:] + xk[:-1])/2
    dx = xk[1]-xk[0]
    pk[pk<0] = 0
    pk = pk*dx
    pk = pk/sum(pk)
    return stats.rv_discrete(name=name, values=(xk, pk)),[pk,xk,width]





def output_distrib(df,etas,pts):
    distributions = {}
    histograms = {}
    etas,pts = hist_to_pairs(etas),hist_to_pairs(pts)
    for eta in etas:
        distributions[eta] = {}
        histograms[eta]={}
        for pt in pts:
            df_ = df.query('gamma_PT > {0} & gamma_PT < {1} & gamma_ETA > {2} & gamma_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))
            df0_ = df.query('gamma0_PT > {0} & gamma0_PT < {1} & gamma0_ETA > {2} & gamma0_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))

            df_ = df.query('gamma_P > {0} & gamma_P < {1} & gamma_ETA > {2} & gamma_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))
            df0_ = df.query('gamma0_P > {0} & gamma0_P < {1} & gamma0_ETA > {2} & gamma0_ETA < {3}'.format(pt[0],pt[1],eta[0],eta[1]))

            
        
            #distributions[eta,pt]   = get_distrib(np.append(df_['delta'],df0_['delta0'],np.append(df_['sweights'],df0_['sweights']),name='eta={0},pt={1}'.format(eta,pt)))
            distributions[eta][pt],histograms[eta][pt] = get_distrib(np.append(df_['res'].array,df0_['res0'].array),name='eta={0},pt={1}'.format(eta,pt))
            
        
        with open ('distributions.p','wb') as handle:
            pickle.dump( distributions,handle)
    
    return histograms

def plot_smearing_distribs(histograms,pts):
    pTs = hist_to_pairs(pts)
    xplots,yplots = nearest_rect(len(histograms))
    fig,ax = plt.subplots(xplots,yplots,figsize=(20,9),dpi=80)
    n_plots = xplots*yplots
    ax_flat =ax.flatten()
    fig.suptitle("")
    
    for pT,i in zip(pTs,range(n_plots)):
        print(pT)
        myhist = histograms[pT]
        ax_flat[i].set_title('PT in ({0:.2f},{1:.2f})'.format(pT[0],pT[1]))
        ax_flat[i].bar(myhist[1],myhist[0],align='center',width=myhist[2],alpha=0.6)
        
                                                                        
    ax_flat[0].legend(('MC'))
    ax_flat[0].set_xlabel('MeV')
    #plt.show()
                                                                                        
#Testing functions
def test_smearing(df,distributions,etas,pts,offset=0):
    etas,pts = hist_to_pairs(etas),hist_to_pairs(pts)
    gamma = 'gamma'
    for eta in etas:               
        for pt in pts:
            print("PT is {}".format(pt))
            condition = '{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma',pt[0],pt[1],eta[0],eta[1])
            condition0 = '{0}_PT > {1} & {0}_PT < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma0',pt[0],pt[1],eta[0],eta[1])

            condition = '{0}_P > {1} & {0}_P < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma',pt[0],pt[1],eta[0],eta[1])
            condition0 = '{0}_P > {1} & {0}_P < {2} & {0}_ETA > {3} & {0}_ETA < {4} '.format('gamma0',pt[0],pt[1],eta[0],eta[1])
            
            dflocal  = df.query(condition)
            dflocal0  = df.query(condition0)
            pk,xk,_ = distributions[eta][pt]
            
            rvs0 = np.random.choice(xk,len(dflocal0),p=pk)
            rvs1 =np.random.choice(xk,len(dflocal),p=pk)
            df.loc[dflocal.index,'gamma_smearedP'] = (1-rvs1)*dflocal['gamma_TRUEP_E']
            df.loc[dflocal0.index,'gamma0_smearedP'] = (1-rvs0)*dflocal0['gamma0_TRUEP_E']

    return df
if __name__=='__main__':
    df = get_df()
    pts = np.linspace(5000,130000,17)
    etas = np.linspace(1,6,2)
    histograms = output_distrib(df,etas,pts)
    plot_smearing_distribs(histograms[(1.0,6.0)],pts)
    plt.show()
    df = test_smearing(df,histograms,etas,pts,offset=0)
    #df['B_s0_M_smeared']=np.sqrt(2*df['gamma_smearedP']*df['gamma0_smearedP']*(1-np.cos(df['angle'])))
    df.eval('B_s0_M_smeared = sqrt(2*gamma_smearedP*gamma0_smearedP*(1-cosangle))',inplace=True)
    plt.hist(df['B_s0_M'],bins=50,density=True,alpha=.9,histtype='step',range=(4000,6500),color='xkcd:red')
    plt.hist(df['B_s0_M_smeared'],bins=50,color='xkcd:blue',density=True,alpha=.9,histtype='step',range=(4000,6500))
    plt.show()


with open ('../Hlt1Sys/distributions.p','rb') as handle:
    distributions,distributionsMC = pickle.load(handle)
