from GaudiConf import IOHelper
ind1,ind2 = 0,0

if len(sys.argv)>1:
    ind1 = int(sys.argv[1])
    ind2 = int(sys.argv[1]) #no need for ind1 ....

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]
                    

mainpath = '/castor/cern.ch/user/a/acasaisv/dsts/ALPs/5GeV_GenProdCuts_md_2016/'
onlyfiles = map(lambda x: "MC2016_Priv_MagDown_ALP5_{0}.ldst".format(x), range(5,49))
mychunks = list(chunks(files,4)) #11 chunks
inpdsts = mychunks[ind2]

IOHelper('ROOT').inputFiles(inpdsts)
