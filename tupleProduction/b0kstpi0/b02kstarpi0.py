from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter, TupleToolTISTOS, TupleToolCaloHypo, TupleToolTrackInfo, MCDecayTreeTuple
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons, StdAllLoosePions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence, RebuildSelection
import os
import sys

#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'DST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'b0kstartuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
# DaVinci().Simulation = True
# DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
#DaVinci().RootInTES = "/Event/BEAUTY2XGAMMA.STRIP"
#DaVinci().RootInTES = "/Event/AllStreams"
DaVinci().EnableUnpack=['Reconstruction', 'Stripping']


dtt = DecayTreeTuple('DecayTree')
dtt.Decay = '[B0 -> ^(K*(892)0 -> ^K+ ^pi-) ^pi0]CC'
dtt.addBranches({
    "B"      : "[B0 -> (K*(892)0 -> K+ pi-) pi0]CC",
    "pi0" : "[B0 -> (K*(892)0 -> K+ pi-) ^pi0]CC",
    "kst"  : "[B0 -> ^(K*(892)0 -> K+ pi-) pi0]CC",
    "kplus"  : "[B0 -> (K*(892)0 -> ^K+ pi-) pi0]CC",
    "piminus"  : "[B0 -> (K*(892)0 -> K+ ^pi-) pi0]CC",

        })

if DaVinci().getProp('Simulation'):
    dtt.Inputs = ['/Event/Beauty2XGamma.Strip/Phys/Beauty2XGammaExclTDCPVBd2KstGammaLine/Particles']
else:
    dtt.Inputs = ['/Event/BhadronCompleteEvent/Phys/Beauty2XGammaExclTDCPVBd2KstGammaLine/Particles',
                  '/Event/BhadronCompleteEvent/Phys/Beauty2XGammaExclusiveBd2KstGammaLine/Particles']
# TupleToolProtoPData configuration

if DaVinci().getProp('Simulation'):
    mc_truth = dtt.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    
    dtt.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
    dtt.MCLoKi_All.Variables =  {
        'TRUEETA'    : 'MCETA',
        'TRUEPHI'    : 'MCPHI',
        'TRUEID': 'MCID'
        #'isPhoton' : 'ISPHOTON'
        }
    decay = '[B0 => ^(K*(892)0 -> ^K+ ^pi-) ^gamma]CC'

    mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    mctuple.Decay = decay 
    mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
    mctuple.LoKi_All.Variables =  {
        'TRUEID' : 'MCID',
        'M'      : 'MCMASS',
        'THETA'  : 'MCTHETA',
        'PT'     : 'MCPT',
        'PX'     : 'MCPX',
        'PY'     : 'MCPY',
        'PZ'     : 'MCPZ',
        'ETA'    : 'MCETA',
        'PHI'    : 'MCPHI'
        }
    
    DaVinci().UserAlgorithms += [mctuple]


if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    dtt.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]


dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
dtt.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA':'ETA',
    'PHI':'PHI'
            }
triglist = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    'L0HadronDecision',
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    'Hlt1MBNoBiasDecision',
    'Hlt1B2PhiGamma_LTUNBDecision',
    "Hlt2RadiativeB2GammaGammaDecision",
    'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision',
    'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision',
    'Hlt2CaloPIDBd2KstGammaTurboCalibDecision',
    'Hlt2RadiativeBs2PhiGammaUnbiasedDecision',
    'Hlt2Bs2PhiGammaDecision',
    'Hlt2Bd2KstGammaDecision',
    'Hlt2Bs2PhiGammaWideBMassDecision',
    'Hlt2Bd2KstGammaWideKMassDecision',
    'Hlt2Bd2KstGammaWideBMassDecision',
    'Hlt1TrackPhotonDecision',
    'Hlt2RadiativeBd2KstGammaDecision',
    'Hlt2RadiativeBs2PhiGammaDecision',
    'Hlt2RadiativeBd2KstGammaULUnbiasedDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt2RadiativeIncHHGammaDecision',
    'Hlt2RadiativeIncHHHGammaDecision',
    'Hlt2Topo2BodyDecision'
    ]
TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]
dtt.addTupleTool(TISTOS)
dtt.ToolList += ['TupleToolPhotonInfo',
                 #'TupleToolCaloHypo',
                 'TupleToolEventInfo',"TupleToolRecoStats",'TupleToolTrackInfo']

#dtt.gamma.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
#dtt.gamma.gammaL0ECalo.WhichCalo = "ECAL"
#dtt.gamma.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
#dtt.gamma.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]
TrackInfo = TupleToolTrackInfo('TrackInfo')
TrackInfo.Verbose=True
dtt.addTupleTool(TrackInfo)
NeutralsHypo =TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList = ['Saturation', 'CellID','Hcal2Ecal','X','Y','Z']
#dtt.gamma.addTupleTool(NeutralsHypo)
dtt.gamma.addTupleTool("TupleToolProtoPData")
dtt.gamma.TupleToolProtoPData.DataList = ["IsPhoton",
        "IsNotE",
        "IsNotH",
        'Saturation',
        'IsPhotonXGB',
        'ClusterAsX',
        'ClusterAsY',
        'CaloClusterCode',
        'CaloClusterFrac',
        'CaloShapeKappa',
        'CaloShapeAsym',
        'CaloShapeE1',
        'CaloShapeE2',
        'CaloPrsShapeE1',
        'CaloPrsShapeE2',
        'CaloPrsShapeEmax',
        'CaloPrsShapeFr2',
        'CaloPrsShapeAsym',
        'CaloPrsM',
        'CaloPrsM15',
        'CaloPrsM30',
        'CaloPrsM45',
        'CaloTrMatch',
        'CaloNeutralID',
        'CaloTrajectoryL',
        'CaloNeutralE19',
        'CaloPrsNeutralE19',
        'CaloPrsNeutralE49',
        'CaloNeutralPrsM',
        'CaloShapeFr2r4',
        'CaloPrsNeutralE4max',
        'CaloNeutralHcal2Ecal',
        'CaloNeutralEcal',
        'CaloNeutralPrs',
        'CaloNeutralSpd',
        'ClusterMass',
        'ShowerShape',
        'CaloDepositID',
        'CaloChargedID'
]

#DaVinci().UserAlgorithms += [dtt]

dtt.addTupleTool('TupleToolL0Data')
dtt.TupleToolL0Data.DataList = ['*Et*','*Pt*']
from Configurables import TupleToolConeIsolation
ConeIsol = TupleToolConeIsolation('ConeIsol',
                                  FillComponents=True,
                                  MinConeSize=1.0,
                                  SizeStep=0.35,
                                  MaxConeSize=2.05,
                                  Verbose=True,
                                  FillPi0Info=False,
                                  FillMergedPi0Info=False,
                                  FillAsymmetry=True,
                                  FillDeltas= False,
                                  ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                                  MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                                  )
ConeIsol.Verbose=True
ConeIsol.OutputLevel=3.
dtt.B.addTupleTool(ConeIsol)
dtt.gamma.addTupleTool(ConeIsol)
caloPostCalib = True
if caloPostCalib:
    importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc.Inputs=dtt.Inputs
    DaVinci().UserAlgorithms  += [cpc]

DaVinci().UserAlgorithms += [dtt]




# importOptions('pool_xml_catalog.py')
# importOptions('files.py')
# import GaudiPython

# gaudi = GaudiPython.AppMgr()
# TES = gaudi.evtsvc()x

# gaudi.initialize()
# gaudi.run(1000)
# for i in range(1000):
#     if TES['/Event/Beauty2XGamma.Strip']:
#         break
#         TES.dump()
