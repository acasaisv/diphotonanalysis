#-- GAUDI jobOptions generated on Wed Jun 22 12:59:43 2022
#-- Contains event types : 
#--   11102204 - 10 files - 146481 events - 36.48 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Sim09j/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34r0p1Filtered' 

#--  StepId : 144211 
#--  StepName : Merge for RD Filtered Productions (Bd2Kspipigamma TD-CPV) - DST 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v45r6 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DVMergeDST.py;$APPCONFIGOPTS/DaVinci/DataType-2018.py;$APPCONFIGOPTS/Merging/WriteFSR.py;$APPCONFIGOPTS/Merging/MergeFSR.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py;$APPCONFIGOPTS/DaVinci/Simulation.py 
#--  DDDB : fromPreviousStep 
#--  CONDDB : fromPreviousStep 
#--  ExtraPackages : AppConfig.v3r402 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000005_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000009_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000007_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000006_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000010_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000002_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000004_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000001_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000003_1.beauty2xgamma.strip.dst',
# 'LFN:/lhcb/MC/2018/BEAUTY2XGAMMA.STRIP.DST/00124024/0000/00124024_00000008_1.beauty2xgamma.strip.dst',
], clear=True)

