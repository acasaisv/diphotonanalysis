#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os
myApp = GaudiExec(directory='/scratch09/adrian.casais/cmtuser/DaVinciDev_v45r5',
                  platform = 'x86_64-centos7-gcc9-opt')

j = Job(name='etammg-MC-MU')
options = ['etapi0gg.py',
           './caloPostCalib.py',
           ]


extraopt = 'import os\n'\
'os.environ["GRINDEX"] = 0\n'
j.application = myApp

j.application.options = options
lfns =['LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000/MC2016_Priv_md_30000000_{}.ldst'.format(i) for i in range(189)]
input = LHCbCompressedDataset(lfns) 
j.inputdata = input 
j.backend = Dirac()
j.splitter = SplitByFiles(filesPerJob=10)
j.outputfiles = [DiracFile('*.root')]
j.submit()
