from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
import os

#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'LDST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'etapi02gg.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
## this has be to be updated as well (below, case for ZMuMu)
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20170721-2-vc-mu100"

eta2gg                 = CombineParticles("MCSel_eta2gg")
eta2gg.Preambulo       = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
eta2gg.DecayDescriptor = "eta -> gamma gamma"
eta2gg.DaughtersCuts   = {"gamma"  : "mcMatch('eta -> ^gamma ^gamma') & (PT > 500*MeV) & (CL > 0.05)"
                        }

eta2gg.CombinationCut = "ADAMASS('eta') < 100*MeV"
eta2gg.MotherCut       = "mcMatch('eta -> gamma gamma')"
#eta2gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
eta2gg.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})
Seleta2gg = Selection("Seleta2gg",
                     Algorithm = eta2gg,
                     RequiredSelections = [ StdLooseAllPhotons])

Seqeta2gg = SelectionSequence('Seqeta2gg', TopSelection = Seleta2gg)


pi02gg                 = CombineParticles("MCSel_pi02gg")
pi02gg.Preambulo       = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
pi02gg.DecayDescriptor = "pi0 -> gamma gamma"
pi02gg.DaughtersCuts   = {"gamma"  : "mcMatch('pi0 -> ^gamma ^gamma')  & (PT > 500*MeV) & (CL >0.05)"                  
                        }

pi02gg.CombinationCut = "ADAMASS('pi0') < 100*MeV"
pi02gg.MotherCut       = "mcMatch('pi0 -> gamma gamma')"
#pi02gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
pi02gg.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})
Selpi02gg = Selection("Selpi02gg",
                     Algorithm = pi02gg,
                     RequiredSelections = [ StdLooseAllPhotons])
Seqpi02gg = SelectionSequence('Seqpi02gg', TopSelection = Selpi02gg)


caloPostCalib=True
if caloPostCalib:
    importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc_inputs = ['Phys/MCSel_eta2gg/Particles','Phys/MCSel_pi02gg/Particles']
    cpc.Inputs=cpc_inputs
    DaVinci().UserAlgorithms  += [cpc]
    


ToolList = [ #-- global event data
                      #"TupleToolEventInfo",
                      #"TupleToolPrimaries",
                      #"TupleToolCPU",
                      #-- particle data
                      #"TupleToolKinematic",
                      #"TupleToolDalitz",
                      "TupleToolPid",
                      #"TupleToolPropertime",
                      #"TupleToolTrackInfo",
                      # "TupleToolHelicity",      # private tool
                      #"TupleToolAngles",
                      "TupleToolPhotonInfo",
                      #"TupleToolANNPID",           # includes all MCTuneV
                      #"TupleToolCaloHypo"
                      ]


dtt_eta = DecayTreeTuple('Eta2ggTuple')
dtt_eta.Decay ='eta -> ^gamma ^gamma'
dtt_eta.Inputs = [ Seleta2gg.outputLocation() ]
dtt_eta.ToolList += ToolList
# TupleToolProtoPData configuration

dtt_eta.addTupleTool("TupleToolProtoPData")
dtt_eta.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH"]
if DaVinci().getProp('Simulation'):
    mc_truth = dtt_eta.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")




# dtt_eta.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
# dtt_eta.LoKi_All.Variables =  {
#     'PT'    : 'PT',
#     'P'    : 'P',
#     #'TRUEID': 'MCID'
#     #'isPhoton' : 'ISPHOTON'
#             }
# dtt_eta.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
# dtt_eta.MCLoKi_All.Variables =  {
#     'TRUEETA'    : 'MCETA',
#     'TRUEPHI'    : 'MCPHI',
#     'TRUEID': 'MCID'
#     #'isPhoton' : 'ISPHOTON'
#             }
                                        


DaVinci().UserAlgorithms += [Seqeta2gg.sequence() , dtt_eta]

dtt_pi0 = DecayTreeTuple('Pi02ggTuple')
dtt_pi0.Decay ='pi0 -> ^gamma ^gamma'
dtt_pi0.Inputs = [ Selpi02gg.outputLocation() ]

# TupleToolProtoPData configuration

dtt_pi0.addTupleTool("TupleToolProtoPData")
dtt_pi0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH"]
dtt_pi0.ToolList += ToolList
if DaVinci().getProp('Simulation'):
    mc_truth = dtt_pi0.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")




# dtt_pi0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
# dtt_pi0.LoKi_All.Variables =  {
#     'PT'    : 'PT',
#     'P'    : 'P',
#     #'TRUEID': 'MCID'
#     #'isPhoton' : 'ISPHOTON'
#             }
# dtt_pi0.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
# dtt_pi0.MCLoKi_All.Variables =  {
#     'TRUEETA'    : 'MCETA',
#     'TRUEPHI'    : 'MCPHI',
#     'TRUEID': 'MCID'
#     #'isPhoton' : 'ISPHOTON'
#             }
                                        


DaVinci().UserAlgorithms += [Seqpi02gg.sequence() , dtt_pi0]

