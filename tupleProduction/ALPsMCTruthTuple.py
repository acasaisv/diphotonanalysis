from Configurables import (
        DaVinci,
            EventSelector,
            PrintMCTree,
            MCDecayTreeTuple
        )
from DecayTreeTuple.Configuration import *
from Gaudi.Configuration import importOptions
DaVinci().DataType = "2017"
DaVinci().Simulation = True
importOptions("makeALPs.py")
# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = "AxR0 -> ^gamma ^gamma "
decay_heads = ["AxR0"]

year = 2016

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################
DaVinci().InputType = "DST" # if running on stripped data... change
# Create an MC DTT containing any candidates matching the decay descriptor

mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
# mctuple.ToolList = [
#         "MCTupleToolHierarchy",
#             "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
#         ]


mctuple.ToolList += [ #-- global event data
                         "TupleToolEventInfo",
			 "TupleToolPrimaries",
			 "TupleToolCPU",
			 #-- particle data
			 "TupleToolKinematic",
			 "TupleToolDalitz",
			 "TupleToolPid",
			 "TupleToolPropertime",
			 "TupleToolTrackInfo",
			 # "TupleToolHelicity",      # private tool
			 "TupleToolAngles",
			 "TupleToolPhotonInfo",
			 "TupleToolANNPID",           # includes all MCTuneV
			 ]

if DaVinci().getProp('Simulation'):
        mc_truth = mctuple.addTupleTool("TupleToolMCTruth")
        mc_truth.addTupleTool("MCTupleToolHierarchy")

        # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
        mctuple.addTupleTool("TupleToolMCBackgroundInfo")
        if DaVinci().InputType == "DST": # doesnt work with mDST
            mctuple.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
            #Butupl.TupleToolMCBackgroundInfo.OutputLevel = VERBOSE





mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
mctuple.LoKi_All.Variables =  {
    'TRUEID' : 'MCID',
    'M'      : 'MCMASS',
    'THETA'  : 'MCTHETA',
    'PT'     : 'MCPT',
    'PX'     : 'MCPX',
    'PY'     : 'MCPY',
    'PZ'     : 'MCPZ',
    'ETA'    : 'MCETA',
    'PHI'    : 'MCPHI'
        }


DaVinci().UserAlgorithms += [mctuple]
