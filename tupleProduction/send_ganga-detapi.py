#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser2/DaVinciDev_v45r5')


for pol in ['md','mu']:
    j = Job(name='d+etappi-MC-{}'.format(pol))
    options = ['dsetapi.py',
               './caloPostCalib.py',
               ]


    j.application = myApp
    
    j.application.options = options
    j.application.extraOpts = 'from Configurables import DaVinci\n'\
                                  'DaVinci().DDDBtag  = "dddb-20170721-3"\n'\
                                  'DaVinci().CondDBtag  = "sim-20190430-vc-{}100"'.format(pol)
    

    j.application.platform = 'x86_64-centos7-gcc9-opt'

    polarity = 'MagUp'
    if pol == 'md': polarity='MagDown'
    bkk_data = '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/21103210/ALLSTREAMS.MDST'
    
    j.inputdata = BKQuery(bkk_data).getDataset()
         

    j.backend = Dirac()
    j.splitter = SplitByFiles(filesPerJob=50)
    j.outputfiles = [LocalFile('*.root')]
    j.submit()
