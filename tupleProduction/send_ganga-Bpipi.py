# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
for pol in ['MagDown','MagUp']:
 if pol=='MagDown':
     mag='md'
 else:
     mag='mu'
     
 j = Job()
 root = '/home3/adrian.casais/cmtuser/'
 j.application = BenderModule(
                    module = "./BenderALPsTuple.py",
                    directory = root + "/BenderDev_v33r3",
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )

 
 j.name = "B-pipi"+pol
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2016,"Polarity":pol,"Simulation":True,"DDDBTag":"dddb-20170721-3","CondDBtag":"sim-20190430-1-vc-{}100".format(mag)}
 j.inputfiles = [root + "/makeALPs.py",'./caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 
 bkk_data = '/MC/2016/Beam6500GeV-2016-{}-Nu1.6-25ns-Pythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03a/Stripping28r1NoPrescalingFlagged/11102013/ALLSTREAMS.DST'.format(pol)
 j.inputdata = BKQuery(bkk_data).getDataset()
 
 
 j.submit()
 
