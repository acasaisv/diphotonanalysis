mag = 'md'
for pol in ['MagDown','MagUp']:
 if pol == 'MagUp': mag='mu'
 j = Job()
 root = '/home3/adrian.casais/cmtuser/'
 j.application = BenderModule(
                    module = "./BenderALPsTuple.py",
                    directory = root + "/BenderDev_v33r3",
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )

 
 j.name = "bender-inclJpsiMC2018-"+pol
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2018,"Polarity":pol,"Simulation":True,"DDDBTag":"dddb-20170721-3","CondDBtag":"sim-20190430-vc-{}100".format(mag)}
 j.inputfiles = [root + "/makeALPs.py",'./caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 
 
 #bkk_data = "/LHCb/Collision18/Beam6500GeV-VeloClosed-{}/Real Data/Reco18/Stripping34/90000000/DIMUON.DST".format(pol)
 bkk_data = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim09g/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/24142001/ALLSTREAMS.DST'.format(pol)
 
 j.inputdata = BKQuery(bkk_data).getDataset()
 
 
 j.submit()
 
