# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
for pol in ['MagDown','MagUp']:
 j = Job()
 root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
 j.application = BenderModule(
                    module = root + "/BenderALPsTuple.py",
                    directory = root + "/BenderDev_v32r9",
                    platform = "x86_64-slc6-gcc62-opt",
                    #events = 1000,
                    )

 
 j.name = "bender-MBdata-"+pol
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2018,"Polarity":pol,"Simulation":False,"DDDBTag":"dddb-20171030-3","CondDBtag":"cond-20180202"}
 j.inputfiles = [root + "/makeALPs.py"]
 j.outputfiles = [DiracFile('*.root')]
 
 #bkk = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/300000{}/ALLSTREAMS.DST"
 bkk_data = "/LHCb/Collision18/Beam6500GeV-VeloClosed-{}/Real Data/Reco18/96000000/FULL.DST".format(pol)
 
 #j.inputdata = BKQuery(bkk.format(n)).getDataset()
 j.inputdata = BKQuery(bkk_data).getDataset()
 
 # lfns =['LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000/MC2016_Priv_md_30000000_{}.ldst'.format(i) for i in range(189)]
 # input = LHCbDataset()
 # for lfn in lfns: input.append(DiracFile(lfn))
 # j.inputdata = input
 
 j.submit()
 
