from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci
from GaudiConf import IOHelper

# Stream and stripping line we want to use
stream = 'AllStreams'
line = 'Bs2MuMuLinesBs2JPsiPhiLine'

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('StrippingBs2MuMuLinesBs2JPsiPhiLineDecision')"
)
DaVinci().EventPreFilters = fltrs.filters('Filters')


# Create an ntuple to capture Bs decays from the StrippingLine line
dtt = DecayTreeTuple('TupleBs2JpsiPhi')
dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)]
dtt.Decay = 'B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)'

dtt.ToolList += [
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolKinematic",
    "TupleToolPid",
    # "TupleToolTISTOS",     
    "TupleToolPropertime",
    "TupleToolMuonPid",
    "TupleToolTrackInfo"
    ]



L0Triggers = ["L0MuonDecision", "L0DiMuonDecision", "L0HadronDecision", "L0ElectronDecision", "L0PhotonDecision"]

Hlt1Triggers = [ "Hlt1SingleMuonNoIPDecision", "Hlt1TrackMuonDecision", "Hlt1DiMuonDetachedDecision", "Hlt1TrackMVADecision",
 "Hlt1DiMuonLowMassDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TwoTrackMVADecision", "Hlt1DiMuonNoL0Decision"]

Hlt2Triggers = ["Hlt2SingleMuonHighPTDecision", "Hlt2SingleMuonRareDecision", "Hlt2DiMuonDetachedDecision", "Hlt2DiMuonDetachedHeavyDecision", 
"Hlt2DiMuonBDecision", "Hlt2TopoMu2BodyDecision", "Hlt2DiMuonSoftDecision", "Hlt2TopoMu3BodyDecision","Hlt2TopoMu4BodyDecision","Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision",
"Hlt2Topo4BodyDecision","Hlt2TopoMuMu2BodyDecision", "Hlt2TopoMuMu3BodyDecision",
"Hlt2TopoMuMu4BodyDecision"]

triggerListF = L0Triggers + Hlt1Triggers + Hlt2Triggers

dtt.addTupleTool("TupleToolTISTOS")

dtt.TupleToolTISTOS.VerboseL0 = True
dtt.TupleToolTISTOS.VerboseHlt1 = True
dtt.TupleToolTISTOS.VerboseHlt2 = True
dtt.TupleToolTISTOS.FillL0 = True
dtt.TupleToolTISTOS.FillHlt1 = True
dtt.TupleToolTISTOS.FillHlt2 = True
dtt.TupleToolTISTOS.OutputLevel = INFO
dtt.TupleToolTISTOS.TriggerList = triggerListF

mc_truth = dtt.addTupleTool("TupleToolMCTruth")
mc_truth.addTupleTool("MCTupleToolHierarchy")
dtt.addTupleTool("TupleToolMCBackgroundInfo")



mctuple = MCDecayTreeTuple('Bs2JpsiPhi_MC')
mctuple.Decay = '^[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC'




# Configure DaVinci
DaVinci().UserAlgorithms += [mctuple,dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = 100
DaVinci().CondDBtag = 'sim-20201113-8-vc-md100-Sim10'
DaVinci().DDDBtag = 'dddb-20220927-2018'

# Use the local input data
IOHelper().inputFiles([
    #'LFN:/lhcb/MC/2018/ALLSTREAMS.DST/00185720/0000/00185720_00000003_7.AllStreams.dst'
    'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/2018/ALLSTREAMS.DST/00185720/0000/00185720_00000003_7.AllStreams.dst'
], clear=True)
