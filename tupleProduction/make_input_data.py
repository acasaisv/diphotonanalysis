import math as m
import os
root = '/scratch36/xabier.cid/23173200/'
dst_list = [root+x for x in os.listdir(root) if 'ldst' in x]

files_per_job = 10

no_jobs = int(m.ceil(len(dst_list)/1./files_per_job))
index_file =0
for job in range(no_jobs):
    no_files = 1

    with open("etammg_input/data_job_{}.py".format(job),"w") as f:
        f.write("from Gaudi.Configuration import *\nfrom GaudiConf import IOHelper\nIOHelper('ROOT').inputFiles([\n")


        while no_files <= files_per_job and index_file < len(dst_list):
            f.write("'" + dst_list[index_file] +"',\n")
            index_file +=1
            no_files += 1
        f.write("], clear=True)\n")
