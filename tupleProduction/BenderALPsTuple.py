# -*- coding: utf-8 -*-
#!/usr/bin/env python
from Bender.Main import *
import math as m
import BenderTools.Fill
from   LoKiPhys.decorators import *
import BenderTools.TisTos
from LoKiCore.math import sqrt
from ROOT import TFile, TTree
import os 
import IPython


        
    
class Alllg(Algo):

    
    ###############################################################
    def initialize ( self ) :
       
        sc = Algo.initialize ( self )
        if sc.isFailure() : return sc
        
        self.IsMC = True
        
        self.pid = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPid', parent=self)
        if not self.pid : return FAILURE
        
        self.trig = self.tool(cpp.IEventTupleTool,
                              'TupleToolTrigger', parent=self)
        
        if not self.trig : return FAILURE
        
        self.tuptistos = self.tool(cpp.IParticleTupleTool,
                                   'TupleToolTISTOS')


        if not self.tuptistos : return FAILURE
        
        self.kin = self.tool(cpp.IParticleTupleTool,
                             'TupleToolKinematic', parent=self)
        if not self.kin : return FAILURE
        
        self.geo = self.tool(cpp.IParticleTupleTool,
                             'TupleToolGeometry', parent=self)
        if not self.geo : return FAILURE
        
        self.prim = self.tool(cpp.IEventTupleTool,
                              'TupleToolPrimaries', parent=self)
        if not self.prim : return FAILURE
        
        self.track = self.tool(cpp.IParticleTupleTool,
                               'TupleToolTrackInfo', parent=self)
        if not self.track : return FAILURE
        
        self.proptime = self.tool(cpp.IParticleTupleTool,
                                  'TupleToolPropertime', parent=self)
        if not self.proptime : return FAILURE
        
        self.event = self.tool(cpp.IEventTupleTool,
                                'TupleToolEventInfo', parent=self)
        if not self.event : return FAILURE
        
        self.muon = self.tool(cpp.IParticleTupleTool,
                              'TupleToolMuonPid', parent=self)
        if not self.muon : return FAILURE

        self.mc = self.tool(cpp.IParticleTupleTool,
                            'TupleToolMCTruth', parent=self)
        if not self.mc : return FAILURE
        
        self.recostats = self.tool(cpp.IEventTupleTool,
                            'TupleToolRecoStats', parent=self)
        if not self.recostats : return FAILURE
        
        self.coneiso = self.tool(cpp.IParticleTupleTool,
                            'TupleToolConeIsolation', parent=self)
        if not self.coneiso : return FAILURE

        self.photoninfo = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPhotonInfo', parent=self)
        if not self.photoninfo : return FAILURE

        self.tuplecalohypo = self.tool(cpp.IParticleTupleTool,
                                        'TupleToolCaloHypo',parent=self)
        if not self.tuplecalohypo : return FAILURE

        self.tupleprotopdata = self.tool(cpp.IParticleTupleTool,
                                        'TupleToolProtoPData',parent=self)
        if not self.tupleprotopdata : return FAILURE

        self.tuplel0calo = self.tool(cpp.IParticleTupleTool,
                                        'TupleToolL0Calo',parent=self)
        if not self.tuplel0calo : return FAILURE

        
        self._dtf = self.decayTreeFitter('LoKi::DecayTreeFit')
        
        if not self._dtf  : return FAILURE
        
        import ROOT 
        ROOT.gInterpreter.ProcessLine("#include \"Kernel/IMatterVeto.h\"") 
        
        self.matterveto = self.tool(cpp.IMatterVeto,
                            'MatterVetoTool', parent=self)
        if not self.matterveto : return FAILURE
        
        
        triggers = {}
        
        triggers["B_s0"] = {}
        
        lines = {}
        
        lines ["B_s0"]  = {}
        lines ["B_s0"]['L0TOS'] = 'L0(Photon|Electron)Decision'
        lines ["B_s0"]['L0TIS'] = ''
        lines ["B_s0"]['Hlt1TOS'] = "Hlt1B2GammaGamma.*Decision"
        lines ["B_s0"]['Hlt1TIS'] = ''
        lines ["B_s0"]['Hlt2TOS'] = "Hlt2RadiativeB2GammaGammaDecision"
        lines ["B_s0"]['Hlt2TIS'] = ''
                      

        # sc = self.tisTos_initialize ( triggers , lines  )
        # if sc.isFailure() : return sc


        # import GaudiPython
        
        # gaudi = GaudiPython.AppMgr()
        # TES = gaudi.evtsvc()
        

        return SUCCESS
    
    ################################################################

    def ISMC( self, sim=True):
    
        self.IsMC = sim

    def id_matching(self,line_gamma,jet_gamma):
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False

        return (line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())

    def kinetic_matching(self,line_gamma,jet_gamma,tolerance = [0.001,0.001]):
        #if not jet_gamma.particleID().pid()==22: return False
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
        if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
        if not jet_gamma.proto().calo()[0].position() or not line_gamma.proto().calo()[0].position(): return False
        x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
        y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
        pos_match = x_match and y_match
        pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]
        
        return (pos_match)

    def calo_matching(self,line_gamma,jet_gamma):
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
        if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
        if not jet_gamma.proto().calo()[0] or not line_gamma.proto().calo()[0]: return False

        return (jet_gamma.proto().calo()[0].data() == line_gamma.proto().calo()[0].data())

    
    def closer_cluster(self,g,PF):
        default_value = -50
        if not g.proto(): return default_value
        if not 'calo' in dir(g.proto()): return default_value
        if not g.proto().calo().size(): return default_value
        if not g.proto().calo()[0].data(): return default_value
        PF_calo = filter(lambda x: x.proto(),PF)
        PF_calo = filter(lambda x: x.proto().calo(),PF_calo)
        PF_calo = filter(lambda x: x.proto().calo().size(),PF_calo)
        PF_calo = filter(lambda x: x.proto().calo()[0].data(),PF_calo)
        PF_calo = filter(lambda x: x.proto().calo()[0].data().position(),PF_calo)
        position = g.proto().calo()[0].data().position()
        try:
            distances = map(lambda x: m.sqrt((position.x() - x.proto().calo()[0].data().position().x())**2 + (position.y() - x.proto().calo()[0].data().position().y())**2),PF_calo)
            result = min(distances)
        except:
            result = -50
        return result

    def hf_quark_mcparticles(self):
        mcpars = self.get('MC/Particles')
        if mcpars.empty(): return 0
        abs_pids = map(lambda x: abs(x.particleID().pid()),mcpars)
        if 5 in abs_pids:
            return 1
        elif 4 in abs_pids:
            return 2
        else:
            return 0
        
        

    ## the main 'analysis' method
    def analyse( self ) :   ## IMPORTANT!
        tup = self.nTuple('DecayTree')
        header  = self.get( '/Event/Rec/Header' )
        self.event.fill(tup)
        self.recostats.fill(tup)
        self.prim.fill(tup)
        #print jets
        particles = self.get( "Phys/Bs2GammaGamma_NoConvLine/Particles" )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        PF = self.get( "/Event/Phys/PFParticles/Particles" )
        if particles.empty(): return self.Warning("No input particles", SUCCESS)

        matching = self.calo_matching
        iso0 = {}
        iso1 = {}
        jets = {}

        iso_withB = {}
        jets_withB = {}

        R = [i/10. for i in range(1,15)]
        for r in R:
            jets[r]=self.get( 'Phys/StdJets-{}/Particles'.format(r) )
            iso0[r] = [-50.]
            iso1[r] = [-50.]

            jets_withB[r]=self.get( 'Phys/StdJetsWithB-{}/Particles'.format(r) )
            iso_withB[r] = [-50.]

        for p in particles:            
            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            
            # g0_matching=map(lambda x: matching(g0,x),PF)
            # g1_matching=map(lambda x: matching(g1,x),PF)
            # count_g0 = g0_matching.count(True)
            # count_g1 = g1_matching.count(True)

            #tup.column('gamma0_inPF',count_g0)
            #tup.column('gamma1_inPF',count_g1)

            #tup.column('gamma0_closerClusterinPFDistance',self.closer_cluster(g0,PF))
            #tup.column('gamma1_closerClusterinPFDistance',self.closer_cluster(g1,PF))
                       
            # for r in R:
            #     for jet in jets[r]:
            #         for daug in jet:
            #             if matching(g0,daug):
            #                 try:
            #                     iso0[r].append(daug.pt().value()/jet.pt().value())
            #                 except:
            #                     iso0[r].append(1.)
                            
                            
            #             if matching(g1,daug):
            #                 try:
            #                     iso1[r].append(daug.pt().value()/jet.pt().value())
            #                 except:
            #                     iso1[r].append(1.)
                        

            #     for jet in jets_withB[r]:
            #         for daug in jet:
            #             if daug.particleID().pid() == 531:
            #                 iso_withB[r].append(daug.pt().value()/jet.pt().value())
                            
            # for r in R:
            #     tup.column_float('gamma0_iso_{}'.format(r),max(iso0[r]))
            #     tup.column_float('gamma_iso_{}'.format(r), max(iso1[r]))

            #     tup.column_float('B_s0_iso_{}'.format(r),max(iso_withB[r]))
            
            
            #RELINFO
            cone_vars = ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']
            cone_angles = ["1.0","1.35","1.7"]
            teslocation = "Phys/Bs2GammaGamma_NoConvLine"
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)
                    self.photoninfo.fill(p,part,name,tup)
                    self.tuplel0calo.fill(p,part,name,tup)

            tup.column('HF_in_MCPARS',self.hf_quark_mcparticles())

                # for angle in cone_angles:
                #     for conevar in cone_vars:
                #         value = RELINFO("{0}/ConeVarsInfo/{1}/{2}".format(teslocation,names_cones[name],angle),conevar,-1.)(part)
                #         tup.column_float("{0}_{1}_{2}_RELINFO".format(name,conevar,angle.replace(".","_")),value)
                        
                
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!
    
    ################################################################
    
    def finalize ( self ) :
        
        del self.pid
        del self.trig
        del self.tuptistos
        del self.kin
        del self.geo
        del self.prim
        del self.track
        del self.proptime
        del self.event
        del self.muon
        del self._dtf
        del self.coneiso
        del self.recostats
        del self.matterveto
        
        return Algo.finalize ( self )

    ################################################################

    
    
    
    
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files
               catalogs = []    ,    ## xml-catalogs (filled by GRID)
               castor   = False ,    ## use the direct access to castor/EOS ?
               params   = {}   ) :
    
    year       = params.get('Year',2016)
    pol        = params.get('Polarity','MagDown')
    info       = params.get('Info','tuple')
    sim        = params.get('Simulation',True)
    stream     = params.get('Stream','EW')
    file_name  = params.get('FileName','ntuple')
    decay_descriptor = params.get('Decay Descriptor','B_s0 -> gamma gamma')
    dddbtag = params.get("DDDBtag","dddb-20170721-3")
    conddbtag = params.get("CondDBtag","sim-20170721-2-vc-md100")
    inputtype = params.get('InputType','DST')


    
    ## import DaVinci
    from Configurables import DaVinci, EventSelector
    dv = DaVinci (
                  DataType   = str(year) ,
                  InputType  = inputtype  ,
                  Simulation = sim  )
    
    dv.Simulation = sim
    if dv.Simulation:
        dv.DDDBtag = dddbtag
        dv.CondDBtag = conddbtag


    dv.TupleFile = "tuple.root"

    #

    ## Cuts dictionary
    # 

    cuts =              { 'gammaPT'             : 1000    # MeV/c
                          ,'gammaP'              : 6000   # MeV/c
                          ,'gammaCL'             : 0.3     # adimensional
                          ,'gammaNonePT'         : 1100    # MeV/c
                          ,'gammaNoneP'          : 6000   # MeV/c
                          ,'gammaNoneCL'         : 0.3    # adimensional
                          ,'NoConvHCAL2ECAL'     : 0.1   # adimensional
                          ,'BsPT'                : 2000    # MeV/c
                          ,'BsVertexCHI2pDOF'    : 20      # adimensional
                          ,'BsLowMass'           : 4300    # MeV/cc
                          ,'BsNonePT'            : 2000    # MeV/c
                          ,'BsLowMassDouble'     : 4000    # MeV/cc
                          ,'BsLowMassNone'       : 4800    # MeV/cc
                          ,'BsLowMassNoneWide'   : 0       # MeV/cc
                          ,'BsHighMass'          : 20000    # MeV/cc
                          ,'BsHighMassNone'      : 20000    # MeV/cc
                          ,'BsHighMassNoneWide'  : 4800   # MeV/cc
                          ,'scaleWide'           : 1.0
                          ,'HLT1None'            : "HLT_PASS_RE('Hlt1.*GammaGammaDecision')"
                          ,'HLT2None'            : "HLT_PASS_RE('Hlt2.*GammaGammaDecision')"
                          }


    #
    ## Setting up clean photon selection
    #
    from Configurables import FilterDesktop, FilterInTrees
    from PhysSelPython.Wrappers import Selection, SelectionSequence,DataOnDemand
    from Configurables import CombineParticles
    
    name = "Bs2gg"
    
    fltrCode_nonConv = "(PT>%(gammaPT)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s)" % cuts
    _trkFilterNonConv = FilterDesktop('NoConvFilterDesk', Code = fltrCode_nonConv )

    stdPhotons     = DataOnDemand(Location='Phys/StdLooseAllPhotons/Particles')
    

    # Clean up the non-converted photons to reduce the timing
    stdPhotons_clean = Selection( 'PhotonFilter' + name, Algorithm = _trkFilterNonConv, RequiredSelections = [stdPhotons])
    

    #
    ## Setting up selection
    #

    def TOSFilter( name = None, sel = None, trigger = None ):
        from Configurables import TisTosParticleTagger
        _filter = TisTosParticleTagger(name+"_TriggerTos")
        _filter.TisTosSpecs = { trigger+"%TOS" : 0 }
        from PhysSelPython.Wrappers import Selection
        _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = sel, Algorithm = _filter )
        return _sel



    #can this flag be configured externally ?
    wide = False

    BsGG_DC_none = "(PT>%(gammaNonePT)s*MeV) & (P>%(gammaNoneP)s*MeV) & (CL>%(gammaNoneCL)s)" % cuts
    if wide == True:
        BsGG_CC_none = "(in_range( ( %(BsLowMassNoneWide)s )*MeV, AM, ( %(BsHighMassNoneWide)s  )*MeV) )" % cuts
        BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range( ( %(BsLowMassNoneWide)s )*MeV, M, ( %(BsHighMassNoneWide)s )*MeV) )" % cuts
    else:
        BsGG_CC_none = "(in_range(%(BsLowMassNone)s*MeV, AM, %(BsHighMassNone)s*MeV))" % cuts
        BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range(%(BsLowMassNone)s*MeV, M, %(BsHighMassNone)s*MeV))" % cuts
        
    suffix=''
    if wide == True:
        scaleWide = config['scaleWide']
        suffix='wide'
    else:
        scaleWide = 1.0

        _Bs2gammagamma_none = CombineParticles(name = "CombineParticles_BsGG_none",
                                               DecayDescriptor = "B_s0 -> gamma gamma"
                                               , DaughtersCuts  = {'gamma' : BsGG_DC_none}
                                               , CombinationCut = BsGG_CC_none
                                               , MotherCut      = BsGG_MC_none)
        _Bs2gammagamma_none.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})

    Bs2gammagamma_none = Selection(
        name+"_none",
        Algorithm = _Bs2gammagamma_none,
        RequiredSelections = [stdPhotons_clean])

    Bs2gg_seq = SelectionSequence("Bs2ggSeq", TopSelection=Bs2gammagamma_none)

    
    # topselL0 = TOSFilter(name+"_NoConvTOSLineL0",[Bs2gammagamma_none],"L0(Photon|Electron)Decision")
    # topselHLT1 = TOSFilter(name+"_NoConvTOSLineHLT1",[topselL0],"Hlt1(B2GammaGamma|B2GammaGammaHighMass)Decision")
    # topselHLT2 = TOSFilter(name+"_NoConvTOSLineHLT2",[topselHLT1],"Hlt2(RadiativeB2GammaGamma)Decision")

    #sel = TOSFilter("dummysel",[Bs2gammagamma_none],"Hlt1B2GammaGammaHighMassDecision")

    
    
    #dv.appendToMainSequence([Bs2gg_seq])
    
    #STRIPPING

    ## STRIPPING
    ##########################################################################
    from Gaudi.Configuration import *
    from Configurables import DaVinci, ChargedPP2MC, ChargedProtoParticleMaker
    from StrippingConf.Configuration import StrippingConf
    
    # Specify the name of your configuration
    confname='Bs2GammaGamma' #FOR USERS    
    
    # NOTE: this will work only if you inserted correctly the 
    # default_config dictionary in the code where your LineBuilder 
    # is defined.
    from StrippingSelections import buildersConf
    confs = buildersConf()
    from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
    
    streams = buildStreamsFromBuilder(confs,confname)

    #define stream names
    leptonicMicroDSTname   = 'Leptonic'
    charmMicroDSTname      = 'Charm'
    pidMicroDSTname        = 'PID'
    bhadronMicroDSTname    = 'Bhadron'
    mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
    dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                    "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]
    
    stripTESPrefix = 'Strip'
    
    from Configurables import ProcStatusCheck
    
    sc = StrippingConf( Streams = streams,
                        MaxCandidates = 2000,
                        MaxCombinations = 10000000,
                        AcceptBadEvents = False,
                        BadEventSelection = ProcStatusCheck(),
                        TESPrefix = stripTESPrefix,
                        ActiveMDSTStream = True,
                        Verbose = True,
                        DSTStreams = dstStreams,
                        MicroDSTStreams = mdstStreams )

    #Configure DaVinci
    #DaVinci().EvtMax = 10000
    dv.appendToMainSequence( [ sc.sequence() ] )
    dv.ProductionType = "Stripping"

    #Input

    from PhysConf.Selections import AutomaticData

    #input = AutomaticData( 'Phys/Bs2gg_none/Particles' )
    input = AutomaticData( 'Phys/Bs2GammaGamma_NoConvLine/Particles')
    input_sel = BenderSelection ( 'B2GammaGammaTuple' , input )

    

    dv.ProductionType = "Stripping"

    # #JET ISOLATION
    # from JetAccessories.ParticleFlow_Config import ParticleFlowConf
    # from JetAccessories.JetMaker_Config import JetMakerConf
    # from JetAccessories.HltJetConf import HltParticleFlowConf, HltJetBuilderConf
    # from GaudiKernel import SystemOfUnits as Units
    
    # from PhysConf.Filters import LoKi_Filters
    # from CommonParticles import StdLooseAllPhotons

    # from Configurables import FilterDesktop, FilterInTrees, FilterBan, ClearDaughters
    # from PhysSelPython.Wrappers import Selection, SelectionSequence,DataOnDemand

    # from Configurables import TisTosParticleTagger
    
    # ### Selection of gammas 
    
    # _cleanBs0 = TisTosParticleTagger("FilterBs0_TriggerTos")
    # _cleanBs0.TisTosSpecs = {"Hlt2RadiativeB2GammaGammaDecision%TOS":0,
    #                          "Hlt1B2GammaGammaHighMassDecision%TOS":0,
    #                          "Hlt1B2GammaGammaDecision%TOS":0,
    #                          "L0PhotonDecision%TOS":0,
    #                          "L0ElectronDecision%TOS":0}
    # cleanBs0 = Selection("Bs0TriggerTOS",RequiredSelections=[DataOnDemand("Phys/Bs2GammaGamma_NoConvLine/Particles")],Algorithm = _cleanBs0)
    # cleanBs0_seq = SelectionSequence("CleanBs0Seq", TopSelection=cleanBs0)
    # _gammas = FilterInTrees( "StrippedGammasDesktop",Code = "ABSID=='gamma'" )
    # gammas = Selection("StrippedGammas",Algorithm = _gammas,RequiredSelections=[DataOnDemand("Phys/Bs0TriggerTOS/Particles")])
    # gammas_seq = SelectionSequence("StrippedGammasSeq", TopSelection=gammas)

    # _clear = ClearDaughters('BsNoDaughtersDesktop')
    # clear = Selection("BsNoDaughters",Algorithm = _clear,RequiredSelections=[DataOnDemand("Phys/Bs0TriggerTOS/Particles")])
    # clear_seq = SelectionSequence("clearSeq", TopSelection=clear)

    # _stdphotons = FilterBan('FilterBanPhotons', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
    # stdphotons = Selection('StdBannedPhotons', Algorithm = _stdphotons,RequiredSelections=[DataOnDemand('Phys/StdLooseAllPhotons/Particles')])
    # stdphotons_seq = SelectionSequence('StdPhotonsSeq',TopSelection=stdphotons)

    # _stdpi0s = FilterBan('FilterBanMergedPi0s', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
    # stdpi0s = Selection('StdBannedMergedPi0s', Algorithm = _stdpi0s,RequiredSelections=[DataOnDemand('Phys/StdLooseMergedPi0/Particles')])
    # stdpi0s_seq = SelectionSequence('StdPi0sSeq',TopSelection=stdpi0s)

    # _stdresolvedpi0s = FilterBan('FilterBanResolvedPi0s', Code='ALL',InputBans=['Phys/StrippedGammas/Particles'])
    # stdresolvedpi0s = Selection('StdBannedResolvedPi0s', Algorithm = _stdresolvedpi0s,RequiredSelections=[DataOnDemand('Phys/StdLooseResolvedPi0/Particles')])
    # stdresolvedpi0s_seq = SelectionSequence('StdResolvedpi0sSeq',TopSelection=stdresolvedpi0s)

    # # dv.appendToMainSequence([
    # # cleanBs0_seq,
    # # clear_seq,
    # # gammas_seq,
    # # stdphotons_seq,
    # # stdpi0s_seq,
    # # stdresolvedpi0s_seq
    # # ])
    
    

    
    # pf_hltconf = HltParticleFlowConf('pf_hlt',
    #                              [
    #                               ['Particle','particle','Phys/StdBannedPhotons/Particles'],
    #                               ['Particle','particle','Phys/StdBannedResolvedPi0s/Particles'],
    #                               'Ks',
    #                               'Lambda',
    #                               'ChargedProtos',
    #                               'PrimaryVtxs',
    #                               ],
    #                              SprRecover=False,
    #                              EcalBest = False )

    # pf_hltconf_withB = HltParticleFlowConf('pf_hlt_withB',
    #                              [
    #                               'Photons',
    #                               'MergedPi0s',
    #                               'ResolvedPi0s',
    #                               'Ks',
    #                               'Lambda',
    #                               'ChargedProtos',
    #                               'NeutralProtos',
    #                               'EcalClusters',
    #                               'HcalClusters',
    #                               'EcalMatch',
    #                               'HcalMatch', 
    #                               'PrimaryVtxs',
    #                               ],
    #                              SprRecover=False,
    #                              EcalBest = False )
    
    
    # # dv.appendToMainSequence([pf_hltconf.seq,pf_hltconf_withB.seq])
    
    # R = [i/10. for i in range(1,15)]
    # jetmakers = []
    # jetmakers_withB = []
    # for r in R:
    #     jetmakers += [HltJetBuilderConf("StdJets-{}".format(r),                               
    #                             [
    #                             pf_hltconf.getOutputLocation(),
    #                             "Phys/StrippedGammas/Particles"             
    #                             ],
    #                            JetEcPath = '',
    #                            JetPtMin = 500*Units.MeV,
    #                            JetVrt = False,
    #                            JetR = r)]
    #     jetmakers_withB += [HltJetBuilderConf("StdJetsWithB-{}".format(r),                               
    #                            [
    #                             pf_hltconf_withB.getOutputLocation(),
    #                             'Phys/BsNoDaughters/Particles',
    #                             ],
    #                            InputBans = ["Phys/StrippedGammas/Particles"],
    #                            JetEcPath = '',
    #                            JetPtMin = 500*Units.MeV,
    #                            JetVrt = False,
    #                            JetR = r)]
     
        # dv.appendToMainSequence([jetmakers[-1].seq])
        # dv.appendToMainSequence([jetmakers_withB[-1].seq])


    if type(inputdata) == list:
        setData(inputdata,catalogs,grid =True,useDBtags = True)
    if type(inputdata) == str:
        importOptions("./data.py")

    caloPostCalib = True
    if caloPostCalib:
        importOptions('./caloPostCalib.py')
        from Configurables import CaloPostCalibAlg
        cpc = CaloPostCalibAlg('CaloPostCalib')
        cpc.Inputs=["Phys/Bs2GammaGamma_NoConvLine/Particles"]
        dv.UserAlgorithms  += [cpc]
    
    #Setting trigger lines for the job
    from DecayTreeTuple.Configuration import TupleToolMCTruth
    from Configurables import TupleToolTrigger, TupleToolTISTOS
    Trigger_List = [
          "L0ElectronDecision",
          "L0PhotonDecision",
          "L0MuonDecision",
          "L0DiMuonDecision",
          "Hlt1B2GammaGammaDecision",
          "Hlt1B2GammaGammaHighMassDecision",
          'Hlt1MBNoBiasDecision',
          "Hlt2RadiativeB2GammaGammaDecision"
        ]

    
    
    from DecayTreeTuple.Configuration import TupleToolMCTruth
    from Configurables import DecayTreeTuple
    from DecayTreeTuple.Configuration import *

    conf = {}
    tistostool = {}
    triggertool = {}
    conf["B2GammaGammaTuple"] = TupleToolMCTruth('B2GammaGammaTuple.TupleToolMCTruth')
    conf["B2GammaGammaTuple"].ToolList =[
                                         'MCTupleToolKinematic',
                                         'MCTupleToolHierarchy',
                                         'MCTupleToolReconstructed',
                                         ]
    tistostool["B2GammaGammaTuple"]  = TupleToolTISTOS('B2GammaGammaTuple.TupleToolTISTOS')
    tistostool["B2GammaGammaTuple"].Verbose = True
    tistostool["B2GammaGammaTuple"].TriggerList = Trigger_List

    from Configurables import TupleToolProtoPData, TupleToolCaloHypo
    protopdata = TupleToolProtoPData('B2GammaGammaTuple.TupleToolProtoPData')
    protopdata.DataList = [
                            "IsPhoton",
                            "IsNotE",
                            "IsNotH",
                            'CaloNeutralID',
                            "CaloNeutralSpd",
                            'CellID',
                            'CaloNeutralHcal2Ecal'
                            ]
    

    NeutralsHypo = TupleToolCaloHypo("B2GammaGammaTuple.TupleToolCaloHypo")
    #NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

    
    
    
    from Configurables import TupleToolConeIsolation
    ConeIsol = TupleToolConeIsolation('B2GammaGammaTuple.TupleToolConeIsolation',
                                      FillComponents=True,
                                      MinConeSize=1.0,
                                      
                                      SizeStep=0.35,
                                      MaxConeSize=2.05,
                                      Verbose=True,
                                      FillPi0Info=False,
                                      FillMergedPi0Info=False,
                                      FillAsymmetry=True,
                                      FillDeltas= False,
                                      ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                                      MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                                      )
    ConeIsol.Verbose=True
    ConeIsol.OutputLevel=3.


    from Configurables import TupleToolL0Calo
    
    L0Calo = TupleToolL0Calo('B2GammaGammaTuple.TupleToolL0Calo',
                             WhichCalo = "ECAL",
                             TriggerClusterLocation = "/Event/Trig/L0/FullCalo")

    
    from Configurables import EventTuple
    dv.appendToMainSequence(EventTuple("EventTuple"))
    dv.UserAlgorithms += [EventTuple("EventTuple")]
    
    #Add MCTruthTuple
    if dv.Simulation:
        importOptions("makeALPs.py")
        decay = "AxR0 -> ^gamma ^gamma "
    
        mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
        mctuple.Decay = decay
        mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
        mctuple.LoKi_All.Variables =  {
            'TRUEID' : 'MCID',
            'M'      : 'MCMASS',
            'THETA'  : 'MCTHETA',
            'PT'     : 'MCPT',
            'PX'     : 'MCPX',
            'PY'     : 'MCPY',
            'PZ'     : 'MCPZ',
            'ETA'    : 'MCETA',
            'PHI'    : 'MCPHI'
            }     

        dv.UserAlgorithms += [mctuple]
        
    dv.UserAlgorithms.append(input_sel)
    ## get/create application manager
    gaudi = appMgr()                  
    ## (1) create the algorithm with the given name
    algs = {}
    algs["B2GammaGammaTuple"]=  Alllg(input_sel)
    
        

        
        
    return SUCCESS

#=============================================================================

#=============================================================================
#Job steering
if __name__ == '__main__' :
    
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083521/0000/"
    files = map(lambda x: root + "00083521_0000108{}_5.ldst".format(x),range(0,5)+[8])
    files = ["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
    from os import listdir
    from os.path import isfile, join
    mypath = '/scratch17/adrian.casais/dsts/ALPs'
    #files = [join(mypath,f) for f in listdir(mypath) if isfile(join(mypath, f))]
    files= ['/scratch03/adrian.casais/diphotonanalysis/tupleProduction/Brunel.dst']
    files = ['/scratch48/adrian.casais/dsts/49100040_1100000_DST/49100040_1100000_0.dst']
    files = ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/STRIP.DST/00106723/0000/00106723_00000038_1.strip.dst']
    #files = ['/scratch48/adrian.casais/dsts/00106723_00000053_1.strip.dst']
    configure(inputdata = files, catalogs = [], castor = False, params = {})


    #run(1000) 


    import GaudiPython

    gaudi = GaudiPython.AppMgr()
    gaudi.initialize()
    gaudi.run(5000)
    gaudi.stop()
    gaudi.finalize()
    from ROOT import *
    f = TFile('tuple.root')
    t = f.Get('B2GammaGammaTuple/DecayTree')

    
#=============================================================================
#The END
#=============================================================================
