# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
import os
numbers = range(50,53)
for n in [40,42,44,46,47,48,50]:
#for n in [40]:
 j = Job()
 root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
 j.application = BenderModule(
                    module = root + "/BenderALPsTuple.py",
                    directory = '/home3/adrian.casais/cmtuser2/BenderDev_v33r8/',
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )

 
 j.name = "bender-ALPs-{}".format(n)
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 5 , ignoremissing=True)
 
 j.backend = Local()
 j.application.params = {
     "DDDBtag":"dddb-20170721-3",
     "CondDBtag":"sim-20190430-vc-md100",
     "Year":2018,
     "Simulation":True
         }
 j.inputfiles = [root + "/makeALPs.py"]
 j.outputfiles = [LocalFile('*.root')]

      

 root ='/scratch48/adrian.casais/dsts/491000{}_1100000_DST/'.format(n)
 data = [LocalFile(root+x) for x in os.listdir(root) if 'dst' in x]

 j.inputdata = data
 
 j.submit()
 
