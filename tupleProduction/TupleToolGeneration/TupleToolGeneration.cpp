/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TupleToolGeneration.h"

#include "Event/GenHeader.h"
#include "Kernel/ParticleID.h"

#include "GaudiAlg/ITupleTool.h"
#include "GaudiAlg/Tuple.h"
#include "Event/MCHeader.h"


#include "GaudiKernel/IRegistry.h" // IOpaqueAddress

//-----------------------------------------------------------------------------
// Implementation file for class : GenerationTupleTool
//
// 2008-07-01 : Patrick Koppenburg
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolGeneration::TupleToolGeneration( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IEventTupleTool>( this );
}

//=============================================================================

StatusCode TupleToolGeneration::fill( Tuples::Tuple& tuple ) {

  const std::string prefix = fullName();

  if ( msgLevel( MSG::DEBUG ) ) debug() << "TupleToolGeneration" << endmsg;

  const LHCb::MCParticles* mcpars = getIfExists<const LHCb::MCParticles>(LHCb::MCParticleLocation::Default);
  if ( !mcpars ) { return Warning( "NoGenHeader. You probably don't need this tool.", StatusCode::SUCCESS, 1 ); }

  std::vector<unsigned int> heaviestQuark;
  std::vector<int>          processType;
  unsigned int              hqEvent = 0;

  // quarks
  unsigned int hq = 2;
  for ( LHCb::MCParticles::const_iterator p = mcpars->begin();
        p != mcpars->end(); ++p ) {
    const LHCb::ParticleID pid( ( *p )->particleID() );
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Gen particle " << ( *p )->particleID() << " " << pid.hasQuark( LHCb::ParticleID::bottom ) << " "
                << pid.hasQuark( LHCb::ParticleID::charm ) << endmsg;
    if ( ( hq < LHCb::ParticleID::top ) && pid.hasQuark( LHCb::ParticleID::top ) ) {
      hq = LHCb::ParticleID::top;
      break;
    } else if ( ( hq < LHCb::ParticleID::bottom ) && pid.hasQuark( LHCb::ParticleID::bottom ) ) {
      hq = LHCb::ParticleID::bottom;
    } else if ( ( hq < LHCb::ParticleID::charm ) && pid.hasQuark( LHCb::ParticleID::charm ) ) {
      hq = LHCb::ParticleID::charm;
    } else if ( ( hq < LHCb::ParticleID::strange ) && pid.hasQuark( LHCb::ParticleID::strange ) ) {
      hq = LHCb::ParticleID::strange;
    }
  }
  heaviestQuark.push_back( hq );
  if ( hq > hqEvent ) hqEvent = hq;


bool test = true;
test &= tuple->farray( prefix + "ProcessType", processType, prefix + "Collisions", 20 );
test &= tuple->farray( prefix + "HeaviestQuark", heaviestQuark, prefix + "Collisions", 20 );
test &= tuple->column( prefix + "HeaviestQuarkInEvent", hqEvent );

return StatusCode( test );
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolGeneration )
