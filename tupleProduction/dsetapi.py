from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons, StdAllLoosePions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence, RebuildSelection
import os
import sys

#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'MDST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'dsetappi.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
# path = '/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.MDST/00095850/0000/'
# items = [os.path.join(path,f) for f in os.listdir(path) if os.path.isfile( os.path.join(path, f) )]
# #DaVinci().Input=['00095836_00001414_7.AllStreams.mdst']
# DaVinci().Input=items
DaVinci().RootInTES = "/Event/AllStreams"





dtt = DecayTreeTuple('DecayTree')
dtt.Decay ='[D+ -> ^(eta_prime -> ^pi+ ^pi- ^gamma) ^pi+]CC'
dtt.Inputs = [
    'Phys/D2EtaPrimeHD2PiEtaPrimePPGLine/Particles',
    ]
dtt.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     #"TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                     "TupleToolCaloHypo"
                     ]
# TupleToolProtoPData configuration

dtt.addTupleTool("TupleToolProtoPData")
dtt.TupleToolProtoPData.DataList = ["CaloNeutralID","CaloNeutralSpd","IsNotH","IsNotE","IsPhoton"]

if DaVinci().getProp('Simulation'):
    mc_truth = dtt.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        dtt.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]



dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
dtt.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA' : 'ETA',
    'PHI': 'PHI'
            }
dtt.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
dtt.MCLoKi_All.Variables =  {
    'TRUEETA'    : 'MCETA',
    'TRUEPHI'    : 'MCPHI',
    'TRUEID': 'MCID'
    #'isPhoton' : 'ISPHOTON'
            }
                                        

DaVinci().UserAlgorithms += [dtt]

# import GaudiPython

# gaudi = GaudiPython.AppMgr()
# TES = gaudi.evtsvc()
# gaudi.run(20000)

