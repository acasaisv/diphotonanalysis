def getPFNs(job,outputfile):
    pfns = job.backend.getOutputDataAccessURLs()
    with open(outputfile,"w") as f:
        for pfn in pfns:
            f.write("{}\n".format(pfn))


def getLFNs(job,outputfile):
    lfns = []
    for sj in job.subjobs:
        lfns.append(sj.outputfiles[0].lfn)
    with open(outputfile,"w") as f:
        for lfn in lfns:
            f.write("{}\n".format(lfn))

def readTxt(filename):
    with open(filename,'r') as f:
        content = f.readlines()
        return [x.strip() for x in content]
