import os
numbers = range(50,53)
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')
options = [
    #'./caloPostCalib.py',
    'ALPsFromStrippingBs2gg.py',
    ]
dddb =  {10:"dddb-20220927-2018",9:"dddb-20170721-3"}
condb = {10:"sim-20201113-8-vc-{}100-Sim10",9:"sim-20190430-vc-{}100"}
for n in [40,41,42,43,44,45,46,47,48,49,50,51]:
    for pol in ['md','mu']:
        j = Job()
        root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
        j.application = myApp
        j.application.platform = 'x86_64-centos7-gcc62-opt'

        j.name = "ALPs-{}".format(n)

        j.splitter = SplitByFiles ( filesPerJob = 10 , ignoremissing=True)
        j.application.options= options
        j.backend = Dirac()
        j.inputfiles = ["./makeALPs.py",'./caloPostCalib.py']
        j.outputfiles = [DiracFile('*.root')]

        j.application.extraOpts = '''from Configurables import DaVinci\nDaVinci().DDDBtag  = "{0}"\nDaVinci().CondDBtag  = "{1}"
        '''.format(dddb[10],condb[10].format(pol))
        sufix = 'MagUp'
        if pol=='md':
            sufix = 'MagDown'

        bkk_i = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-MadgraphPythia8/Sim10b/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/491000{}/ALLSTREAMS.DST'.format(sufix,n)

        j.inputdata = BKQuery(bkk_i).getDataset()


        j.submit()
 
