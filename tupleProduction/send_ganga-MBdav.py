# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
numbers = [51,52]
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')
options = [
    'ALPsFromStrippingBs2gg.py',
    ]

for pol in ['MagDown','MagUp']:
    j = Job()
    root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc62-opt'

    #j.name= "bender-MB"

    j.splitter = SplitByFiles ( filesPerJob = 10 , ignoremissing=True)
    j.application.options= options
    j.inputfiles = ["./makeALPs.py",'./caloPostCalib.py']

    j.name = "MB{}-filtered".format(pol)


    j.backend = Dirac()
    j.outputfiles = [LocalFile('*.root')]

    bkk_data = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim09k/Trig0x617d18a4/Reco18/30000000/LDST'.format(pol)

    j.inputdata = BKQuery(bkk_data).getDataset()
    j.submit()
 
