#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser2/DaVinciDev_v44r10p7')

j = Job(name='etammg-MC-MU')
options = ['etammg.py',
           './caloPostCalib.py',
           ]


extraopt = 'import os\n'\
'os.environ["GRINDEX"] = 0\n'
j.application = myApp

j.application.options = options
j.application.platform = 'x86_64-slc6-gcc62-opt'
root ='/scratch36/xabier.cid/23173200/'
data = [LocalFile(root+x) for x in os.listdir(root) if 'ldst' in x]
j.inputdata = data     # access only the first 2 files of data
j.backend = Local()
j.splitter = SplitByFiles(filesPerJob=10)
j.outputfiles = [LocalFile('*.root')]
j.submit()
