from GaudiConf import IOHelper
import sys
ind1,ind2 = 0,0

# if len(sys.argv)>1:
#     ind1 = int(sys.argv[1])
#     ind2 = int(sys.argv[1]) #no need for ind1 ....

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]
                    
mainpath = "LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000/MC2016_Priv_md_30000000_{}.ldst"
onlyfiles = map(lambda x: mainpath+"MC2016_Priv_md_30000000_Merged_{0}.ldst".format(x), range(0,373))
mychunks = list(chunks(onlyfiles,38)) #10 chunks
inpdsts = mychunks[ind2]

IOHelper('ROOT').inputFiles(onlyfiles)
