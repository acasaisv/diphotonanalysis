#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os

myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')

for pol in ['md','mu']:
    j = Job(name='b02kstgamma-MC-{}'.format(pol))
    options = ['extraopts-{}-Data.py'.format(pol),
               'b02kstargamma.py'               
               ]


    j.application = myApp
    
    j.application.options = options
    

    
    j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
    j.application.platform = 'x86_64-centos7-gcc62-opt'

    polarity = 'MagUp'
    if pol == 'md': polarity='MagDown'
    bkk_data ='/LHCb/Collision18/Beam6500GeV-VeloClosed-{}/Real Data/Reco18/Stripping34r0p1/90000000/BHADRONCOMPLETEEVENT.DST'.format(polarity)
    
    j.inputdata = BKQuery(bkk_data).getDataset()
         

    j.backend = Dirac()

    j.inputfiles=['./caloPostCalib.py']
    j.splitter = SplitByFiles(filesPerJob=50)
    j.outputfiles = [DiracFile('*.root')]
    j.submit()
