#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os

myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')

for pol in ['md','mu']:
    j = Job(name='b02kstgamma-MC-{}'.format(pol))
    options = ['extraopts-{}-MC.py'.format(pol),
               'b02kstargamma.py'
           
               ]


    j.application = myApp
    
    j.application.options = options
    

    j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
    j.application.platform = 'x86_64-centos7-gcc62-opt'
    
    polarity = 'MagUp'
    if pol == 'md': polarity='MagDown'
    bkk_data = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim09l-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34r0p1Filtered/11102453/BEAUTY2XGAMMA.STRIP.DST'.format(polarity)
    j.inputdata = BKQuery(bkk_data).getDataset()
         

    j.backend = Dirac()
    j.splitter = SplitByFiles(filesPerJob=20)
    j.inputfiles=['./caloPostCalib.py']
    j.outputfiles = [DiracFile('*.root')]
    j.submit()
