# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
numbers = [51,52]
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')
#myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v46r3')
options = [
    'ALPsFromStrippingBs2gg.py',
    ]

dddb =  {10:"dddb-20220927-2018"}
condb = {10:"sim-20201113-8-vc-{}100-Sim10"}
bkk = {10:'/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim10b/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/11100400/ALLSTREAMS.DST'}
for n in [10]:
    for pol in ['md','mu']:
        j = Job()
        root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
        j.application = myApp
        j.application.platform = 'x86_64-centos7-gcc62-opt'
        #j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
        
        
        j.splitter = SplitByFiles ( filesPerJob = 5 , ignoremissing=True)
        j.application.options= options
        j.inputfiles = ["./makeALPs.py",'./caloPostCalib.py']
        
        j.application.extraOpts = '''from Configurables import DaVinci\nDaVinci().DDDBtag  = "{0}"\nDaVinci().CondDBtag  = "{1}"
                                    

        '''.format(dddb[n],condb[n].format(pol))
        j.name = "B2pi0pi0-sim{0}".format(n)
 
 
        j.backend = Dirac()
        j.outputfiles = [DiracFile('*.root')]
        sufix = 'MagUp'
        if pol=='md':
            sufix = 'MagDown'

        bkk_i = bkk[n].format(sufix)

        j.inputdata = BKQuery(bkk_i).getDataset()
        j.submit()
 
