from DecayTreeTuple.Configuration import *
from Gaudi.Configuration import *
from Configurables import *
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons, StdAllLoosePions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence, RebuildSelection
import os
import sys
from Configurables import TupleToolL0Calo
#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'MDST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'b2gg.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
# path = '/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.MDST/00095850/0000/'
# items = [os.path.join(path,f) for f in os.listdir(path) if os.path.isfile( os.path.join(path, f) )]
# #DaVinci().Input=['00095836_00001414_7.AllStreams.mdst']
# DaVinci().Input=items
DaVinci().RootInTES = "/Event/Leptonic"


dtt = DecayTreeTuple('DecayTree')
dtt.Decay ='B_s0 -> ^gamma ^gamma'
dtt.Inputs = [
    'Phys/Bs2GammaGamma_NoConvLine/Particles',
    ]
dtt.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     #"TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                     #"TupleToolCaloHypo",
                     #"TupleToolCaloHypo"
                     ]
# TupleToolProtoPData configuration
triglist = [
    #"L0MuonDecision",
    #"L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    #'Hlt1MBNoBiasDecision',
    #'Hlt1B2PhiGamma_LTUNBDecision',
    "Hlt2RadiativeB2GammaGammaDecision",
    #'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision',
    #'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision',
    #'Hlt2CaloPIDBd2KstGammaTurboCalibDecision',
    #'Hlt2RadiativeB2PhiGammaUnbiasedDecision'
    ]

TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]
NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
dtt.addTupleTool(TISTOS)
dtt.addTupleTool("TupleToolProtoPData")
dtt.addTupleTool("TupleToolCaloHypo")
dtt.TupleToolProtoPData.DataList = ["IsPhoton",
                                    "IsNotE",
                                    "IsNotH",
                                    'Saturation',
                                    'IsPhotonXGB',
                                    'ClusterAsX',
                                    'ClusterAsY',
                                    'CaloClusterCode',
                                    'CaloClusterFrac',
                                    'CaloShapeKappa',
                                    'CaloShapeAsym',
                                    'CaloShapeE1',
                                    'CaloShapeE2',
                                    'CaloPrsShapeE1',
                                    'CaloPrsShapeE2',
                                    'CaloPrsShapeEmax',
                                    'CaloPrsShapeFr2',
                                    'CaloPrsShapeAsym',
                                    'CaloPrsM',
                                    'CaloPrsM15',
                                    'CaloPrsM30',
                                    'CaloPrsM45',
                                    'CaloTrMatch',
                                    'CaloNeutralID',
                                    'CaloTrajectoryL',
                                    'CaloNeutralE19',
                                    'CaloPrsNeutralE19',
                                    'CaloPrsNeutralE49',
                                    'CaloNeutralPrsM',
                                    'CaloShapeFr2r4',
                                    'CaloPrsNeutralE4max',
                                    'CaloNeutralHcal2Ecal',
                                    'CaloNeutralEcal',
                                    'CaloNeutralPrs',
                                    'CaloNeutralSpd',
                                    'ClusterMass',
                                    'ShowerShape',
                                    'CaloDepositID',
                                    'CaloChargedID'



                                    ]
dtt.addBranches({'B':'B_s0 -> gamma gamma',
                'Gamma1':'B_s0 -> ^gamma gamma',
                'Gamma2':'B_s0 -> gamma ^gamma'})

dtt.Gamma1.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
dtt.Gamma1.gammaL0ECalo.WhichCalo = "ECAL"
dtt.Gamma1.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
dtt.Gamma1.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]
dtt.Gamma1.addTupleTool(NeutralsHypo)
dtt.Gamma2.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
dtt.Gamma2.gammaL0ECalo.WhichCalo = "ECAL"
dtt.Gamma2.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
dtt.Gamma2.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]
dtt.Gamma2.addTupleTool(NeutralsHypo)

cone_vars = ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']
cone_angles = ["1.0","1.35","1.7"]
RelInfo =dtt.B.addTupleTool('LoKi::Hybrid::TupleTool/RelInfo')
conevars = {}

for par in ['B','Gamma1','Gamma2']:
    for angle in cone_angles:
        for conevar in cone_vars:
            loc = '/Event/Leptonic/Phys/Bs2GammaGamma_NoConvLine/ConeVarsInfo/{0}/{1}'.format(par,angle)
            conevars['{0}_{1}_{2}'.format(par,conevar,angle.replace('.','_'))] = "RELINFO('{0}','{1}', -1.)".format(loc,conevar)
        
            RelInfo.Variables = conevars

from Configurables import CaloPostCalibAlg
cpc = CaloPostCalibAlg('CaloPostCalib')

cpc.Inputs=dtt.Inputs
myprescaler = DeterministicPrescaler('B2GGPrescaler',AcceptFraction=0.04)
myseq = GaudiSequencer('dtt')
myseq.Members += [myprescaler,cpc,dtt]
DaVinci().UserAlgorithms += [myseq]
importOptions('./caloPostCalib.py')

#DaVinci().UserAlgorithms  += [cpc]
#DaVinci().UserAlgorithms += [dtt]

if __name__=='__main__':
    from Gaudi.Configuration import importOptions
    importOptions('data.py')
    importOptions('pool_xml_catalog.py')
    import GaudiPython
    
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    gaudi.run(10000)
    # for i in range(1000):
    #     gaudi.run(1)
    #     #if TES['/Event/Leptonic/Phys/Bs2GammaGamma_NoConvLine/Particles']:
    #     #    break
    gaudi.stop()
    gaudi.finalize()
    from ROOT import *
    f = TFile('b2gg.root')
    t=f.Get('DecayTree/DecayTree')
    

