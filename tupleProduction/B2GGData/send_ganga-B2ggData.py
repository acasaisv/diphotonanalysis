#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
import os
import random
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')


for pol in [#'md',
            'mu'
            ]:
    j = Job(name='B2gg-data-{}'.format(pol))
    options = ['B2ggFromData.py',
               './caloPostCalib.py',
               ]


    j.application = myApp
    
    j.application.options = options
    

    j.application.platform = 'x86_64-centos7-gcc9-opt'
    j.application.platform = 'x86_64-centos7-gcc62-opt'

    polarity = 'MagUp'
    if pol == 'md': polarity='MagDown'
    bkk_data = '/LHCb/Collision18/Beam6500GeV-VeloClosed-{}/Real Data/Reco18/Stripping34/90000000/LEPTONIC.MDST'.format(polarity)
    raw_dataset = BKQuery(bkk_data).getDataset()
    lfns = [x.lfn for x in raw_dataset]
    lfns = random.sample(lfns,k=int(0.0001*len(lfns)))
    datalink = LHCbCompressedDataset()
    for l in lfns: datalink.append(l)
    j.inputdata =  raw_dataset
    
    j.backend = Dirac()
    j.splitter = SplitByFiles(filesPerJob=50,ignoremissing=True)
    j.outputfiles = [LocalFile('*.root')]
    j.submit()
