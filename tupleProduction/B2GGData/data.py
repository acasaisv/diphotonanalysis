#-- GAUDI jobOptions generated on Mon Feb 15 19:33:21 2021
#-- Contains event types : 
#--   90000000 - 20 files - 7012948 events - 79.57 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco18/Stripping34' 

#--  StepId : 133756 
#--  StepName : Stripping34-Merging-DV-v44r4-AppConfig-v3r361-LZMA4-Compression 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r4 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20171030-3 
#--  CONDDB : cond-20180202 
#--  ExtraPackages : AppConfig.v3r361;Det/SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00006432_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00001937_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00000947_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00004856_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00000778_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00006308_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00005802_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00002548_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00005373_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00004724_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00005353_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00002198_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00000605_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00005144_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00004361_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00006784_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00002727_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00002900_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00001664_1.leptonic.mdst',
'LFN:/lhcb/LHCb/Collision18/LEPTONIC.MDST/00077434/0000/00077434_00001320_1.leptonic.mdst',
], clear=True)
