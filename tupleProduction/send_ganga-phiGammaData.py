for pol in ['MagDown','MagUp']:
 j = Job()
 root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
 j.application = BenderModule(
                    module = root + "/phiGamma.py",
                    directory = "/home3/adrian.casais/cmtuser/BenderDev_v32r9/",
                    platform = "x86_64-centos7-gcc7-opt",
                    )

 
 j.name = "phiGamma-data-{}".format(pol)

 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2018,"Polarity":pol,"Simulation":False,"DDDBTag":"dddb-20171030-3","CondDBTag":"cond-20180202",'OutputName':'NTUPLE.root'}
 j.inputfiles = [root + "/makeALPs.py",'caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 

 bkk_data = "/LHCb/Collision18/Beam6500GeV-VeloClosed-{}/Real Data/Reco18/Stripping34/90000000/BHADRONCOMPLETEEVENT.DST".format(pol)

 
 j.inputdata = BKQuery(bkk_data).getDataset()
 
 
 j.submit()
 
