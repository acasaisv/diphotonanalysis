myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')

j = {}
options = [
           './caloPostCalib.py',
           'etammg.py',
           ]

polarity = 'MagUp'
for pol in ['U','D']:
#for pol in ['D']:
    j[pol] = Job(name=f'etammg 2018 MC - M{pol}')
    j[pol].application = myApp

    j[pol].application.options = options
    j[pol].application.autoDBtags = True
    j[pol].application.platform = 'x86_64-centos7-gcc62-opt'
    if pol=='D':
        polarity='MagDown'
    bkPath = f'/MC/2018/Beam6500GeV-2018-{polarity}-Nu1.6-25ns-Pythia8/Sim10b/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/39112231/ALLSTREAMS.DST'
    bkk = bkPath
    data = BKQuery(bkk).getDataset()
    j[pol].inputdata = data     
    j[pol].backend = Dirac()
    j[pol].splitter = SplitByFiles(filesPerJob=50)
    j[pol].outputfiles = [DiracFile('*.root')]
    j[pol].submit()
