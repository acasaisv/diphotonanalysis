/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TupleToolL0Calo.h"

#include "CaloDAQ/ICaloTriggerAdcsFromRaw.h"
#include "CaloInterfaces/IPart2Calo.h"
#include "Event/L0CaloAdc.h"
#include "Event/L0CaloCandidate.h"
#include "Event/L0DUBase.h"
#include "Event/Particle.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Kernel/IParticleTupleTool.h"

#include "TMath.h"
#include "boost/foreach.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : L0CaloTupleTool
//
// 2012-10-11 : Alexandra Martin Sanchez
//-----------------------------------------------------------------------------

using namespace Gaudi;
using namespace LHCb;

// parameters
namespace {
  // HCAL constants
  const double HCAL_CellSize_Inner = 131.3;
  // const double HCAL_CellSize_Outer = 262.6;  // commented out since unused
  const double HCAL_xMax_Inner = 2101;
  const double HCAL_yMax_Inner = 1838;
  const double HCAL_xMax_Outer = 4202;
  const double HCAL_yMax_Outer = 3414;

  // ECAL constants
  const double ECAL_CellSize_Inner = 40.4;
  // const double ECAL_CellSize_Middle = 60.6;   // commented out since unused
  // const double ECAL_CellSize_Outer  = 121.2;  // commented out since unused
  const double ECAL_xMax_Inner  = 970;
  const double ECAL_yMax_Inner  = 725;
  const double ECAL_xMax_Middle = 1940;
  const double ECAL_yMax_Middle = 1210;
  const double ECAL_xMax_Outer  = 3880;
  const double ECAL_yMax_Outer  = 3150;
} // namespace

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolL0Calo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolL0Calo::TupleToolL0Calo( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent )
    , m_fillTriggerEt( false )
    , m_caloDe( 0 )
    , m_adcsHcal( NULL )

{
  declareInterface<IParticleTupleTool>( this );
  declareProperty( "WhichCalo", m_calo = "HCAL" );
  declareProperty( "TriggerClusterLocation", m_location = "" );
}

//=============================================================================

StatusCode TupleToolL0Calo::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  m_part2calo = tool<IPart2Calo>( "Part2Calo", "Part2Calo", this );

  // Check selected calo is valid
  if ( m_calo != "HCAL" && m_calo != "ECAL" ) {
    err() << "TupleToolL0Calo -- Invalid calo: " << m_calo << "." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( m_calo == "HCAL" ) {
    m_caloDe   = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
    m_adcsHcal = tool<ICaloTriggerAdcsFromRaw>( "CaloTriggerAdcsFromRaw", "HcalTriggerAdcTool" );
  } else
    m_caloDe = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

  // Fill trigger info ?
  if ( "" != m_location ) m_fillTriggerEt = true;

  return sc;
}

//=============================================================================
StatusCode TupleToolL0Calo::fill( const LHCb::Particle* /* mother */, const LHCb::Particle* P, const std::string& head,
                                  Tuples::Tuple& tuple ) {

  bool test = true;
  if ( !( P->isBasicParticle() ) ) return StatusCode( test );

  const std::string prefix = fullName( head );

  double trackET (-1), xProjection (-1), yProjection(-1);
  double triggerET( -1. ), triggerHCALET( -1. ), xtrigger( 0. ), ytrigger( 0. );
  const LHCb::ProtoParticle* proto =  P->proto();
  auto hypos = proto->calo();
  if ( hypos.size()>0 && proto!=NULL ){
    auto x = hypos[0]->position()->x();
    auto y = hypos[0]->position()->y();
    auto z = hypos[0]->position()->z();
    auto E = hypos[0]->e();
    // printf("Loaded positions of Hypo!\n");

    
    //LHCb::CaloCellID CellID =  LHCb::CaloCellID(proto->info(LHCb::ProtoParticle::CaloNeutralID,0.));
    //if (CellIDcluster.calo() != CellID.calo()) printf("The CellIDs are different! \n");
  // Calculate a few observables
  trackET =
      TMath::Sqrt( TMath::Power( x, 2 ) + TMath::Power( y, 2 ) ) /
      TMath::Sqrt( TMath::Power( x, 2 ) + TMath::Power( y, 2 ) + TMath::Power( z, 2 ) ) * E;
  xProjection = x;
  yProjection = y;
  
  if ( m_fillTriggerEt ) {
      //LHCb::CaloCellID CellID = hypos[0]->clusters()[0]->seed();
      //triggerET = getAssociatedCluster( CellID, xtrigger, ytrigger );
      triggerET = getAssociatedClusterDistance( x,y, xtrigger, ytrigger ); }
  }
  

  

  // Fill the tuple
  if ( m_calo == "HCAL" ) {
    test &= tuple->column( prefix + "_L0Calo_HCAL_realET", trackET );
    test &= tuple->column( prefix + "_L0Calo_HCAL_xProjection", xProjection );
    test &= tuple->column( prefix + "_L0Calo_HCAL_yProjection", yProjection );
    test &= tuple->column( prefix + "_L0Calo_HCAL_region", isinside_HCAL( xProjection, yProjection ) );
    test &= tuple->column( prefix + "_L0Calo_HCAL_TriggerET", triggerET );
    test &= tuple->column( prefix + "_L0Calo_HCAL_TriggerHCALET", triggerHCALET );
    test &= tuple->column( prefix + "_L0Calo_HCAL_xTrigger", xtrigger );
    test &= tuple->column( prefix + "_L0Calo_HCAL_yTrigger", ytrigger );
  } else if ( m_calo == "ECAL" ) {
    test &= tuple->column( prefix + "_L0Calo_ECAL_realET", trackET );
    test &= tuple->column( prefix + "_L0Calo_ECAL_xProjection", xProjection );
    test &= tuple->column( prefix + "_L0Calo_ECAL_yProjection", yProjection );
    test &= tuple->column( prefix + "_L0Calo_ECAL_region", isinside_ECAL( xProjection, yProjection ) );
    test &= tuple->column( prefix + "_L0Calo_ECAL_TriggerET", triggerET );
    test &= tuple->column( prefix + "_L0Calo_ECAL_xTrigger", xtrigger );
    test &= tuple->column( prefix + "_L0Calo_ECAL_yTrigger", ytrigger );
  }

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Returns " << test << endmsg;
  return StatusCode( test );
}

int TupleToolL0Calo::isinside_HCAL( double x, double y ) {
  bool inside, inner, outer;
  inside = true;
  inner  = false;
  outer  = false;

  // projection inside calo
  if ( TMath::Abs( x ) < HCAL_xMax_Outer && TMath::Abs( y ) < HCAL_yMax_Outer ) {
    // projection inside inner calo (else is outer calo)
    if ( TMath::Abs( x ) < HCAL_xMax_Inner && TMath::Abs( y ) < HCAL_yMax_Inner ) {
      // projections outside the beampipe (in x)
      if ( TMath::Abs( x ) > 2 * HCAL_CellSize_Inner ) inner = true;
      // projections outside the beampipe (not in x, so in y)
      else if ( TMath::Abs( y ) > 2 * HCAL_CellSize_Inner )
        inner = true;
      else
        inside = false;
    } else
      outer = true;
  } else
    inside = false;

  if ( !inside )
    return -1;
  else if ( inner )
    return 1;
  else if ( outer )
    return 0;
  else
    return -999;
}

int TupleToolL0Calo::isinside_ECAL( double x, double y ) {
  bool inside, inner, middle, outer;
  inside = true;
  inner  = false;
  middle = false;
  outer  = false;

  // projection inside calo
  if ( TMath::Abs( x ) < ECAL_xMax_Outer && TMath::Abs( y ) < ECAL_yMax_Outer ) {
    // projection inside middle calo (else is outer calo)
    if ( TMath::Abs( x ) < ECAL_xMax_Middle && TMath::Abs( y ) < ECAL_yMax_Middle ) {
      // projection inside inner calo
      if ( TMath::Abs( x ) < ECAL_xMax_Inner && TMath::Abs( y ) < ECAL_yMax_Inner ) {
        // projections outside the beampipe (in x)
        if ( TMath::Abs( x ) > 2 * ECAL_CellSize_Inner ) inner = true;
        // projections outside the beampipe (not in x, so in y)
        else if ( TMath::Abs( y ) > 2 * ECAL_CellSize_Inner )
          inner = true;
        else
          inside = false;
      } else
        middle = true;
    } else
      outer = true;
  } else
    inside = false;

  if ( !inside )
    return -1;
  else if ( inner )
    return 2;
  else if ( middle )
    return 1;
  else if ( outer )
    return 0;
  else
    return -999;
}

//=============================================================================
// Get associated L0 or LLT cluster
//=============================================================================
double TupleToolL0Calo::getAssociatedCluster( LHCb::CaloCellID centerCell, double& xTrigger, double& yTrigger ) {
  // First get the CALO cells in the 3x3 cluster around the track projection
  std::vector<LHCb::CaloCellID> cells3x3;
  cells3x3.push_back( centerCell );
  BOOST_FOREACH ( LHCb::CaloCellID cell, m_caloDe->neighborCells( centerCell ) ) { cells3x3.push_back( cell ); };
  std::sort( cells3x3.begin(), cells3x3.end() );

  // loop over the L0 candidates
  LHCb::L0CaloCandidates* candidates = getIfExists<LHCb::L0CaloCandidates>( m_location );

  LHCb::L0CaloCandidates::iterator cand;
  double                           result = -1.;

  float higherPT = -1;
  for ( cand = candidates->begin(); candidates->end() != cand; ++cand ) {
    LHCb::L0CaloCandidate* theCand = ( *cand );
    if ( (theCand->type() == L0DUBase::CaloType::Photon) || (theCand->type() == L0DUBase::CaloType::Electron)  )
    //if (true)
    {
      LHCb::CaloCellID cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8, cell9;
      cell1 = theCand->id();
      cell2 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() + 1, cell1.col() );
      cell3 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row(), cell1.col() + 1 );
      cell4 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() + 1, cell1.col() + 1 );
      cell5 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() - 1, cell1.col()  );
      cell6 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() , cell1.col() - 1 );
      cell7 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() - 1, cell1.col() - 1 );
      cell8 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() + 1, cell1.col() - 1 );
      cell9 = LHCb::CaloCellID( cell1.calo(), cell1.area(), cell1.row() - 1, cell1.col() + 1 );
      

      // std::vector<LHCb::CaloCellID> candcells3x3;
      // candcells3x3.push_back( cell1 );
      // BOOST_FOREACH ( LHCb::CaloCellID itcell, m_caloDe->neighborCells( cell1 ) ) { candcells3x3.push_back( itcell ); };
      // std::sort(candcells3x3.begin(),candcells3x3.end());
      // bool condition = false;
      // for(auto it=std::begin(cells3x3); it!=std::end(cells3x3); ++it){
      //   condition = std::binary_search(candcells3x3.begin(),candcells3x3.end(),*it);
      //   if (condition) break;
      //   }
      bool condition = (( std::binary_search( cells3x3.begin(), cells3x3.end(), cell1 ) ||
                         std::binary_search( cells3x3.begin(), cells3x3.end(), cell2 ) ||
                         std::binary_search( cells3x3.begin(), cells3x3.end(), cell3 ) ||
                         std::binary_search( cells3x3.begin(), cells3x3.end(), cell4 )) &&
                         (theCand->et() > higherPT)
                         // std::binary_search( cells3x3.begin(), cells3x3.end(), cell5 ) ||
                         // std::binary_search( cells3x3.begin(), cells3x3.end(), cell6 ) ||
                         // std::binary_search( cells3x3.begin(), cells3x3.end(), cell7 ) ||
                         // std::binary_search( cells3x3.begin(), cells3x3.end(), cell8 ) ||
                         // std::binary_search( cells3x3.begin(), cells3x3.end(), cell9 ) 
                         );
      if (condition){
      xTrigger = theCand->position().x();
      yTrigger = theCand->position().y();
      result   = theCand->et();
      higherPT = result;
      }
    }
  }

  return result;
}


double TupleToolL0Calo::getAssociatedClusterDistance( double xCell, double yCell, double& xTrigger, double& yTrigger ) {

  // loop over the L0 candidates
  LHCb::L0CaloCandidates* candidates = getIfExists<LHCb::L0CaloCandidates>( m_location );

  LHCb::L0CaloCandidates::iterator cand;
  double                           result = -1.;

  double higherPT = -1;
  double minDistance2 = 999999; 
  for ( cand = candidates->begin(); candidates->end() != cand; ++cand ) {
    LHCb::L0CaloCandidate* theCand = ( *cand );
    if ( (theCand->type() == L0DUBase::CaloType::Photon) || (theCand->type() == L0DUBase::CaloType::Electron)  )
    //if (true)
    {
      double xtrig = theCand->position().x();
      double ytrig = theCand->position().y();
      //double ettrig =theCand->et();
      double distance2 = (xCell-xtrig) * (xCell - xtrig) + (yCell-ytrig) * (yCell - ytrig);
      
      bool condition = ( distance2 < 200*200 && distance2 < minDistance2 && (theCand->et() > higherPT));
      if (condition){
      xTrigger = xtrig;
      yTrigger = ytrig;
      result   = theCand->et();
      higherPT = result;
      minDistance2 = distance2;
      }
    }
  }

  return result;
}