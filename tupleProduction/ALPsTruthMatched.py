from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence

#needed to handle ALPs
importOptions("makeALPs.py")
#initial DV confg
DaVinci().InputType = 'LDST' # LDST if Upgrade, DST if Run 2
DaVinci().TupleFile = 'ALP2GammaGamma.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
## this has be to be updated as well (below, case for ZMuMu)
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20170721-2-vc-md100"

X2YY                 = CombineParticles("MCSel_X2YY")
X2YY.Preambulo       = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
X2YY.DecayDescriptor = "AxR0 -> gamma gamma"
X2YY.DaughtersCuts   = {"gamma"  : "mcMatch('AxR0 -> ^gamma ^gamma')"}
#X2YY.DaughtersCuts = {"gamma": "mcMatch('gamma')"}
X2YY.MotherCut       = "mcMatch('AxR0 -> gamma gamma')"
#X2YY.addTool( LoKi__FastVertexFitter, name="LoKi::FastVertexFitter" )
X2YY.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})
#X2YY.MotherCut       = "ALL"
SelX2YY = Selection("SelX2YY",
                     Algorithm = X2YY,
                     RequiredSelections = [ StdLooseAllPhotons ])
SeqX2YY = SelectionSequence('SeqX2YY', TopSelection = SelX2YY)
dtt = DecayTreeTuple('DecayTree')
dtt.setDescriptorTemplate('AxR0 -> ${ALP_0_Gamma_0}gamma ${ALP_0_Gamma_1}gamma') # TO BE UPDATED FOR EACH MODE
dtt.Decay ='AxR0 -> ^gamma ^gamma'
dtt.Inputs = [ SeqX2YY.outputLocation() ]

#dtt.ToolList += [ #-- global event data
#                      "TupleToolEventInfo",
#                      "TupleToolPrimaries",
#                      "TupleToolCPU",
#                      #-- particle data
#                      "TupleToolKinematic",
#                      "TupleToolDalitz",
#                      "TupleToolPid",
#                      #"TupleToolPropertime",
#                      "TupleToolTrackInfo",
#                      # "TupleToolHelicity",      # private tool
#                      "TupleToolAngles",
#                      "TupleToolPhotonInfo",
#                      "TupleToolANNPID",           # includes all MCTuneV
#                      #"TupleToolCaloHypo"
#                      ]
# TupleToolProtoPData configuration

dtt.addTupleTool("TupleToolProtoPData")
dtt.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH"]
if DaVinci().getProp('Simulation'):
    mc_truth = dtt.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        dtt.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]



dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
dtt.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    #'TRUEID': 'MCID'
    #'isPhoton' : 'ISPHOTON'
            }
dtt.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
dtt.MCLoKi_All.Variables =  {
    'TRUEETA'    : 'MCETA',
    'TRUEPHI'    : 'MCPHI',
    'TRUEID': 'MCID'
    #'isPhoton' : 'ISPHOTON'
            }
                                        

DaVinci().UserAlgorithms += [SeqX2YY.sequence() , dtt]

from GaudiConf import IOHelper
mainpath = '/eos/lhcb/wg/QEE/PIDCalibUpgradeDSTs/2016_CONDITIONS_ALP_gammagamma/'
mainpath = '/castor/cern.ch/user/a/acasaisv/dsts/ALPs/5GeV_GenProdCuts_md_2016/'
onlyfiles = map(lambda x: "MC2016_Priv_MagDown_ALP5_{0}.ldst".format(x), range(5,39))
inputdata = []
for files in onlyfiles:
  if 'ALP5' in files:
    #inputdata.append("root://eoslhcb.cern.ch/"+mainpath+files)
    inputdata.append("root://castorlhcb.cern.ch/"+mainpath+files)

index = 0
if len(sys.argv)>1:
    index = int(sys.argv[1])
IOHelper().inputFiles([inputdata][index], clear=True)

# import GaudiPython
# gaudi = GaudiPython.AppMgr()
# gaudi.initialize()
# TES = gaudi.evtsvc()
# gaudi.run(1000)
# gaudi.stop()
# gaudi.finalize()

# for i in range(10000):
#     gaudi.run(1)
#     if TES["/Event/Phys/SelX2YY/Particles"] and TES["/Event/Phys/SelX2YY/Particles"].size():
#         print "HURRA"
#         break
