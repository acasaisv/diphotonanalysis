for pol in ['MagDown','MagUp']:
    _pol = 'md'
    if pol == 'MagUp': _pol='mu'
    j = Job()
    root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
    j.application = BenderModule(
        module = root + "/phiGamma.py",
        directory = "/home3/adrian.casais/cmtuser/BenderDev_v32r9/",
        platform = "x86_64-centos7-gcc7-opt",
        )

 
    j.name = "phiGamma-MC-{}".format(pol)
    
    
    j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
    
    j.backend = Dirac()
    j.application.params = {"Year":2018,"Polarity":pol,"Simulation":True,"DDDBTag":"dddb-20170721-3","CondDBTag":"sim-20190128-vc-{}100".format(_pol),'OutputName':'NTUPLE.root'}
    j.inputfiles = [root + "/makeALPs.py",'caloPostCalib.py']
    j.outputfiles = [DiracFile('*.root')]
    

    bkk_data = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/13102202/ALLSTREAMS.DST'.format(pol)
    

    
    j.inputdata = BKQuery(bkk_data).getDataset()
 
    
    j.submit()
 
