from   Gaudi.Configuration import *
import GaudiPython
from Configurables import DaVinci


## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, CombineParticles
from StandardParticles import StdLooseAllPhotons
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS

# NOTE: this will work only if you inserted correctly the
# default_config dictionary in the code where your LineBuilder
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder

streams = buildStreamsFromBuilder(confs,confname)

#define stream names
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#Configure DaVinci
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().ProductionType = "Stripping"



#importOptions("makeALPs.py")
X2YY                 = CombineParticles("MCSel_X2YY")
X2YY.Preambulo       = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
X2YY.DecayDescriptor = "B_s0 -> gamma gamma"
X2YY.DaughtersCuts   = {"gamma"  : "mcMatch('AxR0 -> ^gamma ^gamma')"}
#X2YY.DaughtersCuts   = {"gamma"  : "mcMatch('[Upsilon(1S) -> ^gamma ^gamma]CC')"}
#X2YY.DaughtersCuts   = {"gamma"  : "mcMatch('[B_s0 -> ^gamma ^gamma]CC')"}
X2YY.MotherCut       = "mcMatch('AxR0 -> gamma gamma')"
#X2YY.MotherCut       = "mcMatch('[B_s0 -> gamma gamma]CC')"
#X2YY.MotherCut       = "mcMatch('[Upsilon(1S) -> gamma gamma]CC')"

X2YY.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})
X2YY.MotherCut       = "ALL"
SelX2YY = Selection("SelX2YY",
                     Algorithm = X2YY,
                     RequiredSelections = [ StdLooseAllPhotons ])
SeqX2YY = SelectionSequence('SeqX2YY', TopSelection = SelX2YY)

datatype=2018
#############
DaVinci().Simulation = True
DaVinci().DataType = "2018"
DaVinci().DDDBtag  = "dddb-20170721-3"
DaVinci().CondDBtag  = "sim-20190430-vc-md100"

DaVinci().InputType = "DST" # if running on stripped data... change



from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
Butuple = DecayTreeTuple("DTTBs2GammaGamma")
Butuple.Decay = "B_s0 -> ^gamma ^gamma"
Butuple.Inputs = [SeqX2YY.outputLocation()]
#Butuple.Inputs = ["Phys/Bs2GammaGamma_NoConvLine"]

Butuple.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     'TupleToolRecoStats',
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     #"TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                     ]


from Configurables import TupleToolProtoPData, TupleToolCaloHypo,TupleToolL0Data
NeutralsProto = TupleToolProtoPData("NeutralsProto")
NeutralsProto.DataList = ["IsPhoton",
                          "IsNotE",
                          "IsNotH",
                          'CaloNeutralID',
                          "CaloNeutralSpd",
                          'CellID',
                          'CaloNeutralHcal2Ecal',
                          'CaloTrMatch',
                          'Saturation']


NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
#NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]



Butuple.addBranches({"B_s0": "B_s0 -> gamma gamma",
                     "gamma": "B_s0 -> ^gamma gamma",
                     "gamma0": "B_s0 -> gamma ^gamma",
                     })

Butuple.gamma.addTupleTool(NeutralsProto)
Butuple.gamma0.addTupleTool(NeutralsProto)

# Butuple.gamma.addTupleTool(NeutralsHypo)
# Butuple.gamma0.addTupleTool(NeutralsHypo)

Butuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
Butuple.gammaL0ECalo.WhichCalo = "ECAL"
Butuple.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
Butuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

Butuple.addTupleTool('TupleToolTISTOS/TISTOSTool')
Butuple.TISTOSTool.VerboseL0 = True
Butuple.TISTOSTool.VerboseHlt1 = True
Butuple.TISTOSTool.VerboseHlt2 = True
Butuple.TISTOSTool.TriggerList = ["L0ElectronDecision","L0PhotonDecision","Hlt1B2GammaGammaDecision","Hlt1B2GammaGammaHighMassDecision","Hlt2RadiativeB2GammaGammaDecision"]

Butuple.addTupleTool('TupleToolStripping/StrippingTool')
Butuple.StrippingTool.StrippingList=['StrippingBs2GammaGamma_NoConvLineDecision']

from Configurables import TupleToolConeIsolation
ConeIsol = TupleToolConeIsolation('TupleToolConeIsolation/B_s0',
                                      )

ConeIsol = Butuple.addTupleTool('TupleToolConeIsolation/ConeIsolB')
ConeIsol.FillComponents=True
ConeIsol.MinConeSize=1.0

ConeIsol.SizeStep=0.35
ConeIsol.MaxConeSize=2.05
ConeIsol.Verbose=False
ConeIsol.FillPi0Info=False
ConeIsol.FillMergedPi0Info=False
ConeIsol.FillAsymmetry=True
ConeIsol.FillDeltas= False
ConeIsol.ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
ConeIsol.MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"

RelInfo = Butuple.B_s0.addTupleTool('LoKi::Hybrid::TupleTool/RelInfo')
cone_vars = ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']
cone_angles = ["1.0","1.35","1.7"]
teslocation = "Phys/Bs2GammaGamma_NoConvLine"
conevars = {}
for angle in cone_angles:
    for conevar in cone_vars:
        loc = "Phys/Bs2GammaGamma_NoConvLine/ConeVarsInfo/B/" + angle
        conevars['{0}_{1}'.format(conevar,angle.replace('.','_'))] = 'RELINFO("'+loc+'","'+conevar+'", -1.)'

RelInfo.Variables = conevars

Butuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
Butuple.LoKi_All.Variables =  {
    #'THETA'  : 'THETA',
    'PT'     : 'PT',
    'ETA'    : 'ETA',
    #'PHI'    : 'MCPHI'
        }







L0Data = TupleToolL0Data('L0Data')
L0Data.DataList = ['*Et*']
#Butuple.addTupleTool(L0Data)

importOptions('./caloPostCalib.py')
from Configurables import CaloPostCalibAlg
cpc = CaloPostCalibAlg('CaloPostCalib')

cpc.Inputs=Butuple.Inputs
DaVinci().UserAlgorithms += [SeqX2YY.sequence()]
DaVinci().UserAlgorithms  += [cpc]


DaVinci().UserAlgorithms += [Butuple]

#EventTuple
from Configurables import EventTuple
DaVinci().UserAlgorithms += [EventTuple("EventTuple")]


DaVinci().TupleFile = "ALPsFromBs2ggStripping.root"

if DaVinci().getProp('Simulation'):
    mc_truth = Butuple.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")
    Butuple.addTupleTool('TupleToolGeneration')
    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    Butuple.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        Butuple.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
        
        decay = "AxR0 -> ^gamma ^gamma"
        #decay = "[Upsilon(1S) -> ^gamma ^gamma]CC"
        #decay = "[B_s0 -> ^gamma ^gamma]CC"
        #decay = "[B0 -> ^pi0 ^pi0]CC"

        mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
        mctuple.Decay = decay
        mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
        mctuple.LoKi_All.Variables =  {
            'TRUEID' : 'MCID',
            'M'      : 'MCMASS',
            'THETA'  : 'MCTHETA',
            'PT'     : 'MCPT',
            'PX'     : 'MCPX',
            'PY'     : 'MCPY',
            'PZ'     : 'MCPZ',
            'ETA'    : 'MCETA',
            'PHI'    : 'MCPHI'
            }

        DaVinci().UserAlgorithms += [mctuple]


#MAKE ALPS and MCTRUTH
importOptions("./makeALPs.py")
#importOptions("./MCTruthTuple.py")
#importOptions("./inputMB.py")



# =============================================================================

# =============================================================================
## Job steering
if __name__ == '__main__' :
    from os import listdir
    from os.path import isfile, join
    mypath = '/scratch48/adrian.casais/dsts/49100050_1100000_DST'
    files = [join(mypath,f) for f in listdir(mypath) if isfile(join(mypath, f)) and 'dst' in f]

    DaVinci().Input = files
    DaVinci().Input = ["/scratch47/adrian.casais/dsts/00185718_00000006_7.AllStreams.dst"]
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    ToolSvc = gaudi.toolSvc()

    gaudi.initialize()
    gaudi.run(3000)
    gaudi.stop()
    gaudi.finalize()

    from ROOT import *
    f = TFile("ALPsFromBs2ggStripping.root")
    t = f.Get('DTTBs2GammaGamma/DecayTree')
    truth = "abs(gamma_MC_MOTHER_ID) == 531 && abs(gamma0_MC_MOTHER_ID) == 531 && gamma_TRUEID==22 && gamma0_TRUEID==22 && abs(B_s0_TRUEID)==511"
    #print(t.Show(0))

#    ## make printout of the own documentations
#    print __doc__
#    import os
#    gaudi.run(1000)#2000)
#    TES = gaudi.evtsvc()
#    gaudi.stop()
#    gaudi.finalize()

# =============================================================================
# The END
# =============================================================================
