# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
for pol in ['MagDown','MagUp']:
 if pol=='MagDown':
     mag='md'
 else:
     mag='mu'
     
 j = Job()
 root = '/home3/adrian.casais/cmtuser/'
 j.application = BenderModule(
                    module = "./BenderALPsTuple.py",
                    directory = root + "/BenderDev_v33r3",
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )

 
 j.name = "B-JpsiK"+pol
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2018,"Polarity":pol,"Simulation":True,"DDDBTag":"dddb-20170721-3","CondDBtag":"sim-20190430-1-vc-{}100".format(mag)}
 j.inputfiles = [root + "/makeALPs.py",'./caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 
 bkk_data = '/MC/2018/Beam6500GeV-2018-{}-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/12143001/LDST'.format(pol)
 j.inputdata = BKQuery(bkk_data).getDataset()
 
 
 j.submit()
 
