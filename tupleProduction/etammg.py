from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter,TupleToolTISTOS,TupleToolCaloHypo, MCDecayTreeTuple
#from Configurables import *
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
import os

#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'DST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'eta2mmg.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
## this has be to be updated as well (below, case for ZMuMu)
#DaVinci().DDDBtag  = "dddb-20170721-3"
#DaVinci().CondDBtag  = "sim-20170721-2-vc-md100"
# if DaVinci().DataType=='2018':
#     DaVinci().DDDBtag  = "dddb-20170721-3"
#     DaVinci().CondDBtag  = "sim-20170721-2-vc-md100"

 

X2YY                 = CombineParticles("MCSel_X2YY")
X2YY.Preambulo       = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
X2YY.DecayDescriptor = "eta -> mu+ mu- gamma"
X2YY.DaughtersCuts   = {"gamma"  : "mcMatch('eta -> mu+ mu- ^gamma')",
                        "mu+"  : "mcMatch('eta -> ^mu+ mu- gamma')",
                        "mu-"  : "mcMatch('eta ->  mu+ ^mu- gamma')",
                        }
#X2YY.DaughtersCuts = {"gamma": "mcMatch('gamma')"}
X2YY.MotherCut       = "mcMatch('eta -> mu+ mu- gamma')"
#X2YY.addTool( LoKi__FastVertexFitter, name="LoKi::FastVertexFitter" )
#X2YY.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})
#X2YY.MotherCut       = "ALL"
SelX2YY = Selection("SelX2YY",
                     Algorithm = X2YY,
                     RequiredSelections = [ StdLooseAllPhotons,StdAllLooseMuons ])
SeqX2YY = SelectionSequence('SeqX2YY', TopSelection = SelX2YY)

caloPostCalib=True
if caloPostCalib:
    importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc_inputs = ['Phys/SelX2YY/Particles']
    cpc.Inputs=cpc_inputs
    DaVinci().UserAlgorithms  += [cpc]
    


dtt = DecayTreeTuple('DecayTree')
dtt.Decay ='eta -> ^mu+ ^mu- ^gamma'
dtt.addBranches({
    "eta"    : "eta",
    "mu1"    : "eta -> ^mu+  mu-  gamma",
    "mu2"    : "eta ->  mu+ ^mu-  gamma",
    "gamma"  : "eta ->  mu+  mu- ^gamma"})
dtt.Inputs = [ SeqX2YY.outputLocation() ]
triglist = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    'Hlt1MBNoBiasDecision',
    'Hlt1B2PhiGamma_LTUNBDecision',
    "Hlt2RadiativeB2GammaGammaDecision",
    'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision',
    'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision',
    'Hlt2CaloPIDBd2KstGammaTurboCalibDecision',
    'Hlt2RadiativeB2PhiGammaUnbiasedDecision'
    ]

from Configurables import TupleToolL0Calo
dtt.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
dtt.gammaL0ECalo.WhichCalo = "ECAL"
dtt.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
dtt.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]
NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy",'X','Y','Z','Saturation', 'CellID','Hcal2Ecal']
dtt.addTupleTool(NeutralsHypo)
TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]
dtt.addTupleTool(TISTOS)
dtt.ToolList += ['TupleToolPhotonInfo',
                 #'TupleToolCaloHypo',
                 'TupleToolEventInfo',"TupleToolRecoStats", 'TupleToolTrackInfo']
# TupleToolProtoPData configuration

dtt.addTupleTool("TupleToolProtoPData")
dtt.TupleToolProtoPData.DataList = ["CaloNeutralID","CaloNeutralSpd","IsNotH","IsNotE","IsPhoton","CaloNeutralHcal2Ecal"]

if DaVinci().getProp('Simulation'):
    mc_truth = dtt.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        dtt.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
    decay = "eta -> ^mu+ ^mu- ^gamma"

    mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    mctuple.Decay = decay
    mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
    mctuple.LoKi_All.Variables =  {
        'TRUEID' : 'MCID',
        'M'      : 'MCMASS',
        'THETA'  : 'MCTHETA',
        'PT'     : 'MCPT',
        'PX'     : 'MCPX',
        'PY'     : 'MCPY',
        'PZ'     : 'MCPZ',
        'ETA'    : 'MCETA',
        'PHI'    : 'MCPHI'
        }

    DaVinci().UserAlgorithms += [mctuple]




dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
dtt.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA': 'ETA',
    'PHI':'PHI'
            }
dtt.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
dtt.MCLoKi_All.Variables =  {
    'TRUEETA'    : 'MCETA',
    'TRUEPHI'    : 'MCPHI',
    'TRUEID': 'MCID'
    #'isPhoton' : 'ISPHOTON'
            }
                                        

DaVinci().UserAlgorithms += [SeqX2YY.sequence() , dtt]

