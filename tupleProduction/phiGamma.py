
from   Bender.Awesome      import *
from   Gaudi.Configuration import *
import GaudiPython
from Configurables       import CombineParticles
from Configurables import OfflineVertexFitter
from Configurables import SubstitutePID
import sys,os
from Configurables import DeterministicPrescaler
from Configurables import LoKi__HDRFilter as HltFilter
from Configurables import LoKi__L0Filter as L0Filter
from PhysSelPython.Wrappers import Selection, SelectionSequence, MergedSelection, DataOnDemand
from CommonParticles import StdAllLooseGammaConversion
from CommonParticles import StdLooseAllPhotons
from Configurables import FilterDesktop
from Configurables import DecayTreeTuple
# =============================================================================

# =============================================================================

mytriggerlist = ['L0ElectronDecision','L0PhotonDecision',
                 'Hlt1B2GammaGammaDecision',
                 'Hlt1B2GammaGammaHighMassDecision',
                 'Hlt1TrackMVADecision',
                 'Hlt1MBNoBiasDecision',
                 'Hlt1SingleElectronNoIPDecision',
                 'Hlt1B2PhiGamma_LTUNBDecision',
                 'Hlt1TwoTrackMVADecision',
                 'Hlt2RadiativeB2GammaGammaDecision',
                 'Hlt2RadiativeB2GammaGammaDDDecision',
                 'Hlt2RadiativeB2GammaGammaLLDecision',
                 'Hlt2RadiativeB2GammaGammaDoubleDecision',
                 'Hlt2RadiativeBs2PhiGammaDecision',
                 'Hlt2RadiativeBs2PhiGammaUnbiasedDecision'
                 ]


strip_loc = {"None":"Radiative/Phys/Bs2GammaGamma_NoConvLine/Particles",
             "DD":'Radiative/Phys/Bs2GammaGamma_DDLine/Particles',
             "LL":'Radiative/Phys/Bs2GammaGamma_LLLine/Particles',
             "Double":'Radiative/Phys/Bs2GammaGamma_doubleLine/Particles'}

## configure the job
def configure (datafiles,catalogs = [], castor = False ,
               params   = {'MC': "MC2016", 'Mag': 'MagUp', 'Year': 2016 , 'OutputName' : 'NTUPLE.root'}):


   """
   Configure the job
   """
   isPhi = True
   from Configurables import NTupleSvc, HistogramPersistencySvc, DaVinci, EventSelector, LHCbApp
   LHCbApp().OutputLevel=5
   
   DaVinci (
       Simulation = params['Simulation'],
       Lumi       = not params['Simulation']  ,
       DataType	= str(params['Year']),
       PrintFreq = 1000,
       # DDDBtag = params['DDDBTag'],
       # CondDBtag = params['CondDBTag']
       )

   if params['Simulation']:
      DaVinci().DDDBtag = params['DDDBTag']
      DaVinci().CondDBtag = params['CondDBTag']

   print params
   

   setData ( datafiles , catalogs , castor)
   if params['Simulation']:

      # kill old strip report
      from Configurables import EventNodeKiller
      eventNodeKiller = EventNodeKiller('Stripkiller')
      eventNodeKiller.Nodes = ['/Event/AllStreams','/Event/Strip']

      #DaVinci().EventPreFilters += [eventNodeKiller] #(before new stripping sequence)
      #STRIPPING FOR SIMULATION
      from StrippingConf.Configuration import StrippingConf
      confname= 'Beauty2XGammaExclusive'#FOR USERS

      from StrippingSelections import buildersConf
      confs = buildersConf()
      from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
      streams = buildStreamsFromBuilder(confs,confname)

      #define stream names
      leptonicMicroDSTname   = 'Leptonic'
      charmMicroDSTname      = 'Charm'
      pidMicroDSTname        = 'PID'
      bhadronMicroDSTname    = 'Bhadron'
      mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
      dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon",
                      "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]
      
      stripTESPrefix = 'Strip'

      from Configurables import ProcStatusCheck

      sc = StrippingConf( Streams = streams,
                          MaxCandidates = 2000,
                          AcceptBadEvents = False,
                          BadEventSelection = ProcStatusCheck(),
                          TESPrefix = stripTESPrefix,
                          ActiveMDSTStream = False,
                          Verbose = True,
                          DSTStreams = dstStreams,
                          MicroDSTStreams = mdstStreams )
      
      DaVinci().appendToMainSequence( [eventNodeKiller, sc.sequence() ] )

   caloPostCalib = True
   if caloPostCalib:
      importOptions('./caloPostCalib.py')
      from Configurables import CaloPostCalibAlg
      cpc = CaloPostCalibAlg('CaloPostCalib')
      cpc_inputs = ['Phys/Beauty2XGammaExclusiveBs2PhiGammaLine/Particles','BhadronCompleteEvent/Phys/Beauty2XGammaExclusiveBs2PhiGammaLine/Particles']
      cpc.Inputs=cpc_inputs
      DaVinci().UserAlgorithms  += [cpc]
   tupsconf = ["DTTBs2phig"]
   
   from Configurables import TupleToolMCTruth
   from DecayTreeTuple.Configuration import addTupleTool, addBranches
   from Configurables import LoKi__Hybrid__TupleTool

   StrippingVars = {}
   for name in ["Bs2GammaGamma_NoConvLine",
                "Bs2GammaGamma_doubleLine",
                "Bs2GammaGamma_LLLine",
                "Bs2GammaGamma_DDLine",
                "Bs2GammaGammaWide_NoConvLine",
                "Beauty2XGammaExclusiveBs2PhiGammaLine"]:
      StrippingVars[name] = "CONTAINS('Radiative/Phys/"+name+"/Particles')"

   for tconf in tupsconf:
      
      A1ggtuple = DecayTreeTuple(tconf)
      A1ggtuple.ToolList += [
         "TupleToolEventInfo"
         ,"TupleToolPrimaries"
         , "TupleToolRecoStats"
         , "TupleToolParticleStats"  # default = photon counters          
         , "TupleToolCPU"
         , "TupleToolAngles"
         , "TupleToolANNPID"
         #, "TupleToolCaloHypo"
         ,"TupleToolPhotonInfo"
         ,"TupleToolTrackInfo"
         ]
      
      if "Bs2phig" in tconf:
         A1ggtuple.Decay = "B_s0 -> ^(phi(1020)-> ^K+ ^K-) ^gamma"
         A1ggtuple.addBranches({
            "Bs" : "B_s0 -> (phi(1020)-> K+ K-) gamma",
            "phi" : "B_s0 -> ^(phi(1020)-> K+ K-) gamma",
            "kplus" : "B_s0 -> (phi(1020)-> ^K+ K-) gamma",
            "kminus" : "B_s0 -> (phi(1020)-> K+ ^K-) gamma",
            "gamma" : "B_s0 -> (phi(1020)-> K+ K-) ^gamma"
            })
      
      
      ## STRIPPING  DECISIONS
      A1ggtuple.addTupleTool('LoKi::Hybrid::EvtTupleTool/LoKi_Evt')
      A1ggtuple.LoKi_Evt.VOID_Variables = StrippingVars
   
      if "Bs2phig" in tconf: LoKi_A1_Std = A1ggtuple.Bs.addTupleTool("LoKi__Hybrid__TupleTool/LoKi_H10Std")
      LoKi_A1_Std.Preambulo = ['import math  as math1','FCOSDPHI = cos(CHILD(PHI,1)-CHILD(PHI,2))']
      LoKi_A1_Std.Variables = {"COSDPHI" : "FCOSDPHI",
                               "ETA":"ETA","PHI":"PHI"}   

      #A1ggtuple.ToolList.remove("TupleToolGeometry")
      A1ggtuple.addTupleTool('TupleToolTISTOS/TISTOSTool')
      A1ggtuple.TISTOSTool.VerboseL0 = True
      A1ggtuple.TISTOSTool.VerboseHlt1 = True
      A1ggtuple.TISTOSTool.VerboseHlt2 = True
      A1ggtuple.TISTOSTool.TriggerList = mytriggerlist
      A1ggtuple.addTupleTool("TupleToolProtoPData")
      A1ggtuple.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH","CaloNeutralID"]

      A1ggtuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
      A1ggtuple.gammaL0ECalo.WhichCalo = "ECAL"
      A1ggtuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

      if (DaVinci().Simulation):
         #A1ggtuple.addTupleTool('TupleToolMCBackgroundInfo')
         MCTruth = A1ggtuple.addTupleTool("TupleToolMCTruth")
         MCTruth.addTupleTool("MCTupleToolHierarchy")
      

      if "Bs2phig" in tconf: leps = ["kplus","kminus","gamma"]
      for lep in leps:
         mymod = getattr(A1ggtuple,lep)
         LoKi_LStd = mymod.addTupleTool("LoKi__Hybrid__TupleTool/LoKi_LStd")
         LoKi_LStd.Preambulo = ['import math  as math1']
         LoKi_LStd.Variables = {"ETA":"ETA","PHI":"PHI","CL":"CL",} 
         if "gamma" in lep:
            if not "Conv" in lep:
               LoKi_LStd.Variables["CaloShapeE1"]="PPINFO( LHCb.ProtoParticle.CaloShapeE1,-1000)"
               LoKi_LStd.Variables["HCAL2ECAL"]="MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000))"
            continue ## for kaons and electrons, add track info
         mymod.addTupleTool("TupleToolTrackInfo")
                           


      
      ## define input locations. Stripping for Rad stream. For the rest, my own selection output
      cname = tconf.split("_")[-1]
      if params['Simulation']:
         if "Bs2phig" in tconf: A1ggtuple.Inputs = ['Phys/Beauty2XGammaExclusiveBs2PhiGammaLine/Particles']
         DaVinci().UserAlgorithms += [A1ggtuple]
      else:
         A1ggtuple.Inputs = ['BhadronCompleteEvent/Phys/Beauty2XGammaExclusiveBs2PhiGammaLine/Particles']
         DaVinci().UserAlgorithms += [A1ggtuple]
         
         
   from Configurables import EventTuple
   DaVinci().prependToMainSequence(EventTuple("EventTuple"))
   DaVinci().UserAlgorithms += [EventTuple("EventTuple")]

   ## add mctruth tuple to MC ALP
        
   DaVinci().TupleFile = params["OutputName"]

   gaudi = appMgr()
   TES = gaudi.evtsvc()
   ToolSvc = gaudi.toolSvc()


# =============================================================================

# =============================================================================
## Job steering

if __name__ == '__main__' :

   ## make printout of the own documentations
   print __doc__
   HOME = '/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/'
   files = map(lambda x: 'PFN:root://eoslhcb.cern.ch/'+HOME+x,[
                                                               #'00075555_00009741_1.bhadroncompleteevent.dst',
                                                               '00075555_00004518_1.bhadroncompleteevent.dst'] )
   #configure(files,params   = {'Simulation': False, 'Polarity': 'MagUp', 'Year': 2018 , 'OutputName' : 'NTUPLE_TE.root','DDDBTag':'dddb-20171030-3','CondDBTag':'cond-20180202'})

   configure(['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00086519/0000/00086519_00000192_7.AllStreams.dst'],params   = {'Simulation': True, 'Polarity': 'MagDown', 'Year': 2018 , 'OutputName' : 'NTUPLE_TE.root','DDDBTag':'dddb-20170721-3','CondDBTag':'sim-20190128-vc-md100'})

   
   
   run(10000)


   
   gaudi = appMgr()
   TES = gaudi.evtsvc()
   gaudi.stop()
   gaudi.finalize()


# =============================================================================
# The END
# =============================================================================
