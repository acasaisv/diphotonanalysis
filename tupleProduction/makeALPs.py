
# from Configurables import GenerationToSimulation
# from Configurables import GiGaALPsParticles, GiGaPhysListModular
# GenerationToSimulation("GenToSim").KeepCode = "in_list( GABSID, [ 'AxR0' ] )"
# gigaAxR0Part = GiGaALPsParticles()
# gigaAxR0Part.ALPs = ["AxR0"]
# GiGaPhysListModular("ModularPL").PhysicsConstructors += [ gigaAxR0Part ]



m0, mw = 3, 7.04E-7
m0, mw = 5, 6e-6
#m0, mw = 10,1.29E-05
def tau(m0, mw):
    tau = 6.582119514E-25/mw
    return ["AxR0 54 54 0.0 "+str(m0)+" "+str(tau)+" AxR0 54 1.e-2"]

from Configurables import LHCb__ParticlePropertySvc as PPS
PPS().Particles += tau(m0, mw)

