from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import *
from DecayTreeTuple.Configuration import *
from Configurables import PrintDecayTree,  DecayTreeTuple, DaVinci, SelDSTWriter, FilterDesktop, FilterInTrees, LoKi__HDRFilter, FilterDesktop,CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from Configurables import TupleToolRecoStats,TupleToolProtoPData, TupleToolCaloHypo
from Configurables import TupleToolKinematic, TupleToolTrackInfo, TupleToolPrimaries
from Configurables import TupleToolCaloHypo, TupleToolGeometry, TupleToolRecoStats, TupleToolPid, TupleToolTISTOS, TupleToolConeIsolation, TupleToolStripping
from PhysConf.Selections import PrintSelection  , FilterSelection , CombineSelection , Hlt1Selection , Hlt2Selection , L0Selection , TupleSelection


#input containers------

Photons = AutomaticData('Phys/StdLooseAllPhotons/Particles')
Muons = AutomaticData('Phys/StdAllLooseMuons/Particles')
Pions = AutomaticData("Phys/StdAllLoosePions/Particles")
LooseEta = AutomaticData('Phys/StdLooseEta2gg/Particles')

EtaFilter = FilterDesktop('EtaFilter',Code='(CHILD(CL,1)>0.2) & (CHILD(CL,2)>0.2) & (PT>2*GeV)')
Eta = Selection('Eta_Sel',Algorithm=EtaFilter,RequiredSelections=[LooseEta])                

#cuts------------

eta2mumug_name = "Eta2MuMuGamma"
eta2mumug_decd = "eta -> mu+ mu- gamma"
eta2mumug_daug = {
    "gamma" : "(PT > 500*MeV) & (CL>0.05)" ,
    "mu+"   : "(PT > 500.0*MeV) & (PROBNNmu > 0.8) & (MIPCHI2DV(PRIMARY) < 6.0)",
    "mu-"   : "(PT > 500.0*MeV) & (PROBNNmu > 0.8) & (MIPCHI2DV(PRIMARY) < 6.0)"}
eta2mumug_comb = "(AM>400*MeV) & (AM<700*MeV)"
eta2mumug_moth = "(MM>405) & (MM<695)"

eta2gg_name = "Eta2GammaGamma"
eta2gg_decd = "eta -> gamma gamma"
eta2gg_daug = {
    "gamma" : "(CL>0.05)"}
eta2gg_comb = "(AM>400*MeV) & (AM<700*MeV) & (APT>2*GeV) "
eta2gg_moth = "(MM>405) & (MM<695) & ( MAXTREE(ABSID=='gamma',PT) > 500*MeV )"

b2gg_name = "B2GammaGamma"
b2gg_decd = "B0 -> gamma gamma"
b2gg_daug = {
    "gamma" : "(PT > 500*MeV) & (CL>0.05)"}
b2gg_comb = "(AM>4000*MeV) & (AM<7000*MeV) & (APT>2*GeV)"
b2gg_moth = "(MM>4100) & (MM<6900)"

etap2pipigamma_name='Etap2PiPiGamma'
etap2pipigamma_decd="[eta_prime -> pi+ pi- gamma]cc"
etap2pipigamma_daug={
    'pi+':
    "(PT > 500. * MeV)"
    "& (MIPCHI2DV(PRIMARY) > 16.) "
    "& (TRCHI2DOF < 5.) "
    "& (TRGHOSTPROB < 0.5) "
    "& (P > 1000.*MeV) "
    "& (PIDK-PIDpi < 0)",
    'gamma':"(PT > 1000. * MeV)"
    }
etap2pipigamma_comb='ATRUE'
etap2pipigamma_moth="(MM > 900. * MeV) & (MM < 1020. * MeV)"
                     

d2etappi_name = 'D2EtapPi'
d2etappi_decd = ["[D+ -> eta_prime pi+]cc",
                 #"[D- -> eta_prime pi-]cc"
                 ]
d2etappi_daug = {
    "eta_prime":"ALL",
    "pi+":"(PT > 600.*MeV) & (MIPCHI2DV(PRIMARY) > 16.) & (TRCHI2DOF < 5.) & (TRGHOSTPROB < 0.5) & (P > 1000.*MeV)"}
d2etappi_comb = "(APT > 2000. * MeV) & (in_range( 1700. * MeV,AM,2200.* MeV))"
d2etappi_moth = "(VFASPF(VCHI2PDOF) < 4.) & (BPVLTIME() > 0.25*picosecond) & (BPVDIRA>0.999975) & (BPVIP()<0.05*mm) & (BPVIPCHI2()<10.)"


#Hlt2 calopid filters -----------

filterHlt2_eta2mumug     = LoKi__HDRFilter( 'MyHlt2Filter_Eta2MuMuGamma', Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_kstrgamma     = LoKi__HDRFilter( 'MyHlt2Filter_KstrGamma', Code="HLT_PASS('Hlt2CaloPIDBd2KstGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_phigamma      = LoKi__HDRFilter( 'MyHlt2Filter_PhiGamma', Code="HLT_PASS('Hlt2CaloPIDBs2PhiGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_ds2etappi     = LoKi__HDRFilter( 'MyHlt2Filter_Ds2EtapPi', Code="HLT_PASS('Hlt2CaloPIDD2EtapPiTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_dsstr2dsgamma = LoKi__HDRFilter( 'MyHlt2Filter_Dsstr2DsGamma', Code="HLT_PASS('Hlt2CaloPIDDsst2DsGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_resolved      = LoKi__HDRFilter( 'MyHlt2Filter_ResolvedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_merged        = LoKi__HDRFilter( 'MyHlt2Filter_MergedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )

evtPreselectors = []
#evtPreselectors.append(filterHlt2_eta2mumug)

#sequences-----------------

eta2mumug = CombineParticles(eta2mumug_name , DecayDescriptor=eta2mumug_decd , DaughtersCuts=eta2mumug_daug , CombinationCut=eta2mumug_comb , MotherCut=eta2mumug_moth)
eta2mumug_sel = Selection(eta2mumug_name + "_Sel" , Algorithm=eta2mumug ,  RequiredSelections=[Muons,Photons])
eta2mumug_seq = SelectionSequence(eta2mumug_name + "_Seq", TopSelection=eta2mumug_sel, EventPreSelector = evtPreselectors)

eta2gg = CombineParticles(eta2gg_name , DecayDescriptor=eta2gg_decd , DaughtersCuts=eta2gg_daug , CombinationCut=eta2gg_comb , MotherCut=eta2gg_moth)
eta2gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
eta2gg_sel = Selection(eta2gg_name + "_Sel" , Algorithm=eta2gg ,  RequiredSelections=[Photons])
eta2gg_seq = SelectionSequence(eta2gg_name + "_Seq", TopSelection=eta2gg_sel, EventPreSelector = evtPreselectors)

b2gg = CombineParticles(b2gg_name , DecayDescriptor=b2gg_decd , DaughtersCuts=b2gg_daug , CombinationCut=b2gg_comb , MotherCut=b2gg_moth)
b2gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
b2gg_sel = Selection(b2gg_name + "_Sel" , Algorithm=b2gg ,  RequiredSelections=[Photons])
b2gg_seq = SelectionSequence(b2gg_name + "_Seq", TopSelection=b2gg_sel, EventPreSelector = evtPreselectors)

etap2pipigamma = CombineParticles(etap2pipigamma_name , DecayDescriptor=etap2pipigamma_decd , DaughtersCuts=etap2pipigamma_daug , CombinationCut=etap2pipigamma_comb , MotherCut=etap2pipigamma_moth)
etap2pipigamma_sel = Selection(etap2pipigamma_name + "_Sel" , Algorithm=etap2pipigamma ,  RequiredSelections=[Photons,Pions])
etap2pipigamma_seq = SelectionSequence(etap2pipigamma_name + "_Seq", TopSelection=etap2pipigamma_sel, EventPreSelector = evtPreselectors)

d2etappi= CombineParticles(d2etappi_name , DecayDescriptors=d2etappi_decd , DaughtersCuts=d2etappi_daug , CombinationCut=d2etappi_comb , MotherCut=d2etappi_moth)
d2etappi_sel = Selection(d2etappi_name + "_Sel" , Algorithm=d2etappi ,  RequiredSelections=[AutomaticData(etap2pipigamma_sel.outputLocation()),Pions])
d2etappi_seq = SelectionSequence(d2etappi_name + "_Seq", TopSelection=d2etappi_sel, EventPreSelector = evtPreselectors)


#tools----------------

StdToolList = ["TupleToolEventInfo","TupleToolKinematic","TupleToolGeometry","TupleToolPid","TupleToolAngles"]

PidVar = {"ID":"ID", "PROBNNk":"PROBNNk", "PROBNNpi":"PROBNNpi", "PROBNNp":"PROBNNp", "PROBNNe":"PROBNNe", "PROBNNmu":"PROBNNmu", "PROBNNghost":"PROBNNghost", "PIDK":"PIDK", "PIDp":"PIDp", "PIDe":"PIDe", "PIDmu":"PIDmu" , "MIPCHI2DV":"MIPCHI2DV(PRIMARY)"}

NeutralsProto = TupleToolProtoPData("NeutralsProto")
NeutralsProto.DataList = ["CaloNeutralID","CaloNeutralSpd","IsNotH","IsNotE","IsPhoton"]

NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

triglist = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    'Hlt1MBNoBiasDecision',
    "Hlt2RadiativeB2GammaGammaDecision"
    ]

TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]

# StrippingFlag = TupleToolStripping('TupleToolStripping.B2ggS')
# StrippingFlag.StrippingList = ['StrippingBs2GammaGamma_NoConvLineDecision']

ConeIsol = TupleToolConeIsolation('TupleToolConeIsolation.B2gg',
                       FillComponents=True,
                       MinConeSize=1.0,
                       SizeStep=0.35,
                       MaxConeSize=2.05,
                       Verbose=False,
                       FillPi0Info=False,
                       FillMergedPi0Info=False,
                       FillAsymmetry=True,
                       FillDeltas= False,
                       ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                       MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                       )


#tuple----------------

eta2mumug_tuple = DecayTreeTuple(eta2mumug_name + "_Tuple")
eta2mumug_tuple.Decay = "eta -> ^mu+ ^mu- ^gamma"
eta2mumug_tuple.Inputs = [ eta2mumug_seq.outputLocation() ]
eta2mumug_tuple.ToolList = StdToolList

eta2mumug_tuple.addBranches({
    "eta"    : "eta",
    "mu1"    : "eta -> ^mu+  mu-  gamma",
    "mu2"    : "eta ->  mu+ ^mu-  gamma",
    "gamma"  : "eta ->  mu+  mu- ^gamma"})

eta2mumug_tuple.eta.addTupleTool(TISTOS)
eta2mumug_tuple.gamma.addTupleTool(NeutralsProto)
eta2mumug_tuple.gamma.addTupleTool(NeutralsHypo)
#eta2mumug_tuple.gamma.addTupleTool(ConeIsol)
eta2mumug_tuple.addTupleTool("TupleToolVeto")
eta2mumug_tuple.TupleToolVeto.Particle="gamma"
eta2mumug_tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
eta2mumug_tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

#----

eta2gg_tuple = DecayTreeTuple(eta2gg_name + "_Tuple")
eta2gg_tuple.Decay = "eta -> ^gamma ^gamma"
eta2gg_tuple.Inputs = [ eta2gg_seq.outputLocation() ]
eta2gg_tuple.ToolList = StdToolList

eta2gg_tuple.addBranches({
    "eta"    : "eta",
    "gamma0" : "eta -> ^gamma  gamma",
    "gamma"  : "eta ->  gamma ^gamma"})

eta2gg_tuple.gamma.addTupleTool(NeutralsProto)
eta2gg_tuple.gamma.addTupleTool(NeutralsHypo)
eta2gg_tuple.gamma0.addTupleTool(NeutralsProto)
eta2gg_tuple.gamma0.addTupleTool(NeutralsHypo)
eta2gg_tuple.eta.addTupleTool(TISTOS)
# eta2gg_tuple.gamma.addTupleTool(ConeIsol)
# eta2gg_tuple.gamma0.addTupleTool(ConeIsol)

#----

b2gg_tuple = DecayTreeTuple(b2gg_name + "_Tuple")
b2gg_tuple.Decay = "B0 -> ^gamma ^gamma"
b2gg_tuple.Inputs = [ b2gg_seq.outputLocation() ]
b2gg_tuple.ToolList = StdToolList

b2gg_tuple.addBranches({
    "B"      : "B0",
    "gamma0" : "B0 -> ^gamma  gamma",
    "gamma"  : "B0 ->  gamma ^gamma"})

from Configurables import TupleToolSelResults
#import dvSettings

#b2gg_tuple.B.ToolList += ['TupleToolSelResults']
#b2gg_tuple.B.addTupleTool(TupleToolSelResults)
#b2gg_tuple.B.TupleToolSelResults.Selections = dvSettings.StrippingSelections    
#dtt_stripping.StrippingList = ['StrippingBs2GammaGamma_NoConvLineDecision']
b2gg_tuple.gamma.addTupleTool(NeutralsProto)
b2gg_tuple.gamma.addTupleTool(NeutralsHypo)
b2gg_tuple.gamma0.addTupleTool(NeutralsProto)
b2gg_tuple.gamma0.addTupleTool(NeutralsHypo)
b2gg_tuple.B.addTupleTool(TISTOS)
b2gg_tuple.B.addTupleTool(ConeIsol)
b2gg_tuple.gamma.addTupleTool(ConeIsol)
b2gg_tuple.gamma0.addTupleTool(ConeIsol)


    

#main-----------------

DaVinci().UserAlgorithms += [eta2mumug_seq.sequence(),
                             
                             #eta2mumug_tuple
                             ]
DaVinci().UserAlgorithms += [eta2gg_seq.sequence(),
                             #eta2gg_tuple
                             ]

DaVinci().UserAlgorithms += [etap2pipigamma_seq.sequence(),
                             d2etappi_seq.sequence()
                             ]


#DaVinci().UserAlgorithms += [b2gg_seq.sequence(), b2gg_tuple]

caloPostCalib = 0
if caloPostCalib:
    #importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc.Inputs=["Phys/Bs2GammaGamma_NoConvLine/Particles"]
    DaVinci().UserAlgorithms  += [cpc]
