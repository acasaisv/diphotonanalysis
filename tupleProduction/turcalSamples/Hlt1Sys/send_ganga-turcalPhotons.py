#myApp = prepareGaudiExec('DaVinci','v45r4', myPath='/home3/adrian.casais/cmtuser')
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser2/DaVinciDev_v45r7')
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')

j = {}
#j['U'] = Job(name='Turcal samples - MU')
#j['D'] = Job(name='Turcal samples - MD')
options = [
    #'B2ggStripping.py',
           #'./caloPostCalib.py',
           'davinci.py',
           #'StrippingTuple.py',
           #'options_2018.py'
           ]

bkPath = {'U':'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Turbo05/95100000/FULLTURBO.DST',
          'D':'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Turbo05/95100000/FULLTURBO.DST'}

extraopt = 'import os\n'\
'os.environ["PYTHONPATH] = os.environ["PWD"]\n'
for pol in ['U','D']:
#for pol in ['D']:
    j[pol] = Job(name='Turcal samples - M{}'.format(pol))
    j[pol].application = myApp

    j[pol].application.options = options
    j[pol].application.platform = 'x86_64-centos7-gcc62-opt'
    bkk = bkPath[pol] 
    data = BKQuery(bkk).getDataset()
    j[pol].inputdata = data     # access only the first 2 files of data
    #j[pol].application.extraOpts = extraopt
    j[pol].inputfiles = ['caloPostCalib.py']
    j[pol].backend = Dirac()
    #j[pol].backend.settings['BannedSites'] = 'LCG.RAL.uk'
    j[pol].splitter = SplitByFiles(filesPerJob=50,ignoremissing=True)
    j[pol].outputfiles = [DiracFile('*.root')]
    j[pol].submit()

