from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import *
from DecayTreeTuple.Configuration import *
from Configurables import PrintDecayTree,  DecayTreeTuple, DaVinci, SelDSTWriter, FilterDesktop, FilterInTrees, LoKi__HDRFilter, FilterDesktop,CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from Configurables import TupleToolRecoStats,TupleToolProtoPData, TupleToolCaloHypo
from Configurables import TupleToolKinematic, TupleToolTrackInfo, TupleToolPrimaries
from Configurables import TupleToolCaloHypo, TupleToolGeometry, TupleToolRecoStats, TupleToolPid, TupleToolTISTOS, TupleToolConeIsolation, TupleToolStripping, TupleToolPhotonInfo,TupleToolL0Data
from PhysConf.Selections import PrintSelection  , FilterSelection , CombineSelection , Hlt1Selection , Hlt2Selection , L0Selection , TupleSelection

DaVinci().InputType = 'DST'
DaVinci().DataType = '2018'
DaVinci().Simulation = False
DaVinci().DDDBtag='dddb-20171030-3'
DaVinci().CondDBtag='cond-20180202'
DaVinci.Lumi = True
DaVinci().EvtMax = -1
DaVinci().TupleFile='tuple.root'
DaVinci().RootInTES=''
DaVinci().ProductionType = 'Stripping'
DaVinci().EnableUnpack=['Reconstruction', 'Stripping']

#input containers------

Photons = AutomaticData('Phys/StdLooseAllPhotons/Particles')
Muons = AutomaticData('Phys/StdAllLooseMuons/Particles')
Pions = AutomaticData("Phys/StdAllLoosePions/Particles")
LooseEta = AutomaticData('Phys/StdLooseEta2gg/Particles')

EtaFilter = FilterDesktop('EtaFilter',Code='(CHILD(CL,1)>0.2) & (CHILD(CL,2)>0.2) & (PT>2*GeV)')
Eta = Selection('Eta_Sel',Algorithm=EtaFilter,RequiredSelections=[LooseEta])                

#cuts------------

eta2mumug_name = "Eta2MuMuGamma"
eta2mumug_decd = "eta -> mu+ mu- gamma"
eta2mumug_daug = {
    #"gamma" : "(PT > 500*MeV) & (CL>0.05)" ,
    "gamma" : "(PT > 500*MeV)" ,
    "mu+"   : "(PT > 500.0*MeV) & (PROBNNmu > 0.8) & (MIPCHI2DV(PRIMARY) < 6.0)",
    "mu-"   : "(PT > 500.0*MeV) & (PROBNNmu > 0.8) & (MIPCHI2DV(PRIMARY) < 6.0)"}
eta2mumug_comb = "(AM>400*MeV) & (AM<700*MeV)"
eta2mumug_moth = "(MM>405) & (MM<695)"

eta2gg_name = "Eta2GammaGamma"
eta2gg_decd = "eta -> gamma gamma"
eta2gg_daug = {
    "gamma" : "(CL>0.05)"}
eta2gg_comb = "(AM>400*MeV) & (AM<700*MeV) & (APT>2*GeV) "
eta2gg_moth = "(MM>405) & (MM<695) & ( MAXTREE(ABSID=='gamma',PT) > 500*MeV )"

b2gg_name = "B2GammaGamma"
b2gg_decd = "B0 -> gamma gamma"
b2gg_daug = {
    "gamma" : "(PT > 500*MeV) & (CL>0.05)"}
b2gg_comb = "(AM>4000*MeV) & (AM<7000*MeV) & (APT>2*GeV)"
b2gg_moth = "(MM>4100) & (MM<6900)"

etap2pipigamma_name='Etap2PiPiGamma'
etap2pipigamma_decd="[eta_prime -> pi+ pi- gamma]cc"
etap2pipigamma_daug={
    'pi+':
    "(PT > 500. * MeV)"
    "& (MIPCHI2DV(PRIMARY) > 16.) "
    "& (TRCHI2DOF < 5.) "
    "& (TRGHOSTPROB < 0.5) "
    "& (P > 1000.*MeV) "
    "& (PIDK-PIDpi < 0)",
    'gamma':"(PT > 1000. * MeV)"
    }
etap2pipigamma_comb='ATRUE'
etap2pipigamma_moth="(MM > 900. * MeV) & (MM < 1020. * MeV)"
                     

d2etappi_name = 'D2EtapPi'
d2etappi_decd = ["[D+ -> eta_prime pi+]cc",
                 #"[D- -> eta_prime pi-]cc"
                 ]
d2etappi_daug = {
    "eta_prime":"ALL",
    "pi+":"(PT > 600.*MeV) & (MIPCHI2DV(PRIMARY) > 16.) & (TRCHI2DOF < 5.) & (TRGHOSTPROB < 0.5) & (P > 1000.*MeV)"}
d2etappi_comb = "(APT > 2000. * MeV) & (in_range( 1700. * MeV,AM,2200.* MeV))"
d2etappi_moth = "(VFASPF(VCHI2PDOF) < 4.) & (BPVLTIME() > 0.25*picosecond) & (BPVDIRA>0.999975) & (BPVIP()<0.05*mm) & (BPVIPCHI2()<10.)"


phi2kk_name = 'Phi2KK'
phi2kk_decd = 'phi(1020) -> K+ K-'
phi2kk_preambulo = ["goodKaon = ((MIPCHI2DV(PRIMARY) > 16.) & (TRCHI2DOF < 3) & (MAXTREE(TRGHOSTPROB, HASTRACK) < 0.4) & (P > 3000) & (PT > 500.))",
                    "goodPhi = ( (VFASPF(VCHI2/VDOF) < 9.) & (ADMASS('phi(1020)') < 15*MeV) & (SUMTREE(PT, ISBASIC, 0.0) > 1500))" 
                    ]
phi2kk_code = 'goodPhi & CHILDCUT( goodKaon, 1 ) & CHILDCUT( goodKaon, 2 )'

kstar2kpi_name = 'Kstar2KPi'
kstar2kpi_decd = '[K*(892)0 -> K+ pi-]CC'
kstar2kpi_preambulo = ["goodTrack = ((MIPCHI2DV(PRIMARY) > 16.) & (TRCHI2DOF < 3) & (MAXTREE(TRGHOSTPROB, HASTRACK) < 0.4) & (P > 3000) & (PT > 500.))",
                       "goodKstar = ( (VFASPF(VCHI2/VDOF) < 9.) & (ADMASS('K*(892)0') < 100*MeV) & (SUMTREE(PT, ISBASIC, 0.0) > 1500))" 
                    ]
kstar2kpi_code = "goodKstar & CHILDCUT( goodTrack , 1 ) & CHILDCUT( goodTrack , 2 )"

bs2phigamma_name = 'Bs2phigamma'
bs2phigamma_daug = {'gamma':'PT>1000*MeV'}
bs2phigamma_decd = '[B_s0 -> phi(1020) gamma]cc'
bs2phigamma_comb = "((AM > 0.75*4000.) & (AM < 1.25*7000))"
bs2phigamma_moth = "(VFASPF(VCHI2/VDOF) < 9) & (BPVIPCHI2() < 9) & (PT > 2000) & (M > 4000) & (M < 7000)  & (SUMTREE(PT, ISBASIC, 0.0) > 3000)"

bd2kstgamma_name = 'Bd2kstgamma'
bd2kstgamma_daug = {'gamma':'PT>1000*MeV'}
bd2kstgamma_decd = '[B0 -> K*(892)0 gamma]cc'
bd2kstgamma_comb = "((AM > 0.75*4000.) & (AM < 1.25*7000))"
bd2kstgamma_moth = "(VFASPF(VCHI2/VDOF) < 9) & (BPVIPCHI2() < 9) & (PT > 2000) & (M > 4000) & (M < 7000)  & (SUMTREE(PT, ISBASIC, 0.0) > 3000) & (acos(BPVDIRA) < 0.15)"



#Hlt2 calopid filters -----------

filterHlt2_eta2mumug     = LoKi__HDRFilter( 'MyHlt2Filter_Eta2MuMuGamma', Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_kstrgamma     = LoKi__HDRFilter( 'MyHlt2Filter_KstrGamma', Code="HLT_PASS('Hlt2CaloPIDBd2KstGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_phigamma      = LoKi__HDRFilter( 'MyHlt2Filter_PhiGamma', Code="HLT_PASS('Hlt2CaloPIDBs2PhiGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_ds2etappi     = LoKi__HDRFilter( 'MyHlt2Filter_Ds2EtapPi', Code="HLT_PASS('Hlt2CaloPIDD2EtapPiTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_dsstr2dsgamma = LoKi__HDRFilter( 'MyHlt2Filter_Dsstr2DsGamma', Code="HLT_PASS('Hlt2CaloPIDDsst2DsGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_resolved      = LoKi__HDRFilter( 'MyHlt2Filter_ResolvedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_merged        = LoKi__HDRFilter( 'MyHlt2Filter_MergedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )

evtPreselectors = []
#evtPreselectors.append(filterHlt2_eta2mumug)
evtPreselectors = [filterHlt2_phigamma]
#sequences-----------------

eta2mumug = CombineParticles(eta2mumug_name , DecayDescriptor=eta2mumug_decd , DaughtersCuts=eta2mumug_daug , CombinationCut=eta2mumug_comb , MotherCut=eta2mumug_moth)
eta2mumug_sel = Selection(eta2mumug_name + "_Sel" , Algorithm=eta2mumug ,  RequiredSelections=[Muons,Photons])
eta2mumug_seq = SelectionSequence(eta2mumug_name + "_Seq", TopSelection=eta2mumug_sel, EventPreSelector = [filterHlt2_eta2mumug])

eta2gg = CombineParticles(eta2gg_name , DecayDescriptor=eta2gg_decd , DaughtersCuts=eta2gg_daug , CombinationCut=eta2gg_comb , MotherCut=eta2gg_moth)
eta2gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
eta2gg_sel = Selection(eta2gg_name + "_Sel" , Algorithm=eta2gg ,  RequiredSelections=[Photons])
eta2gg_seq = SelectionSequence(eta2gg_name + "_Seq", TopSelection=eta2gg_sel, EventPreSelector = [])

b2gg = CombineParticles(b2gg_name , DecayDescriptor=b2gg_decd , DaughtersCuts=b2gg_daug , CombinationCut=b2gg_comb , MotherCut=b2gg_moth)
b2gg.ParticleCombiners.update( { "" : "ParticleAdder"} )
b2gg_sel = Selection(b2gg_name + "_Sel" , Algorithm=b2gg ,  RequiredSelections=[Photons])
b2gg_seq = SelectionSequence(b2gg_name + "_Seq", TopSelection=b2gg_sel, EventPreSelector = [])

etap2pipigamma = CombineParticles(etap2pipigamma_name , DecayDescriptor=etap2pipigamma_decd , DaughtersCuts=etap2pipigamma_daug , CombinationCut=etap2pipigamma_comb , MotherCut=etap2pipigamma_moth)
etap2pipigamma_sel = Selection(etap2pipigamma_name + "_Sel" , Algorithm=etap2pipigamma ,  RequiredSelections=[Photons,Pions])
etap2pipigamma_seq = SelectionSequence(etap2pipigamma_name + "_Seq", TopSelection=etap2pipigamma_sel, EventPreSelector = [filterHlt2_ds2etappi])

d2etappi= CombineParticles(d2etappi_name , DecayDescriptors=d2etappi_decd , DaughtersCuts=d2etappi_daug , CombinationCut=d2etappi_comb , MotherCut=d2etappi_moth)
d2etappi_sel = Selection(d2etappi_name + "_Sel" , Algorithm=d2etappi ,  RequiredSelections=[AutomaticData(etap2pipigamma_sel.outputLocation()),Pions])
d2etappi_seq = SelectionSequence(d2etappi_name + "_Seq", TopSelection=d2etappi_sel, EventPreSelector = [filterHlt2_ds2etappi])

phi2kk_sel = Selection('phi2kk',Algorithm = FilterDesktop('phi2kk_desktop',Preambulo=phi2kk_preambulo, Code=phi2kk_code),RequiredSelections=[DataOnDemand(Location="Phys/StdLoosePhi2KK/Particles")])
phi2kk_seq = SelectionSequence('phi2kk_Seq',TopSelection=phi2kk_sel,EventPreSelector = [filterHlt2_phigamma])
kst2kpi_sel = Selection('kst2kpi',Algorithm = FilterDesktop('kst2kpi_desktop',Preambulo=kstar2kpi_preambulo, Code=kstar2kpi_code),RequiredSelections=[DataOnDemand(Location="Phys/StdVeryLooseDetachedKst2Kpi/Particles")])
kst2kpi_seq = SelectionSequence('kst2kpi_Seq',TopSelection=kst2kpi_sel,EventPreSelector = [filterHlt2_kstrgamma])

bs2phigamma= CombineParticles(bs2phigamma_name , DecayDescriptor=bs2phigamma_decd , DaughtersCuts=bs2phigamma_daug , CombinationCut=bs2phigamma_comb , MotherCut=bs2phigamma_moth)
bs2phigamma_sel = Selection(bs2phigamma_name + "_Sel" , Algorithm=bs2phigamma ,  RequiredSelections=[AutomaticData(phi2kk_sel.outputLocation()),Photons])
bs2phigamma_seq = SelectionSequence(bs2phigamma_name + "_Seq", TopSelection=bs2phigamma_sel, EventPreSelector = [filterHlt2_phigamma])

bd2kstgamma= CombineParticles(bd2kstgamma_name , DecayDescriptor=bd2kstgamma_decd , DaughtersCuts=bd2kstgamma_daug , CombinationCut=bd2kstgamma_comb , MotherCut=bd2kstgamma_moth)
bd2kstgamma_sel = Selection(bd2kstgamma_name + "_Sel" , Algorithm=bd2kstgamma ,  RequiredSelections=[AutomaticData(kst2kpi_sel.outputLocation()),Photons])
bd2kstgamma_seq = SelectionSequence(bd2kstgamma_name + "_Seq", TopSelection=bd2kstgamma_sel, EventPreSelector = [filterHlt2_kstrgamma])


# Stripping for Bd2KstGamma
from Configurables import EventNodeKiller, ProcStatusCheck
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

strip = 'stripping34r0p1'
line = 'Beauty2XGammaExclTDCPVBd2KstGammaLine'
streams = buildStreams(stripping=strippingConfiguration(strip),archive=strippingArchive(strip))

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line

for stream in streams:
    for sline in stream.lines:
        if sline.name() == custom_line:
            print("ATOPADOOO")
            custom_stream.appendLines([sline])
filterBadEvents = ProcStatusCheck()
sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents
                   )
#tools----------------

StdToolList = ["TupleToolEventInfo","TupleToolKinematic","TupleToolGeometry","TupleToolPid","TupleToolAngles","TupleToolRecoStats",
               # 'TupleToolTrackInfo'
               ]

PidVar = {"ID":"ID", "PROBNNk":"PROBNNk", "PROBNNpi":"PROBNNpi", "PROBNNp":"PROBNNp", "PROBNNe":"PROBNNe", "PROBNNmu":"PROBNNmu", "PROBNNghost":"PROBNNghost", "PIDK":"PIDK", "PIDp":"PIDp", "PIDe":"PIDe", "PIDmu":"PIDmu" , "MIPCHI2DV":"MIPCHI2DV(PRIMARY)"}

NeutralsProto = TupleToolProtoPData("NeutralsProto")
NeutralsProto.DataList = ["CaloNeutralID","CaloNeutralSpd","IsNotH","IsNotE","IsPhoton","CaloNeutralHcal2Ecal"]

NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy",'X','Y','Z','Saturation', 'CellID','Hcal2Ecal']
TrackInfo = TupleToolTrackInfo('TrackInfo')
TrackInfo.Verbose=True

L0Data = TupleToolL0Data('L0Data')
L0Data.DataList = ['*Et*']

PhotonInfo = TupleToolPhotonInfo('PhotonInfo')
triglist = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    'L0Hadron',
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    'Hlt1MBNoBiasDecision',
    'Hlt1B2PhiGamma_LTUNBDecision',
    "Hlt2RadiativeB2GammaGammaDecision",
    'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision',
    'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision',
    'Hlt2CaloPIDBd2KstGammaTurboCalibDecision',
    'Hlt2RadiativeB2PhiGammaUnbiasedDecision',
    'Hlt2Bs2PhiGammaDecision',
    'Hlt2Bd2KstGammaDecision',
    'Hlt2Bs2PhiGammaWideBMassDecision',
    'Hlt2Bd2KstGammaWideKMassDecision',
    'Hlt2Bd2KstGammaWideBMassDecision',
    'Hlt1TrackPhotonDecision',
    'Hlt2RadiativeBd2KstGammaDecision',
    'Hlt2RadiativeBd2KstGammaULUnbiasedDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt2RadiativeIncHHGammaDecision',
    'Hlt2RadiativeIncHHHGammaDecision',
    'Hlt2Topo2BodyDecision'
    ]

TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]

# StrippingFlag = TupleToolStripping('TupleToolStripping.B2ggS')
# StrippingFlag.StrippingList = ['StrippingBs2GammaGamma_NoConvLineDecision']

ConeIsol = TupleToolConeIsolation('TupleToolConeIsolation.B2gg',
                       FillComponents=True,
                       MinConeSize=1.0,
                       SizeStep=0.35,
                       MaxConeSize=2.05,
                       Verbose=False,
                       FillPi0Info=False,
                       FillMergedPi0Info=False,
                       FillAsymmetry=True,
                       FillDeltas= False,
                       ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                       MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                       )

from Configurables import TupleToolL0Calo
L0Calo = TupleToolL0Calo('L0ECalo')
L0Calo.WhichCalo='ECAL'
#tuple----------------

eta2mumug_tuple = DecayTreeTuple(eta2mumug_name + "_Tuple")
eta2mumug_tuple.Decay = "eta -> ^mu+ ^mu- ^gamma"
eta2mumug_tuple.Inputs = [ eta2mumug_seq.outputLocation() ]
eta2mumug_tuple.ToolList += StdToolList

eta2mumug_tuple.addBranches({
    "eta"    : "eta",
    "mu1"    : "eta -> ^mu+  mu-  gamma",
    "mu2"    : "eta ->  mu+ ^mu-  gamma",
    "gamma"  : "eta ->  mu+  mu- ^gamma"})

eta2mumug_tuple.addTupleTool(TISTOS)
eta2mumug_tuple.addTupleTool(TrackInfo)
eta2mumug_tuple.gamma.addTupleTool(NeutralsProto)
eta2mumug_tuple.gamma.addTupleTool(NeutralsHypo)
eta2mumug_tuple.gamma.addTupleTool(PhotonInfo)
eta2mumug_tuple.eta.addTupleTool(ConeIsol)
eta2mumug_tuple.gamma.addTupleTool(ConeIsol)
eta2mumug_tuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
eta2mumug_tuple.gammaL0ECalo.WhichCalo = "ECAL"
eta2mumug_tuple.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
eta2mumug_tuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

#eta2mumug_tuple.gamma.addTupleTool(ConeIsol)
eta2mumug_tuple.addTupleTool("TupleToolVeto")
eta2mumug_tuple.TupleToolVeto.Particle="gamma"
eta2mumug_tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
eta2mumug_tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

eta2mumug_tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
eta2mumug_tuple.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA': 'ETA',
    'PHI':'PHI'
            }
eta2mumug_tuple.addTupleTool(L0Data)

#----

eta2gg_tuple = DecayTreeTuple(eta2gg_name + "_Tuple")
eta2gg_tuple.Decay = "eta -> ^gamma ^gamma"
eta2gg_tuple.Inputs = [ eta2gg_seq.outputLocation() ]
eta2gg_tuple.ToolList += StdToolList

eta2gg_tuple.addBranches({
    "eta"    : "eta",
    "gamma0" : "eta -> ^gamma  gamma",
    "gamma"  : "eta ->  gamma ^gamma"})

eta2gg_tuple.gamma.addTupleTool(NeutralsProto)
eta2gg_tuple.gamma.addTupleTool(NeutralsHypo)
eta2gg_tuple.gamma0.addTupleTool(NeutralsProto)
eta2gg_tuple.gamma0.addTupleTool(NeutralsHypo)
eta2gg_tuple.addTupleTool(TISTOS)
# eta2gg_tuple.gamma.addTupleTool(ConeIsol)
# eta2gg_tuple.gamma0.addTupleTool(ConeIsol)

#----

b2gg_tuple = DecayTreeTuple(b2gg_name + "_Tuple")
b2gg_tuple.Decay = "B0 -> ^gamma ^gamma"
b2gg_tuple.Inputs = [ b2gg_seq.outputLocation() ]
b2gg_tuple.ToolList += StdToolList

b2gg_tuple.addBranches({
    "B"      : "B0",
    "gamma0" : "B0 -> ^gamma  gamma",
    "gamma"  : "B0 ->  gamma ^gamma"})

from Configurables import TupleToolSelResults
#import dvSettings

#b2gg_tuple.B.ToolList += ['TupleToolSelResults']
#b2gg_tuple.B.addTupleTool(TupleToolSelResults)
#b2gg_tuple.B.TupleToolSelResults.Selections = dvSettings.StrippingSelections    
#dtt_stripping.StrippingList = ['StrippingBs2GammaGamma_NoConvLineDecision']
b2gg_tuple.gamma.addTupleTool(NeutralsProto)
b2gg_tuple.gamma.addTupleTool(NeutralsHypo)
b2gg_tuple.gamma0.addTupleTool(NeutralsProto)
b2gg_tuple.gamma0.addTupleTool(NeutralsHypo)
b2gg_tuple.addTupleTool(TISTOS)
b2gg_tuple.B.addTupleTool(ConeIsol)
b2gg_tuple.gamma.addTupleTool(ConeIsol)
b2gg_tuple.gamma0.addTupleTool(ConeIsol)

#----

bs2phigamma_tuple = DecayTreeTuple(bs2phigamma_name + "_Tuple")
bs2phigamma_tuple.Decay = "B_s0 -> ^(phi(1020) -> ^K+ ^K-) ^gamma"
bs2phigamma_tuple.Inputs = [ bs2phigamma_seq.outputLocation() ]
bs2phigamma_tuple.ToolList += StdToolList

bs2phigamma_tuple.addBranches({
    "B"      : "B_s0",
    "gamma" : "B_s0 -> phi(1020) ^gamma",
    "phi"  : "B_s0 ->  ^(phi(1020) -> K+ K-) gamma",
    "kplus"  : "B_s0 ->  (phi(1020) -> ^K+ K-) gamma",
    "kminus"  : "B_s0 ->  (phi(1020) -> K+ ^K-) gamma",
    
    })

from Configurables import TupleToolSelResults
#import dvSettings

bs2phigamma_tuple.gamma.addTupleTool(NeutralsProto)
bs2phigamma_tuple.gamma.addTupleTool(NeutralsHypo)
bs2phigamma_tuple.addTupleTool(TISTOS)
bs2phigamma_tuple.addTupleTool(TrackInfo)
bs2phigamma_tuple.gamma.addTupleTool(PhotonInfo)
bs2phigamma_tuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
bs2phigamma_tuple.gammaL0ECalo.WhichCalo = "ECAL"
bs2phigamma_tuple.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
bs2phigamma_tuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

bs2phigamma_tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
bs2phigamma_tuple.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA': 'ETA',
    'PHI':'PHI'
            }
bs2phigamma_tuple.addTupleTool(L0Data)
#-------------------------

bd2kstgamma_tuple = DecayTreeTuple(bd2kstgamma_name + "_Tuple")
bd2kstgamma_tuple.Decay = "B0 -> ^[K*(892)0 -> ^K+ ^pi-]CC ^gamma"
# bd2kstgamma_tuple.Inputs = [ bd2kstgamma_seq.outputLocation() ]
bd2kstgamma_tuple.Inputs = [ '/Event/Phys/{0}/Particles'.format(line) ]
bd2kstgamma_tuple.ToolList += StdToolList

bd2kstgamma_tuple.addBranches({
    "B"      : "B0",
    "gamma" : "B0 -> [K*(892)0 -> K+ pi-]CC ^gamma",
    "kst"  : "B0 -> ^[K*(892)0 -> K+ pi-]CC gamma",
    "kplus"  : "B0 -> [K*(892)0 -> ^K+ pi-]CC gamma",
    "piminus"  : "B0 -> [K*(892)0 -> K+ ^pi-]CC gamma",
    
    })

from Configurables import TupleToolSelResults

bd2kstgamma_tuple.gamma.addTupleTool(NeutralsProto)
bd2kstgamma_tuple.gamma.addTupleTool(NeutralsHypo)
bd2kstgamma_tuple.addTupleTool(TISTOS)
bd2kstgamma_tuple.addTupleTool(TrackInfo)
bd2kstgamma_tuple.B.addTupleTool(ConeIsol)
bd2kstgamma_tuple.gamma.addTupleTool(ConeIsol)
bd2kstgamma_tuple.gamma.addTupleTool(PhotonInfo)
bd2kstgamma_tuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
bd2kstgamma_tuple.gammaL0ECalo.WhichCalo = "ECAL"
bd2kstgamma_tuple.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
bd2kstgamma_tuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

bd2kstgamma_tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
bd2kstgamma_tuple.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA': 'ETA',
    'PHI':'PHI'
            }
bd2kstgamma_tuple.addTupleTool(L0Data)

#-------------------------
d2etappi_tuple = DecayTreeTuple(d2etappi_name + "_Tuple")
d2etappi_tuple.Decay = "[D+ -> ^[eta_prime-> ^pi+ ^pi- ^gamma]CC ^pi+]CC"
d2etappi_tuple.Inputs = [ d2etappi_seq.outputLocation() ]
d2etappi_tuple.ToolList += StdToolList

d2etappi_tuple.addBranches({
    "Dplus"      : "D+",
    "eta_prime" : "[D+ -> ^[eta_prime-> pi+ pi- gamma]CC pi+]CC",
    "piplus0"  : "[D+ -> [eta_prime-> ^pi+ pi- gamma]CC pi+]CC",
    "piminus0"  : "[D+ -> [eta_prime-> pi+ ^pi- gamma]CC pi+]CC",
    "piplus1"  : "[D+ -> [eta_prime-> pi+ pi- gamma]CC ^pi+]CC",
    "gamma"  : "[D+ -> [eta_prime-> pi+ pi- ^gamma]CC pi+]CC",
    })

from Configurables import TupleToolSelResults

d2etappi_tuple.gamma.addTupleTool(NeutralsProto)
d2etappi_tuple.gamma.addTupleTool(NeutralsHypo)
d2etappi_tuple.addTupleTool(TISTOS)
d2etappi_tuple.addTupleTool(TrackInfo)
d2etappi_tuple.Dplus.addTupleTool(ConeIsol)
d2etappi_tuple.gamma.addTupleTool(ConeIsol)
d2etappi_tuple.eta_prime.addTupleTool(ConeIsol)
d2etappi_tuple.gamma.addTupleTool(PhotonInfo)
d2etappi_tuple.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
d2etappi_tuple.gammaL0ECalo.WhichCalo = "ECAL"
d2etappi_tuple.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
d2etappi_tuple.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

d2etappi_tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
d2etappi_tuple.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA': 'ETA',
    'PHI':'PHI'
            }
d2etappi_tuple.addTupleTool(L0Data)



#main-----------------
caloPostCalib = 1
if caloPostCalib:
    #importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc.Inputs=eta2mumug_tuple.Inputs
    DaVinci().UserAlgorithms  += [cpc]
DaVinci().UserAlgorithms += [eta2mumug_seq.sequence(),
                             
                             eta2mumug_tuple
                              ]
# DaVinci().UserAlgorithms += [eta2gg_seq.sequence(),
#                              eta2gg_tuple
#                              ]

# DaVinci().UserAlgorithms += [etap2pipigamma_seq.sequence(),
#                              d2etappi_seq.sequence(),
#                              d2etappi_tuple
#                              ]

#DaVinci().UserAlgorithms += [phi2kk_seq.sequence(),
                             #bs2phigamma_seq.sequence(),
                             #bs2phigamma_tuple]

DaVinci().appendToMainSequence([event_node_killer,
                             sc.sequence()])
#DaVinci().UserAlgorithms += [
                             #kst2kpi_seq.sequence(),
                             #dbd2kstgamma_seq.sequence(),
                             #bd2kstgamma_tuple]


#DaVinci().UserAlgorithms += [b2gg_seq.sequence(), b2gg_tuple]



######################
if __name__=='__main__':
    importOptions('pool_xml_catalog.py')
    importOptions('files.py')
    import GaudiPython
    
    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    gaudi.initialize()
    # for i in range(2000):
    #   gaudi.run(1)
    #   par = TES['Phys/Eta2MuMuGamma_Sel/Particles']
    #   if par and par.size():
    #         break
    gaudi.run(10)
    
    gaudi.run(1000)
    gaudi.stop()
    gaudi.finalize()
    
    from ROOT import *
    f = TFile('tuple.root')
    # t = f.Get('Eta2MuMuGamma_Tuple/DecayTree')
    # counter = 0
    # for ev in t:
    #     if ev.gamma_L0Calo_ECAL_TriggerET != -1:
    #         counter +=1
    #         print 'ECAL_realET',ev.gamma_L0Calo_ECAL_realET
    #         print 'gamma_PT',ev.gamma_PT
    #         print 'ECAL_TriggerET',ev.gamma_L0Calo_ECAL_TriggerET
    #         print '----------------------------'
            
