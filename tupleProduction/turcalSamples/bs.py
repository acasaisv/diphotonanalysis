from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = [ 'xmlcatalog_file:bs.xml' ]

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00035842/0000/00035842_00000019_1.allstreams.dst'
    ], clear=True)

from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().CondDBtag = 'sim-20130522-1-vc-md100'
DaVinci().DDDBtag = 'dddb-20130929-1'
DaVinci().DataType='2012'
DaVinci().Lumi = False
DaVinci().TupleFile = 'tuple.root'
