# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)

bkPath = {'U':'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Turbo05/95100000/FULLTURBO.DST',
          'D':'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Turbo05/95100000/FULLTURBO.DST'}

#for pol in bkPath.keys():
for pol in ['U','D']:
 j = Job()
 root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction/turcalSamples"
 j.application = BenderModule(
                    module = root + "/BenderTurcalSamples.py",
                    #directory ='/home3/adrian.casais/cmtuser/BenderDev_v33r3',
                    directory = '/scratch09/adrian.casais/cmtuser/BenderDev_v33r3',
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )


 j.name = "turcalSamples-{}".format(pol)
 #j.name= "bender-MB"
 
 
 magnet='MagDown'
 if pol=="U":
     magnet = 'MagUp'
 j.backend = Dirac()
 #j.backend.settings['BannedSites'] = 'LCG.RAL.uk'
 j.application.params = {"Year":2018,"Polarity":magnet,"Simulation":False,'NeedsTags':True,"DDDBTag":'dddb-20171030-3',"CondDBtag":'cond-20180202'}
 j.inputfiles = ['./davinci.py',
                 #'./charmoniaRadiative.py',
                 #'./SelWDs.py',
                 './caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 bkk = bkPath[pol] 
 data = BKQuery(bkk).getDataset()
 j.inputdata = data     # access only the first 2 files of data
 j.splitter = SplitByFiles ( filesPerJob =20  , ignoremissing=True)
 
 j.submit()
 
