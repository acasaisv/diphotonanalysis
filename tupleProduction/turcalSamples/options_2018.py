from Configurables import *

DaVinci().Simulation = 0
DaVinci().EvtMax = -1
DaVinci().DataType = "2018"
DaVinci().TupleFile = "Ntuple.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = True

filterHlt2_eta2mumug = LoKi__HDRFilter(
    'MyHlt2Filter_Eta2MuMuGamma',
    Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision') | HLT_PASS('Hlt2CaloPIDD2EtapPiTurboCalibDecision')",
    Location="Hlt2/DecReports" )
DaVinci().EventPreFilters = [filterHlt2_eta2mumug]

#DaVinci().Turbo = True
#DaVinci().RootInTES = "/Event/Turbo"

#DaVinci().Input = ['/afs/cern.ch/work/c/chefdevi/public/Turcal2018/00075638_00000769_2.fullturbo.dst']
