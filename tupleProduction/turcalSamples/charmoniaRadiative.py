from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import *
from DecayTreeTuple.Configuration import *
from Configurables import PrintDecayTree,  DecayTreeTuple, DaVinci, SelDSTWriter, FilterDesktop, FilterInTrees, LoKi__HDRFilter, FilterDesktop,CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from Configurables import TupleToolRecoStats,TupleToolProtoPData, TupleToolCaloHypo
from Configurables import TupleToolKinematic, TupleToolTrackInfo, TupleToolPrimaries
from Configurables import TupleToolCaloHypo, TupleToolGeometry, TupleToolRecoStats, TupleToolPid, TupleToolTISTOS, TupleToolConeIsolation, TupleToolStripping
from PhysConf.Selections import PrintSelection  , FilterSelection , CombineSelection , Hlt1Selection , Hlt2Selection , L0Selection , TupleSelection

#input containers------

Photons = AutomaticData('Phys/StdLooseAllPhotons/Particles')
Muons = AutomaticData('Phys/StdAllLooseMuons/Particles')



evtPreselectors = []
#evtPreselectors.append(filterHlt2_eta2mumug)


#cuts------------

jpsi2mumu_name = 'JPsi2MuMu'
jpsi2mumu_decd = 'J/psi(1S) -> mu+ mu-'
jpsi2mumu_daug = {
    'mu+': '( MIPCHI2DV(PRIMARY)<6 ) & ( PROBNNmu > 0.3 ) & (PT > 100*MeV)',
    'mu-': '( MIPCHI2DV(PRIMARY)<6 ) & ( PROBNNmu > 0.3 ) & (PT > 100*MeV)'}
jpsi2mumu_comb = "(AM>2900*MeV) & (AM<3300*MeV)"
jpsi2mumu_moth = '( VFASPF(VCHI2/VDOF)<16 ) & ( BPVVDCHI2<100 ) '


charm2Jpsig_name = "Charm2JpsiGamma"
charm2Jpsig_decd = "chi_c0(1P) -> J/psi(1S) gamma"
charm2Jpsig_daug = {
    "gamma" : "(PT > 1000*MeV) & (CL > 0.05)",
    #'J/psi(1S)': 'ALL',
    #'mu+':'ALL',
    #'mu-':'ALL'
    }
charm2Jpsig_comb = "(AM>3.0*GeV) & (AM<4.0*GeV)"
#charm2Jpsig_comb = "ALL"

charm2Jpsig_moth = "(MM>3.0*GeV) & (MM<4.0*GeV)"

jpsi2mumu = CombineParticles(jpsi2mumu_name , DecayDescriptor=jpsi2mumu_decd , DaughtersCuts=jpsi2mumu_daug , CombinationCut=jpsi2mumu_comb , MotherCut=jpsi2mumu_moth)
jpsi2mumu_sel = Selection(jpsi2mumu_name + "_Sel" , Algorithm=jpsi2mumu ,  RequiredSelections=[Muons])
jpsi2mumu_seq = SelectionSequence(jpsi2mumu_name + "_Seq", TopSelection=jpsi2mumu_sel, EventPreSelector = evtPreselectors)

Jpsis = AutomaticData('Phys/JPsi2MuMu_Sel/Particles')
#Jpsis = AutomaticData("Phys/StdLooseJpsi2MuMu/Particles")
charm2Jpsig = CombineParticles(charm2Jpsig_name , DecayDescriptor=charm2Jpsig_decd , DaughtersCuts=charm2Jpsig_daug , CombinationCut=charm2Jpsig_comb , MotherCut=charm2Jpsig_moth)
charm2Jpsig.OutputLevel = 5
charm2Jpsig_sel = Selection(charm2Jpsig_name + "_Sel" , Algorithm=charm2Jpsig ,RequiredSelections=[Jpsis,Photons])
charm2Jpsig_seq = SelectionSequence(charm2Jpsig_name + "_Seq", TopSelection=charm2Jpsig_sel, EventPreSelector = evtPreselectors)



DaVinci().UserAlgorithms += [jpsi2mumu_seq]
DaVinci().UserAlgorithms += [charm2Jpsig_seq]
