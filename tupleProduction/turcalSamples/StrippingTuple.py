from Configurables import *
from DecayTreeTuple.Configuration import *
Butuple = DecayTreeTuple("DTTBs2GammaGammaStripping")
Butuple.Decay = "B_s0 -> ^gamma ^gamma"
Butuple.Inputs = ["Phys/Bs2GammaGamma_NoConvLine/Particles"]

Butuple.ToolList += [ #-- global event data
                     "TupleToolEventInfo",
                     "TupleToolPrimaries",
                     "TupleToolCPU",
                     #-- particle data
                     "TupleToolKinematic",
                     "TupleToolDalitz",
                     "TupleToolPid",
                     "TupleToolPropertime",
                     "TupleToolTrackInfo",
                     # "TupleToolHelicity",      # private tool
                     "TupleToolAngles",
                     "TupleToolPhotonInfo",
                     "TupleToolANNPID",           # includes all MCTuneV
                     ]


from Configurables import TupleToolProtoPData, TupleToolCaloHypo
NeutralsProto = TupleToolProtoPData("NeutralsProto")
NeutralsProto.DataList = ["CaloNeutralID",
                          "CaloNeutralSpd",
                          "IsNotH",
                          "IsNotE",
                          "IsPhoton",
                          "CellID",
                          "CaloNeutralID"
                          ]

NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

if DaVinci().getProp('Simulation'):
    mc_truth = Butuple.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    Butuple.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        Butuple.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]
        #Butupl.TupleToolMCBackgroundInfo.OutputLevel = VERBOSE

                                        
Butuple.addBranches({"B_s0": "B_s0 -> gamma gamma",
                     "gamma": "B_s0 -> ^gamma gamma",
                     "gamma0": "B_s0 -> gamma ^gamma",
                     })

Butuple.gamma.addTupleTool(NeutralsProto)
Butuple.gamma0.addTupleTool(NeutralsProto)

Butuple.gamma.addTupleTool(NeutralsHypo)
Butuple.gamma0.addTupleTool(NeutralsHypo)

Butuple.addTupleTool('TupleToolTISTOS/TISTOSTool')
Butuple.TISTOSTool.VerboseL0 = True
Butuple.TISTOSTool.VerboseHlt1 = True
Butuple.TISTOSTool.VerboseHlt2 = True
Butuple.TISTOSTool.TriggerList = ["L0ElectronDecision",
                                  "L0PhotonDecision",
                                  "Hlt1B2GammaGammaDecision",
                                  "Hlt1B2GammaGammaHighMassDecision",
                                  'Hlt1MBNoBiasDecision',
                                  "Hlt2RadiativeB2GammaGammaDecision"]

ConeIsol = TupleToolConeIsolation('TupleToolConeIsolation.B2gg',
                       FillComponents=True,
                       MinConeSize=1.0,
                       SizeStep=0.35,
                       MaxConeSize=2.05,
                       Verbose=False,
                       FillPi0Info=False,
                       FillMergedPi0Info=False,
                       FillAsymmetry=True,
                       FillDeltas= False,
                       ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                       MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                       )

Butuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
Butuple.addTupleTool(ConeIsol)
#dtt_stripping =Butuple.B_s0.addTupleTool('TupleToolStripping')
#dtt_stripping.StrippingList = ['StrippingBs2GammaGamma_NoConvLineDecision']

DaVinci().UserAlgorithms += [Butuple]
