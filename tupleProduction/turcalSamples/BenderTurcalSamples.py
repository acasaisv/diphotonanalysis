# -*- coding: utf-8 -*-
#!/usr/bin/env python
from Bender.Main import *
import math as m
import BenderTools.Fill
from   LoKiPhys.decorators import *
import BenderTools.TisTos
from LoKiCore.math import sqrt
from ROOT import TFile, TTree
import os 
import IPython


        
    
class B2ggFromEta(Algo):
    
    ###############################################################
    def initialize ( self ) :
       
        sc = Algo.initialize ( self )
        if sc.isFailure() : return sc
        
        self.IsMC = True
        
        self.pid = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPid', parent=self)
        if not self.pid : return FAILURE
        
        self.trig = self.tool(cpp.IEventTupleTool,
                              'TupleToolTrigger', parent=self)
        
        if not self.trig : return FAILURE
        
        self.tuptistos = self.tool(cpp.IParticleTupleTool,
                                   'TupleToolTISTOS')


        if not self.tuptistos : return FAILURE
        
        self.kin = self.tool(cpp.IParticleTupleTool,
                             'TupleToolKinematic', parent=self)
        if not self.kin : return FAILURE
        
        self.geo = self.tool(cpp.IParticleTupleTool,
                             'TupleToolGeometry', parent=self)
        if not self.geo : return FAILURE
        
        self.prim = self.tool(cpp.IEventTupleTool,
                              'TupleToolPrimaries', parent=self)
        if not self.prim : return FAILURE
        
        self.track = self.tool(cpp.IParticleTupleTool,
                               'TupleToolTrackInfo', parent=self)
        if not self.track : return FAILURE
        
        self.proptime = self.tool(cpp.IParticleTupleTool,
                                  'TupleToolPropertime', parent=self)
        if not self.proptime : return FAILURE
        
        self.event = self.tool(cpp.IEventTupleTool,
                                'TupleToolEventInfo', parent=self)
        if not self.event : return FAILURE
        
        self.muon = self.tool(cpp.IParticleTupleTool,
                              'TupleToolMuonPid', parent=self)
        if not self.muon : return FAILURE

        self.mc = self.tool(cpp.IParticleTupleTool,
                            'TupleToolMCTruth', parent=self)
        if not self.mc : return FAILURE
        
        self.recostats = self.tool(cpp.IEventTupleTool,
                            'TupleToolRecoStats', parent=self)
        if not self.recostats : return FAILURE
        
        self.coneiso = self.tool(cpp.IParticleTupleTool,
                            'TupleToolConeIsolation', parent=self)
        if not self.coneiso : return FAILURE

        self.photoninfo = self.tool(cpp.IParticleTupleTool,
                            'TupleToolPhotonInfo', parent=self)
        if not self.photoninfo : return FAILURE

        self.tuplecalohypo = self.tool(cpp.IParticleTupleTool,
                                        'TupleToolCaloHypo',parent=self)
        if not self.tuplecalohypo : return FAILURE

        self.tupleprotopdata = self.tool(cpp.IParticleTupleTool,
                                        'TupleToolProtoPData',parent=self)
        if not self.tupleprotopdata : return FAILURE
        
        self._dtf = self.decayTreeFitter('LoKi::DecayTreeFit')
        
        if not self._dtf  : return FAILURE
        
        import ROOT 
        ROOT.gInterpreter.ProcessLine("#include \"Kernel/IMatterVeto.h\"") 
        
        self.matterveto = self.tool(cpp.IMatterVeto,
                            'MatterVetoTool', parent=self)
        if not self.matterveto : return FAILURE
        return SUCCESS
    
    ################################################################

    def ISMC( self, sim=True):
    
        self.IsMC = sim

    def id_matching(self,line_gamma,jet_gamma):
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False

        return (line_gamma.proto().calo()[0].key() == jet_gamma.proto().calo()[0].key())
    
    def calo_matching(self,line_gamma,jet_gamma):
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
        if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
        if not jet_gamma.proto().calo()[0] or not line_gamma.proto().calo()[0]: return False

        return (jet_gamma.proto().calo()[0].data() == line_gamma.proto().calo()[0].data())


    def kinetic_matching(self,line_gamma,jet_gamma,tolerance = [0.001,0.001]):
        #if not jet_gamma.particleID().pid()==22: return False
        if not jet_gamma.proto() or not line_gamma.proto(): return False
        if not jet_gamma.proto().calo() or not line_gamma.proto().calo(): return False
        if not jet_gamma.proto().calo().size() or not line_gamma.proto().calo().size(): return False
        if not jet_gamma.proto().calo().data() or not line_gamma.proto().calo().data(): return False
        if not jet_gamma.proto().calo()[0].position() or not line_gamma.proto().calo()[0].position(): return False
        if line_gamma.proto().calo()[0].data().position().x() == 0: return False
        if line_gamma.proto().calo()[0].data().position().y() == 0: return False
        if line_gamma.pt().value() == 0: return False
        x_match = abs((line_gamma.proto().calo()[0].data().position().x() - jet_gamma.proto().calo()[0].data().position().x())/(line_gamma.proto().calo()[0].data().position().x())) <= tolerance[0]
        y_match = abs((line_gamma.proto().calo()[0].data().position().y() - jet_gamma.proto().calo()[0].data().position().y())/line_gamma.proto().calo()[0].data().position().y()) <= tolerance[0]
        pos_match = x_match and y_match
        pt_match = abs(line_gamma.pt().value()-jet_gamma.pt().value())/line_gamma.pt().value()<=tolerance[1]
        
        return (pos_match)


    ## the main 'analysis' method
    def analyse( self ) :   ## IMPORTANT!
        
        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        gammas = self.get('Phys/EtaGammas/Particles')
        etas = self.get('Phys/Eta2MuMuGamma_Sel/Particles')

        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:
        
            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            etas_filtered = []
            eta0,eta1 = 0,0
            pt0,pt1 = 0,0
            for eta in etas:
                if not (eta.daughters() and eta.daughters().size()): continue
                gamma = eta.daughters()[2]
                if matching(gamma,g0) and (eta.momentum().pt() > pt0):
                    eta0 = eta
                    pt0 = eta.momentum().pt()
                    
            if not eta0: continue
            
            for eta in etas:
                if not (eta.daughters() and eta.daughters().size()): continue
                gamma_ = eta.daughters()[2]
                if matching(gamma_,eta0.daughters()[2]): continue
                if matching(gamma_,g1) and (eta.momentum().pt() > pt1):
                    eta1 = eta
                    pt1 = eta.momentum().pt()
                    
            if not(eta0 and eta1): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            
            for eta,index in zip([eta0,eta1],[0,1]):
                mup = eta.daughters()[0]
                mum = eta.daughters()[1]
                gamma = eta.daughters()[2]
                names = ['eta{}','muplus{}','muminus{}','gamma_eta{}']
                names = [_name.format(index) for _name in names]
                for part_,name_ in zip([eta,mup,mum,gamma],names):
                    self.kin.fill(eta, part_, name_, tup)
                    self.geo.fill(eta, part_, name_, tup)
                    self.track.fill(eta, part_, name_, tup)
                    if 'mu' in name_:
                        self.pid.fill(eta, part_, name_, tup)
                    if 'gamma' in name_:
                        self.photoninfo.fill(eta0, part_, name_, tup)
                        self.tuptistos.fill(eta0,part_,name_,tup)        
                
            #RELINFO
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)                        
                
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!
    
    ################################################################
    
    def finalize ( self ) :
        
        del self.pid
        del self.trig
        del self.tuptistos
        del self.kin
        del self.geo
        del self.prim
        del self.track
        del self.proptime
        del self.event
        del self.muon
        del self._dtf
        del self.coneiso
        del self.recostats
        del self.matterveto
        
        return Algo.finalize ( self )

    ################################################################

    
    


class B2ggFromEta2gg(B2ggFromEta):
    def analyse( self ) :   ## IMPORTANT!

        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        eta2gg_gammas = self.get('Phys/Eta2ggGammas/Particles')
        eta2ggs = self.get('Phys/Eta2GammaGamma_Sel/Particles')

        eta_gammas = self.get('Phys/EtaGammas/Particles')
        etas = self.get('Phys/Eta2MuMuGamma_Sel/Particles')

        

        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:
            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            etas_filtered = []
            eta0 = 0
            pt0 = 0
            for eta in etas:
                if not (eta.daughters() and eta.daughters().size()): continue
                gamma_eta = eta.daughters()[2]
                if matching(gamma_eta,g0) and eta.momentum().pt()>pt0:
                    eta0 = eta
                    pt0 =eta.momentum().pt()
                if matching(gamma_eta,g1) and eta.momentum().pt()>pt0:
                    eta0 = eta
                    pt0 =eta.momentum().pt()
                        
            eta2gg0 = 0
            pt_eta0 =0

            for eta2gg in eta2ggs:
                if not (eta2gg.daughters() and eta2gg.daughters().size()): continue
                for gamma_eta2gg in eta2gg.daughters():
                    if matching(eta0.daughters()[2],gamma_eta2gg):
                        eta2gg0 = 0
                        break
                    if matching(gamma_eta2gg,g0) and eta2gg.momentum().pt() > pt_eta0:
                        eta2gg0 = eta2gg
                        pt_eta0 = eta2gg.momentum().pt()
                    if matching(gamma_eta2gg,g1) and eta2gg.momentum().pt() > pt_eta0:
                        eta2gg0 = eta2gg
                        pt_eta0 = eta2gg.momentum().pt()
                
            if not(eta0 and eta2gg0): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            
            mup = eta0.daughters()[0]
            mum = eta0.daughters()[1]
            
            gamma = eta0.daughters()[2]
            names = ['eta0','muplus','muminus','gamma_eta']
            for part_,name_ in zip([eta0,mup,mum,gamma],names):
                self.kin.fill(eta0, part_, name_, tup)
                self.geo.fill(eta0, part_, name_, tup)
                self.track.fill(eta0, part_, name_, tup)
                if 'mu' in name_:
                    self.pid.fill(eta0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(eta0, part_, name_, tup)
                    self.tuptistos.fill(eta0,part_,name_,tup)

            
            gamma_eta0 = eta2gg0.daughters()[0]
            gamma_eta1 = eta2gg0.daughters()[1]
            names = ['eta1','gamma_eta0','gamma_eta1']
            for part_,name_ in zip([eta2gg0,gamma_eta0,gamma_eta1],names):
                self.kin.fill(eta2gg0, part_, name_, tup)
                self.geo.fill(eta2gg0, part_, name_, tup)
                self.track.fill(eta2gg0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(eta2gg0, part_, name_, tup)
                    self.tuptistos.fill(eta2gg0, part_, name_, tup)
                
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma1"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)
                    self.photoninfo.fill(p, part, name, tup)
            
                
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!




class B2ggFromEtaCharm(B2ggFromEta):
    def analyse( self ) :   ## IMPORTANT!
        
        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        charm_gammas = self.get('Phys/D2EtapPiGammas/Particles')
        charms = self.get('Phys/D2EtapPi_Sel/Particles')

        eta_gammas = self.get('Phys/EtaGammas/Particles')
        etas = self.get('Phys/Eta2MuMuGamma_Sel/Particles')

        

        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:
            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            eta0 = 0
            pt0 = 0
            for eta in etas:
                if not (eta.daughters() and eta.daughters().size()): continue
                gamma_eta = eta.daughters()[2]
                if matching(gamma_eta,g0) and eta.momentum().pt() > pt0:
                    eta0 = eta
                    pt0 = eta.momentum().pt()
                if matching(gamma_eta,g1) and eta.momentum().pt() > pt0:
                    eta0 = eta
                    pt0 = eta.momentum().pt()
                
            charm0 = 0
            pt_charm0 = 0
            for charm in charms:
                if not (charm.daughters() and charm.daughters().size()): continue
                etap = charm.daughters()[0]
                if not (etap.daughters() and etap.daughters().size()): continue
                
                gamma_charm = etap.daughters()[2]
                if matching(gamma_charm,eta0.daughters()[2]):
                    continue
                if matching(gamma_charm,g0) and charm.momentum().pt() > pt_charm0:
                    charm0 = charm
                    pt_charm0 = eta.momentum().pt()
                if matching(gamma_charm,g1) and charm.momentum().pt() > pt_charm0:
                    charm0 = charm
                    pt_charm0 = eta.momentum().pt()
                
            if not(eta0 and charm0): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            mup = eta0.daughters()[0]
            mum = eta0.daughters()[1]
            gamma = eta0.daughters()[2]
            names = ['eta','muplus_eta','muminus_eta','gamma_eta']
            for part_,name_ in zip([eta0,mup,mum,gamma],names):
                self.kin.fill(eta0, part_, name_, tup)
                self.geo.fill(eta0, part_, name_, tup)
                self.track.fill(eta0, part_, name_, tup)
                if 'mu' in name_:
                    self.pid.fill(eta0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(eta0, part_, name_, tup)
                    self.tuptistos.fill(eta0,part_,name_,tup)        


            etap = charm0.daughters()[0]
            pip = etap.daughters()[0]
            pim = etap.daughters()[1]
            gamma = etap.daughters()[2]
            pip0 = charm0.daughters()[1]
            names = ['charm','eta_prime','piplus_etaprime','piminus_etaprime','gamma_etaprime','pi_charm']
            for part_,name_ in zip([charm0,etap,pip,pim,gamma,pip0],names):
                self.kin.fill(charm0, part_, name_, tup)
                self.geo.fill(charm0, part_, name_, tup)
                self.track.fill(charm0, part_, name_, tup)
                if 'pi' in name_:
                    self.pid.fill(charm0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(charm0, part_, name_, tup)
                    self.tuptistos.fill(charm0,part_,name_,tup)        


                
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma1"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)                        

                
            if (eta0 and charm0): tup.write()
            
        return SUCCESS      ## IMPORTANT!!!

class B2ggFromEta2ggCharm(B2ggFromEta):
    def analyse( self ) :   ## IMPORTANT!

        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        eta2gg_gammas = self.get('Phys/Eta2ggGammas/Particles')
        eta2ggs = self.get('Phys/Eta2GammaGamma_Sel/Particles')

        charm_gammas = self.get('Phys/D2EtapPiGammas/Particles')
        charms = self.get('Phys/D2EtapPi_Sel/Particles')

        

        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:
            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            etas_filtered = []
            charm0 = 0
            pt0 = 0
            for charm in charms:
                if not (charm.daughters() and charm.daughters().size()): continue
                etap = charm.daughters()[0]
                if not (etap.daughters() and etap.daughters().size()): continue
                gamma_charm = etap.daughters()[2]
                if matching(gamma_charm,g0) and charm.momentum().pt()>pt0:
                    charm0 = charm
                    pt0 =charm.momentum().pt()
                if matching(gamma_charm,g1) and charm.momentum().pt()>pt0:
                    charm0 = charm
                    pt0 =charm.momentum().pt()
                        
            eta2gg0 = 0
            pt_eta0 =0

            for eta2gg in eta2ggs:
                if not (eta2gg.daughters() and eta2gg.daughters().size()): continue
                for gamma_eta2gg in eta2gg.daughters():
                    if matching(charm0.daughters()[0].daughters()[2],gamma_eta2gg):
                        eta2gg0 = 0
                        break
                    if matching(gamma_eta2gg,g0) and eta2gg.momentum().pt() > pt_eta0:
                        eta2gg0 = eta2gg
                        pt_eta0 = eta2gg.momentum().pt()
                    if matching(gamma_eta2gg,g1) and eta2gg.momentum().pt() > pt_eta0:
                        eta2gg0 = eta2gg
                        pt_eta0 = eta2gg.momentum().pt()
                
            if not(charm0 and eta2gg0): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            etap = charm0.daughters()[0]
            pip = etap.daughters()[0]
            pim = etap.daughters()[1]
            gamma = etap.daughters()[2]
            pi_charm = charm0.daughters()[1]
            
            names = ['charm','etap','piplus_etap','piminus_etap','gamma_etap','pi_charm']
            for part_,name_ in zip([charm0,etap,pip,pim,gamma,pi_charm],names):
                self.kin.fill(charm0, part_, name_, tup)
                self.geo.fill(charm0, part_, name_, tup)
                self.track.fill(charm0, part_, name_, tup)
                if 'pi' in name_:
                    self.pid.fill(charm0, part_, name_, tup)
                if 'gamma' in name_:
                        self.photoninfo.fill(charm0, part_, name_, tup)
                        self.tuptistos.fill(charm0,part_,name_,tup)        

            
            gamma_eta0 = eta2gg0.daughters()[0]
            gamma_eta1 = eta2gg0.daughters()[1]
            names = ['eta','gamma_eta0','gamma_eta1']
            for part_,name_ in zip([eta2gg0,gamma_eta0,gamma_eta1],names):
                self.kin.fill(eta2gg0, part_, name_, tup)
                self.geo.fill(eta2gg0, part_, name_, tup)
                self.track.fill(eta2gg0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(eta2gg0, part_, name_, tup)
                    self.tuptistos.fill(eta2gg0, part_, name_, tup)
                
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma1"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)
                    self.photoninfo.fill(p, part, name, tup)
            
                
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!


class B2ggFromPi0Charm(B2ggFromEta):
    def analyse( self ) :   ## IMPORTANT!

        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        eta2gg_gammas = self.get('Phys/Pi02ggGammas/Particles')
        pi0s = self.get('Phys/StdLooseResolvedPi0/Particles')

        charm_gammas = self.get('Phys/D2EtapPiGammas/Particles')
        charms = self.get('Phys/D2EtapPi_Sel/Particles')


        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:

            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            etas_filtered = []
            charm0 = 0
            pt0=0
            
            for charm in charms:
                if not (charm.daughters() and charm.daughters().size()): continue
                etap = charm.daughters()[0]
                if not (etap.daughters() and etap.daughters().size()): continue
                gamma_charm = etap.daughters()[2]
                if matching(gamma_charm,g0) and charm.momentum().pt()>pt0:
                    charm0 = charm
                    pt0 =charm.momentum().pt()
                if matching(gamma_charm,g1) and charm.momentum().pt()>pt0:
                    charm0 = charm
                    pt0 =charm.momentum().pt()

                        
            pi00 = 0
            pt_pi00 =0

            for pi0 in pi0s:
                if not (pi0.daughters() and pi0.daughters().size()): continue
                for gamma_pi0 in pi0.daughters():
                    if matching(charm0.daughters()[0].daughters()[2],gamma_pi0):
                        pi00 = 0
                        break
                    if matching(gamma_pi0,g0) and pi0.momentum().pt() > pt_pi00:
                        pi00 = pi0
                        pt_pi00 = pi0.momentum().pt()
                    if matching(gamma_pi0,g1) and pi0.momentum().pt() > pt_pi00:
                        pi00 = pi0
                        pt_pi00 = pi0.momentum().pt()
                
            if not(charm0 and pi00): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            etap = charm0.daughters()[0]
            pip = etap.daughters()[0]
            pim = etap.daughters()[1]
            gamma = etap.daughters()[2]
            pi_charm = charm0.daughters()[1]
            
            names = ['charm','etap','piplus_etap','piminus_etap','gamma_etap','pi_charm']
            for part_,name_ in zip([charm0,etap,pip,pim,gamma,pi_charm],names):
                self.kin.fill(charm0, part_, name_, tup)
                self.geo.fill(charm0, part_, name_, tup)
                self.track.fill(charm0, part_, name_, tup)
                if 'pi' in name_:
                    self.pid.fill(charm0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(charm0, part_, name_, tup)
                    self.tuptistos.fill(charm0, part_, name_, tup)
        

            
            gamma_pi00 = pi00.daughters()[0]
            gamma_pi01 = pi00.daughters()[1]
            names = ['pi0','gamma_pi00','gamma_pi01']
            for part_,name_ in zip([pi00,gamma_pi00,gamma_pi01],names):
                self.kin.fill(pi00, part_, name_, tup)
                self.geo.fill(pi00, part_, name_, tup)
                self.track.fill(pi00, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(pi00, part_, name_, tup)
                    self.tuptistos.fill(pi00, part_, name_, tup)
        
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma1"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)                             
                    self.photoninfo.fill(p, part, name, tup)
        
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!

class B2ggFromPi0(B2ggFromEta):
    def analyse( self ) :   ## IMPORTANT!

        header  = self.get( '/Event/Rec/Header' )
        particles = self.select('B_s0','B_s0 -> gamma gamma')
        eta2gg_gammas = self.get('Phys/Pi02ggGammas/Particles')
        pi0s = self.get('Phys/StdLooseResolvedPi0/Particles')

        eta_gammas = self.get('Phys/EtaGammas/Particles')
        etas = self.get('Phys/Eta2MuMuGamma_Sel/Particles')

        

        if particles.empty(): return self.Warning("No input particles", SUCCESS)
        
        for p in particles:

            g0 = p.daughters()[0]
            g1 = p.daughters()[1]
            matching = self.kinetic_matching
            etas_filtered = []
            eta0 = 0
            pt0 = 0
            for eta in etas:
                if not (eta.daughters() and eta.daughters().size()): continue
                gamma_eta = eta.daughters()[2]
                if matching(gamma_eta,g0) and eta.momentum().pt()>pt0:
                    eta0 = eta
                    pt0 =eta.momentum().pt()
                if matching(gamma_eta,g1) and eta.momentum().pt()>pt0:
                    eta0 = eta
                    pt0 =eta.momentum().pt()
                        
            pi00 = 0
            pt_pi00 =0

            for pi0 in pi0s:
                if not (pi0.daughters() and pi0.daughters().size()): continue
                for gamma_pi0 in pi0.daughters():
                    if matching(eta0.daughters()[2],gamma_pi0):
                        pi00 = 0
                        break
                    if matching(gamma_pi0,g0) and pi0.momentum().pt() > pt_pi00:
                        pi00 = pi0
                        pt_pi00 = pi0.momentum().pt()
                    if matching(gamma_pi0,g1) and pi0.momentum().pt() > pt_pi00:
                        pi00 = pi0
                        pt_pi00 = pi0.momentum().pt()
                
            if not(eta0 and pi00): continue
            tup = self.nTuple('DecayTree')
            self.event.fill(tup)
            self.prim.fill(tup)

            
            mup = eta0.daughters()[0]
            mum = eta0.daughters()[1]
            
            gamma = eta0.daughters()[2]
            names = ['eta0','muplus','muminus','gamma_eta']
            for part_,name_ in zip([eta0,mup,mum,gamma],names):
                self.kin.fill(eta0, part_, name_, tup)
                self.geo.fill(eta0, part_, name_, tup)
                self.track.fill(eta0, part_, name_, tup)
                if 'mu' in name_:
                    self.pid.fill(eta0, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(eta0, part_, name_, tup)
                    self.tuptistos.fill(eta0, part_, name_, tup)
        

            
            gamma_pi00 = pi00.daughters()[0]
            gamma_pi01 = pi00.daughters()[1]
            names = ['pi0','gamma_pi00','gamma_pi01']
            for part_,name_ in zip([pi00,gamma_pi00,gamma_pi01],names):
                self.kin.fill(pi00, part_, name_, tup)
                self.geo.fill(pi00, part_, name_, tup)
                self.track.fill(pi00, part_, name_, tup)
                if 'gamma' in name_:
                    self.photoninfo.fill(pi00, part_, name_, tup)
        
            parts = [p,g0,g1]
            names = ["B_s0","gamma0","gamma1"]
            names_cones = {"B_s0":"B","gamma0":"Gamma1","gamma":"Gamma2"}
            for part,name in zip(parts,names):
                self.kin.fill(p, part, name, tup)
                #self.mc.fill(p,part,name,tup)
                self.tuptistos.fill(p,part,name,tup)
                self.pid.fill(p, part, name, tup)
                self.geo.fill(p, part, name, tup)
                self.track.fill(p, part, name, tup)
                self.coneiso.fill(p,part,name,tup)
                if name != 'B_s0':
                    self.tupleprotopdata.fill(p,part,name,tup)
                    self.tuplecalohypo.fill(p,part,name,tup)                             
                    self.photoninfo.fill(p, part, name, tup)
        
            tup.write()
            
        return SUCCESS      ## IMPORTANT!!!


    
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files
               catalogs = []    ,    ## xml-catalogs (filled by GRID)
               castor   = False ,    ## use the direct access to castor/EOS ?
               params   = {}   ) :
    
    year       = params.get('Year',2016)
    pol        = params.get('Polarity','MagDown')
    info       = params.get('Info','tuple')
    sim        = params.get('Simulation',True)
    stream     = params.get('Stream','EW')
    file_name  = params.get('FileName','ntuple')
    decay_descriptor = params.get('Decay Descriptor','B_s0 -> gamma gamma')
    needs_tags = params.get('NeedsTags',False)
    dddbtag = params.get("DDDBtag","dddb-20170721-3")
    conddbtag = params.get("CondDBtag","sim-20170721-2-vc-md100")


    
    ## import DaVinci
    from Configurables import DaVinci, EventSelector,LoKi__HDRFilter
    dv = DaVinci (
                  DataType   = str(year) ,
                  InputType  = 'DST'  ,
                  Simulation = sim  )
    
    if needs_tags:
        dv.DDDBtag = dddbtag
        dv.CondDBtag = conddbtag

    

    #####
    #importOptions('pool_xml_catalog.py')
    #####
    
    dv.TupleFile = "tuple.root"


    importOptions('./davinci.py')
    #importOptions('./charmoniaRadiative.py')
    #importOptions('./SelWDs.py')
    
    filterHlt2_eta2mumug = LoKi__HDRFilter( 'MyHlt2Filter_Eta2MuMuGamma', Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision') | HLT_PASS('Hlt2CaloPIDD2EtapPiTurboCalibDecision')", Location="Hlt2/DecReports" )
    dv.EventPreFilters = [filterHlt2_eta2mumug]


    
    #
    ## Cuts dictionary
    # 

    cuts =              {
                          'gammaPT'             : 1000    # MeV/c
                          #'gammaPT'             : 500    # MeV/c
                          ,'gammaP'              : 6000   # MeV/c
                          ,'gammaCL'             : 0.3     # adimensional
                          #,'gammaCL'             : 0.05   # adimensional
                          ,'gammaNonePT'         : 1000    # MeV/                       
                          #,'gammaNonePT'         : 500      # MeV/
                          ,'gammaNoneP'          : 6000   # MeV/c
                          ,'gammaNoneCL'         : 0.3    # adimensional
                          ,'NoConvHCAL2ECAL'     : 0.1   # adimensional
                          ,'BsPT'                : 2000    # MeV/c
                          #,'BsPT'                : 500    # MeV/c
                          ,'BsVertexCHI2pDOF'    : 20      # adimensional
                          ,'BsLowMass'           : 4300    # MeV/cc
                          #,'BsLowMass'           : 100    # MeV/cc
                          #,'BsNonePT'            : 2000    # MeV/c
                          ,'BsNonePT'            : 2000    # MeV/c
                          ,'BsLowMassDouble'     : 4000    # MeV/cc
                          ,'BsLowMassNone'       : 4800    # MeV/cc
                          #,'BsLowMassNone'       : 100    # MeV/cc
                          ,'BsLowMassNoneWide'   : 0       # MeV/cc
                          ,'BsHighMass'          : 20000    # MeV/cc
                          ,'BsHighMassNone'      : 20000    # MeV/cc
                          ,'BsHighMassNoneWide'  : 4800   # MeV/cc
                          ,'scaleWide'           : 1.0
                          ,'HLT1None'            : "HLT_PASS_RE('Hlt1.*GammaGammaDecision')"
                          ,'HLT2None'            : "HLT_PASS_RE('Hlt2.*GammaGammaDecision')"
                          }


    #
    ## Setting up clean photon selection
    #
    from Configurables import FilterDesktop, FilterInTrees
    from PhysSelPython.Wrappers import Selection, SelectionSequence,DataOnDemand
    from Configurables import CombineParticles
    
    
    fltrCode_nonConv = " (PT>%(gammaPT)s*MeV) & (CL>%(gammaCL)s) & (MAXTREE(ISBASIC,PPINFO( LHCb.ProtoParticle.CaloNeutralHcal2Ecal,1000 ))<%(NoConvHCAL2ECAL)s) " % cuts
    
    _photons= FilterInTrees( "GammasDesktop",Code = "(ABSID== 'gamma') & "+fltrCode_nonConv, CloneFilteredParticles=True)
    
    eta_photons = Selection("EtaGammas",Algorithm = _photons,RequiredSelections=[DataOnDemand('Phys/Eta2MuMuGamma_Sel/Particles')])
    eta_photons_seq = SelectionSequence('EtaGammasSeq',TopSelection=eta_photons)

    eta2gg_photons = Selection("Eta2ggGammas",Algorithm = _photons,RequiredSelections=[DataOnDemand('Phys/Eta2GammaGamma_Sel/Particles')])
    eta2gg_photons_seq = SelectionSequence('Eta2ggGammasSeq',TopSelection=eta2gg_photons)

    pi02gg_photons = Selection("Pi02ggGammas",Algorithm = _photons,RequiredSelections=[DataOnDemand('Phys/StdLooseResolvedPi0/Particles')])
    pi02gg_photons_seq = SelectionSequence('Pi02ggGammasSeq',TopSelection=pi02gg_photons)

    d2etapPi_photons = Selection("D2EtapPiGammas",Algorithm = _photons,RequiredSelections=[DataOnDemand('Phys/D2EtapPi_Sel/Particles')])
    d2etapPi_photons_seq = SelectionSequence('D2EtapPiGammasSeq',TopSelection=d2etapPi_photons)
    
    dv.UserAlgorithms +=[eta_photons_seq,
                         eta2gg_photons_seq,
                         pi02gg_photons_seq,
                         d2etapPi_photons_seq
                         ]
   
    
    eta_gammas = DataOnDemand(Location = 'Phys/EtaGammas/Particles')
    eta2gg_gammas = DataOnDemand(Location = 'Phys/Eta2ggGammas/Particles')
    pi02gg_gammas = DataOnDemand(Location = 'Phys/Pi02ggGammas/Particles')
    d2etapPi_gammas = DataOnDemand(Location = 'Phys/D2EtapPiGammas/Particles')
    
    
    #
    ## Setting up selection
    #
    
    def TOSFilter( name = None, sel = None, trigger = None ):
        from Configurables import TisTosParticleTagger
        _filter = TisTosParticleTagger(name+"_TriggerTos")
        _filter.TisTosSpecs = { trigger+"%TOS" : 0 }
        from PhysSelPython.Wrappers import Selection
        _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = sel, Algorithm = _filter )
        return _sel



    #can this flag be configured externally ?
    name = "Bs2gg"
    wide = False

    from DecayTreeTuple.Configuration import TupleToolMCTruth
    from Configurables import DecayTreeTuple
    #from DecayTreeTuple.Configuration import *

    def configB2ggSelection(name,wide,cuts,photons,mother_cut,caloPostCalib=True):
        BsGG_DC_none = "(PT>%(gammaNonePT)s*MeV) & (P>%(gammaNoneP)s*MeV) & (CL>%(gammaNoneCL)s)" % cuts
        if wide == True:
            BsGG_CC_none = "(in_range( ( %(BsLowMassNoneWide)s )*MeV, AM, ( %(BsHighMassNoneWide)s  )*MeV) )" % cuts
            BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range( ( %(BsLowMassNoneWide)s )*MeV, M, ( %(BsHighMassNoneWide)s )*MeV) )" % cuts
        else:
            BsGG_CC_none = "(in_range(%(BsLowMassNone)s*MeV, AM, %(BsHighMassNone)s*MeV))" % cuts
            BsGG_MC_none = "(PT>%(BsNonePT)s*MeV) & (in_range(%(BsLowMassNone)s*MeV, M, %(BsHighMassNone)s*MeV))" % cuts

        BsGG_MC_none += mother_cut
            
        suffix=''
        if wide == True:
            scaleWide = config['scaleWide']
            suffix='wide'
        else:
            scaleWide = 1.0

        _Bs2gammagamma_none = CombineParticles(name = "CombineParticles_{}".format(name),
                                               DecayDescriptor = "B_s0 -> gamma gamma"
                                               , DaughtersCuts  = {'gamma' : BsGG_DC_none}
                                               , CombinationCut = BsGG_CC_none
                                               , MotherCut      = BsGG_MC_none)
        _Bs2gammagamma_none.ParticleCombiners.update({ '' : 'MomentumCombiner:PUBLIC'})

        Bs2gammagamma_none = Selection(
            name+"_none",
            Algorithm = _Bs2gammagamma_none,
            RequiredSelections = photons)
        
        Bs2gg_seq = SelectionSequence("{}Seq".format(name), TopSelection=Bs2gammagamma_none)

    
        # topselL0 = TOSFilter(name+"_NoConvTOSLineL0",[Bs2gammagamma_none],"L0(Photon|Electron)Decision")
        # topselHLT1 = TOSFilter(name+"_NoConvTOSLineHLT1",[topselL0],"Hlt1(B2GammaGamma|B2GammaGammaHighMass)Decision")
        # topselHLT2 = TOSFilter(name+"_NoConvTOSLineHLT2",[topselHLT1],"Hlt2(RadiativeB2GammaGamma)Decision")
        
        #sel = TOSFilter("dummysel",[Bs2gammagamma_none],"Hlt1B2GammaGammaHighMassDecision")

    
    
        DaVinci().UserAlgorithms+=[Bs2gg_seq]


        #Setting trigger lines for the job
        from DecayTreeTuple.Configuration import TupleToolMCTruth
        from Configurables import TupleToolTrigger, TupleToolTISTOS
        Trigger_List = [
            "L0ElectronDecision",
            "L0PhotonDecision",
            "L0MuonDecision",
            "L0DiMuonDecision",
            "Hlt1B2GammaGammaDecision",
            "Hlt1B2GammaGammaHighMassDecision",
            'Hlt1MBNoBiasDecision',
            "Hlt2RadiativeB2GammaGammaDecision"
            ]

    
    

        tistostool  = TupleToolTISTOS('{}Tuple.TupleToolTISTOS'.format(name))
        tistostool.Verbose = True
        tistostool.TriggerList = Trigger_List
    
        from Configurables import TupleToolProtoPData, TupleToolCaloHypo
        protopdata = TupleToolProtoPData('{}Tuple.TupleToolProtoPData'.format(name))
        protopdata.DataList = [ "IsPhoton",
                                "IsNotE",
                                "IsNotH",
                                'CaloNeutralID',
                                "CaloNeutralSpd",
                                'CellID']
        
        NeutralsHypo = TupleToolCaloHypo("{}Tuple.TupleToolCaloHypo".format(name))
        NeutralsHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

    
    
    
        from Configurables import TupleToolConeIsolation
        ConeIsol = TupleToolConeIsolation('{}Tuple.TupleToolConeIsolation'.format(name),
                                          FillComponents=True,
                                          MinConeSize=1.0,
                                      
                                          SizeStep=0.35,
                                          MaxConeSize=2.05,
                                          Verbose=True,
                                          FillPi0Info=False,
                                          FillMergedPi0Info=False,
                                          FillAsymmetry=True,
                                          FillDeltas= False,
                                          ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                                          MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                                          )
        ConeIsol.OutputLevel=3.

        from Configurables import TupleToolGeometry

        geometry = TupleToolGeometry('{}Tuple.TupleToolGeometry',
                                     Verbose = True)
        
        #Input
        
        from PhysConf.Selections import AutomaticData
        
        input = AutomaticData( 'Phys/{}_none/Particles'.format(name) )
    
        input_sel = BenderSelection ( '{}Tuple'.format(name) , input )
        
        dv.UserAlgorithms.append(input_sel)

        if caloPostCalib:
            importOptions('./caloPostCalib.py')
            from Configurables import CaloPostCalibAlg
            cpc = CaloPostCalibAlg('CaloPostCalib')
            cpc_inputs = [
                'Phys/B2GammaGammaFromEta/Particles_none',
                'Phys/B2GammaGammaFromEta2gg/Particles_none',
                'Phys/B2GammaGammaFromPi02gg/Particles_none',
                'Phys/B2GammaGammaFromEtaCharm/Particles_none',
                'Phys/B2GammaGammaFromPi02ggCharm/Particles_none',
                'Phys/B2GammaGammaFromEta2ggCharm/Particles_none',
                          ]
            cpc.Inputs=cpc_inputs
            dv.UserAlgorithms  += [cpc]

        
        return input_sel
        
    
    dv.ProductionType = "Stripping"

    if type(inputdata) == list:
        setData(inputdata,catalogs,grid =True,useDBtags = False)
    if type(inputdata) == str:
        importOptions(str)

    
    

    from Configurables import EventTuple
    dv.UserAlgorithms += [EventTuple("EventTuple")]

    
    input_sel = configB2ggSelection('B2GammaGammaFromEta',wide,cuts,[eta_gammas],mother_cut=" & (NINTREE(INTES('Phys/EtaGammas/Particles')) >1 )")
    input_sel5 = configB2ggSelection('B2GammaGammaFromEta2gg',wide,cuts,[eta_gammas,eta2gg_gammas],mother_cut=" & (NINTREE(INTES('/Event/Phys/EtaGammas/Particles')) == 1) & (NINTREE(INTES('/Event/Phys/Eta2ggGammas/Particles')) == 1)")
    input_sel6 = configB2ggSelection('B2GammaGammaFromPi02gg',wide,cuts,[eta_gammas,pi02gg_gammas],mother_cut=" & (NINTREE(INTES('/Event/Phys/EtaGammas/Particles')) == 1) & (NINTREE(INTES('/Event/Phys/Pi02ggGammas/Particles')) == 1)")
    input_sel7 = configB2ggSelection('B2GammaGammaFromEtaCharm',wide,cuts,[eta_gammas,d2etapPi_gammas],mother_cut=" & (NINTREE(INTES('/Event/Phys/EtaGammas/Particles')) == 1) & (NINTREE(INTES('/Event/Phys/D2EtapPiGammas/Particles')) == 1)")
    input_sel8 = configB2ggSelection('B2GammaGammaFromEta2ggCharm',wide,cuts,[eta2gg_gammas,d2etapPi_gammas],mother_cut=" & (NINTREE(INTES('/Event/Phys/Eta2ggGammas/Particles')) == 1) & (NINTREE(INTES('/Event/Phys/D2EtapPiGammas/Particles')) == 1)")
    input_sel9 = configB2ggSelection('B2GammaGammaFromPi0Charm',wide,cuts,[pi02gg_gammas,d2etapPi_gammas],mother_cut=" & (NINTREE(INTES('/Event/Phys/Pi02ggGammas/Particles')) == 1) & (NINTREE(INTES('/Event/Phys/D2EtapPiGammas/Particles')) == 1)")
    
    ## get/create application manager
    gaudi = appMgr()                  
    ## (1) create the algorithm with the given name
    algs = {}
    algs["B2GammaGammaFromEta"]=  B2ggFromEta(input_sel)
    algs["B2GammaGammaFromEta2gg"]=  B2ggFromEta2gg(input_sel5)
    algs["B2GammaGammaFromPi02gg"]=  B2ggFromPi0(input_sel6)
    algs["B2GammaGammaFromEtaCharm"]=  B2ggFromEtaCharm(input_sel7)
    algs["B2GammaGammaFromEta2ggCharm"]=  B2ggFromEta2ggCharm(input_sel8)
    algs["B2GammaGammaFromPi02ggCharm"]=  B2ggFromPi0Charm(input_sel9)
    
            
    return SUCCESS

#=============================================================================

#=============================================================================
#Job steering
if __name__ == '__main__' :
    
    root = "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/LDST/00083521/0000/"
    files = map(lambda x: root + "00083521_0000108{}_5.ldst".format(x),range(0,5)+[8])
    files = ["root://eoslhcb.cern.ch//eos/lhcb/user/a/acasaisv/ALPs/5GeV_GenProdCuts_md_2016/MC2016_Priv_MagDown_ALP5_35.ldst"]
    from os import listdir
    from os.path import isfile, join
    mypath = '/scratch17/adrian.casais/dsts/ALPs'
    #files = [join(mypath,f) for f in listdir(mypath) if isfile(join(mypath, f))]
    #files= ['/scratch03/adrian.casais/diphotonanalysis/tupleProduction/Brunel.dst']
    #files = ["LFN:/lhcb/LHCb/Collision18/FULLTURBO.DST/00075638/0000/00075638_00002785_2.fullturbo.dst"]
    from files import list_of_files
    configure(inputdata = list_of_files, catalogs = ['pool_xml_catalog.xml'], castor = False, params = {'Year':2018,"Simulation":False})


    #run(1000) 


    import GaudiPython

    gaudi = GaudiPython.AppMgr()
    TES = gaudi.evtsvc()
    gaudi.initialize()
    gaudi.run(-1)
    gaudi.stop()
    gaudi.finalize()
    
#=============================================================================
#The END
#=============================================================================



# size = 1000
# counter = 0
# m = []
# for i in range(size):
#     gaudi.run(1)
    
#     gammas = TES['Phys/EtaGammas/Particles']
#     line = TES['Phys/JPsi2MuMu_Sel/Particles']
#     line = TES['Phys/Charm2JpsiGamma_Sel/Particles']
#     line = TES['Phys/DW3_Sel/Particles']
#     line = TES['Phys/Eta2ggGammas/Particles']
#     line = TES['Phys/B2GammaGammaFromEta2gg_none/Particles']
#     line = TES['/Event/Phys/Eta2MuMuGamma_Sel/Particles']
#     line = TES['Phys/B2GammaGammaFromEta2ggCharm_none/Particles']
#     #d_gammas = TES['Phys/D2EtapPiGammas/Particles']
#     #line = TES['Phys/B2GammaGammaFromEta_none/Particles']
#     #line = TES['Phys/EtaGammas/Particles']
#     #line = TES['Phys/PhotonFilterBs2gg/Particles']
#     #line = TES['Phys/Bs2gg_none/Particles']
#     #line = TES['Phys/PhotonFilterBs2gg/Particles']
#     if line and line.size()>0:
#     #if (gammas and gammas.size()) and (d_gammas and d_gammas.size()):
#     #if gammas and gammas.size>0:
#     #if line and line.size()>0 and gammas and gammas.size()>0:
#         #continue
#         break
#         m.append(line[0].momentum().M())
#         print len(m)
#         if len(m) > 100:
#             break
#        #continue
#        #break
#        #print (line[0].momentum()+line[1].momentum()).mass()
         
        
# from ROOT import *
# f = TFile('tuple.root')
# t = f.Get('B2GammaGammaFromPi0CharmTuple/DecayTree')

# for ev in t:
#     print t.gamma_eta0_P + t.gamma_eta1_P - t.gamma0_P - t.gamma_P
#     print t.B_s0_M
# t = f.Get('B2GammaGammaFromCharmTuple/DecayTree')
# for ev in t:
#     _0 = t.gamma0_PT - t.gamma_etap_PT
#     _1 = t.gamma1_PT - t.gamma_etap_PT
#     cond = (_0==0 or _1==0)
#     _20 = t.gamma0_PT - t.gamma_pi00_PT
#     _21 = t.gamma0_PT - t.gamma_pi01_PT

#     _30 = t.gamma1_PT - t.gamma_pi00_PT
#     _31 = t.gamma1_PT - t.gamma_pi01_PT

#     cond2 = (_20 ==0 or _21 ==0 or _30 ==0 or _31 ==0)
#     cond = cond and cond2
#     if not cond: print '0'
#     if (_0==0 and (_20==0 or _21==0)): print "1"
#     if (_1==0 and (_30==0 or _31==0)): print "2"
#     print '----------------'
    #print t.B_s0_M

# # # print 1.*counter/size
