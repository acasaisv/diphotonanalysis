## STRIPPING
##########################################################################
from Gaudi.Configuration import *
from Configurables import *
from StrippingConf.Configuration import StrippingConf

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

from DecayTreeTuple.Configuration import *

# Specify the name of your configuration
confname='Bs2GammaGamma' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder

streams = buildStreamsFromBuilder(confs,confname)

line = "Bs2GammaGamma_NoConvLine"
custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line

for stream in streams:
    for sline in stream.lines:
        if sline.name() == custom_line:
            custom_stream.appendLines([sline])
            
from Configurables import ProcStatusCheck

filterBadEvents=ProcStatusCheck()
ReStripping = StrippingConf(Streams=[custom_stream],
                            MaxCandidates=2000,
                            AcceptBadEvents=False,
                            BadEventSelection=filterBadEvents)

# import dvSettings
# dvSettings.StrippingSelections =  ReStripping.selections()

DaVinci().appendToMainSequence( [ ReStripping.sequence() ] )
DaVinci().ProductionType = "Stripping"
