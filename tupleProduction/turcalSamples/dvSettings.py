######################################################################
def parser( argv, test, analysis, decayMode, optionMCDecay, dataType, magnetPolarity, fileType, dddb, conddb, strippingProduction ) :

    if ( len( argv ) > 2 ) :
        print
        print "######################################################################"
        for i in range( len( argv ) ) :
            argvtmp = argv[i].replace( "--option=", "" )
            argvtmp = argvtmp.replace( '"', "" )
            print i, argvtmp
            if "TEST" in argvtmp :
                test=True
            if "=1" in argvtmp and not "=10" in argvtmp  :
                analysis = argvtmp.replace( "=1", "" )
            if "=2" in argvtmp :
                decayMode = argvtmp.replace( "=2", "" )
            if "=3" in argvtmp :
                optionMCDecay = argvtmp.replace( "=3", "" )
            if "=4" in argvtmp :
                dataType = argvtmp.replace( "=4", "" )
            if "=5" in argvtmp :
                magnetPolarity = argvtmp.replace( "=5", "" )
            if "=6" in argvtmp :
                fileType = argvtmp.replace( "=6", "" )
            if "=7" in argvtmp :
                dddb = argvtmp.replace( "=7", "" )
            if "=8" in argvtmp :
                conddb = argvtmp.replace( "=8", "" )
            if "=9" in argvtmp :
                strippingProduction = argvtmp.replace( "=9", "" )
        print "######################################################################"

    return test, analysis, decayMode, optionMCDecay, dataType, magnetPolarity, fileType, dddb, conddb, strippingProduction
######################################################################
def init( test, analysis, decayMode, optionMCDecay, dataType, magnetPolarity, fileType, dddb, conddb, strippingProduction, strippingVersion, strippingStream, strippingModule, strippingLine, strippingRestrip ) :

    global Test, Analysis, DecayMode, OptionMCDecay, DataType, DataYear, MagnetPolarity, FileType, DDDB, CondDB, StrippingProduction, StrippingVersion, StrippingStream, StrippingModule, StrippingLine, StrippingRestrip , StrippingSelections

    Test           = test
    Analysis       = analysis
    DecayMode      = decayMode
    OptionMCDecay  = optionMCDecay
    DataType       = dataType[0:2]
    DataYear       = dataType[2:4]
    MagnetPolarity = magnetPolarity
    FileType       = fileType
    DDDB           = dddb
    CondDB         = conddb
    StrippingProduction      = strippingProduction

    StrippingVersion = strippingVersion
    StrippingStream  = strippingStream
    StrippingModule  = strippingModule
    StrippingLine    = strippingLine
    StrippingRestrip = strippingRestrip
    #If re-run Stripping, the set of selections applied are globbally stored, so they are passed after to the MCDecayTuple Maker
    StrippingSelections = []
    
    if "MODE1" in Analysis :
        if "Bd" in Analysis :
            Analysis = "Bd2KstMM"
        elif "Bu2KPiPi" in Analysis :
            Analysis = "Bu2KPiPiMM"
        elif "Bu" in Analysis :
            Analysis = "Bu2KMM"
        elif "Bs" in Analysis :
            Analysis = "Bs2PhiMM"
    elif "MODE2" in Analysis :
        if "Bd" in Analysis :
            Analysis = "Bd2KstEE"
        elif "Bu2KPiPi" in Analysis :
            Analysis = "Bu2KPiPiEE"
        elif "Bu" in Analysis :
            Analysis = "Bu2KEE"
        elif "Bs" in Analysis :
            Analysis = "Bs2PhiEE"
    elif "MODE3" in Analysis :
        if "Bd" in Analysis :
            Analysis = "Bd2KstME"
        elif "Bu2KPiPi" in Analysis :
            Analysis = "Bu2KPiPiME"
        elif "Bu" in Analysis :
            Analysis = "Bu2KME"
        elif "Bs" in Analysis :
            Analysis = "Bs2PhiME"
    elif "MODE4" in Analysis :
        if "Bd" in Analysis :
            Analysis = "Bd2KstLL"
        elif "Bu2KPiPi" in Analysis :
            Analysis = "Bu2KPiPiLL"
        elif "Bu" in Analysis :
            Analysis = "Bu2KLL"
        elif "Bs" in Analysis :
            Analysis = "Bs2PhiLL"

    if "DECAY" in OptionMCDecay :
        if "EE" in Analysis :
            OptionMCDecay = "EE"
        if "MM" in Analysis :
            OptionMCDecay = "MM"

    if StrippingStream == "STREAM" :
        StrippingStream = "Leptonic"

    if StrippingModule == "MODULE" :
        StrippingModule = "Bu2LLK"

    if "LINE" in StrippingLine :
        if "MM" in Analysis :
            StrippingLine = "Bu2LLK_mmLine"
        if "EE" in Analysis :
            StrippingLine = "Bu2LLK_eeLine2"
        if "ME" in Analysis :
            StrippingLine = "Bu2LLK_meLine"

    if DataYear == "11" :
        StrippingVersion = "stripping21r1p1"
    if DataYear == "12" :
        StrippingVersion = "stripping21r0p1"
    if DataYear == "15" :
        StrippingVersion = "stripping24r1p1"
    if DataYear == "16" :
        StrippingVersion = "stripping28r1p1"
    if DataYear == "17" :
        StrippingVersion = "stripping29r2"
    if DataYear == "18" :
        StrippingVersion = "stripping34"

    if FileType == "FDST" or FileType == "FMDST" :
        StrippingStream = StrippingModule + "_NoPID.Strip"
        StrippingLine   = StrippingLine.replace( StrippingModule, StrippingModule + "NoPID" )

    if FileType == "FDSTLb" :
        StrippingModule = "Bu2KLL"
        StrippingStream = StrippingModule + "_NoPID.Strip"

    if FileType == "FDSTLep" :
        StrippingModule = "Leptonic"
        StrippingStream = StrippingModule + ".Strip"

    if "FDST" in FileType or "MDST" in FileType :
        StrippingRestrip = False

    if "MC" not in DataType :
        StrippingRestrip = False

    print
    print "######################################################################"
    if Test :
        print "##############################   TEST   ##############################"
        print "######################################################################"
    print "Analysis            =", Analysis
    print "DecayMode           =", DecayMode
    print "OptionMCDecay       =", OptionMCDecay
    print "DataType            =", DataType
    print "DataYear            =", DataYear
    print "MagnetPolarity      =", MagnetPolarity
    print "FileType            =", FileType
    print "DDDB                =", DDDB
    print "CondDB              =", CondDB
    print "StrippingProduction =", StrippingProduction
    print "StrippingRestrip    =", StrippingRestrip
    print "StrippingVersion    =", StrippingVersion
    print "StrippingStream     =", StrippingStream
    print "StrippingModule     =", StrippingModule
    print "StrippingLine       =", StrippingLine
    if StrippingRestrip :
        print "######################################################################"
        print "############################   RE-STRIP   ############################"
    print "######################################################################"
    print
######################################################################