
from Gaudi.Configuration import *
from Configurables       import CombineParticles, FilterDesktop,DaVinci
from PhysSelPython.Wrappers import Selection,DataOnDemand,AutomaticData, SelectionSequence
from CommonParticles.Utils import *
from CommonParticles import StdLoosePions, StdLooseKaons, StdAllLooseMuons
from GaudiKernel.PhysicalConstants import c_light

locations={}
## KAONS
## INCLUDES AN AND OF TAU23MU, D2HMUMU AND D2MUMU3PI SELECTIONS!

name_kaon = "KaonFilter"
npkaonSel = FilterDesktop( name_kaon,
                           Inputs = ["Phys/StdLooseKaons/Particles"],
                           Code = ' ( PT > 300 * MeV ) & ( TRGHOSTPROB < 0.45 ) '\
                           ' & ( P > 2000 * MeV ) '\
                           ' & ( TRCHI2DOF < 3 ) & ( BPVIPCHI2()  > 9 ) '\
                           ' & (PIDK > 0)' )
npkaon = "Phys/"+name_kaon+"/Particles"
locations.update(updateDoD ( npkaonSel ))

## PIONS
name_pion = "PionFilter"
nppionSel = FilterDesktop( name_pion,
                           Inputs = ["Phys/StdLoosePions/Particles"],
                           Code = ' ( PT > 300 * MeV ) & ( TRGHOSTPROB < 0.45 ) '\
                           ' & ( P > 2000 * MeV ) '\
                           ' & ( TRCHI2DOF < 3 ) & ( BPVIPCHI2()  > 9 ) ')
nppion = "Phys/"+name_pion+"/Particles"
locations.update(updateDoD ( nppionSel ))

## MUONS (filter muons from W)
name_muon = "MuonFilter"
npmuonSel = FilterDesktop( name_muon,
                           Inputs = ["Phys/StdLooseMuons/Particles"],
                           Code = ' ( PT > 20000 * MeV ) ')
npmuon = "Phys/"+name_muon+"/Particles"
locations.update(updateDoD ( npmuonSel ))

## PHOTONS
name_photon = "PhotonFilter"
npphotonSel = FilterDesktop( name_photon,
                           Inputs = ["Phys/StdLooseAllPhotons/Particles"],
                           Code = ' (PT > 500*MeV) & (CL > 0.3) ')
npphoton = "Phys/"+name_photon+"/Particles"


locations.update(updateDoD ( npphotonSel ))


#D decays
_dcut   = "( VFASPF(VCHI2/VDOF) < 5 ) & "\
          "( PT > 300 * MeV ) & ( BPVVDCHI2 > 10 ) & "\
          "( ( BPVLTIME() * c_light ) > 100 * micrometer ) & "\
          "( BPVIPCHI2()< 10 ) &"\
          "( BPVDIRA > 0.9999 )"
_dcutcomb = "( ADAMASS('D0') < 100 *MeV) & ( AMAXDOCA('') < 0.15 * mm)"

#Algorithms
name01 = 'D2Kpi'
D2Kpi1 = CombineParticles (name01)
D2Kpi1.Inputs = [ nppion, npkaon ]
D2Kpi1.DecayDescriptor = "[D0 -> K- pi+]cc"
D2Kpi1.CombinationCut = _dcutcomb
D2Kpi1.MotherCut = _dcut
d1_output0 = "Phys/"+name01+"/Particles"
locations.update(updateDoD ( D2Kpi1 ))

name02 = 'D2K3pi'
D2Kpi2 = CombineParticles (name02)
D2Kpi2.Inputs = [ nppion, npkaon ]
D2Kpi2.DecayDescriptor = "[D0 -> K- pi+ pi- pi+]cc"
D2Kpi2.CombinationCut = _dcutcomb
D2Kpi2.MotherCut = _dcut
d2_output0 = "Phys/"+name02+"/Particles"
locations.update(updateDoD ( D2Kpi2 ))

name03 = 'D2Kpipi'
D2Kpi3 = CombineParticles (name03)
D2Kpi3.Inputs = [ nppion, npkaon ]
D2Kpi3.DecayDescriptor = "[D+ -> K- pi+ pi+]cc"
D2Kpi3.CombinationCut = "(ADAMASS('D+') < 100 *MeV) "
D2Kpi3.MotherCut = _dcut
d3_output0 = "Phys/"+name03+"/Particles"

locations.update(updateDoD ( D2Kpi3 ))

from Configurables import LoKi__HDRFilter
filterHlt2_eta2mumug     = LoKi__HDRFilter( 'MyHlt2Filter_Eta2MuMuGamma', Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision')", Location="Hlt2/DecReports" )

evtPreselectors = []
#evtPreselectors.append(filterHlt2_eta2mumug)

name1 = 'DW1'
DW1 = CombineParticles (name1)
#DW1.Inputs = [ npphoton, d1_output0 ]
DW1.DecayDescriptors = ["D*(2640)0 -> D0 gamma","D*(2640)~0 -> D~0 gamma"]
DW1.CombinationCut = '(AM > 2500*MeV) & (AM < 2700*MeV)'
DW1.MotherCut = "ALL"

DW1_Sel = Selection(name1 + "_Sel" , Algorithm=DW1 ,RequiredSelections=[AutomaticData(npphoton),AutomaticData(d1_output0)])
DW1_Seq = SelectionSequence(name1 + "_Seq", TopSelection=DW1_Sel, EventPreSelector = evtPreselectors)
DaVinci().UserAlgorithms += [DW1_Seq]

name3 = 'DW3'
DW3 = CombineParticles (name3)
DW3.Inputs = [ npphoton, d3_output0 ]
DW3.DecayDescriptors = ["D*(2640)+ -> D+ gamma","D*(2640)- -> D- gamma"]
DW3.CombinationCut = '(AM > 2500*MeV) & (AM < 2700*MeV)'
DW3.MotherCut = "ALL"

DW3_Sel = Selection(name3 + "_Sel" , Algorithm=DW3 ,RequiredSelections=[AutomaticData(npphoton),AutomaticData(d3_output0)])
DW3_Seq = SelectionSequence(name3 + "_Seq", TopSelection=DW3_Sel, EventPreSelector = evtPreselectors)
DaVinci().UserAlgorithms += [DW3_Seq]

#locations.update(updateDoD ( DW3 ))


# mylocations = {name1:d1_output,
#                #name2:d2_output,
#                name3:d3_output}

## ============================================================================
if '__main__' == __name__ :
    #print __doc__
    #print __author__
    #print __version__
    print locationsDoD ( locations )

