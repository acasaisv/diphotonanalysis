# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
for pol in ['MagDown']:
 j = Job()
 root = '/home3/adrian.casais/cmtuser/'
 j.application = BenderModule(
                    module = "./BenderALPsTuple.py",
                    directory = root + "/BenderDev_v33r3",
                    platform = "x86_64+avx2+fma-centos7-gcc9-opt",
                    #events = 1000,
                    )

 
 j.name = "bender-MBPriv2016-"+pol
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 50 , ignoremissing=True)
 
 j.backend = Dirac()
 j.application.params = {"Year":2016,"Polarity":pol,"Simulation":True,"DDDBTag":"dddb-20170721-3","CondDBtag":"sim-20170721-2-vc-md100"}
 j.inputfiles = [root + "/makeALPs.py",'./caloPostCalib.py']
 j.outputfiles = [DiracFile('*.root')]
 
 #bkk = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/300000{}/ALLSTREAMS.DST"
 #bkk_data = "/LHCb/Collision16/Beam6500GeV-VeloClosed-{}/Real Data/Reco16/96000000/FULL.DST".format(pol)
 
 #j.inputdata = BKQuery(bkk_data).getDataset()
 
 lfns =['LFN:/lhcb/grid/wg/RD/K0S2mu2/30000000/MC2016_Priv_md_30000000_{}.ldst'.format(i) for i in range(189)]
 input = LHCbDataset()
 for lfn in lfns: input.append(DiracFile(lfn))
 j.inputdata = input
 
 j.submit()
 
