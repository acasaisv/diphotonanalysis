for job in jobs:
    for sj in job.subjobs.select(status = 'submitted'):
        if sj.backend.actualCE == "LCG.Beijing.cn":
            sj.kill()
            j = sj.copy(unprepare = True)
            j.backend.settings["BannedSites"] = "LCG.Beijing.cn"
            j.splitter = SplitByFiles(filesPerJob = 1)
            j.comment = "Split of job %i, subjob %i" % (job.id, sj.id)
            j.submit()
