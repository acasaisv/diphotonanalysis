#-- GAUDI jobOptions generated on Tue Apr 26 11:28:06 2022
#-- Contains event types : 
#--   90000000 - 50 files - 2095897 events - 182.23 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco18/Stripping34r0p1' 

#--  StepId : 138835 
#--  StepName : Stripping34r0p1-Merging-DV-v44r10p2-AppConfig-v3r382-LZMA4-Compression 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v44r10p2 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20190206-3 
#--  CONDDB : cond-20180202 
#--  ExtraPackages : AppConfig.v3r382;Det/SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000390_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000097_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000336_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000118_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000373_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000252_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000085_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000440_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000060_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000287_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000087_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000343_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000382_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000216_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000067_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000076_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000229_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000150_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000004_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000575_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000023_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000265_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000381_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000189_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000020_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000043_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000359_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000163_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000313_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000204_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000072_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000323_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000142_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000117_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000156_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000235_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000221_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000049_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000158_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000298_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000062_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000326_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000030_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000103_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000324_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000328_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000008_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000322_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000121_1.bhadroncompleteevent.dst',
'LFN:/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00092561/0000/00092561_00000213_1.bhadroncompleteevent.dst',
], clear=True)
