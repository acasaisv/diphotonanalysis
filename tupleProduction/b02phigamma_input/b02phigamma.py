from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, LoKi__VertexFitter, LoKi__FastVertexFitter, TupleToolTISTOS,TupleToolCaloHypo
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons, StdLoosePhotons, StdAllLooseMuons, StdAllLoosePions
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence, RebuildSelection
import os
import sys

#index = os.environ['GRINDEX']
#initial DV confg
DaVinci().InputType = 'DST' # LDST if Upgrade, DST if Run 2
#DaVinci().TupleFile = '/scratch04/adrian.casais/ntuples/eta2mmg{}.root'.format(index)
DaVinci().TupleFile = 'b02phigamma.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
#DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
#DaVinci().RootInTES = "/Event/BEAUTY2XGAMMA.STRIP"
#DaVinci().RootInTES = "/Event/AllStreams"




dtt = DecayTreeTuple('DecayTree')
#dtt.Decay ='[D+ -> ^(eta_prime -> ^pi+ ^pi- ^gamma) ^pi+]CC'
dtt.Decay = '[B_s0 -> ^(phi(1020) -> ^K+ ^K-) ^gamma]CC'
dtt.addBranches({
    "B"      : "[B_s0 -> (phi(1020) -> K+ K-) gamma]CC",
    "gamma" : "[B_s0 -> (phi(1020) -> K+ K-) ^gamma]CC",
    "phi"  : "[B_s0 -> ^(phi(1020) -> K+ K-) gamma]CC",
    "kplus"  : "[B_s0 -> (phi(1020) -> ^K+ K-) gamma]CC",
    "kminus"  : "[B_s0 -> (phi(1020) -> K+ ^K-) gamma]CC",

        })

if DaVinci().getProp('Simulation'):

    dtt.Inputs = [
        '/Event/Beauty2XGamma.Strip/Phys/Beauty2XGammaExclTDCPVBs2PhiGammaLine/Particles',
        ]
else:
    dtt.Inputs = ['/Event/BhadronCompleteEvent/Phys/Beauty2XGammaExclTDCPVBs2PhiGammaLine/Particles',
                  '/Event/BhadronCompleteEvent/Phys/Beauty2XGammaExclusiveBs2PhiGammaLine/Particles']

# TupleToolProtoPData configuration
if DaVinci().getProp('Simulation'):
    mc_truth = dtt.addTupleTool("TupleToolMCTruth")
    mc_truth.addTupleTool("MCTupleToolHierarchy")

    # TupleToolMCBackgroundInfo -> BKGCat via Relations does not work with radiative (?)
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    if DaVinci().InputType == "DST" or DaVinci().InputType == "LDST": # doesnt work with mDST
        dtt.TupleToolMCBackgroundInfo.IBackgroundCategoryTypes = ["BackgroundCategory"]


dtt.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")        
dtt.LoKi_All.Variables =  {
    'PT'    : 'PT',
    'P'    : 'P',
    'ETA':'ETA',
    'PHI':'PHI'
            }
dtt.addTupleTool("LoKi::Hybrid::MCTupleTool/MCLoKi_All")        
dtt.MCLoKi_All.Variables =  {
    'TRUEETA'    : 'MCETA',
    'TRUEPHI'    : 'MCPHI',
    'TRUEID': 'MCID'
    #'isPhoton' : 'ISPHOTON'
            }
triglist = [
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0ElectronDecision",
    "L0PhotonDecision",
    "Hlt1B2GammaGammaDecision",
    "Hlt1B2GammaGammaHighMassDecision",
    'Hlt1MBNoBiasDecision',
    'Hlt1B2PhiGamma_LTUNBDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    "Hlt2RadiativeB2GammaGammaDecision",
    'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision',
    'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision',
    'Hlt2CaloPIDBd2KstGammaTurboCalibDecision',
    'Hlt2RadiativeBs2PhiGammaUnbiasedDecision',
    'Hlt2RadiativeBd2KstGammaULUnbiasedDecision',
    'Hlt2RadiativeBd2KstGammaDecision',
    'Hlt2RadiativeBs2PhiGammaDecision',
    'Hlt2RadiativeIncHHGammaDecision',
    'Hlt2Topo2BodyDecision',
    ]

TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0   = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]
dtt.addTupleTool(TISTOS)
dtt.ToolList += ['TupleToolPhotonInfo',
                 #'TupleToolCaloHypo',
                 'TupleToolEventInfo',"TupleToolRecoStats",'TupleToolTrackInfo']

# dtt.addTupleTool( 'TupleToolL0Calo', name = "gammaL0ECalo" )
# dtt.gammaL0ECalo.WhichCalo = "ECAL"
# dtt.gammaL0ECalo.TriggerClusterLocation = "/Event/Trig/L0/FullCalo"
# dtt.ToolList += [ "TupleToolL0Calo/gammaL0ECalo" ]

dtt.addTupleTool("TupleToolProtoPData")
dtt.TupleToolProtoPData.DataList = ["IsPhoton",
        "IsNotE",
        "IsNotH",
        'Saturation',
        'IsPhotonXGB',
        'ClusterAsX',
        'ClusterAsY',
        'CaloClusterCode',
        'CaloClusterFrac',
        'CaloShapeKappa',
        'CaloShapeAsym',
        'CaloShapeE1',
        'CaloShapeE2',
        'CaloPrsShapeE1',
        'CaloPrsShapeE2',
        'CaloPrsShapeEmax',
        'CaloPrsShapeFr2',
        'CaloPrsShapeAsym',
        'CaloPrsM',
        'CaloPrsM15',
        'CaloPrsM30',
        'CaloPrsM45',
        'CaloTrMatch',
        'CaloNeutralID',
        'CaloTrajectoryL',
        'CaloNeutralE19',
        'CaloPrsNeutralE19',
        'CaloPrsNeutralE49',
        'CaloNeutralPrsM',
        'CaloShapeFr2r4',
        'CaloPrsNeutralE4max',
        'CaloNeutralHcal2Ecal',
        'CaloNeutralEcal',
        'CaloNeutralPrs',
        'CaloNeutralSpd',
        'ClusterMass',
        'ShowerShape',
        'CaloDepositID',
        'CaloChargedID'
]
NeutralsHypo = TupleToolCaloHypo("NeutralsHypo")
NeutralsHypo.DataList=['Saturation']
#dtt.gamma.addTupleTool(NeutralsHypo)
dtt.addTupleTool('TupleToolL0Data')
dtt.TupleToolL0Data.DataList = ['*Et*','*Pt*']
from Configurables import TupleToolConeIsolation
ConeIsol = TupleToolConeIsolation('ConeIsol',
                                  FillComponents=True,
                                  MinConeSize=1.0,
                                  SizeStep=0.35,
                                  MaxConeSize=2.05,
                                  Verbose=True,
                                  FillPi0Info=False,
                                  FillMergedPi0Info=False,
                                  FillAsymmetry=True,
                                  FillDeltas= False,
                                  ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
                                  MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
                                  )
ConeIsol.Verbose=True
ConeIsol.OutputLevel=3.
dtt.B.addTupleTool(ConeIsol)
dtt.gamma.addTupleTool(ConeIsol)

caloPostCalib = True#
caloRecoChain = False
if caloPostCalib:
    importOptions('./caloPostCalib.py')
    from Configurables import CaloPostCalibAlg
    cpc = CaloPostCalibAlg('CaloPostCalib')
    cpc.Inputs=dtt.Inputs
    DaVinci().UserAlgorithms  += [cpc]
if caloRecoChain:
    #importOptions('./CaloRecoChain.py')
    import sys
    import os
    sys.path.append(os.environ["PWD"])
    import CaloRecoChain
    seq = CaloRecoChain.update(dtt.Inputs,UpdateLevel=5,InsertInMainSeq=True,UpdateNeutralID=False)
    DaVinci().UserAlgorithms += [seq]

DaVinci().UserAlgorithms += [dtt]


# ######################
# importOptions('pool_xml_catalog.py')
# importOptions('files.py')
# import GaudiPython

# gaudi = GaudiPython.AppMgr()
# TES = gaudi.evtsvc()
# gaudi.initialize()
# gaudi.run(1000)
# gamma = TES['/Event/Beauty2XGamma.Strip/Phys/Beauty2XGammaExclTDCPVBs2PhiGammaLine/Particles'][0].daughters()[1]
# gaudi.stop()
# gaudi.finalize()

# from ROOT import *
# f = TFile('b02phigamma.root')
# t = f.Get('DecayTree/DecayTree')
# counter = 0
# for ev in t:
#     if ev.gamma_L0Calo_ECAL_TriggerET != -1:
#         counter +=1
#         print 'ECAL_realET',ev.gamma_L0Calo_ECAL_realET
#         print 'gamma_PT',ev.gamma_PT
#         print 'ECAL_TriggerET',ev.gamma_L0Calo_ECAL_TriggerET
#         print '----------------------------'
