from Configurables import DaVinci
DaVinci().DDDBtag  = "dddb-20210528-8"
DaVinci().CondDBtag  = "sim-20201113-8-vc-md100-Sim10"
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().DataType='2018'
