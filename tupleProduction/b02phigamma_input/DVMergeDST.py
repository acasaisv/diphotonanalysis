from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import (DaVinci, LumiAlgsConf)

DaVinci().InputType = 'DST'
ioh = IOHelper()
#ioh.outStream('PFN:$PWD/myfile.dst')
ioh.outputAlgs("PFN:test.dst","InputCopyStream")
