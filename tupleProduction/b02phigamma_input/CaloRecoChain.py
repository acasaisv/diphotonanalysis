"""
Superseds CaloPostCalib.py, using RestoreCaloReco Chain, enables a Refit and a possible ReCalibration
UpdateLevel = 1 by default does PostCalibration
UpdateLevel = 5 does recalibration (LocalTags must be provided)
"""
from Configurables import DaVinci
from Configurables import GaudiSequencer
from Configurables import Gaudi__DataLink as DataLink
from Configurables import TESCheck as DataCheck
from Configurables import RestoreCaloRecoChain
from Configurables import DaVinci
import os, sys

def update(Inputs,UpdateLevel=1,RefitDecayTree=True,UpdateNeutralID=False,Verbose=False,InsertInMainSeq=True) :

    caloSeq=GaudiSequencer('CaloRecoChainSeq',IgnoreFilterPassed=True)
    if Inputs==[]  :
        return caloSeq # return empty sequence

    # == prepare Calo data
    if DaVinci().InputType == 'MDST' :
        # == if data source : persist-calo   => automatically unpacked -
        # == if data source : calo raw banks => reprocess low-level calo-reco explicitely
        from Configurables import CaloProcessor
        calo=CaloProcessor(CaloPIDs=False,RecList=['Clusters','Photons','MergedPi0s','SplitPhotons'])
        DaVinci().EventPreFilters.append(calo.caloSequence())
        path=''
        
    elif DaVinci().InputType == 'DST' :
        # == extract stream name
        str=Inputs[0].replace('/Event/','')
        idx=str.find('/')
        stream=str[0:idx]
        if stream=='' :
            return caloSeq  # stream must be defined for DST format  
        path='/Event/'+stream
        # == if data source : calo raw banks => calo-reco reprocessed onDemand for DST
        # == if data source : persist-calo   => automatically unpacked +  data-link containers to /Event/<stream>

        linkCaloData=GaudiSequencer("LinkCaloChain",IgnoreFilterPassed=True)
        caloData=['Raw/Ecal/Adcs','Raw/Prs/Adcs','Raw/Prs/Digits','Raw/Spd/Digits','Raw/Ecal/Digits','Rec/Calo/EcalClusters','Rec/Calo/EcalSplitClusters']
        for dat in caloData :
            datName=dat.replace('/','_')    
            checkData=DataCheck('Check'+datName,Inputs=[path+'/'+dat],Stop=False)
            linkData=DataLink('Link'+datName,RootInTES='',What=path+'/'+dat,Target='/Event/'+dat)
            seq=GaudiSequencer('DataSeq'+datName)
            seq.Members+=[checkData,linkData]
            linkCaloData.Members+=[seq]
        caloSeq.Members+=[linkCaloData]

    # == RestoreCaloRecoChain
    caloChain=RestoreCaloRecoChain("RestoreCaloRecoChain",TESprefix=path,Verbose=Verbose)
    

    # Local config for 2012 tests
    # ================================= #
    # ==== DATA STRIP 21/24/28/34  ==== #
    # ================================= #
    if not DaVinci().getProp("Simulation") :
        print("  ====  Apply photon post-calibration to Stripping 21/24/28/29/34 data samples"  )  
        if DaVinci().getProp("DataType") == '2011' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2011 data configuration !!! Apply SpdScaling === ")
            caloChain.SpdScale = True
            caloChain.Calib[0]   = -1.00
            caloChain.Calib[1]   = +0.40
            caloChain.Calib[2]   = +2.90
            caloChain.CalibCNV[0]= +0.80
            caloChain.CalibCNV[1]= +0.30
            caloChain.CalibCNV[2]= +7.80
        elif DaVinci().getProp("DataType") == '2012' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2012 data configuration !!! Apply SpdScaling === ")
            caloChain.SpdScale = True
            caloChain.Calib[0]   = -1.60
            caloChain.Calib[1]   = -2.00
            caloChain.Calib[2]   = -2.10
            caloChain.CalibCNV[0]= -0.40
            caloChain.CalibCNV[1]= -3.50
            caloChain.CalibCNV[2]= +0.40
        elif DaVinci().getProp("DataType") == '2015' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2015 (S24) data configuration !!! === ") # no SPD scaling
            pol_fac=1.0
            polarity=os.getenv('POLARITY','unset')
            if  polarity == 'down':
                pol_fac=1.0065
                print("   == polarity factor (MagDown) : ", pol_fac)
            elif  polarity == 'up':
                pol_fac=0.9925
                print("  == polarity factor (MagUp) : ", pol_fac)
            else:
                print("  !!! WARNING :  POLARITY UNSET : 2015 (S24) is up/down assymmetric !! You should update your setting !!!")
            caloChain.Calib[0]   = 1.010*pol_fac
            caloChain.Calib[1]   = 1.016*pol_fac
            caloChain.Calib[2]   = 1.004*pol_fac
            caloChain.CalibCNV[0]= 1.016*pol_fac
            caloChain.CalibCNV[1]= 1.009*pol_fac
            caloChain.CalibCNV[2]= 1.012*pol_fac
            
        elif DaVinci().getProp("DataType") == '2016' :
            os.environ['POSTCALIBRATION']="1"
            updateCalo =os.getenv('updateCalo','0')
            if os.environ['STRIPPINGVERSION']=="stripping28r2" or updateCalo == '1' :
                print("  ==== 2016 (s28) data configuration WITH OFFLINE RECALIBRATION (calo-20170505) !!! === ") 
                #from Configurables import CondDB 
                # == offline recalibration (calo-20170505)
                caloChain.Calib[0]   = 1.019 * 0.988
                caloChain.Calib[1]   = 1.015 * 0.990
                caloChain.Calib[2]   = 0.999 * 0.993
                caloChain.CalibCNV[0]= 1.027 * 0.985
                caloChain.CalibCNV[1]= 1.010 * 0.989
                caloChain.CalibCNV[2]= 1.005 * 0.998
            else :
                print("  ==== 2016 (S26/s28) data configuration !!! === ") # no SPD scaling
                caloChain.Calib[0]   = 1.019
                caloChain.Calib[1]   = 1.015
                caloChain.Calib[2]   = 0.999
                caloChain.CalibCNV[0]= 1.027
                caloChain.CalibCNV[1]= 1.010
                caloChain.CalibCNV[2]= 1.005

        elif DaVinci().getProp("DataType") == '2017'  :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2017 (S29) data configuration !!! === ") # no SPD scaling
            caloChain.Calib[0]   = 1.0083 # 1.019*0.9895
            caloChain.Calib[1]   = 1.0117 # 1.015*0.9967
            caloChain.Calib[2]   = 1.0010 # 0.999*1.0020
            caloChain.CalibCNV[0]= 1.0147 # 1.027*0.9880
            caloChain.CalibCNV[1]= 1.0067 # 1.010*0.9967
            caloChain.CalibCNV[2]= 1.0129 # 1.005*1.0079

        elif DaVinci().getProp("DataType") == '2018'  :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2018 (S34) data configuration !!! === ") # no SPD scaling
            caloChain.Calib[0]   = 1.009 
            caloChain.Calib[1]   = 1.005 
            caloChain.Calib[2]   = 0.989 
            caloChain.CalibCNV[0]= 1.015 
            caloChain.CalibCNV[1]= 0.999 
            caloChain.CalibCNV[2]= 1.006 

    # =============================== #
    # ==== MC SIM06/SIM08/SIM09  ==== #
    # =============================== #
    if DaVinci().getProp("Simulation") :

        # ============ #
        # == MC2011 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2011' :
            # MC SIM08 post-calibration :
            if os.getenv('SIMVERSION','none') == 'SIM08' :
                os.environ['POSTCALIBRATION']="1"
                print("  ==== 2011 MC SIM08 configuration !!! === ")
                caloChain.Calib[0]   = 0.9995
                caloChain.Calib[1]   = 1.0075
                caloChain.Calib[2]   = 1.0280
                caloChain.CalibCNV[0]= 0.9815
                caloChain.CalibCNV[1]= 0.9969
                caloChain.CalibCNV[2]= 1.0259
                caloChain.Smear = 0.0230
            else :
                os.environ['POSTCALIBRATION']="1"
                print("  ==== 2011 MC configuration !!! === ")
                # no MC available : use same coef. as for SIM08 MC2011 !!
                caloChain.Calib[0]   = 0.9995
                caloChain.Calib[1]   = 1.0075
                caloChain.Calib[2]   = 1.0280
                caloChain.CalibCNV[0]= 0.9815
                caloChain.CalibCNV[1]= 0.9969
                caloChain.CalibCNV[2]= 1.0259
                caloChain.Smear = 0.0210



        # ============ #
        # == MC2012 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2012' :
            if os.getenv('SIMVERSION','none') == 'SIM08' :
                os.environ['POSTCALIBRATION']="1"
                print("  ==== 2012 MC SIM08 configuration !!! === ")
                caloChain.Calib[0]   = 1.0014
                caloChain.Calib[1]   =1.0065
                caloChain.Calib[2]   =1.028
                caloChain.CalibCNV[0]=0.9813
                caloChain.CalibCNV[1]=0.9970
                caloChain.CalibCNV[2]=1.026
                caloChain.Smear = 0.0130
            else :         # MC SIM06 post-calibration (also valid for SIM09 - TO BE CHECKED?)
                os.environ['POSTCALIBRATION']="1"
                print("  ==== 2012 MC configuration !!! === ")
                caloChain.Calib[0]   =0.9920
                caloChain.Calib[1]   =1.0005
                caloChain.Calib[2]   =1.0208
                caloChain.CalibCNV[0]=0.9730
                caloChain.CalibCNV[1]=0.9910
                caloChain.CalibCNV[2]=1.0214
                caloChain.Smear = 0.0130

        # ============ #
        # == MC2015 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2015' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2015/2016 MC configuration !!! === ") # Sim09a => very close to Sim08 (large coef to be understood)
            caloChain.Calib[0]   = 1.001
            caloChain.Calib[1]   = 1.006
            caloChain.Calib[2]   = 1.027
            caloChain.CalibCNV[0]= 0.979
            caloChain.CalibCNV[1]= 0.994
            caloChain.CalibCNV[2]= 1.023
            caloChain.Smear = 0.0130

        # ============ #
        # == MC2016 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2016' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2015/2016 MC configuration !!! === ")
            caloChain.Calib[0]   = 1.001
            caloChain.Calib[1]   = 1.006
            caloChain.Calib[2]   = 1.027
            caloChain.CalibCNV[0]= 0.979
            caloChain.CalibCNV[1]= 0.994
            caloChain.CalibCNV[2]= 1.023
            caloChain.Smear = 0.0130


        # ============ #
        # == MC2017 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2017' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2017 MC configuration !!! === ")
            caloChain.Calib[0]   = 1.0004
            caloChain.Calib[1]   = 1.0065
            caloChain.Calib[2]   = 1.0260
            caloChain.CalibCNV[0]= 0.9804
            caloChain.CalibCNV[1]= 0.9960
            caloChain.CalibCNV[2]= 1.0241
            caloChain.Smear = 0.0130

        # ============ #
        # == MC2018 == #
        # ============ #
        if DaVinci().getProp("DataType") == '2018' :
            os.environ['POSTCALIBRATION']="1"
            print("  ==== 2018 MC configuration !!! === ")
            caloChain.Calib[0]   = 1.0012
            caloChain.Calib[1]   = 1.0062
            caloChain.Calib[2]   = 1.0265
            caloChain.CalibCNV[0]= 0.9804
            caloChain.CalibCNV[1]= 0.9950
            caloChain.CalibCNV[2]= 1.0234
            caloChain.Smear = 0.0

    caloChain.Inputs=Inputs
    caloChain.UpdateLevel=UpdateLevel
    caloChain.RefitDecayTree=RefitDecayTree
    caloChain.UpdateNeutralID=UpdateNeutralID
    # == set tools
    from Configurables import ToolSvc,CaloGetterTool,CaloHypo2Calo,CaloHypoEstimator
    caloChain.addTool(CaloHypo2Calo,"CaloHypo2Spd")
    caloChain.addTool(CaloHypo2Calo,"CaloHypo2Prs")
    caloChain.addTool(CaloHypoEstimator,"CaloHypoEstimator")
    caloChain.CaloHypo2Spd.GetterName="CaloGetterForPersistReco"
    caloChain.CaloHypo2Prs.GetterName="CaloGetterForPersistReco"
    caloChain.CaloHypoEstimator.SkipNeutralID=True
    caloChain.CaloHypoEstimator.SkipChargedID=True
    caloChain.CaloHypoEstimator.SkipClusterMatch=False
    caloSeq.Members+=[caloChain]
    if InsertInMainSeq :
        DaVinci().mainSeq.Members.insert(0,caloSeq)
    return caloSeq

def updateSeq(Inputs,UpdateLevel=5,RefitDecayTree=True,UpdateNeutralID=False,Verbose=False) :
    seq= update(Inputs,UpdateLevel,RefitDecayTree,UpdateNeutralID,Verbose,InsertInMainSeq=False)
    return seq
