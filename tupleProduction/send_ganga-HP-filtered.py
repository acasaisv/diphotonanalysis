# template = JobTemplate(
#    application = prepareBender(
#     version = 'v35r5',
#     module = "BenderALPsTuple.py",
#     use_tmp = True             ),
#    )
numbers = range(50,53)
numbers = [51,52]
myApp = GaudiExec(directory='/home3/adrian.casais/cmtuser/DaVinciDev_v44r11p1')
options = [
    'ALPsFromStrippingBs2gg.py',
    ]

for n in numbers:
 j = Job()
 root = "/scratch03/adrian.casais/diphotonanalysis/tupleProduction"
 j.application = myApp
 j.application.platform = 'x86_64-centos7-gcc62-opt'
 
 #j.name= "bender-MB"
 
 j.splitter = SplitByFiles ( filesPerJob = 1 , ignoremissing=True)
 j.application.options= options
 j.inputfiles = ["./makeALPs.py",'./caloPostCalib.py']

 j.name = "HP{}-filtered".format(n)
 
 
 j.backend = Dirac()
 j.outputfiles = [LocalFile('*.root')]
 
 bkk = "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/300000{}/STRIP.DST".format(n)

 j.inputdata = BKQuery(bkk.format(n)).getDataset()
 j.submit()
 
