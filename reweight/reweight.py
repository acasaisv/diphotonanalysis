from hep_ml.reweight import GBReweighter
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plot_helpers

def reweight_etammg_mc(df,dfMC,variables=['eta_PT','eta_ETA','eta_P','dimuon_M'],nbins=50):
        
    reweighter = GBReweighter(max_depth=2, n_estimators=100, gb_args={'subsample': 0.5,'random_state':2456})
    reweighter.fit(original=dfMC[variables], original_weight=dfMC['sweights'],target=df[variables], target_weight=df['sweights'])
    plt.clf()
    plt.hist(reweighter.predict_weights(dfMC[variables], original_weight=dfMC['sweights']),bins=nbins)
    plt.xlabel("weights")
    plt.ylabel("Yield")
    plt.savefig("figs_regular/reweight_etammg_weights.pdf")
    plt.savefig("figs_regular/reweight_etammg_weights.png")
    dfMC['ssweights']= dfMC['sweights']
    dfMC['sweights']= reweighter.predict_weights(dfMC[variables],original_weight=dfMC['sweights'])
    return dfMC
def dimuon_mass(df):
    df['dimuon_M2'] = np.zeros(len(df))
    for c in 'X','Y','Z':
        df['dimuon_M2'] -= df.eval(f'(mu1_P{c}+mu2_P{c})**2')
    df['dimuon_M2'] += df.eval(f'(mu1_PE+mu2_PE)**2')
    df['dimuon_M'] = np.sqrt(df['dimuon_M2'])
    return df
def undo_rw(df):
    df['sweights'] = df['ssweights']
    return df
if __name__=='__main__':

    variables = [
                #'eta_PT',
                'eta_ETA',
                'eta_P',
                'dimuon_M']

    root = '/scratch47/adrian.casais/ntuples/turcal/'
    mydf =pd.read_hdf(root+'etammgTurcalData.h5',key='df')
    mydfMC =pd.read_hdf(root+'etammgTurcalHardPhotonMC.h5',key='df')
    mydf = dimuon_mass(mydf)
    mydfMC = dimuon_mass(mydfMC)

    ## need to apply the same selection on data and MC before reweighting!
    nbins=50
    mydf.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS ",inplace=True)
    mydfMC.query("(mu1_L0DiMuonDecision_Dec | mu2_L0MuonDecision_Dec) & gamma_Hlt1Phys_TIS & eta_Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision_TOS ",inplace=True)
    if 'ssweights' in mydfMC.keys():
        undo_rw(mydfMC)
    mydfMC_noreweight = mydfMC.copy()
    mydfMC = reweight_etammg_mc(mydf,mydfMC,variables)

    plt.clf()
    plt.tight_layout()
    nbins=50
    plt.hist(mydf["eta_PT"],bins=nbins,range=(3000,30000),density=True, weights=mydf["sweights"], alpha=0.8, color="blue", label="$\eta\\to\mu\mu\gamma$ data")
    plt.hist(mydfMC_noreweight["eta_PT"],bins=nbins,range=(3000,30000),density=True, alpha=0.5, weights=mydfMC_noreweight["sweights"], color="red",label="$\eta\\to\mu\mu\gamma$ MC")
    plt.hist(mydfMC["eta_PT"],bins=nbins,range=(3000,30000),density=True, alpha=0.5, weights=mydfMC["sweights"], color="black",histtype='step',label="$\eta\\to\mu\mu\gamma$ rew. MC")
    plt.legend()
    plt.xlabel("$\eta p_T$ [MeV]")
    plt.ylabel("Normalised yield [a.u.]")
    plt.savefig("figs_regular/reweight_etammg_etapt.pdf")
    plt.savefig("figs_regular/reweight_etammg_etapt.png")
    plt.clf()
    plt.hist(mydf["eta_P"],bins=nbins,range=(25000,300000),density=True, weights=mydf["sweights"], alpha=0.8, color="blue", label="$\eta\\to\mu\mu\gamma$ data")
    plt.hist(mydfMC_noreweight["eta_P"],bins=nbins,range=(25000,300000),density=True, alpha=0.5, weights=mydfMC_noreweight["sweights"], color="red",label="$\eta\\to\mu\mu\gamma$ MC")
    plt.hist(mydfMC["eta_P"],bins=nbins,range=(25000,300000),density=True, alpha=0.5, weights=mydfMC["sweights"], color="black",histtype='step',label="$\eta\\to\mu\mu\gamma$ rew. MC")
    plt.legend()
    plt.xlabel("$\eta$ $p$ [MeV]")
    plt.ylabel("Normalised yield [a.u.]")
    plt.savefig("figs_regular/reweight_etammg_etap.pdf")
    plt.savefig("figs_regular/reweight_etammg_etap.png")
    plt.clf()
    plt.hist(mydf["eta_ETA"],bins=nbins,range=(1.5,4.7),density=True, weights=mydf["sweights"], alpha=0.8, color="blue",label="$\eta\\to\mu\mu\gamma$ data")
    plt.hist(mydfMC_noreweight["eta_ETA"],bins=nbins,range=(1.5,4.7),density=True, alpha=0.5, weights=mydfMC_noreweight["sweights"], color="red",label="$\eta\\to\mu\mu\gamma$ MC")
    plt.hist(mydfMC["eta_ETA"],bins=nbins,range=(1.5,4.7),density=True, alpha=0.5, weights=mydfMC["sweights"], color="black",histtype='step',label="$\eta\\to\mu\mu\gamma$ rew. MC")
    plt.legend()
    plt.xlabel("$\eta \ \eta$")
    plt.ylabel("Normalised yield [a.u.]")
    plt.savefig("figs_regular/reweight_etammg_etaeta.pdf")
    plt.savefig("figs_regular/reweight_etammg_etaeta.png")
    plt.clf()
    plt.hist(mydf["dimuon_M"],bins=nbins,range=(200,500),density=True, weights=mydf["sweights"], alpha=0.8, color="blue",label="$\eta\\to\mu\mu\gamma$ data")
    plt.hist(mydfMC_noreweight["dimuon_M"],bins=nbins,range=(100,580),density=True, alpha=0.5, weights=mydfMC_noreweight["sweights"], color="red",label="$\eta\\to\mu\mu\gamma$ MC")
    plt.hist(mydfMC["dimuon_M"],bins=nbins,range=(200,500),density=True, alpha=0.5, weights=mydfMC["sweights"], color="black",histtype='step',label="$\eta\\to\mu\mu\gamma$ rew. MC")
    plt.legend()
    plt.xlabel("$m(\mu\mu)$ [MeV]")
    plt.ylabel("Normalised yield [a.u.]")
    plt.savefig("figs_regular/reweight_etammg_dimuonmass.pdf")
    plt.savefig("figs_regular/reweight_etammg_dimuonmass.png")
    plt.clf()
    plt.hist(mydf["nSPDHits"],bins=nbins,range=(0,1000),density=True, weights=mydf["sweights"], alpha=0.8, color="blue",label="$\eta\\to\mu\mu\gamma$ data")
    plt.hist(mydfMC_noreweight["nSPDHits"],bins=nbins,range=(0,1000),density=True, alpha=0.5, weights=mydfMC_noreweight["sweights"], color="red",label="$\eta\\to\mu\mu\gamma$ MC")
    plt.hist(mydfMC["nSPDHits"],bins=nbins,range=(0,1000),density=True, alpha=0.5, weights=mydfMC["sweights"], color="black",histtype='step',label="$\eta\\to\mu\mu\gamma$ rew. MC")
    plt.legend()
    plt.xlabel("nSPDHits")
    plt.ylabel("Normalised yield [a.u.]")
    plt.savefig("figs_regular/reweight_etammg_nspdhits.pdf")
    plt.savefig("figs_regular/reweight_etammg_nspdhits.png")

    mydfMC.to_hdf(root+'etammgTurcalHardPhotonMC.h5',key='df',mode='w')
    mydf.to_hdf(root+'etammgTurcalData.h5',key='df',mode='w')