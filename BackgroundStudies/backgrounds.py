from __future__ import print_function
from particle import PDGID
from ROOT import TFile, TTree, TCanvas, gROOT,kRed,kBlue,kGreen, TH1F, TLegend
from math import sqrt
import pickle
import sys


def getProperty(pid,property):
    output = getattr(PDGID(pid),property)
        
    return output


def getError(p,N):
    q = 1 - p

    return sqrt(p*q/N)

ntup = "/scratch27/adrian.casais/ntuples/DiPhotons/"
ntup_bender = "/scratch27/adrian.casais/ntuples/DiPhotonsBender/"
# f=TFile(ntup+"DiPhoton-JpsiMeson.root","UPDATE")
f=TFile(ntup+"DiPhoton-JpsiMeson.root")
# f=TFile("/scratch47/adrian.casais/ntuples/background/hardPhoton_51-big.root")
f=TFile("/scratch47/adrian.casais/ntuples/background/hardPhoton_52-big.root")
#f=TFile(ntup+"DiPhoton-minbiasPrivate.root","UPDATE")
#f = TFile(ntup_bender+"background/13144001/13144001.root")
# diphotons_bender = "/scratch27/adrian.casais/ntuples/DiPhotonsBender/"
# f=TFile(diphotons_bender+"hardPhoton_55.root","UPDATE")
#f=TFile(ntup+"DiPhoton-JpsiKstar2018-down.root","UPDATE")

#f=TFile("DiPhoton-inclJpsi.root")
#f=TFile("DiPhoton-Bdpipi.root")
#f=TFile("DiPhoton-minbias2017.root")
#f=TFile("DiPhoton-hardPi02017.root","UPDATE")
#f=TFile("Bu_tuple.root")
#f=TFile(sys.argv[1],"UPDATE")
#f=TFile("DiPhoton-minbias2016data-down.root")
root = '/scratch47/adrian.casais/ntuples/background/'
f = TFile(root + 'hardPhoton_52-big.root')
def getTree(f,tree="DTTBs2GammaGamma/DecayTree",switch=False):
    t_=f.Get(tree)
    if not switch:
        t_counters=f.Get("counters/Counters")
    else:
        t_counters=f.Get("Counters/counters")
    # t_.SetBranchStatus("")
    #f = TFile("dummy.root","RECREATE")
    t=t_.CopyTree("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) && B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")
    return t,t_,t_counters

name="B2GammaGammaTuple/DecayTree"
name = "DTTBs2GammaGamma/DecayTree"
switch=False
t,t_,t_counters=getTree(f,tree=name,switch=switch)

################################
#events list

# evt=[]
# for ev in t:
#     evt.append([int(t.runNumber),int(t.eventNumber)])
# with open("events2017.pkl","wb") as file:
#     pickle.dump(evt,file)


# ##TOTAL EFFICIENCIES

##STRIPPING
if t_counters:
    print("Stripping efficiency: ",100.*t_.GetEntries()/t_counters.GetEntries())
else:
    print("No Stripping efficiency as no normalisation known")
##L0
print("L0: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")/t_.GetEntries())
##HLT1
print("HLT1: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"))
##HLT2
print ("HLT2: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) && B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)"))
# ##STRIPPING
# print("Stripping efficiency: ",100.*t_.GetEntries()/t_counters.GetEntries())
# ##L0
# print("L0: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")/t_.GetEntries())
# ##HLT1
# print("HLT1: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"))
# ##HLT2
# print ("HLT2: ",100.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS"))
print("")




#DISTRIBUTION OF MOTHERS GIVEN ONE IS A PI0

#pi0
counter_pi=0
##eta and pi0
counter_etapi=0
##pi0 and nomother:
counter_pinone=0
##pi0 and charm
counter_picharm=0
##pi0 and bottom
counter_pibottom=0
##pi0 and up
counter_pinohf=0
##pi0 and gamma
counter_pigamma=0
##pi0 pi0
counter_pipi_dif=0
counter_pipi_sam=0
##pi0 pi+-
counter_pipip=0

import numpy as np


mothers={};
# for key in ["eta","pi0","nomother","bottom","charm","noHF"]:
#     mothers[key]=0

TRUEID={};
for key in ["isgammagamma","isgammapi0","isgammaother", "ispi0pi0", "ispi0other","isotherother"]:
    TRUEID[key]=0


for ev in t:
    motherid=[getattr(t,"gamma0_MC_MOTHER_ID"),getattr(t,"gamma_MC_MOTHER_ID")]
    trueid=[getattr(t,"gamma0_TRUEID"),getattr(t,"gamma_TRUEID")]
    motherkey=[getattr(t,"gamma0_MC_MOTHER_KEY"),getattr(t,"gamma_MC_MOTHER_KEY")]

    # check TRUEIDs
    if trueid==[22,22]:TRUEID["isgammagamma"]+=1
    elif trueid==[111,111]: TRUEID["ispi0pi0"]+=1
    elif (trueid==[22,111] or trueid==[111,22]): TRUEID["isgammapi0"]+=1
    elif 22 in trueid: TRUEID["isgammaother"]+=1
    elif 111 in trueid: TRUEID["ispi0other"]+=1
    else: TRUEID["isotherother"]+=1

    if motherkey[0]==motherkey[1]:
        print("!!!!!! Found two candidates from same origin: TRUEID's:",trueid,"mother:",motherid,"!!!!!!")

    # check motherids
    if not abs(motherid[0]) in mothers:
        mothers[abs(motherid[0])]=0
    if not abs(motherid[1]) in mothers:
        mothers[abs(motherid[1])]=0
    mothers[abs(motherid[0])]+=1
    mothers[abs(motherid[1])]+=1


    #study if one candidate is from pi0
    if 111 in motherid:
        counter_pi+=1
        
        if 221 in motherid:
            counter_etapi+=1
            
        elif motherid==[111,111]:
        
            if t.gamma_MC_MOTHER_KEY!=t.gamma0_MC_MOTHER_KEY:
                counter_pipi_dif+=1
            else:
                counter_pipi_sam+=1
                
        elif 0 in motherid:
            counter_pinone+=1
            
        elif np.sum(list(map(lambda x: getProperty(x,"has_bottom") ,motherid))):
            counter_pibottom+=1
            
        elif np.sum(list(map(lambda x: getProperty(x,"has_charm"),motherid))):
            counter_picharm+=1
            
        else: 
            counter_pinohf+=1
        

N=1.*t.GetEntries()
# print("Candidate composition:")
# for key in TRUEID:
#     print(key,":",TRUEID[key],TRUEID[key]/N,"+/-",getError(TRUEID[key]/N,N))
# print("\n")

# print("Mother composition:")
# for key in mothers:
#     print(key,":",mothers[key],mothers[key]/(2*N),"+/-",getError(mothers[key]/(2*N),2*N))



print("\nIf one candidate is from pi0:")
        
pi0="pi0 "
print( pi0,counter_pi/N,"pm",getError(counter_pi/N,N))
print(pi0+" pi0 (dif)",counter_pipi_dif/N,"pm",getError(counter_pipi_dif/N,N))
print(pi0+" pi0 (sam)",counter_pipi_sam/N,"pm",getError(counter_pipi_sam/N,N))
print( pi0+" eta",counter_etapi/N,"pm",getError(counter_etapi/N,N))
print (pi0+" 0",counter_pinone/N,"pm",getError(counter_pinone/N,N))
print (pi0+" charm",counter_picharm/N,"pm",getError(counter_picharm/N,N))
print (pi0+" bottom",counter_pibottom/N,"pm",getError(counter_pibottom/N,N))
print (pi0+" NO_HF",counter_pinohf/N,"pm",getError(counter_pinohf/N,N))
print (pi0+" piplusminus",counter_pipip/N,"pm",getError(counter_pipip/N,N))
print (pi0+" gamma",counter_pigamma/N,"pm",getError(counter_pigamma/N,N))




##GRANDMAS
nograndmas=0
no_pi0s=0
noheavyflavour=0
heavyflavour=0
for ev in t:
    if t.gamma0_MC_GD_MOTHER_ID == t.gamma_MC_GD_MOTHER_ID:
        if t.gamma0_MC_GD_MOTHER_KEY == t.gamma_MC_GD_MOTHER_KEY:
            print("!!!!!! Found the same grandmother: TRUEIDs:",t.gamma0_TRUEID,t.gamma_TRUEID, "mother IDs:", t.gamma0_MC_MOTHER_ID,t.gamma_MC_MOTHER_ID, "grandmother:", t.gamma0_MC_GD_MOTHER_ID, t.gamma_MC_GD_MOTHER_ID, "!!!!!!") 

    if t.gamma0_MC_MOTHER_ID == 111:
        no_pi0s +=1.
        if not t.gamma0_MC_GD_MOTHER_ID:
            nograndmas+=1
        elif not  (getProperty(t.gamma0_MC_GD_MOTHER_ID,"has_charm") or getProperty(t.gamma0_MC_GD_MOTHER_ID,"has_bottom")):
            noheavyflavour+=1
        else: 
            heavyflavour+=1
            
    if t.gamma_MC_MOTHER_ID == 111:
        no_pi0s+=1
        if not t.gamma_MC_GD_MOTHER_ID:
            nograndmas+=1
        elif  not (getProperty(t.gamma_MC_GD_MOTHER_ID,"has_charm") or getProperty(t.gamma_MC_GD_MOTHER_ID,"has_bottom")):
            noheavyflavour+=1
        else: 
            heavyflavour+=1

print ("")
print ("Mothers")
print ("-----------------------------------------")
print ("pi0s without mother", nograndmas/no_pi0s,"pm",getError(nograndmas/no_pi0s,no_pi0s))
print ("pi0s not from  heavy flavour", noheavyflavour/no_pi0s,"pm",getError(noheavyflavour/no_pi0s,no_pi0s))
print ("pi0s from  heavy flavour", heavyflavour/no_pi0s,"pm",getError(heavyflavour/no_pi0s,no_pi0s))

def isPi0nm(id,idm):
    if (id==111 and not idm):
        return True

def isPi0NHF(id,idm):
    if (id==111 and not getProperty(idm,"has_charm") and not getProperty(idm,"has_bottom") and idm):
        return True





#categories for the two daughters
pi0nmpi0nm=0
pi0nmpi0nhf=0
pi0nmeta=0
pi0nmnm=0
pi0nhfpi0nhf=0
pi0nhfeta=0
pi0nhfnm=0
etaeta=0
etanm=0
nmnm=0
for ev in t:
    if isPi0nm(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        if isPi0nm(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nmpi0nm+=1
        if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nmpi0nhf+=1
        if t.gamma_MC_MOTHER_ID==221:
            pi0nmeta+=1
        if not t.gamma_MC_MOTHER_ID:
            pi0nmnm+=1
            
    if isPi0nm(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
        # if isPi0nm(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        #     pi0nmpi0nm+=1
        if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
            pi0nmpi0nhf+=1
        if t.gamma0_MC_MOTHER_ID==221:
            pi0nmeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            pi0nmnm+=1


    if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nhfpi0nhf+=1
        if t.gamma_MC_MOTHER_ID==221:
            pi0nhfeta+=1
        if not t.gamma_MC_MOTHER_ID:
            pi0nhfnm+=1

    if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
        # if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        #     pi0nhfpi0nhf+=1
        if t.gamma0_MC_MOTHER_ID==221:
            pi0nhfeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            pi0nhfnm+=1

    if  t.gamma_MC_MOTHER_ID==221:
        if t.gamma0_MC_MOTHER_ID==221:
            etaeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            etanm+=1

    if  t.gamma0_MC_MOTHER_ID==221:
        # if t.gamma_MC_MOTHER_ID==221:
        #     etaeta+=1
        if not t.gamma_MC_MOTHER_ID:
            etanm+=1
    

    if not t.gamma_MC_MOTHER_ID and not t.gamma0_MC_MOTHER_ID:
            nmnm+=1



N = 1.*t.GetEntries()

print ("")
print ("Categories in pairs")
print ("+++++++++++++++++++++++++++++++++++++")
print ("pi0nmpi0nm",pi0nmpi0nm/N,"pm",getError(pi0nmpi0nm/N,N))
print ("pi0nmpi0nhf",pi0nmpi0nhf/N,"pm",getError(pi0nmpi0nhf/N,N))
print ("pi0nmeta",pi0nmeta/N,"pm",getError(pi0nmeta/N,N))
print ("pi0nmnm",pi0nmnm/N,"pm",getError(pi0nmnm/N,N))
print ("pi0nhfpi0nhf",pi0nhfpi0nhf/N,"pm",getError(pi0nhfpi0nhf/N,N))
print ("pi0nhfeta",pi0nhfeta/N,"pm",getError(pi0nhfeta/N,N))
print ("pi0nhfnm",pi0nhfnm/N,"pm",getError(pi0nhfnm/N,N))
print ("etaeta",etaeta/N,"pm",getError(etaeta/N,N))
print ("etanm",etanm/N,"pm",getError(etanm/N,N))
print ("nmnm",nmnm/N,"pm",getError(nmnm/N,N))


totpi0=pi0nmpi0nm/0.01/N+pi0nmpi0nhf/0.01/N+pi0nmeta/0.01/N+pi0nmnm/0.01/N+pi0nhfpi0nhf/0.01/N+pi0nhfeta/0.01/N+pi0nhfnm/0.01/N
print ("")
print ("pi0tot",totpi0)
print ("#######################################################################")
plot=False
if plot:
    gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

    def fillH(h,t):
        for ev in t:
            if t.gamma0_MC_MOTHER_ID==111:
                h.Fill(t_.gamma0_TRUEPT)
            if t.gamma_MC_MOTHER_ID==111:
                h.Fill(t_.gamma_TRUEPT)
        return h


    def makeH(i):
        return TH1F("pt{0}".format(i),"",70,0,15000)

    # f=TFile("DiPhoton-Bdpipi.root")
    fs=[TFile("DiPhoton-minbias2017.root"),
        #TFile("DiPhoton-inclJpsi.root"),
        #TFile("DiPhoton-Bdpipi.root"),
        TFile("DiPhoton-hardPi02017.root")]

    colours=[kRed,kBlue+3,kGreen+3,6]
    hs=[]
    c=TCanvas()
    for i in range(2):
        t,t_,t_counters=getTree(fs[i],tree=name,switch=switch)
        hs.append(makeH(i))
        hs[i].SetLineColor(colours[i])
        fillH(hs[i],t)
    
        if not i:
            hs[i].DrawNormalized()
        else:
            hs[i].DrawNormalized("same")        
    leg=TLegend(.6,.6,.6,.6)
    leg.AddEntry(hs[0],"minbias","L")
    #leg.AddEntry(hs[1],"inclJpsi","L")
    #leg.AddEntry(hs[2],"Bdpipi","L")
    leg.AddEntry(hs[1],"hardPi0","L")
    
    #leg.Draw()
#c.Print("gammapt.pdf]","pdf")

## To note in overleaf: inclJpsi sample has photons with higher pT ! This makes
## sense and validates the sample. Still: why more rate for minbias2017 ?
plot_selection=True

if plot_selection:
    lowbound=8000
    highbound = 12000
    bins = 5
    L0=TH1F("l0","",bins,lowbound,highbound)
    L1=TH1F("l1","",bins,lowbound,highbound)
    L2=TH1F("l2","",bins,lowbound,highbound)

    gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
    t_.Project("l0","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
    #t_.Project("l1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)")
    t_.Project("l1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && B_s0_Hlt1B2GammaGammaHighMassDecision_TOS")
    #t_.Project("l2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) && B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")
    t_.Project("l2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) && B_s0_Hlt1B2GammaGammaHighMassDecision_TOS && B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")

    L0.SetLineColor(kRed)
    L1.SetLineColor(kBlue+3)
    L2.SetLineColor(kGreen+3)
    leg=TLegend(.6,.6,.6,.6)
    leg.AddEntry(L0,"l0","L")
    leg.AddEntry(L1,"hlt1","L")
    leg.AddEntry(L2,"hlt2","L")
    
    L0.DrawNormalized()
    L1.DrawNormalized("SAME")
    L2.DrawNormalized("same")
    leg.Draw()
    

        
        
