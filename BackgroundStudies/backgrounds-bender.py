from __future__ import print_function
from particle import PDGID
from ROOT import TFile, TTree, TCanvas, gROOT,kRed,kBlue,kGreen, TH1F, TLegend
from math import sqrt
import pickle
import sys


def getProperty(pid,property):
    output = getattr(PDGID(pid),property)
    if abs(pid) == 4 and property =='has_charm':
        return True
    if abs(pid) == 5 and property =='has_bottom':
        return True
    
    return output


def getError(p,N):
    q = 1 - p
    try:
        eff=sqrt(p*q/N)
    except:
        eff=-9999
    return eff


diphotons_bender = "/scratch27/adrian.casais/ntuples/DiPhotonsBender/diffIsolation/"
diphotons_bender = '/scratch47/adrian.casais/ntuples/background/'
key = 51
if len(sys.argv)>1:
    key = int(sys.argv[1])

#f=TFile(diphotons_bender+"hardPhoton-noprotos.root".format(key),"UPDATE")
#f=TFile(diphotons_bender+"11144001.root")
#f=TFile(diphotons_bender+"13144001.root")
#f=TFile(diphotons_bender+"inclJpsi.root")
#f=TFile(diphotons_bender+"hardPhoton_51-big.root")
f=TFile(diphotons_bender+"MB2018_MC.root")
#f=TFile(diphotons_bender+"MB2016Priv.root")
#f=TFile(diphotons_bender+"JpsiMeson3.root")
#f=TFile(diphotons_bender+"MB2017.root")
#f=TFile(diphotons_bender+'B2pipi.root')
#f=TFile(diphotons_bender+"JpsiMeson2016.root")
#f=TFile(diphotons_bender+"inclJpsiMeson.root")
#f=TFile(diphotons_bender+"MB.root","UPDATE")

        



#f=TFile(ntup+"DiPhoton-JpsiKstar2018-down.root","UPDATE")

#
root = '/scratch27/adrian.casais/ntuples/DiPhotons/'
#f=TFile(root+"DiPhoton-Bdpipi.root")

#f=TFile(root+"DiPhoton-inclJpsi.root")
#f=TFile(root + "DiPhoton-minbias2017.root")
#f=TFile("DiPhoton-hardPi02017.root","UPDATE")
#f=TFile("Bu_tuple.root")
#f=TFile(sys.argv[1],"UPDATE")
#f=TFile("DiPhoton-minbias2016data-down.root")
cut = "(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)"\
      "& B_s0_Hlt1B2GammaGammaDecision_TOS "\
      "& B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS"
#iso = "B_s0_iso_0.6 > 0.8"
iso = "(gamma_PP_IsNotH >0.9 && gamma0_PP_IsNotH >0.9) && (gamma_PP_IsNotE > 0.7 && gamma0_PP_IsNotE > 0.7)"
isPhoton='(gamma_PP_IsPhoton > 0.6 && gamma0_PP_IsPhoton > 0.6)'
iso='1'
#cut = cut + "&&" + isPhoton + "&&" + iso
def getTree(f,tree="B2GammaGammaTuple/DecayTree",counters="EventTuple/EventTuple"):
    t_=f.Get(tree)
    t_counters=f.Get(counters)
    t=t_.CopyTree(cut)
    return t,t_,t_counters

feraseme = TFile('eraseme.root','RECREATE')
#t,t_,t_counters=getTree(f,tree='DTTBs2GammaGamma/DecayTree',counters='counters/Counters')
t,t_,t_counters=getTree(f)

# with open('MB2016Priv.txt','w') as handle:
#     for ev in t:
#         handle.write("{0} {1}\n".format(t.runNumber,t.eventNumber))

HP_denominators = {50:101898.,
                   51:101052.,
                   52:108762.,
                   53:102707.,
                   54:100333.,
                   55:107288.,
                   56:106732.}

################################
#events list

# evt=[]
# for ev in t:
#     evt.append([int(t.runNumber),int(t.eventNumber)])
# with open("events2017.pkl","wb") as file:
#     pickle.dump(evt,file)



##TOTAL EFFICIENCIES

##STRIPPING
StripEff=t_.GetEntries()/t_counters.GetEntries()
print("Stripping efficiency: ",100.*StripEff,'pm',100.*getError(StripEff,t_counters.GetEntries()))
##L0
L0Eff=1.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")/t_.GetEntries()
print("L0: ",100.*L0Eff,'pm',100.*getError(L0Eff,t_.GetEntries()))
##HLT1
Hlt1Eff=1.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
print("HLT1: ",100.*Hlt1Eff,'pm',100.*getError(Hlt1Eff,t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")))
##HLT2
Hlt2Eff=1.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")/t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")
print ("HLT2: ",100.*Hlt2Eff,'pm',100.*getError(Hlt2Eff,t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS")))
TotalEff=1.*t_.GetEntries("(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")/t_counters.GetEntries()
print("TOTAL: ",100.*TotalEff,'pm',100.*getError(TotalEff,t_counters.GetEntries()))
print("")




#DISTRIBUTION OF MOTHERS GIVEN ONE IS A PI0

#pi0
counter_pi=0
##eta and pi0
counter_etapi=0
##pi0 and nomother:
counter_pinone=0
##pi0 and charm
counter_picharm=0
##pi0 and bottom
counter_pibottom=0
##pi0 and up
counter_pinohf=0
##pi0 and gamma
counter_pigamma=0
##pi0 pi0
counter_pipi_dif=0
counter_pipi_sam=0
##pi0 pi+-
counter_pipip=0

import numpy as np


mothers={};
for key in ["eta","pi0","nomother","bottom","charm","noHF"]:
    mothers[key]=0

for ev in t:
    motherid=[getattr(t,"gamma0_MC_MOTHER_ID"),getattr(t,"gamma_MC_MOTHER_ID")]
    #PI0
    if 111 in motherid:
        counter_pi+=1
        
        if 221 in motherid:
            counter_etapi+=1
            
        elif motherid==[111,111]:
        
            if t.gamma_MC_MOTHER_KEY!=t.gamma0_MC_MOTHER_KEY:
                counter_pipi_dif+=1
            else:
                counter_pipi_sam+=1
                
        elif 0 in motherid:
            counter_pinone+=1
            
        elif np.sum(list(map(lambda x: getProperty(x,"has_bottom") ,motherid))):
            counter_pibottom+=1
            
        elif np.sum(list(map(lambda x: getProperty(x,"has_charm"),motherid))):
            counter_picharm+=1
            
        else: 
            counter_pinohf+=1
        


        

pi0="pi0 "
N=1.*t.GetEntries()
print( pi0,100.*counter_pi/N,"pm",100.*getError(counter_pi/N,N))
print(pi0+" pi0 (dif)",100.*counter_pipi_dif/N,"pm",100.*getError(counter_pipi_dif/N,N))
print(pi0+" pi0 (sam)",100.*counter_pipi_sam/N,"pm",100.*getError(counter_pipi_sam/N,N))
print( pi0+" eta",100.*counter_etapi/N,"pm",100.*getError(counter_etapi/N,N))
print (pi0+" 0",100.*counter_pinone/N,"pm",100.*getError(counter_pinone/N,N))
print (pi0+" charm",100.*counter_picharm/N,"pm",100.*getError(counter_picharm/N,N))
print (pi0+" bottom",100.*counter_pibottom/N,"pm",100.*getError(counter_pibottom/N,N))
print (pi0+" NO_HF",100.*counter_pinohf/N,"pm",100.*getError(counter_pinohf/N,N))
print (pi0+" piplusminus",100.*counter_pipip/N,"pm",100.*getError(counter_pipip/N,N))
print (pi0+" gamma",100.*counter_pigamma/N,"pm",100.*getError(counter_pigamma/N,N))
print(10*'--')
counter_total = counter_pipi_dif+counter_etapi+counter_pinone+counter_picharm+counter_pibottom+counter_pinohf

print ("Total in categories",100.*counter_total/N,"pm",100.*getError(counter_total/N,N))


##GRANDMAS
nograndmas=0
no_pi0s=0
noheavyflavour=0
heavyflavour=0
for ev in t:
    if t.gamma0_MC_MOTHER_ID == 111:
        no_pi0s +=1.
        if not t.gamma0_MC_GD_MOTHER_ID:
            nograndmas+=1
        elif not  (getProperty(t.gamma0_MC_GD_MOTHER_ID,"has_charm") or getProperty(t.gamma0_MC_GD_MOTHER_ID,"has_bottom")):
            noheavyflavour+=1
        else: 
            heavyflavour+=1
            
    if t.gamma_MC_MOTHER_ID == 111:
        no_pi0s+=1
        if not t.gamma_MC_GD_MOTHER_ID:
            nograndmas+=1
        elif  not (getProperty(t.gamma_MC_GD_MOTHER_ID,"has_charm") or getProperty(t.gamma_MC_GD_MOTHER_ID,"has_bottom")):
            noheavyflavour+=1
        else: 
            heavyflavour+=1

print ("")
print ("Mothers")
print ("-----------------------------------------")
print ("pi0s without mother", 100.*nograndmas/no_pi0s,"pm",100.*getError(nograndmas/no_pi0s,no_pi0s))
print ("pi0s not from  heavy flavour", 100.*noheavyflavour/no_pi0s,"pm",100.*getError(noheavyflavour/no_pi0s,no_pi0s))
print ("pi0s from  heavy flavour", 100.*heavyflavour/no_pi0s,"pm",100.*getError(heavyflavour/no_pi0s,no_pi0s))

def isPi0nm(id,idm):
    if (id==111 and not idm):
        return True

def isPi0NHF(id,idm):
    if (id==111 and not getProperty(idm,"has_charm") and not getProperty(idm,"has_bottom") and idm):
        return True





#categories for the two daughters
pi0nmpi0nm=0
pi0nmpi0nhf=0
pi0nmeta=0
pi0nmnm=0
pi0nhfpi0nhf=0
pi0nhfeta=0
pi0nhfnm=0
etaeta=0
etanm=0
nmnm=0
for ev in t:
    if isPi0nm(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        if isPi0nm(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nmpi0nm+=1
        if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nmpi0nhf+=1
        if t.gamma_MC_MOTHER_ID==221:
            pi0nmeta+=1
        if not t.gamma_MC_MOTHER_ID:
            pi0nmnm+=1
            
    if isPi0nm(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
        # if isPi0nm(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        #     pi0nmpi0nm+=1
        if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
            pi0nmpi0nhf+=1
        if t.gamma0_MC_MOTHER_ID==221:
            pi0nmeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            pi0nmnm+=1


    if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
            pi0nhfpi0nhf+=1
        if t.gamma_MC_MOTHER_ID==221:
            pi0nhfeta+=1
        if not t.gamma_MC_MOTHER_ID:
            pi0nhfnm+=1

    if isPi0NHF(t.gamma_MC_MOTHER_ID,t.gamma_MC_GD_MOTHER_ID):
        # if isPi0NHF(t.gamma0_MC_MOTHER_ID,t.gamma0_MC_GD_MOTHER_ID):
        #     pi0nhfpi0nhf+=1
        if t.gamma0_MC_MOTHER_ID==221:
            pi0nhfeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            pi0nhfnm+=1

    if  t.gamma_MC_MOTHER_ID==221:
        if t.gamma0_MC_MOTHER_ID==221:
            etaeta+=1
        if not t.gamma0_MC_MOTHER_ID:
            etanm+=1

    if  t.gamma0_MC_MOTHER_ID==221:
        # if t.gamma_MC_MOTHER_ID==221:
        #     etaeta+=1
        if not t.gamma_MC_MOTHER_ID:
            etanm+=1
    

    if not t.gamma_MC_MOTHER_ID and not t.gamma0_MC_MOTHER_ID:
            nmnm+=1



N = 1.*t.GetEntries()

print ("")
print ("Categories in pairs")
print ("+++++++++++++++++++++++++++++++++++++")
print ("pi0nmpi0nm",100.*pi0nmpi0nm/N,"pm",100.*getError(pi0nmpi0nm/N,N))
print ("pi0nmpi0nhf",100.*pi0nmpi0nhf/N,"pm",100.*getError(pi0nmpi0nhf/N,N))
print ("pi0nmeta",100.*pi0nmeta/N,"pm",100.*getError(pi0nmeta/N,N))
print ("pi0nmnm",100.*pi0nmnm/N,"pm",100.*getError(pi0nmnm/N,N))
print ("pi0nhfpi0nhf",100.*pi0nhfpi0nhf/N,"pm",100.*getError(pi0nhfpi0nhf/N,N))
print ("pi0nhfeta",100.*pi0nhfeta/N,"pm",100.*getError(pi0nhfeta/N,N))
print ("pi0nhfnm",100.*pi0nhfnm/N,"pm",100.*getError(pi0nhfnm/N,N))
print ("etaeta",100.*etaeta/N,"pm",100.*getError(etaeta/N,N))
print ("etanm",100.*etanm/N,"pm",100.*getError(etanm/N,N))
print ("nmnm",100.*nmnm/N,"pm",100.*getError(nmnm/N,N))


totpi0=pi0nmpi0nm/0.01/N+pi0nmpi0nhf/0.01/N+pi0nmeta/0.01/N+pi0nmnm/0.01/N+pi0nhfpi0nhf/0.01/N+pi0nhfeta/0.01/N+pi0nhfnm/0.01/N
totincats = totpi0 + (etaeta + etanm + nmnm)/0.01/N
print ("")
print ("pi0tot",totpi0)
print("Total in categories",totincats)
print ("#######################################################################")
plot=False
if plot:
    gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")

    def fillH(h,t):
        for ev in t:
            if t.gamma0_MC_MOTHER_ID==111:
                h.Fill(t_.gamma0_TRUEPT)
            if t.gamma_MC_MOTHER_ID==111:
                h.Fill(t_.gamma_TRUEPT)
        return h


    def makeH(i):
        return TH1F("pt{0}".format(i),"",70,0,15000)

    # f=TFile("DiPhoton-Bdpipi.root")
    fs=[TFile("DiPhoton-minbias2017.root"),
        #TFile("DiPhoton-inclJpsi.root"),
        #TFile("DiPhoton-Bdpipi.root"),
        TFile("DiPhoton-hardPi02017.root")]

    colours=[kRed,kBlue+3,kGreen+3,6]
    hs=[]
    c=TCanvas()
    for i in range(2):
        t,t_,t_counters=getTree(fs[i])
        hs.append(makeH(i))
        hs[i].SetLineColor(colours[i])
        fillH(hs[i],t)
    
        if not i:
            hs[i].DrawNormalized()
        else:
            hs[i].DrawNormalized("same")        
    leg=TLegend(.6,.6,.6,.6)
    leg.AddEntry(hs[0],"minbias","L")
    #leg.AddEntry(hs[1],"inclJpsi","L")
    #leg.AddEntry(hs[2],"Bdpipi","L")
    leg.AddEntry(hs[1],"hardPi0","L")
    
    #leg.Draw()
#c.Print("gammapt.pdf]","pdf")

## To note in overleaf: inclJpsi sample has photons with higher pT ! This makes
## sense and validates the sample. Still: why more rate for minbias2017 ?
plot_selection=True

if plot_selection:
    lowbound=8000
    highbound = 12000
    bins = 5
    L0=TH1F("l0","",bins,lowbound,highbound)
    L1=TH1F("l1","",bins,lowbound,highbound)
    L2=TH1F("l2","",bins,lowbound,highbound)

    gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v1r1/RootTools/LHCbStyle/src/lhcbStyle.C")
    t_.Project("l0","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS)")
    #t_.Project("l1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)")
    t_.Project("l1","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaHighMassDecision_TOS")
    #t_.Project("l2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")
    t_.Project("l2","B_s0_M","(B_s0_L0ElectronDecision_TOS || B_s0_L0PhotonDecision_TOS) & B_s0_Hlt1B2GammaGammaHighMassDecision_TOS & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS")

    L0.SetLineColor(kRed)
    L1.SetLineColor(kBlue+3)
    L2.SetLineColor(kGreen+3)
    leg=TLegend(.6,.6,.6,.6)
    leg.AddEntry(L0,"l0","L")
    leg.AddEntry(L1,"hlt1","L")
    leg.AddEntry(L2,"hlt2","L")
    
    L0.DrawNormalized()
    L1.DrawNormalized("SAME")
    L2.DrawNormalized("same")
    leg.Draw()
    

        
        
