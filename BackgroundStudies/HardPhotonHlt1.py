import uproot3 as uproot
import numpy as np
import pandas
import matplotlib.pyplot as plt
def load_dfs():
    root = '/scratch47/adrian.casais/ntuples/background'
    f_51 = uproot.open(root + '/hardPhoton_51-big.root')
    f_52 = uproot.open(root + '/hardPhoton_52-big.root')
    
    t_51 = f_51['B2GammaGammaTuple/DecayTree']
    
    variables = ['B_s0_M',
                 'B_s0_PT',
                 'gamma_L0PhotonDecision_TOS',
                 'gamma_L0ElectronDecision_TOS',
                 'gamma_L0Global_TIS',
                 'gamma_Hlt1Phys_TIS',
                 'gamma_Hlt2Phys_TIS',
                 'gamma_PT',
                 'gamma_CL',
                 'gamma_P',
                 'gamma0_L0Calo_ECAL_TriggerET',
                 'gamma_L0Calo_ECAL_TriggerET',
                 'gamma0_L0PhotonDecision_TOS',
                 'gamma0_L0ElectronDecision_TOS',
                 'gamma0_L0Global_TIS',
                 'gamma0_Hlt1Phys_TIS',
                 'gamma0_Hlt2Phys_TIS',
                 'gamma0_PT',
                 'gamma0_CL',
                 'gamma0_P',
                 'B_s0_L0PhotonDecision_TOS',
                 'B_s0_L0ElectronDecision_TOS',
                 'gamma_L0PhotonDecision_TOS',
                 'gamma0_L0PhotonDecision_TOS',
                 'gamma_L0ElectronDecision_TOS',
                 'gamma0_L0ElectronDecision_TOS',
                 'gamma0_L0Calo_ECAL_xTrigger',
                 'gamma0_L0Calo_ECAL_yTrigger',
                 'gamma_L0Calo_ECAL_xTrigger',
                 'gamma_L0Calo_ECAL_yTrigger',
                 'B_s0_Hlt1B2GammaGammaDecision_TOS',
                 'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
                 'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',

                 'gamma0_TRUEP_Z',
                 'gamma0_TRUEP_X',
                 'gamma0_TRUEP_Y',
                 'gamma0_TRUEP_E',
                 'gamma_TRUEP_Z',
                 'gamma_TRUEP_X',
                 'gamma_TRUEP_Y',
                 'gamma_TRUEP_E',
                 'gamma_PZ',
                 'gamma0_PZ',
                 'gamma_PX',
                 'gamma0_PX',
                 'gamma_PY',
                 'gamma0_PY',
                 ]
    df51 = t_51.pandas.df(branches=variables)
    
    df51['gamma_ETA'] = 0.5*np.log( (df51['gamma_P']+df51['gamma_PZ'])/(df51['gamma_P']-df51['gamma_PZ']) )
    df51['gamma0_ETA'] = 0.5*np.log( (df51['gamma0_P']+df51['gamma0_PZ'])/(df51['gamma0_P']-df51['gamma0_PZ']) )
    #df51.query('gamma_L0Calo_ECAL_TriggerET > 2500 & gamma0_L0Calo_ECAL_TriggerET > 2500',inplace=True)
    return df51

def build_mass(df):
    df.eval('cos_theta_true = (gamma_TRUEP_X*gamma0_TRUEP_X+gamma_TRUEP_Y*gamma0_TRUEP_Y+gamma_TRUEP_Z*gamma0_TRUEP_Z)/gamma_TRUEP_E/gamma0_TRUEP_E',inplace=True)
    df.eval('cos_theta = (gamma_PX*gamma0_PX+gamma_PY*gamma0_PY+gamma_PZ*gamma0_PZ)/gamma_P/gamma0_P',inplace=True)
    df.eval('sin_theta0_true = sqrt(1 - gamma0_TRUEP_Z**2/gamma0_TRUEP_E**2)',inplace=True)
    df.eval('sin_theta1_true = sqrt(1 - gamma_TRUEP_Z**2/gamma_TRUEP_E**2)',inplace=True)

    df.eval('sin_theta0 = sqrt(1 - gamma0_PZ**2/gamma0_P**2)',inplace=True)
    df.eval('sin_theta1 = sqrt(1 - gamma_PZ**2/gamma_P**2)',inplace=True)
    
    df.eval('cos_phi = gamma_PX/gamma_PT', inplace=True)
    df.eval('sin_phi = sqrt(1-gamma_PX**2/gamma_PT**2)', inplace=True)
    df.eval('cos_phi0 = gamma0_PX/gamma0_PT', inplace=True)
    df.eval('sin_phi0 = sqrt(1 - gamma0_PX**2/gamma0_PT**2)', inplace=True)


    df.eval('deltaX=(gamma0_L0Calo_ECAL_xTrigger-gamma_L0Calo_ECAL_xTrigger)**2',inplace=True)
    df.eval('deltaY=(gamma0_L0Calo_ECAL_yTrigger-gamma_L0Calo_ECAL_yTrigger)**2',inplace=True)
    df.eval('sumX=(gamma0_L0Calo_ECAL_xTrigger**2+gamma0_L0Calo_ECAL_yTrigger**2)',inplace=True)
    df.eval('sumY=(gamma_L0Calo_ECAL_xTrigger**2+gamma_L0Calo_ECAL_yTrigger**2)',inplace=True)

    df.eval('B_s0_Hlt1M0=sqrt(gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(deltaX+deltaY)/sqrt(sumX*sumY))',inplace=True)


    #df.eval('B_s0_M_smearedMC = sqrt( 2*gamma_MCsmearedPT*gamma0_MCsmearedPT*(1-cos_theta)/(sin_theta1*sin_theta0) )',inplace=True)
    #df.eval('B_s0_M_smeared = sqrt( 2*gamma_smearedPT*gamma0_smearedPT*(1-cos_theta)/(sin_theta1*sin_theta0) )',inplace=True)
    df.eval('B_s0_M_L0 = sqrt( 2*gamma_L0Calo_ECAL_TriggerET*gamma0_L0Calo_ECAL_TriggerET*(1-cos_theta)/(sin_theta1*sin_theta0) )',inplace=True)
    #df.eval('B_s0_ET_L0 =sqrt((gamma_L0Calo_ECAL_TriggerET*cos_phi + gamma0_L0Calo_ECAL_TriggerET*cos_phi0)**2 + (gamma_L0Calo_ECAL_TriggerET*sin_phi + gamma0_L0Calo_ECAL_TriggerET*sin_phi0)**2)',inplace=True)
    #df.eval('B_s0_ET_smearedMC =sqrt((gamma_MCsmearedPT*cos_phi + gamma0_MCsmearedPT*cos_phi0)**2 + (gamma_MCsmearedPT*sin_phi + gamma0_MCsmearedPT*sin_phi0)**2)',inplace=True)
    #df.eval('B_s0_ET_smeared =sqrt((gamma_smearedPT*cos_phi + gamma0_smearedPT*cos_phi0)**2 + (gamma_smearedPT*sin_phi + gamma0_smearedPT*sin_phi0)**2)',inplace=True)
def compare_mass(df,hlt1cuts = False,nbins=50):
    if hlt1cuts:
        df_ = df.query('B_s0_Hlt1B2GammaGammaDecision_TOS & (B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS)')
    else:
        df_ = df
    #plt.hist(df_.query('B_s0_M_smearedMC<10000')['B_s0_M_smearedMC'],bins=50,density=True,alpha = 0.4)
    #plt.hist(df_.query('B_s0_M_smeared<10000')['B_s0_M_smeared'],bins=50,density=True,alpha = 0.4)
    plt.hist(df_.query('B_s0_Hlt1M0< 14000')['B_s0_Hlt1M0'],bins=nbins,density=True,alpha = 0.4,histtype='step')
    plt.hist(df_.query('B_s0_M< 14000')['B_s0_M'],bins=nbins,density=True,alpha = 0.4,histtype='step')
    
    plt.legend(["Original L0Mass","Offline mass"],bbox_to_anchor=[0.75,1.15])
    
    plt.show()
if __name__ == "__main__":
    df = load_dfs()
    build_mass(df)
    compare_mass(df,hlt1cuts=True,nbins=40)
