from ROOT import *
gROOT.ProcessLine('.x /scratch03/adrian.casais/diphotonanalysis/lhcbStyle.C')
root = '/scratch47/adrian.casais/ntuples/background'
f51=TFile(root + '/hardPhoton_51-big.root')
f52 = TFile(root + '/hardPhoton_52-big.root')
t51 = f51.Get('DTTBs2GammaGamma/DecayTree')
t52 = f52.Get('DTTBs2GammaGamma/DecayTree')
fS34 = TFile(root + '/Stripping34.root')
tS34 = fS34.Get('DecayTree/DecayTree')
fS34 = TFile(root + '/dimuon2018.root')
tS34 = fS34.Get('B2GammaGammaTuple/DecayTree')

L0 = '(B_s0_L0PhotonDecision_TOS || B_s0_L0ElectronDecision_TOS)'
HLT1 = '(B_s0_Hlt1B2GammaGammaDecision_TOS || B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)'
HLT2 = 'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'
cut = '{0} && {1} && {2}'.format(L0,HLT1,HLT2)
nbins = 7
low = 4800
high = 7500
h51 = TH1F('h51','',nbins,low,high)
h51.SetLineColor(kBlue)
h52 = TH1F('h52','',nbins,low,high)
h52.SetLineColor(kRed)
hS34 = TH1F('hS34','',nbins,low,high)
hS34.SetLineColor(kGreen)
var = 'min(gamma_PT,gamma0_PT)'
vars34 = 'min(Gamma1_PT,Gamma2_PT)'
var = 'B_s0_M'
#vars34='B_M'
vars34=var
tS34.Project('hS34',vars34,cut)
t51.Project('h51',var,cut)
t52.Project('h52',var,cut)
hS34.DrawNormalized('L')
h51.DrawNormalized('L E same')
h52.DrawNormalized('L E SAME')
