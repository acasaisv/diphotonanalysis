from ROOT import *
import uproot
import matplotlib.pyplot as plt
import numpy as np
from uncertainties import ufloat
from helpers import ufloat_eff

root = '/scratch47/adrian.casais/ntuples/background/'
f = uproot.open(root + 'hardPhoton_51-big.root:DTTBs2GammaGamma/DecayTree')
# root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
# f = uproot.open(root + '49100050_1000-1099_Sim10a-priv.root:DTTBs2GammaGamma/DecayTree')

keys = ['B_s0_L0PhotonDecision_TOS','B_s0_L0ElectronDecision_TOS','B_s0_Hlt1B2GammaGammaDecision_TOS','B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS','gamma_TRUEID','gamma0_TRUEID','gamma_MC_MOTHER_ID','gamma0_MC_MOTHER_ID','HeaviestQuarkInEvent']
df = f.arrays(keys,library='pd')
trigger = '(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS) and (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS' 
df.query(trigger,inplace=True)

#conditions
def meson_cut(mother1,index=''):
    return f'(abs(gamma{index}_TRUEID) == {mother1} or abs(gamma{index}_MC_MOTHER_ID)=={mother1})'
def double_meson(mother1,mother2):
    return f'({meson_cut(mother1)} and {meson_cut(mother2,index=0)})'

def prompt_cut(index=''):
    return f'(gamma{index}_TRUEID==22 and gamma{index}_MC_MOTHER_ID==0)'

def eff(df):
    return
categories = {

    'pi0pi0': double_meson(111,111),
    'pi0eta' :double_meson(111,221),
    'etapi0' :double_meson(221,111),
    'etaeta' :double_meson(221,221),
    'promptpi0' :f'({prompt_cut()} and {meson_cut(111,0)} )',
    'pi0prompt': f'({prompt_cut(0)} and {meson_cut(111)} )',
    'prompteta' :f'({prompt_cut()} and {meson_cut(221,0)} )',
    'etaprompt': f'({prompt_cut(0)} and {meson_cut(221)} )',
    'promptprompt':f'({prompt_cut(0)} and {prompt_cut()} )',
    'ks0pi0':double_meson(310,111),
    'pi0ks0':double_meson(111,310),
    'pi0el':double_meson(111,11),
    'elpi0':double_meson(11,111),
    'elprompt': f'({prompt_cut()} and {meson_cut(11,0)} )',
    'promptel': f'({prompt_cut(0)} and {meson_cut(11)} )',
    'total':''

}

eff = {}


print('Double categories: ')
print('matched means that either the photon or the mother is matched to the particle')
for key in categories:
    eff[key] = ufloat_eff(df.query(categories[key]).shape[0],df.shape[0])
    if categories['total']=='':
        categories['total'] = categories[key]
    else:
        categories['total'] += f' or {categories[key]}'
    print(key,eff[key])
print('')
print("Some inclusive numbers:")
print('at least one pi0: ', ufloat_eff(df.query(f'{meson_cut(111)} or {meson_cut(111,0)}').shape[0],df.shape[0]))
print('at least one eta: ', ufloat_eff(df.query(f'{meson_cut(221)} or {meson_cut(221,0)}').shape[0],df.shape[0]))
print('at least one pi0 or eta: ', ufloat_eff(df.query(f'{meson_cut(111)} or {meson_cut(111,0)} or {meson_cut(221)} or {meson_cut(221,0)}').shape[0],df.shape[0]))
print('Fraction of gen events with heavier d quark: ',ufloat_eff(df.query(f'HeaviestQuarkInEvent == 2').shape[0],df.shape[0]))
print('Fraction of gen events with heavier s quark: ',ufloat_eff(df.query(f'HeaviestQuarkInEvent == 3').shape[0],df.shape[0]))
print('Fraction of gen events with heavier c quark: ',ufloat_eff(df.query(f'HeaviestQuarkInEvent == 4').shape[0],df.shape[0]))
print('Fraction of gen events with heavier b quark: ',ufloat_eff(df.query(f'HeaviestQuarkInEvent == 5').shape[0],df.shape[0]))
print('Fraction of gen events with heavier c or b quark: ',ufloat_eff(df.query(f'HeaviestQuarkInEvent == 4 or HeaviestQuarkInEvent == 5').shape[0],df.shape[0]))