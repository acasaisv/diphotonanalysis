import numpy as np

def _powerlaw(x, a, k):
    return a * x**k
def crystalball_func(x, mu, sigma, alpha, n):
    t = (x - mu) / sigma * np.sign(alpha)
    abs_alpha = np.abs(alpha)
    a = (n / abs_alpha)**n * np.exp(-0.5 * alpha**2)
    b = (n / abs_alpha) - abs_alpha
    cond = np.less(t,-abs_alpha)
    func = np.where(cond,_powerlaw(b - t, a, -n),np.exp(-0.5 * t**2))
    func = np.maximum(func, np.zeros_like(func))
    return func
def double_crystalball(x, mu, sigmal,sigmar, alphal, nl, alphar, nr):
    cond = np.less(x, mu)
    func = np.where(cond,
                    crystalball_func(x, mu, sigmal, alphal, nl),
                    crystalball_func(x, mu, sigmar, -alphar, nr))
    return func

def sigma(E,b=0.0335):
    #Egev = E/1000                                                                                                                                                    
    #s2 = a**2/E
    s2 = b**2
    #s2 += c**2/E**2
    s = E*np.sqrt(s2)
    return s

def sample_cb(E,size=1000,b=0.0335,mult = 1.):
    x = np.linspace(E-20*sigma(E,b),E+20*sigma(E,b),size)
    dx = (13*sigma(E))/size
    y = double_crystalball(x,mu=E*mult,
                             sigmal=sigma(E,b)-0.0068*E,
                             sigmar=sigma(E,b)+0.0068*E,
                             alphar=1.97,
                             alphal=1.73,
                             nr=4.8,
                             nl=1.19)
    y/=np.sum(y)
    ysum = np.cumsum(y)
    random_seed = np.random.random_sample(1)
    ysampl =  x[np.digitize(random_seed,ysum)] 
    return ysampl
def smear(E,b=0.0335,mult=1.0):
    return np.array([sample_cb(e,b=b,mult=mult) for e in E])