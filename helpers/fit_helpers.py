import zfit
import numpy as np

def create_double_cb(name_prefix,
                     mass_range,
                    nevs,
                    extended=True,
                    scale_mu = True,
                    mass=5000,
                    sigma =(88,75,95),
                    dsigma = None,
                    al=False,
                    ar=False,
                    nl=False,
                    nr=False):
    parameters = {}
    parameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',0.98,0.95,1.1)
    parameters['sigma'] = zfit.Parameter(name_prefix+'sigma',*sigma)
    if not dsigma:
        parameters['dSigma'] = zfit.Parameter(name_prefix+'dsigma',0,-15,15,floating=True)
    else:
        parameters['dSigma'] = zfit.Parameter(name_prefix+'dsigma',*dsigma)
    parameters['sigmaR'] = zfit.ComposedParameter(name_prefix+'sigmaR',lambda x,y: x+y,params =[parameters['sigma'] ,parameters['dSigma']])
    parameters['sigmaL'] = zfit.ComposedParameter(name_prefix+'sigmaL',lambda x,y: x-y,params =[parameters['sigma'] ,parameters['dSigma']])
    #parameters['sigma_d'] = zfit.Parameter(name_prefix+'sigma_d',0,floating=False)
    parameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',mass,floating=False)
    if scale_mu:
        parameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[parameters['scale_m'] ,parameters['m_PDG']])
    else:
        parameters['m'] = zfit.Parameter(name_prefix+'m',mass,-1,1)

    if al:
        parameters['a_l'] = zfit.Parameter(name_prefix+'a_l',al,floating=False)
    else:
        parameters['a_l'] = zfit.Parameter(name_prefix+'a_l',1.,0.05,3.5)
    if nl:
        parameters['n_l'] = zfit.Parameter(name_prefix+'n_l',nl,floating=False)
    else:
        parameters['n_l'] = zfit.Parameter(name_prefix+'n_l',1,0.1,30)
    if ar:
        parameters['a_r'] = zfit.Parameter(name_prefix+'a_r',ar,floating=False)
    else:
        parameters['a_r'] = zfit.Parameter(name_prefix+'a_r',1,0.05,3.5)
    if nr:
        parameters['n_r'] = zfit.Parameter(name_prefix+'n_r',nr,floating=False)
    else:
        parameters['n_r'] = zfit.Parameter(name_prefix+'n_r',1,0.1,30)

    CB = zfit.pdf.DoubleCB(mu=parameters['m'],
                            sigmar=parameters['sigmaR'],
                            sigmal=parameters['sigmaL'],
                            alphar=parameters['a_r'],
                            nr=parameters['n_r'],
                            alphal=parameters['a_l'],
                            nl=parameters['n_l'],
                            obs=mass_range)

    if extended:
        parameters['nSig'] = zfit.Parameter(name_prefix+'nSig',nevs/2,0,nevs)
        CB = CB.create_extended(parameters['nSig'])
    return CB,parameters


def create_cb(name_prefix,mass_range,nevs,extended=True,mass=5000,scale_mu=True,sigma =(88,75,95),left=True,al=False,nl=False):
    parameters = {}
    parameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',0.98,0.95,1.05)
    parameters['sigma'] = zfit.Parameter(name_prefix+'sigma',*sigma)
    parameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',mass,floating=False)
    alpha = np.array((1,0.5,3))
    if scale_mu:
        parameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[parameters['scale_m'] ,parameters['m_PDG']])
    else:
        parameters['m'] = zfit.Parameter(name_prefix+'m',mass,-1,1)
    if not left:
        alpha = -alpha
        alpha[-1],alpha[-2] = alpha[-2],alpha[-1]
    if al:
        parameters['a_l'] = zfit.Parameter(name_prefix+'a_l',al,floating=False)
    else:
        parameters['a_l'] = zfit.Parameter(name_prefix+'a_l',*alpha)
    if nl:
        parameters['n_l'] = zfit.Parameter(name_prefix+'n_l',nl,floating=False)
    else:
        parameters['n_l'] = zfit.Parameter(name_prefix+'n_l',2,0,150)


    CB = zfit.pdf.CrystalBall(mu=parameters['m'],
                            sigma=parameters['sigma'],
                            alpha=parameters['a_l'],
                            n=parameters['n_l'],
                            obs=mass_range)

    if extended:
        parameters['nSig'] = zfit.Parameter(name_prefix+'nSig',nevs/2,0,nevs)
        CB = CB.create_extended(parameters['nSig'])
    return CB,parameters

def create_gauss(name_prefix,mass_range,nevs,extended=True,mass=5000,sigma =(88,75,95)):
    parameters = {}
    parameters['scale_m'] = zfit.Parameter(name_prefix+'scale_m',1.0,0.90,1.1)
    parameters['sigma'] = zfit.Parameter(name_prefix+'sigma',*sigma)
    parameters['m_PDG'] = zfit.Parameter(name_prefix+'m_PDG',mass,floating=False)
    parameters['m'] = zfit.ComposedParameter(name_prefix+'m',lambda x,y: x*y,params =[parameters['scale_m'] ,parameters['m_PDG']])

    CB = zfit.pdf.Gauss(mu=parameters['m'],
                            sigma=parameters['sigma'],
                            obs=mass_range)

    if extended:
        parameters['nSig'] = zfit.Parameter(name_prefix+'nSig',nevs/2,0,nevs)
        CB = CB.create_extended(parameters['nSig'])
    return CB,parameters

