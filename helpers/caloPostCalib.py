from Gaudi.Configuration import *
from Configurables import DaVinci
#--------------------------------------------------------#
#----- Prepare Ecal post-Calibration for B->Xgamma  -----#
#--------------------------------------------------------#
from Configurables import CaloPostCalibAlg
cpc=CaloPostCalibAlg("CaloPostCalib")

print (" ")
print ("  =========== CALO POSTCALIBRATION  ============= ")

postcalib_bool = False

# == Reco14-stripping21 post calibration :

# === Stripping 21r(0,1) data
if not DaVinci().getProp("Simulation") :
    if DaVinci().getProp("DataType") == '2011' :
        postcalib_bool = True
        print ("  ====  Apply photon post-calibration to Stripping 21 data sample (stripping v21r(0,1))")
        print ("  ==== 2011 data configuration !!! Apply SpdScaling === ")
        cpc.SpdScale = True
        cpc.Calib[0]   = -1.00
        cpc.Calib[1]   = +0.40
        cpc.Calib[2]   = +2.90
        cpc.CalibCNV[0]= +0.80
        cpc.CalibCNV[1]= +0.30
        cpc.CalibCNV[2]= +7.80
    elif DaVinci().getProp("DataType") == '2012' :
        postcalib_bool = True
        print ("  ====  Apply photon post-calibration to Stripping 21 data sample (stripping v21r(0,1))")
        print ("  ==== 2012 data configuration !!! Apply SpdScaling === ")
        cpc.SpdScale = True
        cpc.Calib[0]   = -1.60
        cpc.Calib[1]   = -2.00
        cpc.Calib[2]   = -2.10
        cpc.CalibCNV[0]= -0.40
        cpc.CalibCNV[1]= -3.50
        cpc.CalibCNV[2]= +0.40

    elif DaVinci().getProp("DataType") == '2015' :
        postcalib_bool = True
        print ("  ==== 2015 (S24) data configuration !!! === ") # no SPD scaling
        pol_fac=1.0
        polarity=os.getenv('POLARITY','unset')
        if  polarity == 'down':
            pol_fac=1.0065
            print ("   == polarity factor (MagDown) : ", pol_fac)
        elif  polarity == 'up':
            pol_fac=0.9925
            print ("  == polarity factor (MagUp) : ", pol_fac)
        else:
            print ("  !!! WARNING :  POLARITY UNSET : 2015 (S24) is up/down assymmetric !! You should update your setting !!!")
        cpc.Calib[0]   = 1.010*pol_fac
        cpc.Calib[1]   = 1.016*pol_fac
        cpc.Calib[2]   = 1.004*pol_fac
        cpc.CalibCNV[0]= 1.016*pol_fac
        cpc.CalibCNV[1]= 1.009*pol_fac
        cpc.CalibCNV[2]= 1.012*pol_fac
            
    elif DaVinci().getProp("DataType") == '2016' :
        postcalib_bool = True
        print ("  ==== 2016 (s28) data configuration WITH OFFLINE RECALIBRATION (calo-20170505) !!! === " )
        #from Configurables import CondDB 
        # == offline recalibration (calo-20170505)
        cpc.Calib[0]   = 1.019 * 0.988
        cpc.Calib[1]   = 1.015 * 0.990
        cpc.Calib[2]   = 0.999 * 0.993
        cpc.CalibCNV[0]= 1.027 * 0.985
        cpc.CalibCNV[1]= 1.010 * 0.989
        cpc.CalibCNV[2]= 1.005 * 0.998

    elif DaVinci().getProp("DataType") == '2017'  :
        postcalib_bool = True
        print ("  ==== 2017 (S29) data configuration !!! === ") # no SPD scaling
        cpc.Calib[0]   = 1.0083 # 1.019*0.9895
        cpc.Calib[1]   = 1.0117 # 1.015*0.9967
        cpc.Calib[2]   = 1.0010 # 0.999*1.0020
        cpc.CalibCNV[0]= 1.0147 # 1.027*0.9880
        cpc.CalibCNV[1]= 1.0067 # 1.010*0.9967
        cpc.CalibCNV[2]= 1.0129 # 1.005*1.0079

    elif DaVinci().getProp("DataType") == '2018'  :
        postcalib_bool = True
        print ("  ==== 2018 (S34) data configuration !!! === ") # no SPD scaling
        cpc.Calib[0]   = 1.009 
        cpc.Calib[1]   = 1.005 
        cpc.Calib[2]   = 0.989 
        cpc.CalibCNV[0]= 1.015 
        cpc.CalibCNV[1]= 0.999 
        cpc.CalibCNV[2]= 1.006 

if DaVinci().getProp("Simulation"):
    if DaVinci().getProp("DataType") == '2011' :
        # MC SIM08 post-calibration:
        postcalib_bool = True
        print ("  ==== 2011 MC SIM08 configuration !!! === ")
        cpc.Calib[0]   = 0.9995
        cpc.Calib[1]   = 1.0075
        cpc.Calib[2]   = 1.0280
        cpc.CalibCNV[0]= 0.9815
        cpc.CalibCNV[1]= 0.9969
        cpc.CalibCNV[2]= 1.0259
        cpc.Smear = 0.0230
    elif DaVinci().getProp("DataType") == '2012' :
        postcalib_bool = True
        print ("  ==== 2012 MC SIM08 configuration !!! === ")
        cpc.Calib[0]   = 1.0014
        cpc.Calib[1]   =1.0065
        cpc.Calib[2]   =1.028
        cpc.CalibCNV[0]=0.9813
        cpc.CalibCNV[1]=0.9970
        cpc.CalibCNV[2]=1.026
        cpc.Smear = 0.0130
    if DaVinci().getProp("DataType") == '2015' :
        postcalib_bool = True
        print ("  ==== 2015/2016 MC configuration !!! === ") # Sim09a => very close to Sim08 (large coef to be understood)
        cpc.Calib[0]   = 1.001
        cpc.Calib[1]   = 1.006
        cpc.Calib[2]   = 1.027
        cpc.CalibCNV[0]= 0.979
        cpc.CalibCNV[1]= 0.994
        cpc.CalibCNV[2]= 1.023
        cpc.Smear = 0.0130
    if DaVinci().getProp("DataType") == '2016' :
        postcalib_bool = True
        print ("  ==== 2015/2016 MC configuration !!! === ")
        cpc.Calib[0]   = 1.001
        cpc.Calib[1]   = 1.006
        cpc.Calib[2]   = 1.027
        cpc.CalibCNV[0]= 0.979
        cpc.CalibCNV[1]= 0.994
        cpc.CalibCNV[2]= 1.023
        cpc.Smear = 0.0130
    if DaVinci().getProp("DataType") == '2017' :
        postcalib_bool = True
        print ("  ==== 2017 MC configuration !!! === ")
        cpc.Calib[0]   = 1.0004
        cpc.Calib[1]   = 1.0065
        cpc.Calib[2]   = 1.0260
        cpc.CalibCNV[0]= 0.9804
        cpc.CalibCNV[1]= 0.9960
        cpc.CalibCNV[2]= 1.0241
        cpc.Smear = 0.0130
    if DaVinci().getProp("DataType") == '2018' :
       postcalib_bool = True
       print ("  ==== 2018 MC configuration !!! === ")
       cpc.Calib[0]   = 1.0012
       cpc.Calib[1]   = 1.0062
       cpc.Calib[2]   = 1.0265
       cpc.CalibCNV[0]= 0.9804
       cpc.CalibCNV[1]= 0.9950
       cpc.CalibCNV[2]= 1.0234
       cpc.Smear = 0
        
if not postcalib_bool:
    print (" ... no photon post-calibration to be applied ")

print ("  =============================================== ")
print (" ")


