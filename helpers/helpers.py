import numpy as np
import uproot3 as uproot
from uncertainties import ufloat
from sigfig import round

pTs =  np.array([3000,5000,7000,10000,12000,np.inf])
etas = np.array([1.5,2.5,3,np.inf])
spds = np.array([0,100,200,300,400,np.inf])
spds = np.array([0,200,300,np.inf])

pTsHlt1 =[3000,4000,5000,7600,np.inf]
etasHlt1 = np.array([1,np.inf])

pTsHlt2 =  np.array([3000,5000,7000,10000,np.inf])
etasHlt2 = np.array([1,2.5,3.5,np.inf])
spdsHlt2 = np.array([0,200,300,np.inf])
def double_binning(array_in):
    out = []
    l = len(array_in)
    for i in range(l-1):
            out.append(array_in[i])
            out.append((array_in[i]+array_in[i+1])/2.)
    if out[-1]!=np.inf:
        out[-1] = array_in[-1]
    return np.array(out)

def generate_binning(case=1):
    pTs =  np.array([3000,12000,np.inf])
    etas = np.array([1,3.5,np.inf])
    spds = np.array([0,450])
    double = *map(double_binning,(pTs,etas,spds)),
    quad = *map(double_binning,double),
    if case==1:
        return pts,ets,spds
    elif case==2:
        return double
    elif case ==3:
        return quad

bins = {
    'stripping':{
        # 'spds':[0,450],
        # 'etas':[1.0,3.5,6.0],
        # 'pts':[3000,4000,6000,9000,12000,np.inf],
        # 'matching':[0,50,75,100,120,np.inf],
        'etas':[1.0,6.0],
        'pts':[3000,np.inf],
        'matching':[0,50,100,150,200,400,600,800,np.inf],
        # 'matching':[0,40,80,np.inf],
        # 'matching':[0,40,80,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
    'custom':{
        'spds':[0,300,np.inf],
        'etas':[1,2.5,3.5,4.0,np.inf],
        'pts':[3000,np.inf],
        'matching':[0,50,100,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
    'l0':{
        'spds':[0,300,np.inf],
        'etas':[1,3.5,np.inf],
        'pts':[3000,5000,7000,9000,13000,np.inf],
        'matching':[0,50,100,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
    
    'vlow':{
        'spds':[0,np.inf],
        'etas':[0,np.inf],
        'pts':[1000,np.inf],
        'matching':[0,50,100,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
    'low':{
        'spds':[0,150,300,450],
        'etas':[1,3.5,np.inf],
        'pts':[3000,6000,9000,np.inf],
        'matching':[0,50,75,100,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
     'medium':{
        'spds':[0,200,300,np.inf],
        'etas':[1,3.5,np.inf],
        'pts':[3000,5000,7000,12000,np.inf],
        'matching':[0,50,75, 100,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]},
    'high':{
        'spds':[0,200,300,np.inf],
        'etas':[1,2.5,3.5,np.inf],
        'pts':[3000,5000,7000,10000,12000,np.inf],
        'matching':[0,50,75,100,200,np.inf],
        'regions':[0,37.e3,41.e3,np.inf]}}
ecal_regions = {
    'inner':[41.e3,np.inf],
    'middle':[37.e3,41.e3],
    'outer':[0,37.e3]
}
def nearest_rect(n):
    sqn = np.sqrt(n)
    if sqn==int(sqn):
        return (int(sqn),int(sqn))
    else:
        return int(sqn),int(sqn)+1


def get_error_w(weights_num,weights_den):
    num= weights_num.sum()
    den= weights_den.sum()
    if num ==0:
        num = num + weights_den.sum()/len(weights_den) ## coverage "protection" for very low efficiencies: add an average weight

    W1 = num
    W2 = den-num
    num2 = np.square(weights_num)
    den2 = np.square(weights_den)
    num2_sum = num2.sum()
    if num2_sum == 0:
        num2_sum = num**2 ## coverage "protection" for very low efficiencies
    den2_sum = den2.sum()
    sigma1 = num2_sum
    sigma2 = den2_sum - num2_sum
    
    return np.sqrt((W1**2*sigma2 + W2**2*sigma1)/(W1 + W2)**4)

def get_error(num,den):
    return np.sqrt(num*(den-num)/den**3)

def ufloat_eff(num,den):
    return ufloat(num/den,get_error(num,den))
def ufloat_eff_w(num,den):
    return ufloat(num.sum()/den.sum(),get_error_w(num,den))
def pack_eff(eff,err,mult=100):
    if np.isnan(eff):
        return r"-"
    eff_str = round(mult*eff,uncertainty=mult*err,format='PDG',sep='external_brackets',cutoff=35)
    return fr'\num{{{eff_str}}}'
    #return rf'\num{{{eff:.2f}({err:.2f})}}'

def ufloat_to_str(eff,mult=100):
    eff*=mult
    eff_str = round(eff.n,uncertainty=eff.s,format='PDG',sep='external_brackets',cutoff=35)
    return fr'\num{{{eff_str}}}'
    #return rf'\num{{{eff.n:.2f}({eff.s:.2f})}}'
    
def reweight_etammg_mc(df,dfMC):
    
    vars = ['eta_PT','eta_ETA',
            #'gamma_PP_IsNotH'
            #'nSPDHits',
            #'gamma_L0Calo_ECAL_TriggerET'
            ]
    
    reweighter = GBReweighter(max_depth=2, gb_args={'subsample': 0.5})
    reweighter.fit(original=dfMC[vars], original_weight=dfMC['sweights'],target=df[vars], target_weight=df['sweights'],)
    dfMC['sweights']= reweighter.predict_weights(dfMC[vars],original_weight=dfMC['sweights'])
    return dfMC

def get_ALPsdf(i,bs=False,background=False,extravars = [],tree = 'DTTBs2GammaGamma/DecayTree',extracut='',stripping=False):
    if type(i)!=str:
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        f_s = uproot.open(root + f'491000{i}_1000-1099_Sim10a-priv.root')
        f_s = uproot.open(root + f'new/491000{i}_1000-1099_Sim10a-priv-v44r11p1.root')
        
    elif i=='sim10':
        f_s = uproot.open('/scratch47/adrian.casais/ntuples/signal/b2gg-sim10.root')
        bs=True
    elif i=='pi0pi0':
        f_s = uproot.opeN('/scratch47/adrian.casais/ntuples/b2pi0pi0.root')
    else:
        f_s = uproot.open(i)
    t_s = f_s[tree]
    
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'gamma_TRUEPT',
            'gamma0_TRUEPT',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'B_s0_PT',
            'B_s0_P',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
            'B_s0_TRUEID',
            'nSPDHits',
            'gamma_CaloHypo_Saturation',
            'gamma0_CaloHypo_Saturation',
            'gamma_PP_IsPhoton',
            'gamma0_PP_IsPhoton',
            'gamma_CaloHypo_isNotE',
            'gamma0_CaloHypo_isNotE',
            'gamma0_CaloHypo_CellID',
            'gamma_CaloHypo_CellID', 
            'gamma_CaloHypo_X',
            'gamma_CaloHypo_Y',
            'gamma0_CaloHypo_X',
            'gamma0_CaloHypo_Y',
            'gamma_CaloHypo_HypoE',
            'gamma_CaloHypo_HypoEt',
            'gamma0_CaloHypo_HypoE',
            'gamma0_CaloHypo_HypoEt',
            'gamma_CL',
            'gamma0_CL',
            'gamma0_PP_CaloNeutralHcal2Ecal',
            'gamma_PP_CaloNeutralHcal2Ecal',
            'gamma_Matching',
            'gamma0_Matching'

            #'gamma0_CaloHypo_ClusterFrac',
            #'gamma_CaloHypo_ClusterFrac',
            #'gamma0_CaloHypo_Spread',
            #'gamma_CaloHypo_Spread',
            #'gamma0_CaloHypo_PrsE49',
            #'gamma_CaloHypo_PrsE49',
            #'gamma0_CaloHypo_PrsE4Max',
            #'gamma_CaloHypo_PrsE4Max'
            

            #'HeaviestQuark'
         
       
            ]
    vars = set(vars + extravars)
    truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
    if bs:
        truth = "abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22 & abs(B_s0_TRUEID)==531"
    if i=='pi0pi0':
        truth = "(abs(gamma_MC_MOTHER_ID) == 511 & abs(gamma0_MC_MOTHER_ID) == 511 & gamma_TRUEID==111 & gamma0_TRUEID==111 & abs(B_s0_TRUEID)==511)"
        truth += "or (abs(gamma_MC_MOTHER_ID) == 111 & abs(gamma0_MC_MOTHER_ID) == 511) & gamma_TRUEID==22 & gamma0_TRUEID==111 & abs(B_s0_TRUEID)==511)"
        truth += "or (abs(gamma_MC_MOTHER_ID) == 511 & abs(gamma0_MC_MOTHER_ID) == 111) & gamma_TRUEID==111 & gamma0_TRUEID==22 & abs(B_s0_TRUEID)==511)"

    df = t_s.pandas.df(branches=vars)
    if not background:
        df.query(truth,inplace=True)
    if stripping:
        df.query('gamma_P>6000 and gamma_PT > 1100 and gamma0_P>6000 and gamma_P>6000 and gamma0_PT > 1100 and gamma_CL > 0.3 and gamma0_CL > 0.3 and gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and gamma_PP_CaloNeutralHcal2Ecal<0.1 and B_s0_PT > 2000',inplace=True)

    #df.query('gamma_PT > 2500 | gamma0_PT > 2500',inplace=True)
    df['gamma_ETA'] = 0.5*np.log( (df['gamma_P']+df['gamma_PZ'])/(df['gamma_P']-df['gamma_PZ']) )
    df['gamma0_ETA'] = 0.5*np.log( (df['gamma0_P']+df['gamma0_PZ'])/(df['gamma0_P']-df['gamma0_PZ']) )
    #df.eval('dist=sqrt((gamma_CaloHypo_X-gamma0_CaloHypo_X)**2 + (gamma_CaloHypo_Y-gamma0_CaloHypo_Y)**2)',inplace=True)
    
    df['weights'] = np.ones(len(df))
    if extracut!='':
        df.query(extracut,inplace=True)
    df.reset_index(inplace=True)
    # if sample!=1.0 and sample<len(df):
    #     df=df.sample(n=sample)
    return df


def get_ALPsdf_sim10b(i,bs=False,background=False,extravars = [],tree = 'DTTBs2GammaGamma/DecayTree',sample=1.0,extracut='',stripping=False):
    if type(i)!=str:
        root = '/scratch47/adrian.casais/ntuples/signal/sim10/'
        f_s = uproot.open(root + f'new2/491000{i}-Sim10b.root')
    elif i=='pi0pi0':
        f_s = uproot.open('/scratch47/adrian.casais/ntuples/b2pi0pi0.root')
    else:
        f_s = uproot.open(i)
    t_s = f_s[tree]
    
    vars = ['gamma_PT',
            'gamma0_PT',
            'gamma_P',
            'gamma0_P',
            'gamma_PZ',
            'gamma0_PZ',
            'gamma_PX',
            'gamma0_PX',
            'gamma_PY',
            'gamma0_PY',
            'gamma_L0Calo_ECAL_TriggerET',
            'gamma0_L0Calo_ECAL_TriggerET',
            'gamma0_TRUEP_Z',
            'gamma0_TRUEP_X',
            'gamma0_TRUEP_Y',
            'gamma0_TRUEP_E',
            'gamma_TRUEP_Z',
            'gamma_TRUEP_X',
            'gamma_TRUEP_Y',
            'gamma_TRUEP_E',
            'gamma_TRUEPT',
            'gamma0_TRUEPT',
            'B_s0_L0PhotonDecision_TOS',
            'B_s0_L0ElectronDecision_TOS',
            'B_s0_TRUEID',
            'gamma_L0PhotonDecision_TOS',
            'gamma0_L0PhotonDecision_TOS',
            'gamma_L0ElectronDecision_TOS',
            'gamma0_L0ElectronDecision_TOS',
            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',

            'gamma0_L0Calo_ECAL_xTrigger',
            'gamma0_L0Calo_ECAL_yTrigger',
            'gamma_L0Calo_ECAL_xTrigger',
            'gamma_L0Calo_ECAL_yTrigger',
            'gamma0_L0Calo_ECAL_xProjection',
            'gamma0_L0Calo_ECAL_yProjection',
            'gamma_L0Calo_ECAL_xProjection',
            'gamma_L0Calo_ECAL_yProjection',
            
            'B_s0_Hlt1B2GammaGammaDecision_TOS',
            'B_s0_Hlt1B2GammaGammaHighMassDecision_TOS',
            'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
            'B_s0_M',
            'B_s0_PT',
            'B_s0_P',
            'gamma_MC_MOTHER_ID',
            'gamma0_MC_MOTHER_ID',
            'gamma_TRUEID',
            'gamma0_TRUEID',
            'B_s0_TRUEID',
            'nSPDHits',

            'gamma_PP_Saturation',
            'gamma0_PP_Saturation',
            'gamma_PP_IsPhoton',
            'gamma0_PP_IsPhoton',
            'gamma_PP_IsNotE',
            'gamma0_PP_IsNotE',
            'gamma_PP_CaloNeutralID',
            'gamma0_PP_CaloNeutralID',
            
            
            'gamma_CL',
            'gamma0_CL',
            'gamma0_PP_CaloNeutralHcal2Ecal',
            'gamma_PP_CaloNeutralHcal2Ecal',
            'gamma_Matching',
            'gamma0_Matching'

            #'gamma0_CaloHypo_ClusterFrac',
            #'gamma_CaloHypo_ClusterFrac',
            #'gamma0_CaloHypo_Spread',
            #'gamma_CaloHypo_Spread',
            #'gamma0_CaloHypo_PrsE49',
            #'gamma_CaloHypo_PrsE49',
            #'gamma0_CaloHypo_PrsE4Max',
            #'gamma_CaloHypo_PrsE4Max'
            

            #'HeaviestQuark'
         
       
            ]
    
    vars = set(vars + extravars)
    truth = "(abs(gamma_MC_MOTHER_ID) == 54 & abs(gamma0_MC_MOTHER_ID) == 54 & gamma_TRUEID==22 & gamma0_TRUEID==22) & B_s0_TRUEID==54"
    if bs:
        truth = "abs(gamma_MC_MOTHER_ID) == 531 & abs(gamma0_MC_MOTHER_ID) == 531 & gamma_TRUEID==22 & gamma0_TRUEID==22 & abs(B_s0_TRUEID)==531"
    if i=='pi0pi0':
        truth = "(abs(gamma_MC_MOTHER_ID) == 511 & abs(gamma0_MC_MOTHER_ID) == 511 & gamma_TRUEID==111 & gamma0_TRUEID==111 & abs(B_s0_TRUEID)==511)"
        truth += "or (abs(gamma_MC_MOTHER_ID) == 111 & abs(gamma0_MC_MOTHER_ID) == 511) & gamma_TRUEID==22 & gamma0_TRUEID==111 & abs(B_s0_TRUEID)==511)"
        truth += "or (abs(gamma_MC_MOTHER_ID) == 511 & abs(gamma0_MC_MOTHER_ID) == 111) & gamma_TRUEID==111 & gamma0_TRUEID==22 & abs(B_s0_TRUEID)==511)"

    df = t_s.pandas.df(branches=vars)
    if not background:
        df.query(truth,inplace=True)
    if stripping:
        df.query('gamma_P>6000 and gamma_PT > 1100 and gamma0_P>6000 and gamma_P>6000 and gamma0_PT > 1100 and gamma_CL > 0.3 and gamma0_CL > 0.3 and gamma0_PP_CaloNeutralHcal2Ecal < 0.1 and gamma_PP_CaloNeutralHcal2Ecal<0.1 and B_s0_PT > 2000',inplace=True)

    #df.query('gamma_PT > 2500 | gamma0_PT > 2500',inplace=True)
    for i in '','0':
        df[f'gamma{i}_ETA'] = 0.5*np.log( (df[f'gamma{i}_P']+df[f'gamma{i}_PZ'])/(df[f'gamma{i}_P']-df[f'gamma{i}_PZ']) )
        df[f'gamma{i}_CaloHypo_X'] = df[f'gamma{i}_L0Calo_ECAL_xProjection']
        df[f'gamma{i}_CaloHypo_Y'] = df[f'gamma{i}_L0Calo_ECAL_yProjection']
        df[f'gamma{i}_CaloHypo_CellID'] = df[f'gamma{i}_PP_CaloNeutralID']
        df[f'gamma{i}_CaloHypo_Saturation'] = df[f'gamma{i}_PP_Saturation']
        

    
    #df.eval('dist=sqrt((gamma_CaloHypo_X-gamma0_CaloHypo_X)**2 + (gamma_CaloHypo_Y-gamma0_CaloHypo_Y)**2)',inplace=True)
    
    df['weights'] = np.ones(len(df))
    if extracut!='':
        df.query(extracut,inplace=True)
    df.reset_index(inplace=True)
    if sample!=1.0 and sample<len(df):
        df=df.sample(n=sample)
    return df
masses_map = {40:'5 GeV',
              42:'7 GeV',
              44:'9 GeV',
              46:'11 GeV',
              47:'13 GeV',
              50:'15 GeV',
              48:'4 GeV',
              'bs':'Bs0 (5 GeV)'}
sim10_masses_map = {
    #30:4.5,
    40:5,
    41:6,
    42:7,
    43:8,
    44:9,
    45:10,
    46:11,
    47:13,
    48:15,
    49:17,
    50:19,
    51:20,
    'pi0pi0':5,
    'gg':5.367
    #'bs':5.367,
    #'sim09':5.367,
    #'sim10':5.367
}



def latex_fit_parameters(result,params,param_labels,units):
    import pandas as pd
    from helpers import pack_eff
    import zfit
    hessians = result.hesse()
    errors = [hessians[param]['error'] for param in params]
    params = [zfit.run(param) for param in params]
    values = []
    for val,err in zip(params,errors):
        values.append(pack_eff(val,err))
        
    dic = {'Parameter':param_labels,
        'Units':units,
        'Value':values}


    dflatex = pd.DataFrame(dic)
    print(dflatex.to_latex(index=False,escape=False))
    return dflatex.to_latex(index=False,escape=False)
    


trigger_cuts={
    'L0':   '(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS)',
    'HLT1': '(B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS)',
    'HLT2': 'B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS',
    'ALL':  '(B_s0_L0PhotonDecision_TOS or B_s0_L0ElectronDecision_TOS) and (B_s0_Hlt1B2GammaGammaDecision_TOS or B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) and B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS'}

