import matplotlib.pyplot as plt
#import mplhep
#mplhep.style.use("LHCb2")

import numpy as np

font = {'family' : 'serif',
                #'weight' : 'bold',
        'size'   : 12}

plt.rc('font', **font)
plt.rc("text", usetex=True)

def plot_fit(mass,full_model,components,yields,labels,colors,nbins,myrange=(5000,6000),xlabel='Mass',savefile='',plot=False,pull_plot=True):
    import zfit
    m1g,m2g = myrange
    total_yield = sum(yields)
    xBs = np.linspace(m1g,m2g,1000)   
    countsBs, bin_edgesBs = np.histogram(mass, nbins, range=(m1g, m2g))
    bin_centresBs = (bin_edgesBs[:-1] + bin_edgesBs[1:])/2.
    errBs = np.array([np.sqrt(x) if x>0 else 1 for x in countsBs ])
    print(countsBs)
    print(errBs)
    dpi = 80.
    px = 1./dpi
    fig,ax = plt.subplots(2,1,gridspec_kw={'height_ratios': [3, 1]},figsize=(8,8))
    ax[0].errorbar(bin_centresBs, countsBs, yerr=errBs, fmt='o', color='xkcd:black')
    y = []
    for comp,label,color,yiel in zip(components,labels,colors,yields):
        y.append((m2g-m1g)/nbins*zfit.run(yiel*comp.pdf(xBs) ))
        ax[0].plot(xBs,y[-1],'-',linewidth=2,color=color,label=label)
    yfull=(m2g-m1g)/nbins*zfit.run(total_yield*full_model.pdf(xBs) )
    if len(components)>1:
        ax[0].plot(xBs,yfull,'--',linewidth=2,color='blue')
    pulls = []

    for x,i in zip(bin_centresBs,range(len(bin_centresBs))):
        delta = countsBs[i]-(m2g-m1g)/nbins*total_yield*zfit.run(full_model.pdf(x))
        sigma = errBs[i]
        pulls.append(delta/sigma)
    ax[0].legend()
    max_pull = np.ceil(max(abs(np.array(pulls))))
    ax[1].set_ylim([-max_pull,max_pull])
    if pull_plot:
        ticks = np.arange(-max_pull,max_pull+1,2)
        ax[1].set_yticks(ticks)
    ax[0].set_xlim([m1g,m2g])
    #ax[0].set_ylabel()
    ax[0].set_xlabel(xlabel,horizontalalignment='right', x=1.0,
    #fontsize=25
    )
    ax[0].set_ylabel(r'Yield/ {:.2f} MeV'.format((m2g-m1g)/nbins),horizontalalignment='left',
    #fontsize=25
    )
    if pull_plot:
        ax[1].set_xlim([m1g,m2g])
        ax[1].hlines([0],m1g,m2g ,colors='black',linestyles='dashed')
        ax[1].set_ylabel(r'Pull',)#fontsize=20)
    
        ax[1].plot(bin_centresBs,pulls,marker='o',color='black',linestyle='None')
    
    
    ax[0].legend()#fontsize=25)
    plt.tight_layout()
    if savefile!='':
        #pass
        plt.savefig(savefile)
    if plot:
        plt.show()
