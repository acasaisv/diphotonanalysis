import pandas as pd
import numpy as np
def process_variables():
    rs = [
        '1_0',
        '1_35',
        '1_7',
        ]
    
    rs_1 =[
        '1.00',
        '1.35',
        '1.70']
    

    _var_list = [
                #'B_s0_CONEANGLE_{}',
                'B_B_CONEMULT_{}',
                'B_B_CONEP_{}',
                'B_B_CONEPASYM_{}',
                'B_B_CONEPT_{}',
                'B_B_CONEPTASYM_{}']
    _var_list_1 = [
                'B_s0_{}_cc_mult',
                'B_s0_{}_cc_vPT',
                'B_s0_{}_cc_PZ',
                'B_s0_{}_cc_asy_P',
                'B_s0_{}_cc_asy_PT',

                'gamma_{}_cc_mult',
                'gamma_{}_cc_vPT',
                'gamma_{}_cc_PZ',
                'gamma_{}_cc_asy_P',
                'gamma_{}_cc_asy_PT',
                
                'gamma0_{}_cc_mult',
                'gamma0_{}_cc_vPT',
                'gamma0_{}_cc_PZ',
                'gamma0_{}_cc_asy_P',
                'gamma0_{}_cc_asy_PT',
        ]
    
    all_cone_vars=[]
    all_cone_vars_1=[]
    
    trigger_vars = ['B_L0ElectronDecision_TOS','B_L0PhotonDecision_TOS','B_Hlt1B2GammaGammaDecision_TOS','B_Hlt1B2GammaGammaHighMassDecision_TOS','B_Hlt2RadiativeB2GammaGammaDecision_TOS','Gamma1_PT','Gamma2_PT','Gamma1_PP_Saturation','Gamma2_PP_Saturation','B_M', 'Gamma1_PP_IsPhoton', 'Gamma2_PP_IsPhoton', 'Gamma1_PP_IsNotH', 'Gamma2_PP_IsNotH', 'Gamma1_PP_CaloNeutralHcal2Ecal', 'Gamma2_PP_CaloNeutralHcal2Ecal'
                    ]
    trigger_vars_1 = ['B_s0_L0ElectronDecision_TOS','B_s0_L0PhotonDecision_TOS','B_s0_Hlt1B2GammaGammaDecision_TOS','B_s0_Hlt1B2GammaGammaHighMassDecision_TOS','B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS','gamma_PT','gamma0_PT','gamma_CaloHypo_Saturation', 'gamma0_CaloHypo_Saturation','B_s0_M', 'gamma_PP_IsPhoton', 'gamma0_PP_IsPhoton', 'gamma_PP_IsNotH', 'gamma0_PP_IsNotH', 'gamma_PP_CaloNeutralHcal2Ecal', 'gamma0_PP_CaloNeutralHcal2Ecal'
                    ]
    for r in rs:
        all_cone_vars+= [i.format(r) for i in _var_list]
    for r in rs_1:
        all_cone_vars_1 += [i.format(r) for i in _var_list_1]

    return all_cone_vars,trigger_vars,all_cone_vars_1,trigger_vars_1,rs,rs_1

def get_cuts():


    trigger_cond = '(B_L0ElectronDecision_TOS | B_L0PhotonDecision_TOS) & (B_Hlt1B2GammaGammaDecision_TOS | B_Hlt1B2GammaGammaHighMassDecision_TOS) & B_Hlt2RadiativeB2GammaGammaDecision_TOS and Gamma1_PT > 3000 and Gamma2_PT > 3000 and Gamma1_PP_Saturation <1 and Gamma2_PP_Saturation <1 and Gamma1_PP_IsPhoton > 0.85 and Gamma2_PP_IsPhoton > 0.85 and Gamma1_PP_IsNotH > 0.3 & Gamma1_PP_CaloNeutralHcal2Ecal < 0.1 & Gamma2_PP_IsNotH > 0.3 & Gamma2_PP_CaloNeutralHcal2Ecal < 0.1'
    trigger_cond_1  = '(B_s0_L0ElectronDecision_TOS | B_s0_L0PhotonDecision_TOS) & (B_s0_Hlt1B2GammaGammaDecision_TOS | B_s0_Hlt1B2GammaGammaHighMassDecision_TOS) & B_s0_Hlt2RadiativeB2GammaGammaDecision_TOS and gamma_PT  > 3000 and gamma0_PT > 3000 and gamma_CaloHypo_Saturation<1 and gamma0_CaloHypo_Saturation<1 and gamma_PP_IsPhoton > 0.85 and gamma0_PP_IsPhoton > 0.85 and gamma_PP_IsNotH > 0.3 & gamma_PP_CaloNeutralHcal2Ecal < 0.1 & gamma0_PP_IsNotH > 0.3 & gamma0_PP_CaloNeutralHcal2Ecal < 0.1'

    return trigger_cond,trigger_cond_1

def relabel_signal(signal_df_dummy,rs_1,rs):
    signal_df = pd.DataFrame()

    for i,j in zip(rs_1,rs):
        signal_df['B_B_CONEMULT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_mult'.format(i)]
        signal_df['B_B_CONEP_{}'.format(j)] = np.sqrt(np.array(signal_df_dummy['B_s0_{}_cc_vPT'.format(i)])**2 + np.array(signal_df_dummy['B_s0_{}_cc_PZ'.format(i)])**2)
        signal_df['B_B_CONEPASYM_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_asy_P'.format(i)]
        signal_df['B_B_CONEPT_{}'.format(j)] = signal_df_dummy['B_s0_{}_cc_vPT'.format(i)]
        signal_df['B_B_CONEPTASYM_{}'.format(j)]= signal_df_dummy['B_s0_{}_cc_asy_PT'.format(i)]
    signal_df['B_M']=signal_df_dummy['B_s0_M']
    if 'MinPT' in signal_df_dummy.keys():
        signal_df['MinPT']=signal_df_dummy['MinPT']
        signal_df['MaxPT']=signal_df_dummy['MaxPT']

    return signal_df